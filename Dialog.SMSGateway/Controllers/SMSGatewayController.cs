﻿using Dialog.SMSGateway.DAL;
using Dialog.SMSGateway.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Dialog.SMSGateway.Controllers
{
    [RoutePrefix("smsgateway")]
    public class SMSGatewayController : ApiController
    {
        private string _connectionString = "";

        public SMSGatewayController()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        }

        [Route("sendsms")]
        //[HttpGet]
        [AcceptVerbs("POST")]
        public string SendSMS(SMSRequest requestSMS)
        {
            return SendSMS(requestSMS.mobileNumber, requestSMS.otpNumber);
        }

        private string SendSMS(string mobileNo, string message)
        {
            string response = "";
            message = string.Format(ConfigurationManager.AppSettings["OtpMessage"].ToString(), message);

            using (var client = new HttpClient())
            {
                var key = ConfigurationManager.AppSettings["DialogKey"].ToString();

                string s = "https://e-sms.dialog.lk/api/v1/message-via-url/create/url-campaign?esmsqk=" + key +
                    "&list=" + mobileNo + "&source_address=" + "Lka.Sfa" + "&message=" + message;

                client.BaseAddress = new Uri(s);

                var responseTask = client.GetAsync(s);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    HttpStatusCode readTask = result.StatusCode;

                    response = readTask.ToString();
                }
            }

            return response;
        }

        [Route("verifyotp")]
        //[HttpGet]
        [AcceptVerbs("POST")]
        public string VerifyOTP(RepsOTPRequest verifyOtp)
        {
            string response = "Failed";

            SMSRequest otp = new SMSRequest();
            SMSGatewayDAL gatewayDAL = new SMSGatewayDAL();

            if (verifyOtp.accessToken != null)
                otp = gatewayDAL.GetOriginatorOtpByAccessToken(_connectionString, verifyOtp.accessToken);
            else if (verifyOtp.userName != null)
                otp = gatewayDAL.GetOriginatorOtpByUserName(_connectionString, verifyOtp.userName);

            string mobileNo = otp.mobileNumber;
            string otpNo = otp.otpNumber;

            if (otpNo == verifyOtp.otpNumber)
            {
                response = "OK";
            }

            return response;
        }

        [Route("verifyotpnew")]
        //[HttpGet]
        [AcceptVerbs("POST")]
        public Response VerifyOTPNew(RepsOTPRequest verifyOtp)
        {
            bool response = false;

            SMSRequest otp = new SMSRequest();
            SMSGatewayDAL gatewayDAL = new SMSGatewayDAL();

            if (verifyOtp.accessToken != null)
                otp = gatewayDAL.GetOriginatorOtpByAccessToken(_connectionString, verifyOtp.accessToken);
            else if (verifyOtp.userName != null)
                otp = gatewayDAL.GetOriginatorOtpByUserName(_connectionString, verifyOtp.userName);

            string mobileNo = otp.mobileNumber;
            string otpNo = otp.otpNumber;

            if (otpNo == verifyOtp.otpNumber)
            {
                response = true;
            }

            Response res = new Response();
            res.result = response;

            return res;
        }

        [Route("sendotp")]
        //[HttpGet]
        [AcceptVerbs("POST")]
        public string SendOTP(RepsOTPRequest requestSMS)
        {
            SMSRequest otp = new SMSRequest();
            SMSGatewayDAL gatewayDAL = new SMSGatewayDAL();

            int refreshMinute = 1;
            try
            {
                refreshMinute = Convert.ToInt32(ConfigurationManager.AppSettings["RefreshMinute"].ToString());
            }
            catch { }

            if (requestSMS.accessToken != null)
                otp = gatewayDAL.GetOriginatorOtpByAccessToken(_connectionString, requestSMS.accessToken);
            else if (requestSMS.userName != null)
                otp = gatewayDAL.GetOriginatorOtpByUserName(_connectionString, requestSMS.userName);

            string mobileNo = otp.mobileNumber;
            string otpNo = otp.otpNumber;

            if (otp.otpNumber == "")
            {
                otpNo = GetNumericOTP();
                if (otp.userName != null)
                    gatewayDAL.UpdateOTP(_connectionString, otp.userName, otpNo);
                else
                    return "Failed";
            }
            else if (otp.otpCreateTime == DateTime.MinValue)
            {
                otpNo = GetNumericOTP();
                if (otp.userName != null)
                    gatewayDAL.UpdateOTP(_connectionString, otp.userName, otpNo);
                else
                    return "Failed";
            }
            else if (otp.otpCreateTime.AddMinutes(refreshMinute) < DateTime.Now)
            {
                otpNo = GetNumericOTP();
                if (otp.userName != null)
                    gatewayDAL.UpdateOTP(_connectionString, otp.userName, otpNo);
                else
                    return "Failed";
            }

            if (requestSMS.mobileNumber != null)
                mobileNo = requestSMS.mobileNumber;

            return SendSMS(mobileNo, otpNo);
        }

        [Route("sendotpnew")]
        //[HttpGet]
        [AcceptVerbs("POST")]
        public Response SendOTPNew(RepsOTPRequest requestSMS)
        {
            bool response = false;
            SMSRequest otp = new SMSRequest();
            SMSGatewayDAL gatewayDAL = new SMSGatewayDAL();

            int refreshMinute = 1;
            try
            {
                refreshMinute = Convert.ToInt32(ConfigurationManager.AppSettings["RefreshMinute"].ToString());
            }
            catch { }

            if (requestSMS.accessToken != null)
                otp = gatewayDAL.GetOriginatorOtpByAccessToken(_connectionString, requestSMS.accessToken);
            else if (requestSMS.userName != null)
                otp = gatewayDAL.GetOriginatorOtpByUserName(_connectionString, requestSMS.userName);

            string mobileNo = otp.mobileNumber;
            string otpNo = otp.otpNumber;

            if (otp.otpNumber == "")
            {
                otpNo = GetNumericOTP();
                if (otp.userName != null)
                    gatewayDAL.UpdateOTP(_connectionString, otp.userName, otpNo);
                else
                    response = false;
            }
            else if (otp.otpCreateTime == DateTime.MinValue)
            {
                otpNo = GetNumericOTP();
                if (otp.userName != null)
                    gatewayDAL.UpdateOTP(_connectionString, otp.userName, otpNo);
                else
                    response = false;
            }
            else if (otp.otpCreateTime.AddMinutes(refreshMinute) < DateTime.Now)
            {
                otpNo = GetNumericOTP();
                if (otp.userName != null)
                    gatewayDAL.UpdateOTP(_connectionString, otp.userName, otpNo);
                else
                    response = false;
            }

            if (requestSMS.mobileNumber != null)
                mobileNo = requestSMS.mobileNumber;

            SendSMS(mobileNo, otpNo);

            Response res = new Response();
            res.result = true;

            return res;
        }

        public string GetNumericOTP()
        {
            int otpLength = 6;
            try
            {
                otpLength = Convert.ToInt32(ConfigurationManager.AppSettings["OtpLength"].ToString());
            }
            catch { }

            string numbers = "0123456789";
            Random random = new Random();
            string otp = string.Empty;
            for (int i = 0; i < otpLength; i++)
            {
                int tempval = random.Next(0, numbers.Length);
                otp += tempval;
            }

            return otp;
        }
    }

    public class Response
    {
        public bool result { get; set; }
    }
}

