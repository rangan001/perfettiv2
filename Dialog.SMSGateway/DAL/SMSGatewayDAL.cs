﻿using Dialog.SMSGateway.Models;
using System;
using System.Data.SqlClient;

namespace Dialog.SMSGateway.DAL
{
    public class SMSGatewayDAL
    {
        public SMSRequest GetOriginatorOtpByAccessToken(string connectionString, string accessToken)
        {
            SMSRequest response = new SMSRequest();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var query = "SELECT originator, otp_mobile, otp_number, otp_createtime " +
                            "FROM originator " +
                            "WHERE access_token = '" + accessToken + "'";

                SqlCommand command = new SqlCommand(query, conn);

                try
                {
                    conn.Open();
                    var reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                response.userName = reader.GetString(0);
                            }
                            catch { response.userName = ""; }
                            try
                            {
                                response.mobileNumber = reader.GetString(1);
                            }
                            catch { response.mobileNumber = ""; }
                            try
                            {
                                response.otpNumber = reader.GetString(2);
                            }
                            catch { response.otpNumber = ""; }
                            try
                            {
                                response.otpCreateTime = reader.GetDateTime(3);
                            }
                            catch(Exception e) { response.otpCreateTime = DateTime.MinValue; }
                        }
                    }

                    reader.Close();
                    conn.Close();
                    return response;
                }
                catch (Exception ex)
                {
                    if (conn.State != System.Data.ConnectionState.Closed)
                    {
                        conn.Close();
                    }

                    return null;
                }
            }
        }

        public SMSRequest GetOriginatorOtpByUserName(string connectionString, string userName)
        {
            SMSRequest response = new SMSRequest();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var query = "SELECT originator, otp_mobile, otp_number, otp_createtime " +
                            "FROM originator " +
                            "WHERE originator = '" + userName + "'";

                SqlCommand command = new SqlCommand(query, conn);

                try
                {
                    conn.Open();
                    var reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                response.userName = reader.GetString(0);
                            }
                            catch { response.userName = ""; }
                            try
                            {
                                response.mobileNumber = reader.GetString(1);
                            }
                            catch { response.mobileNumber = ""; }
                            try
                            {
                                response.otpNumber = reader.GetString(2);
                            }
                            catch { response.otpNumber = ""; }
                            try
                            {
                                response.otpCreateTime = reader.GetDateTime(3);
                            }
                            catch (Exception e) { response.otpCreateTime = DateTime.MinValue; }
                        }
                    }

                    reader.Close();
                    conn.Close();
                    return response;
                }
                catch (Exception ex)
                {
                    if (conn.State != System.Data.ConnectionState.Closed)
                    {
                        conn.Close();
                    }

                    return null;
                }
            }
        }

        public bool UpdateOTP(string connectionString, string userName, string OTP)
        {
            bool response = false;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var query = "UPDATE originator SET " +
                                "otp_number = '" + OTP + "', " +
                                "otp_createtime = '" + DateTime.Now.ToString() + 
                            "' WHERE originator = '" + userName + "'";

                var command = new SqlCommand(query, conn);

                try
                {
                    conn.Open();
                    int updateRowCount = command.ExecuteNonQuery();
                    if (updateRowCount > 0)
                    {
                        response = true;
                    }

                    conn.Close();
                    return response;
                }
                catch
                {
                    if (conn.State != System.Data.ConnectionState.Closed)
                    {
                        conn.Close();
                    }

                    return response;
                }
            }
        }
    }
}
