﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dialog.SMSGateway.Models
{
    public class SMSRequest
    {
        public string userName { get; set; }
        public string mobileNumber { get; set; }
        public string otpNumber { get; set; }
        public DateTime otpCreateTime { get; set; }
    }

    public class RepsOTPRequest
    {
        public string accessToken { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string mobileNumber { get; set; }
        public string otpNumber { get; set; }
    }
}
