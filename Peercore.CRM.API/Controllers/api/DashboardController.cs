﻿using Peercore.CRM.API.Models;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Peercore.CRM.API.Controllers.api
{
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        //Product[] products = new Product[]
        //{
        //    new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 },
        //    new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M },
        //    new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M }
        //};

        //[Route("~/api/dashboard/products")]
        //public IEnumerable<Product> GetAllProducts()
        //{
        //    return products;
        //}

        //[Route("~/api/dashboard/product/{id}")]
        //[HttpGet]
        //public IHttpActionResult GetProduct(int id)
        //{
        //    var product = products.FirstOrDefault((p) => p.Id == id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(product);
        //}

        // GET api/Books/5

        [Route("GetTodaySales")]
        //[HttpGet]
        [AcceptVerbs("GET", "POST")]
        public IHttpActionResult GetTodaySales(string userName)
        {
            TodaySales sales = new TodaySales();

            try
            {
                var salesVal = SalesBR.Instance.GetTodaySales(userName);
                sales.SalesValue = salesVal;
            }
            catch (Exception ex) { }

            if (sales == null)
            {
                return NotFound();
            }

            return Ok(sales);
        }

        [Route("GetTodayVCCount")]
        [HttpGet]
        public IHttpActionResult GetTodayVCCount(string userName)
        {
            int vnCount = 0;
            TodayVC sales = new TodayVC();

            try
            {
                //salesVal = SalesBR.Instance.GetTodaySales(userName);
                sales.VNCount = 10;
            }
            catch (Exception ex) { }

            if (sales == null)
            {
                return NotFound();
            }

            return Ok(sales);
        }

        [Route("GetTodayVCCount")]
        [HttpGet]
        public IHttpActionResult GetTodayPCCount(string userName)
        {
            int pcCount = 0;
            TodayPC sales = new TodayPC();

            try
            {
                //salesVal = SalesBR.Instance.GetTodaySales(userName);
                sales.PCCount = 50;
            }
            catch (Exception ex) { }

            if (sales == null)
            {
                return NotFound();
            }

            return Ok(sales);
        }

        [Route("GetAllSRRouteLocations")]
        [AcceptVerbs("GET", "POST")]
        public IHttpActionResult GetAllSRRouteLocations(SRRouteSequenceRequestModel request)
        {
            List<SalesRepRouteSequenceModel> srRouteSeqList = new List<SalesRepRouteSequenceModel>();

            try
            {
                srRouteSeqList = RepsBR.Instance.GetAllSRRouteLocations(request.selectRep, request.selectDate);
            }
            catch (Exception ex) { }

            return Ok(srRouteSeqList);
        }
    }

    public class SRRouteSequenceRequestModel
    {
        public string selectRep { get; set; }
        public string selectDate { get; set; }
    }
}
