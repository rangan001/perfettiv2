﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.API.Models
{
    public class Sales
    {
    }

    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
    }

    public class TodaySales
    {
        public double SalesValue { get; set; }
    }

    public class TodayVC
    {
        public int VNCount { get; set; }
    }

    public class TodayPC
    {
        public int PCCount { get; set; }
    }
}