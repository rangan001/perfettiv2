﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Peercore.CRM.API2.Controllers
{
    public class SyncController : ApiController
    {
        [HttpPost]
        [ActionName("UpdateRepSyncData")]
        public IHttpActionResult UpdateRepSyncData(SyncRequestModel request)
        {
            string server_api_key = ConfigurationManager.AppSettings["FCM_SERVER_API_KEY"];
            string sender_id = ConfigurationManager.AppSettings["FCM_SENDER_ID"];
            string notify_type = "syncdet";

            int requestId = 0;

            try
            {
                ArgsModel args = new ArgsModel();
                args.StartIndex = 0;
                args.RowCount = 10000;
                args.StartIndex = 1;
                args.OrderBy = " rep_name asc";

                requestId = SyncBR.Instance.InserNewSyncMaster(request.originator, request.originatorType);

                if (SyncBR.Instance.UpdateRepIMEISyncStatus(request.originator, request.originatorType, request.DeviceIMEI))
                {
                    List<SalesRepSyncModel> salesVal = SyncBR.Instance.GetAllSalesRepSyncDataByIMEI(args, request.originator, request.originatorType, request.DeviceIMEI);

                    foreach (SalesRepSyncModel device in salesVal)
                    {
                        NotificationResponse response = SendNotification(sender_id,
                            server_api_key,
                            device.DeviceToken,
                            notify_type,
                            notify_type,
                            "",
                            "A",
                            requestId);

                        if (response.Status == true)
                        {
                            SyncBR.Instance.UpdateFCMDeliveryStatus(device.RepCode, device.EmeiNo, "A");
                        }
                        else
                        {
                            SyncBR.Instance.UpdateFCMDeliveryStatus(device.RepCode, device.EmeiNo, "I");
                        }
                    }
                }
            }
            catch (Exception ex) { }

            return Ok();
        }

        [NonAction]
        public NotificationResponse SendNotification(string senderId, string apiKey, string deviceId, string notifyType, string contentTitle, string message, string msgStatus, int requestId)
        {
            NotificationResponse result = new NotificationResponse();

            try
            {
                result.Status = false;
                result.Error = null;

                var value = message;
                HttpWebRequest tRequest = (HttpWebRequest)HttpWebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", apiKey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&priority=high" +
                    "&data.message.notifyType=" + notifyType +
                    "&data.message.contentTitle=" + contentTitle +
                    "&data.message.message=" + message +
                    "&data.message.msgstatus=" + msgStatus +
                    "&data.message.requestId=" + requestId +
                    "&data.time=" + System.DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + "&registration_id=" + deviceId + "";

                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (HttpWebResponse tResponse = (HttpWebResponse)tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();

                                if (tResponse.StatusDescription == "OK")
                                {
                                    result.Status = true;
                                    result.Message = sResponseFromServer;
                                    result.Error = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = null;
                result.Error = ex;
            }

            return result;
        }
    }

    public class NotificationResponse
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public Exception Error { get; set; }
    }

    public class SyncRequestModel
    {
        public string originator { get; set; }
        public string originatorType { get; set; }
        public string DeviceIMEI { get; set; }
    }
}
