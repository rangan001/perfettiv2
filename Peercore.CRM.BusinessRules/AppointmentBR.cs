﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.CRM.DataAccess.datasets;

namespace Peercore.CRM.BusinessRules
{
    public class AppointmentBR
    {
        private static AppointmentBR instance = null;
        private static object syncLock = new Object();

        private AppointmentDAO AppointmentDataService;

        public static AppointmentBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new AppointmentBR();
                }
                return instance;
            }
        }

        private AppointmentBR()
        {
            AppointmentDataService = new AppointmentDAO();
        }

        #region - Init Object -

        public AppointmentEntity CreateObject()
        {
            return AppointmentDataService.CreateObject();
        }

        public AppointmentEntity CreateObject(int entityId)
        {
            return AppointmentDataService.CreateObject(entityId);
        }

        #endregion

        public void GetAppointments(ref DataTable dtAppointment, ArgsEntity args)
        {
            try
            {
                this.AppointmentDataService.GetAppointments(ref dtAppointment, args);
            }
            catch
            {
                throw;
            }
        }

        public bool Update(AppointmentEntity appointment, bool isUpdateDTRangeOnly = false, bool isUpdateDelFlagOnly = false)
        {
            bool success = true;
            try
            {
                success= AppointmentDataService.Update(appointment, isUpdateDTRangeOnly, isUpdateDelFlagOnly);
            }
            catch
            {
                throw;
            }
            return success; 
        }

        public bool Insert(AppointmentEntity appointment, out int appointmentID, bool isUpdateActivity = false, int activityId = 0)
        {
            DbWorkflowScope transactionScope = AppointmentDataService.WorkflowScope;
            bool success = true;

            try
            {
                using (transactionScope)
                {
                    AppointmentDAO appointmentService = new AppointmentDAO(transactionScope);
                    success = appointmentService.Insert(appointment, out appointmentID, isUpdateActivity);

                    if (success && isUpdateActivity && (activityId > 0))
                    {
                        appointment.AppointmentID = appointmentID;
                        success=appointmentService.UpdateActivity(appointment, activityId);
                    }

                    if (success)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return success; 
        }

        public int DeleteResources(AppointmentEntity appointment)
        {
            try
            {
                return AppointmentDataService.DeleteResources(appointment);
            }
            catch
            {
                throw;
            }
        }

        public List<KeyValuePair<string, int>> GetAppointmentsCount(ArgsEntity args)
        {
            try
            {
                return AppointmentDataService.GetAppointmentsCount(args);
            }
            catch
            {
                throw;
            }
        }

        public List<AppointmentViewEntity> GetOncomingAppointments(ArgsEntity args)
        {
            try
            {
                return AppointmentDataService.GetOncomingAppointments(args);
            }
            catch
            {
                throw;
            }
        }

        public List<AppointmentViewEntity> GetTodaysAppointments(ArgsEntity args)
        {
            try
            {
                List<AppointmentViewEntity> appointments = AppointmentDataService.GetTodaysAppointments(args);

                
                return appointments;
            }
            catch
            {
                throw;
            }
        }

        public List<AppointmentViewEntity> GetPendingAppointments(ArgsEntity args)
        {
            try
            {
                List<AppointmentViewEntity> appointments = AppointmentDataService.GetPendingAppointments(args);


                return appointments;
            }
            catch
            {
                throw;
            }
        }

        public List<AppointmentViewEntity> GetAllAppointmentsByType(ArgsEntity args)
        {
            try
            {
                List<AppointmentViewEntity> appointments = AppointmentDataService.GetAllAppointmentsByType(args);

                return appointments;
            }
            catch
            {
                throw;
            }
        }
        public List<AppointmentCountViewEntity> GetAllAppointmentsByTypeCount(ArgsEntity args)
        {
            try
            {
                return AppointmentDataService.GetAllAppointmentsByTypeCount(args);
            }
            catch
            {
                throw;
            }
        }

        public bool InsertAppointmentList(List<CustomerActivityEntity> Listactivity, string originator)
        {
            DbWorkflowScope transactionScope = AppointmentDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    AppointmentDAO appointmentService = new AppointmentDAO(transactionScope);


                    CRMAppointmentEntity appointment = null;
                    int appointmentId = 0;// System.Convert.ToInt32(oDataHandle.GetOneValue(CRMSQLs.GetNextAppointmentID));   // Get Next ID
                    foreach (CustomerActivityEntity customerActivities in Listactivity)
                    {

                        appointment = CRMAppointmentEntity.CreateObject();
                        
                        appointment.Subject = customerActivities.Subject;
                        appointment.Body = customerActivities.Comments;
                        appointment.StartTime = DateTime.Parse(customerActivities.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"));
                        appointment.EndTime = DateTime.Parse(customerActivities.EndDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"));
                        appointment.Category = customerActivities.ActivityType;
                        appointment.CreatedBy = originator;

                        isSuccess = AppointmentDataService.InsertAppointmentlst(appointment,customerActivities.ActivityID, out appointmentId);
                        customerActivities.AppointmentId = appointmentId; 
                        appointment.AppointmentID = appointmentId;
                        if (!isSuccess)
                            break;
                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }


        public AppointmentDataSet GetReportData(ArgsEntity args)
        {
            try
            {
                return AppointmentDataService.GetReportData(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

      /*  public List<AppointmentViewEntity> GetFutureAppointments(ArgsEntity args)
        {
            try
            {
                List<AppointmentViewEntity> appointments = AppointmentDataService.GetFurureAppointments(args);

                if (appointments != null && appointments.Count > 0)
                {
                    int listCount = appointments.Count;
                    //appointments.GetRange((startIndex * rowCount), clsGlobal.GetInstance().Count(startIndex, rowCount, listCount));
                    appointments.ForEach(s => s.RowCount = listCount);
                }

                return appointments;
            }
            catch
            {
                throw;
            }
        }
        */
        
    }
}
