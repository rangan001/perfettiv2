﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class AreaBR 
    {
        private static AreaBR instance = null;
        private static object syncLock = new Object();

        private AreaDAO AreaDataService;

        public static AreaBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new AreaBR();
                }
                return instance;
            }
        }

        private AreaBR()
        {
            AreaDataService = new AreaDAO();
        }

        #region - Init Object -

        public AreaEntity CreateObject()
        {
            return AreaDataService.CreateObject();
        }

        public AreaEntity CreateObject(int entityId)
        {
            return AreaDataService.CreateObject(entityId);
        }

        #endregion

        public List<CityAreaEntity> GetAllAreaCodes()
        {
            try
            {
                return AreaDataService.GetAllAreaCodes();
            }
            catch
            {
                throw;
            }
        }

        public List<CityAreaEntity> GetAllAreas(ArgsEntity args)
        {
            try
            {
                return AreaDataService.GetAllAreas(args);
            }
            catch
            {
                throw;
            }
        }

        public bool IsAreaNameExists(CityAreaEntity areaEntity)
        {
            try
            {
                return AreaDataService.IsAreaNameExists(areaEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveArea(List<CityAreaEntity> AreaEntityList)
        {
            DbWorkflowScope transactionScope = AreaDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (AreaEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (CityAreaEntity AreaEntity in AreaEntityList)
                        {
                            if (AreaEntity.AreaID > 0)
                            {
                                isSuccess = AreaDataService.UpdateArea(AreaEntity);
                            }
                            else
                            {
                                isSuccess = AreaDataService.InsertArea(AreaEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteArea(int areaId)
        {
            try
            {
                return AreaDataService.DeleteArea(areaId);
            }
            catch
            {
                throw;
            }
        }

        public List<AreaModel> GetAllAreaNew(ArgsModel args)
        {
            try
            {
                return AreaDataService.GetAllAreaNew(args);
            }
            catch
            {
                throw;
            }
        }
        
        public List<AreaModel> GetAllAreaByAreaCode(ArgsModel args, string AreaCode)
        {
            try
            {
                return AreaDataService.GetAllAreaByAreaCode(args, AreaCode);
            }
            catch
            {
                throw;
            }
        }
        
        public List<AreaModel> GetAllAreaByAreaName(ArgsModel args, string AreaName)
        {
            try
            {
                return AreaDataService.GetAllAreaByAreaName(args, AreaName);
            }
            catch
            {
                throw;
            }
        }

        public List<AreaModel> GetAllAreaByRegionId(ArgsModel args, string RegionId)
        {
            try
            {
                return AreaDataService.GetAllAreaByRegionId(args, RegionId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteAreaFromRegionByAreaId(int areaId)
        {
            try
            {
                return AreaDataService.DeleteAreaFromRegionByAreaId(areaId);
            }
            catch
            {
                throw;
            }
        }

        public bool InsertUpdateArea(string region_id, string area_id, string area_code, string area_name)
        {
            try
            {
                return AreaDataService.InsertUpdateArea(region_id,
                    area_id,
                    area_code,
                    area_name);
            }
            catch
            {
                throw;
            }
        }
    }
}
