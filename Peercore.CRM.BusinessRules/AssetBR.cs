﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class AssetBR
    {
        private static AssetBR instance = null;
        private static object syncLock = new Object();

        private AssetDAO AssetDataService;

        public static AssetBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new AssetBR();
                }
                return instance;
            }
        }

        private AssetBR()
        {
            AssetDataService = new AssetDAO();
        }

        #region  - Asset Type - 

        public List<AssetTypeModel> GetAllAssetTypes(ArgsModel args)
        {
            try
            {
                return AssetDataService.GetAllAssetTypes(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAssetType(AssetTypeModel assetTypeDetail)
        {
            try
            {
                return AssetDataService.SaveAssetType(assetTypeDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateAssetType(int assetTypeId, string typeName, string description, string ModifiedBy)
        {
            try
            {
                return AssetDataService.UpdateAssetType(assetTypeId, typeName, description, ModifiedBy);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteAssetType(int assetTypeId, string originator)
        {
            try
            {
                return AssetDataService.DeleteAssetType(assetTypeId, originator);
            }
            catch
            {
                throw;
            }
        }

        public bool IsAssetTypeAvailable(string assetType)
        {
            try
            {
                return AssetDataService.IsAssetTypeAvailable(assetType);
            }
            catch
            {
                throw;
            }
        }


        #endregion

        #region - Asset Master - 

        public List<AssetModel> GetAllAssets(ArgsModel args)
        {
            try
            {
                return AssetDataService.GetAllAssets(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAsset(AssetModel assetDetail)
        {
            try
            {
                return AssetDataService.SaveAsset(assetDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateAsset(AssetModel assetDetail)
        {
            try
            {
                return AssetDataService.UpdateAsset(assetDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteAsset(int assetId, string originator)
        {
            try
            {
                return AssetDataService.DeleteAsset(assetId, originator);
            }
            catch
            {
                throw;
            }
        }

        public bool UploadExcelfile(string FilePath, string UserName, string logPath)
        {
            try
            {
                return AssetDataService.UploadExcelfile(FilePath, UserName, logPath);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsAssetCodeAvailable(string assetCode)
        {
            try
            {
                return AssetDataService.IsAssetCodeAvailable(assetCode);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region - Asset Assign -

        public List<AssetAssignModel> GetAllAssignAssets(ArgsModel args, string Token)
        {
            try
            {
                return AssetDataService.GetAllAssignAssets(args, Token);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAssignAsset(AssetAssignModel assignDetail)
        {
            try
            {
                return AssetDataService.SaveAssignAsset(assignDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateAssignAsset(AssetAssignModel assignDetail)
        {
            try
            {
                return AssetDataService.UpdateAssignAsset(assignDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteAssignAsset(int assignId, string originator)
        {
            try
            {
                return AssetDataService.DeleteAssignAsset(assignId, originator);
            }
            catch
            {
                throw;
            }
        }

        public bool UploadAssetAssignDetails(string FilePath, string UserName, string logPath, bool IsCustomer)
        {
            try
            {
                return AssetDataService.UploadAssetAssignDetails(FilePath, UserName, logPath, IsCustomer);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorModel> GetUserCodesByUserType(string userType)
        {
            try
            {
                return AssetDataService.GetUserCodesByUserType(userType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AssetModel> GetAssetsCodesByAssetType(string assetType)
        {
            try
            {
                return AssetDataService.GetAssetsCodesByAssetType(assetType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AssetAssignModel> GetAllAssignAssetsByUser(string userCode)
        {
            try
            {
                return AssetDataService.GetAllAssignAssetsByUser(userCode);
            }
            catch
            {
                throw;
            }
        }
        
        public List<AssetAssignModel> GetAllAssignAssetsByAssetCode(string assetCode)
        {
            try
            {
                return AssetDataService.GetAllAssignAssetsByAssetCode(assetCode);
            }
            catch
            {
                throw;
            }
        }
        
        public List<AssetAssignModel> GetAllAssignAssetsByAssetId(int assetId)
        {
            try
            {
                return AssetDataService.GetAllAssignAssetsByAssetId(assetId);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAssignAssetList(string Originator, List<AssetAssignModel> data)
        {
            try
            {
                return AssetDataService.SaveAssignAssetList(Originator, data);
            }
            catch
            {
                throw;
            }
        }

        public string LoadCustomerName(string custCode)
        {
            return AssetDataService.LoadCustomerName(custCode);
        }

        public bool IsAssetAvailable(string assetCode, string cusCode)
        {
            try
            {
                return AssetDataService.IsAssetAvailable(assetCode, cusCode);
            }
            catch
            {
                throw;
            }
        }

        public bool IsLostOrDamaged(string assetCode, string cusCode)
        {
            try
            {
                return AssetDataService.IsLostOrDamaged(assetCode, cusCode);
            }
            catch
            {
                throw;
            }
        }
        
        public List<AssetStatusModel> GetAllassetStatus()
        {
            try
            {
                return AssetDataService.GetAllAssetStatus();
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
