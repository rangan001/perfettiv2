﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class BankAccountBR
    {
        private static BankAccountBR instance = null;
        private static object syncLock = new Object();

        private BankAccountDAO BankAccountDataService;

        public static BankAccountBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new BankAccountBR();
                }
                return instance;
            }
        }

        private BankAccountBR()
        {
            BankAccountDataService = new BankAccountDAO();
        }

        #region - Init Object -

        public BankAccountEntity CreateObject()
        {
            return BankAccountDataService.CreateObject();
        }

        public BankAccountEntity CreateObject(int entityId)
        {
            return BankAccountDataService.CreateObject(entityId);
        }

        #endregion


        public List<BankAccountEntity> GetBankAccountsForUser(string sourceTypeText, string originator)
        {
            try
            {
                return BankAccountDataService.GetBankAccountsForUser(sourceTypeText, originator);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveBankAccounts(BankAccountEntity account)
        {
            try
            {
                return BankAccountDataService.SaveBankAccounts(account);
            }
            catch
            {
                throw;
            }
        }
    }
}
