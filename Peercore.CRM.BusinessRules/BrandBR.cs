﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class BrandBR
    {
        private static BrandBR instance = null;
        private static object syncLock = new Object();

        private BrandDAO BrandDataService;

        public static BrandBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new BrandBR();
                }
                return instance;
            }
        }

        private BrandBR()
        {
            BrandDataService = new BrandDAO();
        }

        #region - Init Object -

        public BrandEntity CreateObject()
        {
            return BrandDataService.CreateObject();
        }

        public BrandEntity CreateObject(int entityId)
        {
            return BrandDataService.CreateObject(entityId);
        }

        #endregion

        public bool SaveBrands(List<BrandEntity> BrandEntityList)
        {
            DbWorkflowScope transactionScope = BrandDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (BrandEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (BrandEntity brandEntity in BrandEntityList)
                        {
                            if (brandEntity.BrandId > 0)
                            {
                                isSuccess = BrandDataService.UpdateBrand(brandEntity);
                            }
                            else
                            {
                                isSuccess = BrandDataService.InsertBrand(brandEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<BrandEntity> GetAllBrandsDataAndCount(ArgsEntity args)
        {
            try
            {
                return BrandDataService.GetAllBrandsDataAndCount(args);
            }
            catch
            {
                throw;
            }
        }

        public bool IsBrandCodeExists(BrandEntity brandEntity)
        {
            try
            {
                return BrandDataService.IsBrandCodeExists(brandEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool IsBrandNameExists(BrandEntity brandEntity)
        {
            try
            {
                return BrandDataService.IsBrandNameExists(brandEntity);
            }
            catch
            {
                throw;
            }
        }

        public List<BrandEntity> GetAllBrands(string accessToken, string syncDate)
        {
            try
            {
                return BrandDataService.GetAllBrands(accessToken, syncDate);
            }
            catch
            {
                throw;
            }
        }

        public byte[] GetBrandByAccessTokenAndId(string accessToken, int brandId)
        {
            try
            {
                return BrandDataService.GetBrandByAccessTokenAndId(accessToken, brandId);
            }
            catch
            {
                throw;
            }
        }

        public bool update_brand_test(int id, byte[] content)
        {
            return BrandDataService.update_brand_test(id, content);
        }

        public bool DeleteBrand(int brandId)
        {
            try
            {
                return BrandDataService.DeleteBrand(brandId);
            }
            catch
            {
                throw;
            }
        }



        public List<BrandECOModel> GetBrandECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            try
            {
                return BrandDataService.GetBrandECOCumulativeByAccessToken(accessToken, strDate, endDate);
            }
            catch
            {
                throw;
            }
        }






    }
}
