﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class CatalogBR
    {
        private static CatalogBR instance = null;
        private static object syncLock = new Object();

        private CatalogDAO CatalogDataService;

        public static CatalogBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new CatalogBR();
                }
                return instance;
            }
        }

        private CatalogBR()
        {
            CatalogDataService = new CatalogDAO();
        }

        #region - Init Object -

        public CatalogEntity CreateObject()
        {
            return CatalogDataService.CreateObject();
        }

        public CatalogEntity CreateObject(int entityId)
        {
            return CatalogDataService.CreateObject(entityId);
        }

        #endregion

        public List<CatalogEntity> GetProductsLookup(ArgsEntity args)
        {
            try
            {
                return CatalogDataService.GetProductsLookup(args);
            }
            catch
            {
                throw;
            }
        }

        public List<CatalogEntity> GetAllCatalog(ArgsEntity args)
        {
            try
            {
                return CatalogDataService.GetAllCatalog(args);
            }
            catch
            {
                throw;
            }
        }

        public List<CatalogEntity> GetCatalogLookup(ArgsEntity args)
        {
            try
            {
                return CatalogDataService.GetCatalogLookup(args);
            }
            catch
            {
                throw;
            }
        }

        
    }
}
