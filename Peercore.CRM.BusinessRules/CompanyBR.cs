﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class CompanyBR
    {
        private static CompanyBR instance = null;
        private static object syncLock = new Object();

        private CompanyDAO CompanyDataService;

        public static CompanyBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new CompanyBR();
                }

                return instance;
            }
        }

        private CompanyBR()
        {
            CompanyDataService = new CompanyDAO();
        }

        public CompanyEntity CreateObject()
        {
            return CompanyDataService.CreateObject();
        }

        public CompanyEntity CreateObject(int entityId)
        {
            return CompanyDataService.CreateObject(entityId);
        }

        public List<CompanyEntity> GetAllCompany()
        {
            try
            {
                return CompanyDataService.GetAllCompany();
            }
            catch
            {
                throw;
            }
        }
    }
}
