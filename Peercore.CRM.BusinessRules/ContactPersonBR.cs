﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ContactPersonBR
    {
        private static ContactPersonBR instance = null;
        private static object syncLock = new Object();
        private ContactPersonDAO contactPersonDataService;

        public static ContactPersonBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ContactPersonBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public ContactPersonEntity CreateObject()
        {
            return contactPersonDataService.CreateObject();
        }

        public ContactPersonEntity CreateObject(int entityId)
        {
            return contactPersonDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private ContactPersonBR()
        {
            contactPersonDataService = new ContactPersonDAO();
        } 
        #endregion

        #region - Public Methods - 
        
        public bool Save(ContactPersonEntity contactPersonEntity,out int contactPersonId)
        {
            DbWorkflowScope transactionScope = contactPersonDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    if (contactPersonEntity.ContactPersonID == 0)
                    {
                        contactPersonId = contactPersonEntity.ContactPersonID = contactPersonDataService.GetNextContactId();
                        isSuccess = contactPersonDataService.InsertLeadContact(contactPersonEntity);
                    }
                    else
                    {
                        contactPersonId = contactPersonEntity.ContactPersonID;
                        isSuccess = contactPersonDataService.UpdateLeadContact(contactPersonEntity);
                    }

                    if (isSuccess) 
                    {
                        if (contactPersonEntity.KeyContact == "Y")
                        {
                            isSuccess = contactPersonDataService.UpdateKeyContact(contactPersonEntity.LeadID, contactPersonEntity.ContactPersonID, "N");
                        }
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool SaveCustomerContact(ContactPersonEntity contactPersonEntity, out int contactPersonId)
        {
            DbWorkflowScope transactionScope = contactPersonDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    contactPersonId = 0;
                    if (contactPersonDataService.isCustomerContactExist(contactPersonEntity) == true)
                    {
                        isSuccess = contactPersonDataService.UpdateCustomerContact(contactPersonEntity);
                    }
                    else
                    {
                        contactPersonId = contactPersonEntity.ContactPersonID = contactPersonDataService.GetNextCustomerContactId();
                        isSuccess = contactPersonDataService.InsertCustomerContact(contactPersonEntity);
                    }
                    
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public ContactPersonEntity GetContactPerson(int contactPersonID)
        {           
            try
            {
               return contactPersonDataService.GetContactPerson(contactPersonID);
            }
            catch
            {
                throw;
            }
            finally
            {
                
            }
        }
        public ContactPersonEntity GetCustomerContactPerson(int contactPersonID)
        {
            try
            {
                return contactPersonDataService.GetCustomerContactPerson(contactPersonID);
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }

        public ContactPersonEntity GetContactPerson(string firstName, string emailAddress, string telephone, string mobile)
        {           
            try
            {
                return contactPersonDataService.GetContactPerson(firstName,emailAddress,telephone,mobile);               
            }
            catch
            {
                throw;
            }
            finally
            {
              
            }
        }

        public List<ContactPersonEntity> GetCustomerContacts(ArgsEntity args, string custCode, string clientType, bool includeChildReps = false)
        {           
            try
            {
                List<OriginatorEntity> childoriginatorlist = OriginatorBR.Instance.GetChildOriginatorsList(args);
                return contactPersonDataService.GetCustomerContacts(args, custCode, clientType,childoriginatorlist, includeChildReps);          
            }
            catch
            {
                throw;
            }
            finally
            {
               
            }
        }

        public ContactPersonEntity GetCustomerContact(string custCode, string contactType, string origin)
        {           
            try
            {
                return contactPersonDataService.GetCustomerContact(custCode, contactType, origin);          
            }
            catch
            {
                throw;
            }
            finally
            {
              
            }
        }
              
        public bool DeleteCustomerContact(int contactId)
        {           
            try
            {
                return contactPersonDataService.DeleteCustomerContact(contactId);     
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<ContactPersonEntity> GetContactPerson(string emailAddress)
        {           
            try
            {
                return contactPersonDataService.GetContactPerson(emailAddress);     
            }
            catch
            {
                throw;
            }
            finally
            {
               
            }
        }

        public List<ContactPersonEntity> GetCustomerContact(string emailAddress)
        {           
            try
            {
                return contactPersonDataService.GetCustomerContact(emailAddress);     
            }
            catch
            {
                throw;
            }
            finally
            {
               
            }
        }

        public List<ContactPersonEntity> GetContactsByOrigin(ArgsEntity args, bool bIncludeChildReps = false)
        {
            try
            {
                return contactPersonDataService.GetContactsByOrigin(args, bIncludeChildReps);
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion
    }
}
