﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class CountryBR
    {
        private static CountryBR instance = null;
        private static object syncLock = new Object();
        private CountryDAO countryDataService;

        public static CountryBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new CountryBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public CountryEntity CreateObject()
        {
            return countryDataService.CreateObject();
        }

        public CountryEntity CreateObject(int entityId)
        {
            return countryDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private CountryBR()
        {
            countryDataService = new CountryDAO();
        }
        #endregion

        #region - Public Methods -

        public List<CountryEntity> GetCountry()
        {
            try
            {
                return countryDataService.GetCountry();
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }

        #endregion
    }
}
