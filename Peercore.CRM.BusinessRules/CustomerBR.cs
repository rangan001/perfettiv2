﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.CRM.DataAccess.datasets;
using System.Data;
using Peercore.CRM.Model;

namespace Peercore.CRM.BusinessRules
{
    public class CustomerBR
    {
        private static CustomerBR instance = null;
        private static object syncLock = new Object();
        private CustomerDAO customerDataService;

        public static CustomerBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new CustomerBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public CustomerEntity CreateObject()
        {
            return customerDataService.CreateObject();
        }

        public CustomerEntity CreateObject(int entityId)
        {
            return customerDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private CustomerBR()
        {
            customerDataService = new CustomerDAO();
        }
        #endregion

        #region - Public Methods -

        public bool SaveCustomerAddress(LeadAddressEntity customerAddress)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    if (customerAddress.AssigneeNo == 0)
                    {
                        customerAddress.AssigneeNo = customerDataService.GetNextCustomerAssigneeNo(customerAddress);
                        isSuccess = customerDataService.InsertCustomerAddress(customerAddress);
                    }
                    else
                    {
                        isSuccess = customerDataService.UpdateCustomerAddress(customerAddress);
                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public CustomerEntity GetCustomer(string customerCode)
        {
            try
            {
                return customerDataService.GetCustomer(customerCode);
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }

        public CustomerEntity GetCustomerPending(string customerCode)
        {
            try
            {
                return customerDataService.GetCustomerPending(customerCode);
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }

        public List<LeadAddressEntity> GetCustomerAddresses(string custCode, ArgsEntity args)
        {
            try
            {
                return customerDataService.GetCustomerAddresses(custCode, args);
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }

        public List<RepCustomerViewEntity> GetNewCustomerCount(ArgsEntity args)
        {
            try
            {
                return customerDataService.GetNewCustomerCount(args);
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        public List<LeadCustomerViewEntity> GetPendingCustomerCollection(ArgsEntity args)
        {
            List<LeadCustomerViewEntity> combinedList = customerDataService.GetPendingCustomerCollection(args);
            return new List<LeadCustomerViewEntity>(combinedList);

        }

        public LeadCustomerViewEntity GetCustomerView(string customerCode, string defaultDeptID)
        {
            try
            {
                return customerDataService.GetCustomerView(customerCode, defaultDeptID);
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        public LeadAddressEntity GetCustomerTableAddresses(string custCode)
        {
            try
            {
                return customerDataService.GetCustomerTableAddresses(custCode);
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        public List<SalesOrderEntity> GetOutstandingDeliveries(string custCode)
        {
            try
            {
                return customerDataService.GetOutstandingDeliveries(custCode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public int GetOutstandingDeliveriesCount(string custCode)
        {
            try
            {
                return customerDataService.GetOutstandingDeliveriesCount(custCode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }
        public DateTime GetLastDelivery(string custCode)
        {
            try
            {
                return customerDataService.GetLastDelivery(custCode);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool InserColumnSetting(ColumnSettingsEntity objColumnSettings)
        {
            return customerDataService.InserColumnSetting(objColumnSettings);
        }

        public ColumnSettingsEntity GetColumnSetting(string originator, string gridName)
        {
            return customerDataService.GetColumnSetting(originator, gridName);
        }

        public ColumnSettingsEntity GetSortItems(string originator, string gridName)
        {
            return customerDataService.GetSortItems(originator, gridName);
        }

        #endregion

        #region - CTC Public Methods -

        public bool SaveCustomer(CustomerEntity customerEntity)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;

            bool isSuccess = false;
            bool isSuccesscontact = false;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);
                    ContactPersonDAO contactPersonDataService = new ContactPersonDAO();
                    ContactPersonEntity contactPersonEntity = new ContactPersonEntity();
                    contactPersonEntity = CreateEntityContactPerson(customerEntity);
                    ArgsEntity args = new ArgsEntity();
                    args.CustomerCode = customerEntity.CustomerCode;

                    if (customerService.isCustomerExist(customerEntity) == true)
                    {
                        isSuccess = customerService.UpdateCustomer(customerEntity);

                        DocumentBR.Instance.Save(ConvertToCustomerDocumentEntity(customerEntity.CustomerCode, customerEntity.ImageContent));
                    }
                    else
                    {
                        //if (customerService.isCustomerExistInNewEntryAndPending(customerEntity) && customerEntity.CustomerTypeOfApprove == "pendingcustomer")
                        //{
                        //    isSuccess = customerService.UpdateCustomerNewEntry(customerEntity);
                        //    if (isSuccess && !customerService.isCustomerExist(customerEntity))//Insert to Customer Table (Since it is Approved)
                        //        isSuccess = customerService.InsertCustomerMaster(customerEntity);
                        //}
                        //else
                        //{
                        //Insert to CustomerNewEntry
                        //isSuccess = customerService.InsertCustomer(customerEntity);
                        //}

                        int cust_code = customerService.InsertCustomer(customerEntity);

                        DocumentBR.Instance.Save(ConvertToCustomerDocumentEntity(cust_code.ToString(), customerEntity.ImageContent));

                        if (cust_code > 0)
                            isSuccess = true;
                    }

                    if (contactPersonDataService.isCustomerContactExist(contactPersonEntity) == true)
                    {
                        isSuccesscontact = contactPersonDataService.UpdateCustomerContact(contactPersonEntity);
                    }
                    else
                    {
                        isSuccesscontact = contactPersonDataService.InsertCustomerContact(contactPersonEntity);
                    }

                    if (isSuccess && isSuccesscontact)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            if (!isSuccesscontact)
                return isSuccesscontact;
            else
                return isSuccess;
        }

        public DocumentEntity ConvertToCustomerDocumentEntity(string custCode, byte[] image)
        {
            DocumentEntity documentEntity = new DocumentEntity();


            try
            {
                documentEntity.DocumentID = 0;
                documentEntity.LeadID = 0;
                documentEntity.DocumentName = custCode;
                documentEntity.Path = custCode + ".jpg";
                documentEntity.CustCode = custCode;
                documentEntity.AttachedDate = DateTime.Now;
                documentEntity.AttachedBy = "";
                documentEntity.LastModifiedBy = "";
                documentEntity.LastModifiedDate = DateTime.Now;
                documentEntity.EnduserCode = "";
                documentEntity.Content = image;


                return documentEntity;

            }
            catch (Exception)
            {

                throw;
            }
            return documentEntity;
        }

        public bool DeleteCustomer(string custCode)
        {
            return customerDataService.DeleteCustomer(custCode);
        }

        public bool DeleteCustomerFromRoute(string custCode, int routeAssignId)
        {
            return customerDataService.DeleteCustomerFromRoute(custCode, routeAssignId);
        }

        public bool DeleteAddCustomerRoute(string custCode)
        {
            return customerDataService.DeleteAddCustomerRoute(custCode);
        }

        public List<TraderCustomerEntity> GetCustomersForTME(string tmeid)
        {
            return customerDataService.GetCustomersForTME(tmeid);
        }

        public List<TraderCustomerEntity> GetCustomersForInvoiceByRoute(int routeId)
        {
            return customerDataService.GetCustomersForInvoiceByRoute(routeId);
        }

        public TraderCustomerEntity GetCustomerForMobileDetails(string custCode)
        {
            List<TraderCustomerEntity> customerList = new List<TraderCustomerEntity>();
            TraderCustomerEntity customer = new TraderCustomerEntity();
            customer = customerDataService.GetCustomerForMobileDetails(custCode);
            customer.Promotions = customerDataService.GetCustomerPromotions(custCode);

            return customer;
        }

        public List<PromotionEntity> GetPromotionsForCustomer(string custcode)
        {
            return customerDataService.GetCustomerPromotions(custcode);
        }

        public List<CustomerEntity> GetDailyEffectiveReportData(string fromDate, string todate)
        {
            return customerDataService.GetDailyEffectiveReportData(fromDate, todate);
        }

        public DailyEffectiveDataSet GetDailyEffectiveReportData(ArgsEntity args)
        {
            return customerDataService.GetDailyEffectiveReportData(args);
        }


        public bool SaveDERData(DailyEffectivenessEntity DEREntity)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);
                    isSuccess = customerService.SaveDERData(DEREntity);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool SaveVolumePerformanceData(List<VolumePerformanceEntity> volumeperformanceList)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);

                    foreach (VolumePerformanceEntity vpEntity in volumeperformanceList)
                    {
                        isSuccess = customerService.InsertVolumePerformanceData(vpEntity);

                        if (!isSuccess)
                            break;
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<ProductEntity> GetCustomersProductStatusList(string custCode)
        {
            return customerDataService.GetCustomerProductStatus(custCode);
        }

        public bool SaveRCSCustomerDetails(TraderCustomerEntity tCustEntity)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);
                    isSuccess = customerService.SaveRCSCustomerDetails(tCustEntity);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool SaveRCSCoantactDetails(ContactPersonEntity contactPersonEntity)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);
                    isSuccess = customerService.SaveRCSCoantactDetails(contactPersonEntity);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool SaveRCSMerchandiseDetails(List<MerchandiseEntity> merchandiseList)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);
                    isSuccess = customerService.SaveRCSMerchandiseDetails(merchandiseList);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool SaveRCSOutletDetails(List<OutletEntity> outletList)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);
                    isSuccess = customerService.SaveRCSOutletDetails(outletList);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool SaveRCSVolumeAssesmentDetails(List<VolumeAssesmentEntity> volumeAssesmentList)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);
                    isSuccess = customerService.SaveRCSVolumeAssesmentDetails(volumeAssesmentList);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }


        public bool SaveRCSCollectionData(TraderCustomerEntity tCustEntity, ContactPersonEntity contactPersonEntity, List<MerchandiseEntity> merchandiseList, List<OutletEntity> outletList, PurchaseBehaviourEntity behaviourEntity, List<VolumeAssesmentEntity> volumeAssesmentList)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    CustomerDAO customerService = new CustomerDAO(transactionScope);
                    isSuccess = customerService.SaveRCSCollectionData(tCustEntity, contactPersonEntity, merchandiseList, outletList, behaviourEntity, volumeAssesmentList);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }


        public List<InvoiceHeaderEntity> GetCustomerUnsettledInvoices(string custcode)
        {
            return customerDataService.GetCustomerUnsettledInvoices(custcode);
        }

        public TraderCustomerEntity GetCustomerCreditDetails(string customerCode)
        {
            try
            {
                return customerDataService.GetCustomerCreditDetails(customerCode);
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }


        public List<TraderCustomerEntity> GetCustomersByRoute(int routeId)
        {
            return customerDataService.GetCustomersByRoute(routeId);
        }


        public List<SalesForceCustomerEntity> GetAllCoveredOutletsForRoute(int routeId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return customerDataService.GetAllCoveredOutletsForRoute(routeId, fromDate, toDate);
            }
            catch
            {
                throw;
            }
        }

        public List<SalesForceCustomerEntity> GetAllOutletsForProgram(int programId)
        {
            try
            {
                return customerDataService.GetAllOutletsForProgram(programId);
            }
            catch
            {
                throw;
            }
        }

        public List<TargetInfoEntity> GetCustomerMonthleyTargetsByDistributor(string username)
        {
            try
            {
                return customerDataService.GetCustomerMonthleyTargetsByDistributor(username);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerEntity> GetCustomerTargetsByRouteId(int routeId, ArgsEntity args)
        {
            try
            {
                return customerDataService.GetCustomerTargetsByRouteId(routeId, args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveCustomerTargets(List<CustomerEntity> customerTargetList)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (customerTargetList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (CustomerEntity customerEntity in customerTargetList)
                        {
                            if (customerEntity.TargetId > 0)
                            {
                                isSuccess = customerDataService.UpdateCustomerTarget(customerEntity);
                            }
                            else
                            {
                                if (customerEntity.StickTarget > 0)
                                    isSuccess = customerDataService.InsertCustomerTarget(customerEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool InsertCustomerCategory(CustomerCategory custCategory)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {

                    if (custCategory.CategoryId > 0)
                    {
                        isSuccess = customerDataService.UpdateCustomerCategory(custCategory);
                    }
                    else
                    {
                        isSuccess = customerDataService.InsertCustomerCategory(custCategory);
                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteCustomerCategory(CustomerCategory custCategory)
        {
            DbWorkflowScope transactionScope = customerDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {

                    if (custCategory.CategoryId > 0)
                    {
                        isSuccess = customerDataService.DeleteCustomerCategory(custCategory);
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool UpdateCustomerRemarks(CustomerEntity customerEntity)
        {
            bool isSuccess = false;
            try
            {
                isSuccess = customerDataService.UpdateCustomerRemarks(customerEntity);
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<KeyValuePair<int, string>> GetAllOutletTypes()
        {
            try
            {
                return customerDataService.GetAllOutletTypes();
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }

        public List<KeyValuePair<string, string>> GetAllTowns()
        {
            try
            {
                return customerDataService.GetAllTowns();
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion

        //public bool UploadExcelfile(string FilePath, string Extension, string UserName, string ASEOriginator, string DistOriginator, string RepOriginator)
        //{
        //    try
        //    {
        //        return customerDataService.UploadExcelfile(FilePath, Extension, UserName, ASEOriginator, DistOriginator,RepOriginator);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //    }
        //}

        public bool UploadExcelfile(string FilePath, string Extension, string UserName, string TerritoryId)
        {
            try
            {
                return customerDataService.UploadExcelfile(FilePath, Extension, UserName, TerritoryId);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        //public DataTable ReviewExcelfile(string FilePath, string Extension, string UserName)
        //{
        //    try
        //    {
        //        return customerDataService.ReviewExcelfile(FilePath, Extension, UserName);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //    }
        //}

        public ContactPersonEntity CreateEntityContactPerson(CustomerEntity tradercust)
        {
            ContactPersonEntity Customer = new ContactPersonEntity();

            try
            {
                Customer.CustCode = tradercust.CustomerCode;
                Customer.FirstName = tradercust.ContactPersonName;
            }
            catch (Exception)
            {

                throw;
            }
            return Customer;
        }

        public List<CustomerEntity> GetRouteCustomerByAccessToken(string accessToken)
        {
            return customerDataService.GetRouteCustomerByAccessToken(accessToken);
        }

        public string InsertCustomerByMob(string accessToken, CustomerEntity customerEntity)
        {
            return customerDataService.InsertCustomerByMob(accessToken, customerEntity);
        }

        public string GetCustomerCodeByTempCustCode(string tempCustCode)
        {
            return customerDataService.GetCustomerCodeByTempCustCode(tempCustCode);
        }

        public bool UpdateCustomerByMob(string accessToken, CustomerEntity customerEntity)
        {
            return customerDataService.UpdateCustomerByMob(accessToken, customerEntity);
        }

        public bool DeleteCustomerFromSystem(string custCode)
        {
            return customerDataService.DeleteCustomerFromSystem(custCode);
        }



        #region "Perfetti 2nd Phase"

        public List<CustomerModel> GetAllPendingCustomers(ArgsModel args, string IsReject, string Originator, string Asm, string Territory)
        {
            try
            {
                return customerDataService.GetAllPendingCustomers(args, IsReject, Originator, Asm, Territory);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerModel> GetAllPendingCustomersForSalesConfirm(ArgsModel args, string IsReject, string Originator, string Asm, string Territory)
        {
            try
            {
                return customerDataService.GetAllPendingCustomersForSalesConfirm(args, IsReject, Originator, Asm, Territory);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerModel> GetAllAllCustomerByRouteMasterId(ArgsModel args, string RouteMasterId)
        {
            try
            {
                return customerDataService.GetAllAllCustomerByRouteMasterId(args, RouteMasterId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteBulkCust(string userType, string userName, string custList)
        {
            try
            {
                return customerDataService.DeleteBulkCust(userType, userName, custList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AcceptCustomer(string Originator, string CustCode)
        {
            try
            {
                return customerDataService.AcceptRejectCustomer(Originator, CustCode, false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AcceptSalesCustomer(string Originator, string CustCode)
        {
            try
            {
                return customerDataService.AcceptSalesRejectCustomer(Originator, CustCode, false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RejectCustomer(string Originator, string CustCode)
        {
            try
            {
                return customerDataService.AcceptRejectCustomer(Originator, CustCode, true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateCustomerDayOrderByRep(string originator, int routeId, List<CustomerSequence> customerCollection)
        {
            try
            {
                foreach (CustomerSequence sequence in customerCollection)
                {
                    customerDataService.UpdateCustomerDayOrderByRep(originator, routeId, sequence.CustomerCode, sequence.DayOrder);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion


        public List<CustomerECOModel> GetCustomerECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            try
            {
                return customerDataService.GetCustomerECOCumulativeByAccessToken(accessToken, strDate, endDate);
            }
            catch
            {
                throw;
            }
        }

    }


}
