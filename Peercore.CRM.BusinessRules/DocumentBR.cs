﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class DocumentBR
    {
        private static DocumentBR instance = null;
        private static object syncLock = new Object();
        private DocumentDAO documentDataService;

        public static DocumentBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new DocumentBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public DocumentEntity CreateObject()
        {
            return documentDataService.CreateObject();
        }

        public DocumentEntity CreateObject(int entityId)
        {
            return documentDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private DocumentBR()
        {
            documentDataService = new DocumentDAO();
        }
        #endregion

        #region - Public Methods -

        public bool Save(DocumentEntity documentEntity)
        {
            DbWorkflowScope transactionScope = documentDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    DocumentEntity document = new DocumentEntity();
                    document = documentDataService.GetDocument(documentEntity.DocumentID);

                    if (document != null && document.DocumentID != 0)
                    {
                        if (document.LastModifiedDate > documentEntity.LastModifiedDate)
                        {
                            isSuccess = false;
                            throw new InvalidOperationException("Data displayed is not up to date. Please refresh data and re-save!");
                        }
                        else 
                        {
                            isSuccess = documentDataService.UpdateDocument(documentEntity);
                        }
                    }
                    else 
                    {
                        documentEntity.DocumentID = documentDataService.GetNextDocumentID();
                        isSuccess = documentDataService.InsertDocument(documentEntity);
                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<DocumentEntity> GetContactDocumentsForCustomer(ArgsEntity args)
        {
            try
            {
                return documentDataService.GetContactDocumentsForCustomer(args);
            }
            catch
            {                
                throw;
            }
        }

        public List<DocumentEntity> GetDocumentDetails(ArgsEntity args)
        {
            try
            {
                return documentDataService.GetDocumentDetails(args);
            }
            catch (Exception )
            {
                throw;
            }
        }

        public DocumentEntity GetDocument(int documentID)
        {
            try
            {
                return documentDataService.GetDocument(documentID);
            }
            catch (Exception ) 
            {
                throw;
            }
        }

        public bool DeleteDocument(int documentID)
        {
            try
            {
                return documentDataService.DeleteDocument(documentID);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public List<DocumentEntity> GetContactDocumentsForEndUser(ArgsEntity args)
        {
            try
            {
                return documentDataService.GetContactDocumentsForEndUser(args);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public bool GetAdminPassword(string password)
        {
            return documentDataService.GetAdminPassword(password);
        }
    }
}
