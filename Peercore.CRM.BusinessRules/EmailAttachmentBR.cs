﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class EmailAttachmentBR
    {
        private static EmailAttachmentBR instance = null;
        private static object syncLock = new Object();

        private EmailAttachmentDAO EmailAttachmentDataService;

        public static EmailAttachmentBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new EmailAttachmentBR();
                }
                return instance;
            }
        }

        private EmailAttachmentBR()
        {
            EmailAttachmentDataService = new EmailAttachmentDAO();
        }

        #region - Init Object -

        public EmailAttachmentEntity CreateObject()
        {
            return EmailAttachmentDataService.CreateObject();
        }

        public EmailAttachmentEntity CreateObject(int entityId)
        {
            return EmailAttachmentDataService.CreateObject(entityId);
        }

        #endregion

        public List<EmailAttachmentEntity> GetEmailAttachment(int emailId)
        {
            try
            {
                return EmailAttachmentDataService.GetEmailAttachment(emailId);
            }
            catch
            {
                throw;
            }
        }
    }
}
