﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class EndUserSalesInfoBR
    {
        private static EndUserSalesInfoBR instance = null;
        private static object syncLock = new Object();

        private EndUserSalesInfoDetailsDAO EndUserSalesInfoDetails;

        public static EndUserSalesInfoBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new EndUserSalesInfoBR();
                }
                return instance;
            }
        }

        private EndUserSalesInfoBR()
        {
            EndUserSalesInfoDetails = new EndUserSalesInfoDetailsDAO();
        }

        #region - Init Object -

        public SalesInfoDetailSearchCriteriaEntity CreateObject()
        {
            return EndUserSalesInfoDetails.CreateObject();
        }

        public SalesInfoDetailViewStateEntity GetEndUserSalesInfoDetails(SalesInfoDetailSearchCriteriaEntity searchCriteria, ArgsEntity args, string type = "grid")
        {
            try
            {
                SalesInfoDetailViewStateEntity oSalesInfoDetailViewState = new SalesInfoDetailViewStateEntity();

                int iCostYear;

                string sEnquiryTitle = "";

                //string sPayMethod = OriginatorBR.Instance.GetPayMethod(searchCriteria.RepCode);
                //string sUsername = OriginatorBR.Instance.GetUserName(searchCriteria.RepCode);


                //if (sUsername.Equals(args.Originator))
                //{
                //    if (sPayMethod.Equals("D"))
                //    {
                //        searchCriteria.cDisplayOption = 'D';
                //    }
                //    else if (sPayMethod.Equals("T"))
                //    {
                //        searchCriteria.cDisplayOption = 'T';
                //    }
                //}

                //if (searchCriteria.cDisplayOption.Equals('D'))
                //    oSalesInfoDetailViewState.DisplayOptionIndex = 0; // Sales in Dollars
                //else if (searchCriteria.cDisplayOption.Equals('T'))
                //    oSalesInfoDetailViewState.DisplayOptionIndex = 1; // Sales in Tonnes
                //else
                //    oSalesInfoDetailViewState.DisplayOptionIndex = 2; // Sales in Units

                //// 19-09-2011 | Set default to Tonnes after the 2nd test note 
                //oSalesInfoDetailViewState.DisplayOptionIndex = 1;
                //oSalesInfoDetailViewState.SortFieldOptionIndex = 3;

                //if (searchCriteria.sLastFlag.Equals("n"))
                //{
                //    // year_lbl = 'This Year';
                //    searchCriteria.sEndUserTable = "enduser_summary";
                //    iCostYear = SalesBR.Instance.GetCurrentCostPeriod().Year;
                //}
                //else
                //{
                //    // year_lbl = 'Last Year';
                //    searchCriteria.sEndUserTable = "enduser_summary";
                //    iCostYear = SalesBR.Instance.GetCurrentCostPeriod().Year - 1;
                //}


                //oSalesInfoDetailViewState.sBrand = searchCriteria.Brand;
                //oSalesInfoDetailViewState.sBusArea = searchCriteria.BusinessArea;
                //oSalesInfoDetailViewState.sCatalogueGroup = searchCriteria.CategoryGroup;
                //oSalesInfoDetailViewState.sCatalogueSubGroup = searchCriteria.CategorySubGroup;
                //oSalesInfoDetailViewState.sCustomer = searchCriteria.CustomerDescription;
                //oSalesInfoDetailViewState.sCustomerGroup = searchCriteria.CustomerGroup;
                //oSalesInfoDetailViewState.sMarket = searchCriteria.Market;
                //oSalesInfoDetailViewState.sMonth = searchCriteria.Month;
                //oSalesInfoDetailViewState.sProduct = searchCriteria.CatalogDescripion;
                //oSalesInfoDetailViewState.sProductType = SalesBR.Instance.GetDescription(searchCriteria.CatalogType, "CT");
                //oSalesInfoDetailViewState.sRep = searchCriteria.RepDescription;
                ////oSalesInfoDetailViewState.sState = StateBR.Instance.GetStateByCode( searchCriteria.State).StateAbbri;
                //oSalesInfoDetailViewState.sSubParent = searchCriteria.SubParentCustomerDescription;

                //switch (searchCriteria.DetailType)
                //{
                //    case "bus area":
                //        sEnquiryTitle = "Business Area";
                //        oSalesInfoDetailViewState.sSubDetailType = "market";
                //        break;
                //    case "market":
                //        sEnquiryTitle = "Market";
                //        oSalesInfoDetailViewState.sSubDetailType = "brand";
                //        break;
                //    case "brand":
                //        sEnquiryTitle = "Brand";
                //        oSalesInfoDetailViewState.sSubDetailType = "product";
                //        break;
                //    case "cat_group":
                //        sEnquiryTitle = "Catalogue Group";
                //        oSalesInfoDetailViewState.sSubDetailType = "cat_sub_group";
                //        break;
                //    case "cat_sub_group":
                //        sEnquiryTitle = "Catalogue Sub Group";
                //        oSalesInfoDetailViewState.sSubDetailType = "product";
                //        break;
                //    case "product":
                //        sEnquiryTitle = "Product";
                //        oSalesInfoDetailViewState.sSubDetailType = "customer";
                //        break;
                //    case "customer":
                //        //sEnquiryTitle = "Customer";
                //        sEnquiryTitle = "Trader";
                //        oSalesInfoDetailViewState.sSubDetailType = "product";
                //        break;
                //    case "custgroup":
                //        sEnquiryTitle = "Customer Group";
                //        oSalesInfoDetailViewState.sSubDetailType = "customer";
                //        break;
                //    case "rep":
                //        sEnquiryTitle = "Sales Rep.";
                //        oSalesInfoDetailViewState.sSubDetailType = "customer";
                //        break;
                //    case "repgroup":
                //        sEnquiryTitle = "Sales Reps.";
                //        oSalesInfoDetailViewState.sSubDetailType = "rep";
                //        break;
                //    case "state":
                //        sEnquiryTitle = "State";
                //        oSalesInfoDetailViewState.sSubDetailType = "rep";
                //        break;
                //    default:
                //        break;
                //}

                ////List<string> lstAllColumnHeaders = new List<string>();
                ////List<string> lstColumnHeaders = new List<string>();

                ////SalesBR.Instance.LoadCostPeriods(ref lstAllColumnHeaders, iCostYear);

                ////int i = lstAllColumnHeaders.IndexOf(searchCriteria.Month) - 5;

                ////if (i < 0) i += 12;

                ////for (int x = 0; x < 6; x++)
                ////{
                ////    lstColumnHeaders.Add(lstAllColumnHeaders[i]);
                ////    lstColumnHeaders.Add(lstAllColumnHeaders[i]);
                ////    lstColumnHeaders.Add(lstAllColumnHeaders[i]);

                ////    i++;
                ////    if (i > 11) i = 0;
                ////}

                ////lstColumnHeaders.Add("Total");
                ////lstColumnHeaders.Add("Total");
                ////lstColumnHeaders.Add("Total");
                ////lstColumnHeaders.Add(sEnquiryTitle);
                ////lstColumnHeaders.Add("This YTD");
                ////lstColumnHeaders.Add("This YTD");
                ////lstColumnHeaders.Add("This YTD");
                ////lstColumnHeaders.Add("Last YTD");
                ////lstColumnHeaders.Add("Last YTD");
                ////lstColumnHeaders.Add("Last YTD");
                ////lstColumnHeaders.Add("%");
                ////lstColumnHeaders.Add("%");
                ////lstColumnHeaders.Add("%");
                ////lstColumnHeaders.Add((iCostYear - 1).ToString());
                ////lstColumnHeaders.Add((iCostYear - 1).ToString());
                ////lstColumnHeaders.Add((iCostYear - 1).ToString());
                ////lstColumnHeaders.Add((iCostYear - 2).ToString());
                ////lstColumnHeaders.Add((iCostYear - 2).ToString());
                ////lstColumnHeaders.Add((iCostYear - 2).ToString());


                //if (type == "grid")
                //{
                //    List<SalesInfoEntity> SalesInfoList = EndUserSalesInfoDetails.GetEndUserSalesInfoDetails(searchCriteria, args);
                //    oSalesInfoDetailViewState.LstSalesInfoDetailGrid = SalesInfoList;
                //    oSalesInfoDetailViewState.LstCustomizedTableGrid = SalesInfoBR.Instance.GetCustomizedTableForGrid(SalesInfoList, sEnquiryTitle, searchCriteria.DetailType, iCostYear, searchCriteria.SortField);
                //}
                //else if (type == "chart")
                //{
                //    oSalesInfoDetailViewState.lstPieChartValues = EndUserSalesInfoDetails.GetEndUserSalesInfoPieChart(searchCriteria, args);
                //}
                
                ////oSalesInfoDetailViewState.lstSalesInfoDetailGridColumnHeaders=lstColumnHeaders;
                ////oSalesInfoDetailViewState.LstSalesInfoDetailGrid = EndUserSalesInfoDetails.GetEndUserSalesInfoDetails(searchCriteria);;

                return oSalesInfoDetailViewState;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }



        #endregion
    }
}
