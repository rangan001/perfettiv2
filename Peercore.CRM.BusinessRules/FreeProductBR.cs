﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class FreeProductBR
    {
        private static FreeProductBR instance = null;
        private static object syncLock = new Object();

        private FreeProductDAO FreeProductDataService;

        public static FreeProductBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new FreeProductBR();
                }
                return instance;
            }
        }

        private FreeProductBR()
        {
            FreeProductDataService = new FreeProductDAO();
        }

        #region - Init Object -

        public FreeProductEntity CreateObject()
        {
            return FreeProductDataService.CreateObject();
        }

        public FreeProductEntity CreateObject(int entityId)
        {
            return FreeProductDataService.CreateObject(entityId);
        }

        #endregion

        public List<FreeProductEntity> GetAllFreeProducts(ArgsEntity args)
        {
            try
            {
                return FreeProductDataService.GetAllFreeProducts(args);
            }
            catch
            {
                throw;
            }
        }

        public List<FreeProductEntity> GetFreeProductsByDivisionId(int divisionId)
        {
            try
            {
                return FreeProductDataService.GetFreeProductsByDivisionId(divisionId);
            }
            catch
            {
                throw;
            }
        }
    }
}
