﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class HolidayBR
    {
         private static HolidayBR instance = null;
        private static object syncLock = new Object();
        private HolidayDAO HolidayDataService;

        public static HolidayBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new HolidayBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        //public HolidayEntity CreateObject()
        //{
        //    return HolidayDataService.CreateObject();
        //}

        #endregion

        #region - Constructor -

        private HolidayBR()
        {
            HolidayDataService = new HolidayDAO();
        }
        #endregion

        public bool InsertHoliday(HolidayEntity holidayentity)
        {
            try
            {
                return HolidayDataService.InsertHoliday(holidayentity);
            }
            catch
            {
                throw;
            }
        }

        public int InsertHolidayReturnId(HolidayEntity holidayentity)
        {
            try
            {
                return HolidayDataService.InsertHolidayReturnId(holidayentity);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateHoliday(HolidayEntity holidayentity)
        {
            try
            {
                return HolidayDataService.UpdateHoliday(holidayentity);
            }
            catch
            {
                throw;
            }
        }

        public List<HolidayEntity> GetAllHolidays(ArgsEntity args)
        {
            try
            {
                return HolidayDataService.GetAllHolidays(args);
            }
            catch
            {
                throw;
            }
        }

        public HolidayEntity GetHolidayTypeByCode(string holidayType)
        {
            try
            {
                return HolidayDataService.GetHolidayTypeByCode(holidayType);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteHoliday(int holidayId)
        {
            try
            {
                return HolidayDataService.DeleteHoliday(holidayId);
            }
            catch
            {
                throw;
            }
        }

        public List<HolidayEntity> GetAllHolidaytypes()
        {
            try
            {
                return HolidayDataService.GetAllHolidaytypes();
            }
            catch
            {
                throw;
            }
        }

        public bool IsHolidayExistsForTheDay(string createdBy, DateTime effDate)
        {
            try
            {
                return HolidayDataService.IsHolidayExistsForTheDay(createdBy, effDate);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateHolidayLastChangedDate(HolidayEntity holidayentity)
        {
            try
            {
                return HolidayDataService.UpdateHolidayLastChangedDate(holidayentity);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateHolidayDate(HolidayEntity holidayentity)
        {
            try
            {
                return HolidayDataService.UpdateHolidayDate(holidayentity);
            }
            catch
            {
                throw;
            }
        }
    }
}
