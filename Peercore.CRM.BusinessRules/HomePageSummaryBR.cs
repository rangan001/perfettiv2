﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class HomePageSummaryBR
    {
        private static HomePageSummaryBR instance = null;
        private static object syncLock = new Object();

        private HomePageSummaryDAO HomePageSummaryDataService;

        public static HomePageSummaryBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new HomePageSummaryBR();
                }
                return instance;
            }
        }

        private HomePageSummaryBR()
        {
            HomePageSummaryDataService = new HomePageSummaryDAO();
        }

        //#region - Init Object -

        //public LeadAddressEntity CreateObject()
        //{
        //    return HomePageSummaryDataService.CreateObject();
        //}

        //public LeadAddressEntity CreateObject(int entityId)
        //{
        //    return HomePageSummaryDataService.CreateObject(entityId);
        //}

        //#endregion

        public List<OpenDealEntity> GetTopFiveOpportunities(string sBusiness, ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetTopFiveOpportunities(sBusiness, args);
            }
            catch
            {
                throw;
            }
        }

        public List<OpportunityOwnerEntity> GetLeaderBoard(string cutoffDate, string closeDate, string businessCode, string sPeriod, ArgsEntity args)
        {
            try
            {
                //DateTime cutoffDate = DateTime.Today;

                //if (sPeriod == "YTD")
                //{
                //    if (DateTime.Now.Date >= new DateTime(DateTime.Now.Date.Year, 7, 1))
                //        cutoffDate = new DateTime(DateTime.Now.Date.Year, 7, 1);
                //    else
                //        cutoffDate = new DateTime(DateTime.Now.Date.Year - 1, 7, 1);
                //}
                //else if (sPeriod == "MTD")
                //    cutoffDate = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 1);

                List<OpportunityOwnerEntity> opportunityOwners = HomePageSummaryDataService.GetLeaderBoard(cutoffDate, closeDate, sPeriod, businessCode, args);

                foreach (var opportunityOwner in opportunityOwners)
                {
                    int? allOpps = HomePageSummaryDataService.GetTotalOpportunities(opportunityOwner.Originator, false, cutoffDate,closeDate, sPeriod, businessCode);

                    if (allOpps.HasValue && allOpps > 0)
                    {
                        int? wonOpps = HomePageSummaryDataService.GetTotalOpportunities(opportunityOwner.Originator, true, cutoffDate, closeDate, sPeriod, businessCode);

                        if (wonOpps.HasValue)
                            opportunityOwner.Ratio = Convert.ToInt32(wonOpps * 100 / allOpps);
                    }
                }

                //return opportunityOwners.OrderByDescending(o => o.Amount).Take(5).ToList();
                return opportunityOwners;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerOpportunityEntity> GetLeaderCustomerOpportunity(ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetLeaderCustomerOpportunity(args);
            }
            catch
            {
                throw;
            }
        }
        public List<LeadCustomerOpportunityEntity> GetLeaderCustomerOpportunityCount(ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetLeaderCustomerOpportunityCount(args);
            }
            catch
            {
                throw;
            }
        }

        public List<SalesTrendEntity> GetSalesTrend(string sBusiness)
        {
            try
            {
                return HomePageSummaryDataService.GetSalesTrend(sBusiness);
            }
            catch
            {
                throw;
            }
        }

        public List<KeyAccountEntity> GetSalesKeyAccounts( string repCode, ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetSalesKeyAccounts(repCode, args);
            }
            catch
            {
                throw;
            }
        }

        public List<SalesLeaderEntity> GetSalesLeaderBoard(string businessCode, string period,ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetSalesLeaderBoard(businessCode, period, args);
            }
            catch
            {
                throw;
            }
        }

        public List<KeyAccountEntity> GetTopSalesReps(ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetTopSalesReps(args);
            }
            catch
            {
                throw;
            }
        }

        public List<KeyAccountEntity> GetTopSalesDistributors(ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetTopSalesDistributors(args);
            }
            catch
            {
                throw;
            }
        }

        public List<KeyAccountEntity> GetTopSalesProducts(ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetTopSalesProducts(args);
            }
            catch
            {
                throw;
            }
        }

        public List<KeyAccountEntity> GetTopSalesMarkets(ArgsEntity args)
        {
            try
            {
                return HomePageSummaryDataService.GetTopSalesMarkets(args);
            }
            catch
            {
                throw;
            }
        }
    }
}
