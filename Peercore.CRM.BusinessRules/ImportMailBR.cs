﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ImportMailBR
    {
        private static ImportMailBR instance = null;
        private static object syncLock = new Object();

        private ImportMailDAO ImportMailDataService;

        public static ImportMailBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ImportMailBR();
                }
                return instance;
            }
        }

        private ImportMailBR()
        {
            ImportMailDataService = new ImportMailDAO();
        }

        #region - Init Object -

        public ImportMailEntity CreateObject()
        {
            return ImportMailDataService.CreateObject();
        }

        public ImportMailEntity CreateObject(int entityId)
        {
            return ImportMailDataService.CreateObject(entityId);
        }

        #endregion

        public List<ImportMailEntity> GetLeadEmails(ArgsEntity args)
        {
            try
            {
                return ImportMailDataService.GetLeadEmails(args);
            }
            catch
            {
                throw;
            }
        }

        public List<ImportMailEntity> GetCustEmails(ArgsEntity args)
        {
            try
            {
                return ImportMailDataService.GetCustEmails(args);
            }
            catch
            {
                throw;
            }
        }

        public ImportMailEntity GetEmailbyId(int mailId)
        {
            try
            {
                return ImportMailDataService.GetEmailbyId(mailId);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
