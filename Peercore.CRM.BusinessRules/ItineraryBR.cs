﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.DataAccess.DAO;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ItineraryBR
    {
        private static ItineraryBR instance = null;
        private static object syncLock = new Object();

        private ItineraryDAO ItineraryDataService;

        public static ItineraryBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ItineraryBR();
                }
                return instance;
            }
        }

        private ItineraryBR()
        {
            ItineraryDataService = new ItineraryDAO();
        }

        #region - Init Object -

        //public ItineraryEntity CreateObject()
        //{
        //    return ItineraryDataService.CreateObject();
        //}

        //public ItineraryEntity CreateObject(int entityId)
        //{
        //    return ItineraryDataService.CreateObject(entityId);
        //}

        #endregion

        public ItineraryHeaderEntity GetItineraryDetailsForUser(string tme , DateTime date)
        {
            try
            {
                ItineraryHeaderEntity headerEntity = new ItineraryHeaderEntity();
                headerEntity = ItineraryDataService.GetItineraryHeaderByUser(tme, date);

                //Get Holidays List
                ArgsEntity args = new ArgsEntity();
                List<HolidayEntity> holidayList = new List<HolidayEntity>();
                args.SToday = string.IsNullOrEmpty(args.SToday) ? "" : args.SToday;
                string firstOfMonth = new DateTime(date.Year, date.Month, 1).ToString("MM/dd/yyyy");
                string lastOfMonth = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).ToString("MM/dd/yyyy");
                args.SStartDate = firstOfMonth;//startdate.ToString("yyyy/MM/dd");
                args.SEndDate = lastOfMonth;//enddate.ToString("yyyy/MM/dd");;
                args.Originator = tme;
                holidayList = HolidayBR.Instance.GetAllHolidays(args);

                List<HolidayEntity> holidayTypes = new List<HolidayEntity>();
                holidayTypes = HolidayBR.Instance.GetAllHolidaytypes();

                if (headerEntity != null)
                {
                    headerEntity.ItineraryDetailList = ItineraryDataService.GetItineraryDetailsByHeaderId(tme, date, headerEntity.ItineraryHeaderId);
                }
                else
                {
                    headerEntity.ItineraryDetailList = ItineraryDataService.GetItineraryDetailsByHeaderId(tme, date, 0);
                }

                foreach (ItineraryDetailEntity item in headerEntity.ItineraryDetailList)
                {
                    HolidayEntity holi = null;
                    if (item.Date.Date.DayOfWeek == DayOfWeek.Saturday)
                    {
                        holi = new HolidayEntity();
                        holi = holidayTypes.SingleOrDefault(a => a.HolidayType.Equals("SATR")) as HolidayEntity;
                        item.DayTypeColor = holi.TypeColor;
                    }
                    else if (item.Date.Date.DayOfWeek == DayOfWeek.Sunday)
                    {
                        holi = new HolidayEntity();
                        holi = holidayTypes.SingleOrDefault(a => a.HolidayType.Equals("SUND")) as HolidayEntity;
                        item.DayTypeColor = holi.TypeColor;
                    }
                    else
                        item.DayTypeColor = "";

                    foreach (HolidayEntity holiday in holidayList)
                    {
                        if (item.Date.Date == holiday.Date.Date)
                        {
                            item.DayTypeColor = holiday.TypeColor;
                        }                        
                    }
                }

                return headerEntity;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveItinerary(ItineraryHeaderEntity itineraryHeaderEntity)
        {
            DbWorkflowScope transactionScope = ItineraryDataService.WorkflowScope;
            bool isSuccess = false;
            bool isSucc = false;
            int headerId=0;
            try
            {

                    using (transactionScope)
                    {
                        if ( itineraryHeaderEntity.ItineraryHeaderId>0)
                        {
                            isSucc = ItineraryDataService.UpdateItineraryHeader(itineraryHeaderEntity);
                            headerId = itineraryHeaderEntity.ItineraryHeaderId;
                        }
                        else
                        {
                            isSucc = ItineraryDataService.InsertItineraryHeader(itineraryHeaderEntity,out headerId);
                        }

                        if (isSucc == true && itineraryHeaderEntity.ItineraryDetailList != null)
                        {

                            foreach (ItineraryDetailEntity itineraryEntity in itineraryHeaderEntity.ItineraryDetailList)
                            {
                                if (itineraryEntity.ItineraryDetailId > 0)
                                {
                                    isSuccess = ItineraryDataService.UpdateItineraryDetails(itineraryEntity);
                                }
                                else
                                {
                                    isSuccess = ItineraryDataService.InsertItineraryDetails(itineraryEntity, headerId);
                                }
                            }
                        }
                        else if (itineraryHeaderEntity.ItineraryDetailList == null)
                        {
                            isSuccess = true;
                        }

                        if (isSuccess && isSucc)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public int InsertItineraryLogEntry(ItineraryHeaderEntity itineraryHeaderEntity)
        {
            int itineraryLogEntryId = 0;
            try
            {
                itineraryLogEntryId = ItineraryDataService.InsertItineraryLogEntry(itineraryHeaderEntity);
            }
            catch
            {
                throw;
            }
            return itineraryLogEntryId;
        }

        public bool UpdateItineraryHeaderStatus(ItineraryHeaderEntity itineraryHeaderEntity)
        {
            bool isSuccess = true;
            try
            {
                isSuccess = ItineraryDataService.UpdateItineraryHeaderStatus(itineraryHeaderEntity);
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public ItineraryHeaderEntity GetItineraryLogEntryById(int itineraryLogEntryId)
        {
            try
            {
                return ItineraryDataService.GetItineraryLogEntryById(itineraryLogEntryId);
            }
            catch
            {
                throw;
            }
        }

        public OriginatorEntity GetItineraryAssignedToByHeaderId(int itineraryHeaderId)
        {
            try
            {
                return ItineraryDataService.GetItineraryAssignedToByHeaderId(itineraryHeaderId);
            }
            catch
            {
                throw;
            }
        }

        public ItineraryHeaderEntity GetItineraryHeaderByUser(string tme, DateTime date)
        {
            try
            {
                return ItineraryDataService.GetItineraryHeaderByUser(tme, date);
            }
            catch
            {
                throw;
            }
        }

        public ItineraryHeaderEntity GetItineraryHeaderByHeaderId(int itineraryHeaderId)
        {
            try
            {
                return ItineraryDataService.GetItineraryHeaderByHeaderId(itineraryHeaderId);
            }
            catch
            {
                throw;
            }
        }
    }
}
