﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class LeadAddressBR
    {
        private static LeadAddressBR instance = null;
        private static object syncLock = new Object();

        private LeadAddressDAO LeadAddressDataService;

        public static LeadAddressBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new LeadAddressBR();
                }
                return instance;
            }
        }

        private LeadAddressBR()
        {
            LeadAddressDataService = new LeadAddressDAO();
        }

        #region - Init Object -

        public LeadAddressEntity CreateObject()
        {
            return LeadAddressDataService.CreateObject();
        }

        public LeadAddressEntity CreateObject(int entityId)
        {
            return LeadAddressDataService.CreateObject(entityId);
        }

        #endregion

        public List<LeadAddressEntity> GetCustomerAddresses(int leadId)
        {
            try
            {
                return LeadAddressDataService.GetCustomerAddresses(leadId);
            }
            catch
            {
                throw;
            }
        }

        public LeadAddressEntity GetLeadTableAddress(int leadId)
        {
            try
            {
                return LeadAddressDataService.GetLeadTableAddress(leadId);
            }
            catch
            {
                throw;
            }
        }

        public bool Save(LeadAddressEntity leadAddress, ref int leadAddressId)
        {
            bool isSuccess = false;
            try
            {
                if (leadAddress.LeadAddressID > 0)
                {
                    isSuccess = LeadAddressDataService.UpdateLeadAddress(leadAddress);
                    leadAddressId = leadAddress.LeadAddressID;
                }
                else
                {
                    isSuccess = LeadAddressDataService.InsertLeadAddress(leadAddress,ref leadAddressId);
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool TransferAddressToCustomer(List<LeadAddressEntity> customerAddressList)
        {
            try
            {
                return LeadAddressDataService.TransferAddressToCustomer(customerAddressList);
            }
            catch
            {
                throw;
            }
        }
    }
}
