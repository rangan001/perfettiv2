﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class LeadStageBR
    {
        private static LeadStageBR instance = null;
        private static object syncLock = new Object();
        private LeadStageDAO LeadStageDataService;

        public static LeadStageBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new LeadStageBR();
                }
                return instance;
            }
        }

        private LeadStageBR()
        {
            LeadStageDataService = new LeadStageDAO();
        }
        
        #region - Init Object -

        public LeadStageEntity CreateObject()
        {
            return LeadStageDataService.CreateObject();
        }

        public LeadStageEntity CreateObject(int entityId)
        {
            return LeadStageDataService.CreateObject(entityId);
        }

        #endregion

        public List<LeadStageEntity> GetLeadStages(ArgsEntity args)
        {
            try
            {
                return LeadStageDataService.GetLeadStages(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadStageEntity> GetLeadStageChartData(ArgsEntity args)
        {
            try
            {
               // List<string> stageNameList = new List<string>(new string[] { "Lead", "Quote", "Customer", "End User" });
                List<string> stageNameList = new List<string>(new string[] { "Customer", "Pending Approvals" });
                List<LeadStageEntity> stageList = new List<LeadStageEntity>(LeadStageDataService.GetLeadStageChartData(args));
                List<LeadStageEntity> orderedStageList = new List<LeadStageEntity>();


                int orderedindex = 0;
                foreach (string order in stageNameList)
                {
                    int index = 0;
                    foreach (LeadStageEntity item in stageList)
                    { 
                        index++;
                        if (item.LeadStage == stageNameList[orderedindex].ToString()) {
                            orderedStageList.Add(item);
                        }
                    }
                        orderedindex++;
                }
                return orderedStageList;

              //  return LeadStageDataService.GetLeadStageChartData(args);
            }
               
            catch (Exception)
            {
                throw;
            }
        }
    }
}
