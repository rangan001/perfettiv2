﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class MarketBR
    {
        private static MarketBR instance = null;
        private static object syncLock = new Object();

        private MarketDAO MarketDataService;

        public static MarketBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new MarketBR();
                }
                return instance;
            }
        }

        private MarketBR()
        {
            MarketDataService = new MarketDAO();
        }

        #region - Init Object -

        public MarketEntity CreateObject()
        {
            return MarketDataService.CreateObject();
        }

        public MarketEntity CreateObject(int entityId)
        {
            return MarketDataService.CreateObject(entityId);
        }

        #endregion

        public List<MarketEntity> GetAllMarket(ArgsEntity args)
        {
            try
            {
                return MarketDataService.GetAllMarket(args);
            }
            catch
            {
                throw;
            }
        }


        public bool SaveMarket(MarketEntity marketEntity)
        {
            DbWorkflowScope transactionScope = MarketDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    if (marketEntity.MarketId == 0)
                    {
                        isSuccess = MarketDataService.InsertMarket(marketEntity);
                    }
                    else
                    {
                        isSuccess = MarketDataService.UpdateMarket(marketEntity);
                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }


        public bool AssignTMEMarkets(List<MarketEntity> marketEntityList)
        {
            DbWorkflowScope transactionScope = MarketDataService.WorkflowScope;
            bool success = true;

            try
            {
                using (transactionScope)
                {
                    MarketDAO marketDataService = new MarketDAO(transactionScope);

                    foreach (MarketEntity objMarketEntity in marketEntityList)
                    {
                        success = marketDataService.UpdateTMEMarkets(objMarketEntity);

                        if (!success)
                            break;
                    }

                    if (success)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return success;
        }

        public bool DeleteTMEMarkets(MarketEntity tmeMarket)
        {
            DbWorkflowScope transactionScope = MarketDataService.WorkflowScope;
            bool success = false;

            try
            {
                using (transactionScope)
                {
                    MarketDAO marketDataService = new MarketDAO(transactionScope);

                    //foreach (MarketEntity objMarketEntity in marketEntityList)
                    //{
                    success = marketDataService.UpdateTMEMarkets(tmeMarket);

                //        if (!success)
                //            break;
                //    }

                    if (success)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
               }
            }
            catch
            {
                throw;
            }

            return success;
        }
        
        public List<MarketEntity> GetMarketTargetsByDistributorId(ArgsEntity args)
        {
            try
            {
                return MarketDataService.GetMarketTargetsByDistributorId(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveMarketTargets(List<MarketEntity> marketTargetList)
        {
            DbWorkflowScope transactionScope = MarketDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (marketTargetList.Count != 0)
                {
                    using (transactionScope)
                    {
                        MarketDataService = new MarketDAO(transactionScope);

                        foreach (MarketEntity marketEntity in marketTargetList)
                        {
                            if (marketEntity.TargetId > 0)
                            {
                                isSuccess = MarketDataService.UpdateMarketTarget(marketEntity);
                            }
                            else
                            {
                                if (marketEntity.Target > 0)
                                    isSuccess = MarketDataService.InsertMarketTarget(marketEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool IsMarketExists(MarketEntity marketEntity)
        {
            try
            {
                return MarketDataService.IsMarketExists(marketEntity);
            }
            catch
            {
                throw;
            }
        }
    }
}
