﻿using Peercore.CRM.DataAccess.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Peercore.CRM.Model;

namespace Peercore.CRM.BusinessRules
{
    public class MediaBR
    {
        private static MediaBR instance = null;
        private static object syncLock = new Object();

        private MediaDAO MediaDataService;

        public static MediaBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new MediaBR();
                }
                return instance;
            }
        }

        private MediaBR()
        {
            MediaDataService = new MediaDAO();
        }

        #region - Upload Media Files -
        public List<UploadMediaModel> GetAllMedia(string repCode, string category, string custCode, ArgsModel args)
        {
            try
            {
                return MediaDataService.GetAllMedia(repCode, category, custCode, args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveUpdateMediaFiles(UploadMediaModel mediaDetail)
        {
            try
            {
                return MediaDataService.SaveUpdateMediaFiles(mediaDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteMediaFiles(int mediaFileId, string modifiedBy)
        {
            try
            {
                return MediaDataService.DeleteMediaFiles(mediaFileId, modifiedBy);
            }
            catch
            {
                throw;
            }
        }

        public List<MediaCategoryModel> GetAllMediaCategoriesForDDL(ArgsModel args)
        {
            try
            {
                return MediaDataService.GetAllMediaCategoriesForDDL(args);
            }
            catch
            {
                throw;
            }
        }

        public string LoadCustomerName(string custCode)
        {
            return MediaDataService.LoadCustomerName(custCode);
        }


        #endregion

        #region - Media File Category -

        public List<MediaCategoryModel> GetAllMediaCategories(ArgsModel args)
        {
            try
            {
                return MediaDataService.GetAllMediaCategories(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveMediaCategory(MediaCategoryModel categoryDetail)
        {
            try
            {
                return MediaDataService.SaveMediaCategoroy(categoryDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateMediaCategory(int catId, string catName, string description, string ModifiedBy)
        {
            try
            {
                return MediaDataService.UpdateMediaCategory(catId, catName, description, ModifiedBy);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteMediaCategory(int catId, string originator)
        {
            try
            {
                return MediaDataService.DeleteMediaCategory(catId, originator);
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
