﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;


namespace Peercore.CRM.BusinessRules
{
    public class MerchandiseBR
    {
        private static MerchandiseBR instance = null;
        private static object syncLock = new Object();

        private MerchandiseDAO MerchandiseDataService;

        public static MerchandiseBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new MerchandiseBR();
                }
                return instance;
            }
        }

        private MerchandiseBR()
        {
            MerchandiseDataService = new MerchandiseDAO();
        }

        #region - Init Object -

        public MerchandiseEntity CreateObject()
        {
            return MerchandiseDataService.CreateObject();
        }

        public MerchandiseEntity CreateObject(int entityId)
        {
            return MerchandiseDataService.CreateObject(entityId);
        }

        #endregion

        public List<MerchandiseEntity> GetAllMerchandise(ArgsEntity args)
        {
            try
            {
                return MerchandiseDataService.GetAllMerchandise(args);
            }
            catch
            {
                throw;
            }
        }

        public List<MerchandiseEntity> GetCustomerMerchandise(ArgsEntity args)
        {
            try
            {
                return MerchandiseDataService.GetCustomerMerchandise(args);
            }
            catch
            {
                throw;
            }
        }

        public bool InsertCustomerMerchandises(List<MobileCustomerMerchandisingEntity> mobileCustomerProductEntityList)
        {
            DbWorkflowScope transactionScope = MerchandiseDataService.WorkflowScope;
            MerchandiseDAO merchandiseDAO = new MerchandiseDAO(transactionScope);
            bool isSuccess = false;
            try
            {
                if (mobileCustomerProductEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        isSuccess = merchandiseDAO.DeleteAllCustomerMerchandisesForToday(mobileCustomerProductEntityList[0]);
                        foreach (MobileCustomerMerchandisingEntity mobileCustomerProductEntity in mobileCustomerProductEntityList)
                        {
                            isSuccess = merchandiseDAO.InsertCustomerMerchandises(mobileCustomerProductEntity);
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }
    }
}
