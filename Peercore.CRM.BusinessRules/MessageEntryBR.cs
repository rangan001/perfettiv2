﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class MessageEntryBR
    {
        private static MessageEntryBR instance = null;
        private static object syncLock = new Object();
        private MessageEntryDAO MessageEntryService;

        public static MessageEntryBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new MessageEntryBR();
                }
                return instance;
            }
        }

        private MessageEntryBR()
        {
            MessageEntryService = new MessageEntryDAO();
        }

        #region - Init Object -

        public MessagesEntity CreateObject()
        {
            return MessageEntryService.CreateObject();
        }

        public MessagesEntity CreateObject(int entityId)
        {
            return MessageEntryService.CreateObject(entityId);
        }

        #endregion


        public bool Save(MessagesEntity message,List<OriginatorEntity> originatorlist,out int newrecordid)
        {
            DbWorkflowScope transactionScope = MessageEntryService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    if (message.MessageID != 0)
                    {
                        newrecordid = message.MessageID;
                        isSuccess = MessageEntryService.Update(message);
                    }
                    else
                    {
                        isSuccess = MessageEntryService.Insert(message, out newrecordid);
                    }

                    if (isSuccess)
                    {
                        foreach (OriginatorEntity originator in originatorlist)
                        {
                            isSuccess = MessageEntryService.InsertMessageRep(message, originator);

                            if (!isSuccess)
                                break;
                        }
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return isSuccess;
        }

        public List<MessagesEntity> GetMessageByOriginator(string originator)
        {
            try
            {
                return MessageEntryService.GetMessageByOriginator(originator);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public MessagesEntity GetMessageCount(string originator, string datestring)
        {
            try
            {
                return MessageEntryService.GetMessageCount(originator, datestring);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public MessagesEntity GetMessage(int messageId)
        {
            try
            {
                return MessageEntryService.GetMessage(messageId);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool DeleteMessage(int messageId)
        {
            try
            {
                 return MessageEntryService.DeleteMessage(messageId);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool UpdateMessageRep(string username)
        {
            try
            {
                return MessageEntryService.UpdateMessageRep(username);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
