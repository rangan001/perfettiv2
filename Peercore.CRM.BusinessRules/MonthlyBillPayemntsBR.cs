﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class MonthlyBillPayemntsBR
    {
        private static MonthlyBillPayemntsBR instance = null;
        private static object syncLock = new Object();

        private MonthlyBillPaymentsDAO MonthlyPaymentService;

        public static MonthlyBillPayemntsBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new MonthlyBillPayemntsBR();
                }
                return instance;
            }
        }

        private MonthlyBillPayemntsBR()
        {
            MonthlyPaymentService = new MonthlyBillPaymentsDAO();
        }

        #region MonthlyBillPayments

        public List<PaymentModel> GetAllPayments(ArgsModel args)
        {
            try
            {
                return MonthlyPaymentService.GetAllPayments(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SavePayment(PaymentModel paymentDetail)
        {
            try
            {
                return MonthlyPaymentService.SavePayments(paymentDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdatePayment(PaymentModel paymentDetail)
        {
            try
            {
                return MonthlyPaymentService.UpdatePayment(paymentDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool DeletePayment(int paymentId, string originator)
        {
            try
            {
                return MonthlyPaymentService.DeletePayment(paymentId, originator);
            }
            catch
            {
                throw;
            }
        }

        public bool UploadPaymentExcel(string FilePath, string UserName)
        {
            try
            {
                return MonthlyPaymentService.UploadPayments(FilePath, UserName);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        #endregion


    }
}
