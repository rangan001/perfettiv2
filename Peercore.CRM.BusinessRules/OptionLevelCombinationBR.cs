﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;


namespace Peercore.CRM.BusinessRules
{
    public class OptionLevelCombinationBR
    {
        private static OptionLevelCombinationBR instance = null;
        private static object syncLock = new Object();

        private OptionLevelCombinationDAO OptionLevelDataService;

        public static OptionLevelCombinationBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new OptionLevelCombinationBR();
                }
                return instance;
            }
        }

        private OptionLevelCombinationBR()
        {
            OptionLevelDataService = new OptionLevelCombinationDAO();
        }

        #region - Init Object -

        public OptionLevelCombinationEntity CreateObject()
        {
            return OptionLevelDataService.CreateObject();
        }

        public OptionLevelCombinationEntity CreateObject(int entityId)
        {
            return OptionLevelDataService.CreateObject(entityId);
        }

        #endregion

        public List<OptionLevelCombinationEntity> GetOptionLevelCombinationByOptionLevelId(int optionLevelId)
        {
            try
            {
                return OptionLevelDataService.GetOptionLevelCombinationByOptionLevelId(optionLevelId);
            }
            catch
            {
                throw;
            }
        }

        public List<OptionLevelCombinationEntity> GetOptionLevelCombinationByDivisionId(int divisionId)
        {
            try
            {
                return OptionLevelDataService.GetOptionLevelCombinationByDivisionId(divisionId);
            }
            catch
            {
                throw;
            }
        }
    }
}
