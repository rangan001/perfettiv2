﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class OriginatorBR
    {
        private static OriginatorBR instance = null;
        private static object syncLock = new Object();
        private OriginatorDAO OriginatorDataService;

        public static OriginatorBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new OriginatorBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public OriginatorEntity CreateObject()
        {
            return OriginatorDataService.CreateObject();
        }

        public OriginatorEntity CreateObject(int entityId)
        {
            return OriginatorDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private OriginatorBR()
        {
            OriginatorDataService = new OriginatorDAO();
        }
        #endregion

        #region - Public Methods -
      
        public string GetChildOriginators(string originator, bool managerMode = false)
        {
            string sWhereCls = "";

            try
            {
                string sChildOriginators = "";

                if (originator != "")
                {
                    if (managerMode)    // Quinn - 16-Mar-2011
                    {
                        sChildOriginators = OriginatorDataService.GetChildOriginators(originator);

                        if (sChildOriginators != "")
                        {
                            sChildOriginators = sChildOriginators.Remove(sChildOriginators.Length - 1, 1);  // Remove the Last Comma
                            sWhereCls = " (originator = '" + originator + "' OR originator IN (" + sChildOriginators + ") )";
                        }
                        else
                            sWhereCls = " (originator = '" + originator + "')";
                    }
                    else
                        sWhereCls = " (originator = '" + originator + "')";
                }

                return sWhereCls;
            }
            catch
            {
                throw;
            }
        }

        public List<string> GetChildReps(string originator, bool managerMode, ref string sWhereCls, string childOriginators = null) 
        {
            try
            {
                return OriginatorDataService.GetChildReps(originator, managerMode, ref sWhereCls, childOriginators);
            }
            catch 
            {                
                throw;
            }
        }

        public OriginatorEntity GetLoginDetails(string userName, string password)
        {
            try
            {
                return OriginatorDataService.GetLoginDetails(userName, password);
            }
            catch
            {                
                throw;
            }
        }

        public OriginatorEntity GetLoginDetailsWithEMEI(string userName, string password, string emei)
        {
            try
            {
                return OriginatorDataService.GetLoginDetailsWithEMEI(userName, password, emei);
            }
            catch
            {                
                throw;
            }
        }

        public bool IsEmeiExsistThisUser(string userName, string emei)
        {
            try
            {
                return OriginatorDataService.IsEmeiExsistThisUser(userName, emei);
            }
            catch
            {                
                throw;
            }
        }

        public bool IsNotEmeiUser(string userName)
        {
            OriginatorDAO originatorDAO = new OriginatorDAO();

            try
            {
                return originatorDAO.IsNotEmeiUser(userName);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public OriginatorEntity GetOriginator(string originator)
        {
            try
            {
                return OriginatorDataService.GetOriginator(originator);
            }
            catch
            {                
                throw;
            }
        }

        public string GetRepCode(string originator)
        {
            try
            {
               return OriginatorDataService.GetRepCode(originator);
            }
            catch
            {
                throw;
            }
        }

        public string GetBDMString()
        {
            try
            {                
                StringBuilder sbBDM = new StringBuilder();
                int i = 1;

                List<OriginatorEntity>  originatorList = OriginatorDataService.GetBDMList();

                foreach (OriginatorEntity originatorEntity in originatorList)
                {
                    sbBDM.Append("'" + originatorEntity.UserName + "',");
                    i++;
                }
                if (i > 1)
                    sbBDM.Remove(sbBDM.Length - 1, 1);
                return sbBDM.ToString();
            }
            catch
            {
                throw;
            }
        }

        public List<KeyValuePair<string, string>> GetChildRepsList(string childOriginators)
        {
            try
            {
                return OriginatorDataService.GetChildRepsList(childOriginators);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetAllRepCodes(string originator)
        {
            try
            {
                return OriginatorDataService.GetAllRepCodes(originator);
            }
            catch
            {                
                throw;
            }
        }

        public List<StateEntity> GetDistinctRepStates()
        {
            try
            {
                return OriginatorDataService.GetDistinctRepStates();
            }
            catch
            {                
                throw;
            }
        }

        public List<OriginatorEntity> GetRepGroupOriginators(string originator)
        {
            try
            {
                return OriginatorDataService.GetRepGroupOriginators(originator);
            }
            catch
            {                
                throw;
            }
        }

        public List<OriginatorEntity> GetCRMReps()
        {
            try
            {
                return OriginatorDataService.GetCRMReps();
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetChildOriginatorsList(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetChildOriginatorsList(args);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetRepForKeyAccounts(string distributor)
        {
            try
            {
                return OriginatorDataService.GetRepForKeyAccounts(distributor);
            }
            catch
            {
                throw;
            }
        }

        public string GetPayMethod(string sRepCode)
        {
            try
            {
                return OriginatorDataService.GetPayMethod(sRepCode);
            }
            catch
            {
                throw;
            }
        }

        public string GetUserName(string sRepCode)
        {
            try
            {
                return OriginatorDataService.GetUserName(sRepCode);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetOriginatorsByCatergory(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetOriginatorsByCatergory(args);
            }
            catch
            {
                throw;
            }
        }
        
        public List<OriginatorEntity> GetDistributersByASMOriginator(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetDistributersByASMOriginator(args);
            }
            catch
            {
                throw;
            }
        }
        
        public List<OriginatorEntity> GetAllSRByOriginator(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetAllSRByOriginator(args);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetAllDistributorList(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetAllDistributorList(args);
            }
            catch
            {
                throw;
            }
        } 
        
        public List<OriginatorEntity> GetAllOriginatorsByCatergory(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetAllOriginatorsByCatergory(args);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetASEsByDateForReport(ArgsEntity args, DateTime startDate, DateTime endDate)
        {
            try
            {
                return OriginatorDataService.GetASEsByDateForReport(args, startDate, endDate);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetDistributorsByCatergoryWithNameAndCode(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetDistributorsByCatergoryWithNameAndCode(args);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetRepsByOriginator_For_Route_Assign(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetOriginatorsByCatergory_For_Route_Assign(args);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetAllOriginators(string originator)
        {
            try
            {
                return OriginatorDataService.GetAllOriginators(originator);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetRepsByOriginator(string rep, string username)
        {
            try
            {
                return OriginatorDataService.GetRepsByOriginator(rep, username);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetRepsByASEOriginator(string ASE)
        {
            try
            {
                return OriginatorDataService.GetRepsByASEOriginator(ASE);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetDistributorsByLikeName(string name)
        {
            try
            {
                return OriginatorDataService.GetDistributorsByLikeName(name);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public List<RepEntity> GetRepTargetsByDistributor(string username)
        //{
        //    try
        //    {
        //        return OriginatorDataService.GetRepTargetsByDistributor(username);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public List<TargetInfoEntity> GetRepMonthleyTargetsByDistributor(string username, string detailType, ArgsEntity args)
        //{
        //    try
        //    {
        //        List<TargetInfoEntity> actuallist = OriginatorDataService.GetRepMonthleyTargetsByDistributor(username, detailType, args);
        //        ReturnCurrentView(ref actuallist);
        //        return actuallist;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public SalesInfoDetailViewStateEntity GetRepMonthleyTargetsByDistributor(SalesInfoDetailSearchCriteriaEntity busSalesEnqSrc, ArgsEntity args)
        {
            try
            {
                SalesInfoDetailViewStateEntity oSalesInfoDetailViewState = new SalesInfoDetailViewStateEntity();

                string sEnquiryTitle = "";

                oSalesInfoDetailViewState.DisplayOptionIndex = 1;
                oSalesInfoDetailViewState.SortFieldOptionIndex = 3;

                oSalesInfoDetailViewState.sBrand = busSalesEnqSrc.BrandDescription;
                oSalesInfoDetailViewState.sCustomer = busSalesEnqSrc.CustomerDescription;
                oSalesInfoDetailViewState.sMarket = busSalesEnqSrc.Market;
                oSalesInfoDetailViewState.sMonth = busSalesEnqSrc.Month;
                oSalesInfoDetailViewState.sRep = busSalesEnqSrc.RepDescription;
                switch (busSalesEnqSrc.DetailType)
                {
                    case "market":
                        sEnquiryTitle = "Market";
                        oSalesInfoDetailViewState.sSubDetailType = "brand";
                        break;
                    case "brand":
                        sEnquiryTitle = "Brand";
                        oSalesInfoDetailViewState.sSubDetailType = "product";
                        break;
                    case "product":
                        sEnquiryTitle = "Product";
                        oSalesInfoDetailViewState.sSubDetailType = "customer";
                        break;
                    case "customer":
                        //sEnquiryTitle = "Customer";
                        sEnquiryTitle = "Distributor";
                        oSalesInfoDetailViewState.sSubDetailType = "product";
                        break;
                    case "rep":
                        sEnquiryTitle = "Sales Rep.";
                        oSalesInfoDetailViewState.sSubDetailType = "customer";
                        break;
                    default:
                        break;
                }

                List<TargetInfoEntity> actuallist = OriginatorDataService.GetRepMonthleyTargetsByDistributor(busSalesEnqSrc, args);
                ReturnCurrentView(ref actuallist);

                oSalesInfoDetailViewState.TargetInfoEntityList = actuallist;

                return oSalesInfoDetailViewState;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ReturnCurrentView(ref List<TargetInfoEntity> returnlist)
        {
            List<int> selectedMonth = new List<int>();

            int currentMonth = DateTime.Today.Month ;
            currentMonth = currentMonth - 2;
            if (currentMonth <= 0) currentMonth += 12;
            for (int x = 0; x < 3; x++)
            {
                selectedMonth.Add(currentMonth + x);

                if (currentMonth > 11) currentMonth = 0;
            }
            if (returnlist != null && returnlist.Count > 0)
                returnlist[0].SelectedMonth = selectedMonth;

            foreach (TargetInfoEntity item in returnlist)
            {
                int index = 0;
                
                foreach (var month in selectedMonth)
                {
                    double targetVal = 0;
                    double actualVal = 0;
                    double targetTotVal = 0;
                    double actualTotVal = 0;
                    if (month == 1)
                    {
                        targetVal = item.TargetMonth1;
                        actualVal = item.ActualMonth1;
                        targetTotVal = item.TargetMonth11Tot;
                        actualTotVal = item.ActualMonth11Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 2)
                    {
                        targetVal = item.TargetMonth2;
                        actualVal = item.ActualMonth2;
                        targetTotVal = item.TargetMonth2Tot;
                        actualTotVal = item.ActualMonth2Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 3)
                    {
                        targetVal = item.TargetMonth3;
                        actualVal = item.ActualMonth3;

                        targetTotVal = item.TargetMonth3Tot;
                        actualTotVal = item.ActualMonth3Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 4)
                    {
                        targetVal = item.TargetMonth4;
                        actualVal = item.ActualMonth4;

                        targetTotVal = item.TargetMonth4Tot;
                        actualTotVal = item.ActualMonth4Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 5)
                    {
                        targetVal = item.TargetMonth5;
                        actualVal = item.ActualMonth5;

                        targetTotVal = item.TargetMonth5Tot;
                        actualTotVal = item.ActualMonth5Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 6)
                    {
                        targetVal = item.TargetMonth6;
                        actualVal = item.ActualMonth6;

                        targetTotVal = item.TargetMonth6Tot;
                        actualTotVal = item.ActualMonth6Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 7)
                    {
                        targetVal = item.TargetMonth7;
                        actualVal = item.ActualMonth7;

                        targetTotVal = item.TargetMonth7Tot;
                        actualTotVal = item.ActualMonth7Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 8)
                    {
                        targetVal = item.TargetMonth8;
                        actualVal = item.ActualMonth8;

                        targetTotVal = item.TargetMonth8Tot;
                        actualTotVal = item.ActualMonth8Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 9)
                    {
                        targetVal = item.TargetMonth9;
                        actualVal = item.ActualMonth9;

                        targetTotVal = item.TargetMonth9Tot;
                        actualTotVal = item.ActualMonth9Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 10)
                    {
                        targetVal = item.TargetMonth10;
                        actualVal = item.ActualMonth10;

                        targetTotVal = item.TargetMonth10Tot;
                        actualTotVal = item.ActualMonth10Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 11)
                    {
                        targetVal = item.TargetMonth11;
                        actualVal = item.ActualMonth11;

                        targetTotVal = item.TargetMonth11Tot;
                        actualTotVal = item.ActualMonth11Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                    else if (month == 12)
                    {
                        targetVal = item.TargetMonth12;
                        actualVal = item.ActualMonth12;

                        targetTotVal = item.TargetMonth12Tot;
                        actualTotVal = item.ActualMonth12Tot;
                        SetActualTarget(item, index, targetVal, actualVal, targetTotVal, actualTotVal);
                        index++;
                    }
                }
            }
        }


        private static void SetActualTarget(TargetInfoEntity item, int index, double targetVal, double actualVal, double targetTotVal, double actualTotVal)
        {
            if (index == 0)
            {
                item.Target1 = targetVal;
                item.Actual1 = actualVal;
                item.Target1Tot = targetTotVal;
                item.Actual1Tot = actualTotVal;
            }
            else if (index == 1)
            {
                item.Target2 = targetVal;
                item.Actual2 = actualVal;
                item.Target2Tot = targetTotVal;
                item.Actual2Tot = actualTotVal;
            }
            else if (index == 2)
            {
                item.Target3 = targetVal;
                item.Actual3 = actualVal;
                item.Target3Tot = targetTotVal;
                item.Actual3Tot = actualTotVal;
            }
        }
        //private void ReturnCurrentView(ref List<TargetInfoEntity> returnlist)
        //{
        //    DateTime last6month = DateTime.Today.AddMonths(-6);

        //    foreach (TargetInfoEntity item in returnlist)
        //    {
        //        for (int i = last6month.Month; i > DateTime.Today.Month; i--)
        //        {
        //            if (i == 1)
        //            {
        //                if(item.TargetMonth1.HasValue)
        //                    item.Actual1 = item.TargetMonth1.Value;
        //            }
        //            else if (i == 2)
        //            {
        //                if (item.TargetMonth2.HasValue)
        //                    item.Actual2 = item.TargetMonth2.Value;
        //            }
        //            else if (i == 3)
        //            {
        //                if (item.TargetMonth3.HasValue)
        //                    item.Actual3 = item.TargetMonth3.Value;
        //            }
        //            else if (i == 4)
        //            {
        //                if (item.TargetMonth4.HasValue)
        //                item.Actual4 = item.TargetMonth4.Value;
        //            }
        //            else if (i == 5)
        //            {
        //                if (item.TargetMonth5.HasValue)
        //                    item.Actual5 = item.TargetMonth5.Value;
        //            }
        //            else if (i == 6)
        //            {
        //                if (item.TargetMonth6.HasValue)
        //                    item.Actual6 = item.TargetMonth6.Value;
        //            }
        //            else if (i == 7)
        //            {
        //                if (item.TargetMonth7.HasValue)
        //                    item.Actual1 = item.TargetMonth7.Value;
        //            }
        //            else if (i == 8)
        //            {
        //                if (item.TargetMonth8.HasValue)
        //                    item.Actual2 = item.TargetMonth8.Value;
        //            }
        //            else if (i == 9)
        //            {
        //                if (item.TargetMonth9.HasValue)
        //                    item.Actual3 = item.TargetMonth9.Value;
        //            }
        //            else if (i == 10)
        //            {
        //                if (item.TargetMonth10.HasValue)
        //                    item.Actual4 = item.TargetMonth10.Value;
        //            }
        //            else if (i == 11)
        //            {
        //                if (item.TargetMonth11.HasValue)
        //                    item.Actual5 = item.TargetMonth11.Value;
        //            }
        //            else if (i == 12)
        //            {
        //                if (item.TargetMonth12.HasValue)
        //                    item.Actual6 = item.TargetMonth12.Value;
        //            }
        //        }
        //    }
        //}


        public SalesInfoDetailViewStateEntity GetTodaySalesDrilldown(SalesInfoDetailSearchCriteriaEntity busSalesEnqSrc, ArgsEntity args)
        {
            try
            {
                SalesInfoDetailViewStateEntity oSalesInfoDetailViewState = new SalesInfoDetailViewStateEntity();

                string sEnquiryTitle = "";

                oSalesInfoDetailViewState.DisplayOptionIndex = 1;
                oSalesInfoDetailViewState.SortFieldOptionIndex = 3;

                oSalesInfoDetailViewState.sBrand = busSalesEnqSrc.BrandDescription;
                oSalesInfoDetailViewState.sCustomer = busSalesEnqSrc.CustomerDescription;
                oSalesInfoDetailViewState.sMarket = busSalesEnqSrc.Market;
                oSalesInfoDetailViewState.sMonth = busSalesEnqSrc.Month;
                oSalesInfoDetailViewState.sRep = busSalesEnqSrc.RepDescription;
                switch (busSalesEnqSrc.DetailType)
                {
                    case "market":
                        sEnquiryTitle = "Market";
                        oSalesInfoDetailViewState.sSubDetailType = "brand";
                        break;
                    case "brand":
                        sEnquiryTitle = "Brand";
                        oSalesInfoDetailViewState.sSubDetailType = "product";
                        break;
                    case "product":
                        sEnquiryTitle = "Product";
                        oSalesInfoDetailViewState.sSubDetailType = "customer";
                        break;
                    case "customer":
                        //sEnquiryTitle = "Customer";
                        sEnquiryTitle = "Distributor";
                        oSalesInfoDetailViewState.sSubDetailType = "product";
                        break;
                    case "rep":
                        sEnquiryTitle = "Sales Rep.";
                        oSalesInfoDetailViewState.sSubDetailType = "customer";
                        break;
                    default:
                        break;
                }

                List<TargetInfoEntity> actuallist = OriginatorDataService.GetTodaySalesDrilldown(busSalesEnqSrc, args);
                ReturnCurrentView(ref actuallist);

                oSalesInfoDetailViewState.TargetInfoEntityList = actuallist;

                return oSalesInfoDetailViewState;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetParentOriginatorsList(string sOriginator)
        {
            try
            {
                return OriginatorDataService.GetParentOriginators(sOriginator);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEMEIEntity> GetAllOriginatorEMEINos(string sOriginator)
        {
            try
            {
                return OriginatorDataService.GetAllOriginatorEMEINos(sOriginator);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateRepEMEI(string accessToken, string emei)
        {
            DbWorkflowScope transactionScope = OriginatorDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();
                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                if (originator == null)
                {
                    transactionScope.Rollback();
                    return false;
                }

                isSuccess = originatorService.UpdateRepEMEI(originator.Originator, emei);
            }
            catch
            {
                throw;
            }

            return isSuccess;
        }

        public bool UpdateRepsSyncedStatus(string RepCode, string emei, bool IsSync, string ApkVersion)
        {
            DbWorkflowScope transactionScope = OriginatorDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();
                isSuccess = originatorService.UpdateRepsSyncedStatus(RepCode, emei, IsSync, ApkVersion);
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }
        public bool UpdateRepsSyncData(string Originator, string OriginatorType)
        {
            DbWorkflowScope transactionScope = OriginatorDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();
                //isSuccess = originatorService.UpdateRepsSyncedStatus(RepCode, emei, IsSync, ApkVersion);
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool UpdateDeviceIdByEmei(string accessToken, string emei, string DeviceId)
        {
            DbWorkflowScope transactionScope = OriginatorDataService.WorkflowScope;
            bool isSuccess = false;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();
                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                if (originator == null)
                {
                    transactionScope.Rollback();
                    return false;
                }

                isSuccess = originatorService.UpdateDeviceIdByEmei(originator.Originator, emei, DeviceId);
            }
            catch
            {
                throw;
            }

            return isSuccess;
        }

        public bool InsertRepLocation(string accessToken, string CurrLat, string CurrLong)
        {
            DbWorkflowScope transactionScope = OriginatorDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();
                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                if (originator == null)
                {
                    transactionScope.Rollback();
                    return false;
                }

                isSuccess = originatorService.InsertRepLocation(originator.Originator, CurrLat, CurrLong);
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public OriginatorEntity GetOriginatorByAccessToken(string accessToken)
        {
            try
            {
                return OriginatorDataService.GetOriginatorByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
        }

        public string GetNewPinByAccessToken(string accessToken)
        {
            try
            {
                return OriginatorDataService.GetNewPinByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
        }

        public OriginatorEntity GetOriginatorRouteByAccessToken(string accessToken)
        {
            try
            {
                return OriginatorDataService.GetOriginatorRouteByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
        }

        public bool PushNotifyGetSyncDetails()
        {
            try
            {
                return OriginatorDataService.PushNotifyGetSyncDetails();
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetLevelThreeParentOriginators(string originator)
        {
            try
            {
                return OriginatorDataService.GetLevelThreeParentOriginators(originator);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<RepGeoLocation> GetRepLocationsByOriginator(string originator)
        {
            try
            {
                return OriginatorDataService.GetRepLocationsByOriginator(originator);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UpdatePassword(string originator, string password)
        {
            try
            {
                return OriginatorDataService.UpdatePassword(originator, password);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateMobilePin()
        {
            try
            {
                return OriginatorDataService.UpdateMobilePin();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        public bool DeleteEMEIFromOriginator(string originator, string emei_no)
        {
            try
            {
                return OriginatorDataService.DeleteEMEIFromOriginator(originator, emei_no);
            }
            catch
            {
                throw;
            }
        }

        public bool ClearDeviceEMEIFromOriginator(string originator, string emei_no)
        {
            try
            {
                return OriginatorDataService.ClearDeviceEMEIFromOriginator(originator, emei_no);
            }
            catch
            {
                throw;
            }
        }

        public string InsertRepEMEI(string userName, string emei)
        {
            try
            {
                return OriginatorDataService.InsertRepEMEI(userName, emei);
            }
            catch
            {
                throw;
            }
        }

        //for originator
        public List<OriginatorEntity> GetOriginators(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetOriginators(args);
            }
            catch
            {
                throw;
            }

        }
        
        public List<OriginatorEntity> GetOriginatorsFor2FA(ArgsEntity args)
        {
            try
            {
                return OriginatorDataService.GetOriginatorsFor2FA(args);
            }
            catch
            {
                throw;
            }

        }

        public bool UpdateIsTFAUser(string originator, bool isTFA)
        {
            try
            {
                return OriginatorDataService.UpdateIsTFAUser(originator, isTFA);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateIsTFAAuth(string originator, bool isTFAAuth)
        {
            try
            {
                return OriginatorDataService.UpdateIsTFAAuth(originator, isTFAAuth);
            }
            catch
            {
                throw;
            }
        }


        //for adding new user
        public string addNewUser(string originator, string userName, string designation, string password, string userType, string mobile)
        {
            try
            {
                return OriginatorDataService.addNewUser(originator, userName, designation, password, userType, mobile);
            }
            catch
            {
                throw;
            }
        }

        //for update originator
        public string updateUser(string originator, string userName, string designation, string password, string userType, int UserID, string mobile)
        {
            try
            {
                return OriginatorDataService.updateUser(originator, userName, designation, password, userType, UserID, mobile);
            }
            catch
            {
                throw;
            }
        }
       
        //for deleting user
        public string deleteUser(int UserId)
        {
            try
            {
                return OriginatorDataService.deleteUser(UserId);
            }
            catch
            {
                throw;
            }

        }


       
        // originator types for dropdown
        public List<OriginatorEntity> getAllOriginatorTypesForDropdown()
        {
            try
            {
                return OriginatorDataService.getAllOriginatorTypesForDropdown();
            }
            catch
            {
                throw;
            }
        }

        // originator types for dropdown
        public List<SalesDataEntity> getRepsLiveSales(string originator)
        {
            try
            {
                return OriginatorDataService.getRepsLiveSales(originator);
            }
            catch
            {
                throw;
            }
        }

        public string InsertDailyRepInvoiceCount(string datetime, string repcode, string emei, int invoicecount, int delinvoicecount)
        {
            try
            {
                return OriginatorDataService.InsertDailyRepInvoiceCount(datetime, repcode, emei, invoicecount, delinvoicecount);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateOriginatorVisitDetail(string accessToken, OrigivatorVisitModel visitDet)
        {
            bool isSuccess = false;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();

                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                if (originator == null)
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(visitDet.fromOriginator))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(visitDet.toOriginator))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(visitDet.cinVisitTime))
                {
                    return false;
                }

                //Insert new Order 
                isSuccess = OriginatorDataService.UpdateOriginatorVisitDetail(originator.Originator, visitDet);
                if (isSuccess)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }
        
        public bool UpdateOriginatorAttendanceCheckIn(string accessToken, OriginatorAttendance attendanceDet)
        {
            bool isSuccess = false;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();

                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                if (originator == null)
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(attendanceDet.checkInTime))
                {
                    return false;
                }

                attendanceDet.originator = originator.Originator;
                //Insert new Order 
                isSuccess = OriginatorDataService.UpdateOriginatorAttendanceCheckIn(attendanceDet);

                if (isSuccess)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        public bool UpdateOriginatorAttendanceCheckOut(string accessToken, OriginatorAttendance attendanceDet)
        {
            bool isSuccess = false;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();

                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                if (originator == null)
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(attendanceDet.checkOutTime))
                {
                    return false;
                }

                attendanceDet.originator = originator.Originator;
                //Insert new Order 
                isSuccess = OriginatorDataService.UpdateOriginatorAttendanceCheckOut(attendanceDet);

                if (isSuccess)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        public List<OrigivatorVisitModel> GetAllASMVisits(string originator)
        {
            try
            {
                return OriginatorDataService.GetAllASMVisits(originator);
            }
            catch
            {
                throw;
            }
        }

        #region "3rd Phase"

        public List<SalesRepAttendanceHoursModel> GetRepsAttendanceHours(string originator, string date)
        {
            try
            {
                return OriginatorDataService.GetRepsAttendanceHours(originator, date);
            }
            catch
            {
                throw;
            }
        }
        
        public List<AttendanceReasons> GetAttendanceReasons()
        {
            try
            {
                return OriginatorDataService.GetAttendanceReasons();
            }
            catch
            {
                throw;
            }
        }

        public bool InsertAttendanceReason(AttendanceReasons attendanceDet)
        {
            bool isSuccess = false;

            try
            {
                if (attendanceDet.id == 0)
                    isSuccess = OriginatorDataService.InsertAttendanceReason(attendanceDet);
                else
                    isSuccess = OriginatorDataService.UpdateAttendanceReason(attendanceDet);

                if (isSuccess)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        public bool InsertOriginatorAttendanceReason(OriginatorAttendanceReasons attendanceDet)
        {
            bool isSuccess = false;

            try
            {
                isSuccess = OriginatorDataService.InsertOriginatorAttendanceReason(attendanceDet);

                if (isSuccess)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        public bool deleteAttendanceReason(string originator, int id)
        {
            try
            {
                return OriginatorDataService.deleteAttendanceReason(originator, id);
            }
            catch
            {
                throw;
            }

        }
        public List<DashboardAttendanceByRepType> GetDashboardAttendanceByRepType(string originator, string date)
        {
            try
            {
                return OriginatorDataService.GetDashboardAttendanceByRepType(originator, date);
            }
            catch
            {
                throw;
            }
        }

        public List<DashboardAttendanceDetails> GetDashboardAttendanceDetails(string originator, string date, string salesType, string attendType)
        {
            try
            {
                return OriginatorDataService.GetDashboardAttendanceDetails(originator, date, salesType, attendType);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateOriginatorTFAAuthenticationSettings(string originator, string tfaPin, bool isAuthenticated)
        {
            return OriginatorDataService.UpdateOriginatorTFAAuthenticationSettings(originator, tfaPin, isAuthenticated);
        }

        #endregion


    }
}
