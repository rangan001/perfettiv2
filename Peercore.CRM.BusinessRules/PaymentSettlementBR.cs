﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class PaymentSettlementBR
    {
        private static PaymentSettlementBR instance = null;
        private static object syncLock = new Object();

        private PaymentSettlementDAO PaymentSettlementDataService;

        public static PaymentSettlementBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new PaymentSettlementBR();
                }
                return instance;
            }
        }

        private PaymentSettlementBR()
        {
            PaymentSettlementDataService = new PaymentSettlementDAO();
        }

        #region - Init Object -

        public PaymentSettlementEntity CreateObject()
        {
            return PaymentSettlementDataService.CreateObject();
        }

        public PaymentSettlementEntity CreateObject(int entityId)
        {
            return PaymentSettlementDataService.CreateObject(entityId);
        }

        #endregion

        public List<PaymentSettlementEntity> GetChequePaymentSettlements(string createdBy, DateTime createdDate, DateTime toDate, int distributerAccountId, string status, ArgsEntity args)
        {
            try
            {
                if (status.Equals("All"))
                    return PaymentSettlementDataService.GetChequePaymentSettlements(createdBy, createdDate, toDate, distributerAccountId, null, args);
                else
                    return PaymentSettlementDataService.GetChequePaymentSettlements(createdBy, createdDate, toDate, distributerAccountId, status, args);
            }
            catch
            {
                throw;
            }
        }

        public bool SavePaymentSettlements(List<PaymentSettlementEntity> paymentSettlementList)
        {
            DbWorkflowScope transactionScope = PaymentSettlementDataService.WorkflowScope;
            CustomerDAO customerDAO = new CustomerDAO(transactionScope);
            bool isSuccess = true;
            try
            {
                if (paymentSettlementList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (PaymentSettlementEntity paymentSettlementEntity in paymentSettlementList)
                        {
                            if (paymentSettlementEntity.PaymentSettlementId > 0)
                            {
                                isSuccess = PaymentSettlementDataService.UpdatePaymentSettlement(paymentSettlementEntity);
                                //reducing returned cheque amount from outstanding balance
                                customerDAO.DeduceOutstandingBalance(paymentSettlementEntity.CustCode, paymentSettlementEntity.Amount, paymentSettlementEntity.LastModifiedBy);
                            }
                            else
                            {
                                isSuccess = PaymentSettlementDataService.InsertPaymentSettlement(paymentSettlementEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }


        public bool SavePaymentSettlementsForMobile(PaymentSettlementEntity paymentSettlementEntity)
        {
            CustomerDAO customerDAO = new CustomerDAO();
            DbWorkflowScope transactionScope = PaymentSettlementDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                    using (transactionScope)
                    {

                            if (paymentSettlementEntity.CashAmount > 0)
                            {
                                paymentSettlementEntity.Amount = paymentSettlementEntity.CashAmount;
                                paymentSettlementEntity.PaymentType = "CASH";

                                isSuccess = PaymentSettlementDataService.InsertPaymentSettlement(paymentSettlementEntity);
                            }

                            if (paymentSettlementEntity.ChequeAmount > 0) 
                            {
                                paymentSettlementEntity.Amount = paymentSettlementEntity.CashAmount;
                                paymentSettlementEntity.PaymentType = "CHQE";
                                paymentSettlementEntity.Status = "REAL";

                                isSuccess = PaymentSettlementDataService.InsertPaymentSettlement(paymentSettlementEntity);
                            }
                            
                        

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            
            catch
            {
                throw;
            }
            return isSuccess;
        }


        public List<PaymentSettlementEntity> GetDatedChequePaymentSettlements(string createdBy, DateTime createdDate, DateTime toDate, ArgsEntity args)
        {
            try
            {
                return PaymentSettlementDataService.GetDatedChequePaymentSettlements(createdBy, createdDate, toDate, args);
            }
            catch
            {
                throw;
            }
        }

    }
}
