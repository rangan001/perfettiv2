﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using System.Data;

namespace Peercore.CRM.BusinessRules
{
    public class PipelineStageBR
    {
        private static PipelineStageBR instance = null;
        private static object syncLock = new Object();
        private PipelineStageDAO  pipelineStageDataService;

        public static PipelineStageBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new PipelineStageBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public PipelineStageEntity CreateObject()
        {
            return pipelineStageDataService.CreateObject();
        }

        public PipelineStageEntity CreateObject(int entityId)
        {
            return pipelineStageDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private PipelineStageBR()
        {
            pipelineStageDataService = new PipelineStageDAO();
        }
        #endregion

        #region - Public Methods -

        //public void GetPipelineStages(ref DataTable dtPipelineStage, string defaultDeptID)
        //{
        //    try
        //    {

        //        pipelineStageDataService.GetPipelineStages(ref dtPipelineStage, defaultDeptID);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public List<PipelineStageEntity> GetPipelineStages(string defaultDeptID)
        {
            try
            {
                return pipelineStageDataService.GetPipelineStages(defaultDeptID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PipelineStageEntity> GetPipelineChart(ArgsEntity argsEntity)
        {
            try
            {
                LeadStageDAO leadStageDAO = new LeadStageDAO();
               
                LeadStageEntity leadStages = leadStageDAO.GetLeadStages(argsEntity).OrderByDescending(s => s.StageOrder).FirstOrDefault();

                bool includeCustomers = true;

                if (leadStages != null)
                {
                    includeCustomers = leadStages.OpportunityAllowed;
                }

                return pipelineStageDataService.GetPipelineChart(includeCustomers,argsEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool Save(List<PipelineStageEntity> pipelineStageList, List<int> deletedStageList, ArgsEntity argsEntity)
        {
            DbWorkflowScope transactionScope = pipelineStageDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    foreach (PipelineStageEntity pipelineStageEntity in pipelineStageList)
                    {
                        if (pipelineStageEntity.PipelineStageID == 0)
                        {
                            pipelineStageEntity.PipelineStageID = pipelineStageDataService.GetNextPipelineStageId();
                            isSuccess = pipelineStageDataService.InsertPipelineStage(pipelineStageEntity, argsEntity);
                        }
                        else
                        {
                            isSuccess = pipelineStageDataService.UpdatePipelineStage(pipelineStageEntity);
                        }

                        
                        if (!isSuccess)
                            break;
                    }

                    if (isSuccess) 
                    {
                        foreach (int pipelineStageID in deletedStageList)
                        {

                            isSuccess = pipelineStageDataService.DeletePipelineStage(pipelineStageID);

                            if (!isSuccess)
                                break;
                        }

                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool CanDelete(int pipelineStageId)
        {
            try
            {
                return pipelineStageDataService.CanDelete(new KeyValuePair<string, object>("pipeline_stage_id", pipelineStageId),"crm_opportunity");
            }
            catch
            {
                throw;
            }
        }

        #endregion
       
    }
}
