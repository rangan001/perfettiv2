﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;


namespace Peercore.CRM.BusinessRules
{
    public class ProductBR
    {
        private static ProductBR instance = null;
        private static object syncLock = new Object();

        private ProductDAO ProductDataService;

        public static ProductBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ProductBR();
                }
                return instance;
            }
        }

        private ProductBR()
        {
            ProductDataService = new ProductDAO();
        }

        #region - Init Object -

        public ProductEntity CreateObject()
        {
            return ProductDataService.CreateObject();
        }

        public ProductEntity CreateObject(int entityId)
        {
            return ProductDataService.CreateObject(entityId);
        }

        #endregion

        public List<ProductEntity> GetCustomerProducts(ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetCustomerProducts(args);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetAllProducts(ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetAllProducts(args);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetAllProductsWithOutImage(ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetAllProductsWithOutImage(args);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetAllProductsWithOutImageForMaster(ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetAllProductsWithOutImageForMaster(args);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetAllProductsForCustomer(string custcode)
        {
            try
            {
                return ProductDataService.GetAllProductsForCustomer(custcode);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateCustomerProductStatus(string custCode, int productId)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {

                    isSuccess = ProductDataService.UpdateCustomerProductStatus(custCode, productId);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<ProductEntity> GetProductsForInvoice()
        {
            try
            {
                return ProductDataService.GetProductsForInvoice();
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetProductsByName(string productNameLike)
        {
            try
            {
                return ProductDataService.GetProductsByName(productNameLike);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductCategoryEntity> GetAllProductCategories(ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetAllProductCategories(args);
            }
            catch
            {
                throw;
            }
        }


        public bool SaveProduct(ProductEntity productEntity)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {

                using (transactionScope)
                {

                    if (productEntity.ProductId > 0)
                    {
                        isSuccess = ProductDataService.UpdateProduct(productEntity);
                    }
                    else
                    {
                        isSuccess = ProductDataService.InsertProducts(productEntity);
                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }

            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool InactiveProduct(ProductEntity productEntity)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    if (productEntity.ProductId > 0)
                    {
                        isSuccess = ProductDataService.InactiveProduct(productEntity);
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool IsProductCodeExists(ProductEntity productEntity)
        {
            try
            {
                return ProductDataService.IsProductCodeExists(productEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool IsProductNameExists(ProductEntity productEntity)
        {
            try
            {
                return ProductDataService.IsProductNameExists(productEntity);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetAllProductsWithNoPrice(ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetAllProductsWithNoPrice(args);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetAllProductPricesDataAndCount(ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetAllProductPricesDataAndCount(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveProductPrice(List<ProductEntity> productEntityList)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (productEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (ProductEntity productEntity in productEntityList)
                        {
                            if (productEntity.ProductPriceId > 0)
                            {
                                isSuccess = ProductDataService.DeleteProductPrice(productEntity);
                                isSuccess = ProductDataService.InsertProductPrice(productEntity);
                            }
                            else
                            {
                                isSuccess = ProductDataService.InsertProductPrice(productEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool InsertProductFreshness(List<MobileCustomerProductEntity> productEntityList)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (productEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (MobileCustomerProductEntity productEntity in productEntityList)
                        {

                            isSuccess = ProductDataService.InsertProductFreshness(productEntity);

                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool InsertCustomerProductslog(List<MobileCustomerProductEntity> mobileCustomerProductEntityList)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            ProductDAO productDAO = new ProductDAO(transactionScope);
            bool isSuccess = false;
            try
            {
                if (mobileCustomerProductEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        isSuccess = productDAO.DeleteAllCustomerProductsLogForToday(mobileCustomerProductEntityList[0]);
                        foreach (MobileCustomerProductEntity mobileCustomerProductEntity in mobileCustomerProductEntityList)
                        {
                            isSuccess = productDAO.InsertCustomerProductslog(mobileCustomerProductEntity);
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<MobileCustomerProductEntity> GetCustomerProductsForMostRecentDate(string customerCode)
        {
            try
            {
                return ProductDataService.GetCustomerProductsForMostRecentDate(customerCode);
            }
            catch
            {
                throw;
            }
        }

        public bool InsertProductDivisions(int divisionId, List<ProductEntity> productEntityList)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (productEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        ProductDataService.DeleteProductDivisionsByDivisionId(divisionId);

                        foreach (ProductEntity productEntity in productEntityList)
                        {
                            isSuccess = ProductDataService.InsertProductDivisions(productEntity);
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool InsertProductForDivision(int divisionId, ProductEntity productEntity)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (productEntity != null)
                {
                    using (transactionScope)
                    {
                        // ProductDataService.DeleteProductDivisionsByDivisionId(divisionId);

                        isSuccess = ProductDataService.InsertProductDivisions(productEntity);

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<ProductEntity> GetProductsByDivisionId(int divisionId, ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetProductsByDivisionId(divisionId, args);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetProductsByAccessToken(string accessToken, string syncDate)
        {
            try
            {
                return ProductDataService.GetProductsByAccessToken(accessToken, syncDate);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductEntity> GetProductsForModernTradeByAccessToken(string accessToken, string syncDate)
        {
            try
            {
                return ProductDataService.GetProductsForModernTradeByAccessToken(accessToken, syncDate);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductCategoryEntity> GetProductCategoriesByAccessToken(string accessToken)
        {
            try
            {
                return ProductDataService.GetProductCategoriesByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
        }

        public byte[] GetProductsByAccessTokenAndId(string accessToken, int productId)
        {
            try
            {
                return ProductDataService.GetProductsByAccessTokenAndId(accessToken, productId);
            }
            catch
            {
                throw;
            }
        }

        public ProductEntity GetProductById(int productId)
        {
            try
            {
                return ProductDataService.GetProductById(productId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteProductDevision(int divisionId, int prodId)
        {
            try
            {
                return ProductDataService.DeleteProductDivisionsByProductId(divisionId, prodId);
            }
            catch
            {
                throw;
            }
        }

        public int IsProductExsistInDiscountScheme(int product_id)
        {
            try
            {
                return ProductDataService.IsProductExsistInDiscountScheme(product_id);
            }
            catch
            {
                throw;
            }
        }

        #region "Perfetti 2nd Phase"

        public ProductModel GetProductByProductCode(string productCode)
        {
            ProductDAO ProductService = new ProductDAO();

            try
            {
                ProductModel product = ProductService.GetProductByProductCode(productCode);

                return product;
            }
            catch
            {
                throw;
            }
        }

        public List<ProductModel> GetAllProducts(ArgsModel args)
        {
            try
            {
                return ProductDataService.GetAllProducts(args);
            }
            catch
            {
                throw;
            }
        }

        #region "Territory Products"
        public List<ProductModel> GetAllProductsByTerritoryId(ArgsModel args, string TerritoryId)
        {
            try
            {
                return ProductDataService.GetAllProductsByTerritoryId(args, TerritoryId);
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoriesByProductId(ArgsModel args, string ProductId)
        {
            try
            {
                return ProductDataService.GetAllTerritoriesByProductId(args, ProductId);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveUpdateTerritoryProducts(string originator,
                                                       string territory_id,
                                                       string product_id,
                                                       string is_checked)
        {
            try
            {
                return ProductDataService.SaveUpdateTerritoryProducts(originator,
                                                       territory_id,
                                                       product_id,
                                                       is_checked);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #endregion

        //public List<ProductUOMModel> GetProductUOMByAccessToken(string accessToken)
        //{
        //    try
        //    {
        //        return ProductDataService.GetProductUOMByAccessToken(accessToken);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}



        //Added By Rangan  
        public List<ProductUOMViewModel> GetProductUOMByAccessToken(string accessToken)
        {
            try
            {
                List<ProductUOMModel> productModelList = ProductDataService.GetProductUOMByAccessToken(accessToken);

                var groupedUOMs = productModelList
                    .GroupBy(uom => new { uom.ProductId, uom.FgCode, uom.Code })
                    .Select(group => new ProductUOMViewModel
                    {
                        ProductId = group.Key.ProductId,
                        FgCode = group.Key.FgCode,
                        Code = group.Key.Code,
                        CassesConversion = group.Any(uom => uom.Name == "Casses") ?
                            group.First(uom => uom.Name == "Casses").Conversion : 0,
                        TonnageConversion = group.Any(uom => uom.Name == "Tonnage") ?
                            group.First(uom => uom.Name == "Tonnage").Conversion : 0
                    })
                    .ToList();

                return groupedUOMs;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public List<ProductECOModel> GetProductECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            try
            {
                return ProductDataService.GetProductECOCumulativeByAccessToken(accessToken, strDate, endDate);
            }
            catch
            {
                throw;
            }
        }




    }
}
