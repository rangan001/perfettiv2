﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;


namespace Peercore.CRM.BusinessRules
{
    public class ProductCategoryBR
    {
        private static ProductCategoryBR instance = null;
        private static object syncLock = new Object();

        private ProductCategoryDAO ProductDataService;

        public static ProductCategoryBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ProductCategoryBR();
                }
                return instance;
            }
        }

        private ProductCategoryBR()
        {
            ProductDataService = new ProductCategoryDAO();
        }

        #region - Init Object -

        public ProductCategoryEntity CreateObject()
        {
            return ProductDataService.CreateObject();
        }

        public ProductCategoryEntity CreateObject(int entityId)
        {
            return ProductDataService.CreateObject(entityId);
        }

        #endregion

        public bool IsProductCategoryCodeExists(ProductCategoryEntity productcategoryEntity)
        {
            try
            {
                return ProductDataService.IsProductCategoryCodeExists(productcategoryEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool IsProductCategoryNameExists(ProductCategoryEntity productcategoryEntity)
        {
            try
            {
                return ProductDataService.IsProductCategoryNameExists(productcategoryEntity);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductCategoryEntity> GetAllProductCategoryDataAndCount(ArgsEntity args)
        {
            try
            {
                return ProductDataService.GetAllProductCategory(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveProductCategory(List<ProductCategoryEntity> ProductCategoryEntityList)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (ProductCategoryEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (ProductCategoryEntity productcategoryEntity in ProductCategoryEntityList)
                        {
                            if (productcategoryEntity.Id > 0)
                            {
                                isSuccess = ProductDataService.UpdateProductCategory(productcategoryEntity);
                            }
                            else
                            {
                                isSuccess = ProductDataService.InsertProductCategory(productcategoryEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteProductCategory(ProductCategoryEntity tmeProductCategory)
        {
            DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
            bool success = false;

            try
            {
                using (transactionScope)
                {
                    ProductCategoryDAO productcategoryDataService = new ProductCategoryDAO(transactionScope);

                    success = productcategoryDataService.DeleteProductCategory(tmeProductCategory);

                    if (success)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return success;
        }
        public List<CategoryECOModel> GetCategoryECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            try
            {
                return ProductDataService.GetCategoryECOCumulativeByAccessToken(accessToken, strDate, endDate);
            }
            catch
            {
                throw;
            }
        }


        //public List<ProductEntity> GetAllProductsForCustomer(string custcode)
        //{
        //    try
        //    {
        //        return ProductDataService.GetAllProductsForCustomer(custcode);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public bool UpdateCustomerProductStatus(string custCode, int productId)
        //{
        //    DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        using (transactionScope)
        //        {

        //            isSuccess = ProductDataService.UpdateCustomerProductStatus(custCode,productId);

        //            if (isSuccess)
        //                transactionScope.Commit();
        //            else
        //                transactionScope.Rollback();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public List<ProductEntity> GetProductsForInvoice()
        //{
        //    try
        //    {
        //        return ProductDataService.GetProductsForInvoice();
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<ProductEntity> GetProductsByName(string productNameLike)
        //{
        //    try
        //    {
        //        return ProductDataService.GetProductsByName(productNameLike);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        ////////public List<ProductCategoryEntity> GetAllProductCategories(ArgsEntity args)
        ////////{
        ////////    try
        ////////    {
        ////////        return ProductDataService.GetAllProductCategory(args);
        ////////    }
        ////////    catch
        ////////    {
        ////////        throw;
        ////////    }
        ////////}


        //public bool SaveProduct(ProductEntity productEntity)
        //{
        //    DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {

        //            using (transactionScope)
        //            {

        //                if (productEntity.ProductId > 0)
        //                    {
        //                        isSuccess = ProductDataService.UpdateProduct(productEntity);
        //                    }
        //                    else
        //                    {
        //                        isSuccess = ProductDataService.InsertProducts(productEntity);
        //                    }


        //                if (isSuccess)
        //                    transactionScope.Commit();
        //                else
        //                    transactionScope.Rollback();
        //            }

        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}



        //public bool IsProductCodeExists(ProductEntity productEntity)
        //{
        //    try
        //    {
        //        return ProductDataService.IsProductCodeExists(productEntity);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public bool IsProductNameExists(ProductEntity productEntity)
        //{
        //    try
        //    {
        //        return ProductDataService.IsProductNameExists(productEntity);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<ProductEntity> GetAllProductsWithNoPrice(ArgsEntity args)
        //{
        //    try
        //    {
        //        return ProductDataService.GetAllProductsWithNoPrice(args);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<ProductEntity> GetAllProductPricesDataAndCount(ArgsEntity args)
        //{
        //    try
        //    {
        //        return ProductDataService.GetAllProductPricesDataAndCount(args);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public bool SaveProductPrice(List<ProductEntity> productEntityList)
        //{
        //    DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        if (productEntityList.Count != 0)
        //        {
        //            using (transactionScope)
        //            {
        //                foreach (ProductEntity productEntity in productEntityList)
        //                {
        //                    if (productEntity.ProductPriceId > 0)
        //                    {
        //                        isSuccess = ProductDataService.DeleteProductPrice(productEntity);
        //                        isSuccess = ProductDataService.InsertProductPrice(productEntity);
        //                    }
        //                    else
        //                    {
        //                        isSuccess = ProductDataService.InsertProductPrice(productEntity);
        //                    }
        //                }

        //                if (isSuccess)
        //                    transactionScope.Commit();
        //                else
        //                    transactionScope.Rollback();
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public bool InsertProductFreshness(List<MobileCustomerProductEntity> productEntityList)
        //{
        //    DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        if (productEntityList.Count != 0)
        //        {
        //            using (transactionScope)
        //            {
        //                foreach (MobileCustomerProductEntity productEntity in productEntityList)
        //                {

        //                    isSuccess = ProductDataService.InsertProductFreshness(productEntity);

        //                }

        //                if (isSuccess)
        //                    transactionScope.Commit();
        //                else
        //                    transactionScope.Rollback();
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public bool InsertCustomerProductslog(List<MobileCustomerProductEntity> mobileCustomerProductEntityList)
        //{
        //    DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
        //    ProductDAO productDAO = new ProductDAO(transactionScope);
        //    bool isSuccess = false;
        //    try
        //    {
        //        if (mobileCustomerProductEntityList.Count != 0)
        //        {
        //            using (transactionScope)
        //            {
        //                isSuccess = productDAO.DeleteAllCustomerProductsLogForToday(mobileCustomerProductEntityList[0]);
        //                foreach (MobileCustomerProductEntity mobileCustomerProductEntity in mobileCustomerProductEntityList)
        //                {
        //                    isSuccess = productDAO.InsertCustomerProductslog(mobileCustomerProductEntity);
        //                }

        //                if (isSuccess)
        //                    transactionScope.Commit();
        //                else
        //                    transactionScope.Rollback();
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public List<MobileCustomerProductEntity> GetCustomerProductsForMostRecentDate(string customerCode)
        //{
        //    try
        //    {
        //        return ProductDataService.GetCustomerProductsForMostRecentDate(customerCode);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public bool InsertProductDivisions(int divisionId , List<ProductEntity> productEntityList)
        //{
        //    DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        if (productEntityList.Count != 0)
        //        {
        //            using (transactionScope)
        //            {
        //                ProductDataService.DeleteProductDivisionsByDivisionId(divisionId);

        //                foreach (ProductEntity productEntity in productEntityList)
        //                {
        //                    isSuccess = ProductDataService.InsertProductDivisions(productEntity);
        //                }

        //                if (isSuccess)
        //                    transactionScope.Commit();
        //                else
        //                    transactionScope.Rollback();
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public bool InsertProductForDivision(int divisionId, ProductEntity productEntity)
        //{
        //    DbWorkflowScope transactionScope = ProductDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        if (productEntity!=null)
        //        {
        //            using (transactionScope)
        //            {
        //               // ProductDataService.DeleteProductDivisionsByDivisionId(divisionId);

        //                isSuccess = ProductDataService.InsertProductDivisions(productEntity);

        //                if (isSuccess)
        //                    transactionScope.Commit();
        //                else
        //                    transactionScope.Rollback();
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public List<ProductEntity> GetProductsByDivisionId(int divisionId, ArgsEntity args)
        //{
        //    try
        //    {
        //        return ProductDataService.GetProductsByDivisionId(divisionId,args);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<ProductEntity> GetProductsByAccessToken(string accessToken)
        //{
        //    try
        //    {
        //        return ProductDataService.GetProductsByAccessToken(accessToken);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<ProductCategoryEntity> GetProductCategoriesByAccessToken(string accessToken)
        //{
        //    try
        //    {
        //        return ProductDataService.GetProductCategoriesByAccessToken(accessToken);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public byte[] GetProductsByAccessTokenAndId(string accessToken, int productId)
        //{
        //    try
        //    {
        //        return ProductDataService.GetProductsByAccessTokenAndId(accessToken, productId);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public ProductEntity GetProductById(int productId)
        //{
        //    try
        //    {
        //        return ProductDataService.GetProductById(productId);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public bool DeleteProductDevision(int divisionId, int prodId)
        //{
        //    try
        //    {
        //        return ProductDataService.DeleteProductDivisionsByProductId(divisionId, prodId);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
    }
}
