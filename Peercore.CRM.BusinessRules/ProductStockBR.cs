﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class ProductStockBR
    {
        public ProductStockModel GetAllProductStockByTerritoryId(ArgsModel args, DateTime allocDate, int territoryId)
        {
            try
            {
                ProductStockModel loadStockEntity = LoadStockService.GetAllProductStockByTerritoryId(args, allocDate, territoryId);
                //foreach (LoadStockDetailModel item in loadStockEntity.LstLoadStockDetail)
                //{
                //    item.StockHeaderId = (LoadStockService.GetStockHeaderDetail(allocDate, 0).StockId);
                //}
                return loadStockEntity;
            }
            catch
            {
                throw;
            }
        }
    }
}
