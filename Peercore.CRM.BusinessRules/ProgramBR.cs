﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ProgramBR
    {
        private static ProgramBR instance = null;
        private static object syncLock = new Object();

        private ProgramDAO ProgramDataService;

        public static ProgramBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ProgramBR();
                }
                return instance;
            }
        }

        private ProgramBR()
        {
            ProgramDataService = new ProgramDAO();
        }

        #region - Init Object -

        public ProgramEntity CreateObject()
        {
            return ProgramDataService.CreateObject();
        }

        public ProgramEntity CreateObject(int entityId)
        {
            return ProgramDataService.CreateObject(entityId);
        }
        #endregion

        public List<ProgramTargetsEntity> GetProgramTargetsByProgram(string programNameLike, int programTargetId, int programId)
        {
            try
            {
                return ProgramDataService.GetProgramTargetsByProgram(programNameLike, programTargetId, programId);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveProgramTargets(List<ProgramTargetsEntity> programTargetList)
        {
            DbWorkflowScope transactionScope = ProgramDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (programTargetList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (ProgramTargetsEntity programTargetsEntity in programTargetList)
                        {
                            if (programTargetsEntity.ProgramTargetId > 0)
                            {
                                isSuccess = ProgramDataService.UpdateProgramTarget(programTargetsEntity);
                            }
                            else
                            {
                                isSuccess = ProgramDataService.InsertProgramTarget(programTargetsEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool SaveProgram(ProgramEntity program)
        {
            DbWorkflowScope transactionScope = ProgramDataService.WorkflowScope;
            bool isSuccess = false;
            bool isSuccess2 = false;
            ProgramCustomerEntity programCustomer;

            int progId = 0;
            try
            {
                if (program != null)
                {
                    using (transactionScope)
                    {

                        if (program.ProgramId > 0)
                        {
                            isSuccess = ProgramDataService.UpdateProgram(program);
                            isSuccess2 = ProgramDataService.DeleteProgramAllCustomers(program.ProgramId);
                            if (program.SelectionType == "V")
                            {
                                foreach (int volume in program.VolumeList)
                                {
                                    programCustomer = ProgramCustomerEntity.CreateObject();

                                    programCustomer.ProgramId = program.ProgramId;
                                    programCustomer.Volume = volume;
                                    programCustomer.SelectionType = program.SelectionType;
                                    programCustomer.CreatedBy = program.CreatedBy;

                                    isSuccess = ProgramDataService.InsertProgramCustomers(programCustomer);

                                }
                            }

                            else if (program.SelectionType == "R")
                            {
                                foreach (string custcode in program.CustomerList)
                                {
                                    programCustomer = ProgramCustomerEntity.CreateObject();

                                    programCustomer.ProgramId = program.ProgramId;
                                    programCustomer.CustCode = custcode;
                                    programCustomer.SelectionType = program.SelectionType;
                                    programCustomer.CreatedBy = program.CreatedBy;

                                    isSuccess = ProgramDataService.InsertProgramCustomers(programCustomer);

                                }
                            }
                        }
                        else
                        {
                            progId = ProgramDataService.InsertProgram(program);
                            if (progId > 0 && program.SelectionType == "V")
                            {
                                foreach (int volume in program.VolumeList)
                                {
                                    programCustomer = ProgramCustomerEntity.CreateObject();

                                    programCustomer.ProgramId = progId;
                                    programCustomer.Volume = volume;
                                    programCustomer.SelectionType = program.SelectionType;
                                    programCustomer.CreatedBy = program.CreatedBy;

                                    isSuccess = isSuccess2 = ProgramDataService.InsertProgramCustomers(programCustomer);

                                }
                            }

                            else if (progId > 0 && program.SelectionType == "R")
                            {
                                foreach (string custcode in program.CustomerList)
                                {
                                    programCustomer = ProgramCustomerEntity.CreateObject();

                                    programCustomer.ProgramId = progId;
                                    programCustomer.CustCode = custcode;
                                    programCustomer.SelectionType = program.SelectionType;
                                    programCustomer.CreatedBy = program.CreatedBy;

                                    isSuccess = isSuccess2 =ProgramDataService.InsertProgramCustomers(programCustomer);

                                }
                            }
                        }

                        if (isSuccess && isSuccess2)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteProgram(int programId)
        {
            DbWorkflowScope transactionScope = ProgramDataService.WorkflowScope;
            bool isSuccess = false;
            bool isSuccess2 = false;
            ProgramCustomerEntity programCustomer;


            try
            {
                using (transactionScope)
                {

                    isSuccess = ProgramDataService.DeleteProgram(programId);
                    isSuccess2 = ProgramDataService.DeleteProgramAllCustomers(programId);


                    if (isSuccess && isSuccess2)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();

                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public ProgramEntity GetProgramDetailsById(int programId)
        {
            List<ProgramCustomerEntity> progCustList = new List<ProgramCustomerEntity>();
           // List<ProgramEntity> programList = new List<ProgramEntity>();
            ProgramEntity program = null;

            try
            {
                progCustList = ProgramDataService.GetProgramDetailsById(programId);
                
                if (progCustList != null)
                {
                    if (progCustList.Count > 0)
                    {
                        program = ProgramEntity.CreateObject();

                        program.ProgramName = progCustList[0].ProgramName;
                        program.ProgramId = progCustList[0].ProgramId;
                        program.SelectionType = progCustList[0].SelectionType;
                        program.CustomerList = null;

                        List<string> custList = new List<string>();
                        List<int> volumeList = new List<int>();

                        foreach (ProgramCustomerEntity progent in progCustList)
                        {
                            if (progent.SelectionType == "V")
                            {
                                volumeList.Add(progent.Volume);
                            }
                            else if (progent.SelectionType == "R")
                            {
                                custList.Add(progent.CustCode);
                            }
                        }

                        program.CustomerList = custList;
                        program.VolumeList = volumeList;
                    }
                }
                
            }
            catch
            {
                throw;
            }
            return program;

        }

        public List<ProgramEntity> GetAllPrograms( )
        {
            try
            {
                return ProgramDataService.GetAllPrograms();
            }
            catch
            {
                throw;
            }
        }        

        public List<ProgramTargetsEntity> GetProgramTargets(int programId, DateTime fromDate, DateTime toDate, ArgsEntity args)
        {
            try
            {
                return ProgramDataService.GetProgramTargets(programId, fromDate, toDate, args);
            }
            catch
            {
                throw;
            }
        }

        public bool IsProgramNameExists(ProgramEntity programEntity)
        {
            try
            {
                return ProgramDataService.IsProgramExists(programEntity);
            }
            catch
            {
                throw;
            }
        }
    }
}



