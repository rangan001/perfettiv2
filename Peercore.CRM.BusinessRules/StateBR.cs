﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class StateBR
    {
        private static StateBR instance = null;
        private static object syncLock = new Object();
        private StateDAO StateDataService;

        public static StateBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new StateBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public StateEntity CreateObject()
        {
            return StateDataService.CreateObject();
        }

        public StateEntity CreateObject(int entityId)
        {
            return StateDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private StateBR()
        {
            StateDataService = new StateDAO();
        }
        #endregion

        public List<StateEntity> GetStates()
        {
            try
            {
                return StateDataService.GetStates();
            }
            catch
            {
                throw;
            }
        }

        public List<StateEntity> GetAllStates()
        {
            try
            {
                return StateDataService.GetAllStates();
            }
            catch
            {
                throw;
            }
        }

        public StateEntity GetStateByCode(string sStateCode)
        {
            try
            {
                return StateDataService.GetStateByCode(sStateCode);
            }
            catch
            {
                throw;
            }
        }


        public StateEntity GetState(string sCountryCode, string sStateCode)
        {
            try
            {
                return StateDataService.GetState(sCountryCode, sStateCode);
            }
            catch
            {
                throw;
            }
        }
    }
}
