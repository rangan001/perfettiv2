﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class SurveyBR
    {
        private static SurveyBR instance = null;
        private static object syncLock = new Object();
        SurveyDAO SurveyDataService;

        public static SurveyBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SurveyBR();
                }
                return instance;
            }
        }

        private SurveyBR()
        {
            SurveyDataService = new SurveyDAO();
        }

        public List<QuestionEntity> GetAllSurveyQuestions()
        {
            try
            {
                return SurveyDataService.GetAllSurveyQuestions();
            }
            catch
            {
                throw;
            }
        }
    }
}
