﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Peercore.CRM.BusinessRules
{
    public class SyncBR
    {
        private static SyncBR instance = null;
        private static object syncLock = new Object();
        private SyncDAO SyncDataService;

        public static SyncBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SyncBR();
                }
                return instance;
            }
        }

        private SyncBR()
        {
            SyncDataService = new SyncDAO();
        }

        public List<SalesRepSyncModel> GetAllSalesRepSyncDataByIMEI(ArgsModel args, string OriginatorType, string Originator, string DeviceIMEI)
        {
            try
            {
                return SyncDataService.GetAllSalesRepSyncDataByIMEI(args, OriginatorType, Originator, DeviceIMEI);
            }
            catch
            {
                throw;
            }
        }

        public List<SalesRepSyncModel> GetAllSalesRepSyncData(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                return SyncDataService.GetAllSalesRepSyncData(args, OriginatorType, Originator);
            }
            catch
            {
                throw;
            }
        }

        public int InserNewSyncMaster(string originator, string originatorType)
        {
            try
            {
                return SyncDataService.InserNewSyncMaster(originator, originatorType);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateRepIMEISyncStatus(string originator, string originatorType, string DeviceIMEI)
        {
            try
            {
                return SyncDataService.UpdateRepIMEISyncStatus(originator, originatorType, DeviceIMEI);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateRepSyncData(string originator, string originatorType)
        {
            try
            {
                return SyncDataService.UpdateRepSyncData(originator, originatorType);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateSRSyncDetail(string accessToken, SalesRepSyncDetailModel srSyncDet)
        {
            bool isSuccess = false;

            try
            {
                OriginatorDAO originatorService = new OriginatorDAO();

                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                if (originator == null)
                {
                    return false;
                }
                else if (srSyncDet.repCode == null)
                {
                    return false;
                }
                else if (srSyncDet.emeiNo == null)
                {
                    return false;
                }

                //Insert new Order 
                isSuccess = SyncDataService.InsertSRSyncDetail(originator.Originator, srSyncDet);

                if (isSuccess)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        public bool UpdateFCMDeliveryStatus(string RepCode, string IMEINo, string status)
        {
            bool isSuccess = true;

            try
            {
                return SyncDataService.UpdateFCMDeliveryStatus(RepCode, IMEINo, status);
            }
            catch
            {
                throw;
            }

            return isSuccess;
        }
    }
}
