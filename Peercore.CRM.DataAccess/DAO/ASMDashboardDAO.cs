﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ASMDashboardDAO : BaseDAO
    {
        private DbSqlAdapter SalesSql { get; set; }

        private void RegisterSql()
        {
            this.SalesSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SalesSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ASMDashboardDAO()
        {
            RegisterSql();
        }

        public ASMDashboardDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ASMDashboardEntity CreateObject()
        {
            return ASMDashboardEntity.CreateObject();
        }

        public ASMDashboardEntity CreateObject(int entityId)
        {
            return ASMDashboardEntity.CreateObject(entityId);
        }

        public List<ASMDashboardEntity> GetDailyRepsSalesforASM(string AccessToken, string date)
        {
            DbDataReader drRepSales = null;

            try
            {
                List<ASMDashboardEntity> repSalesList = new List<ASMDashboardEntity>();
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();//

                paramCollection.Add("@AccessToken", AccessToken, DbType.String);
                paramCollection.Add("@Date", date, DbType.String);

                drRepSales = this.DataAcessService.ExecuteQuery(SalesSql["GetAll_Daily_Reps_Sales_for_ASM"], paramCollection);

                if (drRepSales != null && drRepSales.HasRows)
                {
                    int trn_dateOrdinal = drRepSales.GetOrdinal("trn_date");
                    int rep_codeOrdinal = drRepSales.GetOrdinal("rep_code");
                    int rep_nameOrdinal = drRepSales.GetOrdinal("rep_name");
                    int route_nameOrdinal = drRepSales.GetOrdinal("route_name");
                    int outlet_countOrdinal = drRepSales.GetOrdinal("outlet_count");
                    int pc_countOrdinal = drRepSales.GetOrdinal("pc_count");
                    int vc_countOrdinal = drRepSales.GetOrdinal("vc_count");
                    int gross_totalOrdinal = drRepSales.GetOrdinal("gross_total");
                    int discount_totalOrdinal = drRepSales.GetOrdinal("discount_total");
                    int sales_gumOrdinal = drRepSales.GetOrdinal("sales_gum");
                    int sales_candyOrdinal = drRepSales.GetOrdinal("sales_candy");
                    int sales_jellyOrdinal = drRepSales.GetOrdinal("sales_jelly");
                    int sales_hvpOrdinal = drRepSales.GetOrdinal("sales_hvp");
                    int sales_1300Ordinal = drRepSales.GetOrdinal("sales_1300");
                    int sales_mLinesOrdinal = drRepSales.GetOrdinal("sales_mLines");
                    int qty_1300Ordinal = drRepSales.GetOrdinal("qty_1300");
                    int qty_mLinesOrdinal = drRepSales.GetOrdinal("qty_mLines");
                    int bill_timeOrdinal = drRepSales.GetOrdinal("bill_time");
                    int echoOrdinal = drRepSales.GetOrdinal("echo");

                    while (drRepSales.Read())
                    {
                        ASMDashboardEntity asmDBEntity = CreateObject();

                        if (!drRepSales.IsDBNull(rep_codeOrdinal)) asmDBEntity.RepCode = drRepSales.GetString(rep_codeOrdinal);
                        if (!drRepSales.IsDBNull(rep_nameOrdinal)) asmDBEntity.RepName = drRepSales.GetString(rep_nameOrdinal);
                        try
                        {
                            if (!drRepSales.IsDBNull(route_nameOrdinal)) asmDBEntity.RouteName = drRepSales.GetString(route_nameOrdinal);
                            if (!drRepSales.IsDBNull(outlet_countOrdinal)) asmDBEntity.AvailableOutlets = drRepSales.GetInt32(outlet_countOrdinal);
                            if (!drRepSales.IsDBNull(pc_countOrdinal)) asmDBEntity.PCCount = drRepSales.GetInt32(pc_countOrdinal);
                            if (!drRepSales.IsDBNull(vc_countOrdinal)) asmDBEntity.VCCount = drRepSales.GetInt32(vc_countOrdinal);
                            if (!drRepSales.IsDBNull(gross_totalOrdinal)) asmDBEntity.GrossTotal = drRepSales.GetDouble(gross_totalOrdinal);
                            if (!drRepSales.IsDBNull(discount_totalOrdinal)) asmDBEntity.DiscountTotal = drRepSales.GetDouble(discount_totalOrdinal);
                            if (!drRepSales.IsDBNull(sales_gumOrdinal)) asmDBEntity.GumSales = drRepSales.GetDouble(sales_gumOrdinal);
                            if (!drRepSales.IsDBNull(sales_candyOrdinal)) asmDBEntity.CandySales = drRepSales.GetDouble(sales_candyOrdinal);
                            if (!drRepSales.IsDBNull(sales_jellyOrdinal)) asmDBEntity.JellySales = drRepSales.GetDouble(sales_jellyOrdinal);
                            if (!drRepSales.IsDBNull(sales_hvpOrdinal)) asmDBEntity.HVPSales = drRepSales.GetDouble(sales_hvpOrdinal);
                            if (!drRepSales.IsDBNull(sales_1300Ordinal)) asmDBEntity.Sales1300 = drRepSales.GetDouble(sales_1300Ordinal);
                            if (!drRepSales.IsDBNull(sales_mLinesOrdinal)) asmDBEntity.SalesMLines = drRepSales.GetDouble(sales_mLinesOrdinal);
                            if (!drRepSales.IsDBNull(qty_1300Ordinal)) asmDBEntity.Qty1300 = drRepSales.GetInt32(qty_1300Ordinal);
                            if (!drRepSales.IsDBNull(qty_mLinesOrdinal)) asmDBEntity.QtyMLines = drRepSales.GetInt32(qty_mLinesOrdinal);

                            try
                            {
                                if (!drRepSales.IsDBNull(bill_timeOrdinal)) asmDBEntity.FirstBillTime = drRepSales.GetDateTime(bill_timeOrdinal).ToString("yyyy-MM-dd hh:mm:ss tt");
                            }
                            catch { asmDBEntity.FirstBillTime = ""; }


                            if (!drRepSales.IsDBNull(echoOrdinal)) asmDBEntity.Echo = drRepSales.GetDouble(echoOrdinal).ToString();
                        }
                        catch { }

                        repSalesList.Add(asmDBEntity);
                    }
                }

                return repSalesList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drRepSales != null)
                    drRepSales.Close();
            }
        }

        public List<ASMDashboardModel> GetDailyRepsSalesforASMNew(string AccessToken, string date)
        {
            ASMDashboardModel AsmDashBoardDailySummury = null;
            DataTable objDS = new DataTable();
            List<ASMDashboardModel> AsmDashBoardDailySummuryList = new List<ASMDashboardModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAll_Daily_Reps_Sales_for_ASM";

                    objCommand.Parameters.AddWithValue("@AccessToken", AccessToken);
                    objCommand.Parameters.AddWithValue("@Date", date);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            AsmDashBoardDailySummury = new ASMDashboardModel();
                            if (item["rep_code"] != DBNull.Value) AsmDashBoardDailySummury.RepCode = Convert.ToString(item["rep_code"]);
                            if (item["rep_name"] != DBNull.Value) AsmDashBoardDailySummury.RepName = Convert.ToString(item["rep_name"]);
                            if (item["route_name"] != DBNull.Value) AsmDashBoardDailySummury.RouteName = Convert.ToString(item["route_name"]);
                            if (item["outlet_count"] != DBNull.Value) AsmDashBoardDailySummury.AvailableOutlets = Convert.ToInt32(item["outlet_count"]);
                            if (item["pc_count"] != DBNull.Value) AsmDashBoardDailySummury.PCCount = Convert.ToInt32(item["pc_count"]);
                            if (item["vc_count"] != DBNull.Value) AsmDashBoardDailySummury.VCCount = Convert.ToInt32(item["vc_count"]);
                            if (item["gross_total"] != DBNull.Value) AsmDashBoardDailySummury.GrossTotal = Convert.ToDouble(item["gross_total"]);
                            if (item["discount_total"] != DBNull.Value) AsmDashBoardDailySummury.DiscountTotal = Convert.ToDouble(item["discount_total"]);
                            if (item["sales_gum"] != DBNull.Value) AsmDashBoardDailySummury.GumSales = Convert.ToDouble(item["sales_gum"]);
                            if (item["sales_candy"] != DBNull.Value) AsmDashBoardDailySummury.CandySales = Convert.ToDouble(item["sales_candy"]);
                            if (item["sales_jelly"] != DBNull.Value) AsmDashBoardDailySummury.JellySales = Convert.ToDouble(item["sales_jelly"]);
                            if (item["sales_hvp"] != DBNull.Value) AsmDashBoardDailySummury.HVPSales = Convert.ToDouble(item["sales_hvp"]);
                            if (item["sales_1300"] != DBNull.Value) AsmDashBoardDailySummury.Sales1300 = Convert.ToDouble(item["sales_1300"]);
                            if (item["sales_mLines"] != DBNull.Value) AsmDashBoardDailySummury.SalesMLines = Convert.ToDouble(item["sales_mLines"]);
                            if (item["qty_1300"] != DBNull.Value) AsmDashBoardDailySummury.Qty1300 = Convert.ToInt32(item["qty_1300"]);
                            if (item["qty_mLines"] != DBNull.Value) AsmDashBoardDailySummury.QtyMLines = Convert.ToInt32(item["qty_mLines"]);
                            
                            try
                            {
                                if (item["bill_time"] != DBNull.Value) AsmDashBoardDailySummury.FirstBillTime = Convert.ToDateTime(item["bill_time"]).ToString("yyyy-MM-dd hh:mm:ss tt");
                            }
                            catch { AsmDashBoardDailySummury.FirstBillTime = ""; }

                            if (item["echo"] != DBNull.Value) AsmDashBoardDailySummury.Echo = Convert.ToDouble(item["echo"]).ToString();

                            AsmDashBoardDailySummuryList.Add(AsmDashBoardDailySummury);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return AsmDashBoardDailySummuryList;
        }
    }
}
