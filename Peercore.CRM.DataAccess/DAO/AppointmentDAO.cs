﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using Peercore.CRM.DataAccess.datasets;

namespace Peercore.CRM.DataAccess.DAO
{
    public class AppointmentDAO : BaseDAO
    {
        private DbSqlAdapter AppointmentsSql { get; set; }

        private void RegisterSql()
        {
            this.AppointmentsSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.AppointmentSql.xml", ApplicationService.Instance.DbProvider);
        }

        public AppointmentDAO()
        {
            RegisterSql();
        }

        public AppointmentDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public AppointmentEntity CreateObject()
        {
            return AppointmentEntity.CreateObject();
        }

        public AppointmentEntity CreateObject(int entityId)
        {
            return AppointmentEntity.CreateObject(entityId);
        }

        #region New Methods
        public void GetAppointments(ref DataTable dtAppointment, ArgsEntity args)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Floor", args.Floor, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "created_by"), DbType.String);
                paramCollection.Add("@OriginatorId", args.OriginatorId, DbType.String);
                paramCollection.Add("@DefaultDepartmentId", args.DefaultDepartmentId, DbType.String);

                dtAppointment = this.DataAcessService.ExecuteDisconnected(AppointmentsSql["GetAppointments"],paramCollection);
            }
            catch
            {
                throw;
            }
        }

        public List<AppointmentViewEntity> GetOncomingAppointments(ArgsEntity args)
        {
            DbDataReader drAppointment = null;
            List<AppointmentViewEntity> appointmentCollection = new List<AppointmentViewEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CreatedBy", args.Originator, DbType.String);
                paramCollection.Add("@ResourceId", args.OriginatorId, DbType.Int32);
                drAppointment = this.DataAcessService.ExecuteQuery(AppointmentsSql["GetOncomingAppointments"], paramCollection);

                if (drAppointment != null && drAppointment.HasRows)
                {
                    int appointmentOrdinal = drAppointment.GetOrdinal("appointment");
                    int appointmentIdOrdinal = drAppointment.GetOrdinal("appointment_id");

                    while (drAppointment.Read())
                    {
                        AppointmentViewEntity oAppointmentView = new AppointmentViewEntity();
                        if (!drAppointment.IsDBNull(appointmentIdOrdinal)) oAppointmentView.AppointmentID = drAppointment.GetInt32(appointmentIdOrdinal);
                        if (!drAppointment.IsDBNull(appointmentOrdinal)) oAppointmentView.Subject = drAppointment.GetString(appointmentOrdinal);

                        appointmentCollection.Add(oAppointmentView);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drAppointment != null)
                    drAppointment.Close();
            }
            return appointmentCollection;
        }

        public bool Insert(AppointmentEntity appointment, out int appointmentID, bool isUpdateActivity = false)
        {
            try
            {
                bool isSuccess = true;
                appointment.AppointmentID = System.Convert.ToInt32(DataAcessService.GetOneValue(AppointmentsSql["GetNextAppointmentID"]));   // Get Next ID
                appointmentID = appointment.AppointmentID;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AppointmentId", appointment.AppointmentID, DbType.Int32);
                paramCollection.Add("@Subject", ValidateString(appointment.Subject), DbType.String);
                paramCollection.Add("@Body", ValidateString(appointment.Body), DbType.String);
                paramCollection.Add("@StartTime", appointment.StartTime.ToUniversalTime(), DbType.DateTime);
                paramCollection.Add("@EndTime", appointment.EndTime.ToUniversalTime(), DbType.DateTime);
                paramCollection.Add("@Category", ValidateString(appointment.Category), DbType.String);
                paramCollection.Add("@CreatedBy", ValidateString(appointment.CreatedBy), DbType.String);

                isSuccess = this.DataAcessService.ExecuteNonQuery(AppointmentsSql["InsertAppointment"], paramCollection) > 0;

                return isSuccess;
            }
            catch
            {
                throw;
            }
        }

        public bool Update(AppointmentEntity appointment, bool updateDTRangeOnly = false, bool updateDelFlagOnly = false)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                bool isSuccess = true;
                if (updateDTRangeOnly == false && updateDelFlagOnly == false)
                {
                    paramCollection.Add("@Subject", ValidateString(appointment.Subject), DbType.String);
                    paramCollection.Add("@Body", ValidateString(appointment.Body), DbType.String);
                    paramCollection.Add("@StartTime", appointment.StartTime.ToUniversalTime(), DbType.DateTime);
                    paramCollection.Add("@EndTime", appointment.EndTime.ToUniversalTime(), DbType.DateTime);
                    paramCollection.Add("@Category", appointment.Category, DbType.String);
                    paramCollection.Add("@AppointmentId", appointment.AppointmentID, DbType.Int32);

                    isSuccess = this.DataAcessService.ExecuteNonQuery(AppointmentsSql["UpdateAppointment"], paramCollection)>0;
                }
                else if (updateDTRangeOnly == true && updateDelFlagOnly == false)
                {
                    paramCollection.Add("@StartTime", appointment.StartTime.ToUniversalTime(), DbType.DateTime);
                    paramCollection.Add("@EndTime", appointment.EndTime.ToUniversalTime(), DbType.DateTime);
                    paramCollection.Add("@AppointmentId", appointment.AppointmentID, DbType.Int32);
                    isSuccess = this.DataAcessService.ExecuteNonQuery(AppointmentsSql["UpdateAppointmentDTRange"], paramCollection)>0;
                }
                else if (updateDelFlagOnly == true)
                {
                    paramCollection.Add("@AppointmentId", appointment.AppointmentID, DbType.Int32);
                    isSuccess = this.DataAcessService.ExecuteNonQuery(AppointmentsSql["DeleteAppointment"], paramCollection)>0;
                }
                return isSuccess;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateActivity(AppointmentEntity appointment, int activityId = 0)
        {
            bool isSuccess = true;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@AppointmentId", appointment.AppointmentID, DbType.Int32);
            paramCollection.Add("@ActivityId", activityId, DbType.Int32);
            isSuccess = this.DataAcessService.ExecuteNonQuery(AppointmentsSql["UpdateActivityWithAppointment"], paramCollection) > 0;

            return isSuccess;
        }

        public int DeleteResources(AppointmentEntity appointment)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AppointmentId", appointment.AppointmentID, DbType.Int32);
                return this.DataAcessService.ExecuteNonQuery(AppointmentsSql["DeleteResources"], paramCollection);
            }
            catch
            {
                throw;
            }
        }

        public List<KeyValuePair<string, int>> GetAppointmentsCount(ArgsEntity args)
        {
            DbDataReader drAppointment = null;

            try
            {
                List<KeyValuePair<string, int>> homeOpportunityCount = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                //paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                //paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drAppointment = this.DataAcessService.ExecuteQuery(AppointmentsSql["GetAppointmentsCount"], paramCollection);

                if (drAppointment != null && drAppointment.HasRows)
                {
                    homeOpportunityCount = new List<KeyValuePair<string, int>>();

                    int pendingOrdinal = drAppointment.GetOrdinal("pending");
                    int todayOrdinal = drAppointment.GetOrdinal("today");
                    //int futureOrdinal = drAppointment.GetOrdinal("future");

                    while (drAppointment.Read())
                    {
                        if (!drAppointment.IsDBNull(pendingOrdinal)) homeOpportunityCount.Add(new KeyValuePair<string, int>("pending", drAppointment.GetInt32(pendingOrdinal)));
                        if (!drAppointment.IsDBNull(todayOrdinal)) homeOpportunityCount.Add(new KeyValuePair<string, int>("today", drAppointment.GetInt32(todayOrdinal)));
                        //if (!drOpportunity.IsDBNull(futureOrdinal)) homeOpportunityCount.Add(new KeyValuePair<string, int>("future", drOpportunity.GetInt32(futureOrdinal)));
                    }
                }

                return homeOpportunityCount;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drAppointment != null)
                    drAppointment.Close();
            }
        }

        public List<AppointmentViewEntity> GetTodaysAppointments(ArgsEntity args)
        {
            DbDataReader drAppointment = null;

            try
            {
                List<AppointmentViewEntity> appointmentCollection = new List<AppointmentViewEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drAppointment = this.DataAcessService.ExecuteQuery(AppointmentsSql["GetTodaysAppointments"], paramCollection);

                if (drAppointment != null && drAppointment.HasRows)
                {
                    int appointmentIdOrdinal = drAppointment.GetOrdinal("appointment_id");
                    int subjectIdOrdinal = drAppointment.GetOrdinal("subject");
                    int startTimeIdOrdinal = drAppointment.GetOrdinal("start_time");
                    int endTimeIdOrdinal = drAppointment.GetOrdinal("end_time");
                    int totalcountOrdinal = drAppointment.GetOrdinal("total_count");

                    while (drAppointment.Read())
                    {
                        AppointmentViewEntity oAppointmentView = new AppointmentViewEntity();
                        if (!drAppointment.IsDBNull(appointmentIdOrdinal)) oAppointmentView.AppointmentID = drAppointment.GetInt32(appointmentIdOrdinal);
                        if (!drAppointment.IsDBNull(subjectIdOrdinal)) oAppointmentView.Subject = drAppointment.GetString(subjectIdOrdinal);
                        if (!drAppointment.IsDBNull(startTimeIdOrdinal)) oAppointmentView.StartTime = drAppointment.GetDateTime(startTimeIdOrdinal);
                        if (!drAppointment.IsDBNull(endTimeIdOrdinal)) oAppointmentView.EndTime = drAppointment.GetDateTime(endTimeIdOrdinal);

                        if (!drAppointment.IsDBNull(totalcountOrdinal)) oAppointmentView.TotalCount = drAppointment.GetInt32(totalcountOrdinal);
                        appointmentCollection.Add(oAppointmentView);
                    }
                }

                return appointmentCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drAppointment != null)
                    drAppointment.Close();
            }
        }

        public List<AppointmentViewEntity> GetPendingAppointments(ArgsEntity args)
        {
            DbDataReader drAppointment = null;

            try
            {
                List<AppointmentViewEntity> appointmentCollection = new List<AppointmentViewEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drAppointment = this.DataAcessService.ExecuteQuery(AppointmentsSql["GetPendingAppointments"], paramCollection);

                if (drAppointment != null && drAppointment.HasRows)
                {
                    int appointmentIdOrdinal = drAppointment.GetOrdinal("appointment_id");
                    int subjectIdOrdinal = drAppointment.GetOrdinal("subject");
                    int startTimeIdOrdinal = drAppointment.GetOrdinal("start_time");
                    int endTimeIdOrdinal = drAppointment.GetOrdinal("end_time");
                    int totalcountOrdinal = drAppointment.GetOrdinal("total_count");
                    

                    while (drAppointment.Read())
                    {
                        AppointmentViewEntity oAppointmentView = new AppointmentViewEntity();
                        if (!drAppointment.IsDBNull(appointmentIdOrdinal)) oAppointmentView.AppointmentID = drAppointment.GetInt32(appointmentIdOrdinal);
                        if (!drAppointment.IsDBNull(subjectIdOrdinal)) oAppointmentView.Subject = drAppointment.GetString(subjectIdOrdinal);
                        if (!drAppointment.IsDBNull(startTimeIdOrdinal)) oAppointmentView.StartTime = drAppointment.GetDateTime(startTimeIdOrdinal);
                        if (!drAppointment.IsDBNull(endTimeIdOrdinal)) oAppointmentView.EndTime = drAppointment.GetDateTime(endTimeIdOrdinal);
                        if (!drAppointment.IsDBNull(totalcountOrdinal)) oAppointmentView.TotalCount = drAppointment.GetInt32(totalcountOrdinal);
                        appointmentCollection.Add(oAppointmentView);
                    }
                }

                return appointmentCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drAppointment != null)
                    drAppointment.Close();
            }
        }

        public List<AppointmentViewEntity> GetAllAppointmentsByType(ArgsEntity args)
        {
            DbDataReader idrAppointment = null;

            try
            {
                List<AppointmentViewEntity> appointmentCollection = new List<AppointmentViewEntity>();

                /*if (sOriginator == "")
                {
                    sWhereCls = ChildOriginators;
                    sWhereCls = " AND " + sWhereCls.Replace("originator", "ap.created_by");
                }
                else
                    sWhereCls = " AND ap.created_by = '" + sOriginator + "'";

                // Get Originator ID of the given originator
                if (sOriginator != "")
                    iOriginatorID = (int)oDataHandle.GetOneValue(string.Format(CRMSQLs.GetOriginatorID, sOriginator));

                if (sFromDate != "" && sEndDate != "")
                {
                    sWhereCls = sWhereCls != "" ? sWhereCls + " AND " : sWhereCls;
                    sWhereCls += "ap.start_time >= '" + DateTime.Parse(sFromDate).ToString("dd-MMM-yyyy") + " 00:00:00' and ap.end_time <= '" + DateTime.Parse(sEndDate).ToString("dd-MMM-yyyy") + " 23:59:59'";
                    sResWhereCls = " AND " + "ap.start_time >= '" + DateTime.Parse(sFromDate).ToString("dd-MMM-yyyy") + " 00:00:00' and ap.end_time <= '" + DateTime.Parse(sEndDate).ToString("dd-MMM-yyyy") + " 23:59:59'";
                }

                if (sSector != "")
                {
                    sWhereCls = sWhereCls + " AND (rep_code = '' OR rep_code = '" + sSector + "')";
                    sResWhereCls = sResWhereCls + " AND (rep_code = '' OR rep_code = '" + sSector + "')";
                }
                */



                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                //paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                //paramCollection.Add("@EndDate", args.SEndDate, DbType.String);

                paramCollection.Add("@FromDate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("yyyy/MM/dd") + " 00:00:00", DbType.String);
                paramCollection.Add("@EndDate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("yyyy/MM/dd") + " 23:59:59", DbType.String);

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@OriginatorId", args.OriginatorId, DbType.Int32);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "ap.created_by"), DbType.String);
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
                
                paramCollection.Add("@Sector", args.Sector, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean );

                paramCollection.Add("@SqlType", args.SMonth, DbType.Boolean);
                

                idrAppointment = this.DataAcessService.ExecuteQuery(AppointmentsSql["GetAppointments"], paramCollection);

                if (idrAppointment != null && idrAppointment.HasRows)
                {
                    int appointmentIdOrdinal = idrAppointment.GetOrdinal("appointment_id");
                    int subjectOrdinal = idrAppointment.GetOrdinal("subject");
                    int startTimeOrdinal = idrAppointment.GetOrdinal("start_time");
                    int endTimeOrdinal = idrAppointment.GetOrdinal("end_time");
                    int categoryOrdinal = idrAppointment.GetOrdinal("category");
                    int createdByOrdinal = idrAppointment.GetOrdinal("created_by");
                    int typeDescOrdinal = idrAppointment.GetOrdinal("type_description");
                    int leadNameOrdinal = idrAppointment.GetOrdinal("lead_name");
                    int bodyOrdinal = idrAppointment.GetOrdinal("body");
                    int leadStageOrdinal = idrAppointment.GetOrdinal("lead_stage");
                    int activityIdOrdinal = idrAppointment.GetOrdinal("activity_id");
                    int leadIdOrdinal = idrAppointment.GetOrdinal("lead_id");
                    int custCodeOrdinal = idrAppointment.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = idrAppointment.GetOrdinal("enduser_code");
                    int totalCountOrdinal = idrAppointment.GetOrdinal("total_count");

                    while (idrAppointment.Read())
                    {
                        AppointmentViewEntity appointment = new AppointmentViewEntity();

                        if (!idrAppointment.IsDBNull(appointmentIdOrdinal)) appointment.AppointmentID = idrAppointment.GetInt32(appointmentIdOrdinal);
                        if (!idrAppointment.IsDBNull(subjectOrdinal)) appointment.Subject = idrAppointment.GetString(subjectOrdinal);
                        if (!idrAppointment.IsDBNull(startTimeOrdinal)) appointment.StartTime = idrAppointment.GetDateTime(startTimeOrdinal).ToLocalTime();
                        if (!idrAppointment.IsDBNull(endTimeOrdinal)) appointment.EndTime = idrAppointment.GetDateTime(endTimeOrdinal).ToLocalTime();
                        if (!idrAppointment.IsDBNull(categoryOrdinal)) appointment.Category = idrAppointment.GetString(categoryOrdinal);
                        if (!idrAppointment.IsDBNull(createdByOrdinal)) appointment.CreatedBy = idrAppointment.GetString(createdByOrdinal);
                        if (!idrAppointment.IsDBNull(typeDescOrdinal)) appointment.CategoryDescription = idrAppointment.GetString(typeDescOrdinal);
                        if (!idrAppointment.IsDBNull(leadNameOrdinal)) appointment.LeadName = idrAppointment.GetString(leadNameOrdinal);
                        if (!idrAppointment.IsDBNull(bodyOrdinal)) appointment.Body = idrAppointment.GetString(bodyOrdinal);
                        if (!idrAppointment.IsDBNull(leadStageOrdinal)) appointment.LeadStage = idrAppointment.GetString(leadStageOrdinal);
                        if (!idrAppointment.IsDBNull(activityIdOrdinal)) appointment.ActivityID = idrAppointment.GetInt32(activityIdOrdinal);
                        if (!idrAppointment.IsDBNull(leadIdOrdinal)) appointment.LeadID = idrAppointment.GetInt32(leadIdOrdinal);
                        if (!idrAppointment.IsDBNull(custCodeOrdinal)) appointment.CustCode = idrAppointment.GetString(custCodeOrdinal);
                        if (!idrAppointment.IsDBNull(enduserCodeOrdinal)) appointment.EndUserCode = idrAppointment.GetString(enduserCodeOrdinal);
                        if (!idrAppointment.IsDBNull(totalCountOrdinal)) appointment.TotalCount = idrAppointment.GetInt32(totalCountOrdinal);

                        appointmentCollection.Add(appointment);
                    }
                }

                return appointmentCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrAppointment != null)
                    idrAppointment.Close();
            }
        }

        public List<AppointmentCountViewEntity> GetAllAppointmentsByTypeCount(ArgsEntity args)
        {
            DbDataReader idrAppointment = null;

            try
            {
                List<AppointmentCountViewEntity> appointmentCollection = new List<AppointmentCountViewEntity>();

                



                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                paramCollection.Add("@ToDate", args.SEndDate, DbType.String);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@OriginatorId", args.OriginatorId, DbType.Int32);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "ap.created_by"), DbType.String);
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);

                paramCollection.Add("@Sector", args.Sector, DbType.String);
                //paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                //paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                //paramCollection.Add("@ShowSQL", 0, DbType.Boolean);


                idrAppointment = this.DataAcessService.ExecuteQuery(AppointmentsSql["GetAppointmentCountByTypeTable"], paramCollection);

                if (idrAppointment != null && idrAppointment.HasRows)
                {
                    int categoryOrdinal = idrAppointment.GetOrdinal("category");
                    int colorOrdinal = idrAppointment.GetOrdinal("color");
                    int descriptionOrdinal = idrAppointment.GetOrdinal("type_description");
                    int week1Ordinal = idrAppointment.GetOrdinal("week1");
                    int week2Ordinal = idrAppointment.GetOrdinal("week2");
                    int week3Ordinal = idrAppointment.GetOrdinal("week3");
                    int week4Ordinal = idrAppointment.GetOrdinal("week4");
                    int week5Ordinal = idrAppointment.GetOrdinal("week5");
                    int month1Ordinal = idrAppointment.GetOrdinal("month1");
                    int month2Ordinal = idrAppointment.GetOrdinal("month2");
                    int month3Ordinal = idrAppointment.GetOrdinal("month3");

                    while (idrAppointment.Read())
                    {
                        AppointmentCountViewEntity appointment = AppointmentCountViewEntity.CreateObject();

                        if (!idrAppointment.IsDBNull(categoryOrdinal)) appointment.Category = idrAppointment.GetString(categoryOrdinal);
                        if (!idrAppointment.IsDBNull(colorOrdinal)) appointment.Color = idrAppointment.GetString(colorOrdinal);
                        if (!idrAppointment.IsDBNull(descriptionOrdinal)) appointment.Description = idrAppointment.GetString(descriptionOrdinal);
                        if (!idrAppointment.IsDBNull(week1Ordinal)) appointment.Week1 = idrAppointment.GetInt32(week1Ordinal);
                        if (!idrAppointment.IsDBNull(week2Ordinal)) appointment.Week2 = idrAppointment.GetInt32(week2Ordinal);
                        if (!idrAppointment.IsDBNull(week3Ordinal)) appointment.Week3 = idrAppointment.GetInt32(week3Ordinal);
                        if (!idrAppointment.IsDBNull(week4Ordinal)) appointment.Week4 = idrAppointment.GetInt32(week4Ordinal);
                        if (!idrAppointment.IsDBNull(week5Ordinal)) appointment.Week5 = idrAppointment.GetInt32(week5Ordinal);

                        if (!idrAppointment.IsDBNull(month1Ordinal)) appointment.Month1 = idrAppointment.GetInt32(month1Ordinal);
                        if (!idrAppointment.IsDBNull(month2Ordinal)) appointment.Month2 = idrAppointment.GetInt32(month2Ordinal);
                        if (!idrAppointment.IsDBNull(month3Ordinal)) appointment.Month3 = idrAppointment.GetInt32(month3Ordinal);

                        appointmentCollection.Add(appointment);
                    }
                }

                return appointmentCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrAppointment != null)
                    idrAppointment.Close();
            }
        }

        public bool InsertAppointmentlst(CRMAppointmentEntity appointment,int ActivityId, out int appointmentId)
        {
            try
            {
                bool isSuccess = true;
                appointment.AppointmentID = System.Convert.ToInt32(DataAcessService.GetOneValue(AppointmentsSql["GetNextAppointmentID"]));   // Get Next ID
                appointmentId = appointment.AppointmentID;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AppointmentId", appointment.AppointmentID, DbType.Int32);
                paramCollection.Add("@Subject", ValidateString(appointment.Subject), DbType.String);
                paramCollection.Add("@Body", ValidateString(appointment.Body), DbType.String);
                paramCollection.Add("@StartTime", appointment.StartTime.ToUniversalTime(), DbType.DateTime);
                paramCollection.Add("@EndTime", appointment.EndTime.ToUniversalTime(), DbType.DateTime);
                paramCollection.Add("@Category", ValidateString(appointment.Category), DbType.String);
                paramCollection.Add("@CreatedBy", ValidateString(appointment.CreatedBy), DbType.String);

                isSuccess = this.DataAcessService.ExecuteNonQuery(AppointmentsSql["InsertAppointment"], paramCollection) > 0;

                if (isSuccess)
                {
                    paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@AppointmentId", appointment.AppointmentID, DbType.Int32);
                    paramCollection.Add("@ActivityId", ActivityId, DbType.Int32);
                    isSuccess = this.DataAcessService.ExecuteNonQuery(AppointmentsSql["UpdateActivityWithAppointment"], paramCollection) > 0;
                }

                return isSuccess;
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region - Report Methods -
        public AppointmentDataSet GetReportData( ArgsEntity args)
        {
            DbDataAdapter dapAppointment = null;

            try
            {

                //Instantiate report specific data set.
                AppointmentDataSet dsAppointment = new AppointmentDataSet();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                
                paramCollection.Add("@OriginatorId", args.OriginatorId, DbType.Int32);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "app.created_by"), DbType.String);
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);


                //Retreive the data adapter to fill the report data
                dapAppointment = this.DataAcessService.ExecuteReportQuery(AppointmentsSql["GetAppointmentReportData"], paramCollection);

                //Fill the report specific data set
                dapAppointment.Fill(dsAppointment, dsAppointment.AppointmentDetail.TableName);

                return dsAppointment;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapAppointment != null)
                    dapAppointment.Dispose();
            }
        }
        #endregion

        #region Old Methods
        /*public List<AppointmentViewEntity> GetFurureAppointments(ArgsEntity args)
        {
            DbDataReader drAppointment = null;

            try
            {
                List<AppointmentViewEntity> appointmentCollection = new List<AppointmentViewEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drAppointment = this.DataAcessService.ExecuteQuery(AppointmentsSql["GetAppointmentsHomeFutrPast"], paramCollection);

                if (drAppointment != null && drAppointment.HasRows)
                {
                    int appointmentIdOrdinal = drAppointment.GetOrdinal("appointment_id");
                    int subjectIdOrdinal = drAppointment.GetOrdinal("subject");
                    int startTimeIdOrdinal = drAppointment.GetOrdinal("start_time");
                    int endTimeIdOrdinal = drAppointment.GetOrdinal("end_time");
                    int bodyOrdinal = drAppointment.GetOrdinal("body");
                    int categoryOrdinal = drAppointment.GetOrdinal("category");
                    int createdByOrdinal = drAppointment.GetOrdinal("created_by");
                    int leadNameOrdinal = drAppointment.GetOrdinal("lead_name");
                    int leadIdOrdinal = drAppointment.GetOrdinal("lead_id");
                    int activityIdOrdinal = drAppointment.GetOrdinal("activity_id");
                    int custCodeOrdinal = drAppointment.GetOrdinal("cust_code");

                    while (drAppointment.Read())
                    {
                        AppointmentViewEntity oAppointmentView = new AppointmentViewEntity();
                        if (!drAppointment.IsDBNull(appointmentIdOrdinal)) oAppointmentView.AppointmentID = drAppointment.GetInt32(appointmentIdOrdinal);
                        if (!drAppointment.IsDBNull(subjectIdOrdinal)) oAppointmentView.Subject = drAppointment.GetString(subjectIdOrdinal);
                        if (!drAppointment.IsDBNull(startTimeIdOrdinal)) oAppointmentView.StartTime = drAppointment.GetDateTime(startTimeIdOrdinal).ToLocalTime();
                        if (!drAppointment.IsDBNull(endTimeIdOrdinal)) oAppointmentView.EndTime = drAppointment.GetDateTime(endTimeIdOrdinal).ToLocalTime();
                        if (!drAppointment.IsDBNull(endTimeIdOrdinal)) oAppointmentView.Body = drAppointment.GetString(bodyOrdinal);
                        if (!drAppointment.IsDBNull(categoryOrdinal)) oAppointmentView.Category = drAppointment.GetString(categoryOrdinal);
                        if (!drAppointment.IsDBNull(createdByOrdinal)) oAppointmentView.CreatedBy = drAppointment.GetString(createdByOrdinal);
                        if (!drAppointment.IsDBNull(leadNameOrdinal)) oAppointmentView.LeadName = drAppointment.GetString(leadNameOrdinal);
                        //if (!drAppointment.IsDBNull(leadIdOrdinal)) oAppointmentView.LeadId = drAppointment.GetInt32(leadIdOrdinal);
                        //if (!drAppointment.IsDBNull(activityIdOrdinal)) oAppointmentView.ActivityId = drAppointment.GetInt32(activityIdOrdinal);
                        if (!drAppointment.IsDBNull(custCodeOrdinal)) oAppointmentView.CustCode = drAppointment.GetString(custCodeOrdinal);
                        appointmentCollection.Add(oAppointmentView);
                    }
                }

                return appointmentCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drAppointment != null)
                    drAppointment.Close();
            }
        }
        */
        
        #endregion
    }
}
