﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class AppointmentResourceDAO:BaseDAO
    {
        private DbSqlAdapter AppointmentsSql { get; set; }

        private void RegisterSql()
        {
            this.AppointmentsSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.AppointmentSql.xml", ApplicationService.Instance.DbProvider);
        }

        public AppointmentResourceDAO()
        {
            RegisterSql();
        }

        public AppointmentResourceDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public AppointmentResourceEntity CreateObject()
        {
            return AppointmentResourceEntity.CreateObject();
        }

        public AppointmentResourceEntity CreateObject(int entityId)
        {
            return AppointmentResourceEntity.CreateObject(entityId);
        }

        #region Methods
        public bool InsertResources(AppointmentResourceEntity appointmentResourceEntity)
        {
            try
            {
                bool isSuccess = true;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AppointmentId", appointmentResourceEntity.AppointmentID, DbType.Int32);
                paramCollection.Add("@ResourceID", appointmentResourceEntity.ResourceID, DbType.Int32);
                
                isSuccess = this.DataAcessService.ExecuteNonQuery(AppointmentsSql["InsertAppointment"], paramCollection) > 0;

                return isSuccess;
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
