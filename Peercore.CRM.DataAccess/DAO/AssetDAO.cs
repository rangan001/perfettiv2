﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class AssetDAO : BaseDAO
    {
        #region  - Asset Type - 

        public List<AssetTypeModel> GetAllAssetTypes(ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<AssetTypeModel> assetTypeList = new List<AssetTypeModel>();
            AssetTypeModel assetTypeEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllAssetTypes";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        assetTypeEntity = new AssetTypeModel();

                        if (item["asset_type_id"] != DBNull.Value) assetTypeEntity.AssetTypeId = Convert.ToInt32(item["asset_type_id"]);
                        if (item["asset_type_name"] != DBNull.Value) assetTypeEntity.AssetTypeName = item["asset_type_name"].ToString();
                        if (item["description"] != DBNull.Value) assetTypeEntity.Description = item["description"].ToString();
                        if (item["created_by"] != DBNull.Value) assetTypeEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) assetTypeEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        assetTypeList.Add(assetTypeEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return assetTypeList;
        }

        public bool SaveAssetType(AssetTypeModel assetTypeDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spSaveAssetType";

                    objCommand.Parameters.AddWithValue("@asset_type_name", assetTypeDetail.AssetTypeName);
                    objCommand.Parameters.AddWithValue("@description", assetTypeDetail.Description);
                    objCommand.Parameters.AddWithValue("@created_by", assetTypeDetail.CreatedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool UpdateAssetType(int assetTypeId, string typeName, string description, string ModifiedBy)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateAssetType";

                    objCommand.Parameters.AddWithValue("@asset_type_id", assetTypeId);
                    objCommand.Parameters.AddWithValue("@asset_type_name", typeName);
                    objCommand.Parameters.AddWithValue("@description", description);
                    objCommand.Parameters.AddWithValue("@modified_by", ModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool DeleteAssetType(int assetTypeId, string originator)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spDeleteAssetType";

                    objCommand.Parameters.AddWithValue("@asset_type_id", assetTypeId);
                    objCommand.Parameters.AddWithValue("@modified_by", originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool IsAssetTypeAvailable(string assetType)
        {
            bool isAvailable = true;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spIsAssetTypeAvailable";

                    objCommand.Parameters.AddWithValue("@assetType", assetType);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    int rtn = (int)objCommand.Parameters["@ReturnVal"].Value;
                    if (rtn == 0)
                    {
                        isAvailable = false;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return isAvailable;
        }


        #endregion

        #region - Asset Master -

        public List<AssetModel> GetAllAssets(ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<AssetModel> assetList = new List<AssetModel>();
            AssetModel assetEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllAsset";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        assetEntity = new AssetModel();

                        if (item["asset_id"] != DBNull.Value) assetEntity.AssetId = Convert.ToInt32(item["asset_id"]);
                        if (item["asset_type_id"] != DBNull.Value) assetEntity.AssetTypeId = Convert.ToInt32(item["asset_type_id"]);
                        if (item["asset_type_name"] != DBNull.Value) assetEntity.AssetTypeName = item["asset_type_name"].ToString();
                        if (item["asset_code"] != DBNull.Value) assetEntity.AssetCode = item["asset_code"].ToString();
                        if (item["asset_model"] != DBNull.Value) assetEntity.Model = item["asset_model"].ToString();
                        if (item["imei_1"] != DBNull.Value) assetEntity.IMEI_1 = item["imei_1"].ToString();
                        if (item["imei_2"] != DBNull.Value) assetEntity.IMEI_2 = item["imei_2"].ToString();
                        if (item["serial_no"] != DBNull.Value) assetEntity.SerialNo = item["serial_no"].ToString();
                        if (item["company_asset_code"] != DBNull.Value) assetEntity.CompAssetCode = item["company_asset_code"].ToString();
                        if (item["printer_pouch"] != DBNull.Value) assetEntity.PrinterPouch = item["printer_pouch"].ToString();
                        if (item["monitor_model"] != DBNull.Value) assetEntity.MonitorModel = item["monitor_model"].ToString();
                        if (item["monitor_serial"] != DBNull.Value) assetEntity.MonitorModel = item["monitor_serial"].ToString();
                        if (item["sim_no"] != DBNull.Value) assetEntity.SIM = item["sim_no"].ToString();
                        if (item["supplier"] != DBNull.Value) assetEntity.Supplier = item["supplier"].ToString();
                        if (item["invoice_no"] != DBNull.Value) assetEntity.InvoiceNo = item["invoice_no"].ToString();
                        if (item["value_LKR"] != DBNull.Value) assetEntity.ValueLKR = item["value_LKR"].ToString();
                        if (item["purchase_date"] != DBNull.Value) assetEntity.PurchaseDate = Convert.ToDateTime(item["purchase_date"]);
                        if (item["warranty"] != DBNull.Value) assetEntity.Warranty = item["warranty"].ToString();
                        if (item["status"] != DBNull.Value) assetEntity.AsssetStatus = Convert.ToInt32(item["status"]);
                        if (item["description"] != DBNull.Value) assetEntity.Description = item["description"].ToString();

                        if (item["created_by"] != DBNull.Value) assetEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) assetEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        assetList.Add(assetEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return assetList;
        }

        public bool SaveAsset(AssetModel assetDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spSaveAsset";

                    objCommand.Parameters.AddWithValue("@asset_type_id", assetDetail.AssetTypeId);
                    objCommand.Parameters.AddWithValue("@asset_code", assetDetail.AssetCode);
                    objCommand.Parameters.AddWithValue("@asset_model", assetDetail.Model);
                    objCommand.Parameters.AddWithValue("@imei_1", assetDetail.IMEI_1);
                    objCommand.Parameters.AddWithValue("@imei_2", assetDetail.IMEI_2);
                    objCommand.Parameters.AddWithValue("@serial_no", assetDetail.SerialNo);
                    objCommand.Parameters.AddWithValue("@company_asset_code", assetDetail.CompAssetCode);
                    objCommand.Parameters.AddWithValue("@printer_pouch", assetDetail.PrinterPouch);
                    objCommand.Parameters.AddWithValue("@monitor_model", assetDetail.MonitorModel);
                    objCommand.Parameters.AddWithValue("@monitor_serial", assetDetail.MonitorSerial);
                    objCommand.Parameters.AddWithValue("@sim_no", assetDetail.SIM);
                    objCommand.Parameters.AddWithValue("@supplier", assetDetail.Supplier);
                    objCommand.Parameters.AddWithValue("@invoice_no", assetDetail.InvoiceNo);
                    objCommand.Parameters.AddWithValue("@value_LKR", assetDetail.ValueLKR);
                    objCommand.Parameters.AddWithValue("@purchase_date", assetDetail.PurchaseDate);
                    objCommand.Parameters.AddWithValue("@warranty", assetDetail.Warranty);
                    objCommand.Parameters.AddWithValue("@status", assetDetail.AsssetStatus);
                    objCommand.Parameters.AddWithValue("@description", assetDetail.Description);
                    objCommand.Parameters.AddWithValue("@created_by", assetDetail.CreatedBy);


                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool UpdateAsset(AssetModel assetDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateAsset";

                    objCommand.Parameters.AddWithValue("@asset_id", assetDetail.AssetId);
                    objCommand.Parameters.AddWithValue("@asset_type_id", assetDetail.AssetTypeId);
                    objCommand.Parameters.AddWithValue("@asset_code", assetDetail.AssetCode);
                    objCommand.Parameters.AddWithValue("@asset_model", assetDetail.Model);
                    objCommand.Parameters.AddWithValue("@imei_1", assetDetail.IMEI_1);
                    objCommand.Parameters.AddWithValue("@imei_2", assetDetail.IMEI_2);
                    objCommand.Parameters.AddWithValue("@serial_no", assetDetail.SerialNo);
                    objCommand.Parameters.AddWithValue("@company_asset_code", assetDetail.CompAssetCode);
                    objCommand.Parameters.AddWithValue("@printer_pouch", assetDetail.PrinterPouch);
                    objCommand.Parameters.AddWithValue("@monitor_model", assetDetail.MonitorModel);
                    objCommand.Parameters.AddWithValue("@monitor_serial", assetDetail.MonitorSerial);
                    objCommand.Parameters.AddWithValue("@sim_no", assetDetail.SIM);
                    objCommand.Parameters.AddWithValue("@supplier", assetDetail.Supplier);
                    objCommand.Parameters.AddWithValue("@invoice_no", assetDetail.InvoiceNo);
                    objCommand.Parameters.AddWithValue("@value_LKR", assetDetail.ValueLKR);
                    objCommand.Parameters.AddWithValue("@purchase_date", assetDetail.PurchaseDate);
                    objCommand.Parameters.AddWithValue("@warranty", assetDetail.Warranty);
                    objCommand.Parameters.AddWithValue("@status", assetDetail.AsssetStatus);
                    objCommand.Parameters.AddWithValue("@description", assetDetail.Description);
                    objCommand.Parameters.AddWithValue("@modified_by", assetDetail.LastModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool DeleteAsset(int assetId, string originator)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spDeleteAsset";

                    objCommand.Parameters.AddWithValue("@asset_id", assetId);
                    objCommand.Parameters.AddWithValue("@Modified_By", originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        //public bool UploadExcelfile(string FilePath, string UserName)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();

        //        using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
        //        {
        //            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        //            IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        //            string relationshipId = sheets.First().Id.Value;
        //            WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        //            Worksheet workSheet = worksheetPart.Worksheet;
        //            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        //            IEnumerable<Row> rows = sheetData.Descendants<Row>();

        //            foreach (Cell cell in rows.ElementAt(0))
        //            {
        //                dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
        //            }


        //            foreach (Row row in rows)
        //            {
        //                DataRow tempRow = dt.NewRow();
        //                try
        //                {
        //                    int columnIndex = 0;
        //                    foreach (Cell cell in row.Descendants<Cell>())
        //                    {
        //                        int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
        //                        cellColumnIndex--;
        //                        if (columnIndex < cellColumnIndex)
        //                        {
        //                            do
        //                            {
        //                                tempRow[columnIndex] = "";
        //                                columnIndex++;
        //                            }
        //                            while (columnIndex < cellColumnIndex);
        //                        }
        //                        tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

        //                        columnIndex++;
        //                    }

        //                    if (tempRow[0].ToString() != "")
        //                        dt.Rows.Add(tempRow);
        //                }
        //                catch (Exception)
        //                {
        //                    throw;
        //                }
        //            }
        //        }

        //        dt.Rows.RemoveAt(0);
        //        dt.AcceptChanges();
        //        // Add columns
        //        dt.Columns.Add(new DataColumn("UserID", typeof(System.String)));
        //        dt.Columns.Add(new DataColumn("PurchDate", typeof(DateTime)));
        //        dt.Columns.Add(new DataColumn("AssetTypeID", typeof(System.Int32)));

        //        // Assign values
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            dr["UserID"] = UserName;
        //            string b = dr["Purchase Date"].ToString();
        //            double d = double.Parse(b);
        //            DateTime conv = DateTime.FromOADate(d);
        //            dr["PurchDate"] = conv;

        //            if (dr["Asset Type"] != DBNull.Value)
        //            {
        //                dr["AssetTypeID"] = GetAssetTypeId(dr["Asset Type"].ToString());// Convert.ToInt32(dr["Asset Type"]);
        //            }
        //        }

        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();

        //        if (dt.Rows.Count > 0)
        //        {
        //            using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
        //            {
        //                conn.Open();
        //                using (SqlTransaction sqlTransaction = conn.BeginTransaction())
        //                {
        //                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, sqlTransaction))
        //                    {
        //                        //Set the database table name
        //                        sqlBulkCopy.DestinationTableName = "dbo.asset";

        //                        //[OPTIONAL]: Map the DataTable columns with that of the database table
        //                        //sqlBulkCopy.ColumnMappings.Add("Asset Type","asset_type_id");
        //                        sqlBulkCopy.ColumnMappings.Add("AssetTypeID", "asset_type_id");
        //                        sqlBulkCopy.ColumnMappings.Add("Asset Code", "asset_code");
        //                        sqlBulkCopy.ColumnMappings.Add("Model", "asset_model");
        //                        sqlBulkCopy.ColumnMappings.Add("IMEI Number", "imei_1");
        //                        sqlBulkCopy.ColumnMappings.Add("IMEI Number 2", "imei_2");
        //                        sqlBulkCopy.ColumnMappings.Add("Value LKR", "value_LKR");
        //                        sqlBulkCopy.ColumnMappings.Add("PurchDate", "purchase_date");
        //                        sqlBulkCopy.ColumnMappings.Add("Serial #", "serial_no");
        //                        sqlBulkCopy.ColumnMappings.Add("Supplier", "supplier");
        //                        sqlBulkCopy.ColumnMappings.Add("Invoice #", "invoice_no");
        //                        sqlBulkCopy.ColumnMappings.Add("Warranty", "warranty");
        //                        sqlBulkCopy.ColumnMappings.Add("Company Asset Code", "company_asset_code");
        //                        sqlBulkCopy.ColumnMappings.Add("Printer Pouch", "printer_pouch");
        //                        sqlBulkCopy.ColumnMappings.Add("Monitor Model", "monitor_model");
        //                        sqlBulkCopy.ColumnMappings.Add("Monitor Serial #", "monitor_serial");
        //                        sqlBulkCopy.ColumnMappings.Add("SIM Number", "sim_no");
        //                        sqlBulkCopy.ColumnMappings.Add("UserID", "created_by");

        //                        try
        //                        {
        //                            sqlBulkCopy.WriteToServer(dt);
        //                            sqlTransaction.Commit();
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            sqlTransaction.Rollback();
        //                            throw ex;
        //                        }
        //                    }
        //                }

        //                conn.Close();
        //            }
        //        }

        //        return true;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        public bool UploadExcelfile(string FilePath, string UserName, string logPath)
        {
            LogError("AssetDAO - 1");

            try
            {
                bool status = false;
                DataTable dt = new DataTable();
                DataTable objDS = new DataTable();

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                    }

                    foreach (Row row in rows)
                    {
                        DataRow tempRow = dt.NewRow();
                        try
                        {
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            if (tempRow[0].ToString() != "")
                                dt.Rows.Add(tempRow);
                        }
                        catch (Exception ex) { }
                    }
                }

                dt.Rows.RemoveAt(0);
                dt.AcceptChanges();
                // Add columns
                dt.Columns.Add(new DataColumn("UserID", typeof(System.String)));
                dt.Columns.Add(new DataColumn("PurchDate", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("AssetTypeID", typeof(System.Int32)));

                // Assign values
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UserID"] = UserName;
                    string b = dr["PurchaseDate"].ToString();
                    double d = double.Parse(b);
                    DateTime conv = DateTime.FromOADate(d);
                    dr["PurchDate"] = conv;

                    if (dr["AssetType"] != DBNull.Value)
                    {
                        try
                        {
                            dr["AssetTypeID"] = GetAssetTypeId(dr["AssetType"].ToString());// Convert.ToInt32(dr["Asset Type"]);
                            LogError("AssetDAO - 2 - " + dr["AssetType"].ToString() + " - " + dr["AssetTypeID"].ToString());
                        }
                        catch (Exception ex)
                        {
                            dr["AssetTypeID"] = 0;
                        }
                    }
                }

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (dt.Rows.Count > 0)
                {
                    using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                    {
                        SqlCommand objCommand = new SqlCommand();
                        objCommand.Connection = conn;
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.CommandText = "spUploadAssetByExcel";

                        objCommand.Parameters.AddWithValue("@tblAssets", dt);

                        //Get list of data which alredy exist in the table
                        SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                        objAdap.Fill(objDS);
                        WriteAssetLog(objDS, logPath);
                    }

                    status = true;
                }

                return status;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int GetAssetTypeId(string assetType)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAssetTypeId";

                    objCommand.Parameters.AddWithValue("@assetType", assetType);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    objCommand.ExecuteNonQuery();
                    int rtn = (int)objCommand.Parameters["@ReturnVal"].Value;
                    //var result = returnParameter.Value;

                    return rtn;
                }
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public bool IsAssetCodeAvailable(string assetCode)
        {
            bool isAvailable = true;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spIsAssetCodeAvailable";

                    objCommand.Parameters.AddWithValue("@assetCode", assetCode);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    int rtn = (int)objCommand.Parameters["@ReturnVal"].Value;
                    if (rtn == 0)
                    {
                        isAvailable = false;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return isAvailable;
        }

        public static void WriteAssetLog(DataTable strLog, string logPath)
        {
            try
            {
                string logFilePath = logPath + "AssetUpload-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + "txt";

                FileInfo logFileInfo = new FileInfo(logFilePath);
                DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();
                File.WriteAllText(logFilePath, String.Empty);

                if (strLog.Rows.Count > 0)
                {

                    using (FileStream fileStream = new FileStream(logFilePath, FileMode.Append))
                    {
                        using (StreamWriter log = new StreamWriter(fileStream))
                        {
                            log.Write("Log Entry : Asset Detail ");
                            log.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                            log.WriteLine("--------------");
                            log.WriteLine(strLog.Rows.Count + " Records Not Inserted.");
                            log.WriteLine("--------------");
                            foreach (DataRow row in strLog.Rows)
                            {
                                string assetType = row["AssetType"].ToString();
                                string assetCode = row["AssetCode"].ToString();
                                log.WriteLine("{0} - {1}", assetType, assetCode);
                            }

                        }
                    }
                }
                else
                {
                    //Do nothing
                }

            }
            catch (Exception)
            {

                throw;
            }
        }


        #endregion

        #region - Assign Assets-

        public List<AssetAssignModel> GetAllAssignAssets(ArgsModel args, string Token)
        {
            DataTable objDS = new DataTable();
            List<AssetAssignModel> assetAssignList = new List<AssetAssignModel>();
            AssetAssignModel assignEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllAssignAssets";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@token", Token);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        assignEntity = new AssetAssignModel();

                        if (item["assign_id"] != DBNull.Value) assignEntity.AssignId = Convert.ToInt32(item["assign_id"]);
                        if (item["user_type"] != DBNull.Value) assignEntity.UserType = item["user_type"].ToString();
                        if (item["user_code"] != DBNull.Value) assignEntity.UserCode = item["user_code"].ToString();
                        if (item["name"] != DBNull.Value) assignEntity.UserName = item["name"].ToString();
                        if (item["asset_type"] != DBNull.Value) assignEntity.AssetTypeId = Convert.ToInt32(item["asset_type"]);
                        if (item["asset_type_name"] != DBNull.Value) assignEntity.AssetTypeName = item["asset_type_name"].ToString();
                        if (item["asset_code"] != DBNull.Value) assignEntity.AssetCode = item["asset_code"].ToString();
                        if (item["refund_cost"] != DBNull.Value) assignEntity.RefundCost = Convert.ToInt32(item["refund_cost"]);
                        if (item["status"] != DBNull.Value) assignEntity.Status = Convert.ToInt32(item["status"]);
                        if (item["assign_date"] != DBNull.Value) assignEntity.AssignDate = Convert.ToDateTime(item["assign_date"]);
                        if (item["return_date"] != DBNull.Value) assignEntity.ReturnDate = Convert.ToDateTime(item["return_date"]);
                        if (item["agreement"] != DBNull.Value) assignEntity.Agreement = item["agreement"].ToString();
                        if (item["remark"] != DBNull.Value) assignEntity.Remark = item["remark"].ToString();

                        if (item["created_by"] != DBNull.Value) assignEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) assignEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        assetAssignList.Add(assignEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return assetAssignList;
        }

        public bool SaveAssignAsset(AssetAssignModel assignDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spSaveAssignAsset"; //NO SP

                    objCommand.Parameters.AddWithValue("@user_type", assignDetail.UserType);
                    objCommand.Parameters.AddWithValue("@user_code", assignDetail.UserCode);
                    objCommand.Parameters.AddWithValue("@asset_type", assignDetail.AssetTypeId);
                    objCommand.Parameters.AddWithValue("@asset_code", assignDetail.AssetCode);
                    objCommand.Parameters.AddWithValue("@refund_cost", assignDetail.RefundCost);
                    objCommand.Parameters.AddWithValue("@status", assignDetail.Status);
                    objCommand.Parameters.AddWithValue("@assign_date", assignDetail.AssignDate);
                    objCommand.Parameters.AddWithValue("@return_date", assignDetail.ReturnDate);
                    objCommand.Parameters.AddWithValue("@agreement", assignDetail.Agreement);
                    objCommand.Parameters.AddWithValue("@remark", assignDetail.Remark);
                    objCommand.Parameters.AddWithValue("@created_by", assignDetail.CreatedBy);


                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool SaveAssignAssetList(string Originator, List<AssetAssignModel> data)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    if (conn.State == ConnectionState.Closed) { conn.Open(); }
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        try
                        {
                            foreach (var item in data)
                            {
                                SqlCommand objCommand = new SqlCommand();
                                objCommand.Connection = conn;
                                objCommand.CommandType = CommandType.StoredProcedure;
                                objCommand.CommandText = "spSaveUpdateAssignAsset";
                                objCommand.Transaction = sqlTransaction;

                                if (item.ReturnDate > DateTime.MinValue)
                                {
                                    objCommand.Parameters.AddWithValue("@return_date", item.ReturnDate);
                                }
                                if (Double.IsNaN(item.RefundCost))
                                {
                                    item.RefundCost = 0;
                                }
                                item.AssetTypeId = GetAssetTypeIdFrmAssetName(item.AssetTypeName);

                                objCommand.Parameters.AddWithValue("@user_type", item.UserType);
                                objCommand.Parameters.AddWithValue("@user_code", item.UserCode);
                                objCommand.Parameters.AddWithValue("@asset_type", item.AssetTypeId);
                                objCommand.Parameters.AddWithValue("@asset_code", item.AssetCode);
                                objCommand.Parameters.AddWithValue("@refund_cost", item.RefundCost);
                                objCommand.Parameters.AddWithValue("@status", item.Status);
                                objCommand.Parameters.AddWithValue("@other_status", item.OtherStatus);
                                objCommand.Parameters.AddWithValue("@assign_date", item.AssignDate);
                                objCommand.Parameters.AddWithValue("@agreement", item.Agreement);
                                objCommand.Parameters.AddWithValue("@remark", item.Remark);
                                objCommand.Parameters.AddWithValue("@created_by", Originator);


                                if (objCommand.ExecuteNonQuery() > 0)
                                {
                                    retStatus = true;
                                }
                                else
                                {
                                    retStatus = false;
                                }
                            }
                            sqlTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            sqlTransaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return retStatus;

        }

        public bool UpdateAssignAsset(AssetAssignModel assignDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateAssignAsset"; //NO SP

                    objCommand.Parameters.AddWithValue("@assign_id", assignDetail.AssignId);
                    objCommand.Parameters.AddWithValue("@user_type", assignDetail.UserType);
                    objCommand.Parameters.AddWithValue("@user_code", assignDetail.UserCode);
                    objCommand.Parameters.AddWithValue("@asset_type", assignDetail.AssetTypeId);
                    objCommand.Parameters.AddWithValue("@asset_code", assignDetail.AssetCode);
                    objCommand.Parameters.AddWithValue("@refund_cost", assignDetail.RefundCost);
                    objCommand.Parameters.AddWithValue("@status", assignDetail.Status);
                    objCommand.Parameters.AddWithValue("@assign_date", assignDetail.AssignDate);
                    objCommand.Parameters.AddWithValue("@return_date", assignDetail.ReturnDate);
                    objCommand.Parameters.AddWithValue("@agreement", assignDetail.Agreement);
                    objCommand.Parameters.AddWithValue("@remark", assignDetail.Remark);
                    objCommand.Parameters.AddWithValue("@created_by", assignDetail.CreatedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool DeleteAssignAsset(int assetAssignId, string originator)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spDeleteAssignAsset";

                    objCommand.Parameters.AddWithValue("@assign_id", assetAssignId);
                    objCommand.Parameters.AddWithValue("@modified_by", originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        //public bool UploadAssetAssignDetails(string FilePath, string UserName)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();

        //        using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
        //        {
        //            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        //            IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        //            string relationshipId = sheets.First().Id.Value;
        //            WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        //            Worksheet workSheet = worksheetPart.Worksheet;
        //            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        //            IEnumerable<Row> rows = sheetData.Descendants<Row>();

        //            foreach (Cell cell in rows.ElementAt(0))
        //            {
        //                dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
        //            }

        //            foreach (Row row in rows)
        //            {
        //                DataRow tempRow = dt.NewRow();
        //                try
        //                {
        //                    int columnIndex = 0;
        //                    foreach (Cell cell in row.Descendants<Cell>())
        //                    {
        //                        int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
        //                        cellColumnIndex--;
        //                        if (columnIndex < cellColumnIndex)
        //                        {
        //                            do
        //                            {
        //                                tempRow[columnIndex] = "";
        //                                columnIndex++;
        //                            }
        //                            while (columnIndex < cellColumnIndex);
        //                        }
        //                        tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

        //                        columnIndex++;
        //                    }

        //                    if (tempRow[0].ToString() != "")
        //                        dt.Rows.Add(tempRow);
        //                }
        //                catch (Exception)
        //                {
        //                    throw;
        //                }
        //            }

        //        }

        //        dt.Rows.RemoveAt(0);
        //        dt.AcceptChanges();
        //        //Row Changes--
        //        dt.Columns.Add(new DataColumn("UserID", typeof(System.String)));
        //        dt.Columns.Add(new DataColumn("UserType", typeof(System.String)));
        //        dt.Columns.Add(new DataColumn("AssetTypeId", typeof(System.Int32)));
        //        //dt.Columns.Add(new DataColumn("PurchDate", typeof(DateTime)));
        //        dt.Columns.Add(new DataColumn("AssignDate", typeof(DateTime)));
        //        dt.Columns.Add(new DataColumn("ReturnDate", typeof(DateTime)));

        //        // Assign values
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            dr["UserID"] = UserName;
        //            if (!string.IsNullOrEmpty(dr["User Code"].ToString()))
        //            {
        //                dr["UserType"] = GetUserTypeFrmUserCode(dr["User Code"].ToString());
        //            }

        //            if (!string.IsNullOrEmpty(dr["Asset Code"].ToString()))
        //            {
        //                dr["AssetTypeId"] = GetAssetTypeIdFrmAssetCode(dr["Asset Code"].ToString());
        //            }

        //            if (!string.IsNullOrEmpty(dr["Handover Date"].ToString()))
        //            {
        //                string b = dr["Handover Date"].ToString();
        //                double d = double.Parse(b);
        //                DateTime conv = DateTime.FromOADate(d);
        //                dr["AssignDate"] = conv;
        //            }

        //            if (!string.IsNullOrEmpty(dr["Returned to Company Date"].ToString()))
        //            {
        //                string tmp = dr["Returned to Company Date"].ToString();
        //                double rtn = double.Parse(tmp);
        //                DateTime rtndt = DateTime.FromOADate(rtn);
        //                dr["ReturnDate"] = rtndt;
        //            }
        //        }

        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();

        //        if (dt.Rows.Count > 0)
        //        {
        //            using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
        //            {
        //                if (conn.State == ConnectionState.Closed) { conn.Open(); }
        //                using (SqlTransaction sqlTransaction = conn.BeginTransaction())
        //                {
        //                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, sqlTransaction))
        //                    {
        //                        //Set the database table name
        //                        sqlBulkCopy.DestinationTableName = "dbo.asset_assign";

        //                        //[OPTIONAL]: Map the DataTable columns with that of the database table
        //                        sqlBulkCopy.ColumnMappings.Add("UserType", "user_type");
        //                        sqlBulkCopy.ColumnMappings.Add("User Code", "user_code");
        //                        sqlBulkCopy.ColumnMappings.Add("AssetTypeId", "asset_type");
        //                        sqlBulkCopy.ColumnMappings.Add("Asset Code", "asset_code");
        //                        sqlBulkCopy.ColumnMappings.Add("Status", "status");
        //                        sqlBulkCopy.ColumnMappings.Add("AssignDate", "assign_date");
        //                        sqlBulkCopy.ColumnMappings.Add("ReturnDate", "return_date");
        //                        sqlBulkCopy.ColumnMappings.Add("Agreement", "agreement");
        //                        sqlBulkCopy.ColumnMappings.Add("Remark", "remark");
        //                        sqlBulkCopy.ColumnMappings.Add("UserID", "created_by");

        //                        try
        //                        {
        //                            sqlBulkCopy.WriteToServer(dt);
        //                            sqlTransaction.Commit();
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            sqlTransaction.Rollback();
        //                            throw ex;
        //                        }
        //                    }

        //                    //Update asset table status to ASSIGN(2) for each assign asset
        //                    //----------------------------
        //                }

        //                conn.Close();
        //            }
        //        }

        //        return true;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        public bool UploadAssetAssignDetails(string FilePath, string UserName, string logPath, bool IsCustomer = false)
        {
            try
            {
                DataTable objDS = new DataTable();
                DataTable dt = new DataTable();

                bool status = false;
                int result;
                int assetTypeId = 0;
                string userexist = "";

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                    }

                    foreach (Row row in rows)
                    {
                        DataRow tempRow = dt.NewRow();
                        try
                        {
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            if (tempRow[0].ToString() != "")
                                dt.Rows.Add(tempRow);
                        }
                        catch (Exception) { }
                    }
                }

                dt.Rows.RemoveAt(0);
                dt.AcceptChanges();
                //Row Changes--
                dt.Columns.Add(new DataColumn("UserID", typeof(System.String)));
                dt.Columns.Add(new DataColumn("UserType", typeof(System.String)));
                dt.Columns.Add(new DataColumn("AssetTypeId", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("AssignDate", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("ReturnDate", typeof(DateTime)));
                dt.Columns.Add(new DataColumn("User_Code", typeof(System.String)));

                DataTable inavliedRows = new DataTable();
                inavliedRows = dt.Clone();

                // Assign values
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UserID"] = UserName;
                    if (!string.IsNullOrEmpty(dr["UserCode"].ToString()))
                    {
                        if (IsCustomer)
                            userexist = "CUST";
                        else
                            userexist = GetUserTypeFrmUserCode(dr["UserCode"].ToString());

                        dr["UserType"] = userexist;  // GetUserTypeFrmUserCode(dr["UserCode"].ToString());
                        dr["User_Code"] = dr["UserCode"].ToString();
                    }

                    if (!string.IsNullOrEmpty(dr["AssetCode"].ToString()))
                    {
                        assetTypeId = GetAssetTypeIdFrmAssetCode(dr["AssetCode"].ToString());
                        dr["AssetTypeId"] = assetTypeId;
                    }
 
                    try
                    {
                        if (!string.IsNullOrEmpty(dr["HandoverDate"].ToString()))
                        {
                            dr["AssignDate"] = DateTime.Parse(dr["HandoverDate"].ToString());
                        }
                        else
                        {
                            dr["AssignDate"] = DateTime.Now.Date.ToString();
                        }
                    }
                    catch { dr["AssignDate"] = DateTime.Now.Date.ToString(); }

                    try
                    {
                        if (!string.IsNullOrEmpty(dr["ReturnedToCompanyDate"].ToString()))
                        {
                            string tmp = dr["ReturnedtoCompanyDate"].ToString();

                            bool parsedSuccessfully = int.TryParse(tmp, out result);
                            if (parsedSuccessfully)
                            {
                                double rtn = double.Parse(tmp);
                                DateTime rtndt = DateTime.FromOADate(rtn);
                                dr["ReturnDate"] = rtndt;
                            }
                            else
                            {
                                dr["ReturnDate"] = SqlDateTime.MinValue.Value;
                            }
                        }
                    }
                    catch { dr["ReturnDate"] = SqlDateTime.MinValue.Value; }
  
                    if (assetTypeId == 0 || userexist == "")
                    {
                        inavliedRows.Rows.Add(dr.ItemArray);
                    }

                }

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (dt.Rows.Count > 0)
                {
                    using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                    {
                        SqlCommand objCommand = new SqlCommand();
                        objCommand.Connection = conn;
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.CommandText = "spUploadAssetAssignByExcel";

                        objCommand.Parameters.AddWithValue("@tblAssetAssign", dt);
                        objCommand.Parameters.AddWithValue("@tblInvaliedAssign", inavliedRows);

                        //Get list of data which alredy exist in the table
                        SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                        objAdap.Fill(objDS);
                        WriteAssetAssignLog(objDS, logPath);
                    }
                    status = true;
                }

                return status;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetAssetTypeIdFrmAssetCode(string assetCode)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAssetTypeIdFromAssetCode";

                    objCommand.Parameters.AddWithValue("@assetCode", assetCode);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    int rtn = (int)objCommand.Parameters["@ReturnVal"].Value;
                    //var result = returnParameter.Value;

                    return rtn;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetUserTypeFrmUserCode(string userCode)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetUserTypeFrmUserCode";

                    objCommand.Parameters.AddWithValue("@userCode", userCode);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.VarChar, 20);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    string rtn = objCommand.Parameters["@ReturnVal"].Value.ToString();
                    //var result = returnParameter.Value;

                    return rtn;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<OriginatorModel> GetUserCodesByUserType(string userType)
        {
            DataTable objDS = new DataTable();
            List<OriginatorModel> originatorList = new List<OriginatorModel>();
            OriginatorModel originatorEntity = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllUsersByUserType";

                    objCommand.Parameters.AddWithValue("@userType", userType);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                    //return objDS;
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        originatorEntity = new OriginatorModel();

                        if (item["originator"] != DBNull.Value) originatorEntity.Originator = item["originator"].ToString();
                        if (item["name"] != DBNull.Value) originatorEntity.Name = item["name"].ToString();
                        originatorList.Add(originatorEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return originatorList;
        }

        public List<AssetModel> GetAssetsCodesByAssetType(string assetType)
        {
            DataTable objDS = new DataTable();
            List<AssetModel> assetList = new List<AssetModel>();
            AssetModel assetEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllAssetsByAssetType";

                    objCommand.Parameters.AddWithValue("@assetType", assetType);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        assetEntity = new AssetModel();

                        if (item["asset_id"] != DBNull.Value) assetEntity.AssetId = Convert.ToInt32(item["asset_id"]);
                        if (item["asset_code"] != DBNull.Value) assetEntity.AssetCode = item["asset_code"].ToString();
                        if (item["serial_no"] != DBNull.Value) assetEntity.SerialNo = item["serial_no"].ToString();
                        assetList.Add(assetEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return assetList;
        }

        public List<AssetAssignModel> GetAllAssignAssetsByUser(string userCode)
        {
            DataTable objDS = new DataTable();
            List<AssetAssignModel> assetAssignList = new List<AssetAssignModel>();
            AssetAssignModel assignEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllAssignAssetsByUserCode";

                    objCommand.Parameters.AddWithValue("@userCode", userCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        assignEntity = new AssetAssignModel();

                        if (item["assign_id"] != DBNull.Value) assignEntity.AssignId = Convert.ToInt32(item["assign_id"]);
                        if (item["user_type"] != DBNull.Value) assignEntity.UserType = item["user_type"].ToString();
                        if (item["user_code"] != DBNull.Value) assignEntity.UserCode = item["user_code"].ToString();
                        if (item["name"] != DBNull.Value) assignEntity.UserName = item["name"].ToString();
                        if (item["asset_type"] != DBNull.Value) assignEntity.AssetTypeId = Convert.ToInt32(item["asset_type"]);
                        if (item["asset_type_name"] != DBNull.Value) assignEntity.AssetTypeName = item["asset_type_name"].ToString();
                        if (item["asset_code"] != DBNull.Value) assignEntity.AssetCode = item["asset_code"].ToString();
                        if (item["refund_cost"] != DBNull.Value) assignEntity.RefundCost = Convert.ToInt32(item["refund_cost"]);
                        if (item["status"] != DBNull.Value) assignEntity.Status = Convert.ToInt32(item["status"]);
                        if (item["other_status"] != DBNull.Value) assignEntity.OtherStatus = Convert.ToInt32(item["other_status"]);
                        if (item["assign_date"] != DBNull.Value) assignEntity.AssignDate = Convert.ToDateTime(item["assign_date"]);
                        if (item["return_date"] != DBNull.Value) assignEntity.ReturnDate = Convert.ToDateTime(item["return_date"]);
                        if (item["agreement"] != DBNull.Value) assignEntity.Agreement = item["agreement"].ToString();
                        if (item["remark"] != DBNull.Value) assignEntity.Remark = item["remark"].ToString();

                        if (item["created_by"] != DBNull.Value) assignEntity.CreatedBy = item["created_by"].ToString();
                        assetAssignList.Add(assignEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return assetAssignList;
        }

        public List<AssetAssignModel> GetAllAssignAssetsByAssetCode(string assetCode)
        {
            DataTable objDS = new DataTable();
            List<AssetAssignModel> assetAssignList = new List<AssetAssignModel>();
            AssetAssignModel assignEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllAssignAssetsByAssetCode";

                    objCommand.Parameters.AddWithValue("@assetCode", assetCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        assignEntity = new AssetAssignModel();

                        if (item["assign_id"] != DBNull.Value) assignEntity.AssignId = Convert.ToInt32(item["assign_id"]);
                        if (item["user_type"] != DBNull.Value) assignEntity.UserType = item["user_type"].ToString();
                        if (item["user_code"] != DBNull.Value) assignEntity.UserCode = item["user_code"].ToString();
                        if (item["name"] != DBNull.Value) assignEntity.UserName = item["name"].ToString();
                        if (item["asset_type"] != DBNull.Value) assignEntity.AssetTypeId = Convert.ToInt32(item["asset_type"]);
                        if (item["asset_type_name"] != DBNull.Value) assignEntity.AssetTypeName = item["asset_type_name"].ToString();
                        if (item["asset_code"] != DBNull.Value) assignEntity.AssetCode = item["asset_code"].ToString();
                        if (item["refund_cost"] != DBNull.Value) assignEntity.RefundCost = Convert.ToInt32(item["refund_cost"]);
                        if (item["status"] != DBNull.Value) assignEntity.Status = Convert.ToInt32(item["status"]);
                        if (item["assign_date"] != DBNull.Value) assignEntity.AssignDate = Convert.ToDateTime(item["assign_date"]);
                        if (item["return_date"] != DBNull.Value) assignEntity.ReturnDate = Convert.ToDateTime(item["return_date"]);
                        if (item["agreement"] != DBNull.Value) assignEntity.Agreement = item["agreement"].ToString();
                        if (item["remark"] != DBNull.Value) assignEntity.Remark = item["remark"].ToString();

                        if (item["created_by"] != DBNull.Value) assignEntity.CreatedBy = item["created_by"].ToString();
                        assetAssignList.Add(assignEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return assetAssignList;
        }

        public List<AssetAssignModel> GetAllAssignAssetsByAssetId(int assetId)
        {
            DataTable objDS = new DataTable();
            List<AssetAssignModel> assetAssignList = new List<AssetAssignModel>();
            AssetAssignModel assignEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllAssignAssetsByAssetId";

                    objCommand.Parameters.AddWithValue("@assetId", assetId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        assignEntity = new AssetAssignModel();

                        if (item["assign_id"] != DBNull.Value) assignEntity.AssignId = Convert.ToInt32(item["assign_id"]);
                        if (item["user_type"] != DBNull.Value) assignEntity.UserType = item["user_type"].ToString();
                        if (item["user_code"] != DBNull.Value) assignEntity.UserCode = item["user_code"].ToString();
                        if (item["name"] != DBNull.Value) assignEntity.UserName = item["name"].ToString();
                        if (item["asset_type"] != DBNull.Value) assignEntity.AssetTypeId = Convert.ToInt32(item["asset_type"]);
                        if (item["asset_type_name"] != DBNull.Value) assignEntity.AssetTypeName = item["asset_type_name"].ToString();
                        if (item["asset_code"] != DBNull.Value) assignEntity.AssetCode = item["asset_code"].ToString();
                        if (item["refund_cost"] != DBNull.Value) assignEntity.RefundCost = Convert.ToInt32(item["refund_cost"]);
                        if (item["status"] != DBNull.Value) assignEntity.Status = Convert.ToInt32(item["status"]);
                        if (item["assign_date"] != DBNull.Value) assignEntity.AssignDate = Convert.ToDateTime(item["assign_date"]);
                        if (item["return_date"] != DBNull.Value) assignEntity.ReturnDate = Convert.ToDateTime(item["return_date"]);
                        if (item["agreement"] != DBNull.Value) assignEntity.Agreement = item["agreement"].ToString();
                        if (item["remark"] != DBNull.Value) assignEntity.Remark = item["remark"].ToString();

                        if (item["created_by"] != DBNull.Value) assignEntity.CreatedBy = item["created_by"].ToString();
                        assetAssignList.Add(assignEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return assetAssignList;
        }

        public int GetAssetTypeIdFrmAssetName(string assetType)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAssetTypeIdFromAssetType";

                    objCommand.Parameters.AddWithValue("@assetType", assetType);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    int rtn = (int)objCommand.Parameters["@ReturnVal"].Value;
                    //var result = returnParameter.Value;

                    return rtn;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string LoadCustomerName(string custCode)
        {
            DataTable objDS = new DataTable();
            string userName = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetCustomerName";

                    objCommand.Parameters.AddWithValue("@custCode", custCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        if (item["name"] != DBNull.Value) userName = item["name"].ToString();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return userName;
        }

        public bool IsAssetAvailable(string assetCode, string cusCode)
        {
            bool isAvailable = true;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spIsAssetAvailable";

                    objCommand.Parameters.AddWithValue("@assetCode", assetCode);
                    objCommand.Parameters.AddWithValue("@cusCode", cusCode);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    int rtn = (int)objCommand.Parameters["@ReturnVal"].Value;
                    if (rtn == 0)
                    {
                        isAvailable = false;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return isAvailable;
        }

        public bool IsLostOrDamaged(string assetCode, string cusCode)
        {
            bool isDamaged = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spIsLostOrDamaged";

                    objCommand.Parameters.AddWithValue("@assetCode", assetCode);
                    //objCommand.Parameters.AddWithValue("@cusCode", cusCode);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    int rtn = (int)objCommand.Parameters["@ReturnVal"].Value;
                    if (rtn == 3)
                    {
                        isDamaged = true;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return isDamaged;
        }

        public static void WriteAssetAssignLog(DataTable strLog, string logPath)
        {
            try
            {
                string logFilePath = logPath + "AssetAssignUpload-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + "txt";

                FileInfo logFileInfo = new FileInfo(logFilePath);
                DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();
                File.WriteAllText(logFilePath, String.Empty);

                if (strLog.Rows.Count > 0)
                {
                    using (FileStream fileStream = new FileStream(logFilePath, FileMode.Append))
                    {
                        using (StreamWriter log = new StreamWriter(fileStream))
                        {
                            log.Write("Log Entry : Asset Assign Detail ");
                            log.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                                DateTime.Now.ToLongDateString());
                            log.WriteLine("------------");
                            log.WriteLine(strLog.Rows.Count + " Records Not Inserted.");
                            log.WriteLine("------------");
                            foreach (DataRow row in strLog.Rows)
                            {
                                string assetCode = row["AssetCode"].ToString();
                                string userCode = row["User_Code"].ToString();
                                log.WriteLine("{0} - {1}", assetCode, userCode);
                            }

                        }
                    }
                }
                else
                {
                    //Do Nothiing
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void LogError(string error)
        {
            string logFilePath = @"D:\Logs\ErrorLog_AssetDAO_" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";

            try
            {
                // Check if file already exists. If yes, delete it. 
                using (StreamWriter sw = new StreamWriter(logFilePath, true))
                {
                    string sr = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff tt") + "  " + error + Environment.NewLine;
                    sw.Write(sr);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        #endregion

        #region - Asset Status -

        public List<AssetStatusModel> GetAllAssetStatus()
        {
            DataTable objDS = new DataTable();
            List<AssetStatusModel> statusList = new List<AssetStatusModel>();
            AssetStatusModel statusEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllAssetStatus";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        statusEntity = new AssetStatusModel();

                        if (item["status_id"] != DBNull.Value) statusEntity.StatusId = Convert.ToInt32(item["status_id"]);
                        if (item["status_name"] != DBNull.Value) statusEntity.StatusName = item["status_name"].ToString();

                        statusList.Add(statusEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return statusList;
        }

        #endregion

        #region - Common Methods -

        //--using openXml
        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            try
            {
                SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
                string value = cell.CellValue.InnerXml;
                if (cell.CellValue == null)
                {
                    return "";
                }
                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    return value;
                }
            }
            catch (Exception ex) { return ""; }
        }

        public static int? GetColumnIndexFromName(string columnName)
        {
            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        #endregion

        #region - Asset Agreement -

        public List<AssetModel> GetAssertsForAgreement(string user, string assert)
        {
            DataTable objDS = new DataTable();
            List<AssetModel> assertList = new List<AssetModel>();
            AssetModel assetEntity = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAssignAssertForAgreement";

                    objCommand.Parameters.AddWithValue("@user", user);
                    objCommand.Parameters.AddWithValue("@assert", assert);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                    //return objDS;
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        assetEntity = new AssetModel();
                        if (item["asset_code"] != DBNull.Value) assetEntity.AssetCode = item["asset_code"].ToString();
                        if (item["serial_no"] != DBNull.Value) assetEntity.SerialNo = item["serial_no"].ToString();
                        assertList.Add(assetEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return assertList;
        }

        #endregion
    }
}
