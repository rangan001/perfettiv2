﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common;
using Peercore.DataAccess.Common.Utilties;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using System.IO;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public abstract class BaseDAO
    {
        protected DbDataAccessHandle DataAcessService { get; set; }

        private DbSqlAdapter CommonSql { get; set; }

        public DbWorkflowScope WorkflowScope
        {
            get
            {
                DbWorkflowScope scope = new DbWorkflowScope(ApplicationService.Instance.ConnectionString, ApplicationService.Instance.DbProvider);
                return scope;
            }
        }

        public BaseDAO()
        {
            this.DataAcessService = new DbDataServiceHandle(ApplicationService.Instance.ConnectionString, ApplicationService.Instance.DbProvider);
            RegisterSql();
        }

        /// <summary>
        /// Determines whether this instance can delete the specified entity key value pair.
        /// </summary>
        /// <param name="entityKeyValuePair">The entity key value pair.</param>
        /// <param name="entityTable">The entity table.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can delete the specified entity key value pair; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool CanDelete(KeyValuePair<string, object> entityKeyValuePair, string entityTable)
        {
            DbDataReader dr = null;
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            try
            {
                paramCollection.Add("@columnName", entityKeyValuePair.Key, DbType.String);
                paramCollection.Add("@tableName", entityTable, DbType.String);
                paramCollection.Add("@tableColumn", entityKeyValuePair.Key, DbType.String);
                paramCollection.Add("@columnValue", entityKeyValuePair.Value, DbType.Int32);

                dr = DataAcessService.ExecuteQuery(CommonSql["CheckExistingCount"], paramCollection);

                if (dr.HasRows && dr.Read())
                {
                    if (!dr.IsDBNull(dr.GetOrdinal("TotalReferences")) && dr.GetInt32(dr.GetOrdinal("TotalReferences")) > 0)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null)
                    dr.Close();
            }

        }

        /// <summary>
        /// Determines whether this instance can delete the specified entity key value pair.
        /// </summary>
        /// <param name="entityKeyValuePair">The entity key value pair.</param>
        /// <param name="entityTableList">The entity table list.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can delete the specified entity key value pair; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool CanDelete(KeyValuePair<string, object> entityKeyValuePair, List<string> entityTableList)
        {
            DbDataReader dr = null;
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            try
            {
                foreach (string entityTable in entityTableList)
                {
                    paramCollection.Clear();

                    paramCollection.Add("@columnName", entityKeyValuePair.Key, DbType.String);
                    paramCollection.Add("@tableName", entityTable, DbType.String);
                    paramCollection.Add("@tableColumn", entityKeyValuePair.Key, DbType.String);
                    paramCollection.Add("@columnValue", entityKeyValuePair.Value, DbType.Int32);

                    dr = (DbDataReader)DataAcessService.ExecuteQuery(CommonSql["CheckExistingCount"], paramCollection);

                    if (dr.HasRows && dr.Read())
                    {
                        if (!dr.IsDBNull(dr.GetOrdinal("TotalReferences")) && dr.GetInt32(dr.GetOrdinal("TotalReferences")) > 0)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null)
                    dr.Close();
            }


        }

        /// <summary>
        /// Validates the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public virtual string ValidateString(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
                return value.Replace(@"'", @"''").Trim();
            else
                return string.Empty;
        }

        /// <summary>
        /// Gets the unique key.
        /// </summary>
        /// <param name="keyType">Type of the key.</param>
        /// <returns></returns>
        public int GetUniqueKey(string keyType)
        {
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@keyType", keyType, DbType.String);

            int effectedrecord = this.DataAcessService.ExecuteNonQuery(CommonSql["UpdateUniqueKey"], paramCollection);

            if (effectedrecord <= 0)
            {
                throw new Exception("Uniqe key updation failed.");
            }

            return (int)DataAcessService.GetOneValue(CommonSql["GetUniqueKey"], paramCollection);

        }

        public BaseDAO(DbWorkflowScope scope)
        {
            //this.DataAcessService = new DbWorkflowScope(scope.Connection.ConnectionString,ApplicationService.Instance.DbProvider);
            //RegisterSql();
            //this.DataAcessService = new DbDataServiceHandle(ApplicationService.Instance.ConnectionString, ApplicationService.Instance.DbProvider);
            //RegisterSql();

            this.DataAcessService = new Peercore.DataAccess.Common.DbTransactionHandle(scope.Connection, scope.Transaction, ApplicationService.Instance.DbProvider);
            RegisterSql();
        }

        private void RegisterSql()
        {
            this.CommonSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        protected string GetDefaultLookUpValue(string sTableId,string defaultDepartmentId)
        {
            DbDataReader idrLookup = null;
            try
            {
                string sDefaultValue = String.Empty;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@TableId", sTableId, DbType.String);
                paramCollection.Add("@DefaultValue", "Y", DbType.String);
                paramCollection.Add("@defaultDeptId", defaultDepartmentId, DbType.String);

                idrLookup = this.DataAcessService.ExecuteQuery(CommonSql["GetLookupEntries"], paramCollection);
                if (idrLookup != null && idrLookup.HasRows)
                {
                    int tableCodeOrdinal = idrLookup.GetOrdinal("table_code");
                    while (idrLookup.Read())
                    {
                        sDefaultValue = idrLookup.GetString(tableCodeOrdinal);
                        break;
                    }
                }
                return sDefaultValue;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idrLookup != null)
                    idrLookup.Close();
            }
        }


        public SqlConnection SqlConnOpen(DbWorkflowScope scope)
        {
            SqlConnection conn = new SqlConnection(scope.ConnectionString);
            if (conn.State == ConnectionState.Closed) { conn.Open(); }
            return conn;
        }
    }
}
