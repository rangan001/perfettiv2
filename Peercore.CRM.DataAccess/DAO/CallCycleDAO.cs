﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class CallCycleDAO : BaseDAO
    {
        private DbSqlAdapter CallCycleSql { get; set; }
    //    private DbSqlAdapter CustomerSql { get; set; }

        private void RegisterSql()
        {
            this.CallCycleSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CallCycleSql.xml", ApplicationService.Instance.DbProvider);
  //          this.CustomerSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CustomerSql.xml", ApplicationService.Instance.DbProvider);
        }

        public CallCycleDAO()
        {
            RegisterSql();
        }

        public CallCycleDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public CallCycleEntity CreateObject()
        {
            return CallCycleEntity.CreateObject();
        }

        public CallCycleEntity CreateObject(int entityId)
        {
            return CallCycleEntity.CreateObject(entityId);
        }

        //OK
        public bool GetCallCycleState(int callCycleId)
        {
            string strResult = "N";
            SqlCommand objCommand = new SqlCommand();
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetCallCycleState";

                    objCommand.Parameters.AddWithValue("@CallCycleId", callCycleId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    strResult = objCommand.ExecuteScalar().ToString();
                }
            }
            catch (Exception ex)
            {
                //   strResult = "DB Connection Failed";
            }

            return strResult.ToUpper().Equals("Y");
        }


        public List<CallCycleEntity> GetCallCycle(ArgsEntity args, bool bInActive = false, int iCallCycleID = 0, string CallCycleName = "")
        {
            //DbDataReader drCallCycle = null;
            //List<CallCycleEntity> callCycleCollection = new List<CallCycleEntity>();
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@Originator", args.Originator, DbType.String);
            //    paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "C.created_by"), DbType.String);
            //    paramCollection.Add("@InActive", bInActive ? 1 : 0, DbType.Boolean);
            //    paramCollection.Add("@CallCycleName", CallCycleName, DbType.String);
            //    paramCollection.Add("@CallCycleID", iCallCycleID, DbType.Int32);
            //    paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
            //    paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
            //    paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
            //    paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
            //    paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
            //    paramCollection.Add("@ShowCount", 0, DbType.Boolean);
            //    drCallCycle = this.DataAcessService.ExecuteQuery(CallCycleSql["GetCallCycles"], paramCollection);
            //    if (drCallCycle != null && drCallCycle.HasRows)
            //    {
            //        int callcycleIdOrdinal = drCallCycle.GetOrdinal("CallCycleID");
            //        int descriptionOrdinal = drCallCycle.GetOrdinal("Description");
            //        int dueonOrdinal = drCallCycle.GetOrdinal("DueOn");
            //        int commentsOrdinal = drCallCycle.GetOrdinal("comments");
            //        int createdByOrdinal = drCallCycle.GetOrdinal("created_by");
            //        int createdDateOrdinal = drCallCycle.GetOrdinal("created_date");
            //        int prefixCodeOrdinal = drCallCycle.GetOrdinal("prefix_code");
            //        int repNameOrdinal = drCallCycle.GetOrdinal("name");
            //        int totalCountOrdinal = drCallCycle.GetOrdinal("total_count");
            //        while (drCallCycle.Read())
            //        {
            //            CallCycleEntity callCycle = CreateObject();
            //            if (!drCallCycle.IsDBNull(callcycleIdOrdinal)) callCycle.CallCycleID = drCallCycle.GetInt32(callcycleIdOrdinal);
            //            if (!drCallCycle.IsDBNull(dueonOrdinal)) callCycle.DueOn = drCallCycle.GetDateTime(dueonOrdinal).ToLocalTime();
            //            if (!drCallCycle.IsDBNull(descriptionOrdinal)) callCycle.Description = drCallCycle.GetString(descriptionOrdinal);
            //            if (!drCallCycle.IsDBNull(commentsOrdinal)) callCycle.Comments = drCallCycle.GetString(commentsOrdinal);
            //            if (!drCallCycle.IsDBNull(createdByOrdinal)) callCycle.CreatedBy = drCallCycle.GetString(createdByOrdinal);
            //            if (!drCallCycle.IsDBNull(createdDateOrdinal)) callCycle.CreatedDate = drCallCycle.GetDateTime(createdDateOrdinal);
            //            if (!drCallCycle.IsDBNull(prefixCodeOrdinal)) callCycle.PrefixCode = drCallCycle.GetString(prefixCodeOrdinal);
            //            if (!drCallCycle.IsDBNull(repNameOrdinal)) callCycle.RepName = drCallCycle.GetString(repNameOrdinal);
            //            if (!drCallCycle.IsDBNull(totalCountOrdinal)) callCycle.TotalCount = drCallCycle.GetInt32(totalCountOrdinal);
            //            callCycleCollection.Add(callCycle);
            //        }
            //    }
            //    return callCycleCollection;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drCallCycle != null)
            //        drCallCycle.Close();
            //}

            DataTable objDS = new DataTable();
            List<CallCycleEntity> callCycleCollection = new List<CallCycleEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetCallCycles";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "C.created_by"));
                    objCommand.Parameters.AddWithValue("@InActive", bInActive ? 1 : 0);
                    objCommand.Parameters.AddWithValue("@CallCycleName", CallCycleName);
                    objCommand.Parameters.AddWithValue("@CallCycleID", iCallCycleID);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowCount", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            CallCycleEntity callCycle = CreateObject();

                            callCycle.CallCycleID = Convert.ToInt32(item["CallCycleID"]);
                            callCycle.DueOn = Convert.ToDateTime(item["DueOn"]);
                            callCycle.Description = item["Description"].ToString();
                            callCycle.Comments = item["comments"].ToString();
                            callCycle.CreatedBy = item["created_by"].ToString();
                            callCycle.CreatedDate = Convert.ToDateTime(item["created_date"]);
                            callCycle.PrefixCode = item["prefix_code"].ToString();
                            callCycle.RepName = item["name"].ToString();
                            callCycle.TotalCount = Convert.ToInt32(item["total_count"]);

                            callCycleCollection.Add(callCycle);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return callCycleCollection;
        }

        //OK
        public List<CallCycleContactEntity> GetAllCallCycles(ArgsEntity args, bool bInActive = false, int iCallCycleID = 0, string sCallCycleType = "")
        {
            DataTable objDS = new DataTable();
            List<CallCycleContactEntity> callCycleContactCollection = new List<CallCycleContactEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllCallCycles";
                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@FromDate", (args.SStartDate == null) ? "" : args.SStartDate);
                    objCommand.Parameters.AddWithValue("@Enddate", (args.SEndDate == null) ? "" : args.SEndDate);
                    objCommand.Parameters.AddWithValue("@RepType", sCallCycleType);
                    objCommand.Parameters.AddWithValue("@InActive", bInActive);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", (args.AdditionalParams == null) ? "" : args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "C.created_by"));
                    objCommand.Parameters.AddWithValue("@CallCycleID", iCallCycleID);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowCount", args.ShowCount);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            CallCycleContactEntity callCycleContact = CallCycleContactEntity.CreateObject();
                            callCycleContact.ItemIndex = (int)objDS.Rows.IndexOf(item);
                            callCycleContact.CallCycle.CallCycleID = Convert.ToInt32(item["callcycle_id"]);
                            callCycleContact.CallCycle.DueOn = Convert.ToDateTime(item["dueon"]).ToLocalTime();
                            callCycleContact.CallCycle.Description = item["description"].ToString();
                            callCycleContact.Contact.SourceId = Convert.ToInt32(item["lead_id"]);
                            callCycleContact.Contact.Name = item["lead_name"].ToString();
                            callCycleContact.Contact.City = item["city"].ToString();
                            callCycleContact.Contact.PostalCode = item["postcode"].ToString();
                            callCycleContact.Contact.State = item["State"].ToString();
                            callCycleContact.Originator = item["created_by"].ToString();
                            if (item["lead_Stage"] == null)
                            {
                                callCycleContact.Contact.LeadStage = item["lead_Stage"].ToString();
                                callCycleContact.LeadStage.StageName = item["lead_Stage"].ToString();
                            }
                            callCycleContact.Contact.CustomerCode = item["cust_code"].ToString();
                            callCycleContact.CallCycle.CreatedBy = item["created_by"].ToString();
                            callCycleContact.CallCycle.CreatedDate = Convert.ToDateTime(item["created_date"]);
                            callCycleContact.CallCycle.Comments = item["comments"].ToString();
                            callCycleContact.Contact.EndUserCode = item["enduser_code"].ToString();
                            callCycleContact.WeekDayId = Convert.ToInt32(item["week_day"]);
                            callCycleContact.PrefixCode = item["rep_code"].ToString();
                            callCycleContact.DayOrder = Convert.ToInt32(item["day_order"]);
                            callCycleContact.RouteId = Convert.ToInt32(item["route_master_id"]);

                            if (callCycleContact.Contact.CustomerCode != "" && callCycleContact.Contact.EndUserCode != "")
                            {
                                callCycleContact.LeadStage.StageName = "End User";
                            }
                            else if (callCycleContact.Contact.CustomerCode != "")
                            {
                                callCycleContact.LeadStage.StageName = "Customer";
                            }
                            callCycleContact.TotalCount = Convert.ToInt32(item["total_count"]);
                            callCycleContactCollection.Add(callCycleContact);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return callCycleContactCollection;
        }

        public bool Save(CallCycleEntity callCycle, List<CallCycleContactEntity> oCustomers, string originator, TimeSpan tsStartTime, out int callCycleId)
        {
            DbDataReader drCallCycle = null;
            List<CallCycleContactEntity> callCycleContactCollection = new List<CallCycleContactEntity>();
            bool isSuccessful = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                if (callCycle.CallCycleID > 0)
                {
                    paramCollection.Add("@Description", ValidateString(callCycle.Description), DbType.String);
                    paramCollection.Add("@DueOn", callCycle.DueOn.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                    paramCollection.Add("@Comments", ValidateString(callCycle.Comments), DbType.String);
                    paramCollection.Add("@CallCycleId", callCycle.CallCycleID, DbType.String);

                    isSuccessful = this.DataAcessService.ExecuteNonQuery(CallCycleSql["UpdateCallCycle"], paramCollection) > 0;
                }
                else
                {
                    callCycle.CallCycleID = Convert.ToInt32(DataAcessService.GetOneValue(CallCycleSql["GetNextCallCycleID"]));   // Get Next ID
                    // Insert CallCycle

                    paramCollection.Add("@Description", ValidateString(callCycle.Description), DbType.String);
                    // paramCollection.Add("@DueOn", callCycle.DueOn.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                    paramCollection.Add("@DueOn", callCycle.DueOn, DbType.DateTime);
                    paramCollection.Add("@Comments", ValidateString(callCycle.Comments), DbType.String);
                    paramCollection.Add("@CallCycleId", callCycle.CallCycleID, DbType.Int32);

                    paramCollection.Add("@CreatedBy", originator, DbType.String);
                    //   paramCollection.Add("@CreatedDate", DateTime.Now.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                    paramCollection.Add("@CreatedDate", DateTime.Now.ToUniversalTime(), DbType.DateTime);
                    paramCollection.Add("@DelFlag", ValidateString(callCycle.DelFlag), DbType.String);
                    paramCollection.Add("@CcType", callCycle.CCType, DbType.String);
                    paramCollection.Add("@RepCode", callCycle.PrefixCode, DbType.String);

                    isSuccessful = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertCallCycle"], paramCollection) > 0;

                }
                callCycleId = callCycle.CallCycleID;

                foreach (CallCycleContactEntity drRow in oCustomers)
                {
                    if (!isSuccessful)
                    {
                        return isSuccessful;
                    }

                    if (!string.IsNullOrEmpty(drRow.Contact.CustomerCode) && !string.IsNullOrEmpty(drRow.Contact.EndUserCode))
                    {
                        paramCollection = new DbInputParameterCollection();
                        paramCollection.Add("@CallCycleId", callCycle.CallCycleID, DbType.Int32);
                        paramCollection.Add("@CustCode", drRow.Contact.CustomerCode, DbType.String);
                        paramCollection.Add("@EnduserCode", drRow.Contact.EndUserCode, DbType.String);

                        drCallCycle = this.DataAcessService.ExecuteQuery(CallCycleSql["GetCallCycleEndUser"], paramCollection);

                        if (!drCallCycle.HasRows)
                        {
                            drCallCycle.Close();
                            paramCollection.Add("@WeekDay", drRow.WeekDayId, DbType.Int32);
                            paramCollection.Add("@DayOrder", drRow.DayOrder, DbType.Int32);

                            isSuccessful = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertCallCycleEndUser"], paramCollection) > 0;

                            // Insert an activity for the Call Cycle for the given Customer
                            callCycleContactCollection.Add(drRow);
                        }
                        else
                            drCallCycle.Close();
                    }
                    else if (!string.IsNullOrEmpty(drRow.Contact.CustomerCode))
                    {
                        paramCollection = new DbInputParameterCollection();
                        paramCollection.Add("@CallCycleId", callCycle.CallCycleID, DbType.Int32);
                        paramCollection.Add("@CustCode", drRow.Contact.CustomerCode, DbType.String);

                        drCallCycle = this.DataAcessService.ExecuteQuery(CallCycleSql["GetCallCycleCustomer"], paramCollection);

                        if (!drCallCycle.HasRows)
                        {
                            drCallCycle.Close();
                            paramCollection.Add("@WeekDay", drRow.WeekDayId, DbType.Int32);
                            paramCollection.Add("@DayOrder", drRow.DayOrder, DbType.Int32);
                            paramCollection.Add("@RouteMasterId", drRow.RouteId, DbType.Int32);

                            isSuccessful = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertCallCycleCustomer"], paramCollection) > 0;

                            // Insert an activity for the Call Cycle for the given Customer
                            callCycleContactCollection.Add(drRow);
                        }
                        else
                            drCallCycle.Close();
                    }
                }

                return isSuccessful;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCallCycle != null && !drCallCycle.IsClosed)
                    drCallCycle.Close();
            }
        }

        //OK - 1
        public bool DeactivateCallCycle(int callCycleId, string isDelete)
        {
            //bool status = false;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@CallCycleId", callCycleId, DbType.Int32);
            //    paramCollection.Add("@DelFlag", ValidateString(isDelete), DbType.String);

            //    status = this.DataAcessService.ExecuteNonQuery(CallCycleSql["DeactivateCallCycle"], paramCollection) > 0;
            //}
            //catch (Exception)
            //{

            //    throw;
            //}
            //return status;

            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeactivateCallCycle";

                    objCommand.Parameters.AddWithValue("@CallCycleId", callCycleId);
                    objCommand.Parameters.AddWithValue("@DelFlag", ValidateString(isDelete));

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK - 1
        public bool Delete(CallCycleContactEntity callCycleContact, int callCycleId)
        {
            //try
            //{
            //    bool status = false;

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@CallCycleId", callCycleId, DbType.Int32);
            //    paramCollection.Add("@LeadId", callCycleContact.Contact.SourceId, DbType.Int32);
            //    paramCollection.Add("@CustCode", callCycleContact.Contact.CustomerCode, DbType.String);
            //    paramCollection.Add("@EnduserCode", callCycleContact.Contact.EndUserCode, DbType.String);

            //    status = this.DataAcessService.ExecuteNonQuery(CallCycleSql["DeleteContactFromCallCycle"], paramCollection) > 0;

            //    return status;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{
            //}

            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteContactFromCallCycle";

                    objCommand.Parameters.AddWithValue("@CallCycleId", callCycleId);
                    objCommand.Parameters.AddWithValue("@LeadId", callCycleContact.Contact.SourceId);
                    objCommand.Parameters.AddWithValue("@CustCode", callCycleContact.Contact.CustomerCode);
                    objCommand.Parameters.AddWithValue("@EnduserCode", callCycleContact.Contact.EndUserCode);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK - 1
        public bool SaveScheduleEndUser(CallCycleContactEntity endusercallCyclelist)
        {
            //bool isSuccessful = false;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@WeekDayId", endusercallCyclelist.WeekDayId, DbType.Int32);
            //    paramCollection.Add("@CallCycleID", endusercallCyclelist.CallCycle.CallCycleID, DbType.Int32);
            //    paramCollection.Add("@CustomerCode", ValidateString(endusercallCyclelist.Contact.CustomerCode), DbType.String);
            //    paramCollection.Add("@EndUserCode", ValidateString(endusercallCyclelist.Contact.EndUserCode), DbType.String);
            //    paramCollection.Add("@DayOrder", endusercallCyclelist.DayOrder, DbType.Int32);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["UpdateScheduleEndUser"], paramCollection);

            //    if (iNoRec > 0)
            //        isSuccessful = true;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //return isSuccessful;

            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateScheduleEndUser";

                    objCommand.Parameters.AddWithValue("@WeekDayId", endusercallCyclelist.WeekDayId);
                    objCommand.Parameters.AddWithValue("@CallCycleID", endusercallCyclelist.CallCycle.CallCycleID);
                    objCommand.Parameters.AddWithValue("@CustomerCode", ValidateString(endusercallCyclelist.Contact.CustomerCode));
                    objCommand.Parameters.AddWithValue("@EndUserCode", ValidateString(endusercallCyclelist.Contact.EndUserCode));
                    objCommand.Parameters.AddWithValue("@DayOrder", endusercallCyclelist.DayOrder);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK - 1
        public bool SaveScheduleCustomer(CallCycleContactEntity customercallCyclelist)
        {
            bool isSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@WeekDayId", customercallCyclelist.WeekDayId, DbType.Int32);
                paramCollection.Add("@CallCycleID", customercallCyclelist.CallCycle.CallCycleID, DbType.Int32);
                paramCollection.Add("@CustomerCode", ValidateString(customercallCyclelist.Contact.CustomerCode), DbType.String);
                paramCollection.Add("@DayOrder", customercallCyclelist.DayOrder, DbType.Int32);
                paramCollection.Add("@RouteId", customercallCyclelist.RouteId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["UpdateScheduleCustomer"], paramCollection);

                if (iNoRec > 0)
                    isSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return isSuccessful;

            //bool retStatus = false;

            //try
            //{
            //    using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
            //    {
            //        SqlCommand objCommand = new SqlCommand();
            //        objCommand.Connection = conn;
            //        objCommand.CommandType = CommandType.StoredProcedure;
            //        objCommand.CommandText = "UpdateScheduleCustomer";

            //        objCommand.Parameters.AddWithValue("@WeekDayId", customercallCyclelist.WeekDayId);
            //        objCommand.Parameters.AddWithValue("@CallCycleID", customercallCyclelist.CallCycle.CallCycleID);
            //        objCommand.Parameters.AddWithValue("@CustomerCode", ValidateString(customercallCyclelist.Contact.CustomerCode));
            //        objCommand.Parameters.AddWithValue("@DayOrder", customercallCyclelist.DayOrder);
            //        objCommand.Parameters.AddWithValue("@RouteId", customercallCyclelist.RouteId);

            //        if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
            //        if (objCommand.ExecuteNonQuery() > 0)
            //        {
            //            retStatus = true;
            //        }
            //        else
            //        {
            //            retStatus = false;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}

            //return retStatus;
        }

        //OK - 1
        public bool SaveScheduleLead(CallCycleContactEntity leadcallCyclelist)
        {
            //bool isSuccessful = false;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@WeekDayId", leadcallCyclelist.WeekDayId, DbType.Int32);
            //    paramCollection.Add("@CallCycleID", leadcallCyclelist.CallCycle.CallCycleID, DbType.Int32);
            //    paramCollection.Add("@LeadId", leadcallCyclelist.Contact.SourceId, DbType.Int32);
            //    paramCollection.Add("@DayOrder", leadcallCyclelist.DayOrder, DbType.Int32);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["UpdateScheduleLead"], paramCollection);

            //    if (iNoRec > 0)
            //        isSuccessful = true;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //return isSuccessful;

            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateScheduleLead";

                    objCommand.Parameters.AddWithValue("@WeekDayId", leadcallCyclelist.WeekDayId);
                    objCommand.Parameters.AddWithValue("@CallCycleID", leadcallCyclelist.CallCycle.CallCycleID);
                    objCommand.Parameters.AddWithValue("@LeadId", leadcallCyclelist.Contact.SourceId);
                    objCommand.Parameters.AddWithValue("@DayOrder", leadcallCyclelist.DayOrder);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK - 1
        public bool CreateCallCycleActivitiesFromTemplate(CallCycleEntity callCycle, CallCycleContactEntity callCycleContact, ArgsEntity argsEntity)
        {
            //bool isSuccessful = false;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    if (callCycleContact.CreateActivity)
            //    {
            //        int activityid = Convert.ToInt32(DataAcessService.GetOneValue(CallCycleSql["GetNextActivityId"]));   // Get Next ID

            //        string sStatus = this.GetDefaultLookUpValue("ACST", argsEntity.DefaultDepartmentId);
            //        string sPriority = this.GetDefaultLookUpValue("ACPR", argsEntity.DefaultDepartmentId);

            //        DateTime dueDate = callCycleContact.DueOn.Date.AddHours(callCycleContact.StartTime.Hours).AddMinutes(callCycleContact.StartTime.Minutes).ToUniversalTime();

            //        paramCollection.Add("@Activityid", activityid, DbType.Int32);
            //        paramCollection.Add("@Description", callCycle.Description, DbType.String);
            //        paramCollection.Add("@DueDate", dueDate.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
            //        paramCollection.Add("@Status", sStatus, DbType.String);
            //        paramCollection.Add("@Originator", argsEntity.Originator, DbType.String);
            //        paramCollection.Add("@LeadId", callCycleContact.Contact.SourceId, DbType.Int32);
            //        paramCollection.Add("@Comments", "", DbType.String);
            //        paramCollection.Add("@SentMail", "N", DbType.String);
            //        paramCollection.Add("@Priority", sPriority, DbType.String);
            //        paramCollection.Add("@SendReminder", "", DbType.String);
            //        paramCollection.Add("@ReminderDate", dueDate, DbType.DateTime);
            //        paramCollection.Add("@CreatedBy", argsEntity.Originator, DbType.String);
            //        paramCollection.Add("@CreatedDate", DateTime.Now.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
            //        paramCollection.Add("@LastModifiedBy", argsEntity.Originator, DbType.String);
            //        paramCollection.Add("@LastModifiedDate", DateTime.Now.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
            //        paramCollection.Add("@CustCode", callCycleContact.Contact.CustomerCode, DbType.String);
            //        paramCollection.Add("@ActivityType", "CYCL", DbType.String);
            //        paramCollection.Add("@EndDate", dueDate.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
            //        paramCollection.Add("@LeadStageId", 0, DbType.Int32);
            //        paramCollection.Add("@RepGroupId", 0, DbType.Int32);
            //        paramCollection.Add("@ParentActivityId", 0, DbType.Int32);
            //        paramCollection.Add("@EnduserCode", callCycleContact.Contact.EndUserCode, DbType.String);
            //        paramCollection.Add("@CallcycleId", callCycle.CallCycleID, DbType.Int32);

            //        iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertActivityForCallCycle"], paramCollection);
            //    }

            //    if (iNoRec > 0)
            //        isSuccessful = true;
            //}
            //catch
            //{
            //    throw;
            //}
            //return isSuccessful;


            bool retStatus = false;

            try
            {
                if (callCycleContact.CreateActivity)
                {
                    using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                    {
                        SqlCommand objCommand = new SqlCommand();
                        objCommand.Connection = conn;
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.CommandText = "InsertActivityForCallCycle";

                        int activityid = Convert.ToInt32(DataAcessService.GetOneValue(CallCycleSql["GetNextActivityId"]));   // Get Next ID

                        string sStatus = this.GetDefaultLookUpValue("ACST", argsEntity.DefaultDepartmentId);
                        string sPriority = this.GetDefaultLookUpValue("ACPR", argsEntity.DefaultDepartmentId);

                        DateTime dueDate = callCycleContact.DueOn.Date.AddHours(callCycleContact.StartTime.Hours).AddMinutes(callCycleContact.StartTime.Minutes).ToUniversalTime();


                        objCommand.Parameters.AddWithValue("@Activityid", activityid);
                        objCommand.Parameters.AddWithValue("@Description", callCycle.Description);
                        objCommand.Parameters.AddWithValue("@DueDate", dueDate.ToString("dd-MMM-yyyy HH:mm:ss"));
                        objCommand.Parameters.AddWithValue("@Status", sStatus);
                        objCommand.Parameters.AddWithValue("@Originator", argsEntity.Originator);
                        objCommand.Parameters.AddWithValue("@LeadId", callCycleContact.Contact.SourceId);
                        objCommand.Parameters.AddWithValue("@Comments", "");
                        objCommand.Parameters.AddWithValue("@SentMail", "N");
                        objCommand.Parameters.AddWithValue("@Priority", sPriority);
                        objCommand.Parameters.AddWithValue("@SendReminder", "");
                        objCommand.Parameters.AddWithValue("@ReminderDate", dueDate);
                        objCommand.Parameters.AddWithValue("@CreatedBy", argsEntity.Originator);
                        objCommand.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"));
                        objCommand.Parameters.AddWithValue("@LastModifiedBy", argsEntity.Originator);
                        objCommand.Parameters.AddWithValue("@LastModifiedDate", DateTime.Now.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"));
                        objCommand.Parameters.AddWithValue("@CustCode", callCycleContact.Contact.CustomerCode);
                        objCommand.Parameters.AddWithValue("@ActivityType", "CYCL");
                        objCommand.Parameters.AddWithValue("@EndDate", dueDate.ToString("dd-MMM-yyyy HH:mm:ss"));
                        objCommand.Parameters.AddWithValue("@LeadStageId", 0);
                        objCommand.Parameters.AddWithValue("@RepGroupId", 0);
                        objCommand.Parameters.AddWithValue("@ParentActivityId", 0);
                        objCommand.Parameters.AddWithValue("@EnduserCode", callCycleContact.Contact.EndUserCode);
                        objCommand.Parameters.AddWithValue("@CallcycleId", callCycle.CallCycleID);

                        if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                        if (objCommand.ExecuteNonQuery() > 0)
                        {
                            retStatus = true;
                        }
                        else
                        {
                            retStatus = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public List<LookupEntity> GetAllCallCyclesList(ArgsEntity args)
        {
            DbDataReader drCallCycleLeads = null;
            List<LookupEntity> lookupCollection = new List<LookupEntity>();
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@OriginatorType", args.OriginatorType, DbType.String);

                drCallCycleLeads = this.DataAcessService.ExecuteQuery(CallCycleSql["GetAllCallCyclesList"], paramCollection);

                if (drCallCycleLeads != null && drCallCycleLeads.HasRows)
                {
                    int callcycleIdOrdinal = drCallCycleLeads.GetOrdinal("callcycle_id");
                    int descriptionOrdinal = drCallCycleLeads.GetOrdinal("description");
                    int totalcountOrdinal = drCallCycleLeads.GetOrdinal("total_count");

                    while (drCallCycleLeads.Read())
                    {
                        LookupEntity lookupEntity = LookupEntity.CreateObject();

                        if (!drCallCycleLeads.IsDBNull(callcycleIdOrdinal)) lookupEntity.Code = drCallCycleLeads.GetInt32(callcycleIdOrdinal);
                        if (!drCallCycleLeads.IsDBNull(descriptionOrdinal)) lookupEntity.Description = drCallCycleLeads.GetString(descriptionOrdinal);
                        if (!drCallCycleLeads.IsDBNull(totalcountOrdinal)) lookupEntity.TotalCount = drCallCycleLeads.GetInt32(totalcountOrdinal);

                        lookupCollection.Add(lookupEntity);
                    }
                }

                return lookupCollection;
            }
            catch(Exception ex)
            {
                throw;
            }
            finally
            {
                if (drCallCycleLeads != null)
                    drCallCycleLeads.Close();
            }
        }

        //OK - 1
        public bool SaveRoute(RouteEntity routeEntity)
        {
            //bool isSuccessful = false;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@RouteMasterID", routeEntity.RouteMasterId, DbType.Int32);
            //    paramCollection.Add("@RepCode", routeEntity.RepCode, DbType.String);
            //    paramCollection.Add("@Originator", routeEntity.CreatedBy, DbType.String);
            //    paramCollection.Add("@IsTempRep", routeEntity.IsTempRep, DbType.Boolean);
            //    paramCollection.Add("@TempRepDate", routeEntity.TempRepAssignDate, DbType.DateTime);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertRoutes"], paramCollection);

            //    if (iNoRec > 0)
            //        isSuccessful = true;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //return isSuccessful;

            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertRoutes";

                    objCommand.Parameters.AddWithValue("@RouteMasterID", routeEntity.RouteMasterId);
                    objCommand.Parameters.AddWithValue("@RepCode", routeEntity.RepCode);
                    objCommand.Parameters.AddWithValue("@Originator", routeEntity.CreatedBy);
                    objCommand.Parameters.AddWithValue("@IsTempRep", routeEntity.IsTempRep);
                    objCommand.Parameters.AddWithValue("@TempRepDate", routeEntity.TempRepAssignDate);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK - 1
        public bool SaveRoute(RouteMasterEntity routeMasterEntity)
        {
            //bool isSuccessful = false;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@RouteMasterID", routeMasterEntity.RouteMasterId, DbType.Int32);
            //    paramCollection.Add("@Originator", routeMasterEntity.CreatedBy, DbType.String);
            //    paramCollection.Add("@OriginatorType", routeMasterEntity.RouteType, DbType.String);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertRoutes"], paramCollection);

            //    if (iNoRec > 0)
            //        isSuccessful = true;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //return isSuccessful;

            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertRoutes";

                    objCommand.Parameters.AddWithValue("@RouteMasterID", routeMasterEntity.RouteMasterId);
                    objCommand.Parameters.AddWithValue("@Originator", routeMasterEntity.CreatedBy);
                    objCommand.Parameters.AddWithValue("@OriginatorType", routeMasterEntity.RouteType);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK
        public List<RouteEntity> GetRoutesByRepCode(ArgsEntity args)
        {
            DataTable objDS = new DataTable();
            List<RouteEntity> lstRouteEntity = new List<RouteEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRoutesByRepCode";

                    objCommand.Parameters.AddWithValue("@RepCode", args.RepCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            RouteEntity RouteEntity = RouteEntity.CreateObject();
                            RouteEntity.RouteId = Convert.ToInt32(item["id"]);
                            RouteEntity.RouteMasterId = Convert.ToInt32(item["route_master_id"]);
                            RouteEntity.RouteName = item["route_name"].ToString();
                            RouteEntity.RepCode = item["rep_code"].ToString();
                            RouteEntity.CreatedBy = item["created_by"].ToString();
                            RouteEntity.routeSequence = Convert.ToInt32(item["route_sequence"]);
                            lstRouteEntity.Add(RouteEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return lstRouteEntity;
        }

        //OK
        public List<RouteEntity> GetRoutesForTME(ArgsEntity args)
        {
            DataTable objDS = new DataTable();
            List<RouteEntity> lstRouteEntity = new List<RouteEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRoutesForTME";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@RouteType", args.RepType);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            RouteEntity RouteEntity = RouteEntity.CreateObject();
                            RouteEntity.RouteId = Convert.ToInt32(item["id"]);
                            RouteEntity.RouteMasterId = Convert.ToInt32(item["route_master_id"]);
                            RouteEntity.RouteName = item["route_name"].ToString();
                            RouteEntity.CreatedBy = item["created_by"].ToString();
                            RouteEntity.routeSequence = Convert.ToInt32(item["route_sequence"]);
                            lstRouteEntity.Add(RouteEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return lstRouteEntity;
        }

        public bool SchedulePlanedRoutesForDR(ActivityEntity activityEntity)
        {
            bool isSuccessful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@StartDate", activityEntity.StartDate, DbType.DateTime);
                paramCollection.Add("@EndDate", activityEntity.EndDate, DbType.DateTime);
                paramCollection.Add("@LeaveDay", activityEntity.Comments, DbType.String);
                paramCollection.Add("@RepCode", activityEntity.RepGroupName, DbType.String);
                paramCollection.Add("@Originator", activityEntity.CreatedBy, DbType.String);
                paramCollection.Add("@CallcycleId", activityEntity.CallCycleID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertPlanedRoutesForDR"], paramCollection);

                if (iNoRec > 0)
                    isSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return isSuccessful;
        }

        public bool DeleteRoutesByRepCode(RouteEntity routeEntity)
        {
            bool isSuccessful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@RepCode", routeEntity.RepCode, DbType.String);
                paramCollection.Add("@RouteMasterId", routeEntity.RouteMasterId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["DeleteRoutesByRepCode"], paramCollection);

                if (iNoRec > 0)
                    isSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return isSuccessful;
        }

        public bool DeleteRoutesForTME(RouteEntity routeEntity)
        {
            bool isSuccessful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", routeEntity.RepCode, DbType.String);
                paramCollection.Add("@RouteMasterId", routeEntity.RouteMasterId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["DeleteRoutesForTME"], paramCollection);

                if (iNoRec > 0)
                    isSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return isSuccessful;
        }

        //OK - 1
        public bool UpdateRouteSequence(RouteEntity routeEntity)
        {
            //bool isSuccessful = false;
            //int iNoRec = 0;

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@RepCode", routeEntity.RepCode, DbType.String);
            //    paramCollection.Add("@RouteMasterId", routeEntity.RouteMasterId, DbType.Int32);
            //    paramCollection.Add("@SequenceId", routeEntity.routeSequence, DbType.Int32);
            //    paramCollection.Add("@OriginatorType", routeEntity.OriginatorType, DbType.String);
            //    paramCollection.Add("@CreatedBy", routeEntity.CreatedBy, DbType.String);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["UpdateRouteSequence"], paramCollection);

            //    if (iNoRec > 0)
            //        isSuccessful = true;
            //}
            //catch (Exception ex)
            //{
            //    isSuccessful = false;
            //}

            //return isSuccessful;



            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateRouteSequence";

                    objCommand.Parameters.AddWithValue("@RepCode", routeEntity.RepCode);
                    objCommand.Parameters.AddWithValue("@RouteMasterId", routeEntity.RouteMasterId);
                    objCommand.Parameters.AddWithValue("@SequenceId", routeEntity.routeSequence);
                    objCommand.Parameters.AddWithValue("@OriginatorType", routeEntity.OriginatorType);
                    objCommand.Parameters.AddWithValue("@CreatedBy", routeEntity.CreatedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public List<RouteEntity> GetAssignedRoutes(string parentOriginator, ArgsEntity args)
        {
            DbDataReader drRouteEntity = null;
            List<RouteEntity> lstRouteEntity = new List<RouteEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ParentOriginator", parentOriginator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);

                drRouteEntity = this.DataAcessService.ExecuteQuery(CallCycleSql["GetAssignedRoutesForDR"], paramCollection);
                if (drRouteEntity != null && drRouteEntity.HasRows)
                {
                    int idOrdinal = drRouteEntity.GetOrdinal("id");
                    int routeMasterIdOrdinal = drRouteEntity.GetOrdinal("route_master_id");
                    int routeNameOrdinal = drRouteEntity.GetOrdinal("route_name");
                    int repCodeOrdinal = drRouteEntity.GetOrdinal("rep_code");
                    int repNameOrdinal = drRouteEntity.GetOrdinal("rep_name");//this will be assigned as RepCode
                    int createdByOrdinal = drRouteEntity.GetOrdinal("created_by");
                    int createdDateOrdinal = drRouteEntity.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drRouteEntity.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drRouteEntity.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drRouteEntity.GetOrdinal("total_count");

                    while (drRouteEntity.Read())
                    {
                        RouteEntity RouteEntity = RouteEntity.CreateObject();
                        
                        if (!drRouteEntity.IsDBNull(idOrdinal)) RouteEntity.RouteId = drRouteEntity.GetInt32(idOrdinal);
                        if (!drRouteEntity.IsDBNull(routeMasterIdOrdinal)) RouteEntity.RouteMasterId = drRouteEntity.GetInt32(routeMasterIdOrdinal);
                        if (!drRouteEntity.IsDBNull(routeNameOrdinal)) RouteEntity.RouteName = drRouteEntity.GetString(routeNameOrdinal);
                        if (!drRouteEntity.IsDBNull(repCodeOrdinal)) RouteEntity.RepCode = drRouteEntity.GetString(repCodeOrdinal);
                        if (!drRouteEntity.IsDBNull(repNameOrdinal)) RouteEntity.RepName = drRouteEntity.GetString(repNameOrdinal);
                        if (!drRouteEntity.IsDBNull(createdByOrdinal)) RouteEntity.CreatedBy = drRouteEntity.GetString(createdByOrdinal);
                        if (!drRouteEntity.IsDBNull(createdDateOrdinal)) RouteEntity.CreatedDate = drRouteEntity.GetDateTime(createdDateOrdinal);
                        if (!drRouteEntity.IsDBNull(lastModifiedByOrdinal)) RouteEntity.LastModifiedBy = drRouteEntity.GetString(lastModifiedByOrdinal);
                        if (!drRouteEntity.IsDBNull(lastModifiedDateOrdinal)) RouteEntity.LastModifiedDate = drRouteEntity.GetDateTime(lastModifiedDateOrdinal);
                        if (!drRouteEntity.IsDBNull(totalCountOrdinal)) RouteEntity.TotalCount = drRouteEntity.GetInt32(totalCountOrdinal);

                        lstRouteEntity.Add(RouteEntity);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drRouteEntity != null)
                    drRouteEntity.Close();
            }
            return lstRouteEntity;
        }

        public List<RouteEntity> GetRouteNames(string name)
        {
            DbDataReader drRouteEntity = null;
            List<RouteEntity> lstRouteEntity = new List<RouteEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteName", name, DbType.String);

                drRouteEntity = this.DataAcessService.ExecuteQuery(CallCycleSql["GetRouteMasterNames"], paramCollection);
                if (drRouteEntity != null && drRouteEntity.HasRows)
                {
                    int routeNameOrdinal = drRouteEntity.GetOrdinal("name");

                    while (drRouteEntity.Read())
                    {
                        RouteEntity routeEntity = RouteEntity.CreateObject();
                        if (!drRouteEntity.IsDBNull(routeNameOrdinal)) routeEntity.RouteName = drRouteEntity.GetString(routeNameOrdinal);
                        lstRouteEntity.Add(routeEntity);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drRouteEntity != null)
                    drRouteEntity.Close();
            }
            return lstRouteEntity;
        }

        public List<CallCycleEntity> GetCallCyclesForRep(string repcode)
        {
            DbDataReader drRouteEntity = null;
            List<CallCycleEntity> listCallcycle = new List<CallCycleEntity>();


            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RepCode", repcode, DbType.String);

                drRouteEntity = this.DataAcessService.ExecuteQuery(CallCycleSql["GetCallCycleTempsByRep"], paramCollection);
                if (drRouteEntity != null && drRouteEntity.HasRows)
                {
                    int idOrdinal = drRouteEntity.GetOrdinal("callcycle_id");
                    int descriptionOrdinal = drRouteEntity.GetOrdinal("description");
                    int createdDateOrdinal = drRouteEntity.GetOrdinal("created_date");

                    while (drRouteEntity.Read())
                    {
                        CallCycleEntity callEnt = CallCycleEntity.CreateObject();

                        if (!drRouteEntity.IsDBNull(idOrdinal)) callEnt.CallCycleID = drRouteEntity.GetInt32(idOrdinal);
                        if (!drRouteEntity.IsDBNull(descriptionOrdinal)) callEnt.Description = drRouteEntity.GetString(descriptionOrdinal);
                        if (!drRouteEntity.IsDBNull(createdDateOrdinal)) callEnt.CreatedDate = drRouteEntity.GetDateTime(createdDateOrdinal);

                        listCallcycle.Add(callEnt);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drRouteEntity != null)
                    drRouteEntity.Close();
            }
            return listCallcycle;
        }

        //OK - 1
        public bool SchedulePlanedRoutesForTME(ActivityEntity activityEntity)
        {
            //bool isSuccessful = false;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@StartDate", activityEntity.StartDate, DbType.DateTime);
            //    paramCollection.Add("@EndDate", activityEntity.EndDate, DbType.DateTime);
            //    paramCollection.Add("@LeaveDay", activityEntity.Comments, DbType.String);
            //    paramCollection.Add("@RepCode", activityEntity.RepGroupName, DbType.String);
            //    paramCollection.Add("@Originator", activityEntity.CreatedBy, DbType.String);
            //    paramCollection.Add("@CallcycleId", activityEntity.CallCycleID, DbType.Int32);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertPlanedRoutesForTME"], paramCollection);

            //    if (iNoRec > 0)
            //        isSuccessful = true;
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}
            //return isSuccessful;

            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertPlanedRoutesForTME";

                    objCommand.Parameters.AddWithValue("@StartDate", activityEntity.StartDate);
                    objCommand.Parameters.AddWithValue("@EndDate", activityEntity.EndDate);
                    objCommand.Parameters.AddWithValue("@LeaveDay", activityEntity.Comments);
                    objCommand.Parameters.AddWithValue("@RepCode", activityEntity.RepGroupName);
                    objCommand.Parameters.AddWithValue("@Originator", activityEntity.CreatedBy);
                    objCommand.Parameters.AddWithValue("@CallcycleId", activityEntity.CallCycleID);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK - 1
        public List<MobileRouteEntity> GetTodayRoutesCustomersForTME(string tmeCode)
        {
            //DbDataReader drRouteEntity = null;
            //DbDataReader drCustomerEntity = null;
            //List<MobileRouteEntity> lstRouteEntity = new List<MobileRouteEntity>();

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@TMECode", tmeCode, DbType.String);

            //    drRouteEntity = this.DataAcessService.ExecuteQuery(CallCycleSql["GetTodayRoutesForTME"], paramCollection);
            //    if (drRouteEntity != null && drRouteEntity.HasRows)
            //    {
            //        int routeidOrdinal = drRouteEntity.GetOrdinal("id");
            //        int routeNameOrdinal = drRouteEntity.GetOrdinal("name");
            //        int routeSequenceOrdinal = drRouteEntity.GetOrdinal("route_sequence");
            //        while (drRouteEntity.Read())
            //        {
            //            MobileRouteEntity routeEntity = MobileRouteEntity.CreateObject();
            //            if (!drRouteEntity.IsDBNull(routeidOrdinal)) routeEntity.RouteMasterId = drRouteEntity.GetInt32(routeidOrdinal);
            //            if (!drRouteEntity.IsDBNull(routeNameOrdinal)) routeEntity.RouteName = drRouteEntity.GetString(routeNameOrdinal);
            //            if (!drRouteEntity.IsDBNull(routeSequenceOrdinal)) routeEntity.RouteSequence = drRouteEntity.GetInt32(routeSequenceOrdinal);
            //            lstRouteEntity.Add(routeEntity);
            //            if (drCustomerEntity != null)
            //                drCustomerEntity.Close();
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drRouteEntity != null)
            //        drRouteEntity.Close();
            //}
            //return lstRouteEntity;

            DataTable objDS = new DataTable();
            List<MobileRouteEntity> lstRouteEntity = new List<MobileRouteEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetTodayRoutesForTME";

                    objCommand.Parameters.AddWithValue("@TMECode", tmeCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            MobileRouteEntity routeEntity = MobileRouteEntity.CreateObject();
                            routeEntity.RouteMasterId = Convert.ToInt32(item["id"]);
                            routeEntity.RouteName = item["name"].ToString();
                            routeEntity.RouteSequence = Convert.ToInt32(item["route_sequence"]);


                            lstRouteEntity.Add(routeEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return lstRouteEntity;
        }
        
        //OK - 1
        public bool UpdateTMERouteSequence(MobileRouteEntity routeEntity)
        {
            //bool isSuccessful = false;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@TMECode", routeEntity.TMECode, DbType.String);
            //    paramCollection.Add("@RouteMasterId", routeEntity.RouteMasterId, DbType.Int32);
            //    paramCollection.Add("@SequenceId", routeEntity.RouteSequence, DbType.Int32);


            //    iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["UpdateTMERouteSequence"], paramCollection);

            //    if (iNoRec > 0)
            //        isSuccessful = true;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //return isSuccessful;



            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateTMERouteSequence";

                    objCommand.Parameters.AddWithValue("@TMECode", routeEntity.TMECode);
                    objCommand.Parameters.AddWithValue("@RouteMasterId", routeEntity.RouteMasterId);
                    objCommand.Parameters.AddWithValue("@SequenceId", routeEntity.RouteSequence);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK - 1
        public List<MobileRouteEntity> GetRouteSummaryForTME(int routeId)
        {
            //DbDataReader drRouteEntity = null;
            //List<MobileRouteEntity> lstRouteEntity = new List<MobileRouteEntity>();
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@RouteId", routeId, DbType.Int32);
            //    drRouteEntity = this.DataAcessService.ExecuteQuery(CallCycleSql["GetRouteSummaryForTME"], paramCollection);
            //    if (drRouteEntity != null && drRouteEntity.HasRows)
            //    {
            //        int lstVisitedOrdinal = drRouteEntity.GetOrdinal("last_visited_date");
            //        int routeNameOrdinal = drRouteEntity.GetOrdinal("route_name");
            //        int totOutletOrdinal = drRouteEntity.GetOrdinal("total_outlets");
            //        int visitedOutletOrdinal = drRouteEntity.GetOrdinal("visited_outlets");
            //        while (drRouteEntity.Read())
            //        {
            //            MobileRouteEntity routeEntity = MobileRouteEntity.CreateObject();
            //            if (!drRouteEntity.IsDBNull(lstVisitedOrdinal)) routeEntity.LastVisitedDate = drRouteEntity.GetDateTime(lstVisitedOrdinal);
            //            if (!drRouteEntity.IsDBNull(routeNameOrdinal)) routeEntity.RouteName = drRouteEntity.GetString(routeNameOrdinal);
            //            if (!drRouteEntity.IsDBNull(totOutletOrdinal)) routeEntity.TotalOutlets = drRouteEntity.GetInt32(totOutletOrdinal);
            //            if (!drRouteEntity.IsDBNull(visitedOutletOrdinal)) routeEntity.VisitedOutlet = drRouteEntity.GetInt32(visitedOutletOrdinal);
            //            lstRouteEntity.Add(routeEntity);
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drRouteEntity != null)
            //        drRouteEntity.Close();
            //}
            //return lstRouteEntity;

            DataTable objDS = new DataTable();
            List<MobileRouteEntity> lstRouteEntity = new List<MobileRouteEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRouteSummaryForTME";

                    objCommand.Parameters.AddWithValue("@RouteId", routeId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            MobileRouteEntity routeEntity = MobileRouteEntity.CreateObject();
                            routeEntity.LastVisitedDate = Convert.ToDateTime(item["last_visited_date"]); //drRouteEntity.GetDateTime(lstVisitedOrdinal);
                            routeEntity.RouteName = item["route_name"].ToString(); //drRouteEntity.GetString(routeNameOrdinal);
                            routeEntity.TotalOutlets = Convert.ToInt32(item["total_outlets"]); //drRouteEntity.GetInt32(totOutletOrdinal);
                            routeEntity.VisitedOutlet = Convert.ToInt32(item["visited_outlets"]); //drRouteEntity.GetInt32(visitedOutletOrdinal);

                            lstRouteEntity.Add(routeEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return lstRouteEntity;

        }

        //OK - 1
        public List<CallCycleContactEntity> GetCallCycleContactsForNewEntry(ArgsEntity args, string repCode)
        {
            //DbDataReader drCallCycleLeads = null;
            //List<CallCycleContactEntity> callCycleContactCollection = new List<CallCycleContactEntity>();
            //try
            //{

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@Originator", args.Originator, DbType.String);
            //    paramCollection.Add("@RepCode", repCode, DbType.String);

            //    paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
            //    paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
            //    paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "C.created_by"), DbType.String);

            //    paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
            //    paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
            //    paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
            //    paramCollection.Add("@ShowCount", args.ShowCount, DbType.Int32);

            //    drCallCycleLeads = this.DataAcessService.ExecuteQuery(CallCycleSql["GetAllCallCyclesForNewEntry"], paramCollection);

            //    if (drCallCycleLeads != null && drCallCycleLeads.HasRows)
            //    {
            //        int callcycleIdOrdinal = drCallCycleLeads.GetOrdinal("callcycle_id");
            //        int leadIdOrdinal = drCallCycleLeads.GetOrdinal("lead_id");
            //        int descriptionOrdinal = drCallCycleLeads.GetOrdinal("description");
            //        int dueonOrdinal = drCallCycleLeads.GetOrdinal("dueon");
            //        //int originatorOrdinal = drCallCycleLeads.GetOrdinal("originator");
            //        int leadNameOrdinal = drCallCycleLeads.GetOrdinal("lead_name");
            //        int cityOrdinal = drCallCycleLeads.GetOrdinal("city");
            //        int postcodeOrdinal = drCallCycleLeads.GetOrdinal("postcode");
            //        int addressOrdinal = drCallCycleLeads.GetOrdinal("address");
            //        int stateOrdinal = drCallCycleLeads.GetOrdinal("State");
            //        int leadStageOrdinal = drCallCycleLeads.GetOrdinal("lead_Stage");
            //        int custCodeOrdinal = drCallCycleLeads.GetOrdinal("cust_code");
            //        int createdByOrdinal = drCallCycleLeads.GetOrdinal("created_by");
            //        int createdDateOrdinal = drCallCycleLeads.GetOrdinal("created_date");
            //        int commentsOrdinal = drCallCycleLeads.GetOrdinal("comments");

            //        int endUserCodeOrdinal = drCallCycleLeads.GetOrdinal("enduser_code");
            //        int weekDayOrdinal = drCallCycleLeads.GetOrdinal("week_day");
            //        int prefixCodeOrdinal = drCallCycleLeads.GetOrdinal("rep_code");
            //        int dayOrderOrdinal = drCallCycleLeads.GetOrdinal("day_order");
            //        int totalcountOrdinal = drCallCycleLeads.GetOrdinal("total_count");

            //        int index = 1;
            //        while (drCallCycleLeads.Read())
            //        {
            //            CallCycleContactEntity callCycleContact = CallCycleContactEntity.CreateObject();

            //            callCycleContact.ItemIndex = index;
            //            if (!drCallCycleLeads.IsDBNull(callcycleIdOrdinal)) callCycleContact.CallCycle.CallCycleID = drCallCycleLeads.GetInt32(callcycleIdOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(dueonOrdinal)) callCycleContact.CallCycle.DueOn = drCallCycleLeads.GetDateTime(dueonOrdinal).ToLocalTime();
            //            if (!drCallCycleLeads.IsDBNull(descriptionOrdinal)) callCycleContact.CallCycle.Description = drCallCycleLeads.GetString(descriptionOrdinal);

            //            if (!drCallCycleLeads.IsDBNull(leadIdOrdinal)) callCycleContact.Contact.SourceId = drCallCycleLeads.GetInt32(leadIdOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(leadNameOrdinal)) callCycleContact.Contact.Name = drCallCycleLeads.GetString(leadNameOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(cityOrdinal)) callCycleContact.Contact.City = drCallCycleLeads.GetString(cityOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(postcodeOrdinal)) callCycleContact.Contact.PostalCode = drCallCycleLeads.GetString(postcodeOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(addressOrdinal)) callCycleContact.Contact.Address = drCallCycleLeads.GetString(addressOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(stateOrdinal)) callCycleContact.Contact.State = drCallCycleLeads.GetString(stateOrdinal);


            //            //if (!drCallCycleLeads.IsDBNull(originatorOrdinal)) callCycleContact.Originator = drCallCycleLeads.GetString(originatorOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(createdByOrdinal)) callCycleContact.Originator = drCallCycleLeads.GetString(createdByOrdinal);

            //            //callCycleContact.Contact.LeadCustomerType = LeadCustomerTypes.Lead;
            //            if (!drCallCycleLeads.IsDBNull(leadStageOrdinal))
            //            {
            //                callCycleContact.Contact.LeadStage = drCallCycleLeads.GetString(leadStageOrdinal);
            //                callCycleContact.LeadStage.StageName = drCallCycleLeads.GetString(leadStageOrdinal);
            //            }
            //            if (!drCallCycleLeads.IsDBNull(custCodeOrdinal)) callCycleContact.Contact.CustomerCode = drCallCycleLeads.GetString(custCodeOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(createdByOrdinal)) callCycleContact.CallCycle.CreatedBy = drCallCycleLeads.GetString(createdByOrdinal);

            //            if (!drCallCycleLeads.IsDBNull(createdDateOrdinal)) callCycleContact.CallCycle.CreatedDate = drCallCycleLeads.GetDateTime(createdDateOrdinal);
            //            else callCycleContact.CallCycle.CreatedDate = new DateTime();
            //            if (!drCallCycleLeads.IsDBNull(commentsOrdinal)) callCycleContact.CallCycle.Comments = drCallCycleLeads.GetString(commentsOrdinal);

            //            if (!drCallCycleLeads.IsDBNull(custCodeOrdinal)) callCycleContact.Contact.EndUserCode = drCallCycleLeads.GetString(endUserCodeOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(weekDayOrdinal)) callCycleContact.WeekDayId = drCallCycleLeads.GetInt32(weekDayOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(prefixCodeOrdinal)) callCycleContact.PrefixCode = drCallCycleLeads.GetString(prefixCodeOrdinal);
            //            if (!drCallCycleLeads.IsDBNull(dayOrderOrdinal)) callCycleContact.DayOrder = drCallCycleLeads.GetInt32(dayOrderOrdinal);
            //            callCycleContact.CallCycle.LastModifiedDate = new DateTime();


            //            if (callCycleContact.Contact.CustomerCode != "" && callCycleContact.Contact.EndUserCode != "")
            //            {
            //                callCycleContact.LeadStage.StageName = "Customer";
            //            }
            //            else if (callCycleContact.Contact.CustomerCode != "")
            //            {
            //                callCycleContact.LeadStage.StageName = "Customer";
            //            }
            //            //else
            //            //{
            //            //    callCycleContact.LeadStage.StageName = "Lead";
            //            //}

            //            if (!drCallCycleLeads.IsDBNull(totalcountOrdinal)) callCycleContact.TotalCount = drCallCycleLeads.GetInt32(totalcountOrdinal);

            //            callCycleContactCollection.Add(callCycleContact);
            //            index++;
            //        }
            //    }

            //    return callCycleContactCollection;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drCallCycleLeads != null)
            //        drCallCycleLeads.Close();
            //}

            DataTable objDS = new DataTable();
            List<CallCycleContactEntity> callCycleContactCollection = new List<CallCycleContactEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllCallCyclesForNewEntry";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@RepCode", repCode);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "C.created_by"));
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowCount", args.ShowCount);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        int index = 1;

                        foreach (DataRow item in objDS.Rows)
                        {
                            CallCycleContactEntity callCycleContact = CallCycleContactEntity.CreateObject();
                            callCycleContact.ItemIndex = index;
                            callCycleContact.CallCycle.CallCycleID = Convert.ToInt32(item["callcycle_id"]);
                            callCycleContact.CallCycle.DueOn = Convert.ToDateTime(item["dueon"]);
                            callCycleContact.CallCycle.Description = item["description"].ToString();
                            callCycleContact.Contact.SourceId = Convert.ToInt32(item["lead_id"]);
                            callCycleContact.Contact.Name = item["lead_name"].ToString();
                            callCycleContact.Contact.City = item["city"].ToString();
                            callCycleContact.Contact.PostalCode = item["postcode"].ToString();
                            callCycleContact.Contact.Address = item["address"].ToString();
                            callCycleContact.Contact.State = item["State"].ToString();
                            callCycleContact.Originator = item["created_by"].ToString();
                            callCycleContact.Contact.LeadStage = item["lead_Stage"].ToString();
                            callCycleContact.LeadStage.StageName = item["lead_Stage"].ToString();
                            callCycleContact.Contact.CustomerCode = item["cust_code"].ToString();
                            callCycleContact.CallCycle.CreatedBy = item["created_by"].ToString();

                            try
                            {
                                callCycleContact.CallCycle.CreatedDate = Convert.ToDateTime(item["dueon"]);
                            }
                            catch { callCycleContact.CallCycle.CreatedDate = new DateTime(); }

                            callCycleContact.CallCycle.Comments = item["comments"].ToString();
                            callCycleContact.Contact.EndUserCode = item["enduser_code"].ToString();
                            callCycleContact.WeekDayId = Convert.ToInt32(item["week_day"]);
                            callCycleContact.PrefixCode = item["rep_code"].ToString();
                            callCycleContact.DayOrder = Convert.ToInt32(item["day_order"]);
                            callCycleContact.CallCycle.LastModifiedDate = new DateTime();

                            if (callCycleContact.Contact.CustomerCode != "" && callCycleContact.Contact.EndUserCode != "")
                            {
                                callCycleContact.LeadStage.StageName = "Customer";
                            }
                            else if (callCycleContact.Contact.CustomerCode != "")
                            {
                                callCycleContact.LeadStage.StageName = "Customer";
                            }

                            callCycleContact.TotalCount = Convert.ToInt32(item["total_count"]);

                            callCycleContactCollection.Add(callCycleContact);
                            index++;
                        }
                    }
                }
            }
            catch (Exception ex){ callCycleContactCollection = new List<CallCycleContactEntity>(); }

            return callCycleContactCollection;
        }

        //OK - 1
        public bool IsCallCycleContactInAnyOtherSchedule(string customerCode)
        {
            //DbDataReader drCallCycle = null;

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@custCode", customerCode, DbType.String);

            //    drCallCycle = this.DataAcessService.ExecuteQuery(CallCycleSql["IsCallCycleContactInAnyOtherSchedule"], paramCollection);

            //    if (drCallCycle != null && drCallCycle.HasRows)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drCallCycle != null)
            //        drCallCycle.Close();
            //}

            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "IsCallCycleContactInAnyOtherSchedule";

                    objCommand.Parameters.AddWithValue("@custCode", customerCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //OK
        public string GetRepCodeForCallCycleId(int callCycleId)
        {
            string repCode = "";
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRepCodeForCallCycleId";

                    objCommand.Parameters.AddWithValue("@callCycleId", callCycleId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        repCode = objDS.Rows[0]["rep_code"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return repCode;
        }

        //OK - 1
        public int GetCallCycleForRepOrTme(string repCode, string tme)
        {
            //int callCycleId = 0;
            //DbDataReader drCallCycle = null;

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@repCode", repCode, DbType.String);
            //    paramCollection.Add("@tme", tme, DbType.String);
            //    drCallCycle = this.DataAcessService.ExecuteQuery(CallCycleSql["GetCallCycleForRepOrTme"], paramCollection);
            //    if (drCallCycle != null && drCallCycle.HasRows)
            //    {
            //        int idOrdinal = drCallCycle.GetOrdinal("callcycle_id");
            //        while (drCallCycle.Read())
            //        {
            //            if (!drCallCycle.IsDBNull(idOrdinal)) callCycleId = drCallCycle.GetInt32(idOrdinal);
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drCallCycle != null)
            //        drCallCycle.Close();
            //}
            //return callCycleId;

            int callCycleId = 0;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetCallCycleForRepOrTme";

                    objCommand.Parameters.AddWithValue("@repCode", repCode);
                    objCommand.Parameters.AddWithValue("@tme", tme);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        callCycleId = Int32.Parse(objDS.Rows[0]["callcycle_id"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return callCycleId;
        }

        //OK
        public bool UpdateSRTerritory(DbWorkflowScope transactionScope, string Originator, string RepId, string TerritoryId)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(transactionScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateSRTerritory";

                    objCommand.Parameters.AddWithValue("@Originator", Originator);
                    objCommand.Parameters.AddWithValue("@RepId", RepId);
                    objCommand.Parameters.AddWithValue("@TerritoryId", TerritoryId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //OK
        public bool UpdateDBTerritory(DbWorkflowScope transactionScope, string DistributerId, string TerritoryId, string CreatedBy)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(transactionScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateDBTerritory";

                    objCommand.Parameters.AddWithValue("@DistributerId", DistributerId);
                    objCommand.Parameters.AddWithValue("@TerritoryId", TerritoryId);
                    objCommand.Parameters.AddWithValue("@CreatedBy", CreatedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
    }
}
