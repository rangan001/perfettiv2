﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class CatalogDAO : BaseDAO
    {
        private DbSqlAdapter CatalogSql { get; set; }

        private void RegisterSql()
        {
            this.CatalogSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public CatalogDAO()
        {
            RegisterSql();
        }

        public CatalogDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public CatalogEntity CreateObject()
        {
            return CatalogEntity.CreateObject();
        }

        public CatalogEntity CreateObject(int entityId)
        {
            return CatalogEntity.CreateObject(entityId);
        }

        public List<CatalogEntity> GetOppProducts(int opportunityId,ArgsEntity args)
        {
            DbDataReader drCatalog = null;

            try
            {
                List<CatalogEntity> Cataloglist = new List<CatalogEntity>();
                
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OpportunityId", opportunityId, DbType.Int32);
               
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RawCount", args.RowCount, DbType.Int32);

                drCatalog = this.DataAcessService.ExecuteQuery(CatalogSql["GetOpportunityProducts"], paramCollection);

                if (drCatalog != null && drCatalog.HasRows)
                {
                    int codeOrdinal = drCatalog.GetOrdinal("catlog_code");
                    int descriptioneOrdinal = drCatalog.GetOrdinal("description");
                    int priceOrdinal = drCatalog.GetOrdinal("price");

                    while (drCatalog.Read())
                    {
                        CatalogEntity catalogEntity = CreateObject();

                        if (!drCatalog.IsDBNull(codeOrdinal)) catalogEntity.CatlogCode = drCatalog.GetString(codeOrdinal);
                        if (!drCatalog.IsDBNull(descriptioneOrdinal)) catalogEntity.Description = drCatalog.GetString(descriptioneOrdinal);
                        if (!drCatalog.IsDBNull(priceOrdinal)) catalogEntity.Price = drCatalog.GetDouble(priceOrdinal);

                        Cataloglist.Add(catalogEntity);
                    }
                }

                return Cataloglist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCatalog != null)
                    drCatalog.Close();
            }
        }

        public List<CatalogEntity> GetAllProducts(ArgsEntity args)
        {
            DbDataReader drCatalog = null;

            try
            {
                List<CatalogEntity> Cataloglist = new List<CatalogEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.Int32);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RawCount", args.RowCount, DbType.Int32);

                drCatalog = this.DataAcessService.ExecuteQuery(CatalogSql["GetAllProducts"], paramCollection);

                if (drCatalog != null && drCatalog.HasRows)
                {
                    int codeOrdinal = drCatalog.GetOrdinal("catlog_code");
                    int descriptioneOrdinal = drCatalog.GetOrdinal("description");
                    int priceOrdinal = drCatalog.GetOrdinal("price");

                    while (drCatalog.Read())
                    {
                        CatalogEntity catalogEntity = CreateObject();

                        if (!drCatalog.IsDBNull(codeOrdinal)) catalogEntity.CatlogCode = drCatalog.GetString(codeOrdinal);
                        if (!drCatalog.IsDBNull(descriptioneOrdinal)) catalogEntity.Description = drCatalog.GetString(descriptioneOrdinal);
                        if (!drCatalog.IsDBNull(priceOrdinal)) catalogEntity.Price = drCatalog.GetDouble(priceOrdinal);

                        Cataloglist.Add(catalogEntity);
                    }
                }

                return Cataloglist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCatalog != null)
                    drCatalog.Close();
            }
        }

        public bool Insert(CatalogEntity opportunity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = null;

                //Delete all product for OpportunityID.
                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OpportunityID", opportunity.OpportunityID, DbType.Int32);
                iNoRec = this.DataAcessService.ExecuteNonQuery(CatalogSql["DeleteOpportunityProducts"], paramCollection);

                //if (iNoRec > 0)
                //{
                    paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@OpportunityID", opportunity.OpportunityID, DbType.Int32);
                    paramCollection.Add("@CatlogCode", opportunity.CatlogCode, DbType.String);
                    paramCollection.Add("@Price", opportunity.Price, DbType.Double);

                    iNoRec = this.DataAcessService.ExecuteNonQuery(CatalogSql["InsertOpportunityProduct"], paramCollection);
                //}

                if (iNoRec > 0)
                    saveSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return saveSuccessful;
        }

        public List<CatalogEntity> GetProductsLookup(ArgsEntity args)
        {
            DbDataReader drCatalog = null;

            try
            {
                List<CatalogEntity> Cataloglist = new List<CatalogEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drCatalog = this.DataAcessService.ExecuteQuery(CatalogSql["GetProductsLookup"], paramCollection);

                if (drCatalog != null && drCatalog.HasRows)
                {
                    int codeOrdinal = drCatalog.GetOrdinal("catlog_code");
                    int descriptioneOrdinal = drCatalog.GetOrdinal("description");
                    int productTypeOrdinal = drCatalog.GetOrdinal("product_type");
                    int totalCountOrdinal = drCatalog.GetOrdinal("total_count");
                    

                    while (drCatalog.Read())
                    {
                        CatalogEntity catalogEntity = CreateObject();

                        if (!drCatalog.IsDBNull(codeOrdinal)) catalogEntity.CatlogCode = drCatalog.GetString(codeOrdinal);
                        if (!drCatalog.IsDBNull(descriptioneOrdinal)) catalogEntity.Description = drCatalog.GetString(descriptioneOrdinal);
                        if (!drCatalog.IsDBNull(productTypeOrdinal)) catalogEntity.DeliveryFrequency = drCatalog.GetString(productTypeOrdinal);
                        if (!drCatalog.IsDBNull(totalCountOrdinal)) catalogEntity.TotalCount = drCatalog.GetInt32(totalCountOrdinal);

                        Cataloglist.Add(catalogEntity);
                    }
                }

                return Cataloglist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCatalog != null)
                    drCatalog.Close();
            }
        }

        public List<CatalogEntity> GetAllCatalog(ArgsEntity args)
        {
            DbDataReader drCatalog = null;

            try
            {
                List<CatalogEntity> Cataloglist = new List<CatalogEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drCatalog = this.DataAcessService.ExecuteQuery(CatalogSql["GetAllCatalog"], paramCollection);

                if (drCatalog != null && drCatalog.HasRows)
                {
                    int codeOrdinal = drCatalog.GetOrdinal("catlog_code");
                    int descriptioneOrdinal = drCatalog.GetOrdinal("description");
                    int productTypeOrdinal = drCatalog.GetOrdinal("product_type");
                    int ConversionOrdinal = drCatalog.GetOrdinal("Conversion");
                    int priceOrdinal = drCatalog.GetOrdinal("price");
                    int totalCountOrdinal = drCatalog.GetOrdinal("total_count");

                    while (drCatalog.Read())
                    {
                        CatalogEntity catalogEntity = CreateObject();

                        if (!drCatalog.IsDBNull(codeOrdinal)) catalogEntity.CatlogCode = drCatalog.GetString(codeOrdinal);
                        if (!drCatalog.IsDBNull(descriptioneOrdinal)) catalogEntity.Description = drCatalog.GetString(descriptioneOrdinal);
                        if (!drCatalog.IsDBNull(productTypeOrdinal)) catalogEntity.DeliveryFrequency = drCatalog.GetString(productTypeOrdinal);
                        if (!drCatalog.IsDBNull(ConversionOrdinal)) catalogEntity.Conversion = drCatalog.GetDouble(ConversionOrdinal);
                        if (!drCatalog.IsDBNull(priceOrdinal)) catalogEntity.Price = drCatalog.GetDouble(priceOrdinal);
                        if (!drCatalog.IsDBNull(totalCountOrdinal)) catalogEntity.TotalCount = drCatalog.GetInt32(totalCountOrdinal);

                        Cataloglist.Add(catalogEntity);
                    }
                }

                return Cataloglist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCatalog != null)
                    drCatalog.Close();
            }
        }

        public List<CatalogEntity> GetCatalogLookup(ArgsEntity args)
        {
            DbDataReader drCatalog = null;

            try
            {
                List<CatalogEntity> Cataloglist = new List<CatalogEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drCatalog = this.DataAcessService.ExecuteQuery(CatalogSql["GetCatalogLookup"], paramCollection);

                if (drCatalog != null && drCatalog.HasRows)
                {
                    int catalogIdOrdinal = drCatalog.GetOrdinal("catalog_id");
                    int codeOrdinal = drCatalog.GetOrdinal("catlog_code");
                    int descriptioneOrdinal = drCatalog.GetOrdinal("description");
                    int totalCountOrdinal = drCatalog.GetOrdinal("total_count");

                    while (drCatalog.Read())
                    {
                        CatalogEntity catalogEntity = CreateObject();

                        if (!drCatalog.IsDBNull(catalogIdOrdinal)) catalogEntity.CatlogId = drCatalog.GetInt32(catalogIdOrdinal);
                        if (!drCatalog.IsDBNull(codeOrdinal)) catalogEntity.CatlogCode = drCatalog.GetString(codeOrdinal);
                        if (!drCatalog.IsDBNull(descriptioneOrdinal)) catalogEntity.Description = drCatalog.GetString(descriptioneOrdinal);
                        if (!drCatalog.IsDBNull(totalCountOrdinal)) catalogEntity.TotalCount = drCatalog.GetInt32(totalCountOrdinal);

                        Cataloglist.Add(catalogEntity);
                    }
                }

                return Cataloglist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCatalog != null)
                    drCatalog.Close();
            }
        }
    }
}
