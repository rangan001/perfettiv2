﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
//using Peercore.CRM.Entities.CompositeEntities;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using Peercore.CRM.Model;
using System.Data.SqlClient;
using System.IO;

namespace Peercore.CRM.DataAccess.DAO
{
    public class CommonDAO : BaseDAO
    {
        private DbSqlAdapter ChecklistSql { get; set; }

        public MobileRepsDashboardModel LoadMobileRepsDashboard()
        {
            MobileRepsDashboardModel objEntity = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetMobileRepsDashboard";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new MobileRepsDashboardModel();
                            if (item["BillVal1300"] != DBNull.Value) objEntity.BillVal1300 = item["BillVal1300"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objEntity;
        }

        public bool SaveMobileRepDashboard(MobileRepsDashboardModel mobileDashboard)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateMobileRepDashboard";

                    //objCommand.Parameters.AddWithValue("@datetime", originator);
                    objCommand.Parameters.AddWithValue("@Originator", mobileDashboard.originator);
                    objCommand.Parameters.AddWithValue("@BillVal1300", mobileDashboard.BillVal1300);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public List<CustomerCategory> GetAllCustomerCategory()
        {
            CustomerCategory cModel = null;
            DataTable objDS = new DataTable();
            List<CustomerCategory> cList = new List<CustomerCategory>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllCustomerCategory";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            cModel = new CustomerCategory();
                            if (item["category_id"] != DBNull.Value) cModel.CategoryId = Convert.ToInt32(item["category_id"]);
                            if (item["category_name"] != DBNull.Value) cModel.CategoryName = item["category_name"].ToString();
                           
                            cList.Add(cModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return cList;
        }

        public bool UploadOutletBulkTransfer(string Originator, int NewRouteId, string CustomerCode, string CustomerName)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOutletBulkTransfer";

                    objCommand.Parameters.AddWithValue("@Originator", Originator);
                    objCommand.Parameters.AddWithValue("@NewRoute", NewRouteId);
                    objCommand.Parameters.AddWithValue("@CustomerCode", CustomerCode);
                    objCommand.Parameters.AddWithValue("@CustomerName", CustomerName);
                    
                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                retStatus = false;
            }

            return retStatus;
        }

        
    }
}
