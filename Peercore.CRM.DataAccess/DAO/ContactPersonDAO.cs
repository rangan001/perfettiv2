﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ContactPersonDAO : BaseDAO
    {
        private DbSqlAdapter ContactPersonSql { get; set; }

        private void RegisterSql()
        {
            this.ContactPersonSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ContactPersonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ContactPersonDAO()
        {
            RegisterSql();
        }

        public ContactPersonDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        
        public ContactPersonEntity CreateObject()
        {
            return ContactPersonEntity.CreateObject();
        }

        public ContactPersonEntity CreateObject(int entityId)
        {
            return ContactPersonEntity.CreateObject(entityId);
        }

        public bool InsertLeadContact(ContactPersonEntity contactPersonEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                 DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                 paramCollection.Add("@contactPersonID", contactPersonEntity.ContactPersonID, DbType.Int32);
                 paramCollection.Add("@originator", contactPersonEntity.Originator, DbType.String);
                 paramCollection.Add("@title", contactPersonEntity.Title, DbType.String);
                 paramCollection.Add("@firstName", contactPersonEntity.FirstName, DbType.String);
                 paramCollection.Add("@lastName", contactPersonEntity.LastName, DbType.String);
                 paramCollection.Add("@position", contactPersonEntity.Position, DbType.String);
                 paramCollection.Add("@telephone", contactPersonEntity.Telephone, DbType.String);
                 paramCollection.Add("@fax", contactPersonEntity.Fax, DbType.String);
                 paramCollection.Add("@mobile", contactPersonEntity.Mobile, DbType.String);
                 paramCollection.Add("@emailAddress", contactPersonEntity.EmailAddress, DbType.String);
                 paramCollection.Add("@reportsTo", contactPersonEntity.ReportsTo, DbType.String);
                 paramCollection.Add("@mailingAddress", contactPersonEntity.MailingAddress, DbType.String);
                 paramCollection.Add("@mailingCity", contactPersonEntity.MailingCity, DbType.String);
                 paramCollection.Add("@mailingState", contactPersonEntity.MailingState, DbType.String);
                 paramCollection.Add("@mailingPostcode", contactPersonEntity.MailingPostcode, DbType.String);
                 paramCollection.Add("@mailingCountry", contactPersonEntity.MailingCountry, DbType.String);
                 paramCollection.Add("@otherAddress", contactPersonEntity.OtherAddress, DbType.String);
                 paramCollection.Add("@otherCity", contactPersonEntity.OtherCity, DbType.String);
                 paramCollection.Add("@otherState", contactPersonEntity.OtherState, DbType.String);
                 paramCollection.Add("@otherPostcode", contactPersonEntity.OtherPostCode, DbType.String);
                 paramCollection.Add("@otherCountry", contactPersonEntity.OtherCountry, DbType.String);
                 paramCollection.Add("@description", contactPersonEntity.Description, DbType.String);
                 paramCollection.Add("@createdBy", contactPersonEntity.CreatedBy, DbType.String);
                 paramCollection.Add("@createdDate", contactPersonEntity.CreatedDate, DbType.DateTime);
                 paramCollection.Add("@leadID", contactPersonEntity.LeadID, DbType.Int32);
                 paramCollection.Add("@lastModifiedBy", contactPersonEntity.LastModifiedBy, DbType.String);
                 paramCollection.Add("@lastModifiedDate", contactPersonEntity.LastModifiedDate, DbType.DateTime);
                 paramCollection.Add("@specialInterest", contactPersonEntity.SpecialInterests, DbType.String);
                 paramCollection.Add("@imagePath ", contactPersonEntity.ImagePath, DbType.String);
                 paramCollection.Add("@keyContact", contactPersonEntity.KeyContact, DbType.String);
                 paramCollection.Add("@contactType", contactPersonEntity.ContactType, DbType.String);
                 paramCollection.Add("@origin", contactPersonEntity.Origin, DbType.String);
                
                iNoRec = this.DataAcessService.ExecuteNonQuery(ContactPersonSql["InsertContact"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateLeadContact(ContactPersonEntity contactPersonEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@contactPersonID", contactPersonEntity.ContactPersonID, DbType.Int32);
                paramCollection.Add("@originator", contactPersonEntity.Originator, DbType.String);
                paramCollection.Add("@title", contactPersonEntity.Title, DbType.String);
                paramCollection.Add("@firstName", contactPersonEntity.FirstName, DbType.String);
                paramCollection.Add("@lastName", contactPersonEntity.LastName, DbType.String);
                paramCollection.Add("@position", contactPersonEntity.Position, DbType.String);
                paramCollection.Add("@telephone", contactPersonEntity.Telephone, DbType.String);
                paramCollection.Add("@fax", contactPersonEntity.Fax, DbType.String);
                paramCollection.Add("@mobile", contactPersonEntity.Mobile, DbType.String);
                paramCollection.Add("@emailAddress", contactPersonEntity.EmailAddress, DbType.String);
                paramCollection.Add("@reportsTo", contactPersonEntity.ReportsTo, DbType.String);
                paramCollection.Add("@mailingAddress", contactPersonEntity.MailingAddress, DbType.String);
                paramCollection.Add("@mailingCity", contactPersonEntity.MailingCity, DbType.String);
                paramCollection.Add("@mailingState", contactPersonEntity.MailingState, DbType.String);
                paramCollection.Add("@mailingPostcode", contactPersonEntity.MailingPostcode, DbType.String);
                paramCollection.Add("@mailingCountry", contactPersonEntity.MailingCountry, DbType.String);
                paramCollection.Add("@otherAddress", contactPersonEntity.OtherAddress, DbType.String);
                paramCollection.Add("@otherCity", contactPersonEntity.OtherCity, DbType.String);
                paramCollection.Add("@otherState", contactPersonEntity.OtherState, DbType.String);
                paramCollection.Add("@otherPostcode", contactPersonEntity.OtherPostCode, DbType.String);
                paramCollection.Add("@otherCountry", contactPersonEntity.OtherCountry, DbType.String);
                paramCollection.Add("@description", contactPersonEntity.Description, DbType.String);
                paramCollection.Add("@createdBy", contactPersonEntity.CreatedBy, DbType.String);
                paramCollection.Add("@createdDate", contactPersonEntity.CreatedDate, DbType.DateTime);
                paramCollection.Add("@leadID", contactPersonEntity.LeadID, DbType.Int32);
                paramCollection.Add("@lastModifiedBy", contactPersonEntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@lastModifiedDate", contactPersonEntity.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@specialInterest", contactPersonEntity.SpecialInterests, DbType.String);
                paramCollection.Add("@imagePath ", contactPersonEntity.ImagePath, DbType.String);
                paramCollection.Add("@keyContact", contactPersonEntity.KeyContact, DbType.String);
                paramCollection.Add("@contactType", contactPersonEntity.ContactType, DbType.String);
                paramCollection.Add("@origin", contactPersonEntity.Origin, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ContactPersonSql["UpdateContact"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateKeyContact(int leadID, int contactPersonID, string keyContact)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@keyContact", keyContact, DbType.String);
                paramCollection.Add("@leadID", leadID, DbType.Int32);
                paramCollection.Add("@contactPersonID", contactPersonID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ContactPersonSql["UpdateKeyContact"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public int GetNextContactId()
        {
            try
            {
               
                return  System.Convert.ToInt32(this.DataAcessService.GetOneValue(ContactPersonSql["GetNextContactId"]));   // Get Next ID               
            }
            catch
            {
                throw;
            }
        }

        public int GetNextCustomerContactId()
        {
            try
            {

                return System.Convert.ToInt32(this.DataAcessService.GetOneValue(ContactPersonSql["GetNextCustomerContactId"]));   // Get Next ID               
            }
            catch
            {
                throw;
            }
        }

        public ContactPersonEntity GetContactPerson(int contactPersonID)
        {
            DbDataReader drContactPerson = null;

            try
            {
                ContactPersonEntity contactPerson = ContactPersonEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@contactPersonID", contactPersonID, DbType.Int32);

                drContactPerson = this.DataAcessService.ExecuteQuery(ContactPersonSql["GetContactDetails"], paramCollection);

                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactPersonIdOrdinal = drContactPerson.GetOrdinal("contact_person_id");
                    int leadIdOrdinal = drContactPerson.GetOrdinal("lead_id");
                    int emailAddressOrdinal = drContactPerson.GetOrdinal("email_address");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");

                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");

                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");

                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");

                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int leadNameOrdinal = drContactPerson.GetOrdinal("lead_name");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");

                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int contactTypeOrdinal = drContactPerson.GetOrdinal("contact_type");
                    int originOrdinal = drContactPerson.GetOrdinal("origin");

                    if (drContactPerson.Read())
                    {
                        if (!drContactPerson.IsDBNull(contactPersonIdOrdinal)) contactPerson.ContactPersonID = drContactPerson.GetInt32(contactPersonIdOrdinal);
                        if (!drContactPerson.IsDBNull(leadIdOrdinal)) contactPerson.LeadID = drContactPerson.GetInt32(leadIdOrdinal);
                        if (!drContactPerson.IsDBNull(emailAddressOrdinal)) contactPerson.EmailAddress = drContactPerson.GetString(emailAddressOrdinal);
                        if (!drContactPerson.IsDBNull(titleOrdinal)) contactPerson.Title = drContactPerson.GetString(titleOrdinal);
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) contactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);

                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) contactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) contactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) contactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) contactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) contactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);

                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) contactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) contactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) contactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) contactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) contactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);

                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) contactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) contactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) contactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) contactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal).Trim();
                        if (!drContactPerson.IsDBNull(positionOrdinal)) contactPerson.Position = drContactPerson.GetString(positionOrdinal);

                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) contactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) contactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(faxOrdinal)) contactPerson.Fax = drContactPerson.GetString(faxOrdinal).Trim();
                        if (!drContactPerson.IsDBNull(leadNameOrdinal)) contactPerson.LeadName = drContactPerson.GetString(leadNameOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) contactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);

                        if (!drContactPerson.IsDBNull(createdByOrdinal)) contactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedByOrdinal)) contactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) contactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal).ToLocalTime();
                        if (!drContactPerson.IsDBNull(lastModifiedDateOrdinal)) contactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal).ToLocalTime();
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) contactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);

                        if (!drContactPerson.IsDBNull(keyContactOrdinal)) contactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);
                        if (!drContactPerson.IsDBNull(contactTypeOrdinal)) contactPerson.ContactType = drContactPerson.GetString(contactTypeOrdinal);
                        if (!drContactPerson.IsDBNull(originOrdinal)) contactPerson.Origin = drContactPerson.GetString(originOrdinal);

                    }
                }

                return contactPerson;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }


        public ContactPersonEntity GetCustomerContactPerson(int contactPersonID)
        {
            DbDataReader drContactPerson = null;

            try
            {
                ContactPersonEntity contactPerson = ContactPersonEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@contactPersonID", contactPersonID, DbType.Int32);

                drContactPerson = this.DataAcessService.ExecuteQuery(ContactPersonSql["GetContactDetailByContactPersonId"], paramCollection);

                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactOrdinal = drContactPerson.GetOrdinal("contact");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int internetAddOrdinal = drContactPerson.GetOrdinal("internet_add");
                    int contactNoOrdinal = drContactPerson.GetOrdinal("contact_no");
                    int noteOrdinal = drContactPerson.GetOrdinal("note");

                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");
                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");
                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");
                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");
                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    int contactTypeOrdinal = drContactPerson.GetOrdinal("contact_type");
                    int originOrdinal = drContactPerson.GetOrdinal("origin");
                 //   int totcountOrdinal = drContactPerson.GetOrdinal("totcount");

                    if (drContactPerson.Read())
                    {

                        if (!drContactPerson.IsDBNull(contactNoOrdinal)) contactPerson.ContactPersonID = drContactPerson.GetInt32(contactNoOrdinal);
                        if (!drContactPerson.IsDBNull(contactOrdinal)) contactPerson.Contact = drContactPerson.GetString(contactOrdinal);
                        if (!drContactPerson.IsDBNull(positionOrdinal)) contactPerson.Position = drContactPerson.GetString(positionOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) contactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal);
                        if (!drContactPerson.IsDBNull(faxOrdinal)) contactPerson.Fax = drContactPerson.GetString(faxOrdinal);
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) contactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(internetAddOrdinal)) contactPerson.EmailAddress = drContactPerson.GetString(internetAddOrdinal);
                        if (!drContactPerson.IsDBNull(noteOrdinal)) contactPerson.Note = drContactPerson.GetString(noteOrdinal);

                        if (!drContactPerson.IsDBNull(titleOrdinal)) contactPerson.Title = drContactPerson.GetString(titleOrdinal);
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) contactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);
                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) contactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) contactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) contactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) contactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) contactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);
                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) contactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) contactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) contactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) contactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);
                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) contactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) contactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) contactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);

                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) contactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) contactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) contactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);
                        if (!drContactPerson.IsDBNull(createdByOrdinal)) contactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedByOrdinal)) contactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) contactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedDateOrdinal)) contactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal);
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) contactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);
                        if (!drContactPerson.IsDBNull(keyContactOrdinal)) contactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);

                        if (!drContactPerson.IsDBNull(contactTypeOrdinal)) contactPerson.ContactType = drContactPerson.GetString(contactTypeOrdinal);
                        if (!drContactPerson.IsDBNull(originOrdinal)) contactPerson.Origin = drContactPerson.GetString(originOrdinal).Trim();
                     //   if (!drContactPerson.IsDBNull(totcountOrdinal)) contactPerson.TotalCount = drContactPerson.GetInt32(totcountOrdinal);

                        if (contactPerson.ContactType == "P")
                            contactPerson.TypeDescription = "Personal";
                        else if (contactPerson.ContactType == "D" && contactPerson.Origin == "COA")
                            contactPerson.TypeDescription = "CofA";
                        else
                            contactPerson.TypeDescription = "Department";

                        if (string.IsNullOrEmpty(contactPerson.Contact) &&
                            (!string.IsNullOrEmpty(contactPerson.FirstName) ||
                            !string.IsNullOrEmpty(contactPerson.LastName)))
                        {
                            string ContactName = contactPerson.FirstName;
                            ContactName = string.IsNullOrEmpty(ContactName) ? contactPerson.LastName : ContactName + " " + contactPerson.LastName;
                            contactPerson.Contact = ContactName;

                        }
                    }
                }

                return contactPerson;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }


        public ContactPersonEntity GetContactPerson(string firstName, string emailAddress, string telephone, string mobile)
        {
            DbDataReader idrPerson = null;
            ContactPersonEntity contactPerson = null;

            string sWhereClsLead = "";
            string sWhereClsCust = "";

            if (firstName != "")
            {
                sWhereClsLead = " UPPER(first_name) LIKE '" + firstName.ToUpper() + "%'";
                sWhereClsCust = " UPPER(c.contact) LIKE '" + firstName.ToUpper() + "%'";
            }

            if (emailAddress != "")
            {
                sWhereClsLead = sWhereClsLead != "" ? sWhereClsLead + " AND c.email_address = '" + emailAddress + "'" : " c.email_address = '" + emailAddress + "'";
                sWhereClsCust = sWhereClsCust != "" ? sWhereClsCust + " AND c.internet_add = '" + emailAddress + "'" : " c.internet_add = '" + emailAddress + "'";
            }

            if (telephone != "")
            {
                sWhereClsLead = sWhereClsLead != "" ? sWhereClsLead + " AND c.telephone = '" + telephone + "'" : " c.telephone = '" + telephone + "'";
                sWhereClsCust = sWhereClsCust != "" ? sWhereClsCust + " AND c.telephone = '" + telephone + "'" : " c.telephone = '" + telephone + "'";
            }

            if (mobile != "")
            {
                sWhereClsLead = sWhereClsLead != "" ? sWhereClsLead + " AND c.mobile = '" + mobile + "'" : " c.mobile = '" + mobile + "'";
                sWhereClsCust = sWhereClsCust != "" ? sWhereClsCust + " AND c.mobile = '" + mobile + "'" : " c.mobile = '" + mobile + "'";
            }

            try
            {

                idrPerson = this.DataAcessService.ExecuteQuery(ContactPersonSql["FindContactPerson"].Format(" AND " + sWhereClsLead, " WHERE " + sWhereClsCust));

                if (idrPerson != null && idrPerson.HasRows)
                {
                    if (idrPerson.Read())
                    {
                        contactPerson = ContactPersonEntity.CreateObject();

                        contactPerson.ContactPersonID = !idrPerson.IsDBNull(idrPerson.GetOrdinal("contact_person_id")) ?
                            idrPerson.GetInt32(idrPerson.GetOrdinal("contact_person_id")) : 0;

                        contactPerson.FirstName = !idrPerson.IsDBNull(idrPerson.GetOrdinal("first_name")) ?
                            idrPerson.GetString(idrPerson.GetOrdinal("first_name")).Trim() : string.Empty;

                        contactPerson.EmailAddress = !idrPerson.IsDBNull(idrPerson.GetOrdinal("email_address")) ?
                            idrPerson.GetString(idrPerson.GetOrdinal("email_address")).Trim() : string.Empty;

                        contactPerson.Telephone = !idrPerson.IsDBNull(idrPerson.GetOrdinal("telephone")) ?
                            idrPerson.GetString(idrPerson.GetOrdinal("telephone")).Trim() : string.Empty;

                        contactPerson.Mobile = !idrPerson.IsDBNull(idrPerson.GetOrdinal("mobile")) ?
                            idrPerson.GetString(idrPerson.GetOrdinal("mobile")).Trim() : string.Empty;

                        // Lead Details
                        contactPerson.LeadID = !idrPerson.IsDBNull(idrPerson.GetOrdinal("lead_id")) ?
                            idrPerson.GetInt32(idrPerson.GetOrdinal("lead_id")) : 0;

                        contactPerson.LeadName = !idrPerson.IsDBNull(idrPerson.GetOrdinal("lead_name")) ?
                            idrPerson.GetString(idrPerson.GetOrdinal("lead_name")) : string.Empty;

                        contactPerson.CustCode = !idrPerson.IsDBNull(idrPerson.GetOrdinal("cust_code")) ?
                            idrPerson.GetString(idrPerson.GetOrdinal("cust_code")).Trim() : string.Empty;

                    }
                }

                return contactPerson;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrPerson != null)
                    idrPerson.Close();
            }
        }

        public List<ContactPersonEntity> GetCustomerContacts(ArgsEntity args , string custCode, string clientType ,List<OriginatorEntity> childoriginatorlist, bool includeChildReps = false)
        {
            DbDataReader drContactPerson = null;
            string sClientTypes = "";

            try
            {
                List<ContactPersonEntity> contactPersonCollection = new List<ContactPersonEntity>();
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (includeChildReps)
                {
                    sClientTypes = "";

                    OriginatorEntity originator =new OriginatorEntity();
                    originator.UserName = args.Originator;
                    originator.ClientType = args.ClientType;
                    childoriginatorlist.Add(originator);
                    List<string> listClientTypes = childoriginatorlist.Where(s => s.ClientType != "").Select(s => s.ClientType).Distinct().ToList();

                    if (listClientTypes.Count > 0)
                    {
                        sClientTypes = "origin IN (";

                        foreach (string sType in listClientTypes)
                            sClientTypes += "'" + sType + "',";

                        sClientTypes = sClientTypes.Remove(sClientTypes.Length - 1);
                        sClientTypes += ")";
                    }

                    paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "origin"), DbType.String);
                }
    
                paramCollection.Add("@CustCode", custCode, DbType.String);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ClientType", sClientTypes, DbType.String);
                paramCollection.Add("@IsIncludeChildReps", includeChildReps, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drContactPerson = this.DataAcessService.ExecuteQuery(ContactPersonSql["GetCustomerContacts"],paramCollection);

                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactOrdinal = drContactPerson.GetOrdinal("contact");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int internetAddOrdinal = drContactPerson.GetOrdinal("internet_add");
                    int contactNoOrdinal = drContactPerson.GetOrdinal("contact_no");
                    int noteOrdinal = drContactPerson.GetOrdinal("note");

                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");
                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");
                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");
                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");
                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    int contactTypeOrdinal = drContactPerson.GetOrdinal("contact_type");
                    int originOrdinal = drContactPerson.GetOrdinal("origin");
                    int totcountOrdinal = drContactPerson.GetOrdinal("totcount");

                    while (drContactPerson.Read())
                    {
                        ContactPersonEntity contactPerson = ContactPersonEntity.CreateObject();

                        if (!drContactPerson.IsDBNull(contactNoOrdinal)) contactPerson.ContactPersonID = drContactPerson.GetInt32(contactNoOrdinal);
                        if (!drContactPerson.IsDBNull(contactOrdinal)) contactPerson.Contact = drContactPerson.GetString(contactOrdinal);
                        if (!drContactPerson.IsDBNull(positionOrdinal)) contactPerson.Position = drContactPerson.GetString(positionOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) contactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal);
                        if (!drContactPerson.IsDBNull(faxOrdinal)) contactPerson.Fax = drContactPerson.GetString(faxOrdinal);
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) contactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(internetAddOrdinal)) contactPerson.EmailAddress = drContactPerson.GetString(internetAddOrdinal);
                        if (!drContactPerson.IsDBNull(noteOrdinal)) contactPerson.Note = drContactPerson.GetString(noteOrdinal);

                        if (!drContactPerson.IsDBNull(titleOrdinal)) contactPerson.Title = drContactPerson.GetString(titleOrdinal);
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) contactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);
                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) contactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) contactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) contactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) contactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) contactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);
                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) contactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) contactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) contactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) contactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);
                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) contactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) contactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) contactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);

                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) contactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) contactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) contactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);
                        if (!drContactPerson.IsDBNull(createdByOrdinal)) contactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedByOrdinal)) contactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) contactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedDateOrdinal)) contactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal);
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) contactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);
                        if (!drContactPerson.IsDBNull(keyContactOrdinal)) contactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);

                        if (!drContactPerson.IsDBNull(contactTypeOrdinal)) contactPerson.ContactType = drContactPerson.GetString(contactTypeOrdinal);
                        if (!drContactPerson.IsDBNull(originOrdinal)) contactPerson.Origin = drContactPerson.GetString(originOrdinal).Trim();
                        if (!drContactPerson.IsDBNull(totcountOrdinal)) contactPerson.TotalCount = drContactPerson.GetInt32(totcountOrdinal);

                        if (contactPerson.ContactType == "P")
                            contactPerson.TypeDescription = "Personal";
                        else if (contactPerson.ContactType == "D" && contactPerson.Origin == "COA")
                            contactPerson.TypeDescription = "CofA";
                        else
                            contactPerson.TypeDescription = "Department";

                        if (string.IsNullOrEmpty(contactPerson.Contact) &&
                            (!string.IsNullOrEmpty(contactPerson.FirstName) ||
                            !string.IsNullOrEmpty(contactPerson.LastName)))
                        {
                            string ContactName = contactPerson.FirstName;
                            ContactName = string.IsNullOrEmpty(ContactName) ? contactPerson.LastName : ContactName + " " + contactPerson.LastName;
                            contactPerson.Contact = ContactName;
                        }

                        contactPersonCollection.Add(contactPerson);

                    }
                }

                return contactPersonCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }

        public ContactPersonEntity GetCustomerContact(string custCode, string contactType, string origin)
        {
            DbDataReader drContactPerson = null;

            try
            {
                ContactPersonEntity contactPerson = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@custCode", custCode, DbType.String);
                paramCollection.Add("@contactType", contactType, DbType.String);
                paramCollection.Add("@origin", origin, DbType.String);

                drContactPerson = this.DataAcessService.ExecuteQuery(ContactPersonSql["GetCustomerContact"], paramCollection);

                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactOrdinal = drContactPerson.GetOrdinal("contact");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int internetAddOrdinal = drContactPerson.GetOrdinal("internet_add");
                    int contactNoOrdinal = drContactPerson.GetOrdinal("contact_no");
                    int noteOrdinal = drContactPerson.GetOrdinal("note");

                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");
                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");
                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");
                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");
                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    int contactTypeOrdinal = drContactPerson.GetOrdinal("contact_type");
                    int originOrdinal = drContactPerson.GetOrdinal("origin");

                    if (drContactPerson.Read())
                    {
                        contactPerson = ContactPersonEntity.CreateObject();

                        if (!drContactPerson.IsDBNull(contactNoOrdinal)) contactPerson.ContactPersonID = drContactPerson.GetInt32(contactNoOrdinal);
                        if (!drContactPerson.IsDBNull(contactOrdinal)) contactPerson.Contact = drContactPerson.GetString(contactOrdinal);
                        if (!drContactPerson.IsDBNull(positionOrdinal)) contactPerson.Position = drContactPerson.GetString(positionOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) contactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal).Trim();
                        if (!drContactPerson.IsDBNull(faxOrdinal)) contactPerson.Fax = drContactPerson.GetString(faxOrdinal).Trim();
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) contactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(internetAddOrdinal)) contactPerson.EmailAddress = drContactPerson.GetString(internetAddOrdinal);
                        if (!drContactPerson.IsDBNull(noteOrdinal)) contactPerson.Description = drContactPerson.GetString(noteOrdinal);

                        if (!drContactPerson.IsDBNull(titleOrdinal)) contactPerson.Title = drContactPerson.GetString(titleOrdinal).Trim();
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) contactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);
                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) contactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) contactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) contactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) contactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) contactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);
                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) contactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) contactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) contactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) contactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);
                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) contactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) contactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) contactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);

                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) contactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) contactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) contactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);
                        if (!drContactPerson.IsDBNull(createdByOrdinal)) contactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedByOrdinal)) contactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) contactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedDateOrdinal)) contactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal);
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) contactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);
                        if (!drContactPerson.IsDBNull(keyContactOrdinal)) contactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);

                        if (!drContactPerson.IsDBNull(contactTypeOrdinal)) contactPerson.ContactType = drContactPerson.GetString(contactTypeOrdinal);
                        if (!drContactPerson.IsDBNull(originOrdinal)) contactPerson.Origin = drContactPerson.GetString(originOrdinal).Trim();
                    }
                }

                return contactPerson;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }
        
        public bool InsertCustomerContact(ContactPersonEntity contactPersonEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            //if(string.IsNullOrEmpty(contactPersonEntity.CustCode))
            //    contactPersonEntity.CustCode = contactPersonEntity.cst

            try
            {
                UpdateCustomerKeyContact(contactPersonEntity);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

               // paramCollection.Add("@contactNo", contactPersonEntity.ContactPersonID, DbType.Int32);
                paramCollection.Add("@contact", contactPersonEntity.Contact, DbType.String);
                paramCollection.Add("@position", contactPersonEntity.Position, DbType.String);
                paramCollection.Add("@origin", contactPersonEntity.Origin, DbType.String);
                paramCollection.Add("@telephone", contactPersonEntity.Telephone, DbType.String);
                paramCollection.Add("@fax", contactPersonEntity.Fax, DbType.String);
                paramCollection.Add("@mobile", contactPersonEntity.Mobile, DbType.String);
                paramCollection.Add("@internetAdd", contactPersonEntity.EmailAddress, DbType.String);
                paramCollection.Add("@status", string.IsNullOrEmpty(contactPersonEntity.Status) ? "" : contactPersonEntity.Status, DbType.String);
                paramCollection.Add("@note", string.IsNullOrEmpty(contactPersonEntity.Note) ? "" : contactPersonEntity.Note, DbType.String);
                paramCollection.Add("@custCode", contactPersonEntity.CustCode, DbType.String);
                paramCollection.Add("@originator", contactPersonEntity.Originator, DbType.String);
                paramCollection.Add("@title", contactPersonEntity.Title, DbType.String);
                paramCollection.Add("@firstName", contactPersonEntity.FirstName, DbType.String);
                paramCollection.Add("@lastName", contactPersonEntity.LastName, DbType.String);
                paramCollection.Add("@reportsTo", contactPersonEntity.ReportsTo, DbType.String);
                paramCollection.Add("@mailingAddress", contactPersonEntity.MailingAddress, DbType.String);
                paramCollection.Add("@mailingCity", contactPersonEntity.MailingCity, DbType.String);
                paramCollection.Add("@mailingState", contactPersonEntity.MailingState, DbType.String);
                paramCollection.Add("@mailingPostcode", contactPersonEntity.MailingPostcode, DbType.String);
                paramCollection.Add("@mailingCountry", contactPersonEntity.MailingCountry, DbType.String);
                paramCollection.Add("@otherAddress", contactPersonEntity.OtherAddress, DbType.String);
                paramCollection.Add("@otherCity", contactPersonEntity.OtherCity, DbType.String);
                paramCollection.Add("@otherState", contactPersonEntity.OtherState, DbType.String);
                paramCollection.Add("@otherPostcode", contactPersonEntity.OtherPostCode, DbType.String);
                paramCollection.Add("@otherCountry", contactPersonEntity.OtherCountry, DbType.String);
                paramCollection.Add("@description", contactPersonEntity.Description, DbType.String);
                paramCollection.Add("@createdBy", contactPersonEntity.CreatedBy, DbType.String);
                paramCollection.Add("@createdDate", contactPersonEntity.CreatedDate, DbType.DateTime);
                paramCollection.Add("@lastModifiedBy", contactPersonEntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@lastModifiedDate", contactPersonEntity.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@specialInterest", contactPersonEntity.SpecialInterests, DbType.String);
                paramCollection.Add("@imagePath", contactPersonEntity.ImagePath, DbType.String);
                paramCollection.Add("@keyContact", contactPersonEntity.KeyContact, DbType.String);
                paramCollection.Add("@contactType", contactPersonEntity.ContactType, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ContactPersonSql["InsertCustomerContact"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        public void UpdateCustomerKeyContact(ContactPersonEntity contactPersonEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection1 = new DbInputParameterCollection();

                paramCollection1.Add("@contactPersonID", contactPersonEntity.ContactPersonID, DbType.Int32);
                paramCollection1.Add("@custCode", contactPersonEntity.CustCode, DbType.String);

                if (contactPersonEntity.KeyContact == "Y")
                    iNoRec = DataAcessService.ExecuteNonQuery(ContactPersonSql["UpdateCustomerKeyContact"], paramCollection1);
                if (iNoRec > 0)
                    successful = true;

                // return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateCustomerContact(ContactPersonEntity contactPersonEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                UpdateCustomerKeyContact(contactPersonEntity);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@contact", contactPersonEntity.Contact, DbType.String);
                paramCollection.Add("@position", contactPersonEntity.Position, DbType.String);
                paramCollection.Add("@origin", contactPersonEntity.Origin, DbType.String);
                paramCollection.Add("@telephone", contactPersonEntity.Telephone, DbType.String);
                paramCollection.Add("@fax", contactPersonEntity.Fax, DbType.String);
                paramCollection.Add("@mobile", contactPersonEntity.Mobile, DbType.String);
                paramCollection.Add("@internetAdd", contactPersonEntity.EmailAddress, DbType.String);
                paramCollection.Add("@status", contactPersonEntity.Status, DbType.String);
                paramCollection.Add("@note", contactPersonEntity.Note, DbType.String);
                paramCollection.Add("@custCode", contactPersonEntity.CustCode, DbType.String);
                paramCollection.Add("@originator", contactPersonEntity.Originator, DbType.String);
                paramCollection.Add("@title", contactPersonEntity.Title, DbType.String);
                paramCollection.Add("@firstName", contactPersonEntity.FirstName, DbType.String);
                paramCollection.Add("@lastName", contactPersonEntity.LastName, DbType.String);
                paramCollection.Add("@reportsTo", contactPersonEntity.ReportsTo, DbType.String);
                paramCollection.Add("@mailingAddress", contactPersonEntity.MailingAddress, DbType.String);
                paramCollection.Add("@mailingCity", contactPersonEntity.MailingCity, DbType.String);
                paramCollection.Add("@mailingState", contactPersonEntity.MailingState, DbType.String);
                paramCollection.Add("@mailingPostcode", contactPersonEntity.MailingPostcode, DbType.String);
                paramCollection.Add("@mailingCountry", contactPersonEntity.MailingCountry, DbType.String);
                paramCollection.Add("@otherAddress", contactPersonEntity.OtherAddress, DbType.String);
                paramCollection.Add("@otherCity", contactPersonEntity.OtherCity, DbType.String);
                paramCollection.Add("@otherState", contactPersonEntity.OtherState, DbType.String);
                paramCollection.Add("@otherPostcode", contactPersonEntity.OtherPostCode, DbType.String);
                paramCollection.Add("@otherCountry", contactPersonEntity.OtherCountry, DbType.String);
                paramCollection.Add("@description", contactPersonEntity.Description, DbType.String);
                paramCollection.Add("@createdBy", contactPersonEntity.CreatedBy, DbType.String);
                paramCollection.Add("@createdDate", contactPersonEntity.CreatedDate, DbType.DateTime);
                paramCollection.Add("@lastModifiedBy", contactPersonEntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@lastModifiedDate", contactPersonEntity.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@specialInterest", contactPersonEntity.SpecialInterests, DbType.String);
                paramCollection.Add("@imagePath", contactPersonEntity.ImagePath, DbType.String);
                paramCollection.Add("@keyContact", contactPersonEntity.KeyContact, DbType.String);
                paramCollection.Add("@contactType", contactPersonEntity.ContactType, DbType.String);
               

                iNoRec = this.DataAcessService.ExecuteNonQuery(ContactPersonSql["UpdateCustomerContact"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool isCustomerContactExist(ContactPersonEntity contactPersonEntity)
        {
            DbDataReader drCustomerContact = null;
            bool isExist = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@custCode", contactPersonEntity.CustCode, DbType.String);
                //paramCollection.Add("@contactType", contactPersonEntity.ContactType, DbType.String);
                //paramCollection.Add("@origin", contactPersonEntity.Origin, DbType.String);

                drCustomerContact = this.DataAcessService.ExecuteQuery(ContactPersonSql["GetCustomerContact"], paramCollection);

                if (drCustomerContact.HasRows)
                {
                    isExist = true;
                }
                else
                {
                    isExist = false;
                }

            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCustomerContact != null)
                    drCustomerContact.Close();
            }

            return isExist;
        }



        public bool DeleteCustomerContact(int contactId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@status", "D", DbType.String);
                paramCollection.Add("@delDate", DateTime.Now.ToUniversalTime(), DbType.DateTime);
                paramCollection.Add("@contactNo", contactId, DbType.Int32);
                

                iNoRec = this.DataAcessService.ExecuteNonQuery(ContactPersonSql["DeleteCustomerContact"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<ContactPersonEntity> GetContactPerson(string emailAddress)
        {

            DbDataReader drContactPerson = null;
            List<ContactPersonEntity> contactPersonList = null;
            ContactPersonEntity contactPerson = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@emailAddress", emailAddress, DbType.String);

                drContactPerson = this.DataAcessService.ExecuteQuery(ContactPersonSql["GetUniqueContactPerson"], paramCollection);
             
                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactPersonIdOrdinal = drContactPerson.GetOrdinal("contact_person_id");
                    int leadIdOrdinal = drContactPerson.GetOrdinal("lead_id");
                    int emailAddressOrdinal = drContactPerson.GetOrdinal("email_address");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");
                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");
                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");
                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");
                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int leadNameOrdinal = drContactPerson.GetOrdinal("lead_name");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");
                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int companyOrdinal = drContactPerson.GetOrdinal("company");
                    int addressOrdinal = drContactPerson.GetOrdinal("address1");
                    int companyTelephoneOrdinal = drContactPerson.GetOrdinal("CompanyTelephone");
                   
                    while (drContactPerson.Read())
                    {
                        contactPerson = ContactPersonEntity.CreateObject();

                        if (!drContactPerson.IsDBNull(contactPersonIdOrdinal)) contactPerson.ContactPersonID = drContactPerson.GetInt32(contactPersonIdOrdinal);
                        if (!drContactPerson.IsDBNull(leadIdOrdinal)) contactPerson.LeadID = drContactPerson.GetInt32(leadIdOrdinal);
                        if (!drContactPerson.IsDBNull(emailAddressOrdinal)) contactPerson.EmailAddress = drContactPerson.GetString(emailAddressOrdinal);
                        if (!drContactPerson.IsDBNull(titleOrdinal)) contactPerson.Title = drContactPerson.GetString(titleOrdinal);
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) contactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);
                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) contactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) contactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) contactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) contactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) contactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);
                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) contactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) contactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) contactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) contactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) contactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);
                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) contactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) contactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) contactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) contactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal);
                        if (!drContactPerson.IsDBNull(positionOrdinal)) contactPerson.Position = drContactPerson.GetString(positionOrdinal);
                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) contactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) contactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(faxOrdinal)) contactPerson.Fax = drContactPerson.GetString(faxOrdinal);
                        if (!drContactPerson.IsDBNull(leadNameOrdinal)) contactPerson.LeadName = drContactPerson.GetString(leadNameOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) contactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);

                        if (!drContactPerson.IsDBNull(createdByOrdinal)) contactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedByOrdinal)) contactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) contactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedDateOrdinal)) contactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal);
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) contactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) contactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);

                        if (!drContactPerson.IsDBNull(companyOrdinal)) contactPerson.CompanyName = drContactPerson.GetString(companyOrdinal);
                        if (!drContactPerson.IsDBNull(addressOrdinal)) contactPerson.CompanyAddress = drContactPerson.GetString(addressOrdinal);
                        if (!drContactPerson.IsDBNull(companyTelephoneOrdinal)) contactPerson.CompanyTelephone = drContactPerson.GetString(companyTelephoneOrdinal);

                        contactPersonList.Add(contactPerson);
                    }
                }

                return contactPersonList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }

        public List<ContactPersonEntity> GetCustomerContact(string emailAddress)
        {
            DbDataReader drContactPerson = null;

            try
            {
                ContactPersonEntity contactPerson = null;
                List<ContactPersonEntity> contactPersonList = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@internetAdd", emailAddress, DbType.String);
                
                drContactPerson = this.DataAcessService.ExecuteQuery(ContactPersonSql["GetCustomerContactByEmail"], paramCollection);

                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactOrdinal = drContactPerson.GetOrdinal("contact");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int internetAddOrdinal = drContactPerson.GetOrdinal("internet_add");
                    int contactNoOrdinal = drContactPerson.GetOrdinal("contact_no");
                    int noteOrdinal = drContactPerson.GetOrdinal("note");
                    int custCodeOrdinal = drContactPerson.GetOrdinal("cust_code");

                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");
                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");
                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");
                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");
                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    contactPersonList = new List<ContactPersonEntity>();

                    while (drContactPerson.Read())
                    {
                        contactPerson = ContactPersonEntity.CreateObject();

                        if (!drContactPerson.IsDBNull(contactNoOrdinal)) contactPerson.ContactPersonID = drContactPerson.GetInt32(contactNoOrdinal);
                        if (!drContactPerson.IsDBNull(contactOrdinal)) contactPerson.Contact = drContactPerson.GetString(contactOrdinal);
                        if (!drContactPerson.IsDBNull(positionOrdinal)) contactPerson.Position = drContactPerson.GetString(positionOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) contactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal);
                        if (!drContactPerson.IsDBNull(faxOrdinal)) contactPerson.Fax = drContactPerson.GetString(faxOrdinal);
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) contactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(internetAddOrdinal)) contactPerson.EmailAddress = drContactPerson.GetString(internetAddOrdinal);
                        if (!drContactPerson.IsDBNull(noteOrdinal)) contactPerson.Note = drContactPerson.GetString(noteOrdinal);
                        if (!drContactPerson.IsDBNull(custCodeOrdinal)) contactPerson.CustCode = drContactPerson.GetString(custCodeOrdinal);

                        if (!drContactPerson.IsDBNull(titleOrdinal)) contactPerson.Title = drContactPerson.GetString(titleOrdinal);
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) contactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);
                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) contactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) contactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) contactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) contactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) contactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);
                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) contactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) contactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) contactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) contactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);
                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) contactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) contactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) contactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);

                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) contactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) contactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) contactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);
                        if (!drContactPerson.IsDBNull(createdByOrdinal)) contactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedByOrdinal)) contactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) contactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedDateOrdinal)) contactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal);
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) contactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);
                        if (!drContactPerson.IsDBNull(keyContactOrdinal)) contactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);

                        contactPersonList.Add(contactPerson);
                    }
                }

                return contactPersonList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }

        public List<ContactPersonEntity> GetContactsByOrigin(ArgsEntity args, bool bIncludeChildReps = false)
        {
            DbDataReader drContactPerson = null;
            string sClientTypes = "", sReps = "";

            try
            {
                List<ContactPersonEntity> contactPersonCollection = new List<ContactPersonEntity>();

                if (bIncludeChildReps)
                {
                    sClientTypes = "";

                    List<OriginatorEntity> listOriginator = new OriginatorDAO().GetChildOriginatorsList(args.Originator);
                    OriginatorEntity originator = OriginatorEntity.CreateObject();
                    originator.UserName = args.Originator;
                    originator.ClientType = args.ClientType;
                    listOriginator.Add(originator);
                    List<string> listClientTypes = listOriginator.Where(s => s.ClientType != "").Select(s => s.ClientType).Distinct().ToList();

                    if (listClientTypes.Count > 0)
                    {
                        sClientTypes = "origin IN (";

                        foreach (string sType in listClientTypes)
                            sClientTypes += "'" + sType + "',";

                        sClientTypes = sClientTypes.Remove(sClientTypes.Length - 1);
                        sClientTypes += ")";
                    }

                    if (sClientTypes != "")
                        sClientTypes = "OR (contact_type = 'D' AND " + sClientTypes + ")";
                    else
                        sClientTypes = "OR (contact_type = 'D' AND origin = '')";

                    sReps = args.ChildOriginators;
                    sReps = sReps.Replace("originator", "origin");
                }
                else
                {
                    sClientTypes = "OR (contact_type = 'D' AND origin = '" + args.RepType + "')";
                    sReps = "origin = '" + args.Originator + "'";
                }


                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ClientTypes", sClientTypes, DbType.String);
                paramCollection.Add("@OriginatorList", sReps, DbType.String);
                paramCollection.Add("@LeadId", args.LeadId, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drContactPerson = this.DataAcessService.ExecuteQuery(ContactPersonSql["GetContactsByOrigin"], paramCollection);


                //drContactPerson = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetContactsByOrigin,
                //    LeadId, sClientTypes, sReps));

                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactPersonIdOrdinal = drContactPerson.GetOrdinal("contact_person_id");
                    int leadIdOrdinal = drContactPerson.GetOrdinal("lead_id");
                    int emailAddressOrdinal = drContactPerson.GetOrdinal("email_address");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");
                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");
                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");
                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");
                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    //int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    //int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");
                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int contactTypeOrdinal = drContactPerson.GetOrdinal("contact_type");
                    int originOrdinal = drContactPerson.GetOrdinal("origin");
                    int totalCountnOrdinal = drContactPerson.GetOrdinal("total_count");

                    while (drContactPerson.Read())
                    {
                        ContactPersonEntity contactPerson = ContactPersonEntity.CreateObject();

                        if (!drContactPerson.IsDBNull(contactPersonIdOrdinal)) contactPerson.ContactPersonID = drContactPerson.GetInt32(contactPersonIdOrdinal);
                        if (!drContactPerson.IsDBNull(leadIdOrdinal)) contactPerson.LeadID = drContactPerson.GetInt32(leadIdOrdinal);
                        if (!drContactPerson.IsDBNull(emailAddressOrdinal)) contactPerson.EmailAddress = drContactPerson.GetString(emailAddressOrdinal);
                        if (!drContactPerson.IsDBNull(titleOrdinal)) contactPerson.Title = drContactPerson.GetString(titleOrdinal);
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) contactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);
                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) contactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) contactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) contactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) contactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) contactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);
                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) contactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) contactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) contactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) contactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) contactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);
                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) contactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) contactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) contactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) contactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal);
                        if (!drContactPerson.IsDBNull(positionOrdinal)) contactPerson.Position = drContactPerson.GetString(positionOrdinal);
                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) contactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) contactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(faxOrdinal)) contactPerson.Fax = drContactPerson.GetString(faxOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) contactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);

                        if (!drContactPerson.IsDBNull(createdByOrdinal)) contactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        //contactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) contactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal);
                        //contactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal);
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) contactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);
                        if (!drContactPerson.IsDBNull(keyContactOrdinal)) contactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);
                        if (!drContactPerson.IsDBNull(contactTypeOrdinal)) contactPerson.ContactType = drContactPerson.GetString(contactTypeOrdinal);
                        if (!drContactPerson.IsDBNull(originOrdinal)) contactPerson.Origin = drContactPerson.GetString(originOrdinal);
                        if (!drContactPerson.IsDBNull(totalCountnOrdinal)) contactPerson.TotalCount = drContactPerson.GetInt32(totalCountnOrdinal);

                        contactPersonCollection.Add(contactPerson);

                    }
                }

                return contactPersonCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }
    }
}
