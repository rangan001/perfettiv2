﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ContactPersonImageDAO:BaseDAO
    {
        private DbSqlAdapter ContactPersonImageSql { get; set; }

        //public ContactPersonImageDAO()
        //{
        //    this.ContactPersonImageSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ContactPersonImageSql.xml");
        //}

        private void RegisterSql()
        {
            this.ContactPersonImageSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ContactPersonImageSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ContactPersonImageDAO()
        {
            RegisterSql();
        }

        public ContactPersonImageDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ContactPersonImageEntity CreateObject()
        {
            return ContactPersonImageEntity.CreateObject();
        }

        public ContactPersonImageEntity CreateObject(int entityId)
        {
            return ContactPersonImageEntity.CreateObject(entityId);
        }

        public ContactPersonImageEntity GetContactPersonImage(int contactPersonID, int leadId)
        {
            DbDataReader drContactPersonImage = null;
           
            try
            {
                ContactPersonImageEntity contactPersonImage = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@contactPersonID", contactPersonID, DbType.Int32);
                paramCollection.Add("@leadID", leadId, DbType.Int32);

                drContactPersonImage = this.DataAcessService.ExecuteQuery(ContactPersonImageSql["GetContactPersonImage"], paramCollection);

                if (drContactPersonImage != null && drContactPersonImage.HasRows)
                {
                    int contactImageOrdinal = drContactPersonImage.GetOrdinal("contact_image_id");
                    int contactPersonIdOrdinal = drContactPersonImage.GetOrdinal("contact_person_id");
                    int fileNameOrdinal = drContactPersonImage.GetOrdinal("file_name");
                    int leadIdOrdinal = drContactPersonImage.GetOrdinal("lead_id");
                    int fileContentOrdinal = drContactPersonImage.GetOrdinal("file_content");
                    


                    if (drContactPersonImage.Read())
                    {
                        contactPersonImage = ContactPersonImageEntity.CreateObject();

                        if (!drContactPersonImage.IsDBNull(contactPersonIdOrdinal)) contactPersonImage.ContactPersonId = drContactPersonImage.GetInt32(contactPersonIdOrdinal);
                        if (!drContactPersonImage.IsDBNull(contactImageOrdinal)) contactPersonImage.ContactPersonImageId = drContactPersonImage.GetInt32(contactImageOrdinal);
                        if (!drContactPersonImage.IsDBNull(fileNameOrdinal)) contactPersonImage.FileName = drContactPersonImage.GetString(fileNameOrdinal);
                        if (!drContactPersonImage.IsDBNull(leadIdOrdinal)) contactPersonImage.LeadID = drContactPersonImage.GetInt32(leadIdOrdinal);

                        if (!drContactPersonImage.IsDBNull(leadIdOrdinal)) contactPersonImage.LeadID = drContactPersonImage.GetInt32(leadIdOrdinal);

                        if (drContactPersonImage[fileContentOrdinal] != null)
                        {
                            if (!drContactPersonImage.IsDBNull(fileContentOrdinal)) contactPersonImage.FileContent = (byte[])drContactPersonImage[fileContentOrdinal];
                        }                      
                    }
                }
                             
                return contactPersonImage;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPersonImage != null)
                    drContactPersonImage.Close();
            }
        }

        public ContactPersonImageEntity GetContactPersonImage(int contactNo, string custCode)
        {
            DbDataReader drContactPersonImage = null;

            try
            {
                ContactPersonImageEntity contactPersonImage = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@contactNo", contactNo, DbType.Int32);
                paramCollection.Add("@leadID", custCode, DbType.String);

                drContactPersonImage = this.DataAcessService.ExecuteQuery(ContactPersonImageSql["GetContactPersonImageByCustomerCode"], paramCollection);

                if (drContactPersonImage != null && drContactPersonImage.HasRows)
                {
                    int contactImageOrdinal = drContactPersonImage.GetOrdinal("contact_image_id");
                    int contactPersonIdOrdinal = drContactPersonImage.GetOrdinal("contact_person_id");
                    int fileNameOrdinal = drContactPersonImage.GetOrdinal("file_name");
                    int leadIdOrdinal = drContactPersonImage.GetOrdinal("lead_id");
                    int fileContentOrdinal = drContactPersonImage.GetOrdinal("file_content");



                    if (drContactPersonImage.Read())
                    {
                        contactPersonImage = ContactPersonImageEntity.CreateObject();

                        if (!drContactPersonImage.IsDBNull(contactPersonIdOrdinal)) contactPersonImage.ContactPersonId = drContactPersonImage.GetInt32(contactPersonIdOrdinal);
                        if (!drContactPersonImage.IsDBNull(contactImageOrdinal)) contactPersonImage.ContactPersonImageId = drContactPersonImage.GetInt32(contactImageOrdinal);
                        if (!drContactPersonImage.IsDBNull(fileNameOrdinal)) contactPersonImage.FileName = drContactPersonImage.GetString(fileNameOrdinal);
                        if (!drContactPersonImage.IsDBNull(leadIdOrdinal)) contactPersonImage.LeadID = drContactPersonImage.GetInt32(leadIdOrdinal);

                        if (!drContactPersonImage.IsDBNull(leadIdOrdinal)) contactPersonImage.LeadID = drContactPersonImage.GetInt32(leadIdOrdinal);

                        if (drContactPersonImage[fileContentOrdinal] != null)
                        {
                            if (!drContactPersonImage.IsDBNull(fileContentOrdinal)) contactPersonImage.FileContent = (byte[])drContactPersonImage[fileContentOrdinal];
                        }
                    }
                }

                return contactPersonImage;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPersonImage != null)
                    drContactPersonImage.Close();
            }
        }
       
        public int GetNextContactPersonImageID()
        {
            try
            {
                return System.Convert.ToInt32(this.DataAcessService.GetOneValue(ContactPersonImageSql["GetNextContactPersonImageID"]));   // Get Next ID               
            }
            catch
            {
                throw;
            }
        }

        public bool InsertContactPersonImage(ContactPersonImageEntity contactPersonImageEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@contactImageID", contactPersonImageEntity.ContactPersonImageId, DbType.Int32);
                paramCollection.Add("@contactPersonID", contactPersonImageEntity.ContactPersonId, DbType.Int32);
                paramCollection.Add("@fileContent", contactPersonImageEntity.FileContent, DbType.Binary);
                paramCollection.Add("@leadID", contactPersonImageEntity.LeadID, DbType.Int32);
                paramCollection.Add("@custCode", string.IsNullOrEmpty(contactPersonImageEntity.CustomerCode) ? "" : contactPersonImageEntity.CustomerCode, DbType.String);
                paramCollection.Add("@fileName", contactPersonImageEntity.FileName, DbType.String);
                paramCollection.Add("@contactNo", contactPersonImageEntity.ContactNo, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ContactPersonImageSql["InsertContactPersonImage"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

      

        public bool UpdateContactPersonImage(ContactPersonImageEntity contactPersonImageEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@contactImageID", contactPersonImageEntity.ContactPersonImageId, DbType.Int32);
                paramCollection.Add("@contactPersonID", contactPersonImageEntity.ContactPersonId, DbType.Int32);
                paramCollection.Add("@fileContent", contactPersonImageEntity.FileContent, DbType.Binary);
                paramCollection.Add("@leadID", contactPersonImageEntity.LeadID, DbType.Int32);
                paramCollection.Add("@custCode", string.IsNullOrEmpty(contactPersonImageEntity.CustomerCode) ? "" : contactPersonImageEntity.CustomerCode, DbType.String);
                paramCollection.Add("@fileName", contactPersonImageEntity.FileName, DbType.String);
                paramCollection.Add("@contactNo", contactPersonImageEntity.ContactNo, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ContactPersonImageSql["UpdateContactPersonImage"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool isContactPersonImageExist(int contactImageID)
        {
            DbDataReader drCustomerContact = null;
            bool isExist = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@contactImageID", contactImageID, DbType.Int32);
                

                drCustomerContact = this.DataAcessService.ExecuteQuery(ContactPersonImageSql["GetContactPersonImagebyId"], paramCollection);

                if (drCustomerContact.HasRows)
                {
                    isExist = true;
                }
                else
                {
                    isExist = false;
                }

            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCustomerContact != null)
                    drCustomerContact.Close();
            }

            return isExist;
        }
    }
}
