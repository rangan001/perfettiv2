﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;

namespace Peercore.CRM.DataAccess.DAO
{
    public class CountryDAO:BaseDAO
    {
        private DbSqlAdapter CountrySql { get; set; }
      
        public CountryDAO()
        {
            this.CountrySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CountrySql.xml", ApplicationService.Instance.DbProvider);
        }

        public CountryEntity CreateObject()
        {
            return CountryEntity.CreateObject();
        }

        public CountryEntity CreateObject(int entityId)
        {
            return CountryEntity.CreateObject(entityId);
        }

        public List<CountryEntity> GetCountry()
        {
            DbDataReader drCountry = null;

            drCountry = this.DataAcessService.ExecuteQuery(CountrySql["GetCountry"]);


            try
            {
                List<CountryEntity> countryList = new List<CountryEntity>();
                if (drCountry != null && drCountry.HasRows)
                {
                    int countryIdOrdinal = drCountry.GetOrdinal("country_id");
                    int countryOrdinal = drCountry.GetOrdinal("country");

                    while (drCountry.Read())
                    {
                        CountryEntity country = CountryEntity.CreateObject();
                        if (!drCountry.IsDBNull(countryIdOrdinal)) country.CountryId = drCountry.GetInt32(countryIdOrdinal);
                        if (!drCountry.IsDBNull(countryOrdinal)) country.CountryName = drCountry.GetString(countryOrdinal).ToUpper();

                        countryList.Add(country);

                    }
                }
                return countryList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCountry != null)
                    drCountry.Close();
            }
        }
    }
}
