﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data;
using Peercore.DataAccess.Common.Parameters;
using System.Data.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class DivisionDAO:BaseDAO
    {
        private DbSqlAdapter DivisionSql { get; set; }

        private void RegisterSql()
        {
            this.DivisionSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.DivisionSql.xml", ApplicationService.Instance.DbProvider);
        }

        public DivisionDAO()
        {
            RegisterSql();
        }

        public DivisionDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public DivisionEntity CreateObject()
        {
            return DivisionEntity.CreateObject();
        }

        public DivisionEntity CreateObject(int entityId)
        {
            return DivisionEntity.CreateObject(entityId);
        }

        public bool InsertDivision(out int divisionId, DivisionEntity divisionEntity)
        {
            bool successful = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DivisionCode", divisionEntity.DivisionCode, DbType.String);
                paramCollection.Add("@DivisionName", divisionEntity.DivisionName, DbType.String);
                paramCollection.Add("@IsActive", divisionEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@CreatedBy", divisionEntity.CreatedBy, DbType.String);
                paramCollection.Add("@DivisionId", null, DbType.Int32, ParameterDirection.Output);

                divisionId = this.DataAcessService.ExecuteNonQuery(DivisionSql["InsertDivision"], paramCollection, true, "@DivisionId");
                if (divisionId > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

        public bool UpdateDivision(DivisionEntity divisionEntity)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@DivisionId", divisionEntity.DivisionId, DbType.Int32);
                paramCollection.Add("@DivisionCode", divisionEntity.DivisionCode, DbType.String);
                paramCollection.Add("@DivisionName", divisionEntity.DivisionName, DbType.String);
                paramCollection.Add("@IsActive", divisionEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@LastModifiedBy", divisionEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(DivisionSql["UpdateDivision"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public List<DivisionEntity> GetAllDivisions(ArgsEntity args)
        {
            DbDataReader drDivision = null;
            try
            {
                List<DivisionEntity> divisionEntityCollection = new List<DivisionEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drDivision = this.DataAcessService.ExecuteQuery(DivisionSql["GetAllDivisions"], paramCollection);

                if (drDivision != null && drDivision.HasRows)
                {
                    int divisionIdOrdinal = drDivision.GetOrdinal("division_id");
                    int divisionCodeOrdinal = drDivision.GetOrdinal("division_code");
                    int divisionNameOrdinal = drDivision.GetOrdinal("division_name");
                    int isActiveOrdinal = drDivision.GetOrdinal("is_active");
                    int totalCountOrdinal = drDivision.GetOrdinal("total_count");

                    DivisionEntity divisionEntity = null;
                    while (drDivision.Read())
                    {
                        divisionEntity = CreateObject();

                        if (!drDivision.IsDBNull(divisionIdOrdinal)) divisionEntity.DivisionId = drDivision.GetInt32(divisionIdOrdinal);
                        if (!drDivision.IsDBNull(divisionCodeOrdinal)) divisionEntity.DivisionCode = drDivision.GetString(divisionCodeOrdinal);
                        if (!drDivision.IsDBNull(divisionNameOrdinal)) divisionEntity.DivisionName = drDivision.GetString(divisionNameOrdinal);
                        if (!drDivision.IsDBNull(isActiveOrdinal)) divisionEntity.IsActive = drDivision.GetBoolean(isActiveOrdinal);
                        if (!drDivision.IsDBNull(totalCountOrdinal)) divisionEntity.TotalCount = drDivision.GetInt32(totalCountOrdinal);                       

                        divisionEntityCollection.Add(divisionEntity);

                    }
                }

                return divisionEntityCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drDivision != null)
                    drDivision.Close();
            }
        }

        public DivisionEntity GetDivisionById(int divisionId)
        {
            DbDataReader drDivision = null;

            try
            {
                DivisionEntity divisionEntity = CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);

                drDivision = this.DataAcessService.ExecuteQuery(DivisionSql["GetDivisionById"], paramCollection);

                if (drDivision != null && drDivision.HasRows)
                {
                    int divisionIdOrdinal = drDivision.GetOrdinal("division_id");
                    int divisionCodeOrdinal = drDivision.GetOrdinal("division_code");
                    int divisionNameOrdinal = drDivision.GetOrdinal("division_name");
                    int isActiveOrdinal = drDivision.GetOrdinal("is_active");
                    int createdByOrdinal = drDivision.GetOrdinal("created_by");
                    int createdDateOrdinal = drDivision.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drDivision.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drDivision.GetOrdinal("last_modified_date");
                    

                    if (drDivision.Read())
                    {
                        if (!drDivision.IsDBNull(divisionIdOrdinal)) divisionEntity.DivisionId = drDivision.GetInt32(divisionIdOrdinal);
                        if (!drDivision.IsDBNull(divisionCodeOrdinal)) divisionEntity.DivisionCode = drDivision.GetString(divisionCodeOrdinal);
                        if (!drDivision.IsDBNull(divisionNameOrdinal)) divisionEntity.DivisionName = drDivision.GetString(divisionNameOrdinal);
                        if (!drDivision.IsDBNull(isActiveOrdinal)) divisionEntity.IsActive = drDivision.GetBoolean(isActiveOrdinal);
                        if (!drDivision.IsDBNull(createdByOrdinal)) divisionEntity.CreatedBy = drDivision.GetString(createdByOrdinal);
                        if (!drDivision.IsDBNull(createdDateOrdinal)) divisionEntity.CreatedDate = drDivision.GetDateTime(createdDateOrdinal);
                        if (!drDivision.IsDBNull(lastModifiedByOrdinal)) divisionEntity.LastModifiedBy = drDivision.GetString(lastModifiedByOrdinal);
                        if (!drDivision.IsDBNull(lastModifiedDateOrdinal)) divisionEntity.LastModifiedDate = drDivision.GetDateTime(lastModifiedDateOrdinal);  
                    }
                }

                return divisionEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drDivision != null)
                    drDivision.Close();
            }
        }

        public bool DeleteDivision(int divisionId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drDivision = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(DivisionSql["DeleteDivision"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drDivision != null)
                    drDivision.Close();
            }
        }


    }
}
