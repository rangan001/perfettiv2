﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class DocumentDAO:BaseDAO
    {
        private DbSqlAdapter DocumentSql { get; set; }

        //public DocumentDAO()
        //{
        //    this.DocumentSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.DocumentSql.xml");
        //}

        private void RegisterSql()
        {
            this.DocumentSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.DocumentSql.xml", ApplicationService.Instance.DbProvider);
        }

        public DocumentDAO()
        {
            RegisterSql();
        }

        public DocumentDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public DocumentEntity CreateObject()
        {
            return DocumentEntity.CreateObject();
        }

        public DocumentEntity CreateObject(int entityId)
        {
            return DocumentEntity.CreateObject(entityId);
        }

        public DocumentEntity GetDocument(int documentID)
        {
            DbDataReader drDocument = null;
            List<DocumentEntity> documentCollection = new List<DocumentEntity>();

            DocumentEntity document = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@documentID", documentID, DbType.Int32);

                drDocument = this.DataAcessService.ExecuteQuery(DocumentSql["GetContactDocumentById"], paramCollection);

                if (drDocument != null && drDocument.HasRows)
                {
                    int documentIDOrdinal = drDocument.GetOrdinal("document_id");
                    int leadIDOrdinal = drDocument.GetOrdinal("lead_id");
                    int documentNameOrdinal = drDocument.GetOrdinal("document_name");
                    int pathOrdinal = drDocument.GetOrdinal("path");
                    int custCodeOrdinal = drDocument.GetOrdinal("cust_code");
                    int dateTimeOrdinal = drDocument.GetOrdinal("attached_date");
                    int originatorOrdinal = drDocument.GetOrdinal("attached_by");
                    int lastModifiedDateOrdinal = drDocument.GetOrdinal("last_modified_date");
                    int enduserCodeOrdinal = drDocument.GetOrdinal("enduser_code");
                    int documentContentOrdinal = drDocument.GetOrdinal("document_content");

                    if (drDocument.Read())
                    {
                        document = DocumentEntity.CreateObject();
                        if (!drDocument.IsDBNull(documentIDOrdinal)) document.DocumentID = drDocument.GetInt32(documentIDOrdinal);
                        if (!drDocument.IsDBNull(leadIDOrdinal)) document.LeadID = drDocument.GetInt32(leadIDOrdinal);
                        if (!drDocument.IsDBNull(documentNameOrdinal)) document.DocumentName = drDocument.GetString(documentNameOrdinal);
                        if (!drDocument.IsDBNull(pathOrdinal)) document.Path = drDocument.GetString(pathOrdinal);
                        if (!drDocument.IsDBNull(custCodeOrdinal)) document.CustCode = drDocument.GetString(custCodeOrdinal);
                        if (!drDocument.IsDBNull(dateTimeOrdinal)) document.AttachedDate = drDocument.GetDateTime(dateTimeOrdinal).ToLocalTime();
                        if (!drDocument.IsDBNull(originatorOrdinal)) document.AttachedBy = drDocument.GetString(originatorOrdinal);
                        if (!drDocument.IsDBNull(lastModifiedDateOrdinal)) document.LastModifiedDate = drDocument.GetDateTime(lastModifiedDateOrdinal);
                        if (!drDocument.IsDBNull(originatorOrdinal)) document.EnduserCode = drDocument.GetString(enduserCodeOrdinal);

                        if (drDocument[documentContentOrdinal] != null)
                        {
                            if (!drDocument.IsDBNull(documentContentOrdinal)) document.Content = (byte[])drDocument[documentContentOrdinal];
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drDocument != null )
                    drDocument.Close();
            }

            return document;
        }
        
        public bool InsertDocument(DocumentEntity documentEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@documentID", documentEntity.DocumentID, DbType.Int32);
                paramCollection.Add("@leadID", documentEntity.LeadID, DbType.Int32);
                paramCollection.Add("@documentName", documentEntity.DocumentName, DbType.String);
                paramCollection.Add("@path", documentEntity.Path, DbType.String);
                paramCollection.Add("@custCode", string.IsNullOrEmpty(documentEntity.CustCode) ? "" : documentEntity.CustCode, DbType.String);
                paramCollection.Add("@attachedDate", documentEntity.AttachedDate, DbType.DateTime);
                paramCollection.Add("@attachedBy", documentEntity.AttachedBy, DbType.String);
                paramCollection.Add("@lastModifiedBy", documentEntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@lastModifiedDate", documentEntity.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@enduserCode", string.IsNullOrEmpty(documentEntity.EnduserCode) ? "" : documentEntity.EnduserCode, DbType.String);
                paramCollection.Add("@documentContent", documentEntity.Content, DbType.Binary);

                iNoRec = this.DataAcessService.ExecuteNonQuery(DocumentSql["InsertDocument"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateDocument(DocumentEntity documentEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@documentID", documentEntity.DocumentID, DbType.Int32);
                paramCollection.Add("@leadID", documentEntity.LeadID, DbType.Int32);
                paramCollection.Add("@documentName", documentEntity.DocumentName, DbType.String);
                paramCollection.Add("@path", documentEntity.Path, DbType.String);
                paramCollection.Add("@custCode", documentEntity.CustCode, DbType.String);
                paramCollection.Add("@attachedDate", documentEntity.AttachedDate, DbType.DateTime);
                paramCollection.Add("@attachedBy", documentEntity.AttachedBy, DbType.String);
                paramCollection.Add("@lastModifiedBy", documentEntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@lastModifiedDate", documentEntity.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@enduserCode ", documentEntity.EnduserCode, DbType.String);
                paramCollection.Add("@documentContent", documentEntity.Content, DbType.Byte);

                iNoRec = this.DataAcessService.ExecuteNonQuery(DocumentSql["UpdateDocument"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public int GetNextDocumentID()
        {
            try
            {
                return System.Convert.ToInt32(this.DataAcessService.GetOneValue(DocumentSql["GetNextDocumentID"]));   // Get Next ID               
            }
            catch
            {
                throw;
            }
        }

        public List<DocumentEntity> GetContactDocumentsForCustomer(ArgsEntity args)
        {
            DbDataReader drDocument = null;

            try
            {
                List<DocumentEntity> documentCollection = new List<DocumentEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", args.CustomerCode, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drDocument = this.DataAcessService.ExecuteQuery(DocumentSql["GetContactDocumentForCustomer"], paramCollection);

                if (drDocument != null && drDocument.HasRows)
                {
                    int documentIDOrdinal = drDocument.GetOrdinal("document_id");
                    int leadIDOrdinal = drDocument.GetOrdinal("lead_id");
                    int documentNameOrdinal = drDocument.GetOrdinal("document_name");
                    int pathOrdinal = drDocument.GetOrdinal("path");
                    int custCodeOrdinal = drDocument.GetOrdinal("cust_code");
                    int dateTimeOrdinal = drDocument.GetOrdinal("attached_date");
                    int originatorOrdinal = drDocument.GetOrdinal("attached_by");
                    int totcountOrdinal = drDocument.GetOrdinal("totcount");
                    int documentContentOrdinal = drDocument.GetOrdinal("document_content");

                    while (drDocument.Read())
                    {
                        DocumentEntity document = DocumentEntity.CreateObject();
                        if (!drDocument.IsDBNull(documentIDOrdinal)) document.DocumentID = drDocument.GetInt32(documentIDOrdinal);
                        if (!drDocument.IsDBNull(leadIDOrdinal)) document.LeadID = drDocument.GetInt32(leadIDOrdinal);
                        if (!drDocument.IsDBNull(documentNameOrdinal)) document.DocumentName = drDocument.GetString(documentNameOrdinal);
                        if (!drDocument.IsDBNull(pathOrdinal)) document.Path = drDocument.GetString(pathOrdinal);
                        if (!drDocument.IsDBNull(custCodeOrdinal)) document.CustCode = drDocument.GetString(custCodeOrdinal);
                        if (!drDocument.IsDBNull(dateTimeOrdinal)) document.AttachedDate = drDocument.GetDateTime(dateTimeOrdinal).ToLocalTime();
                        if (!drDocument.IsDBNull(originatorOrdinal)) document.AttachedBy = drDocument.GetString(originatorOrdinal);
                        if (!drDocument.IsDBNull(totcountOrdinal)) document.RowCount = drDocument.GetInt32(totcountOrdinal);
                        if (drDocument[documentContentOrdinal] != null)
                        {
                            if (!drDocument.IsDBNull(documentContentOrdinal)) document.Content = (byte[])drDocument[documentContentOrdinal];
                        }

                       documentCollection.Add(document);
                    }
                }
               
                return documentCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drDocument != null)
                    drDocument.Close();
            }
        }

        public List<DocumentEntity> GetDocumentDetails(ArgsEntity args)
        {
            DbDataReader drDocument = null;
            List<DocumentEntity> documentCollection = new List<DocumentEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@leadID", args.LeadId, DbType.Int32);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drDocument = this.DataAcessService.ExecuteQuery(DocumentSql["GetContactDocument"], paramCollection);

                if (drDocument != null && drDocument.HasRows)
                {
                    int documentIDOrdinal = drDocument.GetOrdinal("document_id");
                    int leadIDOrdinal = drDocument.GetOrdinal("lead_id");
                    int documentNameOrdinal = drDocument.GetOrdinal("document_name");
                    int pathOrdinal = drDocument.GetOrdinal("path");
                    int custCodeOrdinal = drDocument.GetOrdinal("cust_code");
                    int dateTimeOrdinal = drDocument.GetOrdinal("attached_date");
                    int originatorOrdinal = drDocument.GetOrdinal("attached_by");
                    int lastModifiedDateOrdinal = drDocument.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drDocument.GetOrdinal("total_count");
                    int documentContentOrdinal = drDocument.GetOrdinal("document_content");

                    while (drDocument.Read())
                    {
                        DocumentEntity document = DocumentEntity.CreateObject();
                        if (!drDocument.IsDBNull(documentIDOrdinal)) document.DocumentID = drDocument.GetInt32(documentIDOrdinal);
                        if (!drDocument.IsDBNull(leadIDOrdinal)) document.LeadID = drDocument.GetInt32(leadIDOrdinal);
                        if (!drDocument.IsDBNull(documentNameOrdinal)) document.DocumentName = drDocument.GetString(documentNameOrdinal);
                        if (!drDocument.IsDBNull(pathOrdinal)) document.Path = drDocument.GetString(pathOrdinal);
                        if (!drDocument.IsDBNull(custCodeOrdinal)) document.CustCode = drDocument.GetString(custCodeOrdinal);
                        if (!drDocument.IsDBNull(dateTimeOrdinal)) document.AttachedDate = drDocument.GetDateTime(dateTimeOrdinal).ToLocalTime();
                        if (!drDocument.IsDBNull(originatorOrdinal)) document.AttachedBy = drDocument.GetString(originatorOrdinal);
                        if (!drDocument.IsDBNull(lastModifiedDateOrdinal)) document.LastModifiedDate = drDocument.GetDateTime(lastModifiedDateOrdinal);
                        if (!drDocument.IsDBNull(totalCountOrdinal)) document.RowCount = drDocument.GetInt32(totalCountOrdinal);
                        if (drDocument[documentContentOrdinal] != null)
                        {
                            if (!drDocument.IsDBNull(documentContentOrdinal)) document.Content = (byte[])drDocument[documentContentOrdinal];
                        }

                        documentCollection.Add(document);
                    }
                }

                

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drDocument != null)
                    drDocument.Close();
            }

            return documentCollection;
        }
     
        public bool DeleteDocument(int documentID)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@documentID", documentID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(DocumentSql["DeleteDocument"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<DocumentEntity> GetContactDocumentsForEndUser(ArgsEntity args)
        {
            DbDataReader drDocument = null;

            try
            {
                List<DocumentEntity> documentCollection = new List<DocumentEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", args.CustomerCode, DbType.String);
                paramCollection.Add("@EnduserCode", args.EnduserCode, DbType.String);

                drDocument = this.DataAcessService.ExecuteQuery(DocumentSql["GetContactDocumentForEndUser"], paramCollection);

                if (drDocument != null && drDocument.HasRows)
                {
                    int documentIDOrdinal = drDocument.GetOrdinal("document_id");
                    int leadIDOrdinal = drDocument.GetOrdinal("lead_id"); 
                    int documentNameOrdinal = drDocument.GetOrdinal("document_name");
                    int pathOrdinal = drDocument.GetOrdinal("path");
                    int custCodeOrdinal = drDocument.GetOrdinal("cust_code");
                    int dateTimeOrdinal = drDocument.GetOrdinal("attached_date");
                    int originatorOrdinal = drDocument.GetOrdinal("attached_by");
                    int enduserCodeOrdinal = drDocument.GetOrdinal("enduser_code");
                    int documentContentOrdinal = drDocument.GetOrdinal("document_content");

                    while (drDocument.Read())
                    {
                        DocumentEntity document = DocumentEntity.CreateObject();
                        if (!drDocument.IsDBNull(documentIDOrdinal)) document.DocumentID = drDocument.GetInt32(documentIDOrdinal);
                        if (!drDocument.IsDBNull(leadIDOrdinal)) document.LeadID = drDocument.GetInt32(leadIDOrdinal);
                        if (!drDocument.IsDBNull(documentNameOrdinal)) document.DocumentName = drDocument.GetString(documentNameOrdinal);
                        if (!drDocument.IsDBNull(pathOrdinal)) document.Path = drDocument.GetString(pathOrdinal);
                        if (!drDocument.IsDBNull(custCodeOrdinal)) document.CustCode = drDocument.GetString(custCodeOrdinal);
                        if (!drDocument.IsDBNull(dateTimeOrdinal)) document.AttachedDate = drDocument.GetDateTime(dateTimeOrdinal).ToLocalTime();
                        if (!drDocument.IsDBNull(originatorOrdinal)) document.AttachedBy = drDocument.GetString(originatorOrdinal);
                        if (!drDocument.IsDBNull(originatorOrdinal)) document.EnduserCode = drDocument.GetString(enduserCodeOrdinal);
                        document.CustomerType = "enduser";

                        if (drDocument[documentContentOrdinal] != null)
                        {
                            if (!drDocument.IsDBNull(documentContentOrdinal)) document.Content = (byte[])drDocument[documentContentOrdinal];
                        }

                        documentCollection.Add(document);

                    }
                }
               
                return documentCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drDocument != null )
                    drDocument.Close();
            }
        }

        public bool GetAdminPassword(string password)
        {
            bool successful = false;
            DbDataReader drDocument = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Password", password, DbType.String);

                drDocument = this.DataAcessService.ExecuteQuery(DocumentSql["GetAdminPassword"], paramCollection);

                if (drDocument != null && drDocument.HasRows)
                {
                    successful = true;
                }

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drDocument != null)
                    drDocument.Close();
            }
        }
    }
}
