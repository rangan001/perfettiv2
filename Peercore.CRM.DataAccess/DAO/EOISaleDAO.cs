﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class EOISaleDAO:BaseDAO
    {
        private DbSqlAdapter ActivitySql { get; set; }

        private void RegisterSql()
        {
            this.ActivitySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SalesSql.xml", ApplicationService.Instance.DbProvider);
        }

        public EOISaleDAO()
        {
            RegisterSql();
        }

        public EOISaleDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public EOISalesEntity CreateObject()
        {
            return EOISalesEntity.CreateObject();
        }

        public EOISalesEntity CreateObject(int entityId)
        {
            return EOISalesEntity.CreateObject(entityId);
        }

        public List<EOISalesEntity> GetEOISales(ArgsEntity args)
        {
            string thisYearHeader = " "+(DateTime.Today.Year - 1).ToString() + "/" + DateTime.Today.Year.ToString().Substring(2);
            string lastYearHeader = (DateTime.Today.Year - 2).ToString() + "/" + (DateTime.Today.Year - 1).ToString().Substring(2);
            DbDataReader drEndUser = null;

            EOISalesEntity EOISalesTonnes;
            EOISalesEntity EOISalesCases;

            EOISalesEntity EOISalesTonnesLY;
            EOISalesEntity EOISalesCasesLY;

            List<EOISalesEntity> EOISalesList = new List<EOISalesEntity>();
            try
            {
                

                List<EOISalesEntity> listEOISalesThisYear = new List<EOISalesEntity>();
                List<EOISalesEntity> listEOISalesLastYear = new List<EOISalesEntity>();
                List<EOISalesEntity> listTonnes = new List<EOISalesEntity>();
                List<EOISalesEntity> listTonnesLY = new List<EOISalesEntity>();
                EOISalesEntity growthRate = CreateObject();


                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RepCode", args.RepCode, DbType.String);
                paramCollection.Add("@CustCode", args.CustomerCode, DbType.String);
                paramCollection.Add("@RepType", args.RepType, DbType.String);
                //paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drEndUser = this.DataAcessService.ExecuteQuery(ActivitySql["GetEOISales"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    int marketOrdinal = drEndUser.GetOrdinal("market");

                    int cases1Ordinal = drEndUser.GetOrdinal("Cases1");
                    int cases2Ordinal = drEndUser.GetOrdinal("Cases2");
                    int cases3Ordinal = drEndUser.GetOrdinal("Cases3");
                    int cases4Ordinal = drEndUser.GetOrdinal("Cases4");
                    int cases5Ordinal = drEndUser.GetOrdinal("Cases5");
                    int cases6Ordinal = drEndUser.GetOrdinal("Cases6");
                    int cases7Ordinal = drEndUser.GetOrdinal("Cases7");
                    int cases8Ordinal = drEndUser.GetOrdinal("Cases8");
                    int cases9Ordinal = drEndUser.GetOrdinal("Cases9");
                    int cases10Ordinal = drEndUser.GetOrdinal("Cases10");
                    int cases11Ordinal = drEndUser.GetOrdinal("Cases11");
                    int cases12Ordinal = drEndUser.GetOrdinal("Cases12");

                    int tonnes1Ordinal = drEndUser.GetOrdinal("Tonnes1");
                    int tonnes2Ordinal = drEndUser.GetOrdinal("Tonnes2");
                    int tonnes3Ordinal = drEndUser.GetOrdinal("Tonnes3");
                    int tonnes4Ordinal = drEndUser.GetOrdinal("Tonnes4");
                    int tonnes5Ordinal = drEndUser.GetOrdinal("Tonnes5");
                    int tonnes6Ordinal = drEndUser.GetOrdinal("Tonnes6");
                    int tonnes7Ordinal = drEndUser.GetOrdinal("Tonnes7");
                    int tonnes8Ordinal = drEndUser.GetOrdinal("Tonnes8");
                    int tonnes9Ordinal = drEndUser.GetOrdinal("Tonnes9");
                    int tonnes10Ordinal = drEndUser.GetOrdinal("Tonnes10");
                    int tonnes11Ordinal = drEndUser.GetOrdinal("Tonnes11");
                    int tonnes12Ordinal = drEndUser.GetOrdinal("Tonnes12");

                    // Last Year
                    int casesPP1Ordinal = drEndUser.GetOrdinal("CasesPP1");
                    int casesPP2Ordinal = drEndUser.GetOrdinal("CasesPP2");
                    int casesPP3Ordinal = drEndUser.GetOrdinal("CasesPP3");
                    int casesPP4Ordinal = drEndUser.GetOrdinal("CasesPP4");
                    int casesPP5Ordinal = drEndUser.GetOrdinal("CasesPP5");
                    int casesPP6Ordinal = drEndUser.GetOrdinal("CasesPP6");
                    int casesPP7Ordinal = drEndUser.GetOrdinal("CasesPP7");
                    int casesPP8Ordinal = drEndUser.GetOrdinal("CasesPP8");
                    int casesPP9Ordinal = drEndUser.GetOrdinal("CasesPP9");
                    int casesPP10Ordinal = drEndUser.GetOrdinal("CasesPP10");
                    int casesPP11Ordinal = drEndUser.GetOrdinal("CasesPP11");
                    int casesPP12Ordinal = drEndUser.GetOrdinal("CasesPP12");

                    int tonnesPP1Ordinal = drEndUser.GetOrdinal("TonnesPP1");
                    int tonnesPP2Ordinal = drEndUser.GetOrdinal("TonnesPP2");
                    int tonnesPP3Ordinal = drEndUser.GetOrdinal("TonnesPP3");
                    int tonnesPP4Ordinal = drEndUser.GetOrdinal("TonnesPP4");
                    int tonnesPP5Ordinal = drEndUser.GetOrdinal("TonnesPP5");
                    int tonnesPP6Ordinal = drEndUser.GetOrdinal("TonnesPP6");
                    int tonnesPP7Ordinal = drEndUser.GetOrdinal("TonnesPP7");
                    int tonnesPP8Ordinal = drEndUser.GetOrdinal("TonnesPP8");
                    int tonnesPP9Ordinal = drEndUser.GetOrdinal("TonnesPP9");
                    int tonnesPP10Ordinal = drEndUser.GetOrdinal("TonnesPP10");
                    int tonnesPP11Ordinal = drEndUser.GetOrdinal("TonnesPP11");
                    int tonnesPP12Ordinal = drEndUser.GetOrdinal("TonnesPP12");

                    while (drEndUser.Read())
                    {
                        EOISalesTonnes = CreateObject();
                        EOISalesCases = CreateObject();

                        EOISalesTonnesLY = CreateObject();
                        EOISalesCasesLY = CreateObject();

                        // This Year
                        if (!drEndUser.IsDBNull(marketOrdinal)) EOISalesTonnes.Market = drEndUser.GetString(marketOrdinal);
                        EOISalesCases.Market = EOISalesTonnes.Market;

                        EOISalesCases.CostYear = thisYearHeader;
                        EOISalesTonnes.CostYear = thisYearHeader;

                        EOISalesCases.UOM = "Cases";
                        if (EOISalesCases.Market == "BI")
                            EOISalesCases.Description = "VOLUME - Food Services - Branded (cases)";
                        else if (EOISalesCases.Market == "MR")
                            EOISalesCases.Description = "VOLUME - Catering Margarine (cases)";
                        else if (EOISalesCases.Market == "BZ")
                            EOISalesCases.Description = "VOLUME - Baking fats (cases)";
                        else if (EOISalesCases.Market == "MZ")
                            EOISalesCases.Description = "VOLUME - OBM (cases)";
                        
                        if (!drEndUser.IsDBNull(cases1Ordinal)) EOISalesCases.Cases1 = Math.Round(drEndUser.GetDouble(cases1Ordinal));
                        if (!drEndUser.IsDBNull(cases2Ordinal)) EOISalesCases.Cases2 = Math.Round(drEndUser.GetDouble(cases2Ordinal));
                        if (!drEndUser.IsDBNull(cases3Ordinal)) EOISalesCases.Cases3 = Math.Round(drEndUser.GetDouble(cases3Ordinal));
                        if (!drEndUser.IsDBNull(cases4Ordinal)) EOISalesCases.Cases4 = Math.Round(drEndUser.GetDouble(cases4Ordinal));
                        if (!drEndUser.IsDBNull(cases5Ordinal)) EOISalesCases.Cases5 = Math.Round(drEndUser.GetDouble(cases5Ordinal));
                        if (!drEndUser.IsDBNull(cases6Ordinal)) EOISalesCases.Cases6 = Math.Round(drEndUser.GetDouble(cases6Ordinal));
                        if (!drEndUser.IsDBNull(cases7Ordinal)) EOISalesCases.Cases7 = Math.Round(drEndUser.GetDouble(cases7Ordinal));
                        if (!drEndUser.IsDBNull(cases8Ordinal)) EOISalesCases.Cases8 = Math.Round(drEndUser.GetDouble(cases8Ordinal));
                        if (!drEndUser.IsDBNull(cases9Ordinal)) EOISalesCases.Cases9 = Math.Round(drEndUser.GetDouble(cases9Ordinal));
                        if (!drEndUser.IsDBNull(cases10Ordinal)) EOISalesCases.Cases10 = Math.Round(drEndUser.GetDouble(cases10Ordinal));
                        if (!drEndUser.IsDBNull(cases11Ordinal)) EOISalesCases.Cases11 = Math.Round(drEndUser.GetDouble(cases11Ordinal));
                        if (!drEndUser.IsDBNull(cases12Ordinal)) EOISalesCases.Cases12 = Math.Round(drEndUser.GetDouble(cases12Ordinal));

                        EOISalesTonnes.UOM = "Tonnes";
                        if (EOISalesCases.Market == "BI")
                            EOISalesTonnes.Description = "Food Services - Branded (Tonnes)";
                        else if (EOISalesCases.Market == "MR")
                            EOISalesTonnes.Description = "Catering Margarine (Tonnes)";
                        else if (EOISalesCases.Market == "BZ")
                            EOISalesTonnes.Description = "Baking Fats (Tonnes)";
                        else if (EOISalesCases.Market == "MZ")
                            EOISalesTonnes.Description = "OBM (Tonnes)";

                        if (!drEndUser.IsDBNull(tonnes1Ordinal)) EOISalesTonnes.Cases1 = Math.Round(drEndUser.GetDouble(tonnes1Ordinal));
                        if (!drEndUser.IsDBNull(tonnes2Ordinal)) EOISalesTonnes.Cases2 = Math.Round(drEndUser.GetDouble(tonnes2Ordinal));
                        if (!drEndUser.IsDBNull(tonnes3Ordinal)) EOISalesTonnes.Cases3 = Math.Round(drEndUser.GetDouble(tonnes3Ordinal));
                        if (!drEndUser.IsDBNull(tonnes4Ordinal)) EOISalesTonnes.Cases4 = Math.Round(drEndUser.GetDouble(tonnes4Ordinal));
                        if (!drEndUser.IsDBNull(tonnes5Ordinal)) EOISalesTonnes.Cases5 = Math.Round(drEndUser.GetDouble(tonnes5Ordinal));
                        if (!drEndUser.IsDBNull(tonnes6Ordinal)) EOISalesTonnes.Cases6 = Math.Round(drEndUser.GetDouble(tonnes6Ordinal));
                        if (!drEndUser.IsDBNull(tonnes7Ordinal)) EOISalesTonnes.Cases7 = Math.Round(drEndUser.GetDouble(tonnes7Ordinal));
                        if (!drEndUser.IsDBNull(tonnes8Ordinal)) EOISalesTonnes.Cases8 = Math.Round(drEndUser.GetDouble(tonnes8Ordinal));
                        if (!drEndUser.IsDBNull(tonnes9Ordinal)) EOISalesTonnes.Cases9 = Math.Round(drEndUser.GetDouble(tonnes9Ordinal));
                        if (!drEndUser.IsDBNull(tonnes10Ordinal)) EOISalesTonnes.Cases10 = Math.Round(drEndUser.GetDouble(tonnes10Ordinal));
                        if (!drEndUser.IsDBNull(tonnes11Ordinal)) EOISalesTonnes.Cases11 = Math.Round(drEndUser.GetDouble(tonnes11Ordinal));
                        if (!drEndUser.IsDBNull(tonnes12Ordinal)) EOISalesTonnes.Cases12 = Math.Round(drEndUser.GetDouble(tonnes12Ordinal));

                        // Last Year
                        EOISalesCasesLY.Market = EOISalesTonnes.Market;
                        EOISalesTonnesLY.Market = EOISalesTonnes.Market;

                        EOISalesCasesLY.CostYear = lastYearHeader;
                        EOISalesTonnesLY.CostYear = lastYearHeader;

                        EOISalesCasesLY.UOM = "Cases";

                        if (EOISalesCases.Market == "BI")
                            EOISalesCasesLY.Description = "VOLUME - Food Services - Branded (cases)";
                        else if (EOISalesCases.Market == "MR")
                            EOISalesCasesLY.Description = "VOLUME - Catering Margarine (cases)";
                        else if (EOISalesCases.Market == "BZ")
                            EOISalesCasesLY.Description = "VOLUME - Baking fats (cases)";
                        else if (EOISalesCases.Market == "MZ")
                            EOISalesCasesLY.Description = "VOLUME - OBM (cases)";

                        if (!drEndUser.IsDBNull(casesPP1Ordinal)) EOISalesCasesLY.Cases1 = Math.Round(drEndUser.GetDouble(casesPP1Ordinal));
                        if (!drEndUser.IsDBNull(casesPP2Ordinal)) EOISalesCasesLY.Cases2 = Math.Round(drEndUser.GetDouble(casesPP2Ordinal));
                        if (!drEndUser.IsDBNull(casesPP3Ordinal)) EOISalesCasesLY.Cases3 = Math.Round(drEndUser.GetDouble(casesPP3Ordinal));
                        if (!drEndUser.IsDBNull(casesPP4Ordinal)) EOISalesCasesLY.Cases4 = Math.Round(drEndUser.GetDouble(casesPP4Ordinal));
                        if (!drEndUser.IsDBNull(casesPP5Ordinal)) EOISalesCasesLY.Cases5 = Math.Round(drEndUser.GetDouble(casesPP5Ordinal));
                        if (!drEndUser.IsDBNull(casesPP6Ordinal)) EOISalesCasesLY.Cases6 = Math.Round(drEndUser.GetDouble(casesPP6Ordinal));
                        if (!drEndUser.IsDBNull(casesPP7Ordinal)) EOISalesCasesLY.Cases7 = Math.Round(drEndUser.GetDouble(casesPP7Ordinal));
                        if (!drEndUser.IsDBNull(casesPP8Ordinal)) EOISalesCasesLY.Cases8 = Math.Round(drEndUser.GetDouble(casesPP8Ordinal));
                        if (!drEndUser.IsDBNull(casesPP9Ordinal)) EOISalesCasesLY.Cases9 = Math.Round(drEndUser.GetDouble(casesPP9Ordinal));
                        if (!drEndUser.IsDBNull(casesPP10Ordinal)) EOISalesCasesLY.Cases10 = Math.Round(drEndUser.GetDouble(casesPP10Ordinal));
                        if (!drEndUser.IsDBNull(casesPP11Ordinal)) EOISalesCasesLY.Cases11 = Math.Round(drEndUser.GetDouble(casesPP11Ordinal));
                        if (!drEndUser.IsDBNull(casesPP12Ordinal)) EOISalesCasesLY.Cases12 = Math.Round(drEndUser.GetDouble(casesPP12Ordinal));

                        EOISalesTonnesLY.UOM = "Tonnes";
                        if (EOISalesCases.Market == "BI")
                            EOISalesTonnesLY.Description = "Food Services - Branded (Tonnes)";
                        else if (EOISalesCases.Market == "MR")
                            EOISalesTonnesLY.Description = "Catering Margarine (Tonnes)";
                        else if (EOISalesCases.Market == "BZ")
                            EOISalesTonnesLY.Description = "Baking Fats (Tonnes)";
                        else if (EOISalesCases.Market == "MZ")
                            EOISalesTonnesLY.Description = "OBM (Tonnes)";

                        if (!drEndUser.IsDBNull(tonnesPP1Ordinal)) EOISalesTonnesLY.Cases1 = Math.Round(drEndUser.GetDouble(tonnesPP1Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP2Ordinal)) EOISalesTonnesLY.Cases2 = Math.Round(drEndUser.GetDouble(tonnesPP2Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP3Ordinal)) EOISalesTonnesLY.Cases3 = Math.Round(drEndUser.GetDouble(tonnesPP3Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP4Ordinal)) EOISalesTonnesLY.Cases4 = Math.Round(drEndUser.GetDouble(tonnesPP4Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP5Ordinal)) EOISalesTonnesLY.Cases5 = Math.Round(drEndUser.GetDouble(tonnesPP5Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP6Ordinal)) EOISalesTonnesLY.Cases6 = Math.Round(drEndUser.GetDouble(tonnesPP6Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP7Ordinal)) EOISalesTonnesLY.Cases7 = Math.Round(drEndUser.GetDouble(tonnesPP7Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP8Ordinal)) EOISalesTonnesLY.Cases8 = Math.Round(drEndUser.GetDouble(tonnesPP8Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP9Ordinal)) EOISalesTonnesLY.Cases9 = Math.Round(drEndUser.GetDouble(tonnesPP9Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP10Ordinal)) EOISalesTonnesLY.Cases10 = Math.Round(drEndUser.GetDouble(tonnesPP10Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP11Ordinal)) EOISalesTonnesLY.Cases11 = Math.Round(drEndUser.GetDouble(tonnesPP11Ordinal));
                        if (!drEndUser.IsDBNull(tonnesPP12Ordinal)) EOISalesTonnesLY.Cases12 = Math.Round(drEndUser.GetDouble(tonnesPP12Ordinal));

                       
                        listEOISalesThisYear.Add(EOISalesCases);
                        listTonnes.Add(EOISalesTonnes);                        
                        listEOISalesLastYear.Add(EOISalesCasesLY);
                        listTonnesLY.Add(EOISalesTonnesLY);                        

                    }
                }



                listEOISalesThisYear.Add(CalculateTotal(listEOISalesThisYear, thisYearHeader));
                listTonnes.Add(CalculateTotal(listTonnes, thisYearHeader));
                listEOISalesLastYear.Add(CalculateTotal(listEOISalesLastYear, lastYearHeader));
                listTonnesLY.Add(CalculateTotal(listTonnesLY, lastYearHeader));

                growthRate = CalculateGrowthRate(listEOISalesThisYear[2], listEOISalesLastYear[2]);

                //listEOISalesThisYear.Add(CreateObject());
                //listEOISalesLastYear.Add(CreateObject());

                //listEOISalesThisYear.AddRange(listTonnes);
                //listEOISalesLastYear.AddRange(listTonnesLY);

                EOISalesCases = CreateObject();

                EOISalesList.Add(growthRate);

                EOISalesList.AddRange(listEOISalesThisYear);
                EOISalesCases.CostYear = thisYearHeader;
                EOISalesList.Add(EOISalesCases);
                EOISalesList.AddRange(listTonnes);

                EOISalesList.AddRange(listEOISalesLastYear);
                EOISalesCases = CreateObject();
                EOISalesCases.CostYear = lastYearHeader;
                EOISalesList.Add(EOISalesCases);
                EOISalesList.AddRange(listTonnesLY);

                
            }
            catch
            {
                //throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
            return EOISalesList;
        }

        private EOISalesEntity CalculateTotal(List<EOISalesEntity> eoiSalesList, string costYear)
        {
            EOISalesEntity eoiSales = CreateObject();
            try
            {
                EOISalesEntity eoiSalesTy = eoiSalesList[0];
                EOISalesEntity eoiSalesLy = eoiSalesList[1];

                if (eoiSalesTy.UOM == "Cases")
                    eoiSales.Description = "TOTAL (cases)";
                else
                    eoiSales.Description = "TOTAL (Tonnes)";
                eoiSales.CostYear = costYear;

                eoiSales.Cases1 = eoiSalesTy.Cases1 + eoiSalesLy.Cases1;
                eoiSales.Cases2 = eoiSalesTy.Cases2 + eoiSalesLy.Cases2;
                eoiSales.Cases3 = eoiSalesTy.Cases3 + eoiSalesLy.Cases3;
                eoiSales.Cases4 = eoiSalesTy.Cases4 + eoiSalesLy.Cases4;
                eoiSales.Cases5 = eoiSalesTy.Cases5 + eoiSalesLy.Cases5;
                eoiSales.Cases6 = eoiSalesTy.Cases6 + eoiSalesLy.Cases6;
                eoiSales.Cases7 = eoiSalesTy.Cases7 + eoiSalesLy.Cases7;
                eoiSales.Cases8 = eoiSalesTy.Cases8 + eoiSalesLy.Cases8;
                eoiSales.Cases9 = eoiSalesTy.Cases9 + eoiSalesLy.Cases9;
                eoiSales.Cases10 = eoiSalesTy.Cases10 + eoiSalesLy.Cases10;
                eoiSales.Cases11 = eoiSalesTy.Cases11 + eoiSalesLy.Cases11;
                eoiSales.Cases12 = eoiSalesTy.Cases12 + eoiSalesLy.Cases12;
            }
            catch (Exception ex)
            {
            }
            return eoiSales;
        }

        private EOISalesEntity CalculateGrowthRate(EOISalesEntity eoiSalesTy, EOISalesEntity eoiSalesLy)
        {
            EOISalesEntity eoiSales = CreateObject();
            try
            {
                eoiSales.Description = "GROWTH RATE %";
                eoiSales.CostYear = "";

                eoiSales.Cases1 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases1 / eoiSalesLy.Cases1) * 100)) - 100;
                eoiSales.Cases2 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases2 / eoiSalesLy.Cases2) * 100)) - 100;
                eoiSales.Cases3 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases3 / eoiSalesLy.Cases3) * 100)) - 100;
                eoiSales.Quarter1Rate = Math.Round(Convert.ToDouble((eoiSalesTy.Quarter1 / eoiSalesLy.Quarter1) * 100)) - 100;

                eoiSales.Cases4 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases4 / eoiSalesLy.Cases4) * 100)) - 100;
                eoiSales.Cases5 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases5 / eoiSalesLy.Cases5) * 100)) - 100;
                eoiSales.Cases6 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases6 / eoiSalesLy.Cases6) * 100)) - 100;
                eoiSales.Quarter2Rate = Math.Round(Convert.ToDouble((eoiSalesTy.Quarter2 / eoiSalesLy.Quarter2) * 100)) - 100;
                eoiSales.MidYearRate = Math.Round(Convert.ToDouble((eoiSalesTy.MidYearSales / eoiSalesLy.MidYearSales) * 100)) - 100;

                eoiSales.Cases7 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases7 / eoiSalesLy.Cases7) * 100)) - 100;
                eoiSales.Cases8 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases8 / eoiSalesLy.Cases8) * 100)) - 100;
                eoiSales.Cases9 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases9 / eoiSalesLy.Cases9) * 100)) - 100;
                eoiSales.Quarter3Rate = Math.Round(Convert.ToDouble((eoiSalesTy.Quarter3 / eoiSalesLy.Quarter3) * 100)) - 100;
                eoiSales.ThirdQuarterRate = Math.Round(Convert.ToDouble((eoiSalesTy.ThirdQtrSales / eoiSalesLy.ThirdQtrSales) * 100)) - 100;

                eoiSales.Cases10 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases10 / eoiSalesLy.Cases10) * 100)) - 100;
                eoiSales.Cases11 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases11 / eoiSalesLy.Cases11) * 100)) - 100;
                eoiSales.Cases12 = Math.Round(Convert.ToDouble((eoiSalesTy.Cases12 / eoiSalesLy.Cases12) * 100)) - 100;
                eoiSales.Quarter4Rate = Math.Round(Convert.ToDouble((eoiSalesTy.Quarter4 / eoiSalesLy.Quarter4)) * 100) - 100;
                eoiSales.EndOfYearRate = Math.Round(Convert.ToDouble((eoiSalesTy.EndOfYearSales / eoiSalesLy.EndOfYearSales) * 100)) - 100;
            }
            catch (Exception ex)
            {
            }
            return eoiSales;
        }
    }
}
