﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class EmailAttachmentDAO : BaseDAO
    {
        private DbSqlAdapter EmailAttachmentSql { get; set; }

        private void RegisterSql()
        {
            this.EmailAttachmentSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.EmailAttachmentSql.xml", ApplicationService.Instance.DbProvider);
        }

        public EmailAttachmentDAO()
        {
            RegisterSql();
        }

        public EmailAttachmentDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public EmailAttachmentEntity CreateObject()
        {
            return EmailAttachmentEntity.CreateObject();
        }

        public EmailAttachmentEntity CreateObject(int entityId)
        {
            return EmailAttachmentEntity.CreateObject(entityId);
        }

        public List<EmailAttachmentEntity> GetEmailAttachment(int emailId)
        {
            DbDataReader drEmailAttachment = null;
            List<EmailAttachmentEntity> emailAttachmentCollection = new List<EmailAttachmentEntity>();

            try
            {
                EmailAttachmentEntity emailAttachment = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@MailId", emailId, DbType.Int32);

                drEmailAttachment = this.DataAcessService.ExecuteQuery(EmailAttachmentSql["GetEmailAttachmentByEmailId"], paramCollection);
                if (drEmailAttachment != null && drEmailAttachment.HasRows)
                {
                    int attachmentIDOrdinal = drEmailAttachment.GetOrdinal("attachment_id");
                    int emailIDOrdinal = drEmailAttachment.GetOrdinal("mail_id");
                    int documentNameOrdinal = drEmailAttachment.GetOrdinal("document_name");
                    int pathOrdinal = drEmailAttachment.GetOrdinal("path");

                    while (drEmailAttachment.Read())
                    {
                        emailAttachment = EmailAttachmentEntity.CreateObject();
                        if (!drEmailAttachment.IsDBNull(attachmentIDOrdinal)) emailAttachment.AttachmentID = drEmailAttachment.GetInt32(attachmentIDOrdinal);
                        if (!drEmailAttachment.IsDBNull(emailIDOrdinal)) emailAttachment.EmailId = drEmailAttachment.GetInt32(emailIDOrdinal);
                        if (!drEmailAttachment.IsDBNull(documentNameOrdinal)) emailAttachment.FileName = drEmailAttachment.GetString(documentNameOrdinal);
                        if (!drEmailAttachment.IsDBNull(pathOrdinal)) emailAttachment.Path = drEmailAttachment.GetString(pathOrdinal);

                        emailAttachmentCollection.Add(emailAttachment);
                    }
                }

                if (drEmailAttachment != null)
                    drEmailAttachment.Close();

                if (drEmailAttachment != null)
                {
                    foreach (EmailAttachmentEntity item in emailAttachmentCollection)
                    {
                        paramCollection = new DbInputParameterCollection();
                        paramCollection.Add("@AttachmentId", emailId, DbType.Int32);
                        item.FileContent = (byte[])this.DataAcessService.GetOneValue(EmailAttachmentSql["GetEmailAttachmentContent"], paramCollection);
                        item.IsDirty = false;
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drEmailAttachment != null && (!drEmailAttachment.IsClosed))
                    drEmailAttachment.Close();
            }

            return emailAttachmentCollection;
        }
    }
}
