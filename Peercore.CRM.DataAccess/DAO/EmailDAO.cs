﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
//using Peercore.CRM.Entities.CompositeEntities;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class EmailDAO : BaseDAO
    {
        public List<KPIReports> GetAllKPIReports()
        {
            KPIReports objModel = null;
            DataTable objDS = new DataTable();
            List<KPIReports> objList = new List<KPIReports>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllKPIReports";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new KPIReports();
                            if (item["report_id"] != DBNull.Value) objModel.ReportId = Convert.ToInt32(item["report_id"]);
                            if (item["report_name"] != DBNull.Value) objModel.ReportName = item["report_name"].ToString();
                            if (item["email_ids"] != DBNull.Value) objModel.EmailIds = item["email_ids"].ToString();
                            if (item["email_template"] != DBNull.Value) objModel.EmailTemplate = item["email_template"].ToString();
                            if (item["status"] != DBNull.Value) objModel.Status = item["status"].ToString();
                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }


        public bool UpdateKPIReport(KPIReports kpiReport)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateKPIReport";

                    objCommand.Parameters.AddWithValue("@ReportId", kpiReport.ReportId);
                    objCommand.Parameters.AddWithValue("@EmailIds", kpiReport.EmailIds);
                    objCommand.Parameters.AddWithValue("@EmailTemplate", kpiReport.EmailTemplate);
                    objCommand.Parameters.AddWithValue("@Status", kpiReport.Status);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }
        
        public List<KPIReportUser> GetAllKPIReportUsersByReportId(int ReportId)
        {
            KPIReportUser objModel = null;
            DataTable objDS = new DataTable();
            List<KPIReportUser> objList = new List<KPIReportUser>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllKPIReportUsersByReportId";

                    objCommand.Parameters.AddWithValue("@ReportId", ReportId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new KPIReportUser();
                            if (item["report_id"] != DBNull.Value) objModel.ReportId = Convert.ToInt32(item["report_id"]);
                            if (item["originator"] != DBNull.Value) objModel.Originator = item["originator"].ToString();
                            if (item["name"] != DBNull.Value) objModel.Name = item["name"].ToString();
                            if (item["originator_type"] != DBNull.Value) objModel.OriginatorType = item["originator_type"].ToString();
                            if (item["email"] != DBNull.Value) objModel.Email = item["email"].ToString();
                            if (item["status"] != DBNull.Value) objModel.Status = item["status"].ToString();
                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public bool UpdateKPIReportUser(KPIReportUser kpiReportUser)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateKPIReport";

                    objCommand.Parameters.AddWithValue("@ReportId", kpiReportUser.ReportId);
                    objCommand.Parameters.AddWithValue("@Originator", kpiReportUser.Originator);
                    objCommand.Parameters.AddWithValue("@Status", kpiReportUser.Status);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }
    }
}
