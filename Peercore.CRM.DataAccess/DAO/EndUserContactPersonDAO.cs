﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class EndUserContactPersonDAO : BaseDAO
    {
        private DbSqlAdapter EndUserContactPersonSql { get; set; }

        private void RegisterSql()
        {
            this.EndUserContactPersonSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.EndUserContactPersonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public EndUserContactPersonDAO()
        {
            RegisterSql();
        }

        public EndUserContactPersonDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public EndUserContactPersonEntity CreateObject()
        {
            return EndUserContactPersonEntity.CreateObject();
        }

        public EndUserContactPersonEntity CreateObject(int entityId)
        {
            return EndUserContactPersonEntity.CreateObject(entityId);
        }

        public bool UpdateEndUserContactPerson(EndUserContactPersonEntity endUserContactPerson)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", endUserContactPerson.Originator, DbType.String);
                paramCollection.Add("@Title", endUserContactPerson.Title, DbType.String);
                paramCollection.Add("@FirstName", ValidateString(endUserContactPerson.FirstName), DbType.String);
                paramCollection.Add("@LastName", ValidateString(endUserContactPerson.LastName), DbType.String);
                paramCollection.Add("@Position", ValidateString(endUserContactPerson.Position), DbType.String);
                paramCollection.Add("@Telephone", endUserContactPerson.Telephone, DbType.String);
                paramCollection.Add("@Fax", endUserContactPerson.Fax, DbType.String);
                paramCollection.Add("@Mobile", ValidateString(endUserContactPerson.Mobile), DbType.String);
                paramCollection.Add("@EmailAddress", endUserContactPerson.EmailAddress, DbType.String);
                paramCollection.Add("@ReportsTo", ValidateString(endUserContactPerson.ReportsTo), DbType.String);
                paramCollection.Add("@MailingAddress", ValidateString(endUserContactPerson.MailingAddress), DbType.String);
                paramCollection.Add("@MailingCity", endUserContactPerson.MailingCity, DbType.String);
                paramCollection.Add("@MailingState", endUserContactPerson.MailingState, DbType.String);
                paramCollection.Add("@MailingPostcode", endUserContactPerson.MailingPostcode, DbType.String);
                paramCollection.Add("@MailingCountry", endUserContactPerson.MailingCountry, DbType.String);
                paramCollection.Add("@OtherAddress", ValidateString(endUserContactPerson.OtherAddress), DbType.String);
                paramCollection.Add("@OtherCity", endUserContactPerson.OtherCity, DbType.String);
                paramCollection.Add("@OtherState", endUserContactPerson.OtherState, DbType.String);
                paramCollection.Add("@OtherPostcode", endUserContactPerson.OtherPostCode, DbType.String);
                paramCollection.Add("@OtherCountry", endUserContactPerson.OtherCountry, DbType.String);
                paramCollection.Add("@Description", ValidateString(endUserContactPerson.Description), DbType.String);
                paramCollection.Add("@CreatedBy", endUserContactPerson.CreatedBy, DbType.String);
                if (endUserContactPerson.CreatedDate.HasValue)
                    paramCollection.Add("@CreatedDate", endUserContactPerson.CreatedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@LastModifiedBy", endUserContactPerson.LastModifiedBy, DbType.String);
                if (endUserContactPerson.LastModifiedDate.HasValue)
                paramCollection.Add("@LastModifiedDate", endUserContactPerson.LastModifiedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@EnduserCode", endUserContactPerson.EndUserCode, DbType.String);
                paramCollection.Add("@SpecialInterest", ValidateString(endUserContactPerson.SpecialInterests), DbType.String);
                paramCollection.Add("@ImagePath", endUserContactPerson.ImagePath, DbType.String);
                paramCollection.Add("@KeyContact", endUserContactPerson.KeyContact, DbType.String);
                paramCollection.Add("@ContactType", endUserContactPerson.ContactType, DbType.String);
                paramCollection.Add("@Origin", endUserContactPerson.Origin, DbType.String);
                paramCollection.Add("@ContactPersonId", endUserContactPerson.ContactPersonID, DbType.Int32);
                paramCollection.Add("@CustCode", endUserContactPerson.CustCode, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(EndUserContactPersonSql["UpdateEndUserContact"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public bool InsertEndUserContactPerson(EndUserContactPersonEntity endUserContactPerson, ref int contactPersonId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                endUserContactPerson.ContactPersonID = Convert.ToInt32(DataAcessService.GetOneValue(EndUserContactPersonSql["GetNextEndUserContactId"]));   // Get Next ID
                contactPersonId = endUserContactPerson.ContactPersonID;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", endUserContactPerson.Originator, DbType.String);
                paramCollection.Add("@Title", endUserContactPerson.Title, DbType.String);
                paramCollection.Add("@FirstName", ValidateString(endUserContactPerson.FirstName), DbType.String);
                paramCollection.Add("@LastName", ValidateString(endUserContactPerson.LastName), DbType.String);
                paramCollection.Add("@Position", ValidateString(endUserContactPerson.Position), DbType.String);
                paramCollection.Add("@Telephone", endUserContactPerson.Telephone, DbType.String);
                paramCollection.Add("@Fax", endUserContactPerson.Fax, DbType.String);
                paramCollection.Add("@Mobile", ValidateString(endUserContactPerson.Mobile), DbType.String);
                paramCollection.Add("@EmailAddress", endUserContactPerson.EmailAddress, DbType.String);
                paramCollection.Add("@ReportsTo", ValidateString(endUserContactPerson.ReportsTo), DbType.String);
                paramCollection.Add("@MailingAddress", ValidateString(endUserContactPerson.MailingAddress), DbType.String);
                paramCollection.Add("@MailingCity", endUserContactPerson.MailingCity, DbType.String);
                paramCollection.Add("@MailingState", endUserContactPerson.MailingState, DbType.String);
                paramCollection.Add("@MailingPostcode", endUserContactPerson.MailingPostcode, DbType.String);
                paramCollection.Add("@MailingCountry", endUserContactPerson.MailingCountry, DbType.String);
                paramCollection.Add("@OtherAddress", ValidateString(endUserContactPerson.OtherAddress), DbType.String);
                paramCollection.Add("@OtherCity", endUserContactPerson.OtherCity, DbType.String);
                paramCollection.Add("@OtherState", endUserContactPerson.OtherState, DbType.String);
                paramCollection.Add("@OtherPostcode", endUserContactPerson.OtherPostCode, DbType.String);
                paramCollection.Add("@OtherCountry", endUserContactPerson.OtherCountry, DbType.String);
                paramCollection.Add("@Description", ValidateString(endUserContactPerson.Description), DbType.String);
                paramCollection.Add("@CreatedBy", endUserContactPerson.CreatedBy, DbType.String);
               if(endUserContactPerson.CreatedDate.HasValue)
                   paramCollection.Add("@CreatedDate", endUserContactPerson.CreatedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@LastModifiedBy", endUserContactPerson.LastModifiedBy, DbType.String);
                if (endUserContactPerson.LastModifiedDate.HasValue)
                paramCollection.Add("@LastModifiedDate", endUserContactPerson.LastModifiedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@EnduserCode", endUserContactPerson.EndUserCode, DbType.String);
                paramCollection.Add("@SpecialInterest", ValidateString(endUserContactPerson.SpecialInterests), DbType.String);
                paramCollection.Add("@ImagePath", endUserContactPerson.ImagePath, DbType.String);
                paramCollection.Add("@KeyContact", endUserContactPerson.KeyContact, DbType.String);
                paramCollection.Add("@ContactType", endUserContactPerson.ContactType, DbType.String);
                paramCollection.Add("@Origin", endUserContactPerson.Origin, DbType.String);
                paramCollection.Add("@ContactPersonId", endUserContactPerson.ContactPersonID, DbType.Int32);
                paramCollection.Add("@CustCode", endUserContactPerson.CustCode, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(EndUserContactPersonSql["InsertEndUserContact"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }
        
        public bool UpdateKeyContact(string endUserCode, int contactPersonId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@KeyContact", "N", DbType.String);
                paramCollection.Add("@EnduserCode", endUserCode, DbType.String);
                paramCollection.Add("@ContactPersonId", contactPersonId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(EndUserContactPersonSql["UpdateEndUserKeyContact"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public EndUserContactPersonEntity GetEndUserContactPerson(int iContactPersonID)
        {
            DbDataReader drContactPerson = null;
            try
            {
                EndUserContactPersonEntity endUserContactPerson = EndUserContactPersonEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ContactPersonId", iContactPersonID, DbType.Int32);

                drContactPerson = this.DataAcessService.ExecuteQuery(EndUserContactPersonSql["GetEndUserContactDetails"], paramCollection);

                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactPersonIdOrdinal = drContactPerson.GetOrdinal("contact_person_id");
                    int enduser_codeOrdinal = drContactPerson.GetOrdinal("enduser_code");
                    int enduserName_codeOrdinal = drContactPerson.GetOrdinal("name");
                    int emailAddressOrdinal = drContactPerson.GetOrdinal("email_address");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");

                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");

                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");

                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");

                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");

                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int contactTypeOrdinal = drContactPerson.GetOrdinal("contact_type");
                    int originOrdinal = drContactPerson.GetOrdinal("origin");
                    int custCodeOrdinal = drContactPerson.GetOrdinal("cust_code");


                    if (drContactPerson.Read())
                    {
                        if (!drContactPerson.IsDBNull(contactPersonIdOrdinal)) endUserContactPerson.ContactPersonID = drContactPerson.GetInt32(contactPersonIdOrdinal);
                        if (!drContactPerson.IsDBNull(enduser_codeOrdinal)) endUserContactPerson.EndUserCode = drContactPerson.GetString(enduser_codeOrdinal);
                        if (!drContactPerson.IsDBNull(enduserName_codeOrdinal)) endUserContactPerson.EndUserName = drContactPerson.GetString(enduserName_codeOrdinal);
                        if (!drContactPerson.IsDBNull(emailAddressOrdinal)) endUserContactPerson.EmailAddress = drContactPerson.GetString(emailAddressOrdinal);
                        if (!drContactPerson.IsDBNull(titleOrdinal)) endUserContactPerson.Title = drContactPerson.GetString(titleOrdinal);
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) endUserContactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);

                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) endUserContactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) endUserContactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) endUserContactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) endUserContactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) endUserContactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);

                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) endUserContactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) endUserContactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) endUserContactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) endUserContactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) endUserContactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);

                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) endUserContactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) endUserContactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) endUserContactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) endUserContactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal).Trim();
                        if (!drContactPerson.IsDBNull(positionOrdinal)) endUserContactPerson.Position = drContactPerson.GetString(positionOrdinal);

                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) endUserContactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) endUserContactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(faxOrdinal)) endUserContactPerson.Fax = drContactPerson.GetString(faxOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) endUserContactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);

                        if (!drContactPerson.IsDBNull(createdByOrdinal)) endUserContactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedByOrdinal)) endUserContactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) endUserContactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal).ToLocalTime();
                        if (!drContactPerson.IsDBNull(lastModifiedDateOrdinal)) endUserContactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal).ToLocalTime();
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) endUserContactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);

                        if (!drContactPerson.IsDBNull(keyContactOrdinal)) endUserContactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);
                        if (!drContactPerson.IsDBNull(contactTypeOrdinal)) endUserContactPerson.ContactType = drContactPerson.GetString(contactTypeOrdinal);
                        if (!drContactPerson.IsDBNull(originOrdinal)) endUserContactPerson.Origin = drContactPerson.GetString(originOrdinal);

                        if (!drContactPerson.IsDBNull(originOrdinal)) endUserContactPerson.CustCode = drContactPerson.GetString(custCodeOrdinal);

                    }
                }

                return endUserContactPerson;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }

        public List<EndUserContactPersonEntity> GetEndUserContactPerson(ArgsEntity args)
        {
            DbDataReader drContactPerson = null;
            try
            {
                EndUserContactPersonEntity endUserContactPerson;
                List<EndUserContactPersonEntity> endUserContactPersonList = new List<EndUserContactPersonEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", args.CustomerCode, DbType.String);
                paramCollection.Add("@EnduserCode", args.EnduserCode, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
             //   paramCollection.Add("@ShowSQL", 0, DbType.Int32);

                drContactPerson = this.DataAcessService.ExecuteQuery(EndUserContactPersonSql["GetEndUserContactDetailsByEndUserCode"], paramCollection);

                if (drContactPerson != null && drContactPerson.HasRows)
                {
                    int contactPersonIdOrdinal = drContactPerson.GetOrdinal("contact_person_id");
                    int enduser_codeOrdinal = drContactPerson.GetOrdinal("enduser_code");
                    int emailAddressOrdinal = drContactPerson.GetOrdinal("email_address");
                    int titleOrdinal = drContactPerson.GetOrdinal("title");
                    int firstNameOrdinal = drContactPerson.GetOrdinal("first_name");

                    int lastNameOrdinal = drContactPerson.GetOrdinal("last_name");
                    int mailingCityOrdinal = drContactPerson.GetOrdinal("mailing_city");
                    int mailingCountryOrdinal = drContactPerson.GetOrdinal("mailing_country");
                    int mailingPostcodeOrdinal = drContactPerson.GetOrdinal("mailing_postcode");
                    int mailingStateOrdinal = drContactPerson.GetOrdinal("mailing_state");

                    int mailingAddressOrdinal = drContactPerson.GetOrdinal("mailing_address");
                    int mobileOrdinal = drContactPerson.GetOrdinal("mobile");
                    int originatorOrdinal = drContactPerson.GetOrdinal("originator");
                    int otherCityOrdinal = drContactPerson.GetOrdinal("other_city");
                    int otherCountryOrdinal = drContactPerson.GetOrdinal("other_country");

                    int otherPostcodeOrdinal = drContactPerson.GetOrdinal("other_postcode");
                    int otherStateOrdinal = drContactPerson.GetOrdinal("other_state");
                    int otherAddressOrdinal = drContactPerson.GetOrdinal("other_address");
                    int telephoneOrdinal = drContactPerson.GetOrdinal("telephone");
                    int positionOrdinal = drContactPerson.GetOrdinal("position");

                    int reportsToOrdinal = drContactPerson.GetOrdinal("reports_to");
                    int descriptionOrdinal = drContactPerson.GetOrdinal("description");
                    int faxOrdinal = drContactPerson.GetOrdinal("fax");
                    int imagePathOrdinal = drContactPerson.GetOrdinal("image_path");

                    int createdByOrdinal = drContactPerson.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = drContactPerson.GetOrdinal("last_modified_by");
                    int createdDateOrdinal = drContactPerson.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = drContactPerson.GetOrdinal("last_modified_date");
                    int specialInterestOrdinal = drContactPerson.GetOrdinal("special_interest");

                    int keyContactOrdinal = drContactPerson.GetOrdinal("key_contact");
                    int contactTypeOrdinal = drContactPerson.GetOrdinal("contact_type");
                    int originOrdinal = drContactPerson.GetOrdinal("origin");

                    int custCodeOrdinal = drContactPerson.GetOrdinal("cust_code");

                    while (drContactPerson.Read())
                    {
                        endUserContactPerson = EndUserContactPersonEntity.CreateObject();

                        if (!drContactPerson.IsDBNull(contactPersonIdOrdinal)) endUserContactPerson.ContactPersonID = drContactPerson.GetInt32(contactPersonIdOrdinal);
                        if (!drContactPerson.IsDBNull(enduser_codeOrdinal)) endUserContactPerson.EndUserCode = drContactPerson.GetString(enduser_codeOrdinal);

                        if (!drContactPerson.IsDBNull(emailAddressOrdinal)) endUserContactPerson.EmailAddress = drContactPerson.GetString(emailAddressOrdinal);
                        if (!drContactPerson.IsDBNull(titleOrdinal)) endUserContactPerson.Title = drContactPerson.GetString(titleOrdinal);
                        if (!drContactPerson.IsDBNull(firstNameOrdinal)) endUserContactPerson.FirstName = drContactPerson.GetString(firstNameOrdinal);

                        if (!drContactPerson.IsDBNull(lastNameOrdinal)) endUserContactPerson.LastName = drContactPerson.GetString(lastNameOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCityOrdinal)) endUserContactPerson.MailingCity = drContactPerson.GetString(mailingCityOrdinal);
                        if (!drContactPerson.IsDBNull(mailingCountryOrdinal)) endUserContactPerson.MailingCountry = drContactPerson.GetString(mailingCountryOrdinal);
                        if (!drContactPerson.IsDBNull(mailingPostcodeOrdinal)) endUserContactPerson.MailingPostcode = drContactPerson.GetString(mailingPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(mailingStateOrdinal)) endUserContactPerson.MailingState = drContactPerson.GetString(mailingStateOrdinal);

                        if (!drContactPerson.IsDBNull(mailingAddressOrdinal)) endUserContactPerson.MailingAddress = drContactPerson.GetString(mailingAddressOrdinal);
                        if (!drContactPerson.IsDBNull(mobileOrdinal)) endUserContactPerson.Mobile = drContactPerson.GetString(mobileOrdinal);
                        if (!drContactPerson.IsDBNull(originatorOrdinal)) endUserContactPerson.Originator = drContactPerson.GetString(originatorOrdinal);
                        if (!drContactPerson.IsDBNull(otherCityOrdinal)) endUserContactPerson.OtherCity = drContactPerson.GetString(otherCityOrdinal);
                        if (!drContactPerson.IsDBNull(otherCountryOrdinal)) endUserContactPerson.OtherCountry = drContactPerson.GetString(otherCountryOrdinal);

                        if (!drContactPerson.IsDBNull(otherPostcodeOrdinal)) endUserContactPerson.OtherPostCode = drContactPerson.GetString(otherPostcodeOrdinal);
                        if (!drContactPerson.IsDBNull(otherStateOrdinal)) endUserContactPerson.OtherState = drContactPerson.GetString(otherStateOrdinal);
                        if (!drContactPerson.IsDBNull(otherAddressOrdinal)) endUserContactPerson.OtherAddress = drContactPerson.GetString(otherAddressOrdinal);
                        if (!drContactPerson.IsDBNull(telephoneOrdinal)) endUserContactPerson.Telephone = drContactPerson.GetString(telephoneOrdinal);
                        if (!drContactPerson.IsDBNull(positionOrdinal)) endUserContactPerson.Position = drContactPerson.GetString(positionOrdinal);

                        if (!drContactPerson.IsDBNull(reportsToOrdinal)) endUserContactPerson.ReportsTo = drContactPerson.GetString(reportsToOrdinal);
                        if (!drContactPerson.IsDBNull(descriptionOrdinal)) endUserContactPerson.Description = drContactPerson.GetString(descriptionOrdinal);
                        if (!drContactPerson.IsDBNull(faxOrdinal)) endUserContactPerson.Fax = drContactPerson.GetString(faxOrdinal);
                        if (!drContactPerson.IsDBNull(imagePathOrdinal)) endUserContactPerson.ImagePath = drContactPerson.GetString(imagePathOrdinal);

                        if (!drContactPerson.IsDBNull(createdByOrdinal)) endUserContactPerson.CreatedBy = drContactPerson.GetString(createdByOrdinal);
                        if (!drContactPerson.IsDBNull(lastModifiedByOrdinal)) endUserContactPerson.LastModifiedBy = drContactPerson.GetString(lastModifiedByOrdinal);
                        if (!drContactPerson.IsDBNull(createdDateOrdinal)) endUserContactPerson.CreatedDate = drContactPerson.GetDateTime(createdDateOrdinal).ToLocalTime();
                        if (!drContactPerson.IsDBNull(lastModifiedDateOrdinal)) endUserContactPerson.LastModifiedDate = drContactPerson.GetDateTime(lastModifiedDateOrdinal).ToLocalTime();
                        if (!drContactPerson.IsDBNull(specialInterestOrdinal)) endUserContactPerson.SpecialInterests = drContactPerson.GetString(specialInterestOrdinal);

                        if (!drContactPerson.IsDBNull(keyContactOrdinal)) endUserContactPerson.KeyContact = drContactPerson.GetString(keyContactOrdinal);
                        if (!drContactPerson.IsDBNull(contactTypeOrdinal)) endUserContactPerson.ContactType = drContactPerson.GetString(contactTypeOrdinal);
                        if (!drContactPerson.IsDBNull(originOrdinal)) endUserContactPerson.Origin = drContactPerson.GetString(originOrdinal);

                        if (!drContactPerson.IsDBNull(originOrdinal)) endUserContactPerson.CustCode = drContactPerson.GetString(custCodeOrdinal);

                        endUserContactPersonList.Add(endUserContactPerson);
                    }
                }

                return endUserContactPersonList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPerson != null)
                    drContactPerson.Close();
            }
        }
    }
}
