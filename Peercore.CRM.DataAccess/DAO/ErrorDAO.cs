﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ErrorDAO : BaseDAO
    {
        private DbSqlAdapter ErrorSql { get; set; }

        private void RegisterSql()
        {
            this.ErrorSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ErrorDAO()
        {
            RegisterSql();
        }

        public ErrorDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ErrorEntity CreateObject()
        {
            return ErrorEntity.CreateObject();
        }

        public ErrorEntity CreateObject(int entityId)
        {
            return ErrorEntity.CreateObject(entityId);
        }

        public int GetErrorCountForCurrentDate(string errorsource, string referer, string shortDesc, string methodname)
        {
            DbDataReader idError = null;

            string sWhereCls = " error_date >= '" + DateTime.Parse(DateTime.Today.ToString("dd-MMM-yyyy") + " 00:00:00").ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss")
                            + "' AND error_date <= '" + DateTime.Parse(DateTime.Today.ToString("dd-MMM-yyyy") + " 23:59:59").ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss") + "'";

            try
            {
                if (!string.IsNullOrWhiteSpace(errorsource))
                {
                    sWhereCls += " and error_link = '" + errorsource + "'";
                }

                if (!string.IsNullOrWhiteSpace(referer))
                {
                    sWhereCls += " and referer = '" + referer + "'";
                }

                if (!string.IsNullOrWhiteSpace(shortDesc))
                {
                    sWhereCls += " and  short_description= '" + shortDesc + "'";
                }

                int count = 0;
                //string sSQL = string.Format(CRMSQLs.GetErrorCountForCurrentDate,
                //            sWhereCls);

                idError = this.DataAcessService.ExecuteQuery(ErrorSql["GetErrorCountForCurrentDate"].Format(sWhereCls));

                if (idError != null && idError.HasRows)
                {
                    int errorcountOrdinal = idError.GetOrdinal("count");

                    while (idError.Read())
                    {
                        if (!idError.IsDBNull(errorcountOrdinal)) count = idError.GetInt32(errorcountOrdinal);
                    }
                }

                return count;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idError != null)
                    idError.Close();
            }
        }

        public int SaveError(ErrorEntity error)
        {
            int newActivityId = 0;

            try
            {
                // Insert Error
                error.ID = Convert.ToInt32(this.DataAcessService.GetOneValue(ErrorSql["GetNextErrorId"]));   // Get Next ID
                newActivityId = error.ID;
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@id", error.ID, DbType.String);
                paramCollection.Add("@originator_id", error.OriginatorID, DbType.String);
                paramCollection.Add("@short_description", ValidateString(error.ShortDescription), DbType.String);
                paramCollection.Add("@long_description", ValidateString(error.LongDescription), DbType.String);
                paramCollection.Add("@ipaddress", error.IpAddress, DbType.String);
                paramCollection.Add("@referer", ValidateString(error.Referer), DbType.String);
                paramCollection.Add("@error_link", ValidateString(error.Errorlink), DbType.String);
                paramCollection.Add("@method_name", ValidateString(error.MethodName), DbType.String);
                paramCollection.Add("@host_name", ValidateString(error.Host), DbType.String);
                paramCollection.Add("@error_date", error.ErrorDate.HasValue ? error.ErrorDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss") : null, DbType.String);

                return this.DataAcessService.ExecuteNonQuery(ErrorSql["InsertError"],paramCollection);

            }
            catch
            {
                throw;
            }
        }
    }
}
