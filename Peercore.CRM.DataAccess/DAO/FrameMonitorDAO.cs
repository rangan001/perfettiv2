﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class FrameMonitorDAO:BaseDAO
    {
        private DbSqlAdapter FrameMonitorSql { get; set; }

        private void RegisterSql()
        {
            this.FrameMonitorSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.FrameMonitorSql.xml", ApplicationService.Instance.DbProvider);
        }

        public FrameMonitorDAO()
        {
            RegisterSql();
        }

        public FrameMonitorDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public FrameMonitorEntity CreateObject()
        {
            return FrameMonitorEntity.CreateObject();
        }

        public FrameMonitorEntity CreateObject(int entityId)
        {
            return FrameMonitorEntity.CreateObject(entityId);
        }

        public bool InsertFrameMonitor(FrameMonitorEntity frameMonitorEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@start_time", frameMonitorEntity.StartTime, DbType.DateTime);
                paramCollection.Add("@end_time", frameMonitorEntity.EndTime, DbType.DateTime);
                paramCollection.Add("@rep_code", frameMonitorEntity.RepCode, DbType.String);
                paramCollection.Add("@frame_name", frameMonitorEntity.FrameName, DbType.String);
                //commented due to the request
                //paramCollection.Add("@longitude", frameMonitorEntity.Longitude, DbType.Double);
                //paramCollection.Add("@latitude", frameMonitorEntity.Latitude, DbType.Double);
                paramCollection.Add("@created_by", frameMonitorEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(FrameMonitorSql["InsertFrameMonitor"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateFrameMonitor(FrameMonitorEntity frameMonitorEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("frame_monitor_id", frameMonitorEntity.FrameMonitorId, DbType.Int32);
                paramCollection.Add("@start_time", frameMonitorEntity.StartTime, DbType.DateTime);
                paramCollection.Add("@end_time", frameMonitorEntity.EndTime, DbType.DateTime);
                paramCollection.Add("@rep_code", frameMonitorEntity.RepCode, DbType.String);
                paramCollection.Add("@frame_name", frameMonitorEntity.FrameName, DbType.String);
                //commented due to the request
                //paramCollection.Add("@longitude", frameMonitorEntity.Longitude, DbType.Double);
                //paramCollection.Add("@latitude", frameMonitorEntity.Latitude, DbType.Double);
                paramCollection.Add("@last_modified_by", frameMonitorEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(FrameMonitorSql["UpdateFrameMonitor"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
    }
}
