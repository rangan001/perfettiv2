﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class HolidayPlannerChangeLogDAO:BaseDAO
    {
        private DbSqlAdapter ChangeLogSql { get; set; }

        private void RegisterSql()
        {
            this.ChangeLogSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.HolidayPlannerChangeLogSql.xml", ApplicationService.Instance.DbProvider);
        }

        public HolidayPlannerChangeLogDAO()
        {
            RegisterSql();
        }

        public HolidayPlannerChangeLogDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public HolidayPlannerChangeLogEntity CreateObject()
        {
            return HolidayPlannerChangeLogEntity.CreateObject();
        }

        public HolidayPlannerChangeLogEntity CreateObject(int entityId)
        {
            return HolidayPlannerChangeLogEntity.CreateObject(entityId);
        }

        public int InsertHolidayPlannerChangeLog(HolidayPlannerChangeLogEntity holidayPlannerChangeLogEntity)
        {
            bool successful = false;
            int itineraryLogEntryId = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@KeyId", holidayPlannerChangeLogEntity.KeyId, DbType.Int32);
                paramCollection.Add("@FormType", holidayPlannerChangeLogEntity.FormType, DbType.String);
                paramCollection.Add("@ChangedFromDate", holidayPlannerChangeLogEntity.ChangedFromDate, DbType.DateTime);
                paramCollection.Add("@ChangedToDate", holidayPlannerChangeLogEntity.ChangedToDate, DbType.DateTime);
                paramCollection.Add("@Comment", holidayPlannerChangeLogEntity.Comment, DbType.String);
                paramCollection.Add("@Status", holidayPlannerChangeLogEntity.Status, DbType.String);
                paramCollection.Add("@CreatedBy", holidayPlannerChangeLogEntity.CreatedBy, DbType.String);
                paramCollection.Add("@ID", null, DbType.Int32, ParameterDirection.Output);

                itineraryLogEntryId = this.DataAcessService.ExecuteNonQuery(ChangeLogSql["InsertHolidayPlannerChangeLog"], paramCollection, true, "@ID");

                if (itineraryLogEntryId > 0)
                    successful = true;

                return itineraryLogEntryId;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateHolidayPlannerChangeLogStatus(HolidayPlannerChangeLogEntity holidayPlannerChangeLogEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@HolidayPlannerChangeLogId", holidayPlannerChangeLogEntity.HolidayPlannerChangeLogId, DbType.Int32);
                paramCollection.Add("@Status", holidayPlannerChangeLogEntity.Status, DbType.String);
                paramCollection.Add("@Comment", holidayPlannerChangeLogEntity.Comment, DbType.String);
                paramCollection.Add("@LastModifiedBy", holidayPlannerChangeLogEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChangeLogSql["UpdateHolidayPlannerChangeLogStatus"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

        }

        public HolidayPlannerChangeLogEntity GetHolidayPlannerChangeLogById(int holidayPlannerChangeLogId)
        {
            DbDataReader drChangeLog = null;

            try
            {
                HolidayPlannerChangeLogEntity holidayPlannerChangeLogEntity = HolidayPlannerChangeLogEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@HolidayPlannerChangeLogId", holidayPlannerChangeLogId, DbType.Int32);

                drChangeLog = this.DataAcessService.ExecuteQuery(ChangeLogSql["GetHolidayPlannerChangeLogById"], paramCollection);

                if (drChangeLog != null && drChangeLog.HasRows)
                {
                    int IdOrdinal = drChangeLog.GetOrdinal("id");
                    int keyIdOrdinal = drChangeLog.GetOrdinal("key_id");
                    int formTypeOrdinal = drChangeLog.GetOrdinal("form_type");
                    int changedFromDateOrdinal = drChangeLog.GetOrdinal("changed_from_date");
                    int changedToDateOrdinal = drChangeLog.GetOrdinal("changed_to_date");
                    int commentOrdinal = drChangeLog.GetOrdinal("comment");
                    int statusOrdinal = drChangeLog.GetOrdinal("status");
                    int createdByOrdinal = drChangeLog.GetOrdinal("created_by");
                    int createdDateOrdinal = drChangeLog.GetOrdinal("created_date");
                    int modifiedByOrdinal = drChangeLog.GetOrdinal("last_modified_by");
                    int modifiedDateOrdinal = drChangeLog.GetOrdinal("last_modified_date");

                    while (drChangeLog.Read())
                    {
                        if (!drChangeLog.IsDBNull(IdOrdinal)) holidayPlannerChangeLogEntity.HolidayPlannerChangeLogId = drChangeLog.GetInt32(IdOrdinal);
                        if (!drChangeLog.IsDBNull(keyIdOrdinal)) holidayPlannerChangeLogEntity.KeyId = drChangeLog.GetInt32(keyIdOrdinal);
                        if (!drChangeLog.IsDBNull(formTypeOrdinal)) holidayPlannerChangeLogEntity.FormType = drChangeLog.GetString(formTypeOrdinal);
                        if (!drChangeLog.IsDBNull(changedFromDateOrdinal)) holidayPlannerChangeLogEntity.ChangedFromDate = drChangeLog.GetDateTime(changedFromDateOrdinal);
                        if (!drChangeLog.IsDBNull(changedToDateOrdinal)) holidayPlannerChangeLogEntity.ChangedToDate = drChangeLog.GetDateTime(changedToDateOrdinal);
                        if (!drChangeLog.IsDBNull(commentOrdinal)) holidayPlannerChangeLogEntity.Comment = drChangeLog.GetString(commentOrdinal);
                        if (!drChangeLog.IsDBNull(statusOrdinal)) holidayPlannerChangeLogEntity.Status = drChangeLog.GetString(statusOrdinal);

                        if (!drChangeLog.IsDBNull(createdByOrdinal)) holidayPlannerChangeLogEntity.CreatedBy = drChangeLog.GetString(createdByOrdinal);
                        if (!drChangeLog.IsDBNull(createdDateOrdinal)) holidayPlannerChangeLogEntity.CreatedDate = drChangeLog.GetDateTime(createdDateOrdinal);
                        if (!drChangeLog.IsDBNull(modifiedByOrdinal)) holidayPlannerChangeLogEntity.LastModifiedBy = drChangeLog.GetString(modifiedByOrdinal);
                        if (!drChangeLog.IsDBNull(modifiedDateOrdinal)) holidayPlannerChangeLogEntity.LastModifiedDate = drChangeLog.GetDateTime(modifiedDateOrdinal);
                    }
                }
                return holidayPlannerChangeLogEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChangeLog != null)
                    drChangeLog.Close();
            }
        }
    }
}
