﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using System.Linq;


namespace Peercore.CRM.DataAccess.DAO
{
    public class HomePageSummaryDAO : BaseDAO
    {
        private DbSqlAdapter HomePageSummarySql { get; set; }

        private void RegisterSql()
        {
            this.HomePageSummarySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.HomePageSummarySql.xml", ApplicationService.Instance.DbProvider);
        }

        public HomePageSummaryDAO()
        {
            RegisterSql();
        }

        public HomePageSummaryDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        //public LeadAddressEntity CreateObject()
        //{
        //    return LeadAddressEntity.CreateObject();
        //}

        //public LeadAddressEntity CreateObject(int entityId)
        //{
        //    return LeadAddressEntity.CreateObject(entityId);
        //}

        public List<OpenDealEntity> GetTopFiveOpportunities(string sBusiness, ArgsEntity args)
        {
            DbDataReader opportunityReader = null;
            List<OpenDealEntity> lOpportunityOwner = new List<OpenDealEntity>();
            
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Business", sBusiness, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                opportunityReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetTopFiveOpportunities"], paramCollection);
                
                if (opportunityReader.HasRows)
                {
                    int nameOrdinal = opportunityReader.GetOrdinal("name");
                    int amountOrdinal = opportunityReader.GetOrdinal("amount");
                    int repOrdinal = opportunityReader.GetOrdinal("originator");
                    int customerOrdinal = opportunityReader.GetOrdinal("customer");

                    while (opportunityReader.Read())
                    {
                        OpenDealEntity oOpportunityOwner = OpenDealEntity.CreateObject();

                        if (!opportunityReader.IsDBNull(nameOrdinal)) oOpportunityOwner.OpportunityName = opportunityReader.GetString(nameOrdinal);
                        if (!opportunityReader.IsDBNull(amountOrdinal)) oOpportunityOwner.Amount = opportunityReader.GetDouble(amountOrdinal);
                        if (!opportunityReader.IsDBNull(repOrdinal)) oOpportunityOwner.Rep = opportunityReader.GetString(repOrdinal);
                        if (!opportunityReader.IsDBNull(customerOrdinal)) oOpportunityOwner.Customer = opportunityReader.GetString(customerOrdinal);

                        lOpportunityOwner.Add(oOpportunityOwner);
                    }
                }
                return lOpportunityOwner;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (opportunityReader != null && (!opportunityReader.IsClosed))
                    opportunityReader.Close();
            }
        }

        public List<OpportunityOwnerEntity> GetLeaderBoard(string cutoffDate, string closeDate, string sPeriod, string businessCode,ArgsEntity args)
        {
            DbDataReader opportunityReader = null;
            List<OpportunityOwnerEntity> lOpportunityOwner = new List<OpportunityOwnerEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@BusinessCode", businessCode, DbType.String);
                //paramCollection.Add("@Period", sPeriod, DbType.String);
                //paramCollection.Add("@CutoffDate", DateTime.Parse(cutoffDate.Value.ToString("dd-MMM-yyyy") + " 00:00:00").ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@CutoffDate", cutoffDate, DbType.String);
                if (sPeriod == "MTD")
                    paramCollection.Add("@CloseDate", closeDate, DbType.String);
                else
                    paramCollection.Add("@CloseDate", null, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                opportunityReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetLeaderBoard"], paramCollection);
                
                if (opportunityReader.HasRows)
                {
                    int nameOrdinal = opportunityReader.GetOrdinal("name");
                    int amountOrdinal = opportunityReader.GetOrdinal("amount");
                    int tonnesOrdinal = opportunityReader.GetOrdinal("tonnes");
                    int numberOrdinal = opportunityReader.GetOrdinal("number");
                    int originatorOrdinal = opportunityReader.GetOrdinal("originator");

                    while (opportunityReader.Read())
                    {
                        OpportunityOwnerEntity oOpportunityOwner = OpportunityOwnerEntity.CreateObject();

                        if (!opportunityReader.IsDBNull(nameOrdinal)) oOpportunityOwner.OwnerName = opportunityReader.GetString(nameOrdinal);
                        if (!opportunityReader.IsDBNull(amountOrdinal)) oOpportunityOwner.Amount = opportunityReader.GetDouble(amountOrdinal);
                        if (!opportunityReader.IsDBNull(tonnesOrdinal)) oOpportunityOwner.Units = opportunityReader.GetDouble(tonnesOrdinal);
                        if (!opportunityReader.IsDBNull(numberOrdinal)) oOpportunityOwner.Count = opportunityReader.GetInt32(numberOrdinal);
                        if (!opportunityReader.IsDBNull(originatorOrdinal)) oOpportunityOwner.Originator = opportunityReader.GetString(originatorOrdinal);

                        lOpportunityOwner.Add(oOpportunityOwner);
                    }
                }
                return lOpportunityOwner;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (opportunityReader != null && (!opportunityReader.IsClosed))
                    opportunityReader.Close();
            }
        }

        public int? GetTotalOpportunities(string originator, bool isWon,string cutoffDate, string closeDate, string sPeriod, string businessCode)
        {
            try
            {
                //string whereCls = string.Empty;

                //if (isWon)
                //{
                //    whereCls = "AND oc.pipeline_stage_id= (SELECT pipeline_stage_id FROM crm_pipeline_stage WHERE pipeline_stage='Won') ";
                //}

                //if (businessCode != "ALL")
                //    whereCls += " AND ca.market = '" + businessCode + "'";

                //if (sPeriod == "MTD")
                //    whereCls += " AND oc.close_date <= '" + DateTime.Parse(DateTime.Now.Date.ToString("dd-MMM-yyyy") + " 23:59:59").ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss") + "'";

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@BusinessCode", businessCode, DbType.String);
                paramCollection.Add("@IsWon", isWon, DbType.Boolean);
                //paramCollection.Add("@Period", sPeriod, DbType.String);
                paramCollection.Add("@CutoffDate", cutoffDate, DbType.String);
                paramCollection.Add("@CloseDate", closeDate, DbType.String);

                return (Nullable<int>)DataAcessService.GetOneValue(HomePageSummarySql["GetOpportunitiesCount"], paramCollection);

            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        public List<LeadCustomerOpportunityEntity> GetLeaderCustomerOpportunity(ArgsEntity args)
        {
            DbDataReader opportunityReader = null;
            List<LeadCustomerOpportunityEntity> lOpportunityOwner = new List<LeadCustomerOpportunityEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "o.originator"), DbType.String);
                paramCollection.Add("@Market", args.Market, DbType.String);
                paramCollection.Add("@State", args.State, DbType.String);
                paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);                
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowCount", 0, DbType.Boolean);

                opportunityReader = DataAcessService.ExecuteQuery(HomePageSummarySql["GetOpportunityByLeadCust"], paramCollection);

                if (opportunityReader.HasRows)
                {
                    int nameOrdinal = opportunityReader.GetOrdinal("name1");
                    int amountOrdinal = opportunityReader.GetOrdinal("amount");
                    int tonnesOrdinal = opportunityReader.GetOrdinal("tonnes");
                    int opportunityOrdinal = opportunityReader.GetOrdinal("opportunity");
                    int originatorOrdinal = opportunityReader.GetOrdinal("originator");
                    int pipelineStageOrdinal = opportunityReader.GetOrdinal("pipeline_stage");
                    int catlogCodeOrdinal = opportunityReader.GetOrdinal("catlog_code");
                    int stateOrdinal = opportunityReader.GetOrdinal("state");
                    int opportunityIdOrdinal = opportunityReader.GetOrdinal("opportunity_id");
                    int repCodeOrdinal = opportunityReader.GetOrdinal("rep_code");
                    int repNameOrdinal = opportunityReader.GetOrdinal("repname");
                    int marketOrdinal = opportunityReader.GetOrdinal("market");
                    int leadIdOrdinal = opportunityReader.GetOrdinal("lead_id");
                    int custCodeOrdinal = opportunityReader.GetOrdinal("cust_code");
                    int closeDateOrdinal = opportunityReader.GetOrdinal("close_date");
                    int createdDateOrdinal = opportunityReader.GetOrdinal("created_date");
                    int marketDescriptionOrdinal = opportunityReader.GetOrdinal("market_description");
                    int enduserCodeOrdinal = opportunityReader.GetOrdinal("enduser_code");
                    int totalcountOrdinal = opportunityReader.GetOrdinal("total_count");

                    while (opportunityReader.Read())
                    {
                        LeadCustomerOpportunityEntity oOpportunityOwner = LeadCustomerOpportunityEntity.CreateObject();

                        if (!opportunityReader.IsDBNull(nameOrdinal)) oOpportunityOwner.Name = opportunityReader.GetString(nameOrdinal);
                        if (!opportunityReader.IsDBNull(amountOrdinal)) oOpportunityOwner.Amount = opportunityReader.GetDouble(amountOrdinal);
                        if (!opportunityReader.IsDBNull(tonnesOrdinal)) oOpportunityOwner.Tonnes = opportunityReader.GetDouble(tonnesOrdinal);
                        if (!opportunityReader.IsDBNull(opportunityOrdinal)) oOpportunityOwner.Opportunity = opportunityReader.GetString(opportunityOrdinal);
                        if (!opportunityReader.IsDBNull(originatorOrdinal)) oOpportunityOwner.Originator = opportunityReader.GetString(originatorOrdinal).Trim();
                        if (!opportunityReader.IsDBNull(pipelineStageOrdinal)) oOpportunityOwner.Stage = opportunityReader.GetString(pipelineStageOrdinal);
                        if (!opportunityReader.IsDBNull(catlogCodeOrdinal)) oOpportunityOwner.CatalogCode = opportunityReader.GetString(catlogCodeOrdinal);
                        if (!opportunityReader.IsDBNull(stateOrdinal)) oOpportunityOwner.State = opportunityReader.GetString(stateOrdinal);
                        if (!opportunityReader.IsDBNull(opportunityIdOrdinal)) oOpportunityOwner.OpportunityId = opportunityReader.GetInt32(opportunityIdOrdinal);
                        if (!opportunityReader.IsDBNull(repCodeOrdinal)) oOpportunityOwner.RepCode = opportunityReader.GetString(repCodeOrdinal);
                        if (!opportunityReader.IsDBNull(repNameOrdinal)) oOpportunityOwner.RepName = opportunityReader.GetString(repNameOrdinal);
                        if (!opportunityReader.IsDBNull(marketOrdinal)) oOpportunityOwner.Market = opportunityReader.GetString(marketOrdinal);
                        if (!opportunityReader.IsDBNull(leadIdOrdinal)) oOpportunityOwner.LeadId = opportunityReader.GetInt32(leadIdOrdinal);
                        if (!opportunityReader.IsDBNull(custCodeOrdinal)) oOpportunityOwner.CustomerCode = opportunityReader.GetString(custCodeOrdinal);
                        if (!opportunityReader.IsDBNull(closeDateOrdinal)) oOpportunityOwner.CloseDate = opportunityReader.GetDateTime(closeDateOrdinal);
                        if (!opportunityReader.IsDBNull(createdDateOrdinal)) oOpportunityOwner.CreatedDate = opportunityReader.GetDateTime(createdDateOrdinal);
                        if (!opportunityReader.IsDBNull(marketDescriptionOrdinal)) oOpportunityOwner.MarketDescription = opportunityReader.GetString(marketDescriptionOrdinal);
                        if (!opportunityReader.IsDBNull(enduserCodeOrdinal)) oOpportunityOwner.EndUserCode = opportunityReader.GetString(enduserCodeOrdinal);
                        if (!opportunityReader.IsDBNull(totalcountOrdinal)) oOpportunityOwner.TotalCount = opportunityReader.GetInt32(totalcountOrdinal);

                        lOpportunityOwner.Add(oOpportunityOwner);
                    }
                }
                return lOpportunityOwner;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (opportunityReader != null && (!opportunityReader.IsClosed))
                    opportunityReader.Close();
            }
        }

        public List<LeadCustomerOpportunityEntity> GetLeaderCustomerOpportunityCount(ArgsEntity args)
        {
            DbDataReader opportunityReader = null;
            List<LeadCustomerOpportunityEntity> lOpportunityOwner = new List<LeadCustomerOpportunityEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "o.originator"), DbType.String);
                paramCollection.Add("@Market", args.Market, DbType.String);
                paramCollection.Add("@State", args.State, DbType.String);
                paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowCount", 1, DbType.Boolean);

                opportunityReader = DataAcessService.ExecuteQuery(HomePageSummarySql["GetOpportunityByLeadCust"], paramCollection);

                if (opportunityReader.HasRows)
                {
                    int nameOrdinal = opportunityReader.GetOrdinal("name");
                    int amountOrdinal = opportunityReader.GetOrdinal("amount");
                    int originatorOrdinal = opportunityReader.GetOrdinal("originator");
                    int tonnesOrdinal = opportunityReader.GetOrdinal("tonnes");
                    int totalcountOrdinal = opportunityReader.GetOrdinal("total_count");
                    int woncountOrdinal = opportunityReader.GetOrdinal("won_count");

                    while (opportunityReader.Read())
                    {
                        LeadCustomerOpportunityEntity oOpportunityOwner = LeadCustomerOpportunityEntity.CreateObject();

                        if (!opportunityReader.IsDBNull(nameOrdinal)) oOpportunityOwner.Name = opportunityReader.GetString(nameOrdinal);
                        if (!opportunityReader.IsDBNull(amountOrdinal)) oOpportunityOwner.Amount = opportunityReader.GetDouble(amountOrdinal);
                        if (!opportunityReader.IsDBNull(originatorOrdinal)) oOpportunityOwner.Originator = opportunityReader.GetString(originatorOrdinal).Trim();
                        if (!opportunityReader.IsDBNull(tonnesOrdinal)) oOpportunityOwner.Tonnes = opportunityReader.GetDouble(tonnesOrdinal);
                        if (!opportunityReader.IsDBNull(totalcountOrdinal)) oOpportunityOwner.TotalCount = opportunityReader.GetInt32(totalcountOrdinal);
                        if (!opportunityReader.IsDBNull(woncountOrdinal)) oOpportunityOwner.Won = opportunityReader.GetInt32(woncountOrdinal);

                        oOpportunityOwner.Ratio = Math.Round((double.Parse(oOpportunityOwner.Won.ToString()) / oOpportunityOwner.TotalCount) * 100,2);
                        lOpportunityOwner.Add(oOpportunityOwner);
                    }
                }
                return lOpportunityOwner;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (opportunityReader != null && (!opportunityReader.IsClosed))
                    opportunityReader.Close();
            }
        }


        public List<SalesTrendEntity> GetSalesTrend(string sBusiness)
        {
            DbDataReader salesTrendReader = null;
            List<SalesTrendEntity> lSalesTrend = new List<SalesTrendEntity>();
            //string sWhereCls = "";

            try
            {
                int CostPeriod = Convert.ToInt32(DataAcessService.GetOneValue(HomePageSummarySql["GetCurrentCostPeriod"]));

                //if (sBusiness != "ALL")
                //    sWhereCls = " AND sh.market = '" + sBusiness + "'";

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Business", sBusiness, DbType.String);

                salesTrendReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetSalesTrend"], paramCollection);

                //salesTrendReader = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetSalesTrend,
                //    sWhereCls));

                if (salesTrendReader.HasRows)
                {
                    int businessOrdinal = salesTrendReader.GetOrdinal("market");
                    int tableDescriptionOrdinal = salesTrendReader.GetOrdinal("table_description");

                    int julyOrdinal = salesTrendReader.GetOrdinal("jul");
                    int augustOrdinal = salesTrendReader.GetOrdinal("aug");
                    int septemberOrdinal = salesTrendReader.GetOrdinal("sep");
                    int octoberOrdinal = salesTrendReader.GetOrdinal("oct");
                    int novemberOrdinal = salesTrendReader.GetOrdinal("nov");
                    int decemberOrdinal = salesTrendReader.GetOrdinal("dec");

                    int januaryOrdinal = salesTrendReader.GetOrdinal("jan");
                    int februaryOrdinal = salesTrendReader.GetOrdinal("feb");
                    int marchOrdinal = salesTrendReader.GetOrdinal("mar");
                    int aprilOrdinal = salesTrendReader.GetOrdinal("apr");
                    int mayOrdinal = salesTrendReader.GetOrdinal("may");
                    int junOrdinal = salesTrendReader.GetOrdinal("jun");

                    while (salesTrendReader.Read())
                    {
                        SalesTrendEntity oSalesTrend = SalesTrendEntity.CreateObject();
                        List<KeyValuePair<string, double>> monthSalesList = new List<KeyValuePair<string, double>>();

                        if (!salesTrendReader.IsDBNull(businessOrdinal)) oSalesTrend.Channel = salesTrendReader.GetString(businessOrdinal);
                        if (!salesTrendReader.IsDBNull(tableDescriptionOrdinal)) oSalesTrend.ChannelDescription = salesTrendReader.GetString(tableDescriptionOrdinal);

                        if (!salesTrendReader.IsDBNull(julyOrdinal))
                        {
                            KeyValuePair<string, double> julKVP = new KeyValuePair<string, double>("JUL", Convert.ToDouble(salesTrendReader.GetDecimal(julyOrdinal)));
                            monthSalesList.Add(julKVP);
                        }
                        if (!salesTrendReader.IsDBNull(augustOrdinal))
                        {
                            KeyValuePair<string, double> augKVP = new KeyValuePair<string, double>("AUG", Convert.ToDouble(salesTrendReader.GetDecimal(augustOrdinal)));
                            monthSalesList.Add(augKVP);
                        }
                        if (!salesTrendReader.IsDBNull(septemberOrdinal))
                        {
                            KeyValuePair<string, double> sepKVP = new KeyValuePair<string, double>("SEP", Convert.ToDouble(salesTrendReader.GetDecimal(septemberOrdinal)));
                            monthSalesList.Add(sepKVP);
                        }
                        if (!salesTrendReader.IsDBNull(octoberOrdinal))
                        {
                            KeyValuePair<string, double> octKVP = new KeyValuePair<string, double>("OCT", Convert.ToDouble(salesTrendReader.GetDecimal(octoberOrdinal)));
                            monthSalesList.Add(octKVP);
                        }
                        if (!salesTrendReader.IsDBNull(novemberOrdinal))
                        {
                            KeyValuePair<string, double> novKVP = new KeyValuePair<string, double>("NOV", Convert.ToDouble(salesTrendReader.GetDecimal(novemberOrdinal)));
                            monthSalesList.Add(novKVP);
                        }
                        if (!salesTrendReader.IsDBNull(decemberOrdinal))
                        {
                            KeyValuePair<string, double> decKVP = new KeyValuePair<string, double>("DEC", Convert.ToDouble(salesTrendReader.GetDecimal(decemberOrdinal)));
                            monthSalesList.Add(decKVP);
                        }


                        if (!salesTrendReader.IsDBNull(januaryOrdinal))
                        {
                            KeyValuePair<string, double> janKVP = new KeyValuePair<string, double>("JAN", Convert.ToDouble(salesTrendReader.GetDecimal(januaryOrdinal)));
                            monthSalesList.Add(janKVP);
                        }
                        if (!salesTrendReader.IsDBNull(februaryOrdinal))
                        {
                            KeyValuePair<string, double> febKVP = new KeyValuePair<string, double>("FEB", Convert.ToDouble(salesTrendReader.GetDecimal(februaryOrdinal)));
                            monthSalesList.Add(febKVP);
                        }
                        if (!salesTrendReader.IsDBNull(marchOrdinal))
                        {
                            KeyValuePair<string, double> marKVP = new KeyValuePair<string, double>("MAR", Convert.ToDouble(salesTrendReader.GetDecimal(marchOrdinal)));
                            monthSalesList.Add(marKVP);
                        }
                        if (!salesTrendReader.IsDBNull(aprilOrdinal))
                        {
                            KeyValuePair<string, double> aprKVP = new KeyValuePair<string, double>("APR", Convert.ToDouble(salesTrendReader.GetDecimal(aprilOrdinal)));
                            monthSalesList.Add(aprKVP);
                        }
                        if (!salesTrendReader.IsDBNull(mayOrdinal))
                        {
                            KeyValuePair<string, double> mayKVP = new KeyValuePair<string, double>("MAY", Convert.ToDouble(salesTrendReader.GetDecimal(mayOrdinal)));
                            monthSalesList.Add(mayKVP);
                        }
                        if (!salesTrendReader.IsDBNull(junOrdinal))
                        {
                            KeyValuePair<string, double> junKVP = new KeyValuePair<string, double>("JUN", Convert.ToDouble(salesTrendReader.GetDecimal(junOrdinal)));
                            monthSalesList.Add(junKVP);
                        }

                        oSalesTrend.TrendData = monthSalesList.GetRange(0, CostPeriod);
                        oSalesTrend.TotalAmount = oSalesTrend.TrendData.Sum(t => t.Value);
                        lSalesTrend.Add(oSalesTrend);
                    }
                }
                return lSalesTrend.OrderByDescending(t => t.TotalAmount).Take(5).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (salesTrendReader != null && (!salesTrendReader.IsClosed))
                    salesTrendReader.Close();
            }
        }

        public List<KeyAccountEntity> GetSalesKeyAccounts(string repCode, ArgsEntity args)
        {
            DbDataReader keyAccountsReader = null;
            List<KeyAccountEntity> keyAccounts = new List<KeyAccountEntity>();
            try
            {
                //int CostPeriod = Convert.ToInt32(DataAcessService.GetOneValue(HomePageSummarySql["GetCurrentCostPeriod"]));
                
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RepCode", repCode, DbType.String);
                //paramCollection.Add("@CostPeriod", CostPeriod, DbType.Int32);
                paramCollection.Add("@Distributor", args.Originator, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                keyAccountsReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetSalesKeyAccounts"], paramCollection);

                if (keyAccountsReader.HasRows)
                {
                    int nameOrdinal = keyAccountsReader.GetOrdinal("name");

                    //int qtyOrdinal = keyAccountsReader.GetOrdinal("qty");
                    int priceOrdinal = keyAccountsReader.GetOrdinal("price");

                    KeyAccountEntity oKeyAccount = null;

                    while (keyAccountsReader.Read())
                    {
                        oKeyAccount = KeyAccountEntity.CreateObject();

                        if (!keyAccountsReader.IsDBNull(nameOrdinal)) oKeyAccount.AccountName = keyAccountsReader.GetString(nameOrdinal);
                        if (!keyAccountsReader.IsDBNull(priceOrdinal)) oKeyAccount.Amount = Convert.ToDouble(keyAccountsReader.GetDecimal(priceOrdinal));
                        //if (period.Equals("MTD"))
                        //{
                        //    if (!keyAccountsReader.IsDBNull(mtdvalueOrdinal)) oKeyAccount.Amount = Convert.ToDouble(keyAccountsReader.GetDecimal(mtdvalueOrdinal));
                        //}
                        //else
                        //{
                        //    if (!keyAccountsReader.IsDBNull(ytdvalueOrdinal)) oKeyAccount.Amount = Convert.ToDouble(keyAccountsReader.GetDecimal(ytdvalueOrdinal));
                        //}

                        keyAccounts.Add(oKeyAccount);
                    }
                }

                return keyAccounts;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (keyAccountsReader != null && (!keyAccountsReader.IsClosed))
                    keyAccountsReader.Close();
            }
        }

        public List<SalesLeaderEntity> GetSalesLeaderBoard(string businessCode, string period,ArgsEntity args)
        {
            DbDataReader salesLeaderReader = null;
            List<SalesLeaderEntity> lSalesLeader = new List<SalesLeaderEntity>();

            try
            {
                int CostPeriod = Convert.ToInt32(DataAcessService.GetOneValue(HomePageSummarySql["GetCurrentCostPeriod"]));

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Business", businessCode, DbType.String);
                paramCollection.Add("@CostPeriod", CostPeriod, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                salesLeaderReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetSalesLeaderboard"], paramCollection);

                if (salesLeaderReader.HasRows)
                {
                    int nameOrdinal = salesLeaderReader.GetOrdinal("name");
                    int mtdvalueOrdinal = salesLeaderReader.GetOrdinal("mtdvalue");
                    int ytdvalueOrdinal = salesLeaderReader.GetOrdinal("ytdvalue");
                    int mtdtonnesOrdinal = salesLeaderReader.GetOrdinal("mtdtonnes");
                    int ytdtonnesOrdinal = salesLeaderReader.GetOrdinal("ytdtonnes");
                    int totalCountOrdinal = salesLeaderReader.GetOrdinal("TotalCount");
                    


                    while (salesLeaderReader.Read())
                    {
                        SalesLeaderEntity oSalesLeader = SalesLeaderEntity.CreateObject();

                        if (!salesLeaderReader.IsDBNull(nameOrdinal)) oSalesLeader.OwnerName = salesLeaderReader.GetString(nameOrdinal);

                        if (period.Equals("MTD"))
                        {
                            if (!salesLeaderReader.IsDBNull(mtdvalueOrdinal)) oSalesLeader.Amount = Convert.ToDouble(salesLeaderReader.GetDecimal(mtdvalueOrdinal));
                            if (!salesLeaderReader.IsDBNull(mtdtonnesOrdinal)) oSalesLeader.Units = salesLeaderReader.GetDouble(mtdtonnesOrdinal);
                        }
                        else
                        {
                            if (!salesLeaderReader.IsDBNull(ytdvalueOrdinal)) oSalesLeader.Amount = Convert.ToDouble(salesLeaderReader.GetDecimal(ytdvalueOrdinal));
                            if (!salesLeaderReader.IsDBNull(ytdtonnesOrdinal)) oSalesLeader.Units = salesLeaderReader.GetDouble(ytdtonnesOrdinal);
                        }
                        if (!salesLeaderReader.IsDBNull(totalCountOrdinal)) oSalesLeader.TotalCount = salesLeaderReader.GetInt32(totalCountOrdinal);
                        
                        lSalesLeader.Add(oSalesLeader);
                    }
                }
                return lSalesLeader;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (salesLeaderReader != null && (!salesLeaderReader.IsClosed))
                    salesLeaderReader.Close();
            }
        }

        public List<KeyAccountEntity> GetTopSalesReps(ArgsEntity args)
        {
            DbDataReader keyAccountsReader = null;
            List<KeyAccountEntity> keyAccounts = new List<KeyAccountEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Distributor", args.Originator, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                keyAccountsReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetTopSalesReps"], paramCollection);

                if (keyAccountsReader.HasRows)
                {
                    int nameOrdinal = keyAccountsReader.GetOrdinal("name");

                    //int qtyOrdinal = keyAccountsReader.GetOrdinal("qty");
                    int priceOrdinal = keyAccountsReader.GetOrdinal("price");

                    KeyAccountEntity oKeyAccount = null;

                    while (keyAccountsReader.Read())
                    {
                        oKeyAccount = KeyAccountEntity.CreateObject();

                        if (!keyAccountsReader.IsDBNull(nameOrdinal)) oKeyAccount.AccountName = keyAccountsReader.GetString(nameOrdinal);
                        if (!keyAccountsReader.IsDBNull(priceOrdinal)) oKeyAccount.Amount = Convert.ToDouble(keyAccountsReader.GetDecimal(priceOrdinal));

                        keyAccounts.Add(oKeyAccount);
                    }
                }

                return keyAccounts;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (keyAccountsReader != null && (!keyAccountsReader.IsClosed))
                    keyAccountsReader.Close();
            }
        }

        public List<KeyAccountEntity> GetTopSalesDistributors(ArgsEntity args)
        {
            DbDataReader keyAccountsReader = null;
            List<KeyAccountEntity> keyAccounts = new List<KeyAccountEntity>();
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                keyAccountsReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetTopSalesDistributors"], paramCollection);

                if (keyAccountsReader.HasRows)
                {
                    int nameOrdinal = keyAccountsReader.GetOrdinal("name");

                    //int qtyOrdinal = keyAccountsReader.GetOrdinal("qty");
                    int priceOrdinal = keyAccountsReader.GetOrdinal("price");

                    KeyAccountEntity oKeyAccount = null;

                    while (keyAccountsReader.Read())
                    {
                        oKeyAccount = KeyAccountEntity.CreateObject();

                        if (!keyAccountsReader.IsDBNull(nameOrdinal)) oKeyAccount.AccountName = keyAccountsReader.GetString(nameOrdinal);
                        if (!keyAccountsReader.IsDBNull(priceOrdinal)) oKeyAccount.Amount = Convert.ToDouble(keyAccountsReader.GetDecimal(priceOrdinal));

                        keyAccounts.Add(oKeyAccount);
                    }
                }

                return keyAccounts;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (keyAccountsReader != null && (!keyAccountsReader.IsClosed))
                    keyAccountsReader.Close();
            }
        }

        public List<KeyAccountEntity> GetTopSalesProducts(ArgsEntity args)
        {
            DbDataReader keyAccountsReader = null;
            List<KeyAccountEntity> keyAccounts = new List<KeyAccountEntity>();
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Distributor", args.Originator, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                keyAccountsReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetTopSalesProducts"], paramCollection);

                if (keyAccountsReader.HasRows)
                {
                    int nameOrdinal = keyAccountsReader.GetOrdinal("name");

                    //int qtyOrdinal = keyAccountsReader.GetOrdinal("qty");
                    int priceOrdinal = keyAccountsReader.GetOrdinal("price");

                    KeyAccountEntity oKeyAccount = null;

                    while (keyAccountsReader.Read())
                    {
                        oKeyAccount = KeyAccountEntity.CreateObject();

                        if (!keyAccountsReader.IsDBNull(nameOrdinal)) oKeyAccount.AccountName = keyAccountsReader.GetString(nameOrdinal);
                        if (!keyAccountsReader.IsDBNull(priceOrdinal)) oKeyAccount.Amount = Convert.ToDouble(keyAccountsReader.GetDecimal(priceOrdinal));

                        keyAccounts.Add(oKeyAccount);
                    }
                }

                return keyAccounts;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (keyAccountsReader != null && (!keyAccountsReader.IsClosed))
                    keyAccountsReader.Close();
            }
        }

        public List<KeyAccountEntity> GetTopSalesMarkets(ArgsEntity args)
        {
            DbDataReader keyAccountsReader = null;
            List<KeyAccountEntity> keyAccounts = new List<KeyAccountEntity>();
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                keyAccountsReader = this.DataAcessService.ExecuteQuery(HomePageSummarySql["GetTopSalesMarkets"], paramCollection);

                if (keyAccountsReader.HasRows)
                {
                    int nameOrdinal = keyAccountsReader.GetOrdinal("name");

                    //int qtyOrdinal = keyAccountsReader.GetOrdinal("qty");
                    int priceOrdinal = keyAccountsReader.GetOrdinal("price");

                    KeyAccountEntity oKeyAccount = null;

                    while (keyAccountsReader.Read())
                    {
                        oKeyAccount = KeyAccountEntity.CreateObject();

                        if (!keyAccountsReader.IsDBNull(nameOrdinal)) oKeyAccount.AccountName = keyAccountsReader.GetString(nameOrdinal);
                        if (!keyAccountsReader.IsDBNull(priceOrdinal)) oKeyAccount.Amount = Convert.ToDouble(keyAccountsReader.GetDecimal(priceOrdinal));

                        keyAccounts.Add(oKeyAccount);
                    }
                }

                return keyAccounts;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (keyAccountsReader != null && (!keyAccountsReader.IsClosed))
                    keyAccountsReader.Close();
            }
        }
    }
}
