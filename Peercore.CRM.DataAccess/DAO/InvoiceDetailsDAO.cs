﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class InvoiceDetailsDAO : BaseDAO
    {
         private DbSqlAdapter InvoiceDetailSql { get; set; }

        private void RegisterSql()
        {
            this.InvoiceDetailSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.InvoiceHeaderSql.xml", ApplicationService.Instance.DbProvider);
        }

        public InvoiceDetailsDAO()
        {
            RegisterSql();
        }

        public InvoiceDetailsDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public InvoiceDetailEntity CreateObject()
        {
            return InvoiceDetailEntity.CreateObject();
        }

        public List<InvoiceDetailEntity> GetInvoiceDetailsByInvoiceId(int invoiceId, string SQuantitytype)
        {
            //DbDataReader drInvoiceHeader = null;

            //try
            //{
            //    List<InvoiceDetailEntity> InvoiceDetailList = new List<InvoiceDetailEntity>();

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

            //    drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceDetailSql["GetInvoiceDetailsByInvoiceId"], paramCollection);

            //    if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
            //    {
            //        int idOrdinal = drInvoiceHeader.GetOrdinal("id");
            //        int invoiceIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
            //        int productIdOrdinal = drInvoiceHeader.GetOrdinal("product_id");
            //        int quantityOrdinal = drInvoiceHeader.GetOrdinal("quantity");
            //        int discountOrdinal = drInvoiceHeader.GetOrdinal("discount");
            //        int discountValueOrdinal = drInvoiceHeader.GetOrdinal("discount_value");
            //        int subTotalOrdinal = drInvoiceHeader.GetOrdinal("sub_total");
            //        int statusOrdinal = drInvoiceHeader.GetOrdinal("status");
            //        int itemPriceOrdinal = drInvoiceHeader.GetOrdinal("item_price");
            //        int nameOrdinal = drInvoiceHeader.GetOrdinal("name");

            //        InvoiceDetailEntity invoiceDetailEntity = null;

            //        while (drInvoiceHeader.Read())
            //        {
            //            invoiceDetailEntity = CreateObject();

            //            if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceDetailEntity.InvoiceDetailId = drInvoiceHeader.GetInt32(idOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(invoiceIdOrdinal)) invoiceDetailEntity.InvoiceId = drInvoiceHeader.GetInt32(invoiceIdOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(productIdOrdinal)) invoiceDetailEntity.ProductId = drInvoiceHeader.GetInt32(productIdOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(quantityOrdinal)) invoiceDetailEntity.Quantity = drInvoiceHeader.GetDouble(quantityOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(discountOrdinal)) invoiceDetailEntity.Discount = drInvoiceHeader.GetDouble(discountOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(discountValueOrdinal)) invoiceDetailEntity.DiscountValue = (double)drInvoiceHeader.GetDecimal(discountValueOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(subTotalOrdinal)) invoiceDetailEntity.SubTotal = (double)drInvoiceHeader.GetDecimal(subTotalOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(statusOrdinal)) invoiceDetailEntity.Status = drInvoiceHeader.GetString(statusOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(itemPriceOrdinal)) invoiceDetailEntity.ItemPrice = drInvoiceHeader.GetDouble(itemPriceOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(statusOrdinal)) invoiceDetailEntity.ProductName = drInvoiceHeader.GetString(nameOrdinal);

            //            InvoiceDetailList.Add(invoiceDetailEntity);
            //        }
            //    }

            //    return InvoiceDetailList;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drInvoiceHeader != null)
            //        drInvoiceHeader.Close();
            //}

            //*************************Add By Irosh Fernando 2018/10/3************************************

            InvoiceDetailEntity invoiceDetailEntity = null;
            DataTable objDS = new DataTable();
            List<InvoiceDetailEntity> InvoiceDetailList = new List<InvoiceDetailEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetInvoiceDetailsByInvoiceId";

                    objCommand.Parameters.AddWithValue("@InvoiceId", invoiceId);
                    objCommand.Parameters.AddWithValue("@QuantityType", SQuantitytype);


                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            invoiceDetailEntity = CreateObject();
                            if (item["id"] != DBNull.Value) invoiceDetailEntity.InvoiceDetailId = Convert.ToInt32(item["id"]);
                            if (item["invoice_id"] != DBNull.Value) invoiceDetailEntity.InvoiceId = Convert.ToInt32(item["invoice_id"]);
                            if (item["product_id"] != DBNull.Value) invoiceDetailEntity.ProductId = Convert.ToInt32(item["product_id"]);
                            if (item["quantity"] != DBNull.Value) invoiceDetailEntity.Quantity = Convert.ToDouble(item["quantity"]);
                            if (item["discount"] != DBNull.Value) invoiceDetailEntity.Discount = Convert.ToDouble(item["discount"]);
                            if (item["discount_value"] != DBNull.Value) invoiceDetailEntity.DiscountValue = Convert.ToDouble(item["discount_value"]);
                            if (item["sub_total"] != DBNull.Value) invoiceDetailEntity.SubTotal = Convert.ToDouble(item["sub_total"]);
                            if (item["status"] != DBNull.Value) invoiceDetailEntity.Status = item["status"].ToString();
                            if (item["item_price"] != DBNull.Value) invoiceDetailEntity.ItemPrice = Convert.ToDouble(item["item_price"]);
                            if (item["name"] != DBNull.Value) invoiceDetailEntity.ProductName = item["name"].ToString();
                            //if (item["qty_casses"] != DBNull.Value) invoiceDetailEntity.QtyCasess = Convert.ToDouble(item["qty_casses"]);
                            //if (item["qty_tonnage"] != DBNull.Value) invoiceDetailEntity.QtyTonnage = Convert.ToDouble(item["qty_tonnage"]);




                            InvoiceDetailList.Add(invoiceDetailEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return InvoiceDetailList;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public List<InvoiceDetailEntity> GetInvoiceDetailsByInvoiceId(int ivceId)
        {
            throw new NotImplementedException();
        }

        public bool InsertInvoiceDetails(InvoiceDetailEntity invoiceDetailEntity, string status)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@InvoiceId", invoiceDetailEntity.InvoiceId, DbType.Int32);
                paramCollection.Add("@Productid", invoiceDetailEntity.ProductId, DbType.Int32);
                paramCollection.Add("@Quantity", invoiceDetailEntity.Quantity, DbType.Double);

                paramCollection.Add("@Discount", invoiceDetailEntity.Discount, DbType.Double);
                paramCollection.Add("@DiscountValue", invoiceDetailEntity.DiscountValue, DbType.String);
                paramCollection.Add("@SubTotal", invoiceDetailEntity.SubTotal, DbType.Double);

                paramCollection.Add("@ItemPrice", invoiceDetailEntity.ItemPrice, DbType.String);
                paramCollection.Add("@Status", status, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceDetailSql["InsertInvoiceDetails"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool DeleteInvoiceDetails(int invoiceId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceDetailSql["DeleteInvoiceDetails"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool DeleteInvoiceDetailsPhysical(int invoiceId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceDetailSql["DeleteInvoiceDetailsPhysical"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        //Add By Irosh for Add Features from Generic Vertion to Perfetti 2017/02/22
        ///////////////////////////////////////////////////////////////////////////
        public bool ReturnInvoiceDetails(int invId, ReturnInvoiceDetailsEntity invoiceDetailsEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@returnid", invoiceDetailsEntity.returnid, DbType.Int32);
                paramCollection.Add("@invoiceid", invId, DbType.Int32);
                paramCollection.Add("@productid", invoiceDetailsEntity.productid, DbType.Int32);
                paramCollection.Add("@itemprice", invoiceDetailsEntity.itemprice, DbType.Double);
                paramCollection.Add("@qty", invoiceDetailsEntity.qty, DbType.Double);
                paramCollection.Add("@total", invoiceDetailsEntity.total, DbType.Double);
                paramCollection.Add("@usable", invoiceDetailsEntity.usable, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceDetailSql["InsertReturnInvoiceDetails"], paramCollection);
                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool ReturnInvoiceLine(int invId, ReturnInvoiceLineEntity ReturnInvoiceLineEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@returnid", ReturnInvoiceLineEntity.returnid, DbType.Int32);
                paramCollection.Add("@invoiceid", invId, DbType.Int32);
                paramCollection.Add("@return_amt", ReturnInvoiceLineEntity.return_amt, DbType.Double);
                paramCollection.Add("@return_val", ReturnInvoiceLineEntity.return_val, DbType.Double);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceDetailSql["InsertReturnInvoiceLine"], paramCollection);
                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public List<ReturnInvoiceDetailsEntity> GetReturnInvoiceDetailsByReturnId(int returnId)
        {
            DbDataReader drReturnInvoiceDetatils = null;

            try
            {
                List<ReturnInvoiceDetailsEntity> InvoiceDetailList = new List<ReturnInvoiceDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ReturnId", returnId, DbType.Int32);

                drReturnInvoiceDetatils = this.DataAcessService.ExecuteQuery(InvoiceDetailSql["GetReturnInvoiceDetailsByReturnId"], paramCollection);

                if (drReturnInvoiceDetatils != null && drReturnInvoiceDetatils.HasRows)
                {
                    int idOrdinal = drReturnInvoiceDetatils.GetOrdinal("id");
                    int returnidOrdinal = drReturnInvoiceDetatils.GetOrdinal("returnid");
                    int invoicenoOrdinal = drReturnInvoiceDetatils.GetOrdinal("invoiceno");
                    int productidOrdinal = drReturnInvoiceDetatils.GetOrdinal("productid");
                    int itempriceOrdinal = drReturnInvoiceDetatils.GetOrdinal("itemprice");
                    int qtyOrdinal = drReturnInvoiceDetatils.GetOrdinal("qty");
                    int totalOrdinal = drReturnInvoiceDetatils.GetOrdinal("total");
                    int usableOrdinal = drReturnInvoiceDetatils.GetOrdinal("usable");

                    ReturnInvoiceDetailsEntity invoiceDetailEntity = null;

                    while (drReturnInvoiceDetatils.Read())
                    {
                        invoiceDetailEntity = ReturnInvoiceDetailsEntity.CreateObject();

                        if (!drReturnInvoiceDetatils.IsDBNull(idOrdinal)) invoiceDetailEntity.Id = drReturnInvoiceDetatils.GetInt32(idOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(returnidOrdinal)) invoiceDetailEntity.returnid = drReturnInvoiceDetatils.GetInt32(returnidOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(invoicenoOrdinal)) invoiceDetailEntity.invoiceno = drReturnInvoiceDetatils.GetString(invoicenoOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(productidOrdinal)) invoiceDetailEntity.productid = drReturnInvoiceDetatils.GetInt32(productidOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(itempriceOrdinal)) invoiceDetailEntity.itemprice = drReturnInvoiceDetatils.GetDouble(itempriceOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(qtyOrdinal)) invoiceDetailEntity.qty = drReturnInvoiceDetatils.GetDouble(qtyOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(totalOrdinal)) invoiceDetailEntity.total = drReturnInvoiceDetatils.GetDouble(totalOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(usableOrdinal)) invoiceDetailEntity.usable = drReturnInvoiceDetatils.GetString(usableOrdinal);

                        InvoiceDetailList.Add(invoiceDetailEntity);
                    }
                }

                return InvoiceDetailList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drReturnInvoiceDetatils != null)
                    drReturnInvoiceDetatils.Close();
            }
        }

        public List<ReturnInvoiceDetailsEntity> GetReturnInvoiceDetailsByReturnIdAndInvoiceId(int returnId, int invoiceId)
        {
            DbDataReader drReturnInvoiceDetatils = null;

            try
            {
                List<ReturnInvoiceDetailsEntity> InvoiceDetailList = new List<ReturnInvoiceDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ReturnId", returnId, DbType.Int32);
                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                drReturnInvoiceDetatils = this.DataAcessService.ExecuteQuery(InvoiceDetailSql["GetReturnInvoiceDetailsByReturnIdAndInvoiceId"], paramCollection);

                if (drReturnInvoiceDetatils != null && drReturnInvoiceDetatils.HasRows)
                {
                    int idOrdinal = drReturnInvoiceDetatils.GetOrdinal("id");
                    int returnidOrdinal = drReturnInvoiceDetatils.GetOrdinal("returnid");
                    int invoicenoOrdinal = drReturnInvoiceDetatils.GetOrdinal("invoiceno");
                    int productidOrdinal = drReturnInvoiceDetatils.GetOrdinal("productid");
                    int itempriceOrdinal = drReturnInvoiceDetatils.GetOrdinal("itemprice");
                    int qtyOrdinal = drReturnInvoiceDetatils.GetOrdinal("qty");
                    int totalOrdinal = drReturnInvoiceDetatils.GetOrdinal("total");
                    int usableOrdinal = drReturnInvoiceDetatils.GetOrdinal("usable");

                    ReturnInvoiceDetailsEntity invoiceDetailEntity = null;

                    while (drReturnInvoiceDetatils.Read())
                    {
                        invoiceDetailEntity = ReturnInvoiceDetailsEntity.CreateObject();

                        if (!drReturnInvoiceDetatils.IsDBNull(idOrdinal)) invoiceDetailEntity.Id = drReturnInvoiceDetatils.GetInt32(idOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(returnidOrdinal)) invoiceDetailEntity.returnid = drReturnInvoiceDetatils.GetInt32(returnidOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(invoicenoOrdinal)) invoiceDetailEntity.invoiceno = drReturnInvoiceDetatils.GetString(invoicenoOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(productidOrdinal)) invoiceDetailEntity.productid = drReturnInvoiceDetatils.GetInt32(productidOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(itempriceOrdinal)) invoiceDetailEntity.itemprice = drReturnInvoiceDetatils.GetDouble(itempriceOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(qtyOrdinal)) invoiceDetailEntity.qty = drReturnInvoiceDetatils.GetDouble(qtyOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(totalOrdinal)) invoiceDetailEntity.total = drReturnInvoiceDetatils.GetDouble(totalOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(usableOrdinal)) invoiceDetailEntity.usable = drReturnInvoiceDetatils.GetString(usableOrdinal);

                        InvoiceDetailList.Add(invoiceDetailEntity);
                    }
                }

                return InvoiceDetailList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drReturnInvoiceDetatils != null)
                    drReturnInvoiceDetatils.Close();
            }
        }

        public List<ReturnInvoiceLineEntity> GetReturnInvoiceLinesByReturnId(int returnId)
        {
            DbDataReader drReturnInvoiceDetatils = null;

            try
            {
                List<ReturnInvoiceLineEntity> InvoiceDetailList = new List<ReturnInvoiceLineEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ReturnId", returnId, DbType.Int32);

                drReturnInvoiceDetatils = this.DataAcessService.ExecuteQuery(InvoiceDetailSql["GetReturnInvoiceLinesByReturnId"], paramCollection);

                if (drReturnInvoiceDetatils != null && drReturnInvoiceDetatils.HasRows)
                {
                    int idOrdinal = drReturnInvoiceDetatils.GetOrdinal("id");
                    int returnidOrdinal = drReturnInvoiceDetatils.GetOrdinal("returnid");
                    int invoicenoOrdinal = drReturnInvoiceDetatils.GetOrdinal("invoiceno");
                    int return_amtOrdinal = drReturnInvoiceDetatils.GetOrdinal("return_amt");
                    int return_valOrdinal = drReturnInvoiceDetatils.GetOrdinal("return_val");

                    ReturnInvoiceLineEntity invoiceDetailEntity = null;

                    while (drReturnInvoiceDetatils.Read())
                    {
                        invoiceDetailEntity = ReturnInvoiceLineEntity.CreateObject();

                        if (!drReturnInvoiceDetatils.IsDBNull(idOrdinal)) invoiceDetailEntity.Id = drReturnInvoiceDetatils.GetInt32(idOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(returnidOrdinal)) invoiceDetailEntity.returnid = drReturnInvoiceDetatils.GetInt32(returnidOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(invoicenoOrdinal)) invoiceDetailEntity.invoiceno = drReturnInvoiceDetatils.GetString(invoicenoOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(return_amtOrdinal)) invoiceDetailEntity.return_amt = drReturnInvoiceDetatils.GetDouble(return_amtOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(return_valOrdinal)) invoiceDetailEntity.return_val = drReturnInvoiceDetatils.GetDouble(return_valOrdinal);

                        InvoiceDetailList.Add(invoiceDetailEntity);
                    }
                }

                return InvoiceDetailList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drReturnInvoiceDetatils != null)
                    drReturnInvoiceDetatils.Close();
            }
        }

        public List<ReturnInvoiceLineEntity> GetReturnInvoiceLinesByReturnIdAndInvoiceId(int returnId, int invoiceId)
        {
            DbDataReader drReturnInvoiceDetatils = null;

            try
            {
                List<ReturnInvoiceLineEntity> InvoiceDetailList = new List<ReturnInvoiceLineEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ReturnId", returnId, DbType.Int32);
                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                drReturnInvoiceDetatils = this.DataAcessService.ExecuteQuery(InvoiceDetailSql["GetReturnInvoiceLinesByReturnIdAndInvoiceId"], paramCollection);

                if (drReturnInvoiceDetatils != null && drReturnInvoiceDetatils.HasRows)
                {
                    int idOrdinal = drReturnInvoiceDetatils.GetOrdinal("id");
                    int returnidOrdinal = drReturnInvoiceDetatils.GetOrdinal("returnid");
                    int invoicenoOrdinal = drReturnInvoiceDetatils.GetOrdinal("invoiceno");
                    int return_amtOrdinal = drReturnInvoiceDetatils.GetOrdinal("return_amt");
                    int return_valOrdinal = drReturnInvoiceDetatils.GetOrdinal("return_val");

                    ReturnInvoiceLineEntity invoiceDetailEntity = null;

                    while (drReturnInvoiceDetatils.Read())
                    {
                        invoiceDetailEntity = ReturnInvoiceLineEntity.CreateObject();

                        if (!drReturnInvoiceDetatils.IsDBNull(idOrdinal)) invoiceDetailEntity.Id = drReturnInvoiceDetatils.GetInt32(idOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(returnidOrdinal)) invoiceDetailEntity.returnid = drReturnInvoiceDetatils.GetInt32(returnidOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(invoicenoOrdinal)) invoiceDetailEntity.invoiceno = drReturnInvoiceDetatils.GetString(invoicenoOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(return_amtOrdinal)) invoiceDetailEntity.return_amt = drReturnInvoiceDetatils.GetDouble(return_amtOrdinal);
                        if (!drReturnInvoiceDetatils.IsDBNull(return_valOrdinal)) invoiceDetailEntity.return_val = drReturnInvoiceDetatils.GetDouble(return_valOrdinal);

                        InvoiceDetailList.Add(invoiceDetailEntity);
                    }
                }

                return InvoiceDetailList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drReturnInvoiceDetatils != null)
                    drReturnInvoiceDetatils.Close();
            }
        }
    }
}
