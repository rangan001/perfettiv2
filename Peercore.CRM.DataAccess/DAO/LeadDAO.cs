﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class LeadDAO : BaseDAO
    {
        private DbSqlAdapter LeadSql { get; set; }

        private void RegisterSql()
        {
            this.LeadSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.LeadSql.xml", ApplicationService.Instance.DbProvider);
        }

        public LeadDAO()
        {
            RegisterSql();
        }

        public LeadDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public LeadEntity CreateObject()
        {
            return LeadEntity.CreateObject();
        }

        public LeadEntity CreateObject(int entityId)
        {
            return LeadEntity.CreateObject(entityId);
        }

        public bool UpdateLead(LeadEntity lead)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                 
                paramCollection.Add("@leadName", ValidateString(lead.LeadName), DbType.String);
                paramCollection.Add("@originator", lead.Originator, DbType.String);
                paramCollection.Add("@leadSource", lead.LeadSource, DbType.String);
                paramCollection.Add("@company", ValidateString(lead.Company), DbType.String);
                paramCollection.Add("@business", ValidateString(lead.Business), DbType.String);
                paramCollection.Add("@rating", lead.Rating, DbType.Int32);
                paramCollection.Add("@website", lead.Website, DbType.String);
                paramCollection.Add("@industry", ValidateString(lead.Industry), DbType.String);
                paramCollection.Add("@noOfEmployees", lead.NoOfEmployees, DbType.Int32);
                paramCollection.Add("@annualRevenue", lead.AnnualRevenue, DbType.Int32);
                paramCollection.Add("@leadStatus", string.IsNullOrEmpty(lead.LeadStatus) ? "" : lead.LeadStatus, DbType.String);
                paramCollection.Add("@telephone", ValidateString(lead.Telephone), DbType.String);
                paramCollection.Add("@fax", ValidateString(lead.Fax), DbType.String);
                paramCollection.Add("@mobile", ValidateString(lead.Mobile), DbType.String);
                paramCollection.Add("@emailAddress", lead.EmailAddress, DbType.String);
                paramCollection.Add("@address", ValidateString(lead.Address), DbType.String);
                paramCollection.Add("@city",  ValidateString(lead.City), DbType.String);
                paramCollection.Add("@state",  ValidateString(lead.State), DbType.String);
                paramCollection.Add("@postcode",  ValidateString(lead.PostCode), DbType.String);
                paramCollection.Add("@referredBy",  ValidateString(lead.ReferredBy), DbType.String);
                paramCollection.Add("@probability", lead.Probability, DbType.Int32);
                paramCollection.Add("@description", ValidateString(lead.Description), DbType.String);
                paramCollection.Add("@prefContact", ValidateString(lead.PreferredContact), DbType.String);
                paramCollection.Add("@busPotential", ValidateString(lead.BusinessPotential), DbType.String);
                paramCollection.Add("@country", string.IsNullOrEmpty(lead.Country) ? "" : lead.Country, DbType.String);
                paramCollection.Add("@lastModifiedBy", lead.LastModifiedBy, DbType.String);
                paramCollection.Add("@lastModifiedDate", lead.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@leadStageId", lead.LeadStageID, DbType.Int32);
                paramCollection.Add("@custCode", lead.CustCode, DbType.String);
                paramCollection.Add("@litersBy", lead.LitersBy, DbType.String);
                paramCollection.Add("@potentialLiters", lead.PotentialLiters, DbType.Int32);
                paramCollection.Add("@repGroupId", lead.RepGroupID, DbType.Int32);
                paramCollection.Add("@leadId", lead.LeadID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(LeadSql["UpdateLead"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public bool UnassignedRoutefromRep(int RouteId, string RepCode)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@RouteId", RouteId, DbType.String);
                paramCollection.Add("@RepCode", RepCode, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(LeadSql["UnassignedRoutefromRep"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public bool InsertLead(LeadEntity lead, out int leadId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                lead.LeadID = Convert.ToInt32(DataAcessService.GetOneValue(LeadSql["GetNextLeadID"]));   // Get Next ID
                leadId = lead.LeadID;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@leadName", ValidateString(lead.LeadName), DbType.String);
                paramCollection.Add("@originator", lead.Originator, DbType.String);
                paramCollection.Add("@leadSource", lead.LeadSource, DbType.String);
                paramCollection.Add("@company", ValidateString(lead.Company), DbType.String);
                paramCollection.Add("@business", ValidateString(lead.Business), DbType.String);
                paramCollection.Add("@rating", lead.Rating, DbType.Int32);
                paramCollection.Add("@website", lead.Website, DbType.String);
                paramCollection.Add("@industry", ValidateString(lead.Industry), DbType.String);
                paramCollection.Add("@noOfEmployees", lead.NoOfEmployees, DbType.Int32);
                paramCollection.Add("@annualRevenue", lead.AnnualRevenue, DbType.Int32);
                paramCollection.Add("@leadStatus", string.IsNullOrEmpty(lead.LeadStatus) ? "" : lead.LeadStatus, DbType.String);
                paramCollection.Add("@telephone", ValidateString(lead.Telephone), DbType.String);
                paramCollection.Add("@fax", ValidateString(lead.Fax), DbType.String);
                paramCollection.Add("@mobile", ValidateString(lead.Mobile), DbType.String);
                paramCollection.Add("@emailAddress", lead.EmailAddress, DbType.String);
                paramCollection.Add("@address", ValidateString(lead.Address), DbType.String);
                paramCollection.Add("@city", ValidateString(lead.City), DbType.String);
                paramCollection.Add("@state", ValidateString(lead.State), DbType.String);
                paramCollection.Add("@postcode", ValidateString(lead.PostCode), DbType.String);
                paramCollection.Add("@referredBy", ValidateString(lead.ReferredBy), DbType.String);
                paramCollection.Add("@probability", lead.Probability, DbType.Int32);
                paramCollection.Add("@description", ValidateString(lead.Description), DbType.String);
                paramCollection.Add("@prefContact", ValidateString(lead.PreferredContact), DbType.String);
                paramCollection.Add("@busPotential", ValidateString(lead.BusinessPotential), DbType.String);
                paramCollection.Add("@country", string.IsNullOrEmpty(lead.Country) ? "" : lead.Country, DbType.String);
                paramCollection.Add("@lastModifiedBy", ValidateString(lead.LastModifiedBy), DbType.String);
                paramCollection.Add("@lastModifiedDate", lead.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@leadStageId", lead.LeadStageID, DbType.Int32);
                paramCollection.Add("@custCode", string.IsNullOrEmpty(lead.CustCode) ? "" : lead.CustCode, DbType.String);
                paramCollection.Add("@litersBy", ValidateString(lead.LitersBy), DbType.String);
                paramCollection.Add("@potentialLiters", lead.PotentialLiters, DbType.Int32);
                paramCollection.Add("@repGroupId", lead.RepGroupID, DbType.Int32);
                paramCollection.Add("@leadId", lead.LeadID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(LeadSql["InsertLead"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public LeadEntity GetLead(string sLeadID)
        {
            DbDataReader idrLead = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadId", sLeadID, DbType.Int32);
                idrLead = DataAcessService.ExecuteQuery(LeadSql["GetLeadDetails"], paramCollection);

                LeadEntity lead = null;

                if (idrLead != null && idrLead.HasRows)
                {
                    int leadIdOrdinal = idrLead.GetOrdinal("lead_id");
                    int leadNameOrdinal = idrLead.GetOrdinal("lead_name");
                    int leadSourceOrdinal = idrLead.GetOrdinal("lead_source");
                    int originatorOrdinal = idrLead.GetOrdinal("originator");
                    int telephoneOrdinal = idrLead.GetOrdinal("telephone");
                    int faxOrdinal = idrLead.GetOrdinal("fax");
                    int mobileOrdinal = idrLead.GetOrdinal("mobile");
                    int emailAddressOrdinal = idrLead.GetOrdinal("email_address");
                    int annualRevenueOrdinal = idrLead.GetOrdinal("annual_revenue");
                    int businessOrdinal = idrLead.GetOrdinal("Business");
                    int industryOrdinal = idrLead.GetOrdinal("industry");
                    int noOfEmployeesOrdinal = idrLead.GetOrdinal("no_of_employees");
                    int ratingOrdinal = idrLead.GetOrdinal("rating");
                    int websiteOrdinal = idrLead.GetOrdinal("website");
                    int CompanyOrdinal = idrLead.GetOrdinal("Company");
                    int addressOrdinal = idrLead.GetOrdinal("address");
                    int cityOrdinal = idrLead.GetOrdinal("city");
                    int stateOrdinal = idrLead.GetOrdinal("state");
                    int descriptionOrdinal = idrLead.GetOrdinal("description");
                    int postcodeOrdinal = idrLead.GetOrdinal("postcode");
                    int busPotentialOrdinal = idrLead.GetOrdinal("bus_potential");
                    int referredByOrdinal = idrLead.GetOrdinal("referred_by");
                    int countryOrdinal = idrLead.GetOrdinal("country");
                    int prefContactOrdinal = idrLead.GetOrdinal("pref_contact");
                    int createdByOrdinal = idrLead.GetOrdinal("created_by");
                    int lastModifiedByOrdinal = idrLead.GetOrdinal("last_modified_by");
                    int leadStageIdOrdinal = idrLead.GetOrdinal("lead_stage_id");
                    int custCodeOrdinal = idrLead.GetOrdinal("cust_code");
                    int createdDateOrdinal = idrLead.GetOrdinal("created_date");
                    int lastModifiedDateOrdinal = idrLead.GetOrdinal("last_modified_date");
                    int lastCalledDateOrdinal = idrLead.GetOrdinal("last_called_date");
                    int litersByOrdinal = idrLead.GetOrdinal("liters_by");
                    int potentialLitersOrdinal = idrLead.GetOrdinal("potential_liters");
                    int repGroupIdOrdinal = idrLead.GetOrdinal("rep_group_id");
                    int repGroupNameOrdinal = idrLead.GetOrdinal("rep_group_name");
                    int delFlagOrdinal = idrLead.GetOrdinal("del_flag");

                    if (idrLead.Read())
                    {
                        lead = LeadEntity.CreateObject();

                        if (!idrLead.IsDBNull(leadIdOrdinal)) lead.LeadID = idrLead.GetInt32(leadIdOrdinal);
                        if (!idrLead.IsDBNull(leadNameOrdinal)) lead.LeadName = idrLead.GetString(leadNameOrdinal);
                        if (!idrLead.IsDBNull(leadSourceOrdinal)) lead.LeadSource = idrLead.GetString(leadSourceOrdinal);
                        if (!idrLead.IsDBNull(originatorOrdinal)) lead.Originator = idrLead.GetString(originatorOrdinal);
                        if (!idrLead.IsDBNull(telephoneOrdinal)) lead.Telephone = idrLead.GetString(telephoneOrdinal);
                        if (!idrLead.IsDBNull(faxOrdinal)) lead.Fax = idrLead.GetString(faxOrdinal);
                        if (!idrLead.IsDBNull(mobileOrdinal)) lead.Mobile = idrLead.GetString(mobileOrdinal);
                        if (!idrLead.IsDBNull(emailAddressOrdinal)) lead.EmailAddress = idrLead.GetString(emailAddressOrdinal);
                        if (!idrLead.IsDBNull(annualRevenueOrdinal)) lead.AnnualRevenue = idrLead.GetDouble(annualRevenueOrdinal);
                        if (!idrLead.IsDBNull(businessOrdinal)) lead.Business = idrLead.GetString(businessOrdinal);
                        if (!idrLead.IsDBNull(industryOrdinal)) lead.Industry = idrLead.GetString(industryOrdinal);
                        if (!idrLead.IsDBNull(noOfEmployeesOrdinal)) lead.NoOfEmployees = idrLead.GetInt32(noOfEmployeesOrdinal);
                        if (!idrLead.IsDBNull(ratingOrdinal)) lead.Rating = idrLead.GetInt32(ratingOrdinal);
                        if (!idrLead.IsDBNull(websiteOrdinal)) lead.Website = idrLead.GetString(websiteOrdinal);
                        if (!idrLead.IsDBNull(CompanyOrdinal)) lead.Company = idrLead.GetString(CompanyOrdinal);
                        if (!idrLead.IsDBNull(addressOrdinal)) lead.Address = idrLead.GetString(addressOrdinal);
                        if (!idrLead.IsDBNull(cityOrdinal)) lead.City = idrLead.GetString(cityOrdinal);
                        if (!idrLead.IsDBNull(stateOrdinal)) lead.State = idrLead.GetString(stateOrdinal);
                        if (!idrLead.IsDBNull(descriptionOrdinal)) lead.Description = idrLead.GetString(descriptionOrdinal);
                        if (!idrLead.IsDBNull(postcodeOrdinal)) lead.PostCode = idrLead.GetString(postcodeOrdinal);
                        if (!idrLead.IsDBNull(busPotentialOrdinal)) lead.BusinessPotential = idrLead.GetString(busPotentialOrdinal);
                        if (!idrLead.IsDBNull(referredByOrdinal)) lead.ReferredBy = idrLead.GetString(referredByOrdinal);
                        if (!idrLead.IsDBNull(countryOrdinal)) lead.Country = idrLead.GetString(countryOrdinal);
                        if (!idrLead.IsDBNull(prefContactOrdinal)) lead.PreferredContact = idrLead.GetString(prefContactOrdinal);
                        if (!idrLead.IsDBNull(createdByOrdinal)) lead.CreatedBy = idrLead.GetString(createdByOrdinal);
                        if (!idrLead.IsDBNull(lastModifiedByOrdinal)) lead.LastModifiedBy = idrLead.GetString(lastModifiedByOrdinal);
                        if (!idrLead.IsDBNull(leadStageIdOrdinal)) lead.LeadStageID = idrLead.GetInt32(leadStageIdOrdinal);
                        if (!idrLead.IsDBNull(custCodeOrdinal)) lead.CustCode = idrLead.GetString(custCodeOrdinal);
                        if (!idrLead.IsDBNull(createdDateOrdinal)) lead.CreatedDate = idrLead.GetDateTime(createdDateOrdinal).ToLocalTime();
                        if (!idrLead.IsDBNull(lastModifiedDateOrdinal)) lead.LastModifiedDate = idrLead.GetDateTime(lastModifiedDateOrdinal).ToLocalTime();
                        if (!idrLead.IsDBNull(lastCalledDateOrdinal)) lead.LastCalledDate = idrLead.GetDateTime(lastCalledDateOrdinal).ToLocalTime();
                        if (!idrLead.IsDBNull(litersByOrdinal)) lead.LitersBy = idrLead.GetString(litersByOrdinal);
                        if (!idrLead.IsDBNull(potentialLitersOrdinal)) lead.PotentialLiters = idrLead.GetDouble(potentialLitersOrdinal);
                        if (!idrLead.IsDBNull(repGroupIdOrdinal)) lead.RepGroupID = idrLead.GetInt32(repGroupIdOrdinal);
                        if (!idrLead.IsDBNull(repGroupNameOrdinal)) lead.RepGroupName = idrLead.GetString(repGroupNameOrdinal);
                        if (!idrLead.IsDBNull(delFlagOrdinal)) lead.DelFalg = idrLead.GetString(delFlagOrdinal);
                    }

                }

                return lead;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrLead != null)
                    idrLead.Close();
            }
        }

        //public string UpdatePipelineStageQuery(int iLeadID, int iPipelineStageID)
        //{
        //    try
        //    {
        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@LeadId", iLeadID, DbType.Int32);
        //        paramCollection.Add("@LeadStageid", iPipelineStageID, DbType.Int32);

        //        return (string)DataAcessService.GetOneValue(LeadSql["UpdatePipelineStageID"],paramCollection);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        public string UpdatePipelineStageQuery(int iLeadID, int iPipelineStageID)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@leadStageId", iPipelineStageID, DbType.Int32);
                paramCollection.Add("@leadId", iLeadID, DbType.Int32);
                paramCollection.Add("@isExec", 0, DbType.Boolean);
                paramCollection.Add("@sql", "", DbType.String);

                return this.DataAcessService.GetOneValue(LeadSql["UpdatePipelineStageID"], paramCollection).ToString();
                //this.DataAcessService.ExecuteQuery(, paramCollection);

                //return paramCollection.Find(s => s.Name == "@sql").Value.ToString();

            }
            catch
            {
                throw;
            }
        }

        
        //public List<LeadCustomerViewEntity> GetLeadCollection(ArgsEntity args)
        //{
        //    //public ObservableCollection<LeadCustomerView> GetLeadCollection(string sOriginator, int iLeadStageID = 0,
        //    //    bool retreiveActive = true, bool isReqSentVisible = false, string defaultDeptID = "", string childOriginators="")
        //    //{
        //    string sWhereCls = "";
        //    //clsOriginator oOriginator;
        //    //string sRepGroups = "";
        //    DbDataReader leadsReader = null;

        //    try
        //    {
        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();

        //        paramCollection.Add("@Originator", args.Originator, DbType.String);
        //        paramCollection.Add("@childOriginators", args.ChildOriginators, DbType.String);
        //        paramCollection.Add("@defaultDepId", args.DefaultDepartmentId, DbType.String);
        //        paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
        //        paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
        //        paramCollection.Add("@AddressType", args.AddressType, DbType.String);
        //        paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
        //        paramCollection.Add("@RetreiveActive", args.ActiveInactiveChecked, DbType.Boolean);
        //        paramCollection.Add("@IsReqSentVisible", args.ReqSentIsChecked, DbType.Boolean);

        //        paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
        //        paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
        //        paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
        //        paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
        //        paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
        //        // Retreive the Leads
        //        List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();

        //        //string sParameter = " (cust_code IS NULL OR cust_code = '')";

        //        //if (retreiveActive)
        //        //    sParameter += " AND l.del_flag != 'Y' ";
        //        //else
        //        //    sParameter += " AND l.del_flag = 'Y' ";

        //        //if (iLeadStageID > 0)
        //        //    sParameter += " AND l.lead_stage_id = " + iLeadStageID.ToString();

        //        //if (sOriginator != "")
        //        //{
        //        //    //oOriginator = new clsOriginator();
        //        //    //sWhereCls = oOriginator.GetChildOriginators(sOriginator);
        //        //    //sWhereCls = "( " + oOriginator.GetChildOriginators(sOriginator);
        //        //    sWhereCls = "( " + childOriginators;

        //        //    /*sRepGroups = oOriginator.GetRepGroups(sOriginator);
        //        //    if (sRepGroups != "")
        //        //    {
        //        //        sRepGroups = sRepGroups.Remove(sRepGroups.Length - 1, 1);  // Remove the Last Comma
        //        //        sWhereCls += " OR l.rep_group_id IN (" + sRepGroups + ")";
        //        //    }*/

        //        //    sWhereCls += " )";
        //        //}

        //        //string sSql = "";

        //        //if (isReqSentVisible == true)   // REQUEST SENT
        //        //{

        //        //    //leadsReader = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetLeadReqSent, " WHERE " + sWhereCls,
        //        //    //clsGlobal.GetInstance().AddressType, clsGlobal.GetInstance().DefaultDeptID));



        //        //    if (sWhereCls != "")
        //        //        sSql = string.Format(CRMSQLs.GetLead, " WHERE (cust_code IS NULL OR cust_code = '')  AND " + sWhereCls,
        //        //        defaultDepId, "=");
        //        //    else
        //        //        sSql = string.Format(CRMSQLs.GetLead, " WHERE (cust_code IS NULL OR cust_code = '') ",
        //        //        defaultDepId, "=");

        //        //}
        //        //else
        //        //{
        //        //    sWhereCls = sWhereCls != "" ? sWhereCls + " AND " + sParameter : sParameter;

        //        //    sSql = string.Format(CRMSQLs.GetLead, " WHERE " + sWhereCls,
        //        //        defaultDepId, "!=");
        //        //}

        //        //sSql = "SELECT d.* FROM (" + sSql + ")d ";

        //        //if (!string.IsNullOrEmpty(additionalParams))
        //        //    sSql = sSql + " WHERE " + additionalParams;

        //        //if (!string.IsNullOrEmpty(orderBy))
        //        //{
        //        //    sSql = sSql + " ORDER BY d." + orderBy;
        //        //}

        //        leadsReader = this.DataAcessService.ExecuteQuery(LeadSql["GetLead"], paramCollection);


        //        if (leadsReader.HasRows)
        //        {
        //            int idOrdinal = leadsReader.GetOrdinal("lead_id");
        //            int nameOrdinal = leadsReader.GetOrdinal("Name");
        //            int businessOrdinal = leadsReader.GetOrdinal("Business");

        //            int stateOrdinal = leadsReader.GetOrdinal("state");
        //            int telephoneOrdinal = leadsReader.GetOrdinal("telephone");
        //            int faxOrdinal = leadsReader.GetOrdinal("fax");
        //            int mobileOrdinal = leadsReader.GetOrdinal("mobile");
        //            int emailAddressOrdinal = leadsReader.GetOrdinal("email");
        //            int addressOrdinal = leadsReader.GetOrdinal("address");
        //            int cityOrdinal = leadsReader.GetOrdinal("city");
        //            int postcodeOrdinal = leadsReader.GetOrdinal("postcode");
        //            int leadStageOrdinal = leadsReader.GetOrdinal("lead_stage");

        //            int leadSourceOrdinal = leadsReader.GetOrdinal("lead_source");
        //            int industryOrdinal = leadsReader.GetOrdinal("industry");
        //            int originatorOrdinal = leadsReader.GetOrdinal("Rep");
        //            int industryDescriptionOrdinal = leadsReader.GetOrdinal("industry_description");

        //            int annualRevenueOrdinal = leadsReader.GetOrdinal("annual_revenue");
        //            int noOfEmployeesOrdinal = leadsReader.GetOrdinal("no_of_employees");
        //            int ratingOrdinal = leadsReader.GetOrdinal("rating");
        //            int websiteOrdinal = leadsReader.GetOrdinal("website");
        //            int descriptionOrdinal = leadsReader.GetOrdinal("description");

        //            int busPotentialOrdinal = leadsReader.GetOrdinal("bus_potential");
        //            int referredByOrdinal = leadsReader.GetOrdinal("referred_by");
        //            int countryOrdinal = leadsReader.GetOrdinal("country");
        //            int prefContactOrdinal = leadsReader.GetOrdinal("pref_contact");
        //            //int createdByOrdinal = leadsReader.GetOrdinal("created_by");
        //            //int lastModifiedByOrdinal = leadsReader.GetOrdinal("last_modified_by");

        //            int companyOrdinal = leadsReader.GetOrdinal("Company");
        //            int leadStatusOrdinal = leadsReader.GetOrdinal("lead_status");
        //            //int createdDateOrdinal = leadsReader.GetOrdinal("created_date");
        //            //int lastModifiedDateOrdinal = leadsReader.GetOrdinal("last_modified_date");
        //            int prefContactDescriptionOrdinal = leadsReader.GetOrdinal("pref_contact_description");
        //            int channelDescriptionOrdinal = leadsReader.GetOrdinal("channel_description");
        //            int delFlag = leadsReader.GetOrdinal("del_flag");

        //            while (leadsReader.Read())
        //            {
        //                LeadCustomerViewEntity lead = LeadCustomerViewEntity.CreateObject();

        //                if (!leadsReader.IsDBNull(idOrdinal)) lead.SourceId = leadsReader.GetInt32(idOrdinal);
        //                if (!leadsReader.IsDBNull(nameOrdinal)) lead.Name = leadsReader.GetString(nameOrdinal);
        //                if (!leadsReader.IsDBNull(businessOrdinal)) lead.Business = leadsReader.GetString(businessOrdinal);

        //                if (!leadsReader.IsDBNull(stateOrdinal)) lead.State = leadsReader.GetString(stateOrdinal);
        //                if (!leadsReader.IsDBNull(addressOrdinal)) lead.Address = leadsReader.GetString(addressOrdinal);
        //                if (!leadsReader.IsDBNull(cityOrdinal)) lead.City = leadsReader.GetString(cityOrdinal);
        //                if (!leadsReader.IsDBNull(emailAddressOrdinal)) lead.Email = leadsReader.GetString(emailAddressOrdinal);
        //                if (!leadsReader.IsDBNull(mobileOrdinal)) lead.Mobile = leadsReader.GetString(mobileOrdinal);
        //                if (!leadsReader.IsDBNull(postcodeOrdinal)) lead.PostalCode = leadsReader.GetString(postcodeOrdinal);
        //                if (!leadsReader.IsDBNull(telephoneOrdinal)) lead.Telephone = leadsReader.GetString(telephoneOrdinal);
        //                if (!leadsReader.IsDBNull(leadStageOrdinal)) lead.LeadStage = leadsReader.GetString(leadStageOrdinal);

        //                if (!leadsReader.IsDBNull(leadSourceOrdinal)) lead.LeadSource = leadsReader.GetString(leadSourceOrdinal);
        //                if (!leadsReader.IsDBNull(industryOrdinal)) lead.Industry = leadsReader.GetString(industryOrdinal);
        //                if (!leadsReader.IsDBNull(originatorOrdinal)) lead.Originator = leadsReader.GetString(originatorOrdinal);

        //                if (!leadsReader.IsDBNull(annualRevenueOrdinal)) lead.AnnualRevenue = leadsReader.GetDouble(annualRevenueOrdinal);
        //                if (!leadsReader.IsDBNull(noOfEmployeesOrdinal)) lead.NoOfEmployees = leadsReader.GetInt32(noOfEmployeesOrdinal);
        //                if (!leadsReader.IsDBNull(ratingOrdinal)) lead.Rating = leadsReader.GetInt32(ratingOrdinal);
        //                if (!leadsReader.IsDBNull(websiteOrdinal)) lead.Webiste = leadsReader.GetString(websiteOrdinal);
        //                if (!leadsReader.IsDBNull(descriptionOrdinal)) lead.Description = leadsReader.GetString(descriptionOrdinal);

        //                if (!leadsReader.IsDBNull(busPotentialOrdinal)) lead.BusinessPotential = leadsReader.GetString(busPotentialOrdinal);
        //                if (!leadsReader.IsDBNull(referredByOrdinal)) lead.ReferredBy = leadsReader.GetString(referredByOrdinal);
        //                if (!leadsReader.IsDBNull(countryOrdinal)) lead.Country = leadsReader.GetString(countryOrdinal);
        //                if (!leadsReader.IsDBNull(prefContactOrdinal)) lead.PreferredContact = leadsReader.GetString(prefContactOrdinal);
        //                //lead.CreatedBy = leadsReader.GetString(createdByOrdinal);
        //                //lead.LastModifiedBy = leadsReader.GetString(lastModifiedByOrdinal);

        //                if (!leadsReader.IsDBNull(companyOrdinal)) lead.Company = leadsReader.GetString(companyOrdinal);
        //                if (!leadsReader.IsDBNull(leadStatusOrdinal)) lead.LeadStatus = leadsReader.GetString(leadStatusOrdinal);
        //                //lead.CreatedDate = leadsReader.GetDateTime(createdDateOrdinal);
        //                //lead.LastModifiedDate = leadsReader.GetDateTime(lastModifiedDateOrdinal);
        //                if (!leadsReader.IsDBNull(industryDescriptionOrdinal)) lead.IndustryDescription = leadsReader.GetString(industryDescriptionOrdinal);
        //                if (!leadsReader.IsDBNull(prefContactDescriptionOrdinal)) lead.PreferredContactDescription = leadsReader.GetString(prefContactDescriptionOrdinal);
        //                if (!leadsReader.IsDBNull(channelDescriptionOrdinal)) lead.ChannelDescription = leadsReader.GetString(channelDescriptionOrdinal);

        //                if (!leadsReader.IsDBNull(delFlag)) lead.IsDeleted = leadsReader.GetString(delFlag);

        //                lead.LeadCustomerType = LeadCustomerTypes.Lead;
        //                lead.SecondaryRepCode = string.Empty;

        //                leadCollection.Add(lead);

        //            }
        //        }

        //        return leadCollection;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (leadsReader != null && !leadsReader.IsClosed)
        //            leadsReader.Close();
        //    }
        //}

        public List<LeadCustomerViewEntity> GetLeadCollection(ArgsEntity args)
        {
            //DbDataReader customerReader = null;

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@Originator", args.Originator, DbType.String);
            //    paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
            //    paramCollection.Add("@ChildOriginatorsCust", args.ChildOriginators.Replace("originator", "r.originator"), DbType.String);//cust
            //    paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
            //    if (args.OrderBy == "cust_code asc")
            //        paramCollection.Add("@OrderBy", "(00000000 + cust_code) asc", DbType.String);
            //    else if (args.OrderBy == "cust_code desc")
            //        paramCollection.Add("@OrderBy", "(00000000 + cust_code) desc", DbType.String);
            //    else
            //        paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
            //    paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
            //    paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
            //    paramCollection.Add("@RetreiveActive", args.ActiveInactiveChecked, DbType.Boolean);
            //    paramCollection.Add("@IsReqSentVisible", args.ReqSentIsChecked, DbType.Boolean);

            //    paramCollection.Add("@RepType", args.RepType, DbType.String);
            //    paramCollection.Add("@LeadStage", args.LeadStage, DbType.String);

            //    paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
            //    paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
            //    paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

            //    List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();

            //    customerReader = this.DataAcessService.ExecuteQuery(LeadSql["GetLeadsAndCustomers"], paramCollection);

            //    if (customerReader.HasRows)
            //    {
            //        int nameOrdinal = customerReader.GetOrdinal("name");
            //        int stateOrdinal = customerReader.GetOrdinal("state");
            //        int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
            //        int telephoneOrdinal = customerReader.GetOrdinal("telephone");
            //        int address1Ordinal = customerReader.GetOrdinal("address_1");
            //        int address2Ordinal = customerReader.GetOrdinal("address_2");
            //        int cityOrdinal = customerReader.GetOrdinal("city");
            //        int leadSourceOrdinal = customerReader.GetOrdinal("lead_source");
            //        int delFlag = customerReader.GetOrdinal("del_flag");
            //        int leadIdOrdinal = customerReader.GetOrdinal("lead_id");
            //        int leadStageIdOrdinal = customerReader.GetOrdinal("lead_stage_id");
            //        int leadStageOrdinal = customerReader.GetOrdinal("lead_stage");
            //        int leadStatusOrdinal = customerReader.GetOrdinal("lead_status");
            //        int totalCountOrdinal = customerReader.GetOrdinal("total_count");
            //        int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
            //        int volumeOrdinal = customerReader.GetOrdinal("volume");
            //        int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
            //        int categoryOrdinal = customerReader.GetOrdinal("category_name");
            //        int MarketOrdinal = customerReader.GetOrdinal("market_name");
            //        int istlpOrdinal = customerReader.GetOrdinal("istlp");
            //        int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
            //        int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");
            //        int dis_idOrdinal = customerReader.GetOrdinal("dis_id");
            //        int dis_codeOrdinal = customerReader.GetOrdinal("dis_code");
            //        int dis_nameOrdinal = customerReader.GetOrdinal("dis_name");
            //        int lstInvoiceDate = customerReader.GetOrdinal("Last_invoice_date");

            //        while (customerReader.Read())
            //        {
            //            LeadCustomerViewEntity leadCustomerView = LeadCustomerViewEntity.CreateObject();

            //            if (!customerReader.IsDBNull(nameOrdinal)) leadCustomerView.Name = customerReader.GetString(nameOrdinal);
            //            if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(stateOrdinal);
            //            leadCustomerView.LeadCustomerType = LeadCustomerTypes.Customer;

            //            if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.State = customerReader.GetString(stateOrdinal).Trim();

            //            if (!customerReader.IsDBNull(address1Ordinal))
            //                leadCustomerView.Address = leadCustomerView.Address1 = customerReader.GetString(address1Ordinal);

            //            if (!customerReader.IsDBNull(address2Ordinal))
            //            {
            //                leadCustomerView.Address2 = customerReader.GetString(address2Ordinal);
            //                leadCustomerView.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
            //               ", " + customerReader.GetString(address2Ordinal) : string.Empty);
            //            }

            //            if (!customerReader.IsDBNull(cityOrdinal)) leadCustomerView.City = customerReader.GetString(cityOrdinal).Trim();
            //            if (!customerReader.IsDBNull(telephoneOrdinal)) leadCustomerView.Telephone = customerReader.GetString(telephoneOrdinal);
            //            if (!customerReader.IsDBNull(custCodeOrdinal)) leadCustomerView.CustomerCode = customerReader.GetString(custCodeOrdinal).Trim();
            //            if (!customerReader.IsDBNull(istlpOrdinal)) leadCustomerView.isTLP = customerReader.GetBoolean(istlpOrdinal);
            //            if (!customerReader.IsDBNull(LongitudeIdOrdinal)) leadCustomerView.Longitude = customerReader.GetDouble(LongitudeIdOrdinal);
            //            if (!customerReader.IsDBNull(LatitudeIdOrdinal)) leadCustomerView.Latitiude = customerReader.GetDouble(LatitudeIdOrdinal);
            //            if (!customerReader.IsDBNull(peripheryOrdinal)) leadCustomerView.Periphery = customerReader.GetString(peripheryOrdinal);
            //            if (!customerReader.IsDBNull(outlettypeOrdinal)) leadCustomerView.OutletType = customerReader.GetString(outlettypeOrdinal);
            //            if (!customerReader.IsDBNull(categoryOrdinal)) leadCustomerView.Category = customerReader.GetString(categoryOrdinal);
            //            if (!customerReader.IsDBNull(MarketOrdinal)) leadCustomerView.MarketName = customerReader.GetString(MarketOrdinal);
            //            if (!customerReader.IsDBNull(volumeOrdinal)) leadCustomerView.Volume = customerReader.GetString(volumeOrdinal);
            //            if (!customerReader.IsDBNull(leadSourceOrdinal)) leadCustomerView.LeadSource = customerReader.GetString(leadSourceOrdinal).Trim();
            //            if (!customerReader.IsDBNull(delFlag)) leadCustomerView.IsDeleted = customerReader.GetString(delFlag);
            //            if (!customerReader.IsDBNull(leadIdOrdinal)) leadCustomerView.SourceId = customerReader.GetInt32(leadIdOrdinal);
            //            if (!customerReader.IsDBNull(leadStageOrdinal)) leadCustomerView.LeadStage = customerReader.GetString(leadStageOrdinal);
            //            if (!customerReader.IsDBNull(leadStatusOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(leadStatusOrdinal);
            //            if (!customerReader.IsDBNull(totalCountOrdinal)) leadCustomerView.RowCount = customerReader.GetInt32(totalCountOrdinal);
            //            if (!customerReader.IsDBNull(dis_idOrdinal)) leadCustomerView.DistributorID = customerReader.GetInt32(dis_idOrdinal);
            //            if (!customerReader.IsDBNull(dis_codeOrdinal)) leadCustomerView.DistributorCode = customerReader.GetString(dis_codeOrdinal);
            //            if (!customerReader.IsDBNull(dis_nameOrdinal)) leadCustomerView.DistributorName = customerReader.GetString(dis_nameOrdinal);
            //            if (!customerReader.IsDBNull(lstInvoiceDate)) leadCustomerView.LastInvoiceDate = Convert.ToString(customerReader.GetDateTime(lstInvoiceDate));

            //            leadCollection.Add(leadCustomerView);
            //        }
            //    }
            //    return leadCollection;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (customerReader != null && (!customerReader.IsClosed))
            //        customerReader.Close();
            //}

            DataTable objDS = new DataTable();
            List<LeadCustomerViewEntity> objList = new List<LeadCustomerViewEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetLeadsAndCustomers";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators);
                    objCommand.Parameters.AddWithValue("@ChildOriginatorsCust", args.ChildOriginators.Replace("originator", "r.originator"));
                    objCommand.Parameters.AddWithValue("@DefaultDepId", args.DefaultDepartmentId);
                    if (args.OrderBy == "cust_code asc")
                        objCommand.Parameters.AddWithValue("@OrderBy", "(00000000 + cust_code) asc");
                    else if (args.OrderBy == "cust_code desc")
                        objCommand.Parameters.AddWithValue("@OrderBy", "(00000000 + cust_code) desc");
                    else
                        objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@LeadStageId", args.LeadStageId);
                    objCommand.Parameters.AddWithValue("@RetreiveActive", args.ActiveInactiveChecked);
                    objCommand.Parameters.AddWithValue("@IsReqSentVisible", args.ReqSentIsChecked);
                    objCommand.Parameters.AddWithValue("@RepType", args.RepType);
                    objCommand.Parameters.AddWithValue("@LeadStage", args.LeadStage);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            LeadCustomerViewEntity objModel = LeadCustomerViewEntity.CreateObject();
                            if (item["name"] != DBNull.Value) objModel.Name = item["name"].ToString();
                            if (item["state"] != DBNull.Value) objModel.LeadStatus = item["state"].ToString();
                            objModel.LeadCustomerType = LeadCustomerTypes.Customer;
                            objModel.State = objModel.LeadStatus;

                            if (item["address_1"] != DBNull.Value)
                            {
                                objModel.Address = objModel.Address1 = item["address_1"].ToString();
                            }

                            if (item["address_2"] != DBNull.Value)
                            {
                                objModel.Address2 = item["address_2"].ToString();
                                objModel.Address += (!string.IsNullOrWhiteSpace(item["address_2"].ToString()) ?
                                    ", " + item["address_2"].ToString() : string.Empty);
                            }

                            if (item["city"] != DBNull.Value) objModel.City = item["city"].ToString();
                            if (item["telephone"] != DBNull.Value) objModel.Telephone = item["telephone"].ToString();
                            if (item["cust_code"] != DBNull.Value) objModel.CustomerCode = item["cust_code"].ToString();
                            if (item["istlp"] != DBNull.Value) objModel.isTLP = Convert.ToBoolean(item["istlp"]);
                            if (item["longitude"] != DBNull.Value) objModel.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["latitude"] != DBNull.Value) objModel.Latitiude = Convert.ToDouble(item["latitude"]);
                            if (item["periphery_name"] != DBNull.Value) objModel.Periphery = item["periphery_name"].ToString();
                            if (item["outlettype_name"] != DBNull.Value) objModel.OutletType = item["outlettype_name"].ToString();
                            if (item["category_name"] != DBNull.Value) objModel.Category = item["category_name"].ToString();
                            if (item["market_name"] != DBNull.Value) objModel.MarketName = item["market_name"].ToString();
                            if (item["volume"] != DBNull.Value) objModel.Volume = item["volume"].ToString();
                            if (item["lead_source"] != DBNull.Value) objModel.LeadSource = item["lead_source"].ToString();
                            if (item["del_flag"] != DBNull.Value) objModel.IsDeleted = item["del_flag"].ToString();
                            if (item["lead_id"] != DBNull.Value) objModel.SourceId = Convert.ToInt32(item["lead_id"]);
                            if (item["lead_stage"] != DBNull.Value) objModel.LeadStage = item["lead_stage"].ToString();
                            if (item["lead_status"] != DBNull.Value) objModel.LeadStatus = item["lead_status"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.RowCount = Convert.ToInt32(item["total_count"]);
                            if (item["dis_id"] != DBNull.Value) objModel.DistributorID = Convert.ToInt32(item["dis_id"]);
                            if (item["dis_code"] != DBNull.Value) objModel.DistributorCode = item["dis_code"].ToString();
                            if (item["dis_name"] != DBNull.Value) objModel.DistributorName = item["dis_name"].ToString();
                            if (item["Last_invoice_date"] != DBNull.Value) objModel.LastInvoiceDate = Convert.ToString(item["Last_invoice_date"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }


        public List<LeadCustomerViewEntity> GetLeadCollectionForCheckList(int CheckListId, ArgsEntity args)
        {
            DbDataReader customerReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CheckListId", CheckListId, DbType.Int32);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@ChildOriginatorsCust", args.ChildOriginators.Replace("originator", "r.originator"), DbType.String);//cust
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
                if (args.OrderBy == "cust_code asc")
                    paramCollection.Add("@OrderBy", "(00000000 + cust_code) asc", DbType.String);
                else if (args.OrderBy == "cust_code desc")
                    paramCollection.Add("@OrderBy", "(00000000 + cust_code) desc", DbType.String);
                else
                    paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@RetreiveActive", args.ActiveInactiveChecked, DbType.Boolean);
                paramCollection.Add("@IsReqSentVisible", args.ReqSentIsChecked, DbType.Boolean);

                paramCollection.Add("@RepType", args.RepType, DbType.String);//cust
                paramCollection.Add("@LeadStage", args.LeadStage, DbType.String);

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                // Retreive the Leads
                List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();

                customerReader = this.DataAcessService.ExecuteQuery(LeadSql["GetLeadsAndCustomers_For_CheckList"], paramCollection);

                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");
                    int cityOrdinal = customerReader.GetOrdinal("city");
                    int leadSourceOrdinal = customerReader.GetOrdinal("lead_source");
                    int delFlag = customerReader.GetOrdinal("del_flag");
                    int leadIdOrdinal = customerReader.GetOrdinal("lead_id");
                    int leadStageIdOrdinal = customerReader.GetOrdinal("lead_stage_id");
                    int leadStageOrdinal = customerReader.GetOrdinal("lead_stage");
                    int leadStatusOrdinal = customerReader.GetOrdinal("lead_status");
                    int totalCountOrdinal = customerReader.GetOrdinal("total_count");
                    int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
                    int volumeOrdinal = customerReader.GetOrdinal("volume");
                    int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
                    int categoryOrdinal = customerReader.GetOrdinal("category_name");
                    int MarketOrdinal = customerReader.GetOrdinal("market_name");
                    int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");
                    int dis_idOrdinal = customerReader.GetOrdinal("dis_id");
                    int dis_codeOrdinal = customerReader.GetOrdinal("dis_code");
                    int dis_nameOrdinal = customerReader.GetOrdinal("dis_name");
                    int lstInvoiceDate = customerReader.GetOrdinal("Last_invoice_date");
                    int IsCheckList = customerReader.GetOrdinal("IsCheckList");
                    int route_nameOrdinal = customerReader.GetOrdinal("route_name");
                    int originatorOrdinal = customerReader.GetOrdinal("originator");
                    int rep_nameOrdinal = customerReader.GetOrdinal("rep_name");

                    while (customerReader.Read())
                    {
                        LeadCustomerViewEntity leadCustomerView = LeadCustomerViewEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) leadCustomerView.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(stateOrdinal);
                        leadCustomerView.LeadCustomerType = LeadCustomerTypes.Customer;

                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.State = customerReader.GetString(stateOrdinal).Trim();
                        if (!customerReader.IsDBNull(address1Ordinal))
                            leadCustomerView.Address = leadCustomerView.Address1 = customerReader.GetString(address1Ordinal);
                        if (!customerReader.IsDBNull(address2Ordinal))
                        {
                            leadCustomerView.Address2 = customerReader.GetString(address2Ordinal);
                            leadCustomerView.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);
                        }

                        if (!customerReader.IsDBNull(cityOrdinal)) leadCustomerView.City = customerReader.GetString(cityOrdinal).Trim();
                        if (!customerReader.IsDBNull(telephoneOrdinal)) leadCustomerView.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) leadCustomerView.CustomerCode = customerReader.GetString(custCodeOrdinal).Trim();
                        if (!customerReader.IsDBNull(istlpOrdinal)) leadCustomerView.isTLP = customerReader.GetBoolean(istlpOrdinal);
                        if (!customerReader.IsDBNull(LongitudeIdOrdinal)) leadCustomerView.Longitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        if (!customerReader.IsDBNull(LatitudeIdOrdinal)) leadCustomerView.Latitiude = customerReader.GetDouble(LatitudeIdOrdinal);
                        if (!customerReader.IsDBNull(peripheryOrdinal)) leadCustomerView.Periphery = customerReader.GetString(peripheryOrdinal);
                        if (!customerReader.IsDBNull(outlettypeOrdinal)) leadCustomerView.OutletType = customerReader.GetString(outlettypeOrdinal);
                        if (!customerReader.IsDBNull(categoryOrdinal)) leadCustomerView.Category = customerReader.GetString(categoryOrdinal);
                        if (!customerReader.IsDBNull(MarketOrdinal)) leadCustomerView.MarketName = customerReader.GetString(MarketOrdinal);
                        if (!customerReader.IsDBNull(volumeOrdinal)) leadCustomerView.Volume = customerReader.GetString(volumeOrdinal);
                        if (!customerReader.IsDBNull(leadSourceOrdinal)) leadCustomerView.LeadSource = customerReader.GetString(leadSourceOrdinal).Trim();
                        if (!customerReader.IsDBNull(delFlag)) leadCustomerView.IsDeleted = customerReader.GetString(delFlag);
                        if (!customerReader.IsDBNull(leadIdOrdinal)) leadCustomerView.SourceId = customerReader.GetInt32(leadIdOrdinal);
                        if (!customerReader.IsDBNull(leadStageOrdinal)) leadCustomerView.LeadStage = customerReader.GetString(leadStageOrdinal);
                        if (!customerReader.IsDBNull(leadStatusOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(leadStatusOrdinal);
                        if (!customerReader.IsDBNull(totalCountOrdinal)) leadCustomerView.RowCount = customerReader.GetInt32(totalCountOrdinal);
                        if (!customerReader.IsDBNull(dis_idOrdinal)) leadCustomerView.DistributorID = customerReader.GetInt32(dis_idOrdinal);
                        if (!customerReader.IsDBNull(dis_codeOrdinal)) leadCustomerView.DistributorCode = customerReader.GetString(dis_codeOrdinal);
                        if (!customerReader.IsDBNull(dis_nameOrdinal)) leadCustomerView.DistributorName = customerReader.GetString(dis_nameOrdinal);
                        if (!customerReader.IsDBNull(lstInvoiceDate)) leadCustomerView.LastInvoiceDate = Convert.ToString(customerReader.GetDateTime(lstInvoiceDate));
                        if (!customerReader.IsDBNull(IsCheckList)) leadCustomerView.IsCheckList = customerReader.GetBoolean(IsCheckList);
                        if (!customerReader.IsDBNull(route_nameOrdinal)) leadCustomerView.RouteName = customerReader.GetString(route_nameOrdinal);
                        if (!customerReader.IsDBNull(originatorOrdinal)) leadCustomerView.RepCode = customerReader.GetString(originatorOrdinal);
                        if (!customerReader.IsDBNull(rep_nameOrdinal)) leadCustomerView.RepName = customerReader.GetString(rep_nameOrdinal);
                        leadCollection.Add(leadCustomerView);
                    }
                }
                return leadCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public int GetTotalCustomerCount(ArgsEntity args)
        {
            //string sWhereCls = "";
            try
            {
                //if (sOriginator != "")
                //{
                //    sWhereCls = childOriginators;
                //}

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);

                return Convert.ToInt32(DataAcessService.GetOneValue(LeadSql["GetCustomerCount"], paramCollection));
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(LeadEntity lead)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@leadId", lead.LeadID, DbType.String);
                return DataAcessService.ExecuteNonQuery(LeadSql["DeleteLead"], paramCollection)>0;

            }
            catch
            {
                throw;
            }
        }

        public bool ConvertLeadToCustomer(List<string> sSqls)
        {
            bool isSuccess = true;
            try
            {
                foreach (string sql in sSqls)
                {
                    DbSqlStructure sqladapt = new DbSqlStructure();
                    sqladapt.Sql = sql;
                    sqladapt.ProviderType = DbProviderType.SqlServer;
                    sqladapt.SqlType = CommandType.Text;
                    isSuccess = this.DataAcessService.ExecuteNonQuery(sqladapt) > 0;
                    if (!isSuccess)
                        break;
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }


        public List<LeadCustomerViewEntity> GetLeadCustomerCountByState(ArgsEntity args)
        {
            //string leadWhereCls = "";
            //string customerWhereCls = "";

            DbDataReader leadsReader = null;
            try
            {

                // Retreive the Leads

                args.SStartDate = DateTime.Parse((DateTime.Parse(args.SStartDate)).ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString();
                args.SEndDate = DateTime.Parse((DateTime.Parse(args.SEndDate)).ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString();
                List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();

                //if (sOriginator != "")
                //{
                //    leadWhereCls = childOriginators;
                //}

                //customerWhereCls = leadWhereCls.Replace("originator", "modified_by");
                //string activityWhereCls = leadWhereCls.Replace("originator", "a.created_by");
                //string sFilter = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);                
                paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);

                //if (iLeadStageId > 0)
                //    sFilter = " AND l.lead_stage_id = " + iLeadStageId;

                if (args.LeadStage == "All")
                    leadsReader = DataAcessService.ExecuteQuery(LeadSql["GetLeadCustomerCountByState"], paramCollection);
                else if (args.LeadStage != "Customer" && args.LeadStage != "All")
                    leadsReader = DataAcessService.ExecuteQuery(LeadSql["GetLeadCountByState"], paramCollection);
                else if (args.LeadStage == "Customer")
                    leadsReader = DataAcessService.ExecuteQuery(LeadSql["GetCustomerCountByState"], paramCollection);


                if (leadsReader.HasRows)
                {
                    int stateOrdinal = leadsReader.GetOrdinal("State");
                    int countOrdinal = leadsReader.GetOrdinal("LeadCustomerCount");


                    while (leadsReader.Read())
                    {
                        LeadCustomerViewEntity lead = LeadCustomerViewEntity.CreateObject();

                        if (!leadsReader.IsDBNull(stateOrdinal)) lead.State = leadsReader.GetString(stateOrdinal);
                        if (!leadsReader.IsDBNull(countOrdinal)) lead.NoOfLeadCustomers = leadsReader.GetInt32(countOrdinal);

                        leadCollection.Add(lead);

                    }
                }

                return leadCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (leadsReader != null && (!leadsReader.IsClosed))
                    leadsReader.Close();
            }
        }

        public List<LeadCustomerViewEntity> GetLeadCollectionByState(ArgsEntity args)
        {
            string sWhereCls = "";
            //clsOriginator oOriginator;

            DbDataReader leadsReader = null;
            try
            {
                // Retreive the Leads
                args.SStartDate = DateTime.Parse((DateTime.Parse(args.SStartDate)).ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString();
                args.SEndDate = DateTime.Parse((DateTime.Parse(args.SEndDate)).ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString();

                List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();

                //sWhereCls = "WHERE ";
                //if (sOriginator != "")
                //{
                //    sWhereCls = childOriginators;
                //}
                //string sFilter = null;
                //string activityWhereCls = sWhereCls.Replace("originator", "a.created_by");

                //if (iLeadStageID > 0)
                //    sWhereCls = " AND l.lead_stage_id = " + iLeadStageID;

                //if (!string.IsNullOrEmpty(additionalParams))
                //{
                //    activityWhereCls = " AND " + additionalParams;
                //}

                //if (!string.IsNullOrEmpty(orderBy))
                //{
                //    activityWhereCls = " " + orderBy;
                //}

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);

                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);

                leadsReader = this.DataAcessService.ExecuteQuery(LeadSql["GetLeadAll"], paramCollection);

                if (leadsReader.HasRows)
                {
                    int idOrdinal = leadsReader.GetOrdinal("lead_id");
                    int nameOrdinal = leadsReader.GetOrdinal("Name");
                    int businessOrdinal = leadsReader.GetOrdinal("Business");

                    int stateOrdinal = leadsReader.GetOrdinal("state");
                    int telephoneOrdinal = leadsReader.GetOrdinal("telephone");
                    int faxOrdinal = leadsReader.GetOrdinal("fax");
                    int mobileOrdinal = leadsReader.GetOrdinal("mobile");
                    int emailAddressOrdinal = leadsReader.GetOrdinal("email");
                    int addressOrdinal = leadsReader.GetOrdinal("address");
                    int cityOrdinal = leadsReader.GetOrdinal("city");
                    int postcodeOrdinal = leadsReader.GetOrdinal("postcode");
                    int leadStageOrdinal = leadsReader.GetOrdinal("lead_stage");

                    int leadSourceOrdinal = leadsReader.GetOrdinal("lead_source");
                    int industryOrdinal = leadsReader.GetOrdinal("industry");
                    int originatorOrdinal = leadsReader.GetOrdinal("Rep");
                    int industryDescriptionOrdinal = leadsReader.GetOrdinal("industry_description");

                    int annualRevenueOrdinal = leadsReader.GetOrdinal("annual_revenue");
                    int noOfEmployeesOrdinal = leadsReader.GetOrdinal("no_of_employees");
                    int ratingOrdinal = leadsReader.GetOrdinal("rating");
                    int websiteOrdinal = leadsReader.GetOrdinal("website");
                    int descriptionOrdinal = leadsReader.GetOrdinal("description");

                    int busPotentialOrdinal = leadsReader.GetOrdinal("bus_potential");
                    int referredByOrdinal = leadsReader.GetOrdinal("referred_by");
                    int countryOrdinal = leadsReader.GetOrdinal("country");
                    int prefContactOrdinal = leadsReader.GetOrdinal("pref_contact");
                    //int createdByOrdinal = leadsReader.GetOrdinal("created_by");
                    //int lastModifiedByOrdinal = leadsReader.GetOrdinal("last_modified_by");

                    int companyOrdinal = leadsReader.GetOrdinal("Company");
                    int leadStatusOrdinal = leadsReader.GetOrdinal("lead_status");
                    //int createdDateOrdinal = leadsReader.GetOrdinal("created_date");
                    //int lastModifiedDateOrdinal = leadsReader.GetOrdinal("last_modified_date");
                    int prefContactDescriptionOrdinal = leadsReader.GetOrdinal("pref_contact_description");
                    int channelDescriptionOrdinal = leadsReader.GetOrdinal("channel_description");

                    while (leadsReader.Read())
                    {
                        LeadCustomerViewEntity lead = LeadCustomerViewEntity.CreateObject();

                        if (!leadsReader.IsDBNull(idOrdinal)) lead.SourceId = leadsReader.GetInt32(idOrdinal);
                        if (!leadsReader.IsDBNull(nameOrdinal)) lead.Name = leadsReader.GetString(nameOrdinal);
                        if (!leadsReader.IsDBNull(businessOrdinal)) lead.Business = leadsReader.GetString(businessOrdinal);

                        if (!leadsReader.IsDBNull(stateOrdinal)) lead.State = leadsReader.GetString(stateOrdinal);
                        if (!leadsReader.IsDBNull(addressOrdinal)) lead.Address = leadsReader.GetString(addressOrdinal);
                        if (!leadsReader.IsDBNull(cityOrdinal)) lead.City = leadsReader.GetString(cityOrdinal);
                        if (!leadsReader.IsDBNull(emailAddressOrdinal)) lead.Email = leadsReader.GetString(emailAddressOrdinal);
                        if (!leadsReader.IsDBNull(mobileOrdinal)) lead.Mobile = leadsReader.GetString(mobileOrdinal);
                        if (!leadsReader.IsDBNull(postcodeOrdinal)) lead.PostalCode = leadsReader.GetString(postcodeOrdinal);
                        if (!leadsReader.IsDBNull(telephoneOrdinal)) lead.Telephone = leadsReader.GetString(telephoneOrdinal);
                        if (!leadsReader.IsDBNull(leadStageOrdinal)) lead.LeadStage = leadsReader.GetString(leadStageOrdinal);

                        if (!leadsReader.IsDBNull(leadSourceOrdinal)) lead.LeadSource = leadsReader.GetString(leadSourceOrdinal);
                        if (!leadsReader.IsDBNull(industryOrdinal)) lead.Industry = leadsReader.GetString(industryOrdinal);
                        if (!leadsReader.IsDBNull(originatorOrdinal)) lead.Originator = leadsReader.GetString(originatorOrdinal);

                        if (!leadsReader.IsDBNull(annualRevenueOrdinal)) lead.AnnualRevenue = leadsReader.GetDouble(annualRevenueOrdinal);
                        if (!leadsReader.IsDBNull(noOfEmployeesOrdinal)) lead.NoOfEmployees = leadsReader.GetInt32(noOfEmployeesOrdinal);
                        if (!leadsReader.IsDBNull(ratingOrdinal)) lead.Rating = leadsReader.GetInt32(ratingOrdinal);
                        if (!leadsReader.IsDBNull(websiteOrdinal)) lead.Webiste = leadsReader.GetString(websiteOrdinal);
                        if (!leadsReader.IsDBNull(descriptionOrdinal)) lead.Description = leadsReader.GetString(descriptionOrdinal);

                        if (!leadsReader.IsDBNull(busPotentialOrdinal)) lead.BusinessPotential = leadsReader.GetString(busPotentialOrdinal);
                        if (!leadsReader.IsDBNull(referredByOrdinal)) lead.ReferredBy = leadsReader.GetString(referredByOrdinal);
                        if (!leadsReader.IsDBNull(countryOrdinal)) lead.Country = leadsReader.GetString(countryOrdinal);
                        if (!leadsReader.IsDBNull(prefContactOrdinal)) lead.PreferredContact = leadsReader.GetString(prefContactOrdinal);
                        //lead.CreatedBy = leadsReader.GetString(createdByOrdinal);
                        //lead.LastModifiedBy = leadsReader.GetString(lastModifiedByOrdinal);

                        if (!leadsReader.IsDBNull(companyOrdinal)) lead.Company = leadsReader.GetString(companyOrdinal);
                        if (!leadsReader.IsDBNull(leadStatusOrdinal)) lead.LeadStatus = leadsReader.GetString(leadStatusOrdinal);
                        //lead.CreatedDate = leadsReader.GetDateTime(createdDateOrdinal);
                        //lead.LastModifiedDate = leadsReader.GetDateTime(lastModifiedDateOrdinal);
                        if (!leadsReader.IsDBNull(industryDescriptionOrdinal)) lead.IndustryDescription = leadsReader.GetString(industryDescriptionOrdinal);
                        if (!leadsReader.IsDBNull(prefContactDescriptionOrdinal)) lead.PreferredContactDescription = leadsReader.GetString(prefContactDescriptionOrdinal);
                        if (!leadsReader.IsDBNull(channelDescriptionOrdinal)) lead.ChannelDescription = leadsReader.GetString(channelDescriptionOrdinal);

                        lead.LeadCustomerType = LeadCustomerTypes.Lead;

                        leadCollection.Add(lead);

                    }
                }

                return leadCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (leadsReader != null && (!leadsReader.IsClosed))
                    leadsReader.Close();
            }
        }

        public List<LeadCustomerViewEntity> GetLeadCustomerCountByRep(ArgsEntity args)
        {
            string customerWhereCls = "";
            string leadWhereCls = null;
            string activityWhereCls = null;
            //clsOriginator oOriginator;

            DbDataReader leadsReader = null;
            string sSQL = "";

            try
            {
                List<LeadCustomerViewEntity> leadCustomerCollection = new List<LeadCustomerViewEntity>();
                
                args.SStartDate = DateTime.Parse((DateTime.Parse(args.SStartDate)).ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString();
                args.SEndDate = DateTime.Parse((DateTime.Parse(args.SEndDate)).ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString();

                /*
                if (sOriginator != "")
                {
                    oOriginator = new clsOriginator();
                    activityWhereCls = " AND " + childOriginators.Replace("originator", "a.created_by");
                    leadWhereCls = " AND " + childOriginators.Replace("originator", "l.created_by");
                    customerWhereCls = " AND " + childOriginators.Replace("originator", "c.modified_by");
                }


                if (sLeadStage != "All")
                {
                    activityWhereCls += " AND s.lead_stage_id = " + iLeadStageId;
                    leadWhereCls += " AND s.lead_stage_id = " + iLeadStageId;
                }
                */
                
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                paramCollection.Add("@LeadStage" ,args.LeadStage,DbType.String);
                paramCollection.Add("@Status" ,0,DbType.Boolean);


                /*if (sLeadStage == "All")
                    sSQL = string.Format(CRMSQLs.GetLeadCountByRep, sFromDate,
                     sEndDate, activityWhereCls, leadWhereCls)
                     + " UNION ALL "
                     + string.Format(CRMSQLs.GetCustomerCountByRep, sFromDate,
                     sEndDate, customerWhereCls);
                else if (sLeadStage == "Customer")
                    sSQL = string.Format(CRMSQLs.GetCustomerCountByRep, sFromDate,
                     sEndDate, customerWhereCls);
                else
                    sSQL = string.Format(CRMSQLs.GetLeadCountByRep, sFromDate,
                     sEndDate, activityWhereCls, leadWhereCls);
                 * */

                leadsReader = this.DataAcessService.ExecuteQuery(LeadSql["GetLeadCustomerCountByRep"], paramCollection);

                if (leadsReader.HasRows)
                {
                    int repOrdinal = leadsReader.GetOrdinal("Rep");
                    int countOrdinal = leadsReader.GetOrdinal("LeadCustomerCount");


                    while (leadsReader.Read())
                    {
                        LeadCustomerViewEntity lead = LeadCustomerViewEntity.CreateObject();

                        if (!leadsReader.IsDBNull(repOrdinal)) lead.Originator = leadsReader.GetString(repOrdinal);
                        if (!leadsReader.IsDBNull(countOrdinal)) lead.NoOfLeadCustomers = leadsReader.GetInt32(countOrdinal);
                        leadCustomerCollection.Add(lead);

                    }
                }

                return leadCustomerCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (leadsReader != null && (!leadsReader.IsClosed))
                    leadsReader.Close();
            }
        }

        public List<LeadCustomerViewEntity> GetLeadCustomerCollectionByRep(ArgsEntity args)
        {
            string customerWhereCls = "";
            string leadWhereCls = null;
            string activityWhereCls = null;
            //clsOriginator oOriginator;

            DbDataReader leadsReader = null;
            string sSQL = "";

            try
            {
                List<LeadCustomerViewEntity> leadCustomerCollection = new List<LeadCustomerViewEntity>();

                args.SStartDate = DateTime.Parse((DateTime.Parse(args.SStartDate)).ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString();
                args.SEndDate = DateTime.Parse((DateTime.Parse(args.SEndDate)).ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString();

                /*
                if (sOriginator != "")
                {
                    oOriginator = new clsOriginator();
                    activityWhereCls = " AND" + childOriginators.Replace("originator", "a.created_by");
                    leadWhereCls = " AND" + childOriginators.Replace("originator", "l.created_by");
                    customerWhereCls = " AND" + childOriginators.Replace("originator", "c.modified_by");
                }


                if (!sLeadStage.Equals("All"))
                {
                    activityWhereCls += " AND s.lead_stage_id = " + iLeadStageId;
                    leadWhereCls += "AND s.lead_stage_id = " + iLeadStageId;
                }
                
                

                if (sLeadStage.Equals("All"))
                    sSQL = string.Format(CRMSQLs.GetLeadCollectionByRep, sFromDate,
                    sEndDate, activityWhereCls, leadWhereCls)
                     + " UNION ALL "
                     + string.Format(CRMSQLs.GetCustomerCollectionByRep, sFromDate,
                     sEndDate, customerWhereCls);
                else if (sLeadStage == "Customer")
                    sSQL = string.Format(CRMSQLs.GetCustomerCollectionByRep, sFromDate,
                     sEndDate, customerWhereCls);
                else
                    sSQL = string.Format(CRMSQLs.GetLeadCollectionByRep, sFromDate,
                     sEndDate, activityWhereCls, leadWhereCls);
*/
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                paramCollection.Add("@LeadStage" ,args.LeadStage,DbType.String);
                paramCollection.Add("@Status", 1, DbType.Boolean);
                leadsReader = this.DataAcessService.ExecuteQuery(LeadSql["GetLeadCustomerCountByRep"], paramCollection);

                if (leadsReader.HasRows)
                {
                    int leadIdOrdinal = leadsReader.GetOrdinal("lead_id");
                    int custCodeOrdinal = leadsReader.GetOrdinal("cust_code");

                    int nameOrdinal = leadsReader.GetOrdinal("name");
                    int originaotrOrdinal = leadsReader.GetOrdinal("originator");
                    int leadStageOrdinal = leadsReader.GetOrdinal("lead_stage");
                    int address1Ordinal = leadsReader.GetOrdinal("address");

                    int cityOrdinal = leadsReader.GetOrdinal("city");
                    int stateOrdinal = leadsReader.GetOrdinal("state");
                    int postcodeOrdinal = leadsReader.GetOrdinal("postcode");
                    int countryOrdinal = leadsReader.GetOrdinal("country");

                    while (leadsReader.Read())
                    {
                        LeadCustomerViewEntity lead = LeadCustomerViewEntity.CreateObject();

                        if (!leadsReader.IsDBNull(leadIdOrdinal)) lead.SourceId = System.Convert.ToInt32(leadsReader[leadIdOrdinal]);
                        if (!leadsReader.IsDBNull(custCodeOrdinal)) lead.CustomerCode = leadsReader.GetString(custCodeOrdinal);

                        if (!leadsReader.IsDBNull(nameOrdinal)) lead.Name = leadsReader.GetString(nameOrdinal);
                        if (!leadsReader.IsDBNull(originaotrOrdinal)) lead.Originator = leadsReader.GetString(originaotrOrdinal).Trim();
                        if (!leadsReader.IsDBNull(leadStageOrdinal)) lead.LeadStage = leadsReader.GetString(leadStageOrdinal);
                        if (!leadsReader.IsDBNull(address1Ordinal)) lead.Address = leadsReader.GetString(address1Ordinal);

                        if (!leadsReader.IsDBNull(cityOrdinal)) lead.City = leadsReader.GetString(cityOrdinal);
                        if (!leadsReader.IsDBNull(stateOrdinal)) lead.State = leadsReader.GetString(stateOrdinal);
                        if (!leadsReader.IsDBNull(postcodeOrdinal)) lead.PostalCode = leadsReader.GetString(postcodeOrdinal);
                        if (!leadsReader.IsDBNull(countryOrdinal)) lead.Country = leadsReader.GetString(countryOrdinal);

                        leadCustomerCollection.Add(lead);

                    }
                }

                return leadCustomerCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (leadsReader != null && (!leadsReader.IsClosed))
                    leadsReader.Close();
            }
        }

        public bool Activate(int leadId)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@leadId", leadId, DbType.Int32);
                return this.DataAcessService.ExecuteNonQuery(LeadSql["ActivateLead"], paramCollection) > 0;
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerViewEntity> LeadExist(LeadEntity oLead, string defaultDeptID)
        {
            DbDataReader idrLead = null;
            string leadWhereCls = string.Empty;
            string customerWhereCls = string.Empty;
            List<LeadCustomerViewEntity> oExistingLeadDetails = new List<LeadCustomerViewEntity>();
            LeadCustomerViewEntity oLeadCustomer = null;
            try
            {
                /*
                if (!string.IsNullOrEmpty(oLead.LeadName))
                {
                    leadWhereCls = "WHERE UPPER(l.lead_name) LIKE '" + oLead.LeadName.ToUpper() + "'";
                    customerWhereCls = "WHERE UPPER(c.name) LIKE '" + oLead.LeadName.ToUpper() + "'";
                }

                if (!string.IsNullOrEmpty(oLead.Company) && string.IsNullOrEmpty(leadWhereCls))
                {
                    leadWhereCls = "WHERE l.company='" + oLead.Company + "'";
                    customerWhereCls = "WHERE c.company='" + oLead.Company + "'";
                }
                else if (!string.IsNullOrEmpty(oLead.Company) && !string.IsNullOrEmpty(leadWhereCls))
                {
                    leadWhereCls += " OR l.company='" + oLead.Company + "'";
                    customerWhereCls += " OR c.company='" + oLead.Company + "'";
                }

                if (!string.IsNullOrEmpty(oLead.Telephone) && string.IsNullOrEmpty(leadWhereCls))
                {
                    leadWhereCls = "WHERE l.telephone='" + oLead.Telephone.Trim() + "'";
                    customerWhereCls = "WHERE c.telephone='" + oLead.Telephone.Trim() + "'";
                }
                else if (!string.IsNullOrEmpty(oLead.Telephone) && !string.IsNullOrEmpty(leadWhereCls))
                {
                    leadWhereCls += " OR l.telephone='" + oLead.Telephone.Trim() + "'";
                    customerWhereCls += " OR c.telephone='" + oLead.Telephone.Trim() + "'";
                }

                if (!string.IsNullOrEmpty(oLead.Mobile) && string.IsNullOrEmpty(leadWhereCls))
                {
                    leadWhereCls = "WHERE l.mobile='" + oLead.Mobile.Trim() + "'";
                    customerWhereCls = "WHERE c.mobile='" + oLead.Mobile.Trim() + "'";
                }
                else if (!string.IsNullOrEmpty(oLead.Mobile) && !string.IsNullOrEmpty(leadWhereCls))
                {
                    leadWhereCls += " OR l.mobile='" + oLead.Mobile.Trim() + "'";
                    customerWhereCls += " OR c.mobile='" + oLead.Mobile.Trim() + "'";
                }

                if (!string.IsNullOrEmpty(oLead.EmailAddress) && string.IsNullOrEmpty(leadWhereCls))
                {
                    leadWhereCls = "WHERE l.email_address='" + oLead.EmailAddress + "'";
                    customerWhereCls = "WHERE c.internet_add='" + oLead.EmailAddress + "'";
                }
                else if (!string.IsNullOrEmpty(oLead.EmailAddress) && !string.IsNullOrEmpty(leadWhereCls))
                {
                    leadWhereCls += " OR l.email_address='" + oLead.EmailAddress + "'";
                    customerWhereCls += " OR c.internet_add='" + oLead.EmailAddress + "'";
                }*/

                 DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@departmentId", defaultDeptID, DbType.String);
                paramCollection.Add("@leadName", oLead.LeadName, DbType.String);

                paramCollection.Add("@company", oLead.Company, DbType.String);
                paramCollection.Add("@telephone", oLead.Telephone, DbType.String);
                paramCollection.Add("@mobile", oLead.Mobile, DbType.String);
                paramCollection.Add("emailAddress", oLead.EmailAddress, DbType.String);

                paramCollection.Add("@StartIndex", 0, DbType.Int32);
                paramCollection.Add("@RowCount", 0, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                
		


                idrLead = this.DataAcessService.ExecuteQuery(LeadSql["LeadExist"], paramCollection);

                if (idrLead != null && idrLead.HasRows)
                {
                    int leadIdOrdinal = idrLead.GetOrdinal("lead_id");
                    int nameOrdinal = idrLead.GetOrdinal("name");
                    int telephoneOrdinal = idrLead.GetOrdinal("telephone");
                    int mobileOrdinal = idrLead.GetOrdinal("mobile");
                    int emailAddressOrdinal = idrLead.GetOrdinal("email_address");

                    int customerCodeOrdinal = idrLead.GetOrdinal("cust_code");
                    int CompanyOrdinal = idrLead.GetOrdinal("Company");
                    int address1Ordinal = idrLead.GetOrdinal("address1");
                    //int address2Ordinal = idrLead.GetOrdinal("address2");
                    int cityOrdinal = idrLead.GetOrdinal("city");

                    int stateOrdinal = idrLead.GetOrdinal("state");
                    int postCodeOrdinal = idrLead.GetOrdinal("postcode");
                    int countryOrdinal = idrLead.GetOrdinal("country");
                    int leadStageOrdinal = idrLead.GetOrdinal("lead_stage");
                    while (idrLead.Read())
                    {
                        oLeadCustomer = LeadCustomerViewEntity.CreateObject();

                        if (!idrLead.IsDBNull(leadIdOrdinal)) oLeadCustomer.SourceId = System.Convert.ToInt32(idrLead[leadIdOrdinal]);
                        if (!idrLead.IsDBNull(nameOrdinal)) oLeadCustomer.Name = idrLead.GetString(nameOrdinal);
                        if (!idrLead.IsDBNull(telephoneOrdinal)) oLeadCustomer.Telephone = idrLead.GetString(telephoneOrdinal);
                        if (!idrLead.IsDBNull(mobileOrdinal)) oLeadCustomer.Mobile = idrLead.GetString(mobileOrdinal);
                        if (!idrLead.IsDBNull(emailAddressOrdinal)) oLeadCustomer.Email = idrLead.GetString(emailAddressOrdinal);

                        if (!idrLead.IsDBNull(customerCodeOrdinal)) oLeadCustomer.CustomerCode = idrLead.GetString(customerCodeOrdinal);
                        if (!idrLead.IsDBNull(CompanyOrdinal)) oLeadCustomer.Company = idrLead.GetString(CompanyOrdinal);
                        if (!idrLead.IsDBNull(address1Ordinal)) oLeadCustomer.Address = idrLead.GetString(address1Ordinal);
                        if (!idrLead.IsDBNull(cityOrdinal)) oLeadCustomer.City = idrLead.GetString(cityOrdinal);
                        if (!idrLead.IsDBNull(stateOrdinal)) oLeadCustomer.State = idrLead.GetString(stateOrdinal);

                        if (!idrLead.IsDBNull(postCodeOrdinal)) oLeadCustomer.PostalCode = idrLead.GetString(postCodeOrdinal);
                        if (!idrLead.IsDBNull(countryOrdinal)) oLeadCustomer.Country = idrLead.GetString(countryOrdinal);
                        if (!idrLead.IsDBNull(leadStageOrdinal)) oLeadCustomer.LeadStage = idrLead.GetString(leadStageOrdinal);

                        if (oLeadCustomer.LeadStage == "Customer" && string.IsNullOrEmpty(oLeadCustomer.CustomerCode.Trim()))
                            oLeadCustomer.LeadStage = "Req. Sent";

                        oExistingLeadDetails.Add(oLeadCustomer);
                    }

                }
                return oExistingLeadDetails;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idrLead != null && (!idrLead.IsClosed))
                    idrLead.Close();
            }
        }

        public List<LeadEntity> GetLeadMiniViewCollection(ArgsEntity args)
        {
            DbDataReader leadsReader = null;

            try
            {

                // Retreive the Leads
                List<LeadEntity> leadCollection = new List<LeadEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@Search", args.AdditionalParams, DbType.String);

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

               


                leadsReader = this.DataAcessService.ExecuteQuery(LeadSql["GetAllMiniViewLeads"], paramCollection);

                if (leadsReader.HasRows)
                {
                    int idOrdinal = leadsReader.GetOrdinal("lead_id");
                    int nameOrdinal = leadsReader.GetOrdinal("Name");

                    while (leadsReader.Read())
                    {
                        LeadEntity lead = LeadEntity.CreateObject();

                        if (!leadsReader.IsDBNull(idOrdinal)) lead.LeadID = leadsReader.GetInt32(idOrdinal);
                        if (!leadsReader.IsDBNull(nameOrdinal)) lead.LeadName = leadsReader.GetString(nameOrdinal);

                        leadCollection.Add(lead);

                    }
                }

                return leadCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (leadsReader != null && !leadsReader.IsClosed)
                    leadsReader.Close();
            }
        }

        public int GetPendingCustomersCount(ArgsEntity args)
        {
            DbDataReader customerReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "r.originator"), DbType.String);                
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@RetreiveActive", args.ActiveInactiveChecked, DbType.Boolean);
                paramCollection.Add("@RepType", args.RepType, DbType.String);//cust

                List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();

                customerReader = this.DataAcessService.ExecuteQuery(LeadSql["GetPendingCustomersCount"], paramCollection);

                int pendingCustomersCount = 0;


                if (customerReader.HasRows)
                {
                    int totalCountOrdinal = customerReader.GetOrdinal("total_count");

                    while (customerReader.Read())
                    {
                        if (!customerReader.IsDBNull(totalCountOrdinal)) pendingCustomersCount = customerReader.GetInt32(totalCountOrdinal);                        
                    }
                }
                return pendingCustomersCount;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<LeadCustomerViewEntity> GetOutletsForLookupInScheduleEntry(ArgsEntity args)
        {
            DbDataReader customerReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "r.originator"), DbType.String);
                paramCollection.Add("@ChildOriginatorsCust", args.ChildOriginators.Replace("originator", "r.originator"), DbType.String);//cust
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@RetreiveActive", args.ActiveInactiveChecked, DbType.Boolean);
                paramCollection.Add("@IsReqSentVisible", args.ReqSentIsChecked, DbType.Boolean);
                paramCollection.Add("@RepType", args.RepType, DbType.String);//cust
                paramCollection.Add("@LeadStage", args.LeadStage, DbType.String);
                paramCollection.Add("@DeptString", args.SSource, DbType.String);

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();

                customerReader = this.DataAcessService.ExecuteQuery(LeadSql["GetOutletsForLookupInScheduleEntry"], paramCollection);

                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");

                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");

                    int cityOrdinal = customerReader.GetOrdinal("city");
                    int leadSourceOrdinal = customerReader.GetOrdinal("lead_source");
                    int delFlag = customerReader.GetOrdinal("del_flag");
                    int leadIdOrdinal = customerReader.GetOrdinal("lead_id");
                    int leadStageIdOrdinal = customerReader.GetOrdinal("lead_stage_id");
                    int leadStageOrdinal = customerReader.GetOrdinal("lead_stage");
                    int leadStatusOrdinal = customerReader.GetOrdinal("lead_status");
                    int totalCountOrdinal = customerReader.GetOrdinal("total_count");

                    int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
                    int volumeOrdinal = customerReader.GetOrdinal("volume");
                    int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
                    int categoryOrdinal = customerReader.GetOrdinal("category_name");
                    int MarketOrdinal = customerReader.GetOrdinal("market_name");
                    int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");

                    while (customerReader.Read())
                    {
                        LeadCustomerViewEntity leadCustomerView = LeadCustomerViewEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) leadCustomerView.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(stateOrdinal);
                        leadCustomerView.LeadCustomerType = LeadCustomerTypes.Customer;

                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.State = customerReader.GetString(stateOrdinal).Trim();

                        if (!customerReader.IsDBNull(address1Ordinal))
                            leadCustomerView.Address = leadCustomerView.Address1 = customerReader.GetString(address1Ordinal);

                        if (!customerReader.IsDBNull(address2Ordinal))
                        {
                            leadCustomerView.Address2 = customerReader.GetString(address2Ordinal);
                            leadCustomerView.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);
                        }

                        if (!customerReader.IsDBNull(cityOrdinal)) leadCustomerView.City = customerReader.GetString(cityOrdinal).Trim();
                        if (!customerReader.IsDBNull(telephoneOrdinal)) leadCustomerView.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) leadCustomerView.CustomerCode = customerReader.GetString(custCodeOrdinal).Trim();

                        if (!customerReader.IsDBNull(istlpOrdinal)) leadCustomerView.isTLP = customerReader.GetBoolean(istlpOrdinal);
                        if (!customerReader.IsDBNull(LongitudeIdOrdinal)) leadCustomerView.Longitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        if (!customerReader.IsDBNull(LatitudeIdOrdinal)) leadCustomerView.Latitiude = customerReader.GetDouble(LatitudeIdOrdinal);
                        if (!customerReader.IsDBNull(peripheryOrdinal)) leadCustomerView.Periphery = customerReader.GetString(peripheryOrdinal);
                        if (!customerReader.IsDBNull(outlettypeOrdinal)) leadCustomerView.OutletType = customerReader.GetString(outlettypeOrdinal);
                        if (!customerReader.IsDBNull(categoryOrdinal)) leadCustomerView.Category = customerReader.GetString(categoryOrdinal);
                        if (!customerReader.IsDBNull(MarketOrdinal)) leadCustomerView.MarketName = customerReader.GetString(MarketOrdinal);
                        if (!customerReader.IsDBNull(volumeOrdinal)) leadCustomerView.Volume = (customerReader.GetInt32(volumeOrdinal)).ToString();

                        if (!customerReader.IsDBNull(leadSourceOrdinal)) leadCustomerView.LeadSource = customerReader.GetString(leadSourceOrdinal).Trim();
                        if (!customerReader.IsDBNull(delFlag)) leadCustomerView.IsDeleted = customerReader.GetString(delFlag);
                        if (!customerReader.IsDBNull(leadIdOrdinal)) leadCustomerView.SourceId = customerReader.GetInt32(leadIdOrdinal);
                        if (!customerReader.IsDBNull(leadStageOrdinal)) leadCustomerView.LeadStage = customerReader.GetString(leadStageOrdinal);
                        if (!customerReader.IsDBNull(leadStatusOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(leadStatusOrdinal);
                        if (!customerReader.IsDBNull(totalCountOrdinal)) leadCustomerView.RowCount = customerReader.GetInt32(totalCountOrdinal);

                        leadCollection.Add(leadCustomerView);
                    }
                }
                return leadCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<LeadCustomerViewEntity> GetAllNotInvoicedUsers(ArgsEntity args)
        {
            DbDataReader customerReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "t.rep_code"), DbType.String);
                //paramCollection.Add("@ChildOriginatorsCust", args.ChildOriginators.Replace("originator", "r.originator"), DbType.String);//cust
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                //paramCollection.Add("@AddressType", args.AddressType, DbType.String);
                //paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                //paramCollection.Add("@RetreiveActive", args.ActiveInactiveChecked, DbType.Boolean);
                //paramCollection.Add("@IsReqSentVisible", args.ReqSentIsChecked, DbType.Boolean);

                paramCollection.Add("@RepType", args.RepType, DbType.String);//cust
                //paramCollection.Add("@LeadStage", args.LeadStage, DbType.String);

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                //paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                //paramCollection.Add("@start_date", args.RowCount, DbType.String);
                paramCollection.Add("@start_date", args.DtStartDate, DbType.DateTime);

                // Retreive the Leads



                List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();

                customerReader = this.DataAcessService.ExecuteQuery(LeadSql["GetNotInvoicedCustomers"], paramCollection);


                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    // int businessOrdinal = customerReader.GetOrdinal("business");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");

                    //int telephoneOrdinal = customerReader.GetOrdinal("telephone");

                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");

                    int cityOrdinal = customerReader.GetOrdinal("city");


                    int leadSourceOrdinal = customerReader.GetOrdinal("lead_source");

                    //int delFlag = customerReader.GetOrdinal("del_flag");
                    // int secondaryRepOrdinal = customerReader.GetOrdinal("secondary_rep");

                    //int leadIdOrdinal = customerReader.GetOrdinal("lead_id");
                    int leadStageIdOrdinal = customerReader.GetOrdinal("lead_stage_id");
                    int leadStageOrdinal = customerReader.GetOrdinal("lead_stage");
                    int leadStatusOrdinal = customerReader.GetOrdinal("lead_status");
                    int totalCountOrdinal = customerReader.GetOrdinal("total_count");

                    //int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
                    //int volumeOrdinal = customerReader.GetOrdinal("volume");
                    //int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
                    //int categoryOrdinal = customerReader.GetOrdinal("category_name");
                    //int MarketOrdinal = customerReader.GetOrdinal("market_name");

                    //int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    //    int MarketIdOrdinal = customerReader.GetOrdinal("market_id");
                    //int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    //int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");

                    int dis_idOrdinal = customerReader.GetOrdinal("dis_id");
                    int dis_codeOrdinal = customerReader.GetOrdinal("dis_code");
                    int dis_nameOrdinal = customerReader.GetOrdinal("dis_name");

                    int lstInvoiceDate = customerReader.GetOrdinal("Last_invoice_date");

                    while (customerReader.Read())
                    {
                        LeadCustomerViewEntity leadCustomerView = LeadCustomerViewEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) leadCustomerView.Name = customerReader.GetString(nameOrdinal);
                        //if (!customerReader.IsDBNull(businessOrdinal)) leadCustomerView.Business = customerReader.GetString(businessOrdinal);
                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(stateOrdinal);
                        leadCustomerView.LeadCustomerType = LeadCustomerTypes.Customer;

                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.State = customerReader.GetString(stateOrdinal).Trim();

                        if (!customerReader.IsDBNull(address1Ordinal))
                            leadCustomerView.Address = leadCustomerView.Address1 = customerReader.GetString(address1Ordinal);

                        if (!customerReader.IsDBNull(address2Ordinal))
                        {
                            leadCustomerView.Address2 = customerReader.GetString(address2Ordinal);
                            leadCustomerView.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);
                        }

                        if (!customerReader.IsDBNull(cityOrdinal)) leadCustomerView.City = customerReader.GetString(cityOrdinal).Trim();
                        //if (!customerReader.IsDBNull(emailAddressOrdinal)) leadCustomerView.Email = customerReader.GetString(emailAddressOrdinal);
                        // if (!customerReader.IsDBNull(mobileOrdinal)) leadCustomerView.Mobile = customerReader.GetString(mobileOrdinal);
                        // if (!customerReader.IsDBNull(postcodeOrdinal)) leadCustomerView.PostalCode = customerReader.GetString(postcodeOrdinal);
                        //if (!customerReader.IsDBNull(telephoneOrdinal)) leadCustomerView.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) leadCustomerView.CustomerCode = customerReader.GetString(custCodeOrdinal).Trim();
                        // if (!customerReader.IsDBNull(originatorOrdinal)) leadCustomerView.Originator = customerReader.GetString(originatorOrdinal);

                        //if (!customerReader.IsDBNull(istlpOrdinal)) leadCustomerView.isTLP = customerReader.GetBoolean(istlpOrdinal);
                        //if (!customerReader.IsDBNull(LongitudeIdOrdinal)) leadCustomerView.Longitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        //if (!customerReader.IsDBNull(LatitudeIdOrdinal)) leadCustomerView.Latitiude = customerReader.GetDouble(LatitudeIdOrdinal);
                        //if (!customerReader.IsDBNull(peripheryOrdinal)) leadCustomerView.Periphery = customerReader.GetString(peripheryOrdinal);
                        //if (!customerReader.IsDBNull(outlettypeOrdinal)) leadCustomerView.OutletType = customerReader.GetString(outlettypeOrdinal);
                        //if (!customerReader.IsDBNull(categoryOrdinal)) leadCustomerView.Category = customerReader.GetString(categoryOrdinal);
                        //if (!customerReader.IsDBNull(MarketOrdinal)) leadCustomerView.MarketName = customerReader.GetString(MarketOrdinal);
                        //if (!customerReader.IsDBNull(volumeOrdinal)) leadCustomerView.Volume = customerReader.GetString(volumeOrdinal);

                        if (!customerReader.IsDBNull(leadSourceOrdinal)) leadCustomerView.LeadSource = customerReader.GetString(leadSourceOrdinal).Trim();

                        //if (!customerReader.IsDBNull(delFlag)) leadCustomerView.IsDeleted = customerReader.GetString(delFlag);
                        // if (!customerReader.IsDBNull(secondaryRepOrdinal)) leadCustomerView.SecondaryRepCode = customerReader.GetString(secondaryRepOrdinal);

                        //if (!customerReader.IsDBNull(leadIdOrdinal)) leadCustomerView.SourceId = customerReader.GetInt32(leadIdOrdinal);
                        //if (!customerReader.IsDBNull(leadStageIdOrdinal)) leadCustomerView.le = customerReader.GetString(leadStageIdOrdinal);
                        if (!customerReader.IsDBNull(leadStageOrdinal)) leadCustomerView.LeadStage = customerReader.GetString(leadStageOrdinal);
                        if (!customerReader.IsDBNull(leadStatusOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(leadStatusOrdinal);
                        if (!customerReader.IsDBNull(totalCountOrdinal)) leadCustomerView.RowCount = customerReader.GetInt32(totalCountOrdinal);

                        if (!customerReader.IsDBNull(dis_idOrdinal)) leadCustomerView.DistributorID = customerReader.GetInt32(dis_idOrdinal);
                        if (!customerReader.IsDBNull(dis_codeOrdinal)) leadCustomerView.DistributorCode = customerReader.GetString(dis_codeOrdinal);
                        if (!customerReader.IsDBNull(dis_nameOrdinal)) leadCustomerView.DistributorName = customerReader.GetString(dis_nameOrdinal);

                        if (!customerReader.IsDBNull(lstInvoiceDate)) leadCustomerView.LastInvoiceDate = Convert.ToString(customerReader.GetDateTime(lstInvoiceDate));

                        leadCollection.Add(leadCustomerView);
                    }
                }
                return leadCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }
    }
}
