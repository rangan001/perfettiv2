﻿using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using System.Collections.Generic;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System;
using System.Data.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class LeadStageDAO : BaseDAO
    {
        private DbSqlAdapter LeadStageSql { get; set; }

        public LeadStageDAO()
        {
            this.LeadStageSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.LeadSql.xml", ApplicationService.Instance.DbProvider);
        }

        public LeadStageEntity CreateObject()
        {
            return LeadStageEntity.CreateObject();
        }

        public LeadStageEntity CreateObject(int entityId)
        {
            return LeadStageEntity.CreateObject(entityId);
        }

        public List<LeadStageEntity> GetLeadStageChartData(ArgsEntity args)
        {
            DbDataReader drLeadStage = null;
            List<LeadStageEntity> leadStageCollection = new List<LeadStageEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DefaultDeptID", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@RepType", args.RepType, DbType.String);
                paramCollection.Add("@ChildReps", args.ChildReps.Replace("rep_code", "de.rep_code"), DbType.String);

                drLeadStage = this.DataAcessService.ExecuteQuery(LeadStageSql["GetChart"], paramCollection);

                if (drLeadStage != null && drLeadStage.HasRows)
                {
                    int leadStageOrdinal = drLeadStage.GetOrdinal("lead_stage");
                    int leadCountOrdinal = drLeadStage.GetOrdinal("lead_count");
                    int leadStageIdOrdinal = drLeadStage.GetOrdinal("lead_stage_id");

                    while (drLeadStage.Read())
                    {
                        LeadStageEntity leadStageGraphData = CreateObject();

                        if (!drLeadStage.IsDBNull(leadStageOrdinal)) leadStageGraphData.LeadStage = drLeadStage.GetString(leadStageOrdinal);
                        if (!drLeadStage.IsDBNull(leadCountOrdinal)) leadStageGraphData.LeadsCount = drLeadStage.GetInt32(leadCountOrdinal);
                        if (!drLeadStage.IsDBNull(leadStageIdOrdinal)) leadStageGraphData.LeadStageId = drLeadStage.GetInt32(leadStageIdOrdinal);

                        leadStageCollection.Add(leadStageGraphData);
                    }
                }
                return leadStageCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drLeadStage != null)
                    drLeadStage.Close();
            }
        }

        public List<LeadStageEntity> GetLeadStages(ArgsEntity args)
        {
            DbDataReader drLeadStage = null;
            List<LeadStageEntity> leadStageCollection = new List<LeadStageEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DefaultDeptID", args.DefaultDepartmentId, DbType.String);
                //paramCollection.Add("@OrderBy", string.IsNullOrEmpty(args.OrderBy) ? "" : args.OrderBy, DbType.String);

                drLeadStage = this.DataAcessService.ExecuteQuery(LeadStageSql["GetLeadStages"], paramCollection);

                if (drLeadStage != null && drLeadStage.HasRows)
                {
                    int leadStageIdOrdinal = drLeadStage.GetOrdinal("lead_stage_id");
                    int leadStageOrdinal = drLeadStage.GetOrdinal("lead_Stage");
                    int stageOrderOrdinal = drLeadStage.GetOrdinal("stage_order");
                    int opportunityAllowedOrdinal = drLeadStage.GetOrdinal("opportunity_allowed");
                    int maxOpportunitiesOrdinal = drLeadStage.GetOrdinal("max_opportunities");
                    int emailOrdinal = drLeadStage.GetOrdinal("email");

                    while (drLeadStage.Read())
                    {
                        LeadStageEntity leadStage =CreateObject();

                        if (!drLeadStage.IsDBNull(leadStageIdOrdinal)) leadStage.StageId = drLeadStage.GetInt32(leadStageIdOrdinal);
                        if (!drLeadStage.IsDBNull(leadStageOrdinal)) leadStage.StageName = drLeadStage.GetString(leadStageOrdinal);
                        if (!drLeadStage.IsDBNull(stageOrderOrdinal)) leadStage.StageOrder = drLeadStage.GetInt32(stageOrderOrdinal);
                        if (!drLeadStage.IsDBNull(opportunityAllowedOrdinal)) leadStage.OpportunityAllowed =
                            drLeadStage.GetString(opportunityAllowedOrdinal).ToUpper() == "Y" ? true : false;
                        if (!drLeadStage.IsDBNull(maxOpportunitiesOrdinal)) leadStage.MaxOpportunities = drLeadStage.GetInt32(maxOpportunitiesOrdinal);
                        if (!drLeadStage.IsDBNull(emailOrdinal)) leadStage.EmailAddress = drLeadStage.GetString(emailOrdinal);

                        leadStageCollection.Add(leadStage);

                    }
                }
                return leadStageCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drLeadStage != null)
                    drLeadStage.Close();
            }
        }
    }
}
