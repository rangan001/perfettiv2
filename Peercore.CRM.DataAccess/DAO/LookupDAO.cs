﻿using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using System.Collections.Generic;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System;
using System.Data.Common;
using Peercore.DataAccess.Common;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using System.Linq;

namespace Peercore.CRM.DataAccess.DAO
{
    public class LookupDAO : BaseDAO
    {
        private DbSqlAdapter LookupTableSql { get; set; }

        public LookupDAO()
        {
            this.LookupTableSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.LookupTableSql.xml", ApplicationService.Instance.DbProvider);
        }

        public LookupEntity CreateObject()
        {
            return LookupEntity.CreateObject();
        }

        public void GetValues(ref DataTable dtLookup, string sTableName, string sColumnNames, string sDatabase, string sWhereCls)
        {
            try
            {
                if (sDatabase == "CRM")
                {
                    this.DataAcessService.ExecuteQuery(LookupTableSql["GetLookupValues"].Format(sColumnNames, sTableName,
                        (sWhereCls != "" ? " WHERE " + sWhereCls : "")));
                }
                else
                {
                    this.DataAcessService.ExecuteQuery(LookupTableSql["GetLookupValues"].Format(sColumnNames, sTableName,
                        (sWhereCls != "" ? " WHERE " + sWhereCls : "")));
                }
            }
            catch
            {
                throw;
            }
        }

        public void GetValues(ref DataTable dtLookup, string sql)
        {
            try
            {
               // this.DataAcessService.ExecuteQuery(sql, ref dtLookup);
            }
            catch
            {
                throw;
            }
        }

        public List<LookupEntity> GetValues(string sql)
        {
            LookupEntity lookupTable;
            DbDataReader idrLookup = null;

            try
            {
                List<LookupEntity> LookupCollection = new List<LookupEntity>();

               // idrLookup = (idrLookup)this.DataAcessService.ExecuteSelect(sql);

                if (idrLookup != null && idrLookup.HasRows)
                {
                    int IdOrdinal = idrLookup.GetOrdinal("Id");
                    int codeOrdinal = idrLookup.GetOrdinal("Code");
                    int descriptionOrdinal = idrLookup.GetOrdinal("Description");

                    while (idrLookup.Read())
                    {
                        LookupEntity lookup = CreateObject();

                        if (!idrLookup.IsDBNull(IdOrdinal)) lookup.SourceId = idrLookup.GetValue(IdOrdinal);
                        if (!idrLookup.IsDBNull(codeOrdinal)) lookup.Code = idrLookup.GetValue(codeOrdinal);
                        if (!idrLookup.IsDBNull(descriptionOrdinal)) lookup.Description = idrLookup.GetString(descriptionOrdinal);

                        LookupCollection.Add(lookup);
                    }
                }

                return LookupCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrLookup != null)
                    idrLookup.Close();
            }
        }

        public List<LookupEntity> GetValues(LookupTransferEntity lookupTransferComponent,ArgsEntity args = null)
        {
            LookupEntity lookupTable;
            DbDataReader idrLookup = null;

            try
            {
                List<LookupEntity> LookupCollection = new List<LookupEntity>();

                //drLookup = (IngresDataReader)oDataHandle.ExecuteSelect(lookupTransferComponent.Sql);
                DbSqlStructure sqladapt = new DbSqlStructure();
                sqladapt.Sql = lookupTransferComponent.Sql;
                sqladapt.ProviderType = DbProviderType.SqlServer;
                sqladapt.SqlType = CommandType.Text;

                idrLookup = this.DataAcessService.ExecuteQuery(sqladapt);

                if (idrLookup != null && idrLookup.HasRows)
                {
                    int? IdOrdinal = null;
                    if (lookupTransferComponent.IdColumn.HasValue && !string.IsNullOrWhiteSpace(lookupTransferComponent.IdColumn.Value.Key))
                        IdOrdinal = idrLookup.GetOrdinal(lookupTransferComponent.IdColumn.Value.Key);

                    int? codeOrdinal = null;
                    if (lookupTransferComponent.CodeColumn.HasValue && !string.IsNullOrWhiteSpace(lookupTransferComponent.CodeColumn.Value.Key))
                        codeOrdinal = idrLookup.GetOrdinal(lookupTransferComponent.CodeColumn.Value.Key);

                    int descriptionOrdinal = idrLookup.GetOrdinal(lookupTransferComponent.DescriptionColumn.Key);

                    while (idrLookup.Read())
                    {
                        LookupEntity lookup = CreateObject();

                        if (IdOrdinal.HasValue && !idrLookup.IsDBNull(IdOrdinal.Value)) lookup.SourceId = idrLookup.GetValue(IdOrdinal.Value);
                        if (codeOrdinal.HasValue && !idrLookup.IsDBNull(codeOrdinal.Value)) lookup.Code = idrLookup.GetValue(codeOrdinal.Value);
                        if (!idrLookup.IsDBNull(descriptionOrdinal)) lookup.Description = idrLookup.GetString(descriptionOrdinal);

                        if (lookupTransferComponent.AdditionalColumns != null && lookupTransferComponent.AdditionalColumns.Count > 0)
                        {
                            lookup.AdditionalFields = new List<KeyValuePair<string, object>>();

                            foreach (KeyValuePair<string, string> item in lookupTransferComponent.AdditionalColumns)
                            {
                                if (!idrLookup.IsDBNull(idrLookup.GetOrdinal(item.Key)))
                                    lookup.AdditionalFields.Add(new KeyValuePair<string, object>(item.Value, idrLookup.GetValue(idrLookup.GetOrdinal(item.Key))));
                            }
                        }
                        LookupCollection.Add(lookup);
                    }
                }

                return LookupCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrLookup != null)
                    idrLookup.Close();
            }
        }

        public List<LookupEntity> GetPeercoreLookupValues(string sTableId)
        {
            DbDataReader idrLookup = null;

            try
            {
                List<LookupEntity> LookupCollection = new List<LookupEntity>();

                //drLookup = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetPeercoreLookupValues, sTableId));

                idrLookup = this.DataAcessService.ExecuteQuery(LookupTableSql["GetPeercoreLookupValues"].Format(sTableId));

                if (idrLookup != null && idrLookup.HasRows)
                {
                    int IdOrdinal = idrLookup.GetOrdinal("Id");
                    int codeOrdinal = idrLookup.GetOrdinal("Code");
                    int descriptionOrdinal = idrLookup.GetOrdinal("Description");

                    while (idrLookup.Read())
                    {
                        LookupEntity lookup = CreateObject();

                        if (!idrLookup.IsDBNull(IdOrdinal)) lookup.SourceId = idrLookup.GetValue(IdOrdinal);
                        if (!idrLookup.IsDBNull(codeOrdinal)) lookup.Code = idrLookup.GetValue(codeOrdinal);
                        if (!idrLookup.IsDBNull(descriptionOrdinal)) lookup.Description = idrLookup.GetString(descriptionOrdinal);

                        LookupCollection.Add(lookup);

                    }
                }

                return LookupCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrLookup != null)
                    idrLookup.Close();
            }
        }

        public LookupTransferEntity GetLookUpStatment(string lookupSqlKey, string whereCls = "", object[] parameters = null)
        {
            LookupTransferEntity lookupTransferEntity = new LookupTransferEntity();

            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("Peercore.CRM.DataAccess.SQL.LookupSQL.xml");

            XDocument data = XDocument.Load(s);

            lookupTransferEntity = (from section in data.Descendants("LookupSection")
                                                        where section.Attribute("key").Value.Equals(lookupSqlKey)
                                                        select new LookupTransferEntity()
                                                        {
                                                            Sql = GenerateWhereCls(whereCls, parameters, section),

                                                            IdColumn = new KeyValuePair<string, string>
                                                                (section.Element("KeyFieldColumn").Attribute("columnName").Value, section.Element("KeyFieldColumn").Attribute("displayAlias").Value),

                                                            CodeColumn = new KeyValuePair<string, string>
                                                                (section.Element("CodeColumn").Attribute("columnName").Value, section.Element("CodeColumn").Attribute("displayAlias").Value),

                                                            DescriptionColumn = new KeyValuePair<string, string>
                                                                (section.Element("DescriptionColumn").Attribute("columnName").Value, section.Element("DescriptionColumn").Attribute("displayAlias").Value),

                                                            AdditionalColumns = (from column in section.Element("AdditionalColumns").Descendants("AddedColumn")
                                                                                 select new KeyValuePair<string, string>
                                                                                 (column.Attribute("columnName").Value, column.Attribute("displayAlias").Value)).ToList()
                                                        }).FirstOrDefault();

            return lookupTransferEntity;
        }

        public LookupTransferEntity GenerateSelect(KeyValuePair<string, string> descriptionColumn, KeyValuePair<string, string>? codeColumn = null,
             string primaryKey = "")
        {
            try
            {
                LookupTransferEntity LookupSqlInfo = new LookupTransferEntity()
                {
                    Sql = LookupTableSql["GetRepGroups"].Sql,
                    IdColumn = !string.IsNullOrWhiteSpace(primaryKey) ? new KeyValuePair<string, string>(primaryKey, "") : (KeyValuePair<string, string>?)null,
                    CodeColumn = codeColumn,
                    DescriptionColumn = descriptionColumn
                };

                return LookupSqlInfo;
            }
            catch (Exception)
            {
                throw;
            }

        }

        private static string GenerateWhereCls(string whereCls, object[] parameters, XElement section)
        {
            string sql = section.Element("Sql").Value;
            string formattedSql = sql;
            if (!string.IsNullOrWhiteSpace(whereCls) && (parameters == null || parameters.Count() <= 0))
            {
                formattedSql = string.Format(sql, whereCls);
            }
            else if (string.IsNullOrWhiteSpace(whereCls) && parameters != null && parameters.Count() > 0)
            {
                formattedSql = string.Format(sql, parameters);
            }
            return formattedSql;
        }
    }
}
