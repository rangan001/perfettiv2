﻿using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using System.Collections.Generic;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System;
using System.Data.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class LookupTableDAO : BaseDAO
    {
        private DbSqlAdapter LookupTableSql { get; set; }
        public LookupTableDAO()
        {
            this.LookupTableSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.LookupTableSql.xml", ApplicationService.Instance.DbProvider);
        }

        public LookupTableEntity CreateObject()
        {
            return LookupTableEntity.CreateObject();
        }

        public LookupTableEntity CreateObject(int entityId)
        {
            return LookupTableEntity.CreateObject(entityId);
        }

        public List<LookupTableEntity> GetLookupEntries(string sTableID, string defaultDeptID)
        {
            LookupTableEntity lookupTable;
            DbDataReader idrLookup = null;

            try
            {
                List<LookupTableEntity> lookupCollection = new List<LookupTableEntity>();

                if (sTableID != "")
                    idrLookup = this.DataAcessService.ExecuteQuery(LookupTableSql["GetLookupEntries"].Format(defaultDeptID, " AND table_id = '" + sTableID + "'"));
                else
                    idrLookup = this.DataAcessService.ExecuteQuery(LookupTableSql["GetLookupEntries"].Format(sTableID, ""));

                if (idrLookup != null && idrLookup.HasRows)
                {
                    int tableIdOrdinal = idrLookup.GetOrdinal("table_id");
                    int tableCodeOrdinal = idrLookup.GetOrdinal("table_code");
                    int tableDescOrdinal = idrLookup.GetOrdinal("table_description");
                    int defaultValuecOrdinal = idrLookup.GetOrdinal("default_value");

                    while (idrLookup.Read())
                    {
                        lookupTable = CreateObject();
                        lookupTable.TableID = idrLookup.GetString(tableIdOrdinal);
                        lookupTable.TableCode = idrLookup.GetString(tableCodeOrdinal);
                        lookupTable.TableDescription = idrLookup.GetString(tableDescOrdinal);
                        lookupTable.DefaultValue = idrLookup.GetString(defaultValuecOrdinal);

                        lookupCollection.Add(lookupTable);
                    }
                }

                return lookupCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrLookup != null)
                    idrLookup.Close();
            }
        }

        public LookupTableEntity GetDefaultLookupEntry(string sTableID, string defaultDeptID)
        {
            DbDataReader idrLookup = null;

            try
            {
                LookupTableEntity lookup = CreateObject();

                string whereCls = " AND table_id = '" + sTableID + "'";
                whereCls += " AND default_value='Y'";

                idrLookup = this.DataAcessService.ExecuteQuery(LookupTableSql["GetLookupEntries"].Format(defaultDeptID, whereCls));

                if (idrLookup != null && idrLookup.HasRows)
                {
                    int tableIdOrdinal = idrLookup.GetOrdinal("table_id");
                    int tableCodeOrdinal = idrLookup.GetOrdinal("table_code");
                    int tableDescOrdinal = idrLookup.GetOrdinal("table_description");
                    int defaultValuecOrdinal = idrLookup.GetOrdinal("default_value");

                    if (idrLookup.Read())
                    {
                        lookup.TableID = idrLookup.GetString(tableIdOrdinal);
                        lookup.TableCode = idrLookup.GetString(tableCodeOrdinal);
                        lookup.TableDescription = idrLookup.GetString(tableDescOrdinal);
                        lookup.DefaultValue = idrLookup.GetString(defaultValuecOrdinal);
                    }
                }

                return lookup;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrLookup != null)
                    idrLookup.Close();
            }
        }

        public List<LookupTableEntity> GetSalesMarkets()
        {
            LookupTableEntity lookupTable;
            DbDataReader idrLookup = null;

            try
            {
                List<LookupTableEntity> lookupCollection = new List<LookupTableEntity>();
                idrLookup = this.DataAcessService.ExecuteQuery(LookupTableSql["GetSalesMarkets"]);

                if (idrLookup != null && idrLookup.HasRows)
                {
                    while (idrLookup.Read())
                    {
                        lookupTable = CreateObject();
                        lookupTable.TableCode = ExtensionMethods.GetString(idrLookup, "table_code");
                        lookupTable.TableDescription = ExtensionMethods.GetString(idrLookup, "table_description");

                        lookupCollection.Add(lookupTable);
                    }
                }

                return lookupCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrLookup != null)
                    idrLookup.Close();
            }
        }
        public List<LookupTableEntity> GetRepTerritories( ArgsEntity args)
        {
            string sWhereCls = string.Empty;
            LookupTableEntity lookupTable;
            DbDataReader idrLookup = null;
            try
            {
                
                    sWhereCls = args.ChildOriginators;
                    sWhereCls = " WHERE " + sWhereCls.Replace("originator", "user_name") +
                        " AND state != ''";
                    List<LookupTableEntity> lookupCollection = new List<LookupTableEntity>();
                    idrLookup = this.DataAcessService.ExecuteQuery(LookupTableSql["GetRepTerritory"].Format(sWhereCls));
                    if (idrLookup != null && idrLookup.HasRows)
                    {
                        while (idrLookup.Read())
                        {
                            lookupTable = CreateObject();
                            lookupTable.TableCode = ExtensionMethods.GetString(idrLookup, "state");
                            lookupTable.TableDescription = ExtensionMethods.GetString(idrLookup, "state");

                            lookupCollection.Add(lookupTable);
                        }
                    }

                    return lookupCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrLookup != null)
                    idrLookup.Close();
            }
        }

    }
}
