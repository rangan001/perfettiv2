﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class MarketDAO : BaseDAO
    {
        private DbSqlAdapter MarketSql { get; set; }

        private void RegisterSql()
        {
            this.MarketSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.MarketSql.xml", ApplicationService.Instance.DbProvider);
        }

        public MarketDAO()
        {
            RegisterSql();
        }

        public MarketDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public MarketEntity CreateObject()
        {
            return MarketEntity.CreateObject();
        }

        public MarketEntity CreateObject(int entityId)
        {
            return MarketEntity.CreateObject(entityId);
        }

        public List<MarketEntity> GetAllMarket(ArgsEntity args)
        {
            DbDataReader drMarket = null;

            try
            {
                List<MarketEntity> Marketlist = new List<MarketEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drMarket = this.DataAcessService.ExecuteQuery(MarketSql["GetAllMarket"], paramCollection);

                if (drMarket != null && drMarket.HasRows)
                {
                    int marketIdOrdinal = drMarket.GetOrdinal("market_id");
                    int marketNameOrdinal = drMarket.GetOrdinal("market_name");
                    int statusOrdinal = drMarket.GetOrdinal("status");
                    int originatorOrdinal = drMarket.GetOrdinal("originator");
                    int totalCountOrdinal = drMarket.GetOrdinal("total_count");

                    while (drMarket.Read())
                    {
                        MarketEntity marketEntity = CreateObject();

                        if (!drMarket.IsDBNull(marketIdOrdinal)) marketEntity.MarketId = drMarket.GetInt32(marketIdOrdinal);
                        if (!drMarket.IsDBNull(marketNameOrdinal)) marketEntity.MarketName = drMarket.GetString(marketNameOrdinal);
                        if (!drMarket.IsDBNull(statusOrdinal)) marketEntity.Status = drMarket.GetString(statusOrdinal);
                        if (!drMarket.IsDBNull(originatorOrdinal)) marketEntity.Originator = drMarket.GetString(originatorOrdinal);
                        if (!drMarket.IsDBNull(totalCountOrdinal)) marketEntity.TotalCount = drMarket.GetInt32(totalCountOrdinal);

                        Marketlist.Add(marketEntity);
                    }
                }

                return Marketlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drMarket != null)
                    drMarket.Close();
            }
        }


        public bool InsertMarket(MarketEntity marketEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@marketname", marketEntity.MarketName, DbType.String);
                paramCollection.Add("@status", marketEntity.Status, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MarketSql["InsertMarket"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateMarket(MarketEntity marketEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@marketname", marketEntity.MarketName, DbType.String);
                paramCollection.Add("@marketId", marketEntity.MarketId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MarketSql["UpdateMarket"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        public bool UpdateTMEMarkets(MarketEntity marketEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@originator", marketEntity.Originator, DbType.String);
                paramCollection.Add("@marketId", marketEntity.MarketId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MarketSql["AssignTmeMarkets"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteTMEMarkets(MarketEntity tmeMarket)
        {
            bool DeleteSuccessful = false;
            int iNoRec = 0;
            try
            {
               // if (tmeMarketList.Count > 0)
              //  {
                  //  foreach (MarketEntity tmemarket in tmeMarketList)
                  //  {
                        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                        paramCollection.Add("@MarketId", tmeMarket.MarketId, DbType.Int32);

                        iNoRec = this.DataAcessService.ExecuteNonQuery(MarketSql["DeleteTMEMarkets"], paramCollection);

                        if (iNoRec > 0)
                            DeleteSuccessful = true;



               // }
                return DeleteSuccessful;

            }
            catch
            {
                throw;
            }
        }

        public List<MarketEntity> GetMarketTargetsByDistributorId(ArgsEntity args)
        {
            List<MarketEntity> marketTargets = new List<MarketEntity>();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@EffectiveStartDate", args.DtStartDate.ToString("yyyy-MM-dd"), DbType.String);
            //paramCollection.Add("@EffectiveEndDate", args.DtEndDate.ToString("yyyy-MM-dd"), DbType.String);
            paramCollection.Add("@DistributorId", args.OriginatorId, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(MarketSql["GetMarketTargetByDistributorId"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int marketIdOrdinal = dataReader.GetOrdinal("market_id");
                    int targetIdOrdinal = dataReader.GetOrdinal("target_id");
                    int targetOrdinal = dataReader.GetOrdinal("stick_target");
                    int nameOrdinal = dataReader.GetOrdinal("market_name");

                    MarketEntity marketEntity = null;
                    while (dataReader.Read())
                    {
                        marketEntity = CreateObject();

                        if (!dataReader.IsDBNull(marketIdOrdinal)) marketEntity.MarketId = dataReader.GetInt32(marketIdOrdinal);
                        if (!dataReader.IsDBNull(targetIdOrdinal)) marketEntity.TargetId = dataReader.GetInt32(targetIdOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) marketEntity.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(nameOrdinal)) marketEntity.MarketName = dataReader.GetString(nameOrdinal);

                        marketTargets.Add(marketEntity);
                    }
                }
                return marketTargets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public bool InsertMarketTarget(MarketEntity marketEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@MarketID", marketEntity.MarketId, DbType.Int32);
                paramCollection.Add("@DistributorID", marketEntity.DistributorId, DbType.Int32);
                paramCollection.Add("@Target", marketEntity.Target, DbType.Decimal);
                paramCollection.Add("@StickTarget", marketEntity.Target, DbType.Int32);
                paramCollection.Add("@EffStartDate", marketEntity.EffStartDate, DbType.DateTime);
                paramCollection.Add("@EffEndDate", marketEntity.EffEndDate, DbType.DateTime);
                paramCollection.Add("@CreatedBy", marketEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MarketSql["InsertMarketTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateMarketTarget(MarketEntity marketEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@TargetID", marketEntity.TargetId, DbType.Int32);
                paramCollection.Add("@StickTarget", marketEntity.Target, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", marketEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MarketSql["UpdateMarketTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool IsMarketExists(MarketEntity marketEntity)
        {
            DbDataReader drMarket = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@MarketId", marketEntity.MarketId, DbType.String);
                paramCollection.Add("@MarketName", marketEntity.MarketName, DbType.String);

                drMarket = this.DataAcessService.ExecuteQuery(MarketSql["MarketExist"], paramCollection);

                if (drMarket != null && drMarket.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drMarket != null)
                    drMarket.Close();
            }
        }

    }
}
