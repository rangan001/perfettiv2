﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class MarketObjectiveDAO:BaseDAO
    {
        private DbSqlAdapter MarketObjectiveSql { get; set; }

        private void RegisterSql()
        {
            this.MarketObjectiveSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.MarketObjectiveSql.xml", ApplicationService.Instance.DbProvider);
        }

        public MarketObjectiveDAO()
        {
            RegisterSql();
        }

        public MarketObjectiveDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public MarketObjectiveEntity CreateObject()
        {
            return MarketObjectiveEntity.CreateObject();
        }

        public MarketObjectiveEntity CreateObject(int entityId)
        {
            return MarketObjectiveEntity.CreateObject(entityId);
        }

        public List<MobileMarketObjectiveEntity> GetAllObjectivesMarketWise(string tme, int routeMasterId)
        {
            DbDataReader drObjective = null;

            try
            {
                List<MobileMarketObjectiveEntity> objectiveList = new List<MobileMarketObjectiveEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@tme", tme, DbType.String);
                paramCollection.Add("@routeMasterId", routeMasterId, DbType.Int32);

                drObjective = this.DataAcessService.ExecuteQuery(MarketObjectiveSql["GetAllObjectivesMarketWise"], paramCollection);

                if (drObjective != null && drObjective.HasRows)
                {
                    int marketNameOrdinal = drObjective.GetOrdinal("market_name");
                    int codeOrdinal = drObjective.GetOrdinal("code");
                    int descriptionOrdinal = drObjective.GetOrdinal("description");

                    while (drObjective.Read())
                    {
                        MobileMarketObjectiveEntity objectiveEntity = MobileMarketObjectiveEntity.CreateObject();

                        if (!drObjective.IsDBNull(marketNameOrdinal)) objectiveEntity.MarketName = drObjective.GetString(marketNameOrdinal);
                        if (!drObjective.IsDBNull(codeOrdinal)) objectiveEntity.ObjectiveCode = drObjective.GetString(codeOrdinal);
                        if (!drObjective.IsDBNull(descriptionOrdinal)) objectiveEntity.ObjectiveDescription = drObjective.GetString(descriptionOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }

        public List<ObjectiveEntity> GetAllMarketObjectives(int marketId, ArgsEntity args)
        {
            DbDataReader drObjective = null;

            try
            {
                List<ObjectiveEntity> objectiveList = new List<ObjectiveEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@MarketId", marketId, DbType.Int32);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);

                drObjective = this.DataAcessService.ExecuteQuery(MarketObjectiveSql["GetAllMarketObjectives"], paramCollection);

                if (drObjective != null && drObjective.HasRows)
                {
                    int idOrdinal = drObjective.GetOrdinal("id");
                    int codeOrdinal = drObjective.GetOrdinal("code");
                    int descriptionOrdinal = drObjective.GetOrdinal("description");
                    int totalCountOrdinal = drObjective.GetOrdinal("total_count");

                    ObjectiveEntity objectiveEntity = null;
                    while (drObjective.Read())
                    {
                        objectiveEntity = ObjectiveEntity.CreateObject();

                        if (!drObjective.IsDBNull(idOrdinal)) objectiveEntity.ObjectiveId = drObjective.GetInt32(idOrdinal);
                        if (!drObjective.IsDBNull(codeOrdinal)) objectiveEntity.Code = drObjective.GetString(codeOrdinal);
                        if (!drObjective.IsDBNull(descriptionOrdinal)) objectiveEntity.Description = drObjective.GetString(descriptionOrdinal);
                        if (!drObjective.IsDBNull(totalCountOrdinal)) objectiveEntity.TotalCount = drObjective.GetInt32(totalCountOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }

        public bool InsertMarketObjective(MarketObjectiveEntity marketObjectiveEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@MarketId", marketObjectiveEntity.Market.MarketId, DbType.Int32);
                paramCollection.Add("@ObjectiveId", marketObjectiveEntity.Objective.ObjectiveId, DbType.Int32);
                paramCollection.Add("@CreatedBy", marketObjectiveEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MarketObjectiveSql["InsertMarketObjective"], paramCollection);
                if (iNoRec > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }
    }
}
