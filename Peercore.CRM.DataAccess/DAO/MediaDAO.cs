﻿using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class MediaDAO : BaseDAO
    {
        #region - Media File Upload - 

        public List<UploadMediaModel> GetAllMedia(string repCode, string category, string custCode, ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<UploadMediaModel> Medialist = new List<UploadMediaModel>();
            UploadMediaModel mediaEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllMediaFiles";

                    objCommand.Parameters.AddWithValue("@rep_code", repCode);
                    objCommand.Parameters.AddWithValue("@cust_code", custCode);
                    objCommand.Parameters.AddWithValue("@category", category);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        mediaEntity = new UploadMediaModel();

                        if (item["MediaFileId"] != DBNull.Value) mediaEntity.MediaFileId = Convert.ToInt32(item["MediaFileId"]);
                        if (item["RepCode"] != DBNull.Value) mediaEntity.RepCode = item["RepCode"].ToString();
                        if (item["CustCode"] != DBNull.Value) mediaEntity.CustCode = item["CustCode"].ToString();
                        if (item["MediaFileName"] != DBNull.Value) mediaEntity.MediaFileName = item["MediaFileName"].ToString();
                        if (item["category_name"] != DBNull.Value) mediaEntity.MediaCategory = item["category_name"].ToString();
                        if (item["MediaPath"] != DBNull.Value) mediaEntity.MediaFilePath = item["MediaPath"].ToString();
                        if (item["ThumbnailPath"] != DBNull.Value) mediaEntity.ThumbnailPath = item["ThumbnailPath"].ToString();
                        if (item["FileExtention"] != DBNull.Value) mediaEntity.FileExtention = item["FileExtention"].ToString();
                        if (item["Description"] != DBNull.Value) mediaEntity.Description = item["Description"].ToString();
                        if (item["CreatedBy"] != DBNull.Value) mediaEntity.CreatedBy = item["CreatedBy"].ToString();
                        if (item["total_count"] != DBNull.Value) mediaEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                        mediaEntity.MediaFilePath = mediaEntity.MediaFilePath.Replace(@"\", @"\\");
                        Medialist.Add(mediaEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Medialist;
        }

        public bool SaveUpdateMediaFiles(UploadMediaModel mediaDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spInsertMedia";

                    objCommand.Parameters.AddWithValue("@rep_code", mediaDetail.RepCode);
                    objCommand.Parameters.AddWithValue("@cust_code", mediaDetail.CustCode);
                    objCommand.Parameters.AddWithValue("@media_name", mediaDetail.MediaFileName);
                    objCommand.Parameters.AddWithValue("@catPrefix", mediaDetail.Prefix);
                    objCommand.Parameters.AddWithValue("@FileExtention", mediaDetail.FileExtention);
                    objCommand.Parameters.AddWithValue("@media_path", mediaDetail.MediaFilePath);
                    objCommand.Parameters.AddWithValue("@thumbnail_path", mediaDetail.ThumbnailPath);
                    objCommand.Parameters.AddWithValue("@description", mediaDetail.Description);
                    objCommand.Parameters.AddWithValue("@uploadedBy", mediaDetail.CreatedBy);


                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool DeleteMediaFiles(int mediaId, string ModifiedBy)
        {
            bool retStatus = false;
            //string status,string ModifiedBy, DateTime ModifiedDate
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spDeleteMedia";

                    objCommand.Parameters.AddWithValue("@MediaFileId", mediaId);
                    objCommand.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public List<MediaCategoryModel> GetAllMediaCategoriesForDDL(ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<MediaCategoryModel> categoryList = new List<MediaCategoryModel>();
            MediaCategoryModel categoryEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllMediaCategoriesForDDL";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        categoryEntity = new MediaCategoryModel();

                        if (item["id"] != DBNull.Value) categoryEntity.Id = Convert.ToInt32(item["id"]);
                        if (item["category_name"] != DBNull.Value) categoryEntity.CatName = item["category_name"].ToString();
                        if (item["category_prefix"] != DBNull.Value) categoryEntity.Prefix = item["category_prefix"].ToString();
                        if (item["category_description"] != DBNull.Value) categoryEntity.Description = item["category_description"].ToString();
                        if (item["created_by"] != DBNull.Value) categoryEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) categoryEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        categoryList.Add(categoryEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return categoryList;
        }

        public string LoadCustomerName(string custCode)
        {
            DataTable objDS = new DataTable();
            string userName = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetCustomerName";

                    objCommand.Parameters.AddWithValue("@custCode", custCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        if (item["name"] != DBNull.Value) userName = item["name"].ToString();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return userName;
        }


        #endregion

        #region - Media Category  -

        public List<MediaCategoryModel> GetAllMediaCategories(ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<MediaCategoryModel> categoryList = new List<MediaCategoryModel>();
            MediaCategoryModel categoryEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllMediaCategories";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        categoryEntity = new MediaCategoryModel();

                        if (item["id"] != DBNull.Value) categoryEntity.Id = Convert.ToInt32(item["id"]);
                        if (item["category_name"] != DBNull.Value) categoryEntity.CatName = item["category_name"].ToString();
                        if (item["category_prefix"] != DBNull.Value) categoryEntity.Prefix = item["category_prefix"].ToString();
                        if (item["category_description"] != DBNull.Value) categoryEntity.Description = item["category_description"].ToString();
                        if (item["created_by"] != DBNull.Value) categoryEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) categoryEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        categoryList.Add(categoryEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return categoryList;
        }

        public bool SaveMediaCategoroy(MediaCategoryModel categoryDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spSaveMediaCategory";

                    objCommand.Parameters.AddWithValue("@category_name", categoryDetail.CatName);
                    objCommand.Parameters.AddWithValue("@category_description", categoryDetail.Description);
                    objCommand.Parameters.AddWithValue("@created_by", categoryDetail.CreatedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool UpdateMediaCategory(int catId, string catName, string description, string ModifiedBy)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateMediaCategory";

                    objCommand.Parameters.AddWithValue("@id", catId);
                    objCommand.Parameters.AddWithValue("@category_name", catName);
                    objCommand.Parameters.AddWithValue("@category_description", description);
                    objCommand.Parameters.AddWithValue("@modified_by", ModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool DeleteMediaCategory(int catId, string originator)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spDeleteMediaCategory";

                    objCommand.Parameters.AddWithValue("@id", catId);
                    objCommand.Parameters.AddWithValue("@modified_by", originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        #endregion

    }
}
