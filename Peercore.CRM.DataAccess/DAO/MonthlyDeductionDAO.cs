﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class MonthlyDeductionDAO : BaseDAO
    {
        public List<MonthlyDeductionModel> GetAllDeductions(ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<MonthlyDeductionModel> deductionList = new List<MonthlyDeductionModel>();
            MonthlyDeductionModel deductionEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetDeductions";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        deductionEntity = new MonthlyDeductionModel();

                        if (item["id"] != DBNull.Value) deductionEntity.Id = Convert.ToInt32(item["id"]);
                        if (item["rep_code"] != DBNull.Value) deductionEntity.RepCode = item["rep_code"].ToString();
                        if (item["asset_code"] != DBNull.Value) deductionEntity.AssetCode = item["asset_code"].ToString();
                        //if (item["deduction_month"] != DBNull.Value) deductionEntity.DeductionMonth = Convert.ToInt32(item["deduction_month"]);
                        if (item["deduction_month"] != DBNull.Value) deductionEntity.DeductionMonthName = item["deduction_month"].ToString(); //CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(item["deduction_month"]));
                        if (item["deduction_amount"] != DBNull.Value) deductionEntity.DeductionAmount = Convert.ToDecimal(item["deduction_amount"]);
                        if (item["status"] != DBNull.Value) deductionEntity.Status = item["status"].ToString();

                        if (item["created_by"] != DBNull.Value) deductionEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) deductionEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        deductionList.Add(deductionEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return deductionList;
        }

        public bool SaveDeduction(MonthlyDeductionModel deductionDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spSaveDeduction";

                    //deductionDetail.DeductionMonth = DateTime.ParseExact(deductionDetail.DeductionMonthName.ToString(), "MMMM", CultureInfo.InvariantCulture).Month;

                    objCommand.Parameters.AddWithValue("@deduction_month", deductionDetail.DeductionMonthName);
                    objCommand.Parameters.AddWithValue("@rep_code", deductionDetail.RepCode);
                    objCommand.Parameters.AddWithValue("@asset_code", deductionDetail.AssetCode);
                    objCommand.Parameters.AddWithValue("@deduction_amount", deductionDetail.DeductionAmount);
                    objCommand.Parameters.AddWithValue("@created_by", deductionDetail.CreatedBy);


                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool UpdateDeduction(MonthlyDeductionModel deductionDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateDeduction";

                    objCommand.Parameters.AddWithValue("@id", deductionDetail.Id);

                    objCommand.Parameters.AddWithValue("@deduction_month", deductionDetail.DeductionMonthName);
                    objCommand.Parameters.AddWithValue("@rep_code", deductionDetail.RepCode);
                    objCommand.Parameters.AddWithValue("@asset_code", deductionDetail.AssetCode);
                    objCommand.Parameters.AddWithValue("@deduction_amount", deductionDetail.DeductionAmount);
                    objCommand.Parameters.AddWithValue("@modified_by", deductionDetail.LastModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool DeleteDeduction(int deductionId, string originator)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spDeleteDeduction";

                    objCommand.Parameters.AddWithValue("@id", deductionId);
                    objCommand.Parameters.AddWithValue("@Modified_By", originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool UploadDeductionExcelfile(string FilePath, string UserName, string logPath)
        {
            try
            {
                bool status = false;
                DataTable dt = new DataTable();
                DataTable objDS = new DataTable();

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                    }

                    foreach (Row row in rows)
                    {
                        DataRow tempRow = dt.NewRow();
                        try
                        {
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            if (tempRow[0].ToString() != "")
                                dt.Rows.Add(tempRow);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }

                dt.Rows.RemoveAt(0);
                dt.AcceptChanges();
                // Add columns
                dt.Columns.Add(new DataColumn("UserID", typeof(System.String)));
                dt.Columns.Add(new DataColumn("YearMonth", typeof(System.String)));
                dt.Columns.Add(new DataColumn("RepID", typeof(System.String)));

                // Assign values
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UserID"] = UserName;
                    dr["RepID"] = dr["RepCode"].ToString().PadLeft(5, '0');

                    string b = dr["DeductionMonth"].ToString();
                    double d = double.Parse(b);
                    DateTime conv = DateTime.FromOADate(d);
                    //CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(conv.Month.ToString()))
                    dr["YearMonth"] = conv.Year.ToString() + " " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(conv.Month.ToString()));
                }

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (dt.Rows.Count > 0)
                {
                    using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                    {
                        SqlCommand objCommand = new SqlCommand();
                        objCommand.Connection = conn;
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.CommandText = "spUploadMonthlyDeductions";

                        objCommand.Parameters.AddWithValue("@tblMonthlyDeduction", dt);

                        //Get list of data which alredy exist in the table
                        SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                        objAdap.Fill(objDS);
                        // WriteDeductionLog(objDS, logPath);
                    }
                    status = true;
                }

                return status;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IsRepCodeAvailable(string repCode)
        {
            bool isAvailable = true;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spIsRepCodeAvailable";

                    objCommand.Parameters.AddWithValue("@repCode", repCode);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    int rtn = (int)objCommand.Parameters["@ReturnVal"].Value;
                    if (rtn == 0)
                    {
                        isAvailable = false;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return isAvailable;
        }


        #region - Common Methods -

        //--using openXml
        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;
            if (cell.CellValue == null)
            {
                return "";
            }
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        public static int? GetColumnIndexFromName(string columnName)
        {
            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        #endregion

    }
}
