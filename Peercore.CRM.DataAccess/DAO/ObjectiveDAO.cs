﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;


namespace Peercore.CRM.DataAccess.DAO
{
    public class ObjectiveDAO : BaseDAO
    {
        private DbSqlAdapter ObjectiveSql { get; set; }

        private void RegisterSql()
        {
            this.ObjectiveSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ObjectiveSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ObjectiveDAO()
        {
            RegisterSql();
        }

        public ObjectiveDAO(DbWorkflowScope scope)
            : base(scope)
        {
           RegisterSql();
        }

        public ObjectiveEntity CreateObject()
        {
            return ObjectiveEntity.CreateObject();
        }

        public ObjectiveEntity CreateObject(int entityId)
        {
            return ObjectiveEntity.CreateObject(entityId);
        }

        public List<ObjectiveEntity> GetAllObjectives(ArgsEntity args)
        {
            DbDataReader drObjective = null;

            try
            {
                List<ObjectiveEntity> objectiveList = new List<ObjectiveEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drObjective = this.DataAcessService.ExecuteQuery(ObjectiveSql["GetAllObjectives"], paramCollection);

                if (drObjective != null && drObjective.HasRows)
                {
                    int idOrdinal = drObjective.GetOrdinal("id");
                    int codeOrdinal = drObjective.GetOrdinal("code");
                    int descriptionOrdinal = drObjective.GetOrdinal("description");
                    int statusOrdinal = drObjective.GetOrdinal("status");
                    int createdbyOrdinal = drObjective.GetOrdinal("created_by");
                    int createddateOrdinal = drObjective.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drObjective.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drObjective.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drObjective.GetOrdinal("total_count");

                    while (drObjective.Read())
                    {
                        ObjectiveEntity objectiveEntity = CreateObject();

                        if (!drObjective.IsDBNull(idOrdinal)) objectiveEntity.ObjectiveId = drObjective.GetInt32(idOrdinal);
                        if (!drObjective.IsDBNull(codeOrdinal)) objectiveEntity.Code = drObjective.GetString(codeOrdinal);
                        if (!drObjective.IsDBNull(descriptionOrdinal)) objectiveEntity.Description = drObjective.GetString(descriptionOrdinal);
                        if (!drObjective.IsDBNull(statusOrdinal)) objectiveEntity.Status = drObjective.GetString(statusOrdinal);
                        if (!drObjective.IsDBNull(createdbyOrdinal)) objectiveEntity.CreatedBy = drObjective.GetString(createdbyOrdinal);
                        if (!drObjective.IsDBNull(createddateOrdinal)) objectiveEntity.CreatedDate = drObjective.GetDateTime(createddateOrdinal);
                        if (!drObjective.IsDBNull(lastmodifiedbyOrdinal)) objectiveEntity.LastModifiedBy = drObjective.GetString(lastmodifiedbyOrdinal);
                        if (!drObjective.IsDBNull(lastmodifieddateOrdinal)) objectiveEntity.LastModifiedDate = drObjective.GetDateTime(lastmodifieddateOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }


        public List<ObjectiveEntity> GetCustomerObjectives(ArgsEntity args)
        {
            DbDataReader drObjective = null;

            try
            {
                List<ObjectiveEntity> objectiveList = new List<ObjectiveEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drObjective = this.DataAcessService.ExecuteQuery(ObjectiveSql["GetCustomerObjectives"], paramCollection);

                if (drObjective != null && drObjective.HasRows)
                {
                    int idOrdinal = drObjective.GetOrdinal("id");
                    int codeOrdinal = drObjective.GetOrdinal("code");
                    int descriptionOrdinal = drObjective.GetOrdinal("description");
                    int statusOrdinal = drObjective.GetOrdinal("status");
                    int createdbyOrdinal = drObjective.GetOrdinal("created_by");
                    int createddateOrdinal = drObjective.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drObjective.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drObjective.GetOrdinal("last_modified_date");
                    int custCodeOrdinal = drObjective.GetOrdinal("cust_code");
                    int totalCountOrdinal = drObjective.GetOrdinal("total_count");

                    while (drObjective.Read())
                    {
                        ObjectiveEntity objectiveEntity = CreateObject();

                        if (!drObjective.IsDBNull(idOrdinal)) objectiveEntity.ObjectiveId = drObjective.GetInt32(idOrdinal);
                        if (!drObjective.IsDBNull(codeOrdinal)) objectiveEntity.Code = drObjective.GetString(codeOrdinal);
                        if (!drObjective.IsDBNull(descriptionOrdinal)) objectiveEntity.Description = drObjective.GetString(descriptionOrdinal);
                        if (!drObjective.IsDBNull(statusOrdinal)) objectiveEntity.Status = drObjective.GetString(statusOrdinal);
                        if (!drObjective.IsDBNull(createdbyOrdinal)) objectiveEntity.CreatedBy = drObjective.GetString(createdbyOrdinal);
                        if (!drObjective.IsDBNull(createddateOrdinal)) objectiveEntity.CreatedDate = drObjective.GetDateTime(createddateOrdinal);
                        if (!drObjective.IsDBNull(lastmodifiedbyOrdinal)) objectiveEntity.LastModifiedBy = drObjective.GetString(lastmodifiedbyOrdinal);

                        if (!drObjective.IsDBNull(custCodeOrdinal))
                        { objectiveEntity.CustomerCode = drObjective.GetString(custCodeOrdinal); }
                        else
                        { objectiveEntity.CustomerCode = string.Empty; }

                        if (!drObjective.IsDBNull(lastmodifieddateOrdinal)) objectiveEntity.LastModifiedDate = drObjective.GetDateTime(lastmodifieddateOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }

        public List<MobileObjectiveEntity> GetAllNationalObjectives()
        {
            DbDataReader drObjective = null;

            try
            {
                List<MobileObjectiveEntity> objectiveList = new List<MobileObjectiveEntity>();

                drObjective = this.DataAcessService.ExecuteQuery(ObjectiveSql["GetAllNationalObjectives"]);

                if (drObjective != null && drObjective.HasRows)
                {
                    int codeOrdinal = drObjective.GetOrdinal("code");
                    int descriptionOrdinal = drObjective.GetOrdinal("description");

                    while (drObjective.Read())
                    {
                        MobileObjectiveEntity objectiveEntity = MobileObjectiveEntity.CreateObject();

                        if (!drObjective.IsDBNull(codeOrdinal)) objectiveEntity.ObjectiveCode = drObjective.GetString(codeOrdinal);
                        if (!drObjective.IsDBNull(descriptionOrdinal)) objectiveEntity.Description = drObjective.GetString(descriptionOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }

        public List<MobileObjectiveEntity> GetAllObjectivesForMarket(int marketId)
        {
            DbDataReader drObjective = null;

            try
            {
                List<MobileObjectiveEntity> objectiveList = new List<MobileObjectiveEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@marketID", marketId, DbType.Int32);

                drObjective = this.DataAcessService.ExecuteQuery(ObjectiveSql["GetAllObjectivesForMarket"], paramCollection);

                if (drObjective != null && drObjective.HasRows)
                {
                    int codeOrdinal = drObjective.GetOrdinal("code");
                    int descriptionOrdinal = drObjective.GetOrdinal("description");

                    while (drObjective.Read())
                    {
                        MobileObjectiveEntity objectiveEntity = MobileObjectiveEntity.CreateObject();

                        if (!drObjective.IsDBNull(codeOrdinal)) objectiveEntity.ObjectiveCode = drObjective.GetString(codeOrdinal);
                        if (!drObjective.IsDBNull(descriptionOrdinal)) objectiveEntity.Description = drObjective.GetString(descriptionOrdinal);
                        
                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }

        public bool InsertObjective(out int objectiveId, ObjectiveEntity objectiveEntity)
        {
            bool successful = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Code", objectiveEntity.Code, DbType.String);
                paramCollection.Add("@Description", objectiveEntity.Description, DbType.String);
                paramCollection.Add("@Type", objectiveEntity.Type, DbType.String);
                paramCollection.Add("@CreatedBy", objectiveEntity.CreatedBy, DbType.String);
                paramCollection.Add("@ID", null, DbType.Int32, ParameterDirection.Output);

                objectiveId = this.DataAcessService.ExecuteNonQuery(ObjectiveSql["InsertObjective"], paramCollection, true, "@ID");
                if (objectiveId > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

        public bool UpdateObjective(ObjectiveEntity objectiveEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ObjectiveId", objectiveEntity.ObjectiveId, DbType.Int32);
                paramCollection.Add("@Code", objectiveEntity.Code, DbType.String);
                paramCollection.Add("@Description", objectiveEntity.Description, DbType.String);
                paramCollection.Add("@Type", objectiveEntity.Type, DbType.String);
                paramCollection.Add("@LastModifiedBy", objectiveEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ObjectiveSql["UpdateObjective"], paramCollection);
                if (iNoRec > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

        public bool IsObjectiveCodeExists(ObjectiveEntity objectiveEntity)
        {
            DbDataReader drObjective = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ObjectiveId", objectiveEntity.ObjectiveId, DbType.String);
                paramCollection.Add("@ObjectiveCode", objectiveEntity.Code, DbType.String);

                drObjective = this.DataAcessService.ExecuteQuery(ObjectiveSql["ObjectiveCodeExists"], paramCollection);

                if (drObjective != null && drObjective.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }

        public bool InsertCustomerObjective(MobileObjectiveEntity objectiveEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", objectiveEntity.CustomerCode, DbType.String);
                paramCollection.Add("@ObjectiveId", objectiveEntity.ObjectiveId, DbType.Int32);
                paramCollection.Add("@CreatedBy", objectiveEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ObjectiveSql["InsertCustomerObjective"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Updates All the records For today, as Deactivated For the customer
        /// (only Todays record for the relevent customer)
        /// </summary>
        /// <param name="objectiveEntity"></param>
        /// <returns></returns>
        public bool DeleteAllCustomerObjectivesForToday(MobileObjectiveEntity objectiveEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", objectiveEntity.CustomerCode, DbType.String);
                paramCollection.Add("@LastModifiedBy", objectiveEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ObjectiveSql["DeleteAllCustomerObjectivesForToday"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
    }
}
