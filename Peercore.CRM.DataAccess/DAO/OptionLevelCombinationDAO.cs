﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class OptionLevelCombinationDAO :BaseDAO
    {
        private DbSqlAdapter OptionLevelSql { get; set; }

        private void RegisterSql()
        {
            this.OptionLevelSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.OptionLevelSql.xml", ApplicationService.Instance.DbProvider);
        }

        public OptionLevelCombinationDAO()
        {
            RegisterSql();
        }

        public OptionLevelCombinationDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public OptionLevelCombinationEntity CreateObject()
        {
            return OptionLevelCombinationEntity.CreateObject();
        }

        public OptionLevelCombinationEntity CreateObject(int entityId)
        {
            return OptionLevelCombinationEntity.CreateObject(entityId);
        }

        public bool InsertOptionLevelCombination(OptionLevelCombinationEntity optionLevelCombinationEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drOption = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@OptionLevelCombinationId", optionLevelCombinationEntity.OptionLevelCombinationId, DbType.Int32);
                paramCollection.Add("@OptionLevelId", optionLevelCombinationEntity.OptionLevelId, DbType.Int32);
                paramCollection.Add("@ProductId", optionLevelCombinationEntity.ProductId, DbType.Int32);
                paramCollection.Add("@IsExceptProduct", optionLevelCombinationEntity.IsExceptProduct, DbType.Double);
                paramCollection.Add("@ProductCatagoryId", optionLevelCombinationEntity.ProductCategoryId, DbType.Int32);
                paramCollection.Add("@ProductPackingId", optionLevelCombinationEntity.ProductPackingId, DbType.Int32);
                paramCollection.Add("@BrandId", optionLevelCombinationEntity.BrandId, DbType.Int32);
                //paramCollection.Add("@ItemTypeId", optionLevelCombinationEntity.ItemTypeId, DbType.Int32);
                paramCollection.Add("@CreatedBy", optionLevelCombinationEntity.CreatedBy, DbType.String);
                paramCollection.Add("@Ishvp", optionLevelCombinationEntity.IsHvp.HasValue ? optionLevelCombinationEntity.IsHvp : null, DbType.Boolean);
                paramCollection.Add("@FlavorId", optionLevelCombinationEntity.FlavorId, DbType.Int32);
                paramCollection.Add("@ConditionType", optionLevelCombinationEntity.conditionType, DbType.String);	
	
                iNoRec = this.DataAcessService.ExecuteNonQuery(OptionLevelSql["InsertOptionLevelCombination"], paramCollection);
                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drOption != null)
                    drOption.Close();
            }
        }

        public List<OptionLevelCombinationEntity> GetOptionLevelCombinationByOptionLevelId(int optionLevelId)
        {
            DbDataReader drOptionLevel = null;
            try
            {
                List<OptionLevelCombinationEntity> OptionLevelCombinationCollection = new List<OptionLevelCombinationEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OptionLevelId", optionLevelId, DbType.Int32);

                drOptionLevel = this.DataAcessService.ExecuteQuery(OptionLevelSql["GetOptionLevelCombinationByoptionLevelId"], paramCollection);
                if (drOptionLevel != null && drOptionLevel.HasRows)
                {
                    int idOrdinal = drOptionLevel.GetOrdinal("option_level_combination_id");
                    int optionLevelIdOrdinal = drOptionLevel.GetOrdinal("option_level_id");
                    int productCatagoryIdOrdinal = drOptionLevel.GetOrdinal("product_catagory_id");
                    int productPackingIdOrdinal = drOptionLevel.GetOrdinal("product_packing_id");
                    int productIdOrdinal = drOptionLevel.GetOrdinal("product_id");
                    int brandIdOrdinal = drOptionLevel.GetOrdinal("brand_id");
                    int isExceptProductOrdinal = drOptionLevel.GetOrdinal("is_except_product");
                    
                    int ishvpOrdinal = drOptionLevel.GetOrdinal("ishvp");
                    int flavorIdOrdinal = drOptionLevel.GetOrdinal("flavor_id");
                    int productCategoryOrdinal = drOptionLevel.GetOrdinal("product_category_name");
                    int packingNameOrdinal = drOptionLevel.GetOrdinal("packing_name");
                    int brandNameOrdinal = drOptionLevel.GetOrdinal("brand_name");
                    int flavorNameOrdinal = drOptionLevel.GetOrdinal("flavor_name");
                    int productNameOrdinal = drOptionLevel.GetOrdinal("product_name");

                    int conditionTypeOrdinal = drOptionLevel.GetOrdinal("condition_type");

                    while (drOptionLevel.Read())
                    {
                        OptionLevelCombinationEntity optionLevelCombinationEntity = OptionLevelCombinationEntity.CreateObject();

                        if (!drOptionLevel.IsDBNull(idOrdinal)) optionLevelCombinationEntity.OptionLevelCombinationId = drOptionLevel.GetInt32(idOrdinal);
                        if (!drOptionLevel.IsDBNull(optionLevelIdOrdinal)) optionLevelCombinationEntity.OptionLevelId = drOptionLevel.GetInt32(optionLevelIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productCatagoryIdOrdinal)) optionLevelCombinationEntity.ProductCategoryId = drOptionLevel.GetInt32(productCatagoryIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productPackingIdOrdinal)) optionLevelCombinationEntity.ProductPackingId = drOptionLevel.GetInt32(productPackingIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productIdOrdinal)) optionLevelCombinationEntity.ProductId = drOptionLevel.GetInt32(productIdOrdinal);
                        if (!drOptionLevel.IsDBNull(brandIdOrdinal)) optionLevelCombinationEntity.BrandId = drOptionLevel.GetInt32(brandIdOrdinal);
                        if (!drOptionLevel.IsDBNull(isExceptProductOrdinal)) optionLevelCombinationEntity.IsExceptProduct = drOptionLevel.GetBoolean(isExceptProductOrdinal);

                        if (!drOptionLevel.IsDBNull(ishvpOrdinal)) optionLevelCombinationEntity.IsHvp = drOptionLevel.GetBoolean(ishvpOrdinal);
                        if (!drOptionLevel.IsDBNull(flavorIdOrdinal)) optionLevelCombinationEntity.FlavorId = drOptionLevel.GetInt32(flavorIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productCategoryOrdinal)) optionLevelCombinationEntity.ProductCatagoryName = drOptionLevel.GetString(productCategoryOrdinal);
                        if (!drOptionLevel.IsDBNull(packingNameOrdinal)) optionLevelCombinationEntity.ProductPackingName = drOptionLevel.GetString(packingNameOrdinal);
                        if (!drOptionLevel.IsDBNull(brandNameOrdinal)) optionLevelCombinationEntity.BrandName = drOptionLevel.GetString(brandNameOrdinal);
                        if (!drOptionLevel.IsDBNull(flavorNameOrdinal)) optionLevelCombinationEntity.FlavorName = drOptionLevel.GetString(flavorNameOrdinal);
                        if (!drOptionLevel.IsDBNull(productNameOrdinal)) optionLevelCombinationEntity.ProductName = drOptionLevel.GetString(productNameOrdinal);
                        if (!drOptionLevel.IsDBNull(conditionTypeOrdinal)) optionLevelCombinationEntity.conditionType = drOptionLevel.GetString(conditionTypeOrdinal);

                        OptionLevelCombinationCollection.Add(optionLevelCombinationEntity);
                    }
                }

                return OptionLevelCombinationCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOptionLevel != null)
                    drOptionLevel.Close();
            }
        }

        public bool UpdateOLCombinationInActive(int schemeHeaderId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SchemeHeaderId", schemeHeaderId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OptionLevelSql["UpdateOLCombinationInActive"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public List<OptionLevelCombinationEntity> GetOptionLevelCombinationByDivisionId(int divisionId)
        {
            DbDataReader drOptionLevel = null;
            try
            {
                List<OptionLevelCombinationEntity> OptionLevelCombinationCollection = new List<OptionLevelCombinationEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);

                drOptionLevel = this.DataAcessService.ExecuteQuery(OptionLevelSql["GetOptionLevelCombinationByDivisionId"], paramCollection);
                if (drOptionLevel != null && drOptionLevel.HasRows)
                {
                    int idOrdinal = drOptionLevel.GetOrdinal("option_level_combination_id");
                    int optionLevelIdOrdinal = drOptionLevel.GetOrdinal("option_level_id");
                    int productCatagoryIdOrdinal = drOptionLevel.GetOrdinal("product_catagory_id");
                    int productPackingIdOrdinal = drOptionLevel.GetOrdinal("product_packing_id");
                    int productIdOrdinal = drOptionLevel.GetOrdinal("product_id");
                    int brandIdOrdinal = drOptionLevel.GetOrdinal("brand_id");
                    int isExceptProductOrdinal = drOptionLevel.GetOrdinal("is_except_product");
                    
                    int ishvpOrdinal = drOptionLevel.GetOrdinal("ishvp");
                    int catlogIdOrdinal = drOptionLevel.GetOrdinal("flavor_id");
                    //int productCategoryOrdinal = drOptionLevel.GetOrdinal("product_category_name");
                    //int packingNameOrdinal = drOptionLevel.GetOrdinal("packing_name");
                    //int brandNameOrdinal = drOptionLevel.GetOrdinal("brand_name");
                    //int catalogNameOrdinal = drOptionLevel.GetOrdinal("catalog_name");


                    while (drOptionLevel.Read())
                    {
                        OptionLevelCombinationEntity optionLevelCombinationEntity = OptionLevelCombinationEntity.CreateObject();

                        if (!drOptionLevel.IsDBNull(idOrdinal)) optionLevelCombinationEntity.OptionLevelCombinationId = drOptionLevel.GetInt32(idOrdinal);
                        if (!drOptionLevel.IsDBNull(optionLevelIdOrdinal)) optionLevelCombinationEntity.OptionLevelId = drOptionLevel.GetInt32(optionLevelIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productCatagoryIdOrdinal)) optionLevelCombinationEntity.ProductCategoryId = drOptionLevel.GetInt32(productCatagoryIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productPackingIdOrdinal)) optionLevelCombinationEntity.ProductPackingId = drOptionLevel.GetInt32(productPackingIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productIdOrdinal)) optionLevelCombinationEntity.ProductId = drOptionLevel.GetInt32(productIdOrdinal);
                        if (!drOptionLevel.IsDBNull(brandIdOrdinal)) optionLevelCombinationEntity.BrandId = drOptionLevel.GetInt32(brandIdOrdinal);
                        if (!drOptionLevel.IsDBNull(isExceptProductOrdinal)) optionLevelCombinationEntity.IsExceptProduct = drOptionLevel.GetBoolean(isExceptProductOrdinal);

                        if (!drOptionLevel.IsDBNull(ishvpOrdinal)) optionLevelCombinationEntity.IsHvp = drOptionLevel.GetBoolean(ishvpOrdinal);
                        if (!drOptionLevel.IsDBNull(catlogIdOrdinal)) optionLevelCombinationEntity.FlavorId = drOptionLevel.GetInt32(catlogIdOrdinal);
                        //if (!drOptionLevel.IsDBNull(productCategoryOrdinal)) optionLevelCombinationEntity.ProductCatagoryName = drOptionLevel.GetString(productCategoryOrdinal);
                        //if (!drOptionLevel.IsDBNull(packingNameOrdinal)) optionLevelCombinationEntity.ProductPackingName = drOptionLevel.GetString(packingNameOrdinal);
                        //if (!drOptionLevel.IsDBNull(brandNameOrdinal)) optionLevelCombinationEntity.BrandName = drOptionLevel.GetString(brandNameOrdinal);
                        //if (!drOptionLevel.IsDBNull(catalogNameOrdinal)) optionLevelCombinationEntity.CatlogName = drOptionLevel.GetString(catalogNameOrdinal);


                        OptionLevelCombinationCollection.Add(optionLevelCombinationEntity);
                    }
                }

                return OptionLevelCombinationCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOptionLevel != null)
                    drOptionLevel.Close();
            }
        }


        
    }
}
