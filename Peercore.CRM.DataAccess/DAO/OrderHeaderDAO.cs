﻿using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class OrderHeaderDAO : BaseDAO
    {
        private DbSqlAdapter OrderHeaderSql { get; set; }

        private void RegisterSql()
        {
            this.OrderHeaderSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.OrderHeaderSql.xml", ApplicationService.Instance.DbProvider);
        }

        public OrderHeaderDAO()
        {
            RegisterSql();
        }

        public OrderHeaderDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public bool IsOrdedrNoExist(string orderNo)
        {
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "IsOrdedrNoExist";

                    objCommand.Parameters.AddWithValue("@OrderNo", orderNo);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool InsertOrderHeader(out int invoiceId, OrderModel order)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", order.CustomerCode, DbType.String);
                paramCollection.Add("@OrderNo", order.OrderNo, DbType.String);
                paramCollection.Add("@OrderDate", order.OrderDate, DbType.DateTime);
                paramCollection.Add("@RequiredDate", order.RequiredDate.Trim().Substring(0, 10) + " " + "13:00", DbType.DateTime);
                paramCollection.Add("@GrossTotal", order.OrderTotal, DbType.Double);
                paramCollection.Add("@DiscountTotal", order.TotalDiscount, DbType.String);
                paramCollection.Add("@ReturnsTotal", 0, DbType.Double);
                paramCollection.Add("@RepCode", order.RepCode, DbType.String);
                paramCollection.Add("@GrandTotal", 0, DbType.Double);
                paramCollection.Add("@Status", (order.Canceled == false) ? "A" : "D", DbType.String);
                paramCollection.Add("@Remark", order.Remark, DbType.String);
                paramCollection.Add("@Longitude", order.Longitude, DbType.Double);
                paramCollection.Add("@Latitude", order.Latitude, DbType.Double);
                paramCollection.Add("@RouteId", order.RouteId, DbType.Int32);
                paramCollection.Add("@TerritoryId", order.TerritoryId, DbType.Int32);
                paramCollection.Add("@AreaId", order.AreaId, DbType.Int32);
                paramCollection.Add("@RegionId", order.RegionId, DbType.Int32);
                paramCollection.Add("@DistributorId", order.DistributorId, DbType.Int32);
                paramCollection.Add("@AsmId", order.AsmId, DbType.Int32);
                paramCollection.Add("@RsmId", order.RsmId, DbType.Int32);
                paramCollection.Add("@PayType", order.payType, DbType.String);
                paramCollection.Add("@OrderId", null, DbType.Int32, ParameterDirection.Output);

                invoiceId = this.DataAcessService.ExecuteNonQuery(OrderHeaderSql["InsertOrderHeader"], paramCollection, true, "@OrderId");

                if (invoiceId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool UpdateOrderHeader(OrderModel order)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", order.OrderId, DbType.Int32);
                paramCollection.Add("@CustCode", order.CustomerCode, DbType.String);
                paramCollection.Add("@OrderNo", order.OrderNo, DbType.String);
                paramCollection.Add("@OrderDate", order.OrderDate, DbType.DateTime);
                paramCollection.Add("@RequiredDate", order.RequiredDate.Trim().Substring(0, 10) + " " + "13:00", DbType.DateTime);
                paramCollection.Add("@GrossTotal", order.OrderTotal, DbType.Double);
                paramCollection.Add("@DiscountTotal", order.TotalDiscount, DbType.String);
                paramCollection.Add("@ReturnsTotal", 0, DbType.Double);
                paramCollection.Add("@RepCode", order.RepCode, DbType.String);
                paramCollection.Add("@GrandTotal", 0, DbType.Double);
                paramCollection.Add("@Status", (order.Canceled == false) ? "A" : "D", DbType.String);
                paramCollection.Add("@Remark", order.Remark, DbType.String);
                paramCollection.Add("@Longitude", order.Longitude, DbType.Double);
                paramCollection.Add("@Latitude", order.Latitude, DbType.Double);
                paramCollection.Add("@RouteId", order.RouteId, DbType.Int32);
                paramCollection.Add("@TerritoryId", order.TerritoryId, DbType.Int32);
                paramCollection.Add("@AreaId", order.AreaId, DbType.Int32);
                paramCollection.Add("@RegionId", order.RegionId, DbType.Int32);
                paramCollection.Add("@DistributorId", order.DistributorId, DbType.Int32);
                paramCollection.Add("@AsmId", order.AsmId, DbType.Int32);
                paramCollection.Add("@RsmId", order.RsmId, DbType.Int32);
                paramCollection.Add("@PayType", order.payType, DbType.String);

                int ret = this.DataAcessService.ExecuteNonQuery(OrderHeaderSql["UpdateOrderHeader"], paramCollection);

                if (ret > 0)
                    successful = true;

                return successful;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool DeleteOrderHeader(OrderModel order)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", order.OrderId, DbType.Int32);
                paramCollection.Add("@Modified_By", order.CreateBy, DbType.String);

                int retId = this.DataAcessService.ExecuteNonQuery(OrderHeaderSql["DeleteOrderHeader"], paramCollection);

                if (retId > 0)
                    successful = true;

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                
            }

            return successful;
        }

        public bool DeleteOrderHeaderByOrderNo(string originator, string orderNo)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteOrderHeaderByOrderNo";

                    objCommand.Parameters.AddWithValue("@OrderNo", orderNo);
                    objCommand.Parameters.AddWithValue("@Modified_By", originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;
        }

        public bool ArchiveOrderHeader(OrderModel order)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", order.OrderId, DbType.Int32);
                paramCollection.Add("@Modified_By", order.CreateBy, DbType.String);

                int retId = this.DataAcessService.ExecuteNonQuery(OrderHeaderSql["ArchiveOrderHeader"], paramCollection);

                if (retId > 0)
                    successful = true;

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }

            return successful;
        }

        public bool UpdateOrderIsInvoiceActivate(OrderIsInvoiceActivate request)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", request.originator, DbType.String);
                paramCollection.Add("@OriginatorType", request.originatorType, DbType.String);
                paramCollection.Add("@OrderId", request.orderId, DbType.Int32);
                paramCollection.Add("@IsActive", request.isActive, DbType.Boolean);

                int retId = this.DataAcessService.ExecuteNonQuery(OrderHeaderSql["UpdateOrderIsInvoiceActivate"], paramCollection);

                if (retId > 0)
                    successful = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }

            return successful;
        }

        public bool InsertOrderProductDiscount(int orderId)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", orderId, DbType.Int32);

                int ret = this.DataAcessService.ExecuteNonQuery(OrderHeaderSql["InsertOrderDetailsWithDiscounts"], paramCollection);

                if (ret > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public OrderModel GetOrderHeaderByOrderNo(string orderNo)
        {
            OrderModel orderEntity = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOrderHeaderByOrderNo";

                    objCommand.Parameters.AddWithValue("@OrderNo", orderNo);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        orderEntity = new OrderModel();
                        if (item["id"] != DBNull.Value) orderEntity.OrderId = Convert.ToInt32(item["id"]);
                        if (item["order_no"] != DBNull.Value) orderEntity.OrderNo = item["id"].ToString();
                        if (item["cust_code"] != DBNull.Value) orderEntity.CustomerCode = item["cust_code"].ToString();
                        if (item["order_date"] != DBNull.Value) orderEntity.OrderDate = Convert.ToDateTime(item["order_date"]).ToString("yyyy-MM-dd HH:mm"); 
                        if (item["required_date"] != DBNull.Value) orderEntity.RequiredDate = Convert.ToDateTime(item["required_date"]).ToString("yyyy-MM-dd HH:mm");
                        if (item["gross_total"] != DBNull.Value) orderEntity.OrderTotal = Convert.ToDouble(item["gross_total"]);
                        if (item["discount_total"] != DBNull.Value) orderEntity.TotalDiscount = Convert.ToDouble(item["discount_total"]);
                        if (item["latitude"] != DBNull.Value) orderEntity.Latitude = Convert.ToDouble(item["latitude"]);
                        if (item["longitude"] != DBNull.Value) orderEntity.Longitude = Convert.ToDouble(item["longitude"]);
                        if (item["rep_code"] != DBNull.Value) orderEntity.RepCode = item["rep_code"].ToString();
                        if (item["dis_id"] != DBNull.Value) orderEntity.DistributorId = Convert.ToInt32(item["dis_id"]);
                        if (item["asm_id"] != DBNull.Value) orderEntity.AsmId = Convert.ToInt32(item["asm_id"]);
                        if (item["rsm_id"] != DBNull.Value) orderEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
                        if (item["route_id"] != DBNull.Value) orderEntity.RouteId = Convert.ToInt32(item["route_id"]);
                        if (item["territory_id"] != DBNull.Value) orderEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                        if (item["area_id"] != DBNull.Value) orderEntity.AreaId = Convert.ToInt32(item["area_id"]);
                        if (item["region_id"] != DBNull.Value) orderEntity.RegionId = Convert.ToInt32(item["region_id"]);
                        if (item["remark"] != DBNull.Value) orderEntity.Remark = item["remark"].ToString();

                        if (item["status"] != DBNull.Value) orderEntity.Canceled = (item["status"].ToString() == "A") ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return orderEntity;
        }

        public OrderModel GetOrderHeaderByOrderId(int orderId)
        {
            OrderModel orderEntity = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOrderHeaderByOrderId";

                    objCommand.Parameters.AddWithValue("@OrderId", orderId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        orderEntity = new OrderModel();
                        if (item["id"] != DBNull.Value) orderEntity.OrderId = Convert.ToInt32(item["id"]);
                        if (item["order_no"] != DBNull.Value) orderEntity.OrderNo = item["id"].ToString();
                        if (item["cust_code"] != DBNull.Value) orderEntity.CustomerCode = item["cust_code"].ToString();
                        if (item["order_date"] != DBNull.Value) orderEntity.OrderDate = Convert.ToDateTime(item["order_date"]).ToString("yyyy-MM-dd HH:mm");
                        if (item["required_date"] != DBNull.Value) orderEntity.RequiredDate = Convert.ToDateTime(item["required_date"]).ToString("yyyy-MM-dd HH:mm");
                        if (item["gross_total"] != DBNull.Value) orderEntity.OrderTotal = Convert.ToDouble(item["gross_total"]);
                        if (item["discount_total"] != DBNull.Value) orderEntity.TotalDiscount = Convert.ToDouble(item["discount_total"]);
                        if (item["latitude"] != DBNull.Value) orderEntity.Latitude = Convert.ToDouble(item["latitude"]);
                        if (item["longitude"] != DBNull.Value) orderEntity.Longitude = Convert.ToDouble(item["longitude"]);
                        if (item["rep_code"] != DBNull.Value) orderEntity.RepCode = item["rep_code"].ToString();
                        if (item["dis_id"] != DBNull.Value) orderEntity.DistributorId = Convert.ToInt32(item["dis_id"]);
                        if (item["asm_id"] != DBNull.Value) orderEntity.AsmId = Convert.ToInt32(item["asm_id"]);
                        if (item["rsm_id"] != DBNull.Value) orderEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
                        if (item["route_id"] != DBNull.Value) orderEntity.RouteId = Convert.ToInt32(item["route_id"]);
                        if (item["territory_id"] != DBNull.Value) orderEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                        if (item["area_id"] != DBNull.Value) orderEntity.AreaId = Convert.ToInt32(item["area_id"]);
                        if (item["region_id"] != DBNull.Value) orderEntity.RegionId = Convert.ToInt32(item["region_id"]);
                        if (item["remark"] != DBNull.Value) orderEntity.Remark = item["remark"].ToString();

                        if (item["status"] != DBNull.Value) orderEntity.Canceled = (item["status"].ToString() == "A") ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return orderEntity;
        }

        public List<OrderModel> GetOrdersByAccessToken(string accessToken)
        {
            OrderModel orderHeaderEntity = null;
            DataTable objDS = new DataTable();
            List<OrderModel> OrderHeaderList = new List<OrderModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOrdersByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            orderHeaderEntity = new OrderModel();
                            if (item["id"] != DBNull.Value) orderHeaderEntity.OrderId = Convert.ToInt32(item["id"]);
                            if (item["order_no"] != DBNull.Value) orderHeaderEntity.OrderNo = item["order_no"].ToString();
                            if (item["order_date"] != DBNull.Value) orderHeaderEntity.OrderDate = Convert.ToDateTime(item["order_date"]).ToString("yyyy-MM-dd HH:mm");
                            if (item["required_date"] != DBNull.Value) orderHeaderEntity.RequiredDate = Convert.ToDateTime(item["required_date"]).ToString("yyyy-MM-dd");
                            if (item["cust_code"] != DBNull.Value) orderHeaderEntity.CustomerCode = item["cust_code"].ToString();
                            if (item["gross_total"] != DBNull.Value) orderHeaderEntity.OrderTotal = Convert.ToDouble(item["gross_total"]);
                            if (item["discount_total"] != DBNull.Value) orderHeaderEntity.TotalDiscount = Convert.ToDouble(item["discount_total"]);
                            if (item["latitude"] != DBNull.Value) orderHeaderEntity.Latitude = Convert.ToDouble(item["latitude"]);
                            if (item["longitude"] != DBNull.Value) orderHeaderEntity.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["remark"] != DBNull.Value) orderHeaderEntity.Remark = item["remark"].ToString();
                            //if (item["returns_total"] != DBNull.Value) orderHeaderEntity.Re = Convert.ToDouble(item["returns_total"]);
                            //if (item["grand_total"] != DBNull.Value) orderHeaderEntity.Grandtotal = Convert.ToDouble(item["grand_total"]);

                            if (item["route_id"] != DBNull.Value) orderHeaderEntity.RouteId = Convert.ToInt32(item["route_id"]);
                            if (item["territory_id"] != DBNull.Value) orderHeaderEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["area_id"] != DBNull.Value) orderHeaderEntity.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["region_id"] != DBNull.Value) orderHeaderEntity.RegionId = Convert.ToInt32(item["region_id"]);

                            if (item["rep_code"] != DBNull.Value) orderHeaderEntity.RepCode = item["rep_code"].ToString();
                            if (item["dis_id"] != DBNull.Value) orderHeaderEntity.DistributorId = Convert.ToInt32(item["dis_id"]);
                            if (item["asm_id"] != DBNull.Value) orderHeaderEntity.AsmId = Convert.ToInt32(item["asm_id"]);
                            if (item["pay_type"] != DBNull.Value) orderHeaderEntity.payType = item["pay_type"].ToString();
                            if (item["route_name"] != DBNull.Value) orderHeaderEntity.RouteName = item["route_name"].ToString();
                            if (item["distributorname"] != DBNull.Value) orderHeaderEntity.DistributorName = item["distributorname"].ToString();
                            if (item["status"] != DBNull.Value) orderHeaderEntity.Canceled = (item["status"].ToString() == "A") ? false : true;

                            OrderHeaderList.Add(orderHeaderEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return OrderHeaderList;
        }

        public List<OrderModel> GetAllOrders(ArgsModel args, string Originator, string status)
        {
            OrderModel orderHeaderEntity = null;
            DataTable objDS = new DataTable();
            List<OrderModel> OrderHeaderList = new List<OrderModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllOrders";

                    objCommand.Parameters.AddWithValue("@EndDate", args.SEndDate + " 23:59:59");
                    objCommand.Parameters.AddWithValue("@StartDate", args.SStartDate + " 00:00:00");
                    objCommand.Parameters.AddWithValue("@Originator", Originator);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@Status", status);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            orderHeaderEntity = new OrderModel();
                            if (item["id"] != DBNull.Value) orderHeaderEntity.OrderId = Convert.ToInt32(item["id"]);
                            if (item["order_no"] != DBNull.Value) orderHeaderEntity.OrderNo = item["order_no"].ToString();
                            if (item["order_date"] != DBNull.Value) orderHeaderEntity.OrderDate = Convert.ToDateTime(item["order_date"]).ToString("yyyy-MM-dd HH:mm");
                            if (item["required_date"] != DBNull.Value) orderHeaderEntity.RequiredDate = Convert.ToDateTime(item["required_date"]).ToString("yyyy-MM-dd HH:mm");
                            if (item["cust_code"] != DBNull.Value) orderHeaderEntity.CustomerCode = item["cust_code"].ToString();
                            if (item["gross_total"] != DBNull.Value) orderHeaderEntity.OrderTotal = Convert.ToDouble(item["gross_total"]);
                            if (item["discount_total"] != DBNull.Value) orderHeaderEntity.TotalDiscount = Convert.ToDouble(item["discount_total"]);
                            if (item["latitude"] != DBNull.Value) orderHeaderEntity.Latitude = Convert.ToDouble(item["latitude"]);
                            if (item["longitude"] != DBNull.Value) orderHeaderEntity.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["remark"] != DBNull.Value) orderHeaderEntity.Remark = item["remark"].ToString();
                            //if (item["returns_total"] != DBNull.Value) orderHeaderEntity.Re = Convert.ToDouble(item["returns_total"]);
                            //if (item["grand_total"] != DBNull.Value) orderHeaderEntity.Grandtotal = Convert.ToDouble(item["grand_total"]);

                            if (item["route_id"] != DBNull.Value) orderHeaderEntity.RouteId = Convert.ToInt32(item["route_id"]);
                            if (item["territory_id"] != DBNull.Value) orderHeaderEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["area_id"] != DBNull.Value) orderHeaderEntity.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["region_id"] != DBNull.Value) orderHeaderEntity.RegionId = Convert.ToInt32(item["region_id"]);

                            if (item["rep_code"] != DBNull.Value) orderHeaderEntity.RepCode = item["rep_code"].ToString();
                            if (item["dis_id"] != DBNull.Value) orderHeaderEntity.DistributorId = Convert.ToInt32(item["dis_id"]);
                            if (item["asm_id"] != DBNull.Value) orderHeaderEntity.AsmId = Convert.ToInt32(item["asm_id"]);

                            if (item["route_name"] != DBNull.Value) orderHeaderEntity.RouteName = item["route_name"].ToString();
                            if (item["distributorname"] != DBNull.Value) orderHeaderEntity.DistributorName = item["distributorname"].ToString();
                            if (item["status"] != DBNull.Value) orderHeaderEntity.Canceled = (item["status"].ToString() == "A") ? false : true;

                            if (item["total_count"] != DBNull.Value) orderHeaderEntity.RecordCount = Convert.ToInt32(item["total_count"]);
                            if (item["day_count"] != DBNull.Value) orderHeaderEntity.DayCount = Convert.ToInt32(item["day_count"]);
                            if (item["is_ignore_period"] != DBNull.Value) orderHeaderEntity.IsInvoiceActivate = Convert.ToBoolean(item["is_ignore_period"]);

                            OrderHeaderList.Add(orderHeaderEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return OrderHeaderList;
        }

        public bool DeleteBulkOrders(string userType, string userName, string ordList)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteBulkOrders";

                    objCommand.Parameters.AddWithValue("@userType", userType);
                    objCommand.Parameters.AddWithValue("@userName", userName);
                    objCommand.Parameters.AddWithValue("@ordList", ordList);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public List<RepsSKUMasterEntity> GetAllRepsSKUWiseDetails(string AccessToken, string date)
        {
            //DbDataReader drSchemeHeader = null;

            //try
            //{
            //    List<RepsSKUMasterEntity> headerList = new List<RepsSKUMasterEntity>();
            //    RepsSKUMasterEntity header = new RepsSKUMasterEntity();
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@AccessToken", AccessToken, DbType.String);
            //    paramCollection.Add("@date", date, DbType.String);

            //    drSchemeHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetAllRepsSKUWiseDetails"], paramCollection);

            //    if (drSchemeHeader != null && drSchemeHeader.HasRows)
            //    {
            //        int rep_nameOrdinal = drSchemeHeader.GetOrdinal("rep_name");
            //        int route_nameOrdinal = drSchemeHeader.GetOrdinal("route_name");
            //        int brand_nameOrdinal = drSchemeHeader.GetOrdinal("brand_name");
            //        int category_nameOrdinal = drSchemeHeader.GetOrdinal("category_name");
            //        int packing_nameOrdinal = drSchemeHeader.GetOrdinal("packing_name");
            //        int flavour_nameOrdinal = drSchemeHeader.GetOrdinal("flavor_name");
            //        int is_high_productOrdinal = drSchemeHeader.GetOrdinal("is_high_value");
            //        int product_codeOrdinal = drSchemeHeader.GetOrdinal("product_code");
            //        int product_nameOrdinal = drSchemeHeader.GetOrdinal("product_name");
            //        int product_fg_codeOrdinal = drSchemeHeader.GetOrdinal("fg_code");
            //        int product_quantityOrdinal = drSchemeHeader.GetOrdinal("product_quantity");
            //        int product_valueOrdinal = drSchemeHeader.GetOrdinal("product_value");
            //        int free_issueOrdinal = drSchemeHeader.GetOrdinal("free_issue");
            //        int discount_valueOrdinal = drSchemeHeader.GetOrdinal("discount_value");

            //        while (drSchemeHeader.Read())
            //        {
            //            header = RepsSKUMasterEntity.CreateObject();
            //            if (!drSchemeHeader.IsDBNull(rep_nameOrdinal)) header.RepName = drSchemeHeader.GetString(rep_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(route_nameOrdinal)) header.RouteName = drSchemeHeader.GetString(route_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(brand_nameOrdinal)) header.BrandName = drSchemeHeader.GetString(brand_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(category_nameOrdinal)) header.CategoryName = drSchemeHeader.GetString(category_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(packing_nameOrdinal)) header.PackingName = drSchemeHeader.GetString(packing_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(flavour_nameOrdinal)) header.FlavourName = drSchemeHeader.GetString(flavour_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(is_high_productOrdinal)) header.IsHighValue = drSchemeHeader.GetBoolean(is_high_productOrdinal);
            //            if (!drSchemeHeader.IsDBNull(product_codeOrdinal)) header.ProductCode = drSchemeHeader.GetString(product_codeOrdinal);
            //            if (!drSchemeHeader.IsDBNull(product_nameOrdinal)) header.ProductName = drSchemeHeader.GetString(product_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(product_fg_codeOrdinal)) header.ProductFGCode = drSchemeHeader.GetString(product_fg_codeOrdinal);
            //            if (!drSchemeHeader.IsDBNull(product_quantityOrdinal)) header.ProductQty = drSchemeHeader.GetDouble(product_quantityOrdinal);
            //            if (!drSchemeHeader.IsDBNull(product_valueOrdinal)) header.ProductValue = (double)drSchemeHeader.GetDecimal(product_valueOrdinal);
            //            if (!drSchemeHeader.IsDBNull(free_issueOrdinal)) header.FreeIssues = drSchemeHeader.GetDouble(free_issueOrdinal);
            //            if (!drSchemeHeader.IsDBNull(discount_valueOrdinal)) header.DiscountValue = drSchemeHeader.GetDouble(discount_valueOrdinal);
            //            headerList.Add(header);
            //        }
            //    }

            //    return headerList;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drSchemeHeader != null)
            //        drSchemeHeader.Close();
            //}

            RepsSKUMasterEntity orderHeaderEntity = null;
            DataTable objDS = new DataTable();
            List<RepsSKUMasterEntity> OrderHeaderList = new List<RepsSKUMasterEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRepsSKUWiseOrderDetails";

                    objCommand.Parameters.AddWithValue("@AccessToken", AccessToken);
                    objCommand.Parameters.AddWithValue("@date", date);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            orderHeaderEntity = new RepsSKUMasterEntity();
                            if (item["rep_name"] != DBNull.Value) orderHeaderEntity.RepName = item["rep_name"].ToString();
                            if (item["route_name"] != DBNull.Value) orderHeaderEntity.RouteName = item["route_name"].ToString();
                            if (item["brand_name"] != DBNull.Value) orderHeaderEntity.BrandName = item["brand_name"].ToString();
                            if (item["category_name"] != DBNull.Value) orderHeaderEntity.CategoryName = item["category_name"].ToString();
                            if (item["packing_name"] != DBNull.Value) orderHeaderEntity.PackingName = item["packing_name"].ToString();
                            if (item["flavor_name"] != DBNull.Value) orderHeaderEntity.FlavourName = item["flavor_name"].ToString();
                            if (item["is_high_value"] != DBNull.Value) orderHeaderEntity.IsHighValue = Convert.ToBoolean(item["is_high_value"]);
                            if (item["product_code"] != DBNull.Value) orderHeaderEntity.ProductCode = item["product_code"].ToString();
                            if (item["product_name"] != DBNull.Value) orderHeaderEntity.ProductName = item["product_name"].ToString();
                            if (item["fg_code"] != DBNull.Value) orderHeaderEntity.ProductFGCode = item["fg_code"].ToString();
                            if (item["product_quantity"] != DBNull.Value) orderHeaderEntity.ProductQty = Convert.ToDouble(item["product_quantity"]);
                            if (item["product_value"] != DBNull.Value) orderHeaderEntity.ProductValue = Convert.ToDouble(item["product_value"]);
                            if (item["free_issue"] != DBNull.Value) orderHeaderEntity.FreeIssues = Convert.ToDouble(item["free_issue"]);
                            if (item["discount_value"] != DBNull.Value) orderHeaderEntity.DiscountValue = Convert.ToDouble(item["discount_value"]);

                            OrderHeaderList.Add(orderHeaderEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return OrderHeaderList;
        }

        public List<RepsOutletWiseDetailsEntity> GetAllRepsOutletWiseOrderDetails(string AccessToken, string date)
        {
            //DbDataReader drSchemeHeader = null;
            //try
            //{
            //    List<RepsOutletWiseDetailsEntity> detCollection = new List<RepsOutletWiseDetailsEntity>();
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    RepsOutletWiseDetailsEntity schemeHeader = null;
            //    paramCollection.Add("@AccessToken", AccessToken, DbType.String);
            //    paramCollection.Add("@date", date, DbType.String);

            //    drSchemeHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetAllRepsOutletWiseDetails"], paramCollection);

            //    if (drSchemeHeader != null && drSchemeHeader.HasRows)
            //    {
            //        int cust_codeOrdinal = drSchemeHeader.GetOrdinal("cust_code");
            //        int cust_nameOrdinal = drSchemeHeader.GetOrdinal("cust_name");
            //        int prod_codeOrdinal = drSchemeHeader.GetOrdinal("prod_code");
            //        int prod_nameOrdinal = drSchemeHeader.GetOrdinal("prod_name");
            //        int quantityOrdinal = drSchemeHeader.GetOrdinal("quantity");
            //        int sub_totalOrdinal = drSchemeHeader.GetOrdinal("sub_total");
            //        int count_creditOrdinal = drSchemeHeader.GetOrdinal("count_credit");
            //        int count_chequeOrdinal = drSchemeHeader.GetOrdinal("count_cheque");
            //        int count_cashOrdinal = drSchemeHeader.GetOrdinal("count_cash");
            //        int rep_nameOrdinal = drSchemeHeader.GetOrdinal("rep_name");
            //        int route_nameOrdinal = drSchemeHeader.GetOrdinal("route_name");
            //        int discount_totalOrdinal = drSchemeHeader.GetOrdinal("discount_total");

            //        while (drSchemeHeader.Read())
            //        {
            //            schemeHeader = RepsOutletWiseDetailsEntity.CreateObject();
            //            if (!drSchemeHeader.IsDBNull(cust_codeOrdinal)) schemeHeader.CustomerCode = drSchemeHeader.GetString(cust_codeOrdinal);
            //            if (!drSchemeHeader.IsDBNull(cust_nameOrdinal)) schemeHeader.CustomerName = drSchemeHeader.GetString(cust_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(count_creditOrdinal)) schemeHeader.PaymentTypeCredit = drSchemeHeader.GetInt32(count_creditOrdinal);
            //            if (!drSchemeHeader.IsDBNull(count_cashOrdinal)) schemeHeader.PaymentTypeCash = drSchemeHeader.GetInt32(count_cashOrdinal);
            //            if (!drSchemeHeader.IsDBNull(count_chequeOrdinal)) schemeHeader.PaymentTypeCheque = drSchemeHeader.GetInt32(count_chequeOrdinal);

            //            if (!drSchemeHeader.IsDBNull(route_nameOrdinal)) schemeHeader.RouteName = drSchemeHeader.GetString(route_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(rep_nameOrdinal)) schemeHeader.RepName = drSchemeHeader.GetString(rep_nameOrdinal);

            //            if (!drSchemeHeader.IsDBNull(prod_codeOrdinal)) schemeHeader.ProductCode = drSchemeHeader.GetString(prod_codeOrdinal);
            //            if (!drSchemeHeader.IsDBNull(prod_nameOrdinal)) schemeHeader.ProductName = drSchemeHeader.GetString(prod_nameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(quantityOrdinal)) schemeHeader.ProductQty = drSchemeHeader.GetDouble(quantityOrdinal);
            //            if (!drSchemeHeader.IsDBNull(sub_totalOrdinal)) schemeHeader.ProductAmount = (double)drSchemeHeader.GetDecimal(sub_totalOrdinal);
            //            if (!drSchemeHeader.IsDBNull(discount_totalOrdinal)) schemeHeader.DiscountTotal = drSchemeHeader.GetDouble(discount_totalOrdinal);

            //            detCollection.Add(schemeHeader);
            //        }
            //    }

            //    return detCollection;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drSchemeHeader != null)
            //        drSchemeHeader.Close();
            //}


            RepsOutletWiseDetailsEntity orderHeaderEntity = null;
            DataTable objDS = new DataTable();
            List<RepsOutletWiseDetailsEntity> OrderHeaderList = new List<RepsOutletWiseDetailsEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRepsOutletWiseOrderDetails";

                    objCommand.Parameters.AddWithValue("@AccessToken", AccessToken);
                    objCommand.Parameters.AddWithValue("@date", date);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            orderHeaderEntity = new RepsOutletWiseDetailsEntity();
                            if (item["cust_code"] != DBNull.Value) orderHeaderEntity.CustomerCode = item["cust_code"].ToString();
                            if (item["cust_name"] != DBNull.Value) orderHeaderEntity.CustomerName = item["cust_name"].ToString();
                            if (item["count_credit"] != DBNull.Value) orderHeaderEntity.PaymentTypeCredit = Convert.ToInt32(item["count_credit"]);
                            if (item["count_cash"] != DBNull.Value) orderHeaderEntity.PaymentTypeCash = Convert.ToInt32(item["count_cash"]);
                            if (item["count_cheque"] != DBNull.Value) orderHeaderEntity.PaymentTypeCheque = Convert.ToInt32(item["count_cheque"]);
                            if (item["route_name"] != DBNull.Value) orderHeaderEntity.RouteName = item["route_name"].ToString();
                            if (item["rep_name"] != DBNull.Value) orderHeaderEntity.RepName = item["rep_name"].ToString();
                            if (item["prod_code"] != DBNull.Value) orderHeaderEntity.ProductCode = item["prod_code"].ToString();
                            if (item["prod_name"] != DBNull.Value) orderHeaderEntity.ProductName = item["prod_name"].ToString();
                            if (item["quantity"] != DBNull.Value) orderHeaderEntity.ProductQty = Convert.ToDouble(item["quantity"]);
                            if (item["sub_total"] != DBNull.Value) orderHeaderEntity.ProductAmount = Convert.ToDouble(item["sub_total"]);
                            if (item["discount_total"] != DBNull.Value) orderHeaderEntity.DiscountTotal = Convert.ToDouble(item["discount_total"]);

                            OrderHeaderList.Add(orderHeaderEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return OrderHeaderList;
        }
    }
}
