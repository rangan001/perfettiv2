﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using Peercore.CRM.Shared;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class PaymentSettlementDAO: BaseDAO
    {
        private DbSqlAdapter PaymentSettlementSql { get; set; }

        private void RegisterSql()
        {
            this.PaymentSettlementSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.PaymentSettlementSql.xml", ApplicationService.Instance.DbProvider);
        }

        public PaymentSettlementDAO()
        {
            RegisterSql();
        }

        public PaymentSettlementDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public PaymentSettlementEntity CreateObject()
        {
            return PaymentSettlementEntity.CreateObject();
        }

        public PaymentSettlementEntity CreateObject(int entityId)
        {
            return PaymentSettlementEntity.CreateObject(entityId);
        }
               
        public List<PaymentSettlementEntity> GetChequePaymentSettlements(string createdBy, DateTime createdDate, DateTime toDate, int distributerAccountId, string status, ArgsEntity args)
        {
            DbDataReader drPaymentSettlement = null;

            try
            {
                List<PaymentSettlementEntity> PaymentSettlementList = new List<PaymentSettlementEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@createdBy", createdBy, DbType.String);
                paramCollection.Add("@distributorAccountId", distributerAccountId, DbType.Int32);
                paramCollection.Add("@createdDate", createdDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@toDate", toDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@status", status, DbType.String);
                
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);


                drPaymentSettlement = this.DataAcessService.ExecuteQuery(PaymentSettlementSql["GetChequePaymentSettlements"], paramCollection);

                if (drPaymentSettlement != null && drPaymentSettlement.HasRows)
                {
                    int idOrdinal = drPaymentSettlement.GetOrdinal("id");
                    int custCodeOrdinal = drPaymentSettlement.GetOrdinal("cust_code");
                    int chequeNumberOrdinal = drPaymentSettlement.GetOrdinal("cheque_number");
                    int outletBankOrdinal = drPaymentSettlement.GetOrdinal("outlet_bank");
                    int paymentTypeOrdinal = drPaymentSettlement.GetOrdinal("payment_type");
                    int amountOrdinal = drPaymentSettlement.GetOrdinal("amount");
                    int statusOrdinal = drPaymentSettlement.GetOrdinal("status");
                    int ivceNoOrdinal = drPaymentSettlement.GetOrdinal("ivce_no");
                    int distributerAccountIdOrdinal = drPaymentSettlement.GetOrdinal("distributor_account_id");
                    int chequeDateOrdinal = drPaymentSettlement.GetOrdinal("cheque_date");
                    int createdByOrdinal = drPaymentSettlement.GetOrdinal("created_by");
                    int createdDateOrdinal = drPaymentSettlement.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drPaymentSettlement.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drPaymentSettlement.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drPaymentSettlement.GetOrdinal("total_count");

                    while (drPaymentSettlement.Read())
                    {
                        PaymentSettlementEntity paymentSettlementEntity = CreateObject();

                        if (!drPaymentSettlement.IsDBNull(idOrdinal)) paymentSettlementEntity.PaymentSettlementId = drPaymentSettlement.GetInt32(idOrdinal);
                        if (!drPaymentSettlement.IsDBNull(custCodeOrdinal)) paymentSettlementEntity.CustCode = drPaymentSettlement.GetString(custCodeOrdinal);
                        if (!drPaymentSettlement.IsDBNull(chequeNumberOrdinal)) paymentSettlementEntity.ChequeNumber = drPaymentSettlement.GetString(chequeNumberOrdinal);
                        if (!drPaymentSettlement.IsDBNull(outletBankOrdinal)) paymentSettlementEntity.OutletBank = drPaymentSettlement.GetString(outletBankOrdinal);
                        if (!drPaymentSettlement.IsDBNull(paymentTypeOrdinal)) paymentSettlementEntity.PaymentType = drPaymentSettlement.GetString(paymentTypeOrdinal);
                        if (!drPaymentSettlement.IsDBNull(amountOrdinal)) paymentSettlementEntity.Amount = drPaymentSettlement.GetDouble(amountOrdinal);
                        if (!drPaymentSettlement.IsDBNull(statusOrdinal)) paymentSettlementEntity.Status = drPaymentSettlement.GetString(statusOrdinal);
                        if (!drPaymentSettlement.IsDBNull(ivceNoOrdinal)) paymentSettlementEntity.IvceNo = drPaymentSettlement.GetInt32(ivceNoOrdinal);
                        if (!drPaymentSettlement.IsDBNull(distributerAccountIdOrdinal)) paymentSettlementEntity.DistributorAccountId = drPaymentSettlement.GetInt32(distributerAccountIdOrdinal);
                        if (!drPaymentSettlement.IsDBNull(chequeDateOrdinal)) paymentSettlementEntity.ChequeDate = drPaymentSettlement.GetDateTime(chequeDateOrdinal);
                        if (!drPaymentSettlement.IsDBNull(createdByOrdinal)) paymentSettlementEntity.CreatedBy = drPaymentSettlement.GetString(createdByOrdinal);
                        if (!drPaymentSettlement.IsDBNull(createdDateOrdinal)) paymentSettlementEntity.CreatedDate = drPaymentSettlement.GetDateTime(createdDateOrdinal);
                        if (!drPaymentSettlement.IsDBNull(lastModifiedByOrdinal)) paymentSettlementEntity.LastModifiedBy = drPaymentSettlement.GetString(lastModifiedByOrdinal);
                        if (!drPaymentSettlement.IsDBNull(lastModifiedDateOrdinal)) paymentSettlementEntity.LastModifiedDate = drPaymentSettlement.GetDateTime(lastModifiedDateOrdinal);
                        if (!drPaymentSettlement.IsDBNull(totalCountOrdinal)) paymentSettlementEntity.TotalCount = drPaymentSettlement.GetInt32(totalCountOrdinal);

                        PaymentSettlementList.Add(paymentSettlementEntity);
                    }
                }

                return PaymentSettlementList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drPaymentSettlement != null)
                    drPaymentSettlement.Close();
            }
        }

        public bool InsertPaymentSettlement(PaymentSettlementEntity paymentSettlementEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@cust_code", paymentSettlementEntity.CustCode, DbType.String);
                paramCollection.Add("@cheque_number", paymentSettlementEntity.ChequeNumber, DbType.String);
                paramCollection.Add("@outlet_bank", paymentSettlementEntity.OutletBank, DbType.String);
                paramCollection.Add("@payment_type", paymentSettlementEntity.PaymentType, DbType.String);
                paramCollection.Add("@amount", paymentSettlementEntity.Amount, DbType.Double);
                paramCollection.Add("@status", paymentSettlementEntity.Status, DbType.String);
                paramCollection.Add("@ivce_no", paymentSettlementEntity.IvceNo, DbType.Int32);
                paramCollection.Add("@distributor_account_id", paymentSettlementEntity.DistributorAccountId, DbType.Int32);
                paramCollection.Add("@created_by", paymentSettlementEntity.CreatedBy, DbType.String);
                paramCollection.Add("@settlement_date", paymentSettlementEntity.CreatedDate, DbType.DateTime);
                paramCollection.Add("@cheque_date", paymentSettlementEntity.ChequeDate, DbType.DateTime);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PaymentSettlementSql["InsertPaymentSettlement"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdatePaymentSettlement(PaymentSettlementEntity paymentSettlementEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@id", paymentSettlementEntity.PaymentSettlementId, DbType.Int32);
                paramCollection.Add("@cust_code", paymentSettlementEntity.CustCode, DbType.String);
                paramCollection.Add("@cheque_number", paymentSettlementEntity.ChequeNumber, DbType.String);
                paramCollection.Add("@outlet_bank", paymentSettlementEntity.OutletBank, DbType.String);
                paramCollection.Add("@payment_type", paymentSettlementEntity.PaymentType, DbType.String);
                paramCollection.Add("@amount", paymentSettlementEntity.Amount, DbType.Double);
                paramCollection.Add("@status", paymentSettlementEntity.Status, DbType.String);
                paramCollection.Add("@ivce_no", paymentSettlementEntity.IvceNo, DbType.Int32);
                paramCollection.Add("@distributor_account_id", paymentSettlementEntity.DistributorAccountId, DbType.Int32);
                paramCollection.Add("@last_modified_by", paymentSettlementEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PaymentSettlementSql["UpdatePaymentSettlement"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<PaymentSettlementEntity> GetDatedChequePaymentSettlements(string createdBy, DateTime createdDate, DateTime toDate, ArgsEntity args)
        {
            DbDataReader drPaymentSettlement = null;

            try
            {
                List<PaymentSettlementEntity> PaymentSettlementList = new List<PaymentSettlementEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@createdBy", createdBy, DbType.String);
                //paramCollection.Add("@distributorAccountId", distributerAccountId, DbType.Int32);
                paramCollection.Add("@createdDate", createdDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@toDate", toDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                //paramCollection.Add("@status", status, DbType.String);

                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);


                drPaymentSettlement = this.DataAcessService.ExecuteQuery(PaymentSettlementSql["GetDatedChequePaymentSettlements"], paramCollection);

                if (drPaymentSettlement != null && drPaymentSettlement.HasRows)
                {
                    int idOrdinal = drPaymentSettlement.GetOrdinal("id");
                    int custCodeOrdinal = drPaymentSettlement.GetOrdinal("cust_code");
                    int chequeNumberOrdinal = drPaymentSettlement.GetOrdinal("cheque_number");
                    int outletBankOrdinal = drPaymentSettlement.GetOrdinal("outlet_bank");
                    int paymentTypeOrdinal = drPaymentSettlement.GetOrdinal("payment_type");
                    int amountOrdinal = drPaymentSettlement.GetOrdinal("amount");
                    int statusOrdinal = drPaymentSettlement.GetOrdinal("status");
                    int ivceNoOrdinal = drPaymentSettlement.GetOrdinal("ivce_no");
                    int distributerAccountIdOrdinal = drPaymentSettlement.GetOrdinal("distributor_account_id");
                    int chequeDateOrdinal = drPaymentSettlement.GetOrdinal("cheque_date");
                    int createdByOrdinal = drPaymentSettlement.GetOrdinal("created_by");
                    int createdDateOrdinal = drPaymentSettlement.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drPaymentSettlement.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drPaymentSettlement.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drPaymentSettlement.GetOrdinal("total_count");

                    while (drPaymentSettlement.Read())
                    {
                        PaymentSettlementEntity paymentSettlementEntity = CreateObject();

                        if (!drPaymentSettlement.IsDBNull(idOrdinal)) paymentSettlementEntity.PaymentSettlementId = drPaymentSettlement.GetInt32(idOrdinal);
                        if (!drPaymentSettlement.IsDBNull(custCodeOrdinal)) paymentSettlementEntity.CustCode = drPaymentSettlement.GetString(custCodeOrdinal);
                        if (!drPaymentSettlement.IsDBNull(chequeNumberOrdinal)) paymentSettlementEntity.ChequeNumber = drPaymentSettlement.GetString(chequeNumberOrdinal);
                        if (!drPaymentSettlement.IsDBNull(outletBankOrdinal)) paymentSettlementEntity.OutletBank = drPaymentSettlement.GetString(outletBankOrdinal);
                        if (!drPaymentSettlement.IsDBNull(paymentTypeOrdinal)) paymentSettlementEntity.PaymentType = drPaymentSettlement.GetString(paymentTypeOrdinal);
                        if (!drPaymentSettlement.IsDBNull(amountOrdinal)) paymentSettlementEntity.Amount = drPaymentSettlement.GetDouble(amountOrdinal);
                        if (!drPaymentSettlement.IsDBNull(statusOrdinal)) paymentSettlementEntity.Status = drPaymentSettlement.GetString(statusOrdinal);
                        if (!drPaymentSettlement.IsDBNull(ivceNoOrdinal)) paymentSettlementEntity.IvceNo = drPaymentSettlement.GetInt32(ivceNoOrdinal);
                        if (!drPaymentSettlement.IsDBNull(distributerAccountIdOrdinal)) paymentSettlementEntity.DistributorAccountId = drPaymentSettlement.GetInt32(distributerAccountIdOrdinal);
                        if (!drPaymentSettlement.IsDBNull(chequeDateOrdinal)) paymentSettlementEntity.ChequeDate = drPaymentSettlement.GetDateTime(chequeDateOrdinal);
                        if (!drPaymentSettlement.IsDBNull(createdByOrdinal)) paymentSettlementEntity.CreatedBy = drPaymentSettlement.GetString(createdByOrdinal);
                        if (!drPaymentSettlement.IsDBNull(createdDateOrdinal)) paymentSettlementEntity.CreatedDate = drPaymentSettlement.GetDateTime(createdDateOrdinal);
                        if (!drPaymentSettlement.IsDBNull(lastModifiedByOrdinal)) paymentSettlementEntity.LastModifiedBy = drPaymentSettlement.GetString(lastModifiedByOrdinal);
                        if (!drPaymentSettlement.IsDBNull(lastModifiedDateOrdinal)) paymentSettlementEntity.LastModifiedDate = drPaymentSettlement.GetDateTime(lastModifiedDateOrdinal);
                        if (!drPaymentSettlement.IsDBNull(totalCountOrdinal)) paymentSettlementEntity.TotalCount = drPaymentSettlement.GetInt32(totalCountOrdinal);

                        PaymentSettlementList.Add(paymentSettlementEntity);
                    }
                }

                return PaymentSettlementList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drPaymentSettlement != null)
                    drPaymentSettlement.Close();
            }
        }
    }
}
