﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class PipelineStageDAO:BaseDAO
    {
        private DbSqlAdapter PipelineStageSql { get; set; }

        //public PipelineStageDAO()
        //{
        //    this.PipelineStageSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.PipelineStageSql.xml");
        //}

        private void RegisterSql()
        {
            this.PipelineStageSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.PipelineStageSql.xml", ApplicationService.Instance.DbProvider);
        }

        public PipelineStageDAO()
        {
            RegisterSql();
        }

        public PipelineStageDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public PipelineStageEntity CreateObject()
        {
            return PipelineStageEntity.CreateObject();
        }

        public PipelineStageEntity CreateObject(int entityId)
        {
            return PipelineStageEntity.CreateObject(entityId);
        }

        //public void GetPipelineStages(ref DataTable dtPipelineStage, string defaultDeptID)
        //{
        //    try
        //    {
        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@defaultDeptID", defaultDeptID, DbType.String);
        //        dtPipelineStage = this.DataAcessService.ExecuteDisconnected(PipelineStageSql["GetPipelineStage"], paramCollection);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public List<PipelineStageEntity> GetPipelineStages(string defaultDeptID)
        {
            DbDataReader drPipelineStage = null;

            try
            {
                List<PipelineStageEntity> pipelineStageCollection = new List<PipelineStageEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@defaultDeptID", defaultDeptID, DbType.String);

                drPipelineStage = this.DataAcessService.ExecuteQuery(PipelineStageSql["GetPipelineStage"], paramCollection);


                if (drPipelineStage != null && drPipelineStage.HasRows)
                {
                    int leadStageIdOrdinal = drPipelineStage.GetOrdinal("ID");
                    int leadStageOrdinal = drPipelineStage.GetOrdinal("PipelineStage");
                    int orderOrdinal = drPipelineStage.GetOrdinal("stage_order");

                    while (drPipelineStage.Read())
                    {
                        PipelineStageEntity pipelineStage = PipelineStageEntity.CreateObject();

                        if (!drPipelineStage.IsDBNull(leadStageIdOrdinal)) pipelineStage.PipelineStageID = drPipelineStage.GetInt32(leadStageIdOrdinal);
                        if (!drPipelineStage.IsDBNull(leadStageOrdinal)) pipelineStage.PipelineStageName = drPipelineStage.GetString(leadStageOrdinal);
                        if (!drPipelineStage.IsDBNull(orderOrdinal)) pipelineStage.Order = drPipelineStage.GetInt32(orderOrdinal);

                        pipelineStageCollection.Add(pipelineStage);

                    }
                }

                return pipelineStageCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drPipelineStage != null)
                    drPipelineStage.Close();
            }
        }

        public bool InsertPipelineStage(PipelineStageEntity pipelineStageEntity, ArgsEntity argsEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@PipelineStageID", pipelineStageEntity.PipelineStageID, DbType.Int32);
                paramCollection.Add("@PipelineStage", pipelineStageEntity.PipelineStageName, DbType.String);
                paramCollection.Add("@stageOrder", pipelineStageEntity.Order, DbType.Int32);
                paramCollection.Add("@defaultDeptID", argsEntity.DefaultDepartmentId, DbType.String);
                
                iNoRec = this.DataAcessService.ExecuteNonQuery(PipelineStageSql["InsertPipelineStage"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdatePipelineStage(PipelineStageEntity pipelineStageEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@PipelineStageID", pipelineStageEntity.PipelineStageID, DbType.Int32);
                paramCollection.Add("@PipelineStage", pipelineStageEntity.PipelineStageName, DbType.String);
                paramCollection.Add("@stageOrder", pipelineStageEntity.Order, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PipelineStageSql["UpdatePipelineStage"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public int GetNextPipelineStageId()
        {
            try
            {
                return System.Convert.ToInt32(this.DataAcessService.GetOneValue(PipelineStageSql["GetNextPipelineStageId"]));   // Get Next ID               
            }
            catch
            {
                throw;
            }
        }

        public List<PipelineStageEntity> GetPipelineChart(bool includeCustomer, ArgsEntity argsEntity)
        {
            string sWhereCls = "";
            DbDataReader drPipelineStage = null;
            List<PipelineStageEntity> pipelineStageCollection = new List<PipelineStageEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", argsEntity.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", argsEntity.ChildOriginators.Replace("originator", "O.originator"), DbType.String);
                paramCollection.Add("@DefaultDepId", argsEntity.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@IncludeCustomer", includeCustomer?1:0, DbType.Boolean);
                paramCollection.Add("@StartIndex", argsEntity.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", argsEntity.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                


                //if (argsEntity.Originator != "")
                //{                    
                //    sWhereCls = argsEntity.ChildOriginators;
                //    sWhereCls = "(" + sWhereCls.Replace("originator", "O.originator");                 
                //    sWhereCls += " )";
                //}

                //if (sWhereCls != "")
                //{
                //    sWhereCls = sWhereCls.Insert(0, " AND ");
                //}
                                
                //string sqlString = PipelineStageSql["GetLeadsPipelineChart"].Format(sWhereCls, argsEntity.DefaultDepartmentId).ToString();

                //if (includeCustomer)
                //{
                //    sqlString += " \nUNION ALL " + PipelineStageSql["GetCustomerPipelineChart"].Format(sWhereCls, argsEntity.DefaultDepartmentId).ToString();
                //}

                ////dtPipeline = this.DataAcessService.ExecuteDisconnected(PipelineStageSql["GetPipelineChart"].Format(sqlString));

                drPipelineStage = this.DataAcessService.ExecuteQuery(PipelineStageSql["GetPipelineChart"], paramCollection);

                
                if (drPipelineStage != null && drPipelineStage.HasRows)
                {
                    int leadStageIdOrdinal = drPipelineStage.GetOrdinal("StageId");
                    int leadStageOrdinal = drPipelineStage.GetOrdinal("Stage");
                    int orderOrdinal = drPipelineStage.GetOrdinal("StageOrder");
                    int TotalAmountOrdinal = drPipelineStage.GetOrdinal("TotalAmount");
                    int TotalUnitsOrdinal = drPipelineStage.GetOrdinal("TotalUnits");

                    while (drPipelineStage.Read())
                    {
                        PipelineStageEntity pipelineStage = PipelineStageEntity.CreateObject();

                        if (!drPipelineStage.IsDBNull(leadStageIdOrdinal)) pipelineStage.PipelineStageID = drPipelineStage.GetInt32(leadStageIdOrdinal);
                        if (!drPipelineStage.IsDBNull(leadStageOrdinal)) pipelineStage.PipelineStageName = drPipelineStage.GetString(leadStageOrdinal);
                        if (!drPipelineStage.IsDBNull(orderOrdinal)) pipelineStage.Order = drPipelineStage.GetInt32(orderOrdinal);
                        if (!drPipelineStage.IsDBNull(TotalAmountOrdinal)) pipelineStage.TotalAmount = drPipelineStage.GetDouble(TotalAmountOrdinal);
                        if (!drPipelineStage.IsDBNull(TotalUnitsOrdinal)) pipelineStage.TotalUnits = drPipelineStage.GetDouble(TotalUnitsOrdinal);

                        pipelineStageCollection.Add(pipelineStage);

                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drPipelineStage != null)
                    drPipelineStage.Close();
            }
            return pipelineStageCollection;
        }

        public bool DeletePipelineStage(int pipelineStageID)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@PipelineStageID", pipelineStageID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PipelineStageSql["DeletePipelineStage"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

    }
}
