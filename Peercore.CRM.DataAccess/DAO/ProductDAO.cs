﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using System.Data.SqlClient;
using Peercore.CRM.Model;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ProductDAO : BaseDAO
    {
        private DbSqlAdapter ProductSql { get; set; }

        private void RegisterSql()
        {
            this.ProductSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ProductSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ProductDAO()
        {
            RegisterSql();
        }

        public ProductDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ProductEntity CreateObject()
        {
            return ProductEntity.CreateObject();
        }

        public ProductEntity CreateObject(int entityId)
        {
            return ProductEntity.CreateObject(entityId);
        }


        public List<ProductEntity> GetCustomerProducts(ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetCustomerProducts"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int statusOrdinal = drProduct.GetOrdinal("status");
                    int createdbyOrdinal = drProduct.GetOrdinal("created_by");
                    int createddateOrdinal = drProduct.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drProduct.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drProduct.GetOrdinal("last_modified_date");
                    int custCodeOrdinal = drProduct.GetOrdinal("cust_code");
                    int totalCountOrdinal = drProduct.GetOrdinal("total_count");

                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(statusOrdinal)) productEntity.Status = drProduct.GetString(statusOrdinal);
                        if (!drProduct.IsDBNull(createdbyOrdinal)) productEntity.CreatedBy = drProduct.GetString(createdbyOrdinal);
                        if (!drProduct.IsDBNull(createddateOrdinal)) productEntity.CreatedDate = drProduct.GetDateTime(createddateOrdinal);
                        if (!drProduct.IsDBNull(lastmodifiedbyOrdinal)) productEntity.LastModifiedBy = drProduct.GetString(lastmodifiedbyOrdinal);

                        if (!drProduct.IsDBNull(custCodeOrdinal))
                        { productEntity.CustomerCode = drProduct.GetString(custCodeOrdinal); }
                        else
                        { productEntity.CustomerCode = string.Empty; }

                        if (!drProduct.IsDBNull(lastmodifieddateOrdinal)) productEntity.LastModifiedDate = drProduct.GetDateTime(lastmodifieddateOrdinal);

                        productList.Add(productEntity);
                    }
                }

                return productList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetAllProducts(ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", string.IsNullOrEmpty(args.AdditionalParams) ? "" : args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetAllProducts"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int statusOrdinal = drProduct.GetOrdinal("status");
                    int bransIdOrdinal = drProduct.GetOrdinal("brand_id");
                    int brandNameOrdinal = drProduct.GetOrdinal("brand_name");
                    int categoryIdOrdinal = drProduct.GetOrdinal("category_id");
                    int categoryNameOrdinal = drProduct.GetOrdinal("category");
                    int skuOrdinal = drProduct.GetOrdinal("sku");
                    int createdbyOrdinal = drProduct.GetOrdinal("created_by");
                    int createddateOrdinal = drProduct.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drProduct.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drProduct.GetOrdinal("last_modified_date");

                    int flavorIdOrdinal = drProduct.GetOrdinal("flavor_id");
                    int isHighValueOrdinal = drProduct.GetOrdinal("is_high_value");
                    int isPromoItemOrdinal = drProduct.GetOrdinal("is_promotion");
                    int packingIdOrdinal = drProduct.GetOrdinal("packing_id");
                    int fgCodeOrdinal = drProduct.GetOrdinal("fg_code");
                    int imageContentOrdinal = drProduct.GetOrdinal("image_content");
                    int weightOrdinal = drProduct.GetOrdinal("weight");

                    int totalCountOrdinal = drProduct.GetOrdinal("total_count");

                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(statusOrdinal)) productEntity.Status = drProduct.GetString(statusOrdinal);
                        if (!drProduct.IsDBNull(bransIdOrdinal)) productEntity.BrandId = drProduct.GetInt32(bransIdOrdinal);
                        if (!drProduct.IsDBNull(brandNameOrdinal)) productEntity.BrandName = drProduct.GetString(brandNameOrdinal);
                        if (!drProduct.IsDBNull(categoryIdOrdinal)) productEntity.CategoryId = drProduct.GetInt32(categoryIdOrdinal);
                        if (!drProduct.IsDBNull(categoryNameOrdinal)) productEntity.CategoryName = drProduct.GetString(categoryNameOrdinal);
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);
                        if (!drProduct.IsDBNull(createdbyOrdinal)) productEntity.CreatedBy = drProduct.GetString(createdbyOrdinal);
                        if (!drProduct.IsDBNull(createddateOrdinal)) productEntity.CreatedDate = drProduct.GetDateTime(createddateOrdinal);
                        if (!drProduct.IsDBNull(lastmodifiedbyOrdinal)) productEntity.LastModifiedBy = drProduct.GetString(lastmodifiedbyOrdinal);
                        if (!drProduct.IsDBNull(lastmodifieddateOrdinal)) productEntity.LastModifiedDate = drProduct.GetDateTime(lastmodifieddateOrdinal);

                        if (!drProduct.IsDBNull(flavorIdOrdinal)) productEntity.FlavourId = drProduct.GetInt32(flavorIdOrdinal);
                        if (!drProduct.IsDBNull(isHighValueOrdinal)) productEntity.IsHighValue = drProduct.GetBoolean(isHighValueOrdinal);
                        if (!drProduct.IsDBNull(isPromoItemOrdinal)) productEntity.IsPromotion = drProduct.GetBoolean(isPromoItemOrdinal);
                        if (!drProduct.IsDBNull(packingIdOrdinal)) productEntity.PackingId = drProduct.GetInt32(packingIdOrdinal);

                        if (!drProduct.IsDBNull(fgCodeOrdinal)) productEntity.FgCode = drProduct.GetString(fgCodeOrdinal);
                        if (!drProduct.IsDBNull(weightOrdinal)) productEntity.Weight = drProduct.GetDouble(weightOrdinal);

                        if (drProduct[imageContentOrdinal] != null)
                        {
                            if (!drProduct.IsDBNull(imageContentOrdinal)) productEntity.ImageContent = (byte[])drProduct[imageContentOrdinal];
                        }

                        if (!drProduct.IsDBNull(totalCountOrdinal)) productEntity.TotalCount = drProduct.GetInt32(totalCountOrdinal);
                        productList.Add(productEntity);
                    }
                }

                return productList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetAllProductsWithOutImage(ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", string.IsNullOrEmpty(args.AdditionalParams) ? "" : args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetAllProducts"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int statusOrdinal = drProduct.GetOrdinal("status");
                    int bransIdOrdinal = drProduct.GetOrdinal("brand_id");
                    int brandNameOrdinal = drProduct.GetOrdinal("brand_name");
                    int categoryIdOrdinal = drProduct.GetOrdinal("category_id");
                    int categoryNameOrdinal = drProduct.GetOrdinal("category");
                    int skuOrdinal = drProduct.GetOrdinal("sku");
                    int createdbyOrdinal = drProduct.GetOrdinal("created_by");
                    int createddateOrdinal = drProduct.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drProduct.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drProduct.GetOrdinal("last_modified_date");
                    int flavorIdOrdinal = drProduct.GetOrdinal("flavor_id");
                    int isHighValueOrdinal = drProduct.GetOrdinal("is_high_value");
                    int isPromoItemOrdinal = drProduct.GetOrdinal("is_promotion");
                    int packingIdOrdinal = drProduct.GetOrdinal("packing_id");
                    int fgCodeOrdinal = drProduct.GetOrdinal("fg_code");
                    int weightOrdinal = drProduct.GetOrdinal("weight");
                    int totalCountOrdinal = drProduct.GetOrdinal("total_count");

                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(statusOrdinal)) productEntity.Status = drProduct.GetString(statusOrdinal);
                        if (!drProduct.IsDBNull(bransIdOrdinal)) productEntity.BrandId = drProduct.GetInt32(bransIdOrdinal);
                        if (!drProduct.IsDBNull(brandNameOrdinal)) productEntity.BrandName = drProduct.GetString(brandNameOrdinal);
                        if (!drProduct.IsDBNull(categoryIdOrdinal)) productEntity.CategoryId = drProduct.GetInt32(categoryIdOrdinal);
                        if (!drProduct.IsDBNull(categoryNameOrdinal)) productEntity.CategoryName = drProduct.GetString(categoryNameOrdinal);
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);
                        if (!drProduct.IsDBNull(createdbyOrdinal)) productEntity.CreatedBy = drProduct.GetString(createdbyOrdinal);
                        if (!drProduct.IsDBNull(createddateOrdinal)) productEntity.CreatedDate = drProduct.GetDateTime(createddateOrdinal);
                        if (!drProduct.IsDBNull(lastmodifiedbyOrdinal)) productEntity.LastModifiedBy = drProduct.GetString(lastmodifiedbyOrdinal);
                        if (!drProduct.IsDBNull(lastmodifieddateOrdinal)) productEntity.LastModifiedDate = drProduct.GetDateTime(lastmodifieddateOrdinal);
                        if (!drProduct.IsDBNull(flavorIdOrdinal)) productEntity.FlavourId = drProduct.GetInt32(flavorIdOrdinal);
                        if (!drProduct.IsDBNull(isHighValueOrdinal)) productEntity.IsHighValue = drProduct.GetBoolean(isHighValueOrdinal);
                        if (!drProduct.IsDBNull(isPromoItemOrdinal)) productEntity.IsPromotion = drProduct.GetBoolean(isPromoItemOrdinal);
                        if (!drProduct.IsDBNull(packingIdOrdinal)) productEntity.PackingId = drProduct.GetInt32(packingIdOrdinal);
                        if (!drProduct.IsDBNull(fgCodeOrdinal)) productEntity.FgCode = drProduct.GetString(fgCodeOrdinal);
                        if (!drProduct.IsDBNull(weightOrdinal)) productEntity.Weight = drProduct.GetDouble(weightOrdinal);
                        if (!drProduct.IsDBNull(totalCountOrdinal)) productEntity.TotalCount = drProduct.GetInt32(totalCountOrdinal);

                        productList.Add(productEntity);
                    }
                }

                return productList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetAllProductsWithOutImageForMaster(ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", string.IsNullOrEmpty(args.AdditionalParams) ? "" : args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetAllProductsForMaster"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int statusOrdinal = drProduct.GetOrdinal("status");
                    int bransIdOrdinal = drProduct.GetOrdinal("brand_id");
                    int brandNameOrdinal = drProduct.GetOrdinal("brand_name");
                    int categoryIdOrdinal = drProduct.GetOrdinal("category_id");
                    int categoryNameOrdinal = drProduct.GetOrdinal("category");
                    int skuOrdinal = drProduct.GetOrdinal("sku");
                    int createdbyOrdinal = drProduct.GetOrdinal("created_by");
                    int createddateOrdinal = drProduct.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drProduct.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drProduct.GetOrdinal("last_modified_date");
                    int flavorIdOrdinal = drProduct.GetOrdinal("flavor_id");
                    int isHighValueOrdinal = drProduct.GetOrdinal("is_high_value");
                    int isPromoItemOrdinal = drProduct.GetOrdinal("is_promotion");
                    int isMTItemOrdinal = drProduct.GetOrdinal("is_mt");
                    int packingIdOrdinal = drProduct.GetOrdinal("packing_id");
                    int fgCodeOrdinal = drProduct.GetOrdinal("fg_code");
                    int weightOrdinal = drProduct.GetOrdinal("weight");
                    int totalCountOrdinal = drProduct.GetOrdinal("total_count");

                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(statusOrdinal)) productEntity.Status = drProduct.GetString(statusOrdinal);
                        if (!drProduct.IsDBNull(bransIdOrdinal)) productEntity.BrandId = drProduct.GetInt32(bransIdOrdinal);
                        if (!drProduct.IsDBNull(brandNameOrdinal)) productEntity.BrandName = drProduct.GetString(brandNameOrdinal);
                        if (!drProduct.IsDBNull(categoryIdOrdinal)) productEntity.CategoryId = drProduct.GetInt32(categoryIdOrdinal);
                        if (!drProduct.IsDBNull(categoryNameOrdinal)) productEntity.CategoryName = drProduct.GetString(categoryNameOrdinal);
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);
                        if (!drProduct.IsDBNull(createdbyOrdinal)) productEntity.CreatedBy = drProduct.GetString(createdbyOrdinal);
                        if (!drProduct.IsDBNull(createddateOrdinal)) productEntity.CreatedDate = drProduct.GetDateTime(createddateOrdinal);
                        if (!drProduct.IsDBNull(lastmodifiedbyOrdinal)) productEntity.LastModifiedBy = drProduct.GetString(lastmodifiedbyOrdinal);
                        if (!drProduct.IsDBNull(lastmodifieddateOrdinal)) productEntity.LastModifiedDate = drProduct.GetDateTime(lastmodifieddateOrdinal);
                        if (!drProduct.IsDBNull(flavorIdOrdinal)) productEntity.FlavourId = drProduct.GetInt32(flavorIdOrdinal);
                        if (!drProduct.IsDBNull(isHighValueOrdinal)) productEntity.IsHighValue = drProduct.GetBoolean(isHighValueOrdinal);
                        if (!drProduct.IsDBNull(isPromoItemOrdinal)) productEntity.IsPromotion = drProduct.GetBoolean(isPromoItemOrdinal);
                        if (!drProduct.IsDBNull(packingIdOrdinal)) productEntity.PackingId = drProduct.GetInt32(packingIdOrdinal);
                        if (!drProduct.IsDBNull(fgCodeOrdinal)) productEntity.FgCode = drProduct.GetString(fgCodeOrdinal);
                        if (!drProduct.IsDBNull(weightOrdinal)) productEntity.Weight = drProduct.GetDouble(weightOrdinal);
                        if (!drProduct.IsDBNull(totalCountOrdinal)) productEntity.TotalCount = drProduct.GetInt32(totalCountOrdinal);
                        if (!drProduct.IsDBNull(isMTItemOrdinal)) productEntity.IsMTItem = drProduct.GetBoolean(isMTItemOrdinal);

                        productList.Add(productEntity);
                    }
                }

                return productList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetAllProductsForCustomer(string custcode)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", custcode, DbType.String);

                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetAllProductsForCustomer"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int statusOrdinal = drProduct.GetOrdinal("product_status");


                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(statusOrdinal)) productEntity.ProductStatus = drProduct.GetString(statusOrdinal);


                        productList.Add(productEntity);
                    }
                }

                return productList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public bool UpdateCustomerProductStatus(string custCode, int productId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", custCode, DbType.String);
                paramCollection.Add("@ProductId", productId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["UpdateCustomerProductStatus"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<ProductEntity> GetProductsForInvoice()
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> productList = new List<ProductEntity>();

                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetProductsForInvoice"]);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int retpriceOrdinal = drProduct.GetOrdinal("retail_price");
                    int outpriceOrdinal = drProduct.GetOrdinal("outlet_price");
                    int maxdisOrdinal = drProduct.GetOrdinal("max_discount");
                    int skuOrdinal = drProduct.GetOrdinal("sku");


                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(retpriceOrdinal)) productEntity.RetailPrice = drProduct.GetDouble(retpriceOrdinal);
                        if (!drProduct.IsDBNull(outpriceOrdinal)) productEntity.OutletPrice = drProduct.GetDouble(outpriceOrdinal);
                        if (!drProduct.IsDBNull(maxdisOrdinal)) productEntity.MaxDiscount = drProduct.GetDouble(maxdisOrdinal);
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);


                        productList.Add(productEntity);
                    }
                }

                return productList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetProductsByName(string productNameLike)
        {
            DbDataReader drProgram = null;

            try
            {
                List<ProductEntity> productsList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProductName", productNameLike, DbType.String);

                drProgram = this.DataAcessService.ExecuteQuery(ProductSql["GetProductsByName"], paramCollection);

                if (drProgram != null && drProgram.HasRows)
                {
                    int IdOrdinal = drProgram.GetOrdinal("id");
                    int codeOrdinal = drProgram.GetOrdinal("code");
                    int nameOrdinal = drProgram.GetOrdinal("name");

                    int skuOrdinal = drProgram.GetOrdinal("sku");

                    while (drProgram.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProgram.IsDBNull(IdOrdinal)) productEntity.ProductId = drProgram.GetInt32(IdOrdinal);
                        if (!drProgram.IsDBNull(nameOrdinal)) productEntity.Name = drProgram.GetString(nameOrdinal);
                        if (!drProgram.IsDBNull(codeOrdinal)) productEntity.Code = drProgram.GetString(codeOrdinal);
                        if (!drProgram.IsDBNull(skuOrdinal)) productEntity.Sku = drProgram.GetInt32(skuOrdinal);

                        productsList.Add(productEntity);
                    }
                }

                return productsList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProgram != null)
                    drProgram.Close();
            }
        }

        public bool InsertProducts(ProductEntity productEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Code", productEntity.Code, DbType.String);
                paramCollection.Add("@Name", productEntity.Name, DbType.String);
                paramCollection.Add("@BrandId", productEntity.BrandId, DbType.Int32);
                paramCollection.Add("@CategoryId", productEntity.CategoryId, DbType.Int32);
                paramCollection.Add("@SKU", productEntity.Sku, DbType.Int32);
                paramCollection.Add("@CreatedBy", productEntity.CreatedBy, DbType.String);

                paramCollection.Add("@FgCode", productEntity.FgCode, DbType.String);
                paramCollection.Add("@PackingId", productEntity.PackingId, DbType.Int32);
                paramCollection.Add("@IsHighValue", productEntity.IsHighValue, DbType.Boolean);
                paramCollection.Add("@Weight", productEntity.Weight, DbType.Double);
                paramCollection.Add("@FlavorId", productEntity.FlavourId, DbType.Int32);
                paramCollection.Add("@ImageContent", productEntity.ImageContent, DbType.Binary);
                paramCollection.Add("@IsPromoItem", productEntity.IsPromotion, DbType.Boolean);
                paramCollection.Add("@IsMTItem", productEntity.IsMTItem, DbType.Boolean);

                paramCollection.Add("@CaseConfiguration", productEntity.CaseConfiguration, DbType.Int32);
                paramCollection.Add("@TonnageConfiguration", productEntity.TonnageConfiguration, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["InsertProducts"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateProduct(ProductEntity productEntity)
        {
            //bool successful = false;
            //int iNoRec = 0;

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@Code", productEntity.Code, DbType.String);
            //    paramCollection.Add("@Name", productEntity.Name, DbType.String);
            //    paramCollection.Add("@BrandId", productEntity.BrandId, DbType.Int32);
            //    paramCollection.Add("@SKU", productEntity.Sku, DbType.Int32);
            //    paramCollection.Add("@CategoryId", productEntity.CategoryId, DbType.Int32);
            //    paramCollection.Add("@ProductId", productEntity.ProductId, DbType.Int32);
            //    paramCollection.Add("@LastModifiedBy", productEntity.CreatedBy, DbType.String);

            //    paramCollection.Add("@FgCode", productEntity.FgCode, DbType.String);
            //    paramCollection.Add("@PackingId", productEntity.PackingId, DbType.Int32);
            //    paramCollection.Add("@IsHighValue", productEntity.IsHighValue, DbType.Boolean);
            //    paramCollection.Add("@Weight", productEntity.Weight, DbType.Double);
            //    paramCollection.Add("@FlavorId", productEntity.FlavourId, DbType.Int32);
            //    paramCollection.Add("@IsPromoItem", productEntity.IsPromotion, DbType.Boolean);
            //    paramCollection.Add("@Status", productEntity.Status, DbType.String);
            //    paramCollection.Add("@CaseConfiguration", productEntity.CaseConfiguration, DbType.Int32);

            //    if (productEntity.ImageContent != null)
            //        paramCollection.Add("@ImageContent", productEntity.ImageContent, DbType.Binary);
            //    else
            //        paramCollection.Add("@ImageContent", null, DbType.Binary);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["UpdateProduct"], paramCollection);

            //    if (iNoRec > 0)
            //        successful = true;

            //    return successful;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{

            //}

            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateProduct";

                    objCommand.Parameters.AddWithValue("@Code", productEntity.Code);
                    objCommand.Parameters.AddWithValue("@Name", productEntity.Name);
                    objCommand.Parameters.AddWithValue("@BrandId", productEntity.BrandId);
                    objCommand.Parameters.AddWithValue("@SKU", productEntity.Sku);
                    objCommand.Parameters.AddWithValue("@CategoryId", productEntity.CategoryId);
                    objCommand.Parameters.AddWithValue("@ProductId", productEntity.ProductId);
                    objCommand.Parameters.AddWithValue("@LastModifiedBy", productEntity.CreatedBy);
                    objCommand.Parameters.AddWithValue("@FgCode", productEntity.FgCode);
                    objCommand.Parameters.AddWithValue("@PackingId", productEntity.PackingId);
                    objCommand.Parameters.AddWithValue("@IsHighValue", productEntity.IsHighValue);
                    objCommand.Parameters.AddWithValue("@Weight", productEntity.Weight);
                    objCommand.Parameters.AddWithValue("@FlavorId", productEntity.FlavourId);
                    objCommand.Parameters.AddWithValue("@IsPromoItem", productEntity.IsPromotion);
                    objCommand.Parameters.AddWithValue("@IsMTItem", productEntity.IsMTItem);
                    objCommand.Parameters.AddWithValue("@Status", productEntity.Status);
                    objCommand.Parameters.AddWithValue("@CaseConfiguration", productEntity.CaseConfiguration);
                    objCommand.Parameters.AddWithValue("@TonnageConfiguration", productEntity.TonnageConfiguration);



                    if (productEntity.ImageContent != null)
                        objCommand.Parameters.AddWithValue("@ImageContent", productEntity.ImageContent);
                    else
                        objCommand.Parameters.AddWithValue("@ImageContent", null);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool InactiveProduct(ProductEntity productEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProductId", productEntity.ProductId, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", productEntity.CreatedBy, DbType.String);
                paramCollection.Add("@Status", productEntity.Status, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["InactiveProduct"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<ProductCategoryEntity> GetAllProductCategories(ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductCategoryEntity> categoryList = new List<ProductCategoryEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", string.IsNullOrEmpty(args.AdditionalParams) ? "" : args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetAllProductCategories"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("category_code");
                    int descriptionOrdinal = drProduct.GetOrdinal("description");

                    while (drProduct.Read())
                    {
                        ProductCategoryEntity categoryEntity = ProductCategoryEntity.CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) categoryEntity.Id = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) categoryEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(descriptionOrdinal)) categoryEntity.Description = drProduct.GetString(descriptionOrdinal);

                        categoryList.Add(categoryEntity);
                    }
                }

                return categoryList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }


        public bool IsProductCodeExists(ProductEntity productEntity)
        {
            DbDataReader drProduct = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProductId", productEntity.ProductId, DbType.Int32);
                paramCollection.Add("@ProductCode", productEntity.Code, DbType.String);

                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["ProductCodeExists"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public bool IsProductNameExists(ProductEntity productEntity)
        {
            DbDataReader drProduct = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@ProductId", productEntity.ProductId, DbType.Int32);
                paramCollection.Add("@ProductName", productEntity.Name, DbType.String);

                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["ProductNameExists"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetAllProductsWithNoPrice(ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", string.IsNullOrEmpty(args.AdditionalParams) ? "" : args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetAllProductsWithNoPrice"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int bransIdOrdinal = drProduct.GetOrdinal("brand_id");
                    int skuOrdinal = drProduct.GetOrdinal("sku");
                    int totalCountOrdinal = drProduct.GetOrdinal("total_count");

                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(bransIdOrdinal)) productEntity.BrandId = drProduct.GetInt32(bransIdOrdinal);
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);
                        if (!drProduct.IsDBNull(totalCountOrdinal)) productEntity.TotalCount = drProduct.GetInt32(totalCountOrdinal);
                        productList.Add(productEntity);
                    }
                }

                return productList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetAllProductPricesDataAndCount(ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", string.IsNullOrEmpty(args.AdditionalParams) ? "" : args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetAllProductPricesDataAndCount"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int productIdOrdinal = drProduct.GetOrdinal("product_id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int outletRetailPriceOrdinal = drProduct.GetOrdinal("outlet_retail_price");
                    int outletConsumerPriceOrdinal = drProduct.GetOrdinal("outlet_consumer_price");
                    int distributorPriceOrdinal = drProduct.GetOrdinal("distributor_price");
                    int skuOrdinal = drProduct.GetOrdinal("sku");
                    int totalCountOrdinal = drProduct.GetOrdinal("total_count");

                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductPriceId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(productIdOrdinal)) productEntity.ProductId = drProduct.GetInt32(productIdOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(outletRetailPriceOrdinal)) productEntity.RetailPrice = Convert.ToDouble(drProduct.GetDecimal(outletRetailPriceOrdinal));
                        if (!drProduct.IsDBNull(outletConsumerPriceOrdinal)) productEntity.OutletPrice = Convert.ToDouble(drProduct.GetDecimal(outletConsumerPriceOrdinal));
                        if (!drProduct.IsDBNull(distributorPriceOrdinal)) productEntity.DistributorPrice = Convert.ToDouble(drProduct.GetDecimal(distributorPriceOrdinal));
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);
                        if (!drProduct.IsDBNull(totalCountOrdinal)) productEntity.TotalCount = drProduct.GetInt32(totalCountOrdinal);
                        productList.Add(productEntity);
                    }
                }

                return productList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public bool InsertProductPrice(ProductEntity productEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProductId", productEntity.ProductId, DbType.Int32);
                paramCollection.Add("@OutletRetailPrice", productEntity.RetailPrice, DbType.Decimal);
                paramCollection.Add("@OutletConsumerPrice", productEntity.OutletPrice, DbType.Decimal);
                paramCollection.Add("@DistributorPrice", productEntity.DistributorPrice, DbType.Decimal);
                paramCollection.Add("@CreatedBy", productEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["InsertProductPrice"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool UpdateProductPrice(ProductEntity productEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProductPriceId", productEntity.ProductPriceId, DbType.Int32);
                paramCollection.Add("@ProductId", productEntity.ProductId, DbType.Int32);
                paramCollection.Add("@OutletRetailPrice", productEntity.RetailPrice, DbType.Decimal);
                paramCollection.Add("@OutletConsumerPrice", productEntity.OutletPrice, DbType.Decimal);
                paramCollection.Add("@DistributorPrice", productEntity.OutletPrice, DbType.Decimal);
                paramCollection.Add("@LastModifiedBy", productEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["InsertProductPrice"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool DeleteProductPrice(ProductEntity productEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProductPriceId", productEntity.ProductPriceId, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", productEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["DeleteProductPrice"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }


        public bool InsertProductFreshness(MobileCustomerProductEntity productEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", productEntity.CustomerCode, DbType.String);
                paramCollection.Add("@Type", productEntity.Type, DbType.String);
                paramCollection.Add("@ProductId", productEntity.ProductId, DbType.Int32);
                paramCollection.Add("@Quentity", productEntity.Quentity, DbType.Int32);
                paramCollection.Add("@CreatedBy", productEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["InsertProductFreshness"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        public bool InsertCustomerProductslog(MobileCustomerProductEntity mobileCustomerProductEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", mobileCustomerProductEntity.CustomerCode, DbType.String);
                paramCollection.Add("@ProductId", mobileCustomerProductEntity.ProductId, DbType.Int32);
                paramCollection.Add("@CreatedBy", mobileCustomerProductEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["InsertCustomerProductlog"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Updates All the records For today, as Deactivated For the customer
        /// (only Todays record for the relevent customer)
        /// </summary>
        /// <param name="objectiveEntity"></param>
        /// <returns></returns>
        public bool DeleteAllCustomerProductsLogForToday(MobileCustomerProductEntity mobileCustomerProductEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", mobileCustomerProductEntity.CustomerCode, DbType.String);
                paramCollection.Add("@LastModifiedBy", mobileCustomerProductEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["DeleteAllCustomerProductsLogForToday"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<MobileCustomerProductEntity> GetCustomerProductsForMostRecentDate(string customerCode)
        {
            DbDataReader drProduct = null;
            try
            {
                List<MobileCustomerProductEntity> entityList = new List<MobileCustomerProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", customerCode, DbType.String);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetCustomerProductsForMostRecentDate"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int productIdOrdinal = drProduct.GetOrdinal("product_id");
                    int createdDateOrdinal = drProduct.GetOrdinal("created_date");
                    int custCodeOrdinal = drProduct.GetOrdinal("cust_code");

                    MobileCustomerProductEntity mobileCustomerProductEntity = null;
                    while (drProduct.Read())
                    {
                        mobileCustomerProductEntity = MobileCustomerProductEntity.CreateObject();

                        if (!drProduct.IsDBNull(productIdOrdinal)) mobileCustomerProductEntity.ProductId = drProduct.GetInt32(productIdOrdinal);
                        if (!drProduct.IsDBNull(createdDateOrdinal)) mobileCustomerProductEntity.CreatedDate = drProduct.GetDateTime(createdDateOrdinal);
                        if (!drProduct.IsDBNull(custCodeOrdinal))
                        { mobileCustomerProductEntity.CustomerCode = drProduct.GetString(custCodeOrdinal); }
                        else
                        { mobileCustomerProductEntity.CustomerCode = customerCode; }

                        entityList.Add(mobileCustomerProductEntity);
                    }
                }
                return entityList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public bool InsertProductDivisions(ProductEntity productEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drProduct = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProductId", productEntity.ProductId, DbType.Int32);
                paramCollection.Add("@DivisionId", productEntity.DivisionId, DbType.Int32);
                paramCollection.Add("@CreatedBy", productEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["InsertProductDivisions"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetProductsByDivisionId(int divisionId, ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductEntity> Productlist = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetProductsByDivisionId"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int productIdOrdinal = drProduct.GetOrdinal("product_id");
                    int productNameOrdinal = drProduct.GetOrdinal("name");
                    int productfg_codeOrdinal = drProduct.GetOrdinal("fg_code");
                    int divisionIdOrdinal = drProduct.GetOrdinal("division_id");
                    int divisionNameOrdinal = drProduct.GetOrdinal("division_name");
                    int totalCountOrdinal = drProduct.GetOrdinal("total_count");


                    while (drProduct.Read())
                    {
                        ProductEntity productEntity = CreateObject();

                        if (!drProduct.IsDBNull(productIdOrdinal)) productEntity.ProductId = drProduct.GetInt32(productIdOrdinal);
                        if (!drProduct.IsDBNull(productfg_codeOrdinal)) productEntity.FgCode = drProduct.GetString(productfg_codeOrdinal);
                        if (!drProduct.IsDBNull(productNameOrdinal)) productEntity.Name = drProduct.GetString(productNameOrdinal);
                        if (!drProduct.IsDBNull(divisionIdOrdinal)) productEntity.DivisionId = drProduct.GetInt32(divisionIdOrdinal);
                        if (!drProduct.IsDBNull(divisionNameOrdinal)) productEntity.DivisionName = drProduct.GetString(divisionNameOrdinal);
                        if (!drProduct.IsDBNull(totalCountOrdinal)) productEntity.TotalCount = drProduct.GetInt32(totalCountOrdinal);

                        Productlist.Add(productEntity);
                    }
                }

                return Productlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public bool DeleteProductDivisionsByDivisionId(int divisionId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drProduct = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["DeleteProductDivisionsByDivisionId"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public bool DeleteProductDivisionsByProductId(int divisionId, int productId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drProduct = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@product_id", productId, DbType.Int32);
                paramCollection.Add("@division_id", divisionId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductSql["DeleteProductDivisionsByProductId"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductEntity> GetProductsByAccessToken(string accessToken, string syncDate)
        {
            DataTable objDS = new DataTable();
            List<ProductEntity> Productlist = new List<ProductEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetProductsByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@SyncDate", syncDate);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            ProductEntity productEntity = CreateObject();
                            if (item["id"] != DBNull.Value) productEntity.ProductId = Convert.ToInt32(item["id"]);
                            if (item["name"] != DBNull.Value) productEntity.Name = item["name"].ToString();
                            if (item["brand_id"] != DBNull.Value) productEntity.BrandId = Convert.ToInt32(item["brand_id"]);
                            if (item["category_id"] != DBNull.Value) productEntity.CategoryId = Convert.ToInt32(item["category_id"]);
                            if (item["packing_id"] != DBNull.Value) productEntity.PackingId = Convert.ToInt32(item["packing_id"]);
                            if (item["is_high_value"] != DBNull.Value) productEntity.IsHighValue = Convert.ToBoolean(item["is_high_value"]);
                            if (item["flavor_id"] != DBNull.Value) productEntity.FlavourId = Convert.ToInt32(item["flavor_id"]);
                            if (item["fg_code"] != DBNull.Value) productEntity.Code = item["fg_code"].ToString();
                            if (item["weight"] != DBNull.Value) productEntity.Weight = Convert.ToDouble(item["weight"]);
                            if (item["status"] != DBNull.Value) productEntity.Status = item["status"].ToString();
                            if (item["sku"] != DBNull.Value) productEntity.Sku = Convert.ToInt32(item["sku"]);
                            if (item["outlet_retail_price"] != DBNull.Value) productEntity.RetailPrice = Convert.ToDouble(item["outlet_retail_price"]);
                            if (item["outlet_consumer_price"] != DBNull.Value) productEntity.OutletPrice = Convert.ToDouble(item["outlet_consumer_price"]);
                            if (item["is_promotion"] != DBNull.Value) productEntity.IsPromotion = Convert.ToBoolean(item["is_promotion"]);

                            if (item["image_content"] != null)
                            {
                                if (item["image_content"] != DBNull.Value) productEntity.ImageContent = (byte[])item["image_content"];
                            }

                            Productlist.Add(productEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return Productlist;
        }

        public List<ProductEntity> GetProductsForModernTradeByAccessToken(string accessToken, string syncDate)
        {
            DataTable objDS = new DataTable();
            List<ProductEntity> Productlist = new List<ProductEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetProductsForModernTradeByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@SyncDate", syncDate);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            ProductEntity productEntity = CreateObject();
                            if (item["id"] != DBNull.Value) productEntity.ProductId = Convert.ToInt32(item["id"]);
                            if (item["name"] != DBNull.Value) productEntity.Name = item["name"].ToString();
                            if (item["brand_id"] != DBNull.Value) productEntity.BrandId = Convert.ToInt32(item["brand_id"]);
                            if (item["category_id"] != DBNull.Value) productEntity.CategoryId = Convert.ToInt32(item["category_id"]);
                            if (item["packing_id"] != DBNull.Value) productEntity.PackingId = Convert.ToInt32(item["packing_id"]);
                            if (item["is_high_value"] != DBNull.Value) productEntity.IsHighValue = Convert.ToBoolean(item["is_high_value"]);
                            if (item["flavor_id"] != DBNull.Value) productEntity.FlavourId = Convert.ToInt32(item["flavor_id"]);
                            if (item["fg_code"] != DBNull.Value) productEntity.Code = item["fg_code"].ToString();
                            if (item["weight"] != DBNull.Value) productEntity.Weight = Convert.ToDouble(item["weight"]);
                            if (item["status"] != DBNull.Value) productEntity.Status = item["status"].ToString();
                            if (item["sku"] != DBNull.Value) productEntity.Sku = Convert.ToInt32(item["sku"]);
                            if (item["outlet_retail_price"] != DBNull.Value) productEntity.RetailPrice = Convert.ToDouble(item["outlet_retail_price"]);
                            if (item["outlet_consumer_price"] != DBNull.Value) productEntity.OutletPrice = Convert.ToDouble(item["outlet_consumer_price"]);
                            if (item["is_promotion"] != DBNull.Value) productEntity.IsPromotion = Convert.ToBoolean(item["is_promotion"]);

                            if (item["image_content"] != null)
                            {
                                if (item["image_content"] != DBNull.Value) productEntity.ImageContent = (byte[])item["image_content"];
                            }

                            Productlist.Add(productEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return Productlist;
        }

        public List<ProductCategoryEntity> GetProductCategoriesByAccessToken(string accessToken)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductCategoryEntity> categoryList = new List<ProductCategoryEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);

                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetProductCategoriesByAccessToken"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("category_code");
                    int descriptionOrdinal = drProduct.GetOrdinal("description");

                    while (drProduct.Read())
                    {
                        ProductCategoryEntity categoryEntity = ProductCategoryEntity.CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) categoryEntity.Id = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) categoryEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(descriptionOrdinal)) categoryEntity.Description = drProduct.GetString(descriptionOrdinal);

                        categoryList.Add(categoryEntity);
                    }
                }

                return categoryList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public byte[] GetProductsByAccessTokenAndId(string accessToken, int productId)
        {
            DbDataReader drProduct = null;

            try
            {
                byte[] image = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);
                paramCollection.Add("@ProductId", productId, DbType.Int32);

                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetProductsByAccessTokenAndId"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int imageContentOrdinal = drProduct.GetOrdinal("image_content");

                    if (drProduct.Read())
                    {
                        if (drProduct[imageContentOrdinal] != null)
                        {
                            if (!drProduct.IsDBNull(imageContentOrdinal)) image = (byte[])drProduct[imageContentOrdinal];
                        }
                    }
                }

                return image;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public ProductEntity GetProductById(int productId)
        {
            DbDataReader drProduct = null;

            try
            {
                //List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ProductId", productId, DbType.Int32);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetProductById"], paramCollection);

                ProductEntity productEntity = CreateObject();

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int statusOrdinal = drProduct.GetOrdinal("status");
                    int bransIdOrdinal = drProduct.GetOrdinal("brand_id");
                    int brandNameOrdinal = drProduct.GetOrdinal("brand_name");
                    int categoryIdOrdinal = drProduct.GetOrdinal("category_id");
                    int categoryNameOrdinal = drProduct.GetOrdinal("category");
                    int skuOrdinal = drProduct.GetOrdinal("sku");
                    int createdbyOrdinal = drProduct.GetOrdinal("created_by");
                    int createddateOrdinal = drProduct.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drProduct.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drProduct.GetOrdinal("last_modified_date");

                    int flavorIdOrdinal = drProduct.GetOrdinal("flavor_id");
                    int isHighValueOrdinal = drProduct.GetOrdinal("is_high_value");
                    int packingIdOrdinal = drProduct.GetOrdinal("packing_id");
                    int fgCodeOrdinal = drProduct.GetOrdinal("fg_code");
                    int imageContentOrdinal = drProduct.GetOrdinal("image_content");
                    int weightOrdinal = drProduct.GetOrdinal("weight");

                    while (drProduct.Read())
                    {
                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(statusOrdinal)) productEntity.Status = drProduct.GetString(statusOrdinal);
                        if (!drProduct.IsDBNull(bransIdOrdinal)) productEntity.BrandId = drProduct.GetInt32(bransIdOrdinal);
                        if (!drProduct.IsDBNull(brandNameOrdinal)) productEntity.BrandName = drProduct.GetString(brandNameOrdinal);
                        if (!drProduct.IsDBNull(categoryIdOrdinal)) productEntity.CategoryId = drProduct.GetInt32(categoryIdOrdinal);
                        if (!drProduct.IsDBNull(categoryNameOrdinal)) productEntity.CategoryName = drProduct.GetString(categoryNameOrdinal);
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);
                        if (!drProduct.IsDBNull(createdbyOrdinal)) productEntity.CreatedBy = drProduct.GetString(createdbyOrdinal);
                        if (!drProduct.IsDBNull(createddateOrdinal)) productEntity.CreatedDate = drProduct.GetDateTime(createddateOrdinal);
                        if (!drProduct.IsDBNull(lastmodifiedbyOrdinal)) productEntity.LastModifiedBy = drProduct.GetString(lastmodifiedbyOrdinal);
                        if (!drProduct.IsDBNull(lastmodifieddateOrdinal)) productEntity.LastModifiedDate = drProduct.GetDateTime(lastmodifieddateOrdinal);

                        if (!drProduct.IsDBNull(flavorIdOrdinal)) productEntity.FlavourId = drProduct.GetInt32(flavorIdOrdinal);
                        if (!drProduct.IsDBNull(isHighValueOrdinal)) productEntity.IsHighValue = drProduct.GetBoolean(isHighValueOrdinal);
                        if (!drProduct.IsDBNull(packingIdOrdinal)) productEntity.PackingId = drProduct.GetInt32(packingIdOrdinal);

                        if (!drProduct.IsDBNull(fgCodeOrdinal)) productEntity.FgCode = drProduct.GetString(fgCodeOrdinal);
                        if (!drProduct.IsDBNull(weightOrdinal)) productEntity.Weight = drProduct.GetDouble(weightOrdinal);

                        if (drProduct[imageContentOrdinal] != null)
                        {
                            if (!drProduct.IsDBNull(imageContentOrdinal)) productEntity.ImageContent = (byte[])drProduct[imageContentOrdinal];
                        }
                    }
                }

                return productEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public ProductEntity GetProductByCode(string code)
        {
            DbDataReader drProduct = null;

            try
            {
                //List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Code", code, DbType.String);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetProductByCode"], paramCollection);

                ProductEntity productEntity = CreateObject();

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int statusOrdinal = drProduct.GetOrdinal("status");
                    int bransIdOrdinal = drProduct.GetOrdinal("brand_id");
                    int brandNameOrdinal = drProduct.GetOrdinal("brand_name");
                    int categoryIdOrdinal = drProduct.GetOrdinal("category_id");
                    int categoryNameOrdinal = drProduct.GetOrdinal("category");
                    int skuOrdinal = drProduct.GetOrdinal("sku");
                    int createdbyOrdinal = drProduct.GetOrdinal("created_by");
                    int createddateOrdinal = drProduct.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drProduct.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drProduct.GetOrdinal("last_modified_date");

                    int flavorIdOrdinal = drProduct.GetOrdinal("flavor_id");
                    int isHighValueOrdinal = drProduct.GetOrdinal("is_high_value");
                    int packingIdOrdinal = drProduct.GetOrdinal("packing_id");
                    int fgCodeOrdinal = drProduct.GetOrdinal("fg_code");
                    int imageContentOrdinal = drProduct.GetOrdinal("image_content");
                    int weightOrdinal = drProduct.GetOrdinal("weight");

                    while (drProduct.Read())
                    {
                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(statusOrdinal)) productEntity.Status = drProduct.GetString(statusOrdinal);
                        if (!drProduct.IsDBNull(bransIdOrdinal)) productEntity.BrandId = drProduct.GetInt32(bransIdOrdinal);
                        if (!drProduct.IsDBNull(brandNameOrdinal)) productEntity.BrandName = drProduct.GetString(brandNameOrdinal);
                        if (!drProduct.IsDBNull(categoryIdOrdinal)) productEntity.CategoryId = drProduct.GetInt32(categoryIdOrdinal);
                        if (!drProduct.IsDBNull(categoryNameOrdinal)) productEntity.CategoryName = drProduct.GetString(categoryNameOrdinal);
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);
                        if (!drProduct.IsDBNull(createdbyOrdinal)) productEntity.CreatedBy = drProduct.GetString(createdbyOrdinal);
                        if (!drProduct.IsDBNull(createddateOrdinal)) productEntity.CreatedDate = drProduct.GetDateTime(createddateOrdinal);
                        if (!drProduct.IsDBNull(lastmodifiedbyOrdinal)) productEntity.LastModifiedBy = drProduct.GetString(lastmodifiedbyOrdinal);
                        if (!drProduct.IsDBNull(lastmodifieddateOrdinal)) productEntity.LastModifiedDate = drProduct.GetDateTime(lastmodifieddateOrdinal);

                        if (!drProduct.IsDBNull(flavorIdOrdinal)) productEntity.FlavourId = drProduct.GetInt32(flavorIdOrdinal);
                        if (!drProduct.IsDBNull(isHighValueOrdinal)) productEntity.IsHighValue = drProduct.GetBoolean(isHighValueOrdinal);
                        if (!drProduct.IsDBNull(packingIdOrdinal)) productEntity.PackingId = drProduct.GetInt32(packingIdOrdinal);

                        if (!drProduct.IsDBNull(fgCodeOrdinal)) productEntity.FgCode = drProduct.GetString(fgCodeOrdinal);
                        if (!drProduct.IsDBNull(weightOrdinal)) productEntity.Weight = drProduct.GetDouble(weightOrdinal);

                        if (drProduct[imageContentOrdinal] != null)
                        {
                            if (!drProduct.IsDBNull(imageContentOrdinal)) productEntity.ImageContent = (byte[])drProduct[imageContentOrdinal];
                        }
                    }
                }

                return productEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public int IsProductExsistInDiscountScheme(int product_id)
        {
            DbDataReader drProduct = null;
            int numRecords = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@product_id", product_id, DbType.Int32);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["IsProductExsistInDiscountScheme"], paramCollection);

                ProductEntity productEntity = CreateObject();

                if (drProduct != null && drProduct.HasRows)
                {
                    numRecords = 1;
                }

                return numRecords;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        #region "Perfetti 2 Phase Developemtn"

        public List<ProductModel> GetAllProducts(ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<ProductModel> Productlist = new List<ProductModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllProducts";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AdditionalParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            ProductModel productModel = new ProductModel();
                            if (item["id"] != DBNull.Value) productModel.ProductId = Convert.ToInt32(item["id"]);
                            if (item["code"] != DBNull.Value) productModel.Code = item["code"].ToString();
                            if (item["name"] != DBNull.Value) productModel.Name = item["name"].ToString();
                            if (item["fg_code"] != DBNull.Value) productModel.FgCode = item["fg_code"].ToString();
                            if (item["brand_id"] != DBNull.Value) productModel.BrandId = Convert.ToInt32(item["brand_id"]);
                            if (item["category_id"] != DBNull.Value) productModel.CategoryId = Convert.ToInt32(item["category_id"]);
                            if (item["packing_id"] != DBNull.Value) productModel.PackingId = Convert.ToInt32(item["packing_id"]);
                            if (item["is_high_value"] != DBNull.Value) productModel.IsHighValue = Convert.ToBoolean(item["is_high_value"]);
                            if (item["flavor_id"] != DBNull.Value) productModel.FlavourId = Convert.ToInt32(item["flavor_id"]);
                            if (item["weight"] != DBNull.Value) productModel.Weight = Convert.ToDouble(item["weight"]);
                            if (item["status"] != DBNull.Value) productModel.Status = item["status"].ToString();
                            if (item["sku"] != DBNull.Value) productModel.Sku = Convert.ToInt32(item["sku"]);
                            //if (item["outlet_retail_price"] != DBNull.Value) productModel.RetailPrice = Convert.ToDouble(item["outlet_retail_price"]);
                            //if (item["outlet_consumer_price"] != DBNull.Value) productModel.OutletPrice = Convert.ToDouble(item["outlet_consumer_price"]);
                            if (item["is_promotion"] != DBNull.Value) productModel.IsPromotion = Convert.ToBoolean(item["is_promotion"]);
                            //if (item["prod_territory_id"] != DBNull.Value) productModel.ProductTerritoryId = Convert.ToInt32(item["prod_territory_id"]);
                            //if (item["prod_territory_status"] != DBNull.Value) productModel.ProductTerritoryStatus = item["prod_territory_status"].ToString();
                            if (item["total_count"] != DBNull.Value) productModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            Productlist.Add(productModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return Productlist;
        }

        #region "Territory Products"

        public List<ProductModel> GetAllProductsByTerritoryId(ArgsModel args, string TerritoryId)
        {
            DataTable objDS = new DataTable();
            List<ProductModel> Productlist = new List<ProductModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllProductsByTerritoryId";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@TerritoryId", TerritoryId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            ProductModel productModel = new ProductModel();
                            if (item["id"] != DBNull.Value) productModel.ProductId = Convert.ToInt32(item["id"]);
                            if (item["code"] != DBNull.Value) productModel.Code = item["code"].ToString();
                            if (item["name"] != DBNull.Value) productModel.Name = item["name"].ToString();
                            if (item["fg_code"] != DBNull.Value) productModel.FgCode = item["fg_code"].ToString();
                            if (item["brand_id"] != DBNull.Value) productModel.BrandId = Convert.ToInt32(item["brand_id"]);
                            if (item["category_id"] != DBNull.Value) productModel.CategoryId = Convert.ToInt32(item["category_id"]);
                            if (item["packing_id"] != DBNull.Value) productModel.PackingId = Convert.ToInt32(item["packing_id"]);
                            if (item["is_high_value"] != DBNull.Value) productModel.IsHighValue = Convert.ToBoolean(item["is_high_value"]);
                            if (item["flavor_id"] != DBNull.Value) productModel.FlavourId = Convert.ToInt32(item["flavor_id"]);
                            if (item["weight"] != DBNull.Value) productModel.Weight = Convert.ToDouble(item["weight"]);
                            if (item["status"] != DBNull.Value) productModel.Status = item["status"].ToString();
                            if (item["sku"] != DBNull.Value) productModel.Sku = Convert.ToInt32(item["sku"]);
                            //if (item["outlet_retail_price"] != DBNull.Value) productModel.RetailPrice = Convert.ToDouble(item["outlet_retail_price"]);
                            //if (item["outlet_consumer_price"] != DBNull.Value) productModel.OutletPrice = Convert.ToDouble(item["outlet_consumer_price"]);
                            if (item["is_promotion"] != DBNull.Value) productModel.IsPromotion = Convert.ToBoolean(item["is_promotion"]);
                            if (item["prod_territory_id"] != DBNull.Value) productModel.ProductTerritoryId = Convert.ToInt32(item["prod_territory_id"]);
                            if (item["prod_territory_status"] != DBNull.Value) productModel.ProductTerritoryStatus = item["prod_territory_status"].ToString();
                            if (item["total_count"] != DBNull.Value) productModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            Productlist.Add(productModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return Productlist;
        }

        public List<TerritoryModel> GetAllTerritoriesByProductId(ArgsModel args, string ProductId)
        {
            DataTable objDS = new DataTable();
            List<TerritoryModel> objlist = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoriesByProductId";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@ProductId", ProductId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            TerritoryModel objModel = new TerritoryModel();
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            //if (item["rep_code"] != DBNull.Value) objModel.RepCode = item["rep_code"].ToString();
                            //if (item["rep_name"] != DBNull.Value) objModel.RepName = item["rep_name"].ToString();
                            if (item["prod_territory_status"] != DBNull.Value) objModel.ProductTerritoryStatus = item["prod_territory_status"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objlist.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objlist;
        }

        public bool SaveUpdateTerritoryProducts(string originator,
                                                       string territory_id,
                                                       string product_id,
                                                       string is_checked)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertUpdateTerritoryProducts";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@TerritoryId", territory_id);
                    objCommand.Parameters.AddWithValue("@ProductId", product_id);
                    objCommand.Parameters.AddWithValue("@IsChecked", is_checked);

                    //if (territory_id == "0" || territory_id == "")
                    //{
                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                    //}
                    //else
                    //{
                    //    retStatus = false;
                    //}
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        #endregion

        public ProductModel GetProductByProductCode(string code)
        {
            DbDataReader drProduct = null;

            try
            {
                //List<ProductEntity> productList = new List<ProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Code", code, DbType.String);
                drProduct = this.DataAcessService.ExecuteQuery(ProductSql["GetProductByCode"], paramCollection);

                ProductModel productEntity = new ProductModel();

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("id");
                    int codeOrdinal = drProduct.GetOrdinal("code");
                    int nameOrdinal = drProduct.GetOrdinal("name");
                    int statusOrdinal = drProduct.GetOrdinal("status");
                    int bransIdOrdinal = drProduct.GetOrdinal("brand_id");
                    int brandNameOrdinal = drProduct.GetOrdinal("brand_name");
                    int categoryIdOrdinal = drProduct.GetOrdinal("category_id");
                    int categoryNameOrdinal = drProduct.GetOrdinal("category");
                    int skuOrdinal = drProduct.GetOrdinal("sku");
                    int createdbyOrdinal = drProduct.GetOrdinal("created_by");
                    int createddateOrdinal = drProduct.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drProduct.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drProduct.GetOrdinal("last_modified_date");

                    int flavorIdOrdinal = drProduct.GetOrdinal("flavor_id");
                    int isHighValueOrdinal = drProduct.GetOrdinal("is_high_value");
                    int packingIdOrdinal = drProduct.GetOrdinal("packing_id");
                    int fgCodeOrdinal = drProduct.GetOrdinal("fg_code");
                    int imageContentOrdinal = drProduct.GetOrdinal("image_content");
                    int weightOrdinal = drProduct.GetOrdinal("weight");

                    while (drProduct.Read())
                    {
                        if (!drProduct.IsDBNull(idOrdinal)) productEntity.ProductId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(codeOrdinal)) productEntity.Code = drProduct.GetString(codeOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productEntity.Name = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(statusOrdinal)) productEntity.Status = drProduct.GetString(statusOrdinal);
                        if (!drProduct.IsDBNull(bransIdOrdinal)) productEntity.BrandId = drProduct.GetInt32(bransIdOrdinal);
                        if (!drProduct.IsDBNull(brandNameOrdinal)) productEntity.BrandName = drProduct.GetString(brandNameOrdinal);
                        if (!drProduct.IsDBNull(categoryIdOrdinal)) productEntity.CategoryId = drProduct.GetInt32(categoryIdOrdinal);
                        if (!drProduct.IsDBNull(categoryNameOrdinal)) productEntity.CategoryName = drProduct.GetString(categoryNameOrdinal);
                        if (!drProduct.IsDBNull(skuOrdinal)) productEntity.Sku = drProduct.GetInt32(skuOrdinal);
                        if (!drProduct.IsDBNull(createdbyOrdinal)) productEntity.CreatedBy = drProduct.GetString(createdbyOrdinal);
                        if (!drProduct.IsDBNull(createddateOrdinal)) productEntity.CreatedDate = drProduct.GetDateTime(createddateOrdinal);
                        if (!drProduct.IsDBNull(lastmodifiedbyOrdinal)) productEntity.LastModifiedBy = drProduct.GetString(lastmodifiedbyOrdinal);
                        if (!drProduct.IsDBNull(lastmodifieddateOrdinal)) productEntity.LastModifiedDate = drProduct.GetDateTime(lastmodifieddateOrdinal);

                        if (!drProduct.IsDBNull(flavorIdOrdinal)) productEntity.FlavourId = drProduct.GetInt32(flavorIdOrdinal);
                        if (!drProduct.IsDBNull(isHighValueOrdinal)) productEntity.IsHighValue = drProduct.GetBoolean(isHighValueOrdinal);
                        if (!drProduct.IsDBNull(packingIdOrdinal)) productEntity.PackingId = drProduct.GetInt32(packingIdOrdinal);

                        if (!drProduct.IsDBNull(fgCodeOrdinal)) productEntity.FgCode = drProduct.GetString(fgCodeOrdinal);
                        if (!drProduct.IsDBNull(weightOrdinal)) productEntity.Weight = drProduct.GetDouble(weightOrdinal);

                        if (drProduct[imageContentOrdinal] != null)
                        {
                            if (!drProduct.IsDBNull(imageContentOrdinal)) productEntity.ImageContent = (byte[])drProduct[imageContentOrdinal];
                        }
                    }
                }

                return productEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        #endregion
        //Added By Rangan 
        public List<ProductUOMModel> GetProductUOMByAccessToken(string accessToken)
        {
            ProductUOMModel productUOMEntity = null;
            DataTable objDS = new DataTable();
            List<ProductUOMModel> ProductList = new List<ProductUOMModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "getproductuombyaccesstoken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);


                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            productUOMEntity = new ProductUOMModel();
                            if (item["id"] != DBNull.Value) productUOMEntity.Id = Convert.ToInt32(item["id"]);
                            if (item["code"] != DBNull.Value) productUOMEntity.Code = item["code"].ToString();
                            if (item["product_id"] != DBNull.Value) productUOMEntity.ProductId = Convert.ToInt32(item["product_id"]);
                            if (item["name"] != DBNull.Value) productUOMEntity.Name = item["name"].ToString();
                            if (item["conversion"] != DBNull.Value)
                            {
                                float conversionValue;
                                if (float.TryParse(item["conversion"].ToString(), out conversionValue))
                                {
                                    productUOMEntity.Conversion = conversionValue;
                                }
                               
                            }

                            //if (item["conversion"] != DBNull.Value) productUOMEntity.Conversion = item["conversion"].ToString();
                            if (item["status"] != DBNull.Value) productUOMEntity.Status = item["status"].ToString();
                            if (item["fg_code"] != DBNull.Value) productUOMEntity.FgCode = item["fg_code"].ToString();
                            if (item["description"] != DBNull.Value) productUOMEntity.Description = item["description"].ToString();

                            ProductList.Add(productUOMEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return ProductList;
        }



        public List<CategoryECOModel> GetCategoryECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            CategoryECOModel categoryEntity = null;
            DataTable objDS = new DataTable();
            List<CategoryECOModel> CategoryList = new List<CategoryECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetCategoryECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            categoryEntity = new CategoryECOModel();

                            if (item["CatCd"] != DBNull.Value) categoryEntity.CategoryCode = item["CatCd"].ToString();

                            if (item["CatNm"] != DBNull.Value) categoryEntity.CategoryName = item["CatNm"].ToString();

                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    categoryEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    categoryEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    categoryEntity.isPC = false; // Or set a default value
                                }
                            }






                            CategoryList.Add(categoryEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return CategoryList;
        }



        public List<ProductECOModel> GetProductECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            ProductECOModel productEntity = null;
            DataTable objDS = new DataTable();
            List<ProductECOModel> ProductList = new List<ProductECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetProductECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            productEntity = new ProductECOModel();

                            if (item["ProNm"] != DBNull.Value) productEntity.ProductName = item["ProNm"].ToString();

                            if (item["ProCd"] != DBNull.Value) productEntity.ProductCode = item["ProCd"].ToString();
                            if (item["FgCd"] != DBNull.Value) productEntity.ProductFGCode = item["FgCd"].ToString();


                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    productEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    productEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    productEntity.isPC = false; // Or set a default value
                                }
                            }






                            ProductList.Add(productEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return ProductList;
        }



    }
}
