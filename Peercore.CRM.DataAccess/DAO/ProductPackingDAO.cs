﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ProductPackingDAO : BaseDAO
    {
        private DbSqlAdapter PackingSql { get; set; }

        private void RegisterSql()
        {
            this.PackingSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ProductPackingDAO()
        {
            RegisterSql();
        }

        public ProductPackingDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ProductPackingEntity CreateObject()
        {
            return ProductPackingEntity.CreateObject();
        }

        public ProductPackingEntity CreateObject(int entityId)
        {
            return ProductPackingEntity.CreateObject(entityId);
        }

        public List<ProductPackingEntity> GetProductPackingByAccessToken(string accessToken)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductPackingEntity> packingList = new List<ProductPackingEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);

                drProduct = this.DataAcessService.ExecuteQuery(PackingSql["GetProductPackingByAccessToken"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("product_packing_id");
                    int nameOrdinal = drProduct.GetOrdinal("packing_name");

                    while (drProduct.Read())
                    {
                        ProductPackingEntity packingEntity = ProductPackingEntity.CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) packingEntity.PackingId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) packingEntity.PackingName = drProduct.GetString(nameOrdinal);

                        packingList.Add(packingEntity);
                    }
                }

                return packingList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<ProductPackingEntity> GetAllProductPackings(ArgsEntity args)
        {
            DbDataReader drProduct = null;

            try
            {
                List<ProductPackingEntity> productPackingList = new List<ProductPackingEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", string.IsNullOrEmpty(args.AdditionalParams) ? "" : args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProduct = this.DataAcessService.ExecuteQuery(PackingSql["GetAllProductPackings"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int idOrdinal = drProduct.GetOrdinal("product_packing_id");
                    int nameOrdinal = drProduct.GetOrdinal("packing_name");
                    int totalCountOrdinal = drProduct.GetOrdinal("total_count");

                    while (drProduct.Read())
                    {
                        ProductPackingEntity productPackingEntity = ProductPackingEntity.CreateObject();

                        if (!drProduct.IsDBNull(idOrdinal)) productPackingEntity.PackingId = drProduct.GetInt32(idOrdinal);
                        if (!drProduct.IsDBNull(nameOrdinal)) productPackingEntity.PackingName = drProduct.GetString(nameOrdinal);
                        if (!drProduct.IsDBNull(totalCountOrdinal)) productPackingEntity.TotalCount = drProduct.GetInt32(totalCountOrdinal);

                        productPackingList.Add(productPackingEntity);
                    }
                }

                return productPackingList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public bool IsProductPackingNameExists(ProductPackingEntity packingEntity)
        {
            DbDataReader drPacking = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@PackingName", packingEntity.PackingName, DbType.String);

                drPacking = this.DataAcessService.ExecuteQuery(PackingSql["ProductPackingNameExists"], paramCollection);

                if (drPacking != null && drPacking.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drPacking != null)
                    drPacking.Close();
            }
        }

        public bool UpdateProductPacking(ProductPackingEntity packingEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drPacking = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@PackingId", packingEntity.PackingId, DbType.Int32);
                paramCollection.Add("@PackingName", packingEntity.PackingName, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PackingSql["UpdateProductPacking"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drPacking != null)
                    drPacking.Close();
            }
        }

        public bool InsertProductPacking(ProductPackingEntity packingEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drPacking = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@PackingName", packingEntity.PackingName, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PackingSql["InsertProductPacking"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drPacking != null)
                    drPacking.Close();
            }
        }

        public bool DeletePacking(int packingId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drPacking = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@PackingId", packingId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PackingSql["DeletePacking"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drPacking != null)
                    drPacking.Close();
            }
        }

        public bool UpdateCheckListCustomer(int CheckListId, string CustomerCode, bool HasSelect)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drPacking = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CheckListId", CheckListId, DbType.Int32);
                paramCollection.Add("@CustomerCode", CustomerCode, DbType.String);
                paramCollection.Add("@HasSelect", HasSelect, DbType.Boolean);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PackingSql["UpdateCheckListCustomer"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drPacking != null)
                    drPacking.Close();
            }
        }

        public bool UpdateCheckListRoute(int CheckListId, int RouteId, bool HasSelect)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drPacking = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CheckListId", CheckListId, DbType.Int32);
                paramCollection.Add("@RouteId", RouteId, DbType.Int32);
                paramCollection.Add("@HasSelect", HasSelect, DbType.Boolean);

                iNoRec = this.DataAcessService.ExecuteNonQuery(PackingSql["UpdateCheckListRoute"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drPacking != null)
                    drPacking.Close();
            }
        }
    }
}
