﻿using System;
using System.Collections.Generic;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.Shared;

namespace Peercore.CRM.DataAccess.DAO
{
    public class PromotionDAO:BaseDAO
    {
        private DbSqlAdapter Commonsql { get; set; }

        private void RegisterSql()
        {
            this.Commonsql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public PromotionDAO()
        {
            RegisterSql();
        }

        public PromotionEntity CreateObject()
        {
            return PromotionEntity.CreateObject();
        }

        public PromotionEntity CreateObject(int entityId)
        {
            return PromotionEntity.CreateObject(entityId);
        }

        public List<PromotionEntity> GetAllPromotions()
        {
            List<PromotionEntity> promotionlist = new List<PromotionEntity>();
            DbDataReader dataReader = null;

            dataReader = this.DataAcessService.ExecuteQuery(Commonsql["GetAllPromotions"]);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int promoidOrdinal = dataReader.GetOrdinal("promo_id");
                    int nameOrdinal = dataReader.GetOrdinal("name");

                    while (dataReader.Read())
                    {
                        PromotionEntity promotion = CreateObject();
                        if (!dataReader.IsDBNull(promoidOrdinal)) promotion.PromotionId = dataReader.GetInt32(promoidOrdinal);
                        if (!dataReader.IsDBNull(nameOrdinal)) promotion.PromotionName = dataReader.GetString(nameOrdinal);

                        promotionlist.Add(promotion);
                    }
                }
                return promotionlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

    }
}
