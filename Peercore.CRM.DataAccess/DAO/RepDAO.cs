﻿using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class RepDAO : BaseDAO
    {
        public RepDAO()
        {
           
        }

        public List<RegionModel> GetAllRegions(ArgsModel args)
        {
            RegionModel regionEntity = null;
            DataTable objDS = new DataTable();
            List<RegionModel> RegionsList = new List<RegionModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRegions";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            regionEntity = new RegionModel();
                            if (item["region_id"] != DBNull.Value) regionEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["comp_id"] != DBNull.Value) regionEntity.CompId = Convert.ToInt32(item["comp_id"]);
                            if (item["region_code"] != DBNull.Value) regionEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) regionEntity.RegionName = item["region_name"].ToString();
                            if (item["total_count"] != DBNull.Value) regionEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            RegionsList.Add(regionEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return RegionsList;
        } 
        
        public List<SRModel> GetAllSalesRepsByTerritoryId(ArgsModel args, string TerritoryId)
        {
            SRModel objEntity = null;
            DataTable objDS = new DataTable();
            List<SRModel> objList = new List<SRModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesRepsByTerritoryId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@TerritoryId", TerritoryId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SRModel();
                            if (item["rep_id"] != DBNull.Value) objEntity.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objEntity.TerritoryStatus = item["territory_status"].ToString();
                            if (item["rep_code"] != DBNull.Value) objEntity.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objEntity.RepName = item["rep_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        } 
        
        public List<SRModel> GetAllSalesRepsByAreaId(ArgsModel args, string AreaId)
        {
            SRModel objEntity = null;
            DataTable objDS = new DataTable();
            List<SRModel> objList = new List<SRModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesRepsByAreaId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@AreaId", AreaId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SRModel();
                            if (item["rep_id"] != DBNull.Value) objEntity.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objEntity.TerritoryStatus = item["territory_status"].ToString();
                            if (item["territory_code"] != DBNull.Value) objEntity.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objEntity.TerritoryName = item["territory_name"].ToString();
                            if (item["rep_code"] != DBNull.Value) objEntity.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objEntity.RepName = item["rep_name"].ToString();
                            if (item["user_name"] != DBNull.Value) objEntity.UserName = item["user_name"].ToString();
                            if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }
        
        public List<SRModel> GetAllSalesReps(ArgsModel args)
        {
            SRModel objEntity = null;
            DataTable objDS = new DataTable();
            List<SRModel> objList = new List<SRModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesReps";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SRModel();
                            if (item["rep_id"] != DBNull.Value) objEntity.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objEntity.TerritoryStatus = item["territory_status"].ToString();
                            if (item["territory_code"] != DBNull.Value) objEntity.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objEntity.TerritoryName = item["territory_name"].ToString();
                            if (item["rep_code"] != DBNull.Value) objEntity.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objEntity.RepName = item["rep_name"].ToString();
                            if (item["user_name"] != DBNull.Value) objEntity.UserName = item["user_name"].ToString();
                            if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        } 
        
        public List<SRModel> GetAllSRTerritoryByOriginator(ArgsModel args, string OriginatorType, string Originator)
        {
            SRModel objEntity = null;
            DataTable objDS = new DataTable();
            List<SRModel> objList = new List<SRModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSRTerritoryByOriginator";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SRModel();
                            if (item["rep_id"] != DBNull.Value) objEntity.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objEntity.TerritoryStatus = item["territory_status"].ToString();
                            if (item["territory_code"] != DBNull.Value) objEntity.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objEntity.TerritoryName = item["territory_name"].ToString();
                            if (item["rep_code"] != DBNull.Value) objEntity.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objEntity.RepName = item["rep_name"].ToString();
                            if (item["user_name"] != DBNull.Value) objEntity.UserName = item["user_name"].ToString();
                            if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                            if (item["status"] != DBNull.Value) objEntity.Status = item["status"].ToString();
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        } 
        
        public List<SRModel> GetAllSalesRepsByOriginator(ArgsModel args, string Originator)
        {
            SRModel objEntity = null;
            DataTable objDS = new DataTable();
            List<SRModel> objList = new List<SRModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesRepsByOriginator";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SRModel();
                            if (item["rep_id"] != DBNull.Value) objEntity.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objEntity.TerritoryStatus = item["territory_status"].ToString();
                            if (item["territory_code"] != DBNull.Value) objEntity.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objEntity.TerritoryName = item["territory_name"].ToString();
                            if (item["rep_code"] != DBNull.Value) objEntity.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objEntity.RepName = item["rep_name"].ToString();
                            if (item["user_name"] != DBNull.Value) objEntity.UserName = item["user_name"].ToString();
                            if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            List<string> hList = GetSRHoliday(Originator);

                            //if (item["total_count"] != DBNull.Value) objEntity.HolidaysList = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<string> GetSRHoliday(string originator)
        {
            DataTable objDS = new DataTable();
            List<string> holidayList = new List<string>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetDistributorHoliday";

                    objCommand.Parameters.AddWithValue("@originator", originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        string holiday = objDS.Rows[0]["holiday"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return holidayList;
        }

        public List<SRModel> GetAllSalesRepsForRouteAssign(ArgsModel args)
        {
            SRModel objEntity = null;
            DataTable objDS = new DataTable();
            List<SRModel> objList = new List<SRModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesRepsForRouteAssign";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SRModel();
                            if (item["rep_id"] != DBNull.Value) objEntity.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objEntity.TerritoryStatus = item["territory_status"].ToString();
                            if (item["territory_code"] != DBNull.Value) objEntity.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objEntity.TerritoryName = item["territory_name"].ToString();
                            if (item["rep_code"] != DBNull.Value) objEntity.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objEntity.RepName = item["rep_name"].ToString();
                            if (item["user_name"] != DBNull.Value) objEntity.UserName = item["user_name"].ToString();
                            if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        } 
        
        public SRModel GetSRDetailsByOriginator(string originator)
        {
            SRModel objEntity = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetSRDetailsByOriginator";

                    objCommand.Parameters.AddWithValue("@Originator", originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SRModel();
                            if (item["originator"] != DBNull.Value) objEntity.Originator = item["originator"].ToString();
                            if (item["rep_id"] != DBNull.Value) objEntity.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["rep_code"] != DBNull.Value) objEntity.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objEntity.RepName = item["rep_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objEntity.TerritoryStatus = item["territory_status"].ToString();
                            if (item["territory_code"] != DBNull.Value) objEntity.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objEntity.TerritoryName = item["territory_name"].ToString();
                            if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                            if (item["rep_email"] != DBNull.Value) objEntity.Email = item["rep_email"].ToString();
                            if (item["rep_address"] != DBNull.Value) objEntity.Address1 = item["rep_address"].ToString();
                            if (item["group_code"] != DBNull.Value) objEntity.GroupCode = item["group_code"].ToString();
                            if (item["join_date"] != DBNull.Value) objEntity.JoinedDate = Convert.ToDateTime(item["join_date"].ToString());
                            if (item["status"] != DBNull.Value) objEntity.Status = item["status"].ToString();
                            if (item["IsNotEMEIUser"] != DBNull.Value) objEntity.IsNotEMEIUser = Convert.ToBoolean(item["IsNotEMEIUser"].ToString());
                           // if (item["password"] != DBNull.Value) objEntity.Password = item["password"].ToString();

                            List<string> hList = GetSRHoliday(originator);
                            objEntity.HolidaysList = hList;
                            // if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objEntity;
        }
        
        public List<SRModel> GetAllSRDetails()
        {
            List<SRModel> objList = new List<SRModel>();
            SRModel objEntity = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSRDetails";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SRModel();
                            if (item["originator"] != DBNull.Value) objEntity.Originator = item["originator"].ToString();
                            if (item["rep_id"] != DBNull.Value) objEntity.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["rep_code"] != DBNull.Value) objEntity.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objEntity.RepName = item["rep_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objEntity.TerritoryStatus = item["territory_status"].ToString();
                            if (item["territory_code"] != DBNull.Value) objEntity.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objEntity.TerritoryName = item["territory_name"].ToString();
                            if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                            if (item["rep_email"] != DBNull.Value) objEntity.Email = item["rep_email"].ToString();
                            if (item["rep_address"] != DBNull.Value) objEntity.Address1 = item["rep_address"].ToString();
                            if (item["group_code"] != DBNull.Value) objEntity.GroupCode = item["group_code"].ToString();
                            if (item["join_date"] != DBNull.Value) objEntity.JoinedDate = Convert.ToDateTime(item["join_date"].ToString());
                            if (item["status"] != DBNull.Value) objEntity.Status = item["status"].ToString();
                            if (item["IsNotEMEIUser"] != DBNull.Value) objEntity.IsNotEMEIUser = Convert.ToBoolean(item["IsNotEMEIUser"].ToString());
                            // if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public bool SaveSR(SRModel srEntity)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertSalesRep";

                    objCommand.Parameters.AddWithValue("@RepId", srEntity.RepId);
                    objCommand.Parameters.AddWithValue("@originator", srEntity.Originator);
                    objCommand.Parameters.AddWithValue("@prefixCode", srEntity.PrefixCode);
                    objCommand.Parameters.AddWithValue("@name", srEntity.RepName);
                    objCommand.Parameters.AddWithValue("@password", srEntity.Password);
                    objCommand.Parameters.AddWithValue("@deptString", srEntity.DeptString);
                    objCommand.Parameters.AddWithValue("@Address", srEntity.Address1);
                    objCommand.Parameters.AddWithValue("@Email", srEntity.Email);
                    objCommand.Parameters.AddWithValue("@Mobile", srEntity.Mobile);
                    objCommand.Parameters.AddWithValue("@LastModifiedBy", srEntity.LastModifiedBy);
                    objCommand.Parameters.AddWithValue("@GroupCode", srEntity.GroupCode);
                    objCommand.Parameters.AddWithValue("@TerritoryId", srEntity.TerritoryId);
                    objCommand.Parameters.AddWithValue("@JoinedDate", srEntity.JoinedDate);
                    objCommand.Parameters.AddWithValue("@RepCode", srEntity.RepCode);
                    objCommand.Parameters.AddWithValue("@status", srEntity.Status);
                    objCommand.Parameters.AddWithValue("@IsNotEMEIUser", srEntity.IsNotEMEIUser);
                    objCommand.Parameters.AddWithValue("@CreatedBy", srEntity.CreatedBy);
                    objCommand.Parameters.AddWithValue("@NIC", srEntity.NIC);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateSR(SRModel srEntity)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateSalesRep";

                    objCommand.Parameters.AddWithValue("@RepId", srEntity.RepId);
                    objCommand.Parameters.AddWithValue("@originator", srEntity.Originator);
                    objCommand.Parameters.AddWithValue("@prefixCode", srEntity.PrefixCode);
                    objCommand.Parameters.AddWithValue("@name", srEntity.RepName);
                    objCommand.Parameters.AddWithValue("@password", srEntity.Password);
                    objCommand.Parameters.AddWithValue("@deptString", srEntity.DeptString);
                    objCommand.Parameters.AddWithValue("@Address", srEntity.Address1);
                    objCommand.Parameters.AddWithValue("@Email", srEntity.Email);
                    objCommand.Parameters.AddWithValue("@Mobile", srEntity.Mobile);
                    objCommand.Parameters.AddWithValue("@LastModifiedBy", srEntity.LastModifiedBy);
                    objCommand.Parameters.AddWithValue("@GroupCode", srEntity.GroupCode);
                    objCommand.Parameters.AddWithValue("@TerritoryId", srEntity.TerritoryId);
                    objCommand.Parameters.AddWithValue("@JoinedDate", srEntity.JoinedDate);
                    objCommand.Parameters.AddWithValue("@RepCode", srEntity.RepCode);
                    objCommand.Parameters.AddWithValue("@status", srEntity.Status);
                    objCommand.Parameters.AddWithValue("@IsNotEMEIUser", srEntity.IsNotEMEIUser);
                    objCommand.Parameters.AddWithValue("@CreatedBy", srEntity.CreatedBy);
                    objCommand.Parameters.AddWithValue("@NIC", srEntity.NIC);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }


        public List<SalesRepSalesTypeModel> GetAllSalesRepWithSalesType(ArgsModel args, string OriginatorType, string Originator)
        {
            SalesRepSalesTypeModel objModel = null;
            DataTable objDS = new DataTable();
            List<SalesRepSalesTypeModel> objList = new List<SalesRepSalesTypeModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesRepWithSalesType";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new SalesRepSalesTypeModel();

                            if (item["rep_id"] != DBNull.Value) objModel.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["rep_code"] != DBNull.Value) objModel.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objModel.RepName = item["rep_name"].ToString();
                            if (item["db_code"] != DBNull.Value) objModel.DistributerCode = item["db_name"].ToString();
                            if (item["db_name"] != DBNull.Value) objModel.DistributerName = item["db_name"].ToString();
                            if (item["asm_name"] != DBNull.Value) objModel.AsmName = item["asm_name"].ToString();
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["sales_type"] != DBNull.Value) objModel.SalesType = Convert.ToInt32(item["sales_type"]);
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objList = new List<SalesRepSalesTypeModel>();
            }

            return objList;
        }
        
        public List<SalesRepRouteSequenceModel> GetAllSRRouteLocations(string selectRep, string selectDate)
        {
            SalesRepRouteSequenceModel objModel = null;
            DataTable objDS = new DataTable();
            List<SalesRepRouteSequenceModel> objList = new List<SalesRepRouteSequenceModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSRRouteLocations";

                    objCommand.Parameters.AddWithValue("@RepCode", selectRep);
                    objCommand.Parameters.AddWithValue("@SelectDate", selectDate);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        int i = 1;
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new SalesRepRouteSequenceModel();
                            objModel.SequenceId = i;
                            if (item["repCode"] != DBNull.Value) objModel.repCode = item["repCode"].ToString();
                            if (item["invNo"] != DBNull.Value) objModel.invNo = item["invNo"].ToString();
                            if (item["invTotal"] != DBNull.Value) objModel.invTotal = Convert.ToDouble(item["invTotal"]);
                            if (item["custCode"] != DBNull.Value) objModel.custCode = item["custCode"].ToString();
                            if (item["invLatitude"] != DBNull.Value) objModel.invLatitude = item["invLatitude"].ToString();
                            if (item["invLongitude"] != DBNull.Value) objModel.invLongitude = item["invLongitude"].ToString();
                            if (item["invType"] != DBNull.Value) objModel.invType = item["invType"].ToString();
                            if (item["custName"] != DBNull.Value) objModel.custName = item["custName"].ToString();
                            if (item["custTel"] != DBNull.Value) objModel.custTel = item["custTel"].ToString();
                            if (item["custAddress"] != DBNull.Value) objModel.custAddress = item["custAddress"].ToString();
                            if (item["custEmail"] != DBNull.Value) objModel.custEmail = item["custEmail"].ToString();
                            if (item["custLatitude"] != DBNull.Value) objModel.custLatitude = item["custLatitude"].ToString();
                            if (item["custLongitude"] != DBNull.Value) objModel.custLongitude = item["custLongitude"].ToString();
                            objList.Add(objModel);

                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objList = new List<SalesRepRouteSequenceModel>();
            }

            return objList;
        }

        public bool UpdateSRSalesType(DbWorkflowScope transactionScope, string RepId, string SalesType, string Originator)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateSRSalesType";

                    objCommand.Parameters.AddWithValue("@RepId", RepId);
                    objCommand.Parameters.AddWithValue("@SalesType", SalesType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                retStatus = false;
            }

            return retStatus;
        }

        public List<SalesRepSyncModel> GetAllSalesRepSyncData(ArgsModel args, string OriginatorType, string Originator)
        {
            SalesRepSyncModel objModel = null;
            DataTable objDS = new DataTable();
            List<SalesRepSyncModel> objList = new List<SalesRepSyncModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesRepSyncData";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new SalesRepSyncModel();

                            objModel.IsRepSync = false;
                            if (item["rep_id"] != DBNull.Value) objModel.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["rep_code"] != DBNull.Value) objModel.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objModel.RepName = item["rep_name"].ToString();
                            if (item["emei_no"] != DBNull.Value) objModel.EmeiNo = item["emei_no"].ToString();
                            if (item["device_token"] != DBNull.Value) objModel.DeviceToken = item["device_token"].ToString();
                            if (item["asm"] != DBNull.Value) objModel.AsmName = item["asm"].ToString();
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["delivery_status"] != DBNull.Value) objModel.DeliveryStatus = (item["delivery_status"].ToString() == "A") ? true : false;
                            if (item["is_synced"] != DBNull.Value) objModel.IsSynced = Convert.ToBoolean(item["is_synced"]);
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objList = new List<SalesRepSyncModel>();
            }

            return objList;
        }











        public bool DeleteRegion(string region_id)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteRegionByRegionId";

                    objCommand.Parameters.AddWithValue("@RegionId", region_id);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        

        //#region "RSM"
        //public List<RsmModel> GetAllRsm(ArgsModel args)
        //{
        //    RsmModel rsmEntity = null;
        //    DataTable objDS = new DataTable();
        //    List<RsmModel> RsmList = new List<RsmModel>();

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
        //        {
        //            SqlCommand objCommand = new SqlCommand();
        //            objCommand.Connection = conn;
        //            objCommand.CommandType = CommandType.StoredProcedure;
        //            objCommand.CommandText = "GetAllRsm";

        //            objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
        //            objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
        //            objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
        //            objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
        //            objCommand.Parameters.AddWithValue("@ShowSQL", 0);

        //            SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
        //            objAdap.Fill(objDS);

        //            if (objDS.Rows.Count > 0)
        //            {
        //                foreach (DataRow item in objDS.Rows)
        //                {
        //                    rsmEntity = new RsmModel();
        //                    if (item["rsm_id"] != DBNull.Value) rsmEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
        //                    if (item["region_id"] != DBNull.Value) rsmEntity.RegionId = Convert.ToInt32(item["region_id"]);
        //                    if (item["region_code"] != DBNull.Value) rsmEntity.RegionCode = item["region_code"].ToString();
        //                    if (item["region_name"] != DBNull.Value) rsmEntity.RegionName = item["region_name"].ToString();
        //                    if (item["rsm_code"] != DBNull.Value) rsmEntity.RsmCode = item["rsm_code"].ToString();
        //                    if (item["rsm_name"] != DBNull.Value) rsmEntity.RsmName = item["rsm_name"].ToString();
        //                    if (item["rsm_user_name"] != DBNull.Value) rsmEntity.RsmUserName = item["rsm_user_name"].ToString();
        //                    //if (item["rsm_password"] != DBNull.Value) rsmEntity.RsmPassword = item["rsm_password"].ToString();
        //                    if (item["rsm_tel1"] != DBNull.Value) rsmEntity.RsmTel1 = item["rsm_tel1"].ToString();
        //                    if (item["rsm_tel2"] != DBNull.Value) rsmEntity.RsmTel2 = item["rsm_tel2"].ToString();
        //                    if (item["rsm_email"] != DBNull.Value) rsmEntity.RsmEmail = item["rsm_email"].ToString();
        //                    if (item["total_count"] != DBNull.Value) rsmEntity.TotalCount = Convert.ToInt32(item["total_count"]);

        //                    RsmList.Add(rsmEntity);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //    return RsmList;
        //}

        //public List<RsmModel> GetAllRsmByRsmCode(ArgsModel args, string RsmCode)
        //{
        //    RsmModel rsmEntity = null;
        //    DataTable objDS = new DataTable();
        //    List<RsmModel> RsmList = new List<RsmModel>();

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
        //        {
        //            SqlCommand objCommand = new SqlCommand();
        //            objCommand.Connection = conn;
        //            objCommand.CommandType = CommandType.StoredProcedure;
        //            objCommand.CommandText = "GetAllRsmByRsmCode";

        //            objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
        //            objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
        //            objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
        //            objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
        //            objCommand.Parameters.AddWithValue("@ShowSQL", 0);
        //            objCommand.Parameters.AddWithValue("@RsmCode", RsmCode);

        //            SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
        //            objAdap.Fill(objDS);

        //            if (objDS.Rows.Count > 0)
        //            {
        //                foreach (DataRow item in objDS.Rows)
        //                {
        //                    rsmEntity = new RsmModel();
        //                    if (item["rsm_id"] != DBNull.Value) rsmEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
        //                    if (item["region_id"] != DBNull.Value) rsmEntity.RegionId = Convert.ToInt32(item["region_id"]);
        //                    if (item["region_code"] != DBNull.Value) rsmEntity.RegionCode = item["region_code"].ToString();
        //                    if (item["region_name"] != DBNull.Value) rsmEntity.RegionName = item["region_name"].ToString();
        //                    if (item["rsm_code"] != DBNull.Value) rsmEntity.RsmCode = item["rsm_code"].ToString();
        //                    if (item["rsm_name"] != DBNull.Value) rsmEntity.RsmName = item["rsm_name"].ToString();
        //                    if (item["rsm_user_name"] != DBNull.Value) rsmEntity.RsmUserName = item["rsm_user_name"].ToString();
        //                    if (item["rsm_password"] != DBNull.Value) rsmEntity.RsmPassword = item["rsm_password"].ToString();
        //                    if (item["rsm_tel1"] != DBNull.Value) rsmEntity.RsmTel1 = item["rsm_tel1"].ToString();
        //                    if (item["rsm_tel2"] != DBNull.Value) rsmEntity.RsmTel2 = item["rsm_tel2"].ToString();
        //                    if (item["rsm_email"] != DBNull.Value) rsmEntity.RsmEmail = item["rsm_email"].ToString();
        //                    if (item["total_count"] != DBNull.Value) rsmEntity.TotalCount = Convert.ToInt32(item["total_count"]);

        //                    RsmList.Add(rsmEntity);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //    return RsmList;
        //}
        //public List<RsmModel> GetRsmDetailsByRegionId(ArgsModel args, string RegionId)
        //{
        //    RsmModel rsmEntity = null;
        //    DataTable objDS = new DataTable();
        //    List<RsmModel> RsmList = new List<RsmModel>();

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
        //        {
        //            SqlCommand objCommand = new SqlCommand();
        //            objCommand.Connection = conn;
        //            objCommand.CommandType = CommandType.StoredProcedure;
        //            objCommand.CommandText = "GetRsmDetailsByRegionId";

        //            objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
        //            objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
        //            objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
        //            objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
        //            objCommand.Parameters.AddWithValue("@ShowSQL", 0);
        //            objCommand.Parameters.AddWithValue("@RegionId", RegionId);

        //            SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
        //            objAdap.Fill(objDS);

        //            if (objDS.Rows.Count > 0)
        //            {
        //                foreach (DataRow item in objDS.Rows)
        //                {
        //                    rsmEntity = new RsmModel();
        //                    if (item["rsm_id"] != DBNull.Value) rsmEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
        //                    if (item["region_id"] != DBNull.Value) rsmEntity.RegionId = Convert.ToInt32(item["region_id"]);
        //                    if (item["region_code"] != DBNull.Value) rsmEntity.RegionCode = item["region_code"].ToString();
        //                    if (item["region_name"] != DBNull.Value) rsmEntity.RegionName = item["region_name"].ToString();
        //                    if (item["rsm_code"] != DBNull.Value) rsmEntity.RsmCode = item["rsm_code"].ToString();
        //                    if (item["rsm_name"] != DBNull.Value) rsmEntity.RsmName = item["rsm_name"].ToString();
        //                    if (item["rsm_user_name"] != DBNull.Value) rsmEntity.RsmUserName = item["rsm_user_name"].ToString();
        //                    //if (item["rsm_password"] != DBNull.Value) rsmEntity.RsmPassword = item["rsm_password"].ToString();
        //                    if (item["rsm_tel1"] != DBNull.Value) rsmEntity.RsmTel1 = item["rsm_tel1"].ToString();
        //                    if (item["rsm_tel2"] != DBNull.Value) rsmEntity.RsmTel2 = item["rsm_tel2"].ToString();
        //                    if (item["rsm_email"] != DBNull.Value) rsmEntity.RsmEmail = item["rsm_email"].ToString();
        //                    if (item["total_count"] != DBNull.Value) rsmEntity.TotalCount = Convert.ToInt32(item["total_count"]);

        //                    RsmList.Add(rsmEntity);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //    return RsmList;
        //}

        //public bool DeleteRsm(string rsm_id)
        //{
        //    bool retStatus = false;

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
        //        {
        //            SqlCommand objCommand = new SqlCommand();
        //            objCommand.Connection = conn;
        //            objCommand.CommandType = CommandType.StoredProcedure;
        //            objCommand.CommandText = "DeleteRsmByRsmId";

        //            objCommand.Parameters.AddWithValue("@RsmId", rsm_id);

        //            if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
        //            if (objCommand.ExecuteNonQuery() > 0)
        //            {
        //                retStatus = true;
        //            }
        //            else
        //            {
        //                retStatus = false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //    return retStatus;
        //}

        //public bool SaveRsm(string comp_id, string region_id, string region_code, string region_name)
        //{
        //    bool retStatus = false;

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
        //        {
        //            SqlCommand objCommand = new SqlCommand();
        //            objCommand.Connection = conn;
        //            objCommand.CommandType = CommandType.StoredProcedure;
        //            objCommand.CommandText = "InsertUpdateRegion";

        //            objCommand.Parameters.AddWithValue("@CompId", comp_id);
        //            objCommand.Parameters.AddWithValue("@RegionId", region_id);
        //            objCommand.Parameters.AddWithValue("@RegionCode", region_code);
        //            objCommand.Parameters.AddWithValue("@RegionName", region_name);

        //            if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
        //            if (objCommand.ExecuteNonQuery() > 0)
        //            {
        //                retStatus = true;
        //            }
        //            else
        //            {
        //                retStatus = false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //    return retStatus;
        //}

        //#endregion
    }
}
