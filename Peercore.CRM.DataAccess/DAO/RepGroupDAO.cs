﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Collections.ObjectModel;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class RepGroupDAO:BaseDAO
    {
        private DbSqlAdapter RepGroupSql { get; set; }

        //public RepGroupDAO()
        //{
        //    this.RepGroupSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.RepGroupSql.xml");
        //}

        private void RegisterSql()
        {
            this.RepGroupSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.RepGroupSql.xml", ApplicationService.Instance.DbProvider);
        }

        public RepGroupDAO()
        {
            RegisterSql();
        }

        public RepGroupDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public RepGroupEntity CreateObject()
        {
            return RepGroupEntity.CreateObject();
        }

        public RepGroupEntity CreateObject(int entityId)
        {
            return RepGroupEntity.CreateObject(entityId);
        }

        public List<RepGroupEntity> GetAllRepGroups()
        {
            RepGroupEntity repGroupEntity = null;
            DbDataReader idrRepGroups = null;

            try
            {
                List<RepGroupEntity> repGroupList = new List<RepGroupEntity>();
                
                idrRepGroups = this.DataAcessService.ExecuteQuery(RepGroupSql["GetAllRepGroup"]);

                if (idrRepGroups != null && idrRepGroups.HasRows)
                {
                    while (idrRepGroups.Read())
                    {
                        repGroupEntity = RepGroupEntity.CreateObject();
                        if (!idrRepGroups.IsDBNull(idrRepGroups.GetOrdinal("rep_group_id"))) repGroupEntity.RepGroupId = idrRepGroups.GetInt32(idrRepGroups.GetOrdinal("rep_group_id"));
                        if (!idrRepGroups.IsDBNull(idrRepGroups.GetOrdinal("rep_group_name"))) repGroupEntity.RepGroupName = idrRepGroups.GetString(idrRepGroups.GetOrdinal("rep_group_name"));
                        repGroupList.Add(repGroupEntity);
                    }
                }

                return repGroupList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrRepGroups != null)
                    idrRepGroups.Close();
            }
        }

        public int GetNextRepGroupId()
        {
            try
            {
                return System.Convert.ToInt32(this.DataAcessService.GetOneValue(RepGroupSql["GetNextRepGroupId"]));    
            }
            catch
            {
                throw;
            }
        }

        public bool InsertRepGroup(RepGroupEntity repGroupEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@repGroupID", repGroupEntity.RepGroupId, DbType.Int32);
                paramCollection.Add("@repGroupName", repGroupEntity.RepGroupName, DbType.String);
               
                iNoRec = this.DataAcessService.ExecuteNonQuery(RepGroupSql["InsertRepGroup"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateRepGroup(RepGroupEntity repGroupEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@repGroupID", repGroupEntity.RepGroupId, DbType.Int32);
                paramCollection.Add("@repGroupName", repGroupEntity.RepGroupName, DbType.String);

                
                iNoRec = this.DataAcessService.ExecuteNonQuery(RepGroupSql["UpdateRepGroup"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool InsertGroupRep(int? repGroupId, string Originator)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
              
                paramCollection.Add("@repGroupID", repGroupId, DbType.Int32);
                paramCollection.Add("@originator", Originator, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RepGroupSql["InsertGroupRep"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteGroupRep(int? repGroupId, string Originator)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {                
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@repGroupID", repGroupId, DbType.Int32);
                paramCollection.Add("@originator", Originator, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RepGroupSql["DeleteGroupRep"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteRepGroup(int repGroupId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {               
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@repGroupID", repGroupId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RepGroupSql["DeleteRepGroup"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteGroupRepById(int repGroupId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@repGroupID", repGroupId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RepGroupSql["DeleteGroupRepById"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool CheckExistsToDeleteGroups(int? GroupId)
        {
            if (GroupId.HasValue == false)
                return false;
            DbDataReader drDetail = null;
            bool isExists = false;
           
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@repGroupID", GroupId, DbType.Int32);
           
            try
            {
                drDetail = this.DataAcessService.ExecuteQuery(RepGroupSql["GetLeadByRepGroup"], paramCollection);                
                if (drDetail.HasRows)
                {
                    drDetail.Close();
                    isExists = true;
                }
                else
                    drDetail.Close();
                drDetail = this.DataAcessService.ExecuteQuery(RepGroupSql["GetActivityByRepGroup"], paramCollection);    
                if (drDetail.HasRows)
                {
                    drDetail.Close();
                    isExists = true;
                }
                else
                    drDetail.Close();
                drDetail = this.DataAcessService.ExecuteQuery(RepGroupSql["GetActivityByRepGroup"], paramCollection);    
                if (drDetail.HasRows)
                {
                    drDetail.Close();
                    isExists = true;
                }
                else
                    drDetail.Close();
                return isExists;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drDetail != null)
                    drDetail.Close();
            }
        }
    }
}
