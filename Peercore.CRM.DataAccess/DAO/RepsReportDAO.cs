﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class RepsReportDAO : BaseDAO
    {
        private DbSqlAdapter RepReportsSql { get; set; }

        private void RegisterSql()
        {
            this.RepReportsSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.RepReportsSql.xml", ApplicationService.Instance.DbProvider);
        }

        public RepsReportDAO()
        {
            RegisterSql();
        }

        public RepsReportDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public RepsOutletWiseDetailsEntity CreateObject()
        {
            return RepsOutletWiseDetailsEntity.CreateObject();
        }

        public List<RepsOutletWiseDetailsEntity> GetAllRepsOutletWiseDetails(string AccessToken, string date)
        {
            DbDataReader drSchemeHeader = null;
            try
            {
                List<RepsOutletWiseDetailsEntity> detCollection = new List<RepsOutletWiseDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", AccessToken, DbType.String);
                paramCollection.Add("@date", date, DbType.String);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(RepReportsSql["GetAllRepsOutletWiseDetails"], paramCollection);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int cust_codeOrdinal = drSchemeHeader.GetOrdinal("cust_code");
                    int cust_nameOrdinal = drSchemeHeader.GetOrdinal("cust_name");
                    //int dateOrdinal = drSchemeHeader.GetOrdinal("date");
                    //int rep_codeOrdinal = drSchemeHeader.GetOrdinal("rep_code");
                    int count_creditOrdinal = drSchemeHeader.GetOrdinal("count_credit");
                    int count_chequeOrdinal = drSchemeHeader.GetOrdinal("count_cheque");
                    int count_cashOrdinal = drSchemeHeader.GetOrdinal("count_cash");

                    while (drSchemeHeader.Read())
                    {
                        RepsOutletWiseDetailsEntity schemeHeader = RepsOutletWiseDetailsEntity.CreateObject();
                        if (!drSchemeHeader.IsDBNull(cust_codeOrdinal)) schemeHeader.CustomerCode = drSchemeHeader.GetString(cust_codeOrdinal);
                        if (!drSchemeHeader.IsDBNull(cust_nameOrdinal)) schemeHeader.CustomerCode = drSchemeHeader.GetString(cust_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(count_creditOrdinal)) schemeHeader.PaymentTypeCredit = drSchemeHeader.GetInt32(count_creditOrdinal);
                        if (!drSchemeHeader.IsDBNull(count_cashOrdinal)) schemeHeader.PaymentTypeCash = drSchemeHeader.GetInt32(count_cashOrdinal);
                        if (!drSchemeHeader.IsDBNull(count_chequeOrdinal)) schemeHeader.PaymentTypeCheque = drSchemeHeader.GetInt32(count_chequeOrdinal);
                        detCollection.Add(schemeHeader);
                    }
                }

                return detCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        //public List<RepsOutletWiseDetailsEntity> GetAllRepsOutletWiseDetails(string cust_code, string date)
        //{
        //    DbDataReader drSchemeHeader = null;
        //    try
        //    {
        //        List<RepsOutletWiseDetailsEntity> detCollection = new List<RepsOutletWiseDetailsEntity>();

        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@AccessToken", AccessToken, DbType.String);
        //        paramCollection.Add("@date", date, DbType.String);

        //        drSchemeHeader = this.DataAcessService.ExecuteQuery(RepReportsSql["GetAllRepsOutletWiseDetails"], paramCollection);

        //        if (drSchemeHeader != null && drSchemeHeader.HasRows)
        //        {
        //            int cust_codeOrdinal = drSchemeHeader.GetOrdinal("cust_code");
        //            int cust_nameOrdinal = drSchemeHeader.GetOrdinal("cust_name");
        //            //int dateOrdinal = drSchemeHeader.GetOrdinal("date");
        //            //int rep_codeOrdinal = drSchemeHeader.GetOrdinal("rep_code");
        //            int count_creditOrdinal = drSchemeHeader.GetOrdinal("count_credit");
        //            int count_chequeOrdinal = drSchemeHeader.GetOrdinal("count_cheque");
        //            int count_cashOrdinal = drSchemeHeader.GetOrdinal("count_cash");

        //            while (drSchemeHeader.Read())
        //            {
        //                RepsOutletWiseDetailsEntity schemeHeader = RepsOutletWiseDetailsEntity.CreateObject();

        //                if (!drSchemeHeader.IsDBNull(cust_codeOrdinal)) schemeHeader.CustomerCode = drSchemeHeader.GetString(cust_codeOrdinal);
        //                if (!drSchemeHeader.IsDBNull(cust_nameOrdinal)) schemeHeader.CustomerCode = drSchemeHeader.GetString(cust_nameOrdinal);
        //                if (!drSchemeHeader.IsDBNull(count_creditOrdinal)) schemeHeader.PaymentTypeCredit = drSchemeHeader.GetInt32(count_creditOrdinal);
        //                if (!drSchemeHeader.IsDBNull(count_cashOrdinal)) schemeHeader.PaymentTypeCash = drSchemeHeader.GetInt32(count_cashOrdinal);
        //                if (!drSchemeHeader.IsDBNull(count_chequeOrdinal)) schemeHeader.PaymentTypeCheque = drSchemeHeader.GetInt32(count_chequeOrdinal);

        //                detCollection.Add(schemeHeader);

        //            }
        //        }

        //        return detCollection;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (drSchemeHeader != null)
        //            drSchemeHeader.Close();
        //    }
        //}
    }
}
