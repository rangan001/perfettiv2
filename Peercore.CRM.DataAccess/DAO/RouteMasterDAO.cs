﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.Common;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class RouteMasterDAO : BaseDAO
    {
        private DbSqlAdapter RouteMasterSql { get; set; }
   //     private DbSqlAdapter CallCycleSql { get; set; }

        private void RegisterSql()
        {
            this.RouteMasterSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.RouteMasterSql.xml", ApplicationService.Instance.DbProvider);
        //    this.CallCycleSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CallCycleSql.xml", ApplicationService.Instance.DbProvider);
        }

        public RouteMasterDAO()
        {
            RegisterSql();
        }

        public RouteMasterDAO(DbWorkflowScope scope): base(scope)
        {
            RegisterSql();
        }

        public RouteMasterEntity CreateObject()
        {
            return RouteMasterEntity.CreateObject();
        }

        public RouteMasterEntity CreateObject(int entityId)
        {
            return RouteMasterEntity.CreateObject(entityId);
        }

        public bool InsertRouteMaster(RouteMasterEntity routeMasterEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            bool status = false;
             DbDataReader drRouteMaster = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@RouteName", routeMasterEntity.RouteMasterName, DbType.String);
                paramCollection.Add("@CreatedBy", routeMasterEntity.CreatedBy, DbType.String);
                paramCollection.Add("@RouteType", routeMasterEntity.RouteType, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RouteMasterSql["InsertRouteMaster"], paramCollection);

                if (drRouteMaster != null)
                    drRouteMaster.Close();

                if (routeMasterEntity.RouteType == "ASE") { 
                
                    DbInputParameterCollection paramCollection1 = new DbInputParameterCollection();
                    paramCollection1.Add("@RouteName", routeMasterEntity.RouteMasterName, DbType.String);
                    drRouteMaster = this.DataAcessService.ExecuteQuery(RouteMasterSql["GettRouteMasterIdByName"], paramCollection1);

                    if (drRouteMaster != null && drRouteMaster.HasRows)
                    {
                        int idOrdinal = drRouteMaster.GetOrdinal("id");

                        while (drRouteMaster.Read()) 
                        {
                            if (!drRouteMaster.IsDBNull(idOrdinal)) routeMasterEntity.RouteMasterId = drRouteMaster.GetInt32(idOrdinal);
                        }
                    }

                    //if (drRouteMaster != null)
                    //    drRouteMaster.Close();

                    //DbInputParameterCollection paramCollection2 = new DbInputParameterCollection();

                    //paramCollection.Add("@RouteMasterID", routeMasterEntity.RouteMasterId, DbType.Int32);
                    //paramCollection.Add("@Originator", routeMasterEntity.CreatedBy, DbType.String);
                    //paramCollection.Add("@OriginatorType", routeMasterEntity.RouteType, DbType.String);

                    //iNoRec = this.DataAcessService.ExecuteNonQuery(CallCycleSql["InsertRoutes"], paramCollection2);

                    CallCycleDAO callcycle = new CallCycleDAO();

                    status = callcycle.SaveRoute(routeMasterEntity);

                }

                if (iNoRec > 0 || status==true)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drRouteMaster != null)
                    drRouteMaster.Close();
            }
        }

        public bool UpdateRouteMaster(RouteMasterEntity routeMasterEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@id", routeMasterEntity.RouteMasterId, DbType.Int32);
                paramCollection.Add("@RouteName", routeMasterEntity.RouteMasterName, DbType.String);
                paramCollection.Add("@Status", routeMasterEntity.Status, DbType.String);
                paramCollection.Add("@LastModifiedBy", routeMasterEntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@RouteType", routeMasterEntity.RouteType, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RouteMasterSql["UpdateRouteMaster"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<RouteMasterEntity> GetAllRoutesMaster(string routeNameLike)
        {
            DbDataReader drRouteMaster = null;

            try
            {
                List<RouteMasterEntity> routeMasterList = new List<RouteMasterEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@RouteName", routeNameLike, DbType.String);

                drRouteMaster = this.DataAcessService.ExecuteQuery(RouteMasterSql["GetAllRoutesMaster"], paramCollection);

                if (drRouteMaster != null && drRouteMaster.HasRows)
                {
                    int idOrdinal = drRouteMaster.GetOrdinal("id");
                    int routeNameOrdinal = drRouteMaster.GetOrdinal("name");
                    int statusOrdinal = drRouteMaster.GetOrdinal("status");
                    int createdByOrdinal = drRouteMaster.GetOrdinal("created_by");
                    int createdDateOrdinal = drRouteMaster.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drRouteMaster.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drRouteMaster.GetOrdinal("last_modified_date");

                    while (drRouteMaster.Read())
                    {
                        RouteMasterEntity routeMasterEntity = CreateObject();

                        if (!drRouteMaster.IsDBNull(idOrdinal)) routeMasterEntity.RouteMasterId = drRouteMaster.GetInt32(idOrdinal);
                        if (!drRouteMaster.IsDBNull(routeNameOrdinal)) routeMasterEntity.RouteMasterName = drRouteMaster.GetString(routeNameOrdinal);
                        if (!drRouteMaster.IsDBNull(statusOrdinal)) routeMasterEntity.Status = drRouteMaster.GetString(statusOrdinal);                        
                        if (!drRouteMaster.IsDBNull(createdByOrdinal)) routeMasterEntity.CreatedBy = drRouteMaster.GetString(createdByOrdinal);
                        if (!drRouteMaster.IsDBNull(createdDateOrdinal)) routeMasterEntity.CreatedDate = drRouteMaster.GetDateTime(createdDateOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedByOrdinal)) routeMasterEntity.LastModifiedBy = drRouteMaster.GetString(lastModifiedByOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedDateOrdinal)) routeMasterEntity.LastModifiedDate = drRouteMaster.GetDateTime(lastModifiedDateOrdinal);

                        routeMasterList.Add(routeMasterEntity);
                    }
                }

                return routeMasterList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drRouteMaster != null)
                    drRouteMaster.Close();
            }
        }

        public List<RouteMasterEntity> GetAllRoutesMaster(string parentOriginator, ArgsEntity args)
        {
            DbDataReader drRouteMaster = null;

            try
            {
                List<RouteMasterEntity> routeMasterList = new List<RouteMasterEntity>();
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ParentOriginator", parentOriginator, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);
                paramCollection.Add("@RepCodes", args.ChildReps, DbType.String);

                drRouteMaster = this.DataAcessService.ExecuteQuery(RouteMasterSql["GetAllRoutesMasterData"], paramCollection);

                if (drRouteMaster != null && drRouteMaster.HasRows)
                {
                    int idOrdinal = drRouteMaster.GetOrdinal("id");
                    int routeNameOrdinal = drRouteMaster.GetOrdinal("name");
                    int statusOrdinal = drRouteMaster.GetOrdinal("status");
                    int createdByOrdinal = drRouteMaster.GetOrdinal("created_by");
                    int createdDateOrdinal = drRouteMaster.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drRouteMaster.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drRouteMaster.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drRouteMaster.GetOrdinal("total_count");
                    int repCodeOrdinal = drRouteMaster.GetOrdinal("rep_code");
                    int repNameOrdinal = drRouteMaster.GetOrdinal("rep_name");
                    int createdByNameOrdinal = drRouteMaster.GetOrdinal("created_by_name");
                    int callcycleIdOrdinal = drRouteMaster.GetOrdinal("callcycle_id");

                    while (drRouteMaster.Read())
                    {
                        RouteMasterEntity routeMasterEntity = CreateObject();
                        if (!drRouteMaster.IsDBNull(idOrdinal)) routeMasterEntity.RouteMasterId = drRouteMaster.GetInt32(idOrdinal);
                        if (!drRouteMaster.IsDBNull(routeNameOrdinal)) routeMasterEntity.RouteMasterName = drRouteMaster.GetString(routeNameOrdinal);
                        if (!drRouteMaster.IsDBNull(statusOrdinal)) routeMasterEntity.Status = drRouteMaster.GetString(statusOrdinal);
                        if (!drRouteMaster.IsDBNull(createdByOrdinal)) routeMasterEntity.CreatedBy = drRouteMaster.GetString(createdByOrdinal);
                        if (!drRouteMaster.IsDBNull(createdDateOrdinal)) routeMasterEntity.CreatedDate = drRouteMaster.GetDateTime(createdDateOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedByOrdinal)) routeMasterEntity.LastModifiedBy = drRouteMaster.GetString(lastModifiedByOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedDateOrdinal)) routeMasterEntity.LastModifiedDate = drRouteMaster.GetDateTime(lastModifiedDateOrdinal);
                        if (!drRouteMaster.IsDBNull(totalCountOrdinal)) routeMasterEntity.TotalCount = drRouteMaster.GetInt32(totalCountOrdinal);
                        if (!drRouteMaster.IsDBNull(repCodeOrdinal)) routeMasterEntity.RepCode = drRouteMaster.GetString(repCodeOrdinal);
                        if (!drRouteMaster.IsDBNull(repNameOrdinal)) routeMasterEntity.RepName = drRouteMaster.GetString(repNameOrdinal);
                        if (!drRouteMaster.IsDBNull(createdByNameOrdinal)) routeMasterEntity.CreatedByName = drRouteMaster.GetString(createdByNameOrdinal);
                        if (!drRouteMaster.IsDBNull(callcycleIdOrdinal)) routeMasterEntity.CallCycleId = drRouteMaster.GetInt32(callcycleIdOrdinal);
                        routeMasterList.Add(routeMasterEntity);
                    }
                }

                return routeMasterList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drRouteMaster != null)
                    drRouteMaster.Close();
            }
        }

        public List<RouteMasterEntity> GetAllRoutesMasterCheckList(int CheckListId, string parentOriginator, ArgsEntity args)
        {
            DbDataReader drRouteMaster = null;

            try
            {
                List<RouteMasterEntity> routeMasterList = new List<RouteMasterEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CheckListId", CheckListId, DbType.Int32);
                paramCollection.Add("@ParentOriginator", parentOriginator, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);
                paramCollection.Add("@RepCodes", args.ChildReps, DbType.String);


                drRouteMaster = this.DataAcessService.ExecuteQuery(RouteMasterSql["GetAllRoutesMasterDataForCheckList"], paramCollection);

                if (drRouteMaster != null && drRouteMaster.HasRows)
                {
                    int idOrdinal = drRouteMaster.GetOrdinal("id");
                    int routeNameOrdinal = drRouteMaster.GetOrdinal("name");
                    int statusOrdinal = drRouteMaster.GetOrdinal("status");
                    int createdByOrdinal = drRouteMaster.GetOrdinal("created_by");
                    int createdDateOrdinal = drRouteMaster.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drRouteMaster.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drRouteMaster.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drRouteMaster.GetOrdinal("total_count");

                    int repCodeOrdinal = drRouteMaster.GetOrdinal("rep_code");
                    int repNameOrdinal = drRouteMaster.GetOrdinal("rep_name");
                    int createdByNameOrdinal = drRouteMaster.GetOrdinal("created_by_name");
                    int callcycleIdOrdinal = drRouteMaster.GetOrdinal("callcycle_id");
                    int IsCheckListOrdinal = drRouteMaster.GetOrdinal("IsCheckList");

                    while (drRouteMaster.Read())
                    {
                        RouteMasterEntity routeMasterEntity = CreateObject();

                        if (!drRouteMaster.IsDBNull(idOrdinal)) routeMasterEntity.RouteMasterId = drRouteMaster.GetInt32(idOrdinal);
                        if (!drRouteMaster.IsDBNull(routeNameOrdinal)) routeMasterEntity.RouteMasterName = drRouteMaster.GetString(routeNameOrdinal);
                        if (!drRouteMaster.IsDBNull(statusOrdinal)) routeMasterEntity.Status = drRouteMaster.GetString(statusOrdinal);
                        if (!drRouteMaster.IsDBNull(createdByOrdinal)) routeMasterEntity.CreatedBy = drRouteMaster.GetString(createdByOrdinal);
                        if (!drRouteMaster.IsDBNull(createdDateOrdinal)) routeMasterEntity.CreatedDate = drRouteMaster.GetDateTime(createdDateOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedByOrdinal)) routeMasterEntity.LastModifiedBy = drRouteMaster.GetString(lastModifiedByOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedDateOrdinal)) routeMasterEntity.LastModifiedDate = drRouteMaster.GetDateTime(lastModifiedDateOrdinal);
                        if (!drRouteMaster.IsDBNull(totalCountOrdinal)) routeMasterEntity.TotalCount = drRouteMaster.GetInt32(totalCountOrdinal);
                        if (!drRouteMaster.IsDBNull(repCodeOrdinal)) routeMasterEntity.RepCode = drRouteMaster.GetString(repCodeOrdinal);
                        if (!drRouteMaster.IsDBNull(repNameOrdinal)) routeMasterEntity.RepName = drRouteMaster.GetString(repNameOrdinal);
                        if (!drRouteMaster.IsDBNull(createdByNameOrdinal)) routeMasterEntity.CreatedByName = drRouteMaster.GetString(createdByNameOrdinal);
                        if (!drRouteMaster.IsDBNull(callcycleIdOrdinal)) routeMasterEntity.CallCycleId = drRouteMaster.GetInt32(callcycleIdOrdinal);
                        if (!drRouteMaster.IsDBNull(IsCheckListOrdinal)) routeMasterEntity.HasSelect = drRouteMaster.GetBoolean(IsCheckListOrdinal);

                        routeMasterList.Add(routeMasterEntity);
                    }
                }

                return routeMasterList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drRouteMaster != null)
                    drRouteMaster.Close();
            }
        }

        public List<RouteMasterEntity> GetAllTMERoutesMaster(string originator, ArgsEntity args)
        {
            DbDataReader drRouteMaster = null;

            try
            {
                List<RouteMasterEntity> routeMasterList = new List<RouteMasterEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@TMECode", originator, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);

                drRouteMaster = this.DataAcessService.ExecuteQuery(RouteMasterSql["GetAllRoutesForTME"], paramCollection);

                if (drRouteMaster != null && drRouteMaster.HasRows)
                {
                    int idOrdinal = drRouteMaster.GetOrdinal("id");
                    int routeNameOrdinal = drRouteMaster.GetOrdinal("name");
                    int statusOrdinal = drRouteMaster.GetOrdinal("status");
                    int routeSequenceOrdinal = drRouteMaster.GetOrdinal("route_sequence");
                    int createdByOrdinal = drRouteMaster.GetOrdinal("created_by");
                    int createdDateOrdinal = drRouteMaster.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drRouteMaster.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drRouteMaster.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drRouteMaster.GetOrdinal("total_count");
                    int callcycleIdOrdinal = drRouteMaster.GetOrdinal("callcycle_id");
                    int repCodeOrdinal = drRouteMaster.GetOrdinal("rep_code");
                    int repNameOrdinal = drRouteMaster.GetOrdinal("rep_name");

                    while (drRouteMaster.Read())
                    {
                        RouteMasterEntity routeMasterEntity = CreateObject();

                        if (!drRouteMaster.IsDBNull(idOrdinal)) routeMasterEntity.RouteMasterId = drRouteMaster.GetInt32(idOrdinal);
                        if (!drRouteMaster.IsDBNull(routeNameOrdinal)) routeMasterEntity.RouteMasterName = drRouteMaster.GetString(routeNameOrdinal);
                        if (!drRouteMaster.IsDBNull(routeSequenceOrdinal)) routeMasterEntity.RouteSequence = drRouteMaster.GetInt32(routeSequenceOrdinal);
                        if (!drRouteMaster.IsDBNull(statusOrdinal)) routeMasterEntity.Status = drRouteMaster.GetString(statusOrdinal);
                        if (!drRouteMaster.IsDBNull(createdByOrdinal)) routeMasterEntity.CreatedBy = drRouteMaster.GetString(createdByOrdinal);
                        if (!drRouteMaster.IsDBNull(createdDateOrdinal)) routeMasterEntity.CreatedDate = drRouteMaster.GetDateTime(createdDateOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedByOrdinal)) routeMasterEntity.LastModifiedBy = drRouteMaster.GetString(lastModifiedByOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedDateOrdinal)) routeMasterEntity.LastModifiedDate = drRouteMaster.GetDateTime(lastModifiedDateOrdinal);
                        if (!drRouteMaster.IsDBNull(totalCountOrdinal)) routeMasterEntity.TotalCount = drRouteMaster.GetInt32(totalCountOrdinal);
                        if (!drRouteMaster.IsDBNull(callcycleIdOrdinal)) routeMasterEntity.CallCycleId = drRouteMaster.GetInt32(callcycleIdOrdinal);
                        if (!drRouteMaster.IsDBNull(repCodeOrdinal)) routeMasterEntity.RepCode = drRouteMaster.GetString(repCodeOrdinal);
                        if (!drRouteMaster.IsDBNull(repNameOrdinal)) routeMasterEntity.RepName = drRouteMaster.GetString(repNameOrdinal);

                        routeMasterList.Add(routeMasterEntity);
                    }
                }

                return routeMasterList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drRouteMaster != null)
                    drRouteMaster.Close();
            }
        }

        public List<RouteMasterEntity> GetAllTMERoutesMasterCheckList(int CheckListId, string originator, ArgsEntity args)
        {
            DbDataReader drRouteMaster = null;

            try
            {
                List<RouteMasterEntity> routeMasterList = new List<RouteMasterEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CheckListId", CheckListId, DbType.String);
                paramCollection.Add("@TMECode", originator, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);

                drRouteMaster = this.DataAcessService.ExecuteQuery(RouteMasterSql["GetAllRoutesForTMEForCheckList"], paramCollection);

                if (drRouteMaster != null && drRouteMaster.HasRows)
                {
                    int idOrdinal = drRouteMaster.GetOrdinal("id");
                    int routeNameOrdinal = drRouteMaster.GetOrdinal("name");
                    int statusOrdinal = drRouteMaster.GetOrdinal("status");
                    int routeSequenceOrdinal = drRouteMaster.GetOrdinal("route_sequence");
                    int createdByOrdinal = drRouteMaster.GetOrdinal("created_by");
                    int createdDateOrdinal = drRouteMaster.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drRouteMaster.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drRouteMaster.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drRouteMaster.GetOrdinal("total_count");
                    int callcycleIdOrdinal = drRouteMaster.GetOrdinal("callcycle_id");
                    int repCodeOrdinal = drRouteMaster.GetOrdinal("rep_code");
                    int repNameOrdinal = drRouteMaster.GetOrdinal("rep_name");
                    int IsCheckListOrdinal = drRouteMaster.GetOrdinal("IsCheckList");

                    while (drRouteMaster.Read())
                    {
                        RouteMasterEntity routeMasterEntity = CreateObject();

                        if (!drRouteMaster.IsDBNull(idOrdinal)) routeMasterEntity.RouteMasterId = drRouteMaster.GetInt32(idOrdinal);
                        if (!drRouteMaster.IsDBNull(routeNameOrdinal)) routeMasterEntity.RouteMasterName = drRouteMaster.GetString(routeNameOrdinal);
                        if (!drRouteMaster.IsDBNull(routeSequenceOrdinal)) routeMasterEntity.RouteSequence = drRouteMaster.GetInt32(routeSequenceOrdinal);
                        if (!drRouteMaster.IsDBNull(statusOrdinal)) routeMasterEntity.Status = drRouteMaster.GetString(statusOrdinal);
                        if (!drRouteMaster.IsDBNull(createdByOrdinal)) routeMasterEntity.CreatedBy = drRouteMaster.GetString(createdByOrdinal);
                        if (!drRouteMaster.IsDBNull(createdDateOrdinal)) routeMasterEntity.CreatedDate = drRouteMaster.GetDateTime(createdDateOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedByOrdinal)) routeMasterEntity.LastModifiedBy = drRouteMaster.GetString(lastModifiedByOrdinal);
                        if (!drRouteMaster.IsDBNull(lastModifiedDateOrdinal)) routeMasterEntity.LastModifiedDate = drRouteMaster.GetDateTime(lastModifiedDateOrdinal);
                        if (!drRouteMaster.IsDBNull(totalCountOrdinal)) routeMasterEntity.TotalCount = drRouteMaster.GetInt32(totalCountOrdinal);
                        if (!drRouteMaster.IsDBNull(callcycleIdOrdinal)) routeMasterEntity.CallCycleId = drRouteMaster.GetInt32(callcycleIdOrdinal);
                        if (!drRouteMaster.IsDBNull(repCodeOrdinal)) routeMasterEntity.RepCode = drRouteMaster.GetString(repCodeOrdinal);
                        if (!drRouteMaster.IsDBNull(repNameOrdinal)) routeMasterEntity.RepName = drRouteMaster.GetString(repNameOrdinal);
                        if (!drRouteMaster.IsDBNull(IsCheckListOrdinal)) routeMasterEntity.HasSelect = drRouteMaster.GetBoolean(IsCheckListOrdinal);

                        routeMasterList.Add(routeMasterEntity);
                    }
                }

                return routeMasterList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drRouteMaster != null)
                    drRouteMaster.Close();
            }
        }

        public List<RouteMasterEntity> GetRouteTargetsByDistributorId(ArgsEntity args)
        {
            List<RouteMasterEntity> targets = new List<RouteMasterEntity>();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@EffectiveStartDate", args.DtStartDate.ToString("yyyy-MM-dd"), DbType.String);
            //paramCollection.Add("@EffectiveEndDate", args.DtEndDate.ToString("yyyy-MM-dd"), DbType.String);
            paramCollection.Add("@DistributorId", args.OriginatorId, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(RouteMasterSql["GetRouteTargetsByDistributorId"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int routeIdOrdinal = dataReader.GetOrdinal("route_master_id");
                    int targetIdOrdinal = dataReader.GetOrdinal("target_id");
                    int targetOrdinal = dataReader.GetOrdinal("stick_target");
                    int nameOrdinal = dataReader.GetOrdinal("name");

                    RouteMasterEntity targetEntity = null;
                    while (dataReader.Read())
                    {
                        targetEntity = CreateObject();

                        if (!dataReader.IsDBNull(routeIdOrdinal)) targetEntity.RouteMasterId = dataReader.GetInt32(routeIdOrdinal);
                        if (!dataReader.IsDBNull(targetIdOrdinal)) targetEntity.TargetId = dataReader.GetInt32(targetIdOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) targetEntity.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(nameOrdinal)) targetEntity.RouteMasterName = dataReader.GetString(nameOrdinal);

                        targets.Add(targetEntity);
                    }
                }
                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public bool InsertRouteTarget(RouteMasterEntity routeMasterEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@RouteID", routeMasterEntity.RouteMasterId, DbType.Int32);
                paramCollection.Add("@Target", routeMasterEntity.Target, DbType.Decimal);
                paramCollection.Add("@StickTarget", routeMasterEntity.Target, DbType.Int32);
                paramCollection.Add("@EffStartDate", routeMasterEntity.EffStartDate, DbType.DateTime);
                paramCollection.Add("@EffEndDate", routeMasterEntity.EffEndDate, DbType.DateTime);
                paramCollection.Add("@CreatedBy", routeMasterEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RouteMasterSql["InsertRouteTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateRouteTarget(RouteMasterEntity routeMasterEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@TargetID", routeMasterEntity.TargetId, DbType.Int32);
                paramCollection.Add("@StickTarget", routeMasterEntity.Target, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", routeMasterEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RouteMasterSql["UpdateRouteTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool IsRouteMasterExists(RouteMasterEntity routeMasterEntity)
        {
            DbDataReader drRouteMaster = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@RouteMasterId", routeMasterEntity.RouteMasterId, DbType.String);
                paramCollection.Add("@RouteName", routeMasterEntity.RouteMasterName, DbType.String);

                drRouteMaster = this.DataAcessService.ExecuteQuery(RouteMasterSql["RouteMasterExist"], paramCollection);

                if (drRouteMaster != null && drRouteMaster.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drRouteMaster != null)
                    drRouteMaster.Close();
            }
        }

        public RouteMasterEntity GetRouteTargetFromRouteId(int routeMasterId)
        {
            DbDataReader drRouteMaster = null;

            try
            {
                RouteMasterEntity routeMasterEntity = CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@RouteID", routeMasterId, DbType.Int32);

                drRouteMaster = this.DataAcessService.ExecuteQuery(RouteMasterSql["GetRouteTargetFromRouteId"], paramCollection);

                if (drRouteMaster != null && drRouteMaster.HasRows)
                {
                    int targetIdOrdinal = drRouteMaster.GetOrdinal("id");
                    int routeMasterIdOrdinal = drRouteMaster.GetOrdinal("route_master_id");
                    int routeNameOrdinal = drRouteMaster.GetOrdinal("name");
                    int targetOrdinal = drRouteMaster.GetOrdinal("stick_target");


                    while (drRouteMaster.Read())
                    {
                        if (!drRouteMaster.IsDBNull(targetIdOrdinal)) routeMasterEntity.TargetId = drRouteMaster.GetInt32(targetIdOrdinal);
                        if (!drRouteMaster.IsDBNull(routeMasterIdOrdinal)) routeMasterEntity.RouteMasterId = drRouteMaster.GetInt32(routeMasterIdOrdinal);
                        if (!drRouteMaster.IsDBNull(routeNameOrdinal)) routeMasterEntity.RouteMasterName = drRouteMaster.GetString(routeNameOrdinal);
                        if (!drRouteMaster.IsDBNull(targetOrdinal)) routeMasterEntity.Target = drRouteMaster.GetInt32(targetOrdinal);
                    }
                }

                return routeMasterEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drRouteMaster != null)
                    drRouteMaster.Close();
            }
        }

        public List<RouteEntity> GetRouteRepLikeRouteNameForCustomer(string name, int isTemp, string originator, string chlidOriginators)
        {
            DbDataReader drRouteEntity = null;
            List<RouteEntity> lstRouteEntity = new List<RouteEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteName", name, DbType.String);
                paramCollection.Add("@IsTemp", isTemp, DbType.Int32); //0 or 1
                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@ChildOriginator", chlidOriginators.Replace("originator", "rp.user_name"), DbType.String);

                drRouteEntity = this.DataAcessService.ExecuteQuery(RouteMasterSql["GetRouteRepLikeRouteForCustomer"], paramCollection);
                if (drRouteEntity != null && drRouteEntity.HasRows)
                {
                    int idOrdinal = drRouteEntity.GetOrdinal("id");
                    int routeNameOrdinal = drRouteEntity.GetOrdinal("name");
                    int tempRepNameOrdinal = drRouteEntity.GetOrdinal("tempRepName");
                    int originalRepOrdinal = drRouteEntity.GetOrdinal("rep_code");
                    int routeMasterIdOrdinal = drRouteEntity.GetOrdinal("route_master_id");
                    int marketIdOrdinal = drRouteEntity.GetOrdinal("market_id");
                    int marketNameOrdinal = drRouteEntity.GetOrdinal("market_name");

                    while (drRouteEntity.Read())
                    {
                        RouteEntity RouteEntity = RouteEntity.CreateObject();
                        if (!drRouteEntity.IsDBNull(idOrdinal)) RouteEntity.RouteId = drRouteEntity.GetInt32(idOrdinal);
                        if (!drRouteEntity.IsDBNull(routeNameOrdinal)) RouteEntity.RouteName = drRouteEntity.GetString(routeNameOrdinal);
                        if (!drRouteEntity.IsDBNull(tempRepNameOrdinal)) RouteEntity.TempRepName = drRouteEntity.GetString(tempRepNameOrdinal);
                        if (!drRouteEntity.IsDBNull(originalRepOrdinal)) RouteEntity.RepCode = drRouteEntity.GetString(originalRepOrdinal);
                        if (!drRouteEntity.IsDBNull(routeMasterIdOrdinal)) RouteEntity.RouteMasterId = drRouteEntity.GetInt32(routeMasterIdOrdinal);

                        //MarketID
                        if (!drRouteEntity.IsDBNull(marketIdOrdinal)) RouteEntity.OriginatorType = drRouteEntity.GetInt32(marketIdOrdinal).ToString();
                        //MarketName
                        if (!drRouteEntity.IsDBNull(marketNameOrdinal)) RouteEntity.TMECode = drRouteEntity.GetString(marketNameOrdinal);
                        lstRouteEntity.Add(RouteEntity);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drRouteEntity != null)
                    drRouteEntity.Close();
            }
            return lstRouteEntity;
        }

        public bool DeleteRouteMaster(RouteMasterEntity routeMasterEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@id", routeMasterEntity.RouteMasterId, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", routeMasterEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RouteMasterSql["DeleteRouteMaster"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        #region "For 2nd Phase developemtn"
        
        public List<RouteMasterModel> GetAllRouteMaster(ArgsModel args)
        {
            RouteMasterModel objModel = null;
            DataTable objDS = new DataTable();
            List<RouteMasterModel> objList = new List<RouteMasterModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRouteMaster";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new RouteMasterModel();
                            if (item["route_master_id"] != DBNull.Value) objModel.RouteMasterId = Convert.ToInt32(item["route_master_id"]);
                            if (item["route_master_code"] != DBNull.Value) objModel.RouteMasterCode = item["route_master_code"].ToString();
                            if (item["route_master_name"] != DBNull.Value) objModel.RouteMasterName = item["route_master_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["territory_status"] != DBNull.Value) objModel.TerritoryStatus = item["territory_status"].ToString();
                            //if (item["rep_id"] != DBNull.Value) objModel.RepId = Convert.ToInt32(item["rep_id"]);
                            //if (item["rep_code"] != DBNull.Value) objModel.RepCode = item["rep_code"].ToString();
                            //if (item["rep_name"] != DBNull.Value) objModel.RepName = item["rep_name"].ToString();
                            //if (item["rep_status"] != DBNull.Value) objModel.RepStatus = item["rep_status"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<RouteMasterModel> GetAllRouteMasterByTerritoryId(ArgsModel args, string TerritoryId)
        {
            RouteMasterModel objModel = null;
            DataTable objDS = new DataTable();
            List<RouteMasterModel> objList = new List<RouteMasterModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRouteMasterByTerritoryId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@TerritoryId", TerritoryId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new RouteMasterModel();
                            if (item["route_master_id"] != DBNull.Value) objModel.RouteMasterId = Convert.ToInt32(item["route_master_id"]);
                            if (item["route_master_code"] != DBNull.Value) objModel.RouteMasterCode = item["route_master_code"].ToString();
                            if (item["route_master_name"] != DBNull.Value) objModel.RouteMasterName = item["route_master_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_status"] != DBNull.Value) objModel.TerritoryStatus = item["territory_status"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        } 
        
        public RouteMasterModel GetRouteByRouteCode(SqlConnection conn, string RouteCode)
        {
            RouteMasterModel objModel = new RouteMasterModel();
            DataTable objDS = new DataTable();

            try
            {
                using (conn)
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRouteByRouteCode";

                    objCommand.Parameters.AddWithValue("@RouteCode", RouteCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        objModel = new RouteMasterModel();
                        if (objDS.Rows[0]["route_master_id"] != DBNull.Value) objModel.RouteMasterId = Convert.ToInt32(objDS.Rows[0]["route_master_id"]);
                        if (objDS.Rows[0]["route_master_code"] != DBNull.Value) objModel.RouteMasterCode = objDS.Rows[0]["route_master_code"].ToString();
                        if (objDS.Rows[0]["route_master_name"] != DBNull.Value) objModel.RouteMasterName = objDS.Rows[0]["route_master_name"].ToString();
                        if (objDS.Rows[0]["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(objDS.Rows[0]["territory_id"]);
                    }
                    else
                    {
                        objModel.RouteMasterId = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objModel;
        }

        public RouteMasterModel GetRouteByRouteName(SqlConnection conn, string RouteName)
        {
            RouteMasterModel objModel = null;
            DataTable objDS = new DataTable();

            try
            {
                using (conn)
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRouteByRouteName";

                    objCommand.Parameters.AddWithValue("@RouteName", RouteName);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        objModel = new RouteMasterModel();
                        if (objDS.Rows[0]["route_master_id"] != DBNull.Value) objModel.RouteMasterId = Convert.ToInt32(objDS.Rows[0]["route_master_id"]);
                        if (objDS.Rows[0]["route_master_code"] != DBNull.Value) objModel.RouteMasterCode = objDS.Rows[0]["route_master_code"].ToString();
                        if (objDS.Rows[0]["route_master_name"] != DBNull.Value) objModel.RouteMasterName = objDS.Rows[0]["route_master_name"].ToString();
                        if (objDS.Rows[0]["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(objDS.Rows[0]["territory_id"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objModel;
        }

        public bool DeleteRouteFromTerritoryByRouteId(string routeId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteRouteFromTerritoryByRouteId";

                    objCommand.Parameters.AddWithValue("@RouteId", routeId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public bool InsertRouteMaster(DbWorkflowScope transactionScope, RouteMasterModel routeMaster)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(transactionScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertRouteMasterNew";

                    objCommand.Parameters.AddWithValue("@TerritoryId", routeMaster.TerritoryId);
                    objCommand.Parameters.AddWithValue("@RouteCode", routeMaster.RouteMasterCode);
                    objCommand.Parameters.AddWithValue("@RouteName", routeMaster.RouteMasterName);
                    objCommand.Parameters.AddWithValue("@CreatedBy", routeMaster.CreatedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool DeleteRouteMasterByRouteId(DbWorkflowScope transactionScope, string routeId, string DeletedBy)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(transactionScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteRouteMasterByRouteId";

                    objCommand.Parameters.AddWithValue("@routeId", routeId);
                    objCommand.Parameters.AddWithValue("@LastModifiedBy", DeletedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateRouteMaster(DbWorkflowScope transactionScope, RouteMasterModel routeMaster)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(transactionScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateRouteMasterNew";

                    objCommand.Parameters.AddWithValue("@RouteMasterId", routeMaster.RouteMasterId);
                    objCommand.Parameters.AddWithValue("@TerritoryId", routeMaster.TerritoryId);
                    objCommand.Parameters.AddWithValue("@RouteCode", routeMaster.RouteMasterCode);
                    objCommand.Parameters.AddWithValue("@RouteName", routeMaster.RouteMasterName);
                    objCommand.Parameters.AddWithValue("@CreatedBy", routeMaster.CreatedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        #endregion


    }
}
