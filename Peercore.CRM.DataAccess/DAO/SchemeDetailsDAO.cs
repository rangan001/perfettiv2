﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SchemeDetailsDAO : BaseDAO
    {
         private DbSqlAdapter SchemeDetailsSql { get; set; }

        private void RegisterSql()
        {
            this.SchemeDetailsSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SchemeDetailsSql.xml", ApplicationService.Instance.DbProvider);
        }

        public SchemeDetailsDAO()
        {
            RegisterSql();
        }

        public SchemeDetailsDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public SchemeDetailsEntity CreateObject()
        {
            return SchemeDetailsEntity.CreateObject();
        }

        public SchemeDetailsEntity CreateObject(int entityId)
        {
            return SchemeDetailsEntity.CreateObject(entityId);
        }

        public bool UpdateSchemeDetails(SchemeDetailsEntity schemeDetail)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SchemeDetailsId", schemeDetail.SchemeDetailsId, DbType.Int32);
                paramCollection.Add("@SchemeHeaderId", schemeDetail.SchemeHeaderId, DbType.Int32);
                paramCollection.Add("@DetailsTypeId", schemeDetail.DetailsTypeId, DbType.Int32);                
                paramCollection.Add("@SchemeTypeId", schemeDetail.SchemeTypeId, DbType.Int32);
                paramCollection.Add("@SchemeDetails", schemeDetail.SchemeDetails, DbType.String);
                paramCollection.Add("@DiscountValue", schemeDetail.DiscountValue, DbType.Double);
                paramCollection.Add("@DiscountPercentage", schemeDetail.DiscountPercentage, DbType.Double);
                paramCollection.Add("@IsRetail", schemeDetail.IsRetail, DbType.Boolean);
                //paramCollection.Add("@StartDate", schemeDetail.StartDate, DbType.DateTime);
                //paramCollection.Add("@EndDate", schemeDetail.EndDate, DbType.DateTime);
                paramCollection.Add("@IsActive", schemeDetail.IsActive, DbType.Boolean);
                paramCollection.Add("@LastModifiedBy", schemeDetail.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(SchemeDetailsSql["UpdateSchemeDetails"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public bool InsertSchemeDetails(out int schemeDetailsId, SchemeDetailsEntity schemeDetail)
        {
            bool successful = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SchemeHeaderId", schemeDetail.SchemeHeaderId, DbType.Int32);
                paramCollection.Add("@DetailsTypeId", schemeDetail.DetailsTypeId, DbType.Int32);
                paramCollection.Add("@SchemeTypeId", schemeDetail.SchemeTypeId, DbType.Int32);
                paramCollection.Add("@SchemeDetails", schemeDetail.SchemeDetails, DbType.String);
                paramCollection.Add("@DiscountValue", schemeDetail.DiscountValue, DbType.Double);
                paramCollection.Add("@DiscountPercentage", schemeDetail.DiscountPercentage, DbType.Double);
                paramCollection.Add("@IsRetail", schemeDetail.IsRetail, DbType.Boolean);
                //paramCollection.Add("@StartDate", schemeDetail.StartDate, DbType.DateTime);
                //paramCollection.Add("@EndDate", schemeDetail.EndDate, DbType.DateTime);
                paramCollection.Add("@IsActive", true, DbType.Boolean);
                paramCollection.Add("@CreatedBy", schemeDetail.CreatedBy, DbType.String);
                paramCollection.Add("@FreeIssueCondition", schemeDetail.FreeIssueCondition, DbType.String);
                paramCollection.Add("@SchemeDetailsId", null, DbType.Int32, ParameterDirection.Output);

                schemeDetailsId = this.DataAcessService.ExecuteNonQuery(SchemeDetailsSql["InsertSchemeDetails"], paramCollection, true, "@SchemeDetailsId");
                if (schemeDetailsId > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

        public List<SchemeDetailsEntity> GetSchemeDetailsBySchemeHeaderId(ArgsEntity args, int schemeHeaderId)
        {
            DbDataReader drSchemeHeader = null;
            try
            {
                List<SchemeDetailsEntity> SchemeDetailsCollection = new List<SchemeDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SchemeHeaderId", schemeHeaderId, DbType.Int32);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeDetailsSql["GetSchemeDetailsBySchemeHeaderId"], paramCollection);
                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeDetailsIdOrdinal = drSchemeHeader.GetOrdinal("scheme_details_id");
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int detailsTypeIdOrdinal = drSchemeHeader.GetOrdinal("details_type_id");
                    int schemeTypeIdOrdinal = drSchemeHeader.GetOrdinal("scheme_type_id");
                    int schemeDetailsOrdinal = drSchemeHeader.GetOrdinal("scheme_details");

                    int discountValueOrdinal = drSchemeHeader.GetOrdinal("discount_value");
                    int discountPercentageOrdinal = drSchemeHeader.GetOrdinal("discount_percentage");
                    //int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    //int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");

                    int schemeHeaderOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    int detailsTypeOrdinal = drSchemeHeader.GetOrdinal("details_type");
                    int schemeTypeOrdinal = drSchemeHeader.GetOrdinal("scheme_type");
                    int isRetailOrdinal = drSchemeHeader.GetOrdinal("is_retail");
                    

                    while (drSchemeHeader.Read())
                    {
                        SchemeDetailsEntity SchemeDetails = SchemeDetailsEntity.CreateObject();

                        if (!drSchemeHeader.IsDBNull(schemeDetailsIdOrdinal)) SchemeDetails.SchemeDetailsId = drSchemeHeader.GetInt32(schemeDetailsIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) SchemeDetails.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(detailsTypeIdOrdinal)) SchemeDetails.DetailsTypeId = drSchemeHeader.GetInt32(detailsTypeIdOrdinal);                       
                        if (!drSchemeHeader.IsDBNull(schemeTypeIdOrdinal)) SchemeDetails.SchemeTypeId = drSchemeHeader.GetInt32(schemeTypeIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeDetailsOrdinal)) SchemeDetails.SchemeDetails = drSchemeHeader.GetString(schemeDetailsOrdinal);
                        if (!drSchemeHeader.IsDBNull(discountValueOrdinal)) SchemeDetails.DiscountValue = drSchemeHeader.GetDouble(discountValueOrdinal);
                        if (!drSchemeHeader.IsDBNull(discountPercentageOrdinal)) SchemeDetails.DiscountPercentage = drSchemeHeader.GetDouble(discountPercentageOrdinal);
                        //if (!drSchemeHeader.IsDBNull(startDateOrdinal)) SchemeDetails.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        //if (!drSchemeHeader.IsDBNull(endDateOrdinal)) SchemeDetails.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) SchemeDetails.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeHeaderOrdinal)) SchemeDetails.SchemeHeaderName = drSchemeHeader.GetString(schemeHeaderOrdinal);
                        if (!drSchemeHeader.IsDBNull(detailsTypeOrdinal)) SchemeDetails.DetailsTypeName = drSchemeHeader.GetString(detailsTypeOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeTypeOrdinal)) SchemeDetails.SchemeTypeName = drSchemeHeader.GetString(schemeTypeOrdinal);
                        if (!drSchemeHeader.IsDBNull(isRetailOrdinal)) SchemeDetails.IsRetail = drSchemeHeader.GetBoolean(isRetailOrdinal);
                        
                        SchemeDetailsCollection.Add(SchemeDetails);

                    }
                }

                return SchemeDetailsCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        public bool UpdateSchemeDetailsInActive(int schemeHeaderId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SchemeHeaderId", schemeHeaderId, DbType.Int32);
                paramCollection.Add("@IsActive", false, DbType.Boolean);

                iNoRec = this.DataAcessService.ExecuteNonQuery(SchemeDetailsSql["UpdateSchemeDetailsInActive"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public List<SchemeDetailsEntity> GetSchemeDetailsByDivisionId(int divisionId)
        {
            DbDataReader drSchemeHeader = null;
            try
            {
                List<SchemeDetailsEntity> SchemeDetailsCollection = new List<SchemeDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeDetailsSql["GetSchemeDetailsByDivisionId"], paramCollection);
                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeDetailsIdOrdinal = drSchemeHeader.GetOrdinal("scheme_details_id");
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int detailsTypeIdOrdinal = drSchemeHeader.GetOrdinal("details_type_id");
                    int schemeTypeIdOrdinal = drSchemeHeader.GetOrdinal("scheme_type_id");
                    int schemeDetailsOrdinal = drSchemeHeader.GetOrdinal("scheme_details");

                    int discountValueOrdinal = drSchemeHeader.GetOrdinal("discount_value");
                    int discountPercentageOrdinal = drSchemeHeader.GetOrdinal("discount_percentage");
                    //int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    //int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");

                    //int schemeHeaderOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    //int detailsTypeOrdinal = drSchemeHeader.GetOrdinal("details_type");
                    //int schemeTypeOrdinal = drSchemeHeader.GetOrdinal("scheme_type");
                    int isRetailOrdinal = drSchemeHeader.GetOrdinal("is_retail");


                    while (drSchemeHeader.Read())
                    {
                        SchemeDetailsEntity SchemeDetails = SchemeDetailsEntity.CreateObject();

                        if (!drSchemeHeader.IsDBNull(schemeDetailsIdOrdinal)) SchemeDetails.SchemeDetailsId = drSchemeHeader.GetInt32(schemeDetailsIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) SchemeDetails.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(detailsTypeIdOrdinal)) SchemeDetails.DetailsTypeId = drSchemeHeader.GetInt32(detailsTypeIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeTypeIdOrdinal)) SchemeDetails.SchemeTypeId = drSchemeHeader.GetInt32(schemeTypeIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeDetailsOrdinal)) SchemeDetails.SchemeDetails = drSchemeHeader.GetString(schemeDetailsOrdinal);
                        if (!drSchemeHeader.IsDBNull(discountValueOrdinal)) SchemeDetails.DiscountValue = drSchemeHeader.GetDouble(discountValueOrdinal);
                        if (!drSchemeHeader.IsDBNull(discountPercentageOrdinal)) SchemeDetails.DiscountPercentage = drSchemeHeader.GetDouble(discountPercentageOrdinal);
                        //if (!drSchemeHeader.IsDBNull(startDateOrdinal)) SchemeDetails.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        //if (!drSchemeHeader.IsDBNull(endDateOrdinal)) SchemeDetails.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) SchemeDetails.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        //if (!drSchemeHeader.IsDBNull(schemeHeaderOrdinal)) SchemeDetails.SchemeHeaderName = drSchemeHeader.GetString(schemeHeaderOrdinal);
                        //if (!drSchemeHeader.IsDBNull(detailsTypeOrdinal)) SchemeDetails.DetailsTypeName = drSchemeHeader.GetString(detailsTypeOrdinal);
                        //if (!drSchemeHeader.IsDBNull(schemeTypeOrdinal)) SchemeDetails.SchemeTypeName = drSchemeHeader.GetString(schemeTypeOrdinal);
                        if (!drSchemeHeader.IsDBNull(isRetailOrdinal)) SchemeDetails.IsRetail = drSchemeHeader.GetBoolean(isRetailOrdinal);

                        SchemeDetailsCollection.Add(SchemeDetails);

                    }
                }

                return SchemeDetailsCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        public List<SchemeDetailsEntity> GetSchemeDetailsBySchemeHeaderId(int schemeHeaderId)
        {
            DbDataReader drSchemeHeader = null;
            try
            {
                List<SchemeDetailsEntity> SchemeDetailsCollection = new List<SchemeDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SchemeHeaderId", schemeHeaderId, DbType.Int32);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeDetailsSql["GetDetailsBySchemeHeaderId"], paramCollection);
                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeDetailsIdOrdinal = drSchemeHeader.GetOrdinal("scheme_details_id");
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int detailsTypeIdOrdinal = drSchemeHeader.GetOrdinal("details_type_id");
                    int schemeTypeIdOrdinal = drSchemeHeader.GetOrdinal("scheme_type_id");
                    int schemeDetailsOrdinal = drSchemeHeader.GetOrdinal("scheme_details");

                    int discountValueOrdinal = drSchemeHeader.GetOrdinal("discount_value");
                    int discountPercentageOrdinal = drSchemeHeader.GetOrdinal("discount_percentage");
                    //int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    //int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");

                    int schemeHeaderOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    int detailsTypeOrdinal = drSchemeHeader.GetOrdinal("details_type");
                    int schemeTypeOrdinal = drSchemeHeader.GetOrdinal("scheme_type");
                    int isRetailOrdinal = drSchemeHeader.GetOrdinal("is_retail");
                    int freeIssueCondOrdinal = drSchemeHeader.GetOrdinal("free_issue_cond");

                    while (drSchemeHeader.Read())
                    {
                        SchemeDetailsEntity SchemeDetails = SchemeDetailsEntity.CreateObject();

                        if (!drSchemeHeader.IsDBNull(schemeDetailsIdOrdinal)) SchemeDetails.SchemeDetailsId = drSchemeHeader.GetInt32(schemeDetailsIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) SchemeDetails.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(detailsTypeIdOrdinal)) SchemeDetails.DetailsTypeId = drSchemeHeader.GetInt32(detailsTypeIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeTypeIdOrdinal)) SchemeDetails.SchemeTypeId = drSchemeHeader.GetInt32(schemeTypeIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeDetailsOrdinal)) SchemeDetails.SchemeDetails = drSchemeHeader.GetString(schemeDetailsOrdinal);
                        if (!drSchemeHeader.IsDBNull(discountValueOrdinal)) SchemeDetails.DiscountValue = drSchemeHeader.GetDouble(discountValueOrdinal);
                        if (!drSchemeHeader.IsDBNull(discountPercentageOrdinal)) SchemeDetails.DiscountPercentage = drSchemeHeader.GetDouble(discountPercentageOrdinal);
                        //if (!drSchemeHeader.IsDBNull(startDateOrdinal)) SchemeDetails.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        //if (!drSchemeHeader.IsDBNull(endDateOrdinal)) SchemeDetails.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) SchemeDetails.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeHeaderOrdinal)) SchemeDetails.SchemeHeaderName = drSchemeHeader.GetString(schemeHeaderOrdinal);
                        if (!drSchemeHeader.IsDBNull(detailsTypeOrdinal)) SchemeDetails.DetailsTypeName = drSchemeHeader.GetString(detailsTypeOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeTypeOrdinal)) SchemeDetails.SchemeTypeName = drSchemeHeader.GetString(schemeTypeOrdinal);
                        if (!drSchemeHeader.IsDBNull(isRetailOrdinal)) SchemeDetails.IsRetail = drSchemeHeader.GetBoolean(isRetailOrdinal);
                        if (!drSchemeHeader.IsDBNull(freeIssueCondOrdinal)) SchemeDetails.FreeIssueCondition = drSchemeHeader.GetString(freeIssueCondOrdinal).ToLower();

                        SchemeDetailsCollection.Add(SchemeDetails);
                    }
                }

                return SchemeDetailsCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }
    }
}
