﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SearchDAO:BaseDAO
    {
        private DbSqlAdapter SearchSql { get; set; }

        private void RegisterSql()
        {
            this.SearchSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SearchSql.xml", ApplicationService.Instance.DbProvider);
        }

        public SearchDAO()
        {
            RegisterSql();
        }

        public LeadContactPersonViewEntity CreateObject()
        {
            return LeadContactPersonViewEntity.CreateObject();
        }

        public LeadContactPersonViewEntity CreateObject(int entityId)
        {
            return LeadContactPersonViewEntity.CreateObject(entityId);
        }

        public List<LeadContactPersonViewEntity> GetContactCollection(ArgsEntity argsEntity, bool bIncludeContact, bool bIncludeLead,
            bool bIncludeCustomer, bool bIncludeEndUser, string clientType ,  
            List<OriginatorEntity> listOriginator , 
            string sSearchText = "", 
            string childOriginators = "")
        {
            string sWhereCls = "";
            string sWhereClsLeadContact = "";
            string sWhereClsCustomerContact = "";           
            string sSQL = "";
            string sSearchWhrCls = "";
            DbDataReader leadsReader = null;
            string sOriginators = "";
            string sClientTypes = "";

            try
            {
                List<LeadContactPersonViewEntity> contactCollection = new List<LeadContactPersonViewEntity>();

                if (argsEntity.Originator != "")
                {                   
                    sWhereCls = childOriginators;
                    sWhereClsLeadContact = sWhereCls.Replace("originator", "l.originator");
                    sWhereClsCustomerContact = sWhereCls.Replace("originator", "r.originator");
                }

                if (sSearchText == null)
                    sSearchText = "";

                if (sSearchText != "")
                    sSearchText = sSearchText.ToUpper();

                if (bIncludeContact)
                {
                    sOriginators = childOriginators;
                    sOriginators = sOriginators.Replace("originator", "origin");

                    sClientTypes = "";

                   
                   
                    OriginatorEntity originator = OriginatorEntity.CreateObject();
                    originator.UserName = argsEntity.Originator;
                    originator.ClientType = clientType;
                    listOriginator.Add(originator);
                  
                    List<string> listClientTypes = listOriginator.Select(s => s.ClientType).Distinct().ToList();

                    if (listClientTypes.Count > 0)
                    {
                        sClientTypes = "origin IN (";

                        foreach (string sType in listClientTypes)
                            sClientTypes += "'" + sType + "',";

                        sClientTypes = sClientTypes.Remove(sClientTypes.Length - 1);
                        sClientTypes += ")";
                    }

                    //**sSQL = string.Format(CRMSQLs.SearchContactPerson, " AND " + sClientTypes, sOriginators, sSearchText);
                }

                if (bIncludeLead)
                {
                    if (sSQL != "")
                        sSQL = sSQL + " UNION ";

                    //**sSQL += string.Format(CRMSQLs.SearchLead, " AND " + sWhereCls, sSearchText);
                }

                if (bIncludeCustomer)
                {
                    if (sSQL != "")
                        sSQL = sSQL + " UNION ";

                    //**sSQL += string.Format(CRMSQLs.SearchCustomer, " AND " + sWhereCls, sSearchText);
                }

                if (bIncludeEndUser)
                {
                    if (sSQL != "")
                        sSQL = sSQL + " UNION ";

                    //**sSQL += string.Format(CRMSQLs.SearchEndUser, " AND " + sWhereCls, sSearchText);
                }

                //**leadsReader = (IngresDataReader)oDataHandle.ExecuteSelect(sSQL);

                if (leadsReader.HasRows)
                {
                    int contactPersonIdOrdinal = leadsReader.GetOrdinal("contact_person_id");
                    int leadIdOrdinal = leadsReader.GetOrdinal("lead_id");
                    int custCodeOrdinal = leadsReader.GetOrdinal("cust_code");
                    int nameOrdinal = leadsReader.GetOrdinal("lead_name");
                    int shortNameOrdinal = leadsReader.GetOrdinal("short_name");
                    int firstNameOrdinal = leadsReader.GetOrdinal("first_name");
                    int lastNameOrdinal = leadsReader.GetOrdinal("last_name");
                    int companyOrdinal = leadsReader.GetOrdinal("company");
                    int telephoneOrdinal = leadsReader.GetOrdinal("telephone");
                    int mobileOrdinal = leadsReader.GetOrdinal("mobile");
                    int emailAddressOrdinal = leadsReader.GetOrdinal("email_address");
                    int faxOrdinal = leadsReader.GetOrdinal("fax");
                    int websiteOrdinal = leadsReader.GetOrdinal("website");
                    int enduserCodeOrdinal = leadsReader.GetOrdinal("enduser_code");
                    int contactTypeOrdinal = leadsReader.GetOrdinal("contact_type");
                    int originOrdinal = leadsReader.GetOrdinal("origin");


                    while (leadsReader.Read())
                    {
                        LeadContactPersonViewEntity lead = LeadContactPersonViewEntity.CreateObject();

                        if (!leadsReader.IsDBNull(contactPersonIdOrdinal) && (leadsReader.GetValue(contactPersonIdOrdinal).ToString() != "0")) lead.ContactPersonID = leadsReader.GetInt32(contactPersonIdOrdinal);
                        if (!leadsReader.IsDBNull(leadIdOrdinal) && (leadsReader.GetValue(leadIdOrdinal).ToString() != "0")) lead.LeadID = leadsReader.GetInt32(leadIdOrdinal);
                        if (!leadsReader.IsDBNull(custCodeOrdinal)) lead.CustCode = leadsReader.GetString(custCodeOrdinal).Trim();
                        if (!leadsReader.IsDBNull(nameOrdinal)) lead.Name = leadsReader.GetString(nameOrdinal);
                        if (!leadsReader.IsDBNull(shortNameOrdinal)) lead.ShortName = leadsReader.GetString(shortNameOrdinal);
                        if (!leadsReader.IsDBNull(firstNameOrdinal)) lead.FirstName = leadsReader.GetString(firstNameOrdinal);
                        if (!leadsReader.IsDBNull(lastNameOrdinal)) lead.LastName = leadsReader.GetString(lastNameOrdinal);
                        if (!leadsReader.IsDBNull(companyOrdinal)) lead.Company = leadsReader.GetString(companyOrdinal);
                        if (!leadsReader.IsDBNull(telephoneOrdinal)) lead.Telephone = leadsReader.GetString(telephoneOrdinal);
                        if (!leadsReader.IsDBNull(mobileOrdinal)) lead.Mobile = leadsReader.GetString(mobileOrdinal);
                        if (!leadsReader.IsDBNull(emailAddressOrdinal)) lead.Email = leadsReader.GetString(emailAddressOrdinal);
                        if (!leadsReader.IsDBNull(faxOrdinal)) lead.Fax = leadsReader.GetString(faxOrdinal);
                        if (!leadsReader.IsDBNull(websiteOrdinal)) lead.Website = leadsReader.GetString(websiteOrdinal);
                        if (!leadsReader.IsDBNull(enduserCodeOrdinal)) lead.EndUserCode = leadsReader.GetString(enduserCodeOrdinal);
                        if (!leadsReader.IsDBNull(contactTypeOrdinal)) lead.ContactType = leadsReader.GetString(contactTypeOrdinal).Trim();
                        if (!leadsReader.IsDBNull(originOrdinal)) lead.Origin = leadsReader.GetString(originOrdinal).Trim();

                        contactCollection.Add(lead);
                    }
                }

                return contactCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (!leadsReader.IsClosed)
                    leadsReader.Close();
            }
        }

       
    }
}
