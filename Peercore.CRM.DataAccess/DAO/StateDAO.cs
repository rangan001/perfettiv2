﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class StateDAO:BaseDAO
    {
        private DbSqlAdapter StateSql { get; set; }

        private void RegisterSql()
        {
            this.StateSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public StateDAO()
        {
            RegisterSql();
        }

        public StateEntity CreateObject()
        {
            return StateEntity.CreateObject();
        }

        public StateEntity CreateObject(int entityId)
        {
            return StateEntity.CreateObject(entityId);
        }

        public List<StateEntity> GetStates()
        {
            List<StateEntity> statelist = new List<StateEntity>();
            DbDataReader dataReader = null;

            dataReader = this.DataAcessService.ExecuteQuery(StateSql["GetState"]);
            
            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int stateCountOrdinal = dataReader.GetOrdinal("count");
                    int stateNameOrdinal = dataReader.GetOrdinal("name");

                    while (dataReader.Read())
                    {
                        StateEntity state = CreateObject();
                        if (!dataReader.IsDBNull(stateCountOrdinal)) state.TotalCount = dataReader.GetInt32(stateCountOrdinal);
                        if (!dataReader.IsDBNull(stateNameOrdinal)) state.StateName = dataReader.GetString(stateNameOrdinal);

                        statelist.Add(state);
                    }
                }
                return statelist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public List<StateEntity> GetAllStates()
        {
            List<StateEntity> statelist = new List<StateEntity>();
            DbDataReader dataReader = null;

            dataReader = this.DataAcessService.ExecuteQuery(StateSql["GetAllStates"]);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int stateidOrdinal = dataReader.GetOrdinal("state_id");
                    int stateNameOrdinal = dataReader.GetOrdinal("state_name");

                    while (dataReader.Read())
                    {
                        StateEntity state = CreateObject();
                        if (!dataReader.IsDBNull(stateidOrdinal)) state.StateID = dataReader.GetInt32(stateidOrdinal);
                        if (!dataReader.IsDBNull(stateNameOrdinal)) state.StateName = dataReader.GetString(stateNameOrdinal);

                        statelist.Add(state);
                    }
                }
                return statelist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public StateEntity GetStateByCode(string sStateCode)
        {
            DbDataReader dataReader = null;
            try
            {
                List<StateEntity> statelist = new List<StateEntity>();
                

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@StateCode", sStateCode, DbType.String);

                dataReader = this.DataAcessService.ExecuteQuery(StateSql["GetStateByCode"], paramCollection);
                StateEntity state = StateEntity.CreateObject();
                if (dataReader != null && dataReader.HasRows)
                {
                    int countryCodeOrdinal = dataReader.GetOrdinal("country_code");
                    int stateAbbriOrdinal = dataReader.GetOrdinal("state_abbri");
                    int stateCodeOrdinal = dataReader.GetOrdinal("state_code");
                    int stateNameOrdinal = dataReader.GetOrdinal("state_name");
                    if (dataReader.Read())
                    {
                        if (!dataReader.IsDBNull(countryCodeOrdinal)) state.CountryCode = dataReader.GetString(countryCodeOrdinal);
                        if (!dataReader.IsDBNull(stateAbbriOrdinal)) state.StateAbbri = dataReader.GetString(stateAbbriOrdinal);
                        if (!dataReader.IsDBNull(stateCodeOrdinal)) state.StateCode = dataReader.GetString(stateCodeOrdinal);
                        if (!dataReader.IsDBNull(stateNameOrdinal)) state.StateName = dataReader.GetString(stateNameOrdinal);
                    }
                }

                dataReader.Close();

                return state;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public StateEntity GetState(string sCountryCode, string sStateCode)
        {
            List<StateEntity> statelist = new List<StateEntity>();
            DbDataReader dataReader = null;
            StateEntity state = StateEntity.CreateObject();            

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CountryCode", (string.IsNullOrEmpty(sCountryCode) ? "AU" : sCountryCode), DbType.String);
                paramCollection.Add("@StateCode", sStateCode, DbType.String);

                dataReader = this.DataAcessService.ExecuteQuery(StateSql["GetStateForCountryCode"], paramCollection);

                if (dataReader != null && dataReader.HasRows)
                {
                    int countrycodeOrdinal = dataReader.GetOrdinal("country_code");
                    int stateabbriOrdinal = dataReader.GetOrdinal("state_abbri");
                    int statecodeOrdinal = dataReader.GetOrdinal("state_code");
                    int statenameOrdinal = dataReader.GetOrdinal("state_name");

                    if (dataReader.Read())
                    {
                         state= CreateObject();
                         if (!dataReader.IsDBNull(countrycodeOrdinal)) state.CountryCode = dataReader.GetString(countrycodeOrdinal);
                         if (!dataReader.IsDBNull(stateabbriOrdinal)) state.StateAbbri = dataReader.GetString(stateabbriOrdinal);
                         if (!dataReader.IsDBNull(statecodeOrdinal)) state.StateCode = dataReader.GetString(statecodeOrdinal);
                         if (!dataReader.IsDBNull(statenameOrdinal)) state.StateName = dataReader.GetString(statenameOrdinal);
                    }
                }
                return state;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
    }
}
