﻿using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SyncDAO
    {
        public List<SalesRepSyncModel> GetAllSalesRepSyncData(ArgsModel args, string OriginatorType, string Originator)
        {
            SalesRepSyncModel objModel = null;
            DataTable objDS = new DataTable();
            List<SalesRepSyncModel> objList = new List<SalesRepSyncModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesRepSyncData";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new SalesRepSyncModel();

                            objModel.IsRepSync = false;
                            if (item["rep_id"] != DBNull.Value) objModel.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["rep_code"] != DBNull.Value) objModel.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objModel.RepName = item["rep_name"].ToString();
                            if (item["emei_no"] != DBNull.Value) objModel.EmeiNo = item["emei_no"].ToString();
                            if (item["device_token"] != DBNull.Value) objModel.DeviceToken = item["device_token"].ToString();
                            if (item["asm"] != DBNull.Value) objModel.AsmName = item["asm"].ToString();
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["delivery_status"] != DBNull.Value) objModel.DeliveryStatus = (item["delivery_status"].ToString() == "A") ? true : false;
                            if (item["is_synced"] != DBNull.Value) objModel.IsSynced = Convert.ToBoolean(item["is_synced"]);
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objList = new List<SalesRepSyncModel>();
            }

            return objList;
        }
        
        public List<SalesRepSyncModel> GetAllSalesRepSyncDataByIMEI(ArgsModel args, string OriginatorType, string Originator, string DeviceIMEI)
        {
            SalesRepSyncModel objModel = null;
            DataTable objDS = new DataTable();
            List<SalesRepSyncModel> objList = new List<SalesRepSyncModel>();

            string IMEIList = "'" + DeviceIMEI.Substring(0, DeviceIMEI.Length-1).Replace(",", "','") + "'";

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSalesRepSyncDataByIMEI";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);
                    objCommand.Parameters.AddWithValue("@DeviceIMEI", IMEIList);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new SalesRepSyncModel();

                            objModel.IsRepSync = false;
                            if (item["rep_id"] != DBNull.Value) objModel.RepId = Convert.ToInt32(item["rep_id"]);
                            if (item["rep_code"] != DBNull.Value) objModel.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objModel.RepName = item["rep_name"].ToString();
                            if (item["emei_no"] != DBNull.Value) objModel.EmeiNo = item["emei_no"].ToString();
                            if (item["device_token"] != DBNull.Value) objModel.DeviceToken = item["device_token"].ToString();
                            if (item["asm"] != DBNull.Value) objModel.AsmName = item["asm"].ToString();
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["delivery_status"] != DBNull.Value) objModel.DeliveryStatus = (item["delivery_status"].ToString() == "A") ? true : false;
                            if (item["is_synced"] != DBNull.Value) objModel.IsSynced = Convert.ToBoolean(item["is_synced"]);
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objList = new List<SalesRepSyncModel>();
            }

            return objList;
        }
        
        public bool UpdateRepIMEISyncStatus(string originator, string originatorType, string DeviceIMEI)
        {
            bool successful = false;
            int iNoRec = 0;

            string IMEIList = "'" + DeviceIMEI.Substring(0, DeviceIMEI.Length - 1).Replace(",", "','") + "'";

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateRepIMEISyncStatus";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@OriginatorType", originatorType);
                    objCommand.Parameters.AddWithValue("@DeviceIMEI", IMEIList);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public bool UpdateRepSyncData(string originator, string originatorType)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteAsmFromAreaByAsmId";

                    //objCommand.Parameters.AddWithValue("@AsmId", asmId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }
        
        public bool UpdateFCMDeliveryStatus(string RepCode, string IMEINo, string status)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateFCMDeliveryStatus";

                    objCommand.Parameters.AddWithValue("@RepCode", RepCode);
                    objCommand.Parameters.AddWithValue("@IMEINo", IMEINo);
                    objCommand.Parameters.AddWithValue("@Status", status);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public int InserNewSyncMaster(string originator, string originatorType)
        {
            int retVal = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InserNewSyncMaster";

                    objCommand.Parameters.AddWithValue("@CreatedBy", originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    retVal = int.Parse(objCommand.ExecuteScalar().ToString());

                    //if (iNoRec > 0)
                    //    successful = true;
                }
            }
            catch (Exception ex)
            {
                retVal = 0;
            }

            return retVal;
        }
        
        public bool InsertSRSyncDetail(string originator, SalesRepSyncDetailModel srSyncDet)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertSRSyncDetail"; 

                    objCommand.Parameters.AddWithValue("@originator", originator);
                    objCommand.Parameters.AddWithValue("@syncId", srSyncDet.syncId);
                    objCommand.Parameters.AddWithValue("@emeiNo", srSyncDet.emeiNo);
                    objCommand.Parameters.AddWithValue("@mobileSyncTime", srSyncDet.mobileSyncTime);
                    objCommand.Parameters.AddWithValue("@isAddCust", srSyncDet.isAddCust);
                    objCommand.Parameters.AddWithValue("@descAddCust", srSyncDet.descAddCust);
                    objCommand.Parameters.AddWithValue("@isAddInv", srSyncDet.isAddInv);
                    objCommand.Parameters.AddWithValue("@descAddInv", srSyncDet.descAddInv);
                    objCommand.Parameters.AddWithValue("@isAddVn", srSyncDet.isAddVn);
                    objCommand.Parameters.AddWithValue("@descAddVn", srSyncDet.descAddVn);
                    objCommand.Parameters.AddWithValue("@isAddChkLst", srSyncDet.isAddChkLst);
                    objCommand.Parameters.AddWithValue("@descChkLst", srSyncDet.descChkLst);
                    objCommand.Parameters.AddWithValue("@isAddOrder", srSyncDet.isAddOrder);
                    objCommand.Parameters.AddWithValue("@descOrder", srSyncDet.descOrder);
                    objCommand.Parameters.AddWithValue("@isAddCredSettle", srSyncDet.isAddCredSettle);
                    objCommand.Parameters.AddWithValue("@descCredSettle", srSyncDet.descCredSettle);
                    objCommand.Parameters.AddWithValue("@territoryId", srSyncDet.territoryId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = int.Parse(objCommand.ExecuteScalar().ToString());

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }
    }
}
