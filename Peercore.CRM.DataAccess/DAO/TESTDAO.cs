﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class TESTDAO:BaseDAO
    {
        private DbSqlAdapter BrandSql { get; set; }

        private void RegisterSql()
        {
            this.BrandSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.BrandSql.xml", ApplicationService.Instance.DbProvider);
        }

        public TESTDAO()
        {
            RegisterSql();
        }

        public TESTDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public BrandEntity CreateObject()
        {
            return BrandEntity.CreateObject();
        }

        public BrandEntity CreateObject(int entityId)
        {
            return BrandEntity.CreateObject(entityId);
        }

        


        public List<BrandECOModel> GetBrandECOCumulativeByAccessToken(string accessToken,string strDate, string endDate)
        {
            BrandECOModel brandEntity = null;
            DataTable objDS = new DataTable();
            List<BrandECOModel> BrandList = new List<BrandECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetBrandECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            brandEntity = new BrandECOModel();

                            if (item["BrnNm"] != DBNull.Value) brandEntity.BrandName = item["BrnNm"].ToString();

                            //if (item["ProNm"] != DBNull.Value) brandEntity.ProductName = item["ProNm"].ToString();

                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    brandEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    brandEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    brandEntity.isPC = false; // Or set a default value
                                }
                            }






                            BrandList.Add(brandEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return BrandList;
        }




        public List<ProductECOModel> GetProductECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            ProductECOModel productEntity = null;
            DataTable objDS = new DataTable();
            List<ProductECOModel> ProductList = new List<ProductECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetProductECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            productEntity = new ProductECOModel();

                            if (item["ProNm"] != DBNull.Value) productEntity.ProductName = item["ProNm"].ToString();

                            if (item["ProCd"] != DBNull.Value) productEntity.ProductCode = item["ProCd"].ToString();
                            if (item["FgCd"] != DBNull.Value) productEntity.ProductFGCode = item["FgCd"].ToString();


                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    productEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    productEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    productEntity.isPC = false; // Or set a default value
                                }
                            }






                            ProductList.Add(productEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return ProductList;
        }












        public List<CustomerECOModel> GetCustomerECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            CustomerECOModel customerEntity = null;
            DataTable objDS = new DataTable();
            List<CustomerECOModel> CustomerList = new List<CustomerECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetCustomerECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            customerEntity = new CustomerECOModel();

                            if (item["CustomerCode"] != DBNull.Value) customerEntity.CustomerCode = item["CustomerCode"].ToString();

                            if (item["CustomerName"] != DBNull.Value) customerEntity.CustomerName = item["CustomerName"].ToString();

                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    customerEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    customerEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    customerEntity.isPC = false; // Or set a default value
                                }
                            }






                            CustomerList.Add(customerEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return CustomerList;
        }





        public List<CategoryECOModel> GetCategoryECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            CategoryECOModel categoryEntity = null;
            DataTable objDS = new DataTable();
            List<CategoryECOModel> CategoryList = new List<CategoryECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetCategoryECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            categoryEntity = new CategoryECOModel();

                            if (item["CatCd"] != DBNull.Value) categoryEntity.CategoryCode = item["CatCd"].ToString();

                            if (item["CatNm"] != DBNull.Value) categoryEntity.CategoryName = item["CatNm"].ToString();

                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    categoryEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    categoryEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    categoryEntity.isPC = false; // Or set a default value
                                }
                            }






                            CategoryList.Add(categoryEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return CategoryList;
        }




















    }
}
