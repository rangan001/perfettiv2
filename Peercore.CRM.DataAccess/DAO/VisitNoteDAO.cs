﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using Peercore.CRM.Shared;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class VisitNoteDAO:BaseDAO
    {
        private DbSqlAdapter VisitNoteSql { get; set; }

        private void RegisterSql()
        {
            this.VisitNoteSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.VisitNoteSql.xml", ApplicationService.Instance.DbProvider);
        }

        public VisitNoteDAO()
        {
            RegisterSql();
        }

        public VisitNoteDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public VisitNoteEntity CreateObject()
        {
            return VisitNoteEntity.CreateObject();
        }

        public VisitNoteEntity CreateObject(int entityId)
        {
            return VisitNoteEntity.CreateObject(entityId);
        }

        public List<VisitNoteEntity> GetVisitNote(int routeMasterId)
        {
            DbDataReader drObjective = null;

            try
            {
                List<VisitNoteEntity> objectiveList = new List<VisitNoteEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@routeMasterId", routeMasterId, DbType.Int32);

                drObjective = this.DataAcessService.ExecuteQuery(VisitNoteSql["GetVisitNote"], paramCollection);

                if (drObjective != null && drObjective.HasRows)
                {
                    int custCodeOrdinal = drObjective.GetOrdinal("cust_code");
                    int nameOrdinal = drObjective.GetOrdinal("name");
                    int addr1Ordinal = drObjective.GetOrdinal("address_1");
                    int addr2Ordinal = drObjective.GetOrdinal("address_2");
                    int remarksOrdinal = drObjective.GetOrdinal("remarks");
                    int notesOrdinal = drObjective.GetOrdinal("notes");
                    int isCheckNoteOrdinal = drObjective.GetOrdinal("is_check_note");

                    while (drObjective.Read())
                    {
                        VisitNoteEntity objectiveEntity = CreateObject();

                        if (!drObjective.IsDBNull(custCodeOrdinal)) objectiveEntity.CustomerCode = drObjective.GetString(custCodeOrdinal);
                        if (!drObjective.IsDBNull(nameOrdinal)) objectiveEntity.CustomerName = drObjective.GetString(nameOrdinal);
                        if (!drObjective.IsDBNull(addr1Ordinal)) objectiveEntity.CustomerAddress = drObjective.GetString(addr1Ordinal);
                        if (!drObjective.IsDBNull(addr2Ordinal)) objectiveEntity.CustomerAddress = objectiveEntity.CustomerAddress + ", " + drObjective.GetString(addr2Ordinal);
                        if (!drObjective.IsDBNull(remarksOrdinal)) objectiveEntity.Remarks = drObjective.GetString(remarksOrdinal);
                        if (!drObjective.IsDBNull(notesOrdinal)) objectiveEntity.Notes = drObjective.GetString(notesOrdinal);
                        if (!drObjective.IsDBNull(isCheckNoteOrdinal)) objectiveEntity.IsCheckNote = drObjective.GetBoolean(isCheckNoteOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }

        public List<VisitNoteEntity> GetVisitNotesByAccessTokenforASM(string accessToken, string date)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<VisitNoteEntity> InvoiceHeaderList = new List<VisitNoteEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);
                paramCollection.Add("@date", date, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(VisitNoteSql["GetVisitNotesByAccessTokenforASM"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int visitIdOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int custNameOrdinal = drInvoiceHeader.GetOrdinal("name");
                    int visitDateOrdinal = drInvoiceHeader.GetOrdinal("visit_date");
                    int notesOrdinal = drInvoiceHeader.GetOrdinal("notes");
                    int latitudeOrdinal = drInvoiceHeader.GetOrdinal("latitude");
                    int longitudeOrdinal = drInvoiceHeader.GetOrdinal("longitude");

                    VisitNoteEntity invoiceHeaderEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        try
                        {
                            invoiceHeaderEntity = CreateObject();
                            if (!drInvoiceHeader.IsDBNull(visitIdOrdinal)) invoiceHeaderEntity.VisitNoteId = drInvoiceHeader.GetInt32(visitIdOrdinal);
                            if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceHeaderEntity.CustomerCode = drInvoiceHeader.GetString(custCodeOrdinal);
                            if (!drInvoiceHeader.IsDBNull(custNameOrdinal)) invoiceHeaderEntity.CustomerName = drInvoiceHeader.GetString(custNameOrdinal);
                            if (!drInvoiceHeader.IsDBNull(visitDateOrdinal)) invoiceHeaderEntity.VisitDate = drInvoiceHeader.GetString(visitDateOrdinal);
                            if (!drInvoiceHeader.IsDBNull(notesOrdinal)) invoiceHeaderEntity.Notes = drInvoiceHeader.GetString(notesOrdinal);
                            
                            try
                            {
                                if (!drInvoiceHeader.IsDBNull(latitudeOrdinal)) invoiceHeaderEntity.Latitude = double.Parse(drInvoiceHeader.GetString(latitudeOrdinal));
                            }
                            catch { invoiceHeaderEntity.Latitude = 0; }

                            try
                            {
                                if (!drInvoiceHeader.IsDBNull(longitudeOrdinal)) invoiceHeaderEntity.Longitude = double.Parse(drInvoiceHeader.GetString(longitudeOrdinal));
                            }
                            catch { invoiceHeaderEntity.Longitude = 0; }

                            InvoiceHeaderList.Add(invoiceHeaderEntity);
                        }
                        catch { }
                    }
                }

                return InvoiceHeaderList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public List<VisitNoteEntity> GetVisitNotesByAccessToken(string accessToken)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<VisitNoteEntity> InvoiceHeaderList = new List<VisitNoteEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(VisitNoteSql["GetVisitNotesByAccessToken"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int visitIdOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int custNameOrdinal = drInvoiceHeader.GetOrdinal("name");
                    int visitDateOrdinal = drInvoiceHeader.GetOrdinal("visit_date");
                    int notesOrdinal = drInvoiceHeader.GetOrdinal("notes");
                    int latitudeOrdinal = drInvoiceHeader.GetOrdinal("latitude");
                    int longitudeOrdinal = drInvoiceHeader.GetOrdinal("longitude");

                    VisitNoteEntity invoiceHeaderEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        try
                        {
                            invoiceHeaderEntity = CreateObject();
                            if (!drInvoiceHeader.IsDBNull(visitIdOrdinal)) invoiceHeaderEntity.VisitNoteId = drInvoiceHeader.GetInt32(visitIdOrdinal);
                            if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceHeaderEntity.CustomerCode = drInvoiceHeader.GetString(custCodeOrdinal);
                            if (!drInvoiceHeader.IsDBNull(custNameOrdinal)) invoiceHeaderEntity.CustomerName = drInvoiceHeader.GetString(custNameOrdinal);
                            if (!drInvoiceHeader.IsDBNull(visitDateOrdinal)) invoiceHeaderEntity.VisitDate = drInvoiceHeader.GetDateTime(visitDateOrdinal).ToString("yyyy-MM-dd HH:mm");
                            if (!drInvoiceHeader.IsDBNull(notesOrdinal)) invoiceHeaderEntity.Notes = drInvoiceHeader.GetString(notesOrdinal);

                            try
                            {
                                if (!drInvoiceHeader.IsDBNull(latitudeOrdinal)) invoiceHeaderEntity.Latitude = double.Parse(drInvoiceHeader.GetString(latitudeOrdinal));
                            }
                            catch { invoiceHeaderEntity.Latitude = 0; }

                            try
                            {
                                if (!drInvoiceHeader.IsDBNull(longitudeOrdinal)) invoiceHeaderEntity.Longitude = double.Parse(drInvoiceHeader.GetString(longitudeOrdinal));
                            }
                            catch { invoiceHeaderEntity.Longitude = 0; }

                            InvoiceHeaderList.Add(invoiceHeaderEntity);
                        }
                        catch { }
                    }
                }

                return InvoiceHeaderList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public bool InsertVisitNote(string accessToken ,VisitNoteEntity visitNoteEntity)
        {
            //bool successful = false;
            //int iNoRec = 0;

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@CustCode", visitNoteEntity.CustomerCode, DbType.String);
            //    paramCollection.Add("@ActivityId", visitNoteEntity.ActivityId, DbType.Int32);
            //    paramCollection.Add("@Notes", visitNoteEntity.Notes, DbType.String);
            //    paramCollection.Add("@VisitDate", visitNoteEntity.VisitDate, DbType.DateTime);
            //    paramCollection.Add("@RepCode", visitNoteEntity.RepCode, DbType.String);
            //    paramCollection.Add("@Latitude", visitNoteEntity.Latitude, DbType.String);
            //    paramCollection.Add("@Longitude", visitNoteEntity.Longitude, DbType.String);
            //    paramCollection.Add("@RouteId", visitNoteEntity.RouteMasterId, DbType.String);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(VisitNoteSql["InsertVisitNote"], paramCollection);

            //    if (iNoRec > 0)
            //        successful = true;

            //    return successful;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{

            //}

            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertVisitNote";

                    objCommand.Parameters.AddWithValue("@CustCode", visitNoteEntity.CustomerCode);
                    objCommand.Parameters.AddWithValue("@ActivityId", visitNoteEntity.ActivityId);
                    objCommand.Parameters.AddWithValue("@Notes", visitNoteEntity.Notes);
                    objCommand.Parameters.AddWithValue("@VisitDate", visitNoteEntity.VisitDate);
                    objCommand.Parameters.AddWithValue("@RepCode", visitNoteEntity.RepCode);
                    objCommand.Parameters.AddWithValue("@Latitude", visitNoteEntity.Latitude);
                    objCommand.Parameters.AddWithValue("@Longitude", visitNoteEntity.Longitude);
                    objCommand.Parameters.AddWithValue("@RouteId", visitNoteEntity.RouteMasterId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public List<VisitNoteReasonEntity> GetVisitNoteReason()
        {
            DbDataReader drObjective = null;

            try
            {
                List<VisitNoteReasonEntity> objectiveList = new List<VisitNoteReasonEntity>();



                drObjective = this.DataAcessService.ExecuteQuery(VisitNoteSql["GetVisitNoteReason"]);

                if (drObjective != null && drObjective.HasRows)
                {
                    int reasonIdOrdinal = drObjective.GetOrdinal("visit_note_reason_id");
                    int reasonOrdinal = drObjective.GetOrdinal("reason");

                    while (drObjective.Read())
                    {
                        VisitNoteReasonEntity objectiveEntity = VisitNoteReasonEntity.CreateObject();

                        if (!drObjective.IsDBNull(reasonIdOrdinal)) objectiveEntity.VisitNoteReasonID = drObjective.GetInt32(reasonIdOrdinal);
                        if (!drObjective.IsDBNull(reasonOrdinal)) objectiveEntity.Reason = drObjective.GetString(reasonOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }
    }
}
