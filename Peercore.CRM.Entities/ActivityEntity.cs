﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    [Serializable]
    public class ActivityEntity : BaseEntity
    {
        private ActivityEntity()
        {
            this.IsNew = true;
        }
        
        private ActivityEntity(int entityId)
        {
            this.ActivityID = entityId;
        }

        public static ActivityEntity CreateObject()
        {
            ActivityEntity entity = new ActivityEntity();

            return entity;
        }

        public static ActivityEntity CreateObject(int entityId)
        {
            ActivityEntity entity = new ActivityEntity(entityId);

            return entity;
        }

        public int ActivityID { get; set; }
        public string Subject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public string AssignedTo { get; set; }
        public int RepGroupID { get; set; }
        // NOT IN TABLE
        public string RepGroupName { get; set; }

        public int LeadID { get; set; }
        public string Comments { get; set; }
        public string SentMail { get; set; }
        public string Priority { get; set; }
        public string PriorityDescription { get; set; }
        public string SendReminder { get; set; }
        public DateTime ReminderDate { get; set; }
        // NOT IN TABLE - To check whether a reminder activity is alreay created
        public bool ReminderCreated { get; set; }

        public int AppointmentID { get; set; }
        public int ParentActivityID { get; set; }

        public int LeadStageID { get; set; }
        public string ActivityType { get; set; }
        public string ActivityTypeDescription { get; set; }
        public string CustomerCode { get; set; }
        public string EndUserCode { get; set; }
        public int CallCycleID {get; set;}

        //
       // public int ActivityCount { get; set; }

        /* Not in Table
         * ONLY FOR SERVICE CALLS - to check whether checklist is saved */
        public bool HasChecklist { get; set; }
        public int ChecklistID { get; set; }
        public bool IsServiceCall { get; set; }

        public string RepCode { get; set; }
        public int RouteID { get; set; }

        //public int RowCount { get; set; }


        #region - Activity Types -

        /*private string code;
        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                if (code == value)
                    return;

                code = value;
                IsDirty = true;
            }
        }

        private string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                if (description == value)
                    return;

                description = value;
                IsDirty = true;
            }
        }

        private string emailAddresses;
        public string EmailAddresses
        {
            get
            {
                return emailAddresses;
            }
            set
            {
                if (emailAddresses == value)
                    return;

                emailAddresses = value;
                IsDirty = true;
            }
        }

        private string emailTemplate;
        public string EmailTemplate
        {
            get
            {
                return emailTemplate;
            }
            set
            {
                if (emailTemplate == value)
                    return;

                emailTemplate = value;
                IsDirty = true;
            }
        }

        private string showInPlanner;
        public string ShowInPlanner
        {
            get
            {
                return showInPlanner;
            }
            set
            {
                if (showInPlanner == value)
                    return;

                showInPlanner = value;
                IsDirty = true;
            }
        }

        private string activityTypeColour;
        public string ActivityTypeColour
        {
            get
            {
                return activityTypeColour;
            }
            set
            {
                if (activityTypeColour == value)
                    return;

                activityTypeColour = value;
                IsDirty = true;
            }
        }

        private bool showInPlannerChecked;
        public bool ShowInPlannerChecked
        {
            get
            {
                if (ShowInPlanner == "Y")
                    return true;
                else
                    return false;
            }
            set
            {
                IsDirty = true;
            }
        }
        */
        #endregion - Activity Types -
    }
}
