﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    [Serializable]
    public class ActivityTypeEntity : BaseEntity
    {
        public ActivityTypeEntity()
        {
            //this.ActivityType = 0;
            this.IsNew = true;
        }

        public ActivityTypeEntity(int entityId)
        {
           // this.ActivityType = entityId;
        }

        public static ActivityTypeEntity CreateObject()
        {
            ActivityTypeEntity entity = new ActivityTypeEntity();

            return entity;
        }

        public static ActivityTypeEntity CreateObject(int entityId)
        {
            ActivityTypeEntity entity = new ActivityTypeEntity(entityId);

            return entity;
        }

        public string Code { get; set; }
        public string Description { get; set; }
        public string EmailAddresses { get; set; }
        public string EmailTemplate { get; set; }
        public string ActivityTypeColour { get; set; }
        public string SetAsDefault { get; set; }
        public string ColdCallActivity { get; set; }
        public bool SetAsDefaultChecked
        {
            get
            {
                if (SetAsDefault == "Y")
                    return true;
                else
                    return false;
            }
        }

        public string ShowInPlanner { get; set; }
        public bool ColdCallActivityChecked
        {
            get
            {
                if (ColdCallActivity == "Y")
                    return true;
                else
                    return false;
            }
        }
        public bool ShowInPlannerChecked
        {
            get
            {
                if (ShowInPlanner == "Y")
                    return true;
                else
                    return false;
            }
        }
    }
}
