﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ActivityViewEntity : BaseEntity
    {
        public int ActivityID { get; set; }
        public string Subject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public string AssignedTo { get; set; }
        public int LeadID { get; set; }
        public string Comments { get; set; }
        public string SentMail { get; set; }
        public string Priority { get; set; }
        public string PriorityDescription { get; set; }
        public string SendReminder { get; set; }
        public DateTime ReminderDate { get; set; }
        public int PipelineStageID { get; set; }
        public string ActivityType { get; set; }
        public string ActivityTypeDescription { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        public string CustomerCode { get; set; }
        public int Count { get; set; }
    }
}
