﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class AppointmentEntity : BaseEntity
    {
        public AppointmentEntity()
        {
            this.AppointmentID = 0;
            this.IsNew = true;
        }

        public AppointmentEntity(int entityId)
        {
            this.AppointmentID = entityId;
        }

        public static AppointmentEntity CreateObject()
        {
            AppointmentEntity entity = new AppointmentEntity();

            return entity;
        }

        public static AppointmentEntity CreateObject(int entityId)
        {
            AppointmentEntity entity = new AppointmentEntity(entityId);

            return entity;
        }

        private int _AppointmentID;
        public int AppointmentID
        {
            get { return _AppointmentID; }
            set { _AppointmentID = value; }
        }

        private string _Subject;
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }

        private string _Body;
        public string Body
        {
            get { return _Body; }
            set { _Body = value; }
        }

        private DateTime _StartTime;
        public DateTime StartTime
        {
            get { return _StartTime; }
            set { _StartTime = value; }
        }

        private DateTime _EndTime;
        public DateTime EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }

        private int _AllDayEvent;
        public int AllDayEvent
        {
            get { return _AllDayEvent; }
            set { _AllDayEvent = value; }
        }

        private string _Location;
        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        private string _URL;
        public string URL
        {
            get { return _URL; }
            set { _URL = value; }
        }

        private int _Type;
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private string _RecurrencePattern;
        public string RecurrencePattern
        {
            get { return _RecurrencePattern; }
            set { _RecurrencePattern = value; }
        }

        private string _TimeZoneString;
        public string TimeZoneString
        {
            get { return _TimeZoneString; }
            set { _TimeZoneString = value; }
        }

        private string _Developer;
        public string Developer
        {
            get { return _Developer; }
            set { _Developer = value; }
        }

        private string _ProductLine;
        public string ProductLine
        {
            get { return _ProductLine; }
            set { _ProductLine = value; }
        }

        private string _ExceptionAppointments;
        public string ExceptionAppointments
        {
            get { return _ExceptionAppointments; }
            set { _ExceptionAppointments = value; }
        }

        private string _Importance;
        public string Importance
        {
            get { return _Importance; }
            set { _Importance = value; }
        }

        private string _TimeMaker;
        public string TimeMaker
        {
            get { return _TimeMaker; }
            set { _TimeMaker = value; }
        }

        private string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _TypeDescription;
        public string TypeDescription
        {
            get { return _TypeDescription; }
            set { _TypeDescription = value; }
        }

        private string _RepCode;
        public string RepCode
        {
            get { return _RepCode; }
            set { _RepCode = value; }
        }

        private string _LeadStage;
        public string LeadStage
        {
            get { return _LeadStage; }
            set { _LeadStage = value; }
        }

        private int _ActivityId;
        public int ActivityId
        {
            get { return _ActivityId; }
            set { _ActivityId = value; }
        }

        private int _LeadId;
        public int LeadId
        {
            get { return _LeadId; }
            set { _LeadId = value; }
        }

        private string _CustomerCode;
        public string CustomerCode
        {
            get { return _CustomerCode; }
            set { _CustomerCode = value; }
        }

        private string _EnduserCode;
        public string EnduserCode
        {
            get { return _EnduserCode; }
            set { _EnduserCode = value; }
        }
    }
}
