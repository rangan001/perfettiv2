﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Peercore.CRM.Entities
{
    public class AreaEntity:BaseEntity
    {
        public AreaEntity()
        {
        }

        public AreaEntity(int entityId)
        {
        }

        public static AreaEntity CreateObject()
        {
            AreaEntity entity = new AreaEntity();

            return entity;
        }

        public static AreaEntity CreateObject(int entityId)
        {
            AreaEntity entity = new AreaEntity(entityId);

            return entity;
        }
        public string Postcode { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Area { get; set; }
        public string CustCity { get; set; }
    }
}
