﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class AuthorizationGroupEntity : BaseEntity
    {
        private AuthorizationGroupEntity()
        {
            this.IsNew = true;
        }

        private AuthorizationGroupEntity(int entityId)
        {
            this.AuthorizationGroupId = entityId;
        }

        public static AuthorizationGroupEntity CreateObject()
        {
            AuthorizationGroupEntity entity = new AuthorizationGroupEntity();
            return entity;
        }

        public static AuthorizationGroupEntity CreateObject(int entityId)
        {
            AuthorizationGroupEntity entity = new AuthorizationGroupEntity(entityId);
            return entity;
        }

        private int? authorizationGroupId;
        public int? AuthorizationGroupId
        {
            get { return authorizationGroupId; }
            set
            {
                authorizationGroupId = value;
                IsDirty = true;
            }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                IsDirty = true;
            }
        }

        private bool isAdministrator;
        public bool IsAdministrator
        {
            get { return isAdministrator; }
            set
            {
                isAdministrator = value;
                IsDirty = true;
            }
        }

        private List<GroupPermissionEntity> groupPermissionList;
        public List<GroupPermissionEntity> GroupPermissionList
        {
            get { return groupPermissionList; }
            set { groupPermissionList = value; }
        }
    }
}
