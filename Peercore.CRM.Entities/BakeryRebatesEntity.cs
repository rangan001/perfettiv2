﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class BakeryRebatesEntity : BaseEntity
    {
        public BakeryRebatesEntity()
        {
        }
        
        public static BakeryRebatesEntity CreateObject()
        {
            BakeryRebatesEntity entity = new BakeryRebatesEntity();
            return entity;
        }

        public int DocNo { get; set; }
        public string CustCode { get; set; }
        public string EnduserCode { get; set; }
        public DateTime RebateMonth { get; set; }
        public string CustCity { get; set; }

        public string Catlogcode { get; set; }
        public string Description { get; set; }
        public double Price{ get; set; }
        public double CustomerPrice{ get; set; }
        public double CasesSupplied{ get; set; }
        public double PriceImp{ get; set; }
	    public double CustomerPriceImp{ get; set; }
        public string ImportFlag { get; set; }
    }
}
