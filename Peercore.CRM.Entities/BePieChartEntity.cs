﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class BePieChartEntity : BaseEntity
    {
        private BePieChartEntity()
        {
            this.IsNew = true;
        }

        private BePieChartEntity(int entityId)
        {
        }

        public static BePieChartEntity CreateObject()
        {
            BePieChartEntity entity = new BePieChartEntity();

            return entity;
        }
        
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public double Value { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
    }
}
