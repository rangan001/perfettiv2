﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CallCycleContactEntity : BaseEntity
    {
        public CallCycleContactEntity()
        {
            this.IsNew = true;
        }

        private CallCycleContactEntity(int entityId)
        {
            //this.ActivityID = entityId;
        }

        public static CallCycleContactEntity CreateObject()
        {
            CallCycleContactEntity entity = new CallCycleContactEntity();

            return entity;
        }

        public static CallCycleContactEntity CreateObject(int entityId)
        {
            CallCycleContactEntity entity = new CallCycleContactEntity(entityId);

            return entity;
        }

        public int ItemIndex {get;set;}
        private int weekDayId = 0;
        private int routeId = 0;
        private CallCycleEntity callCycle = null;
        private LeadCustomerViewEntity contact = null;
        private LeadStageEntity leadStage = null;
        private string prefixCode;
        private DateTime dueOn = DateTime.Today;
        private TimeSpan startTime = TimeSpan.FromHours(5);
        private bool createActivity = true;
        private int dayOrder = 0;

        /// <summary>
        /// Gets or sets the call cycle.
        /// </summary>
        /// <value>The call cycle.</value>
        public CallCycleEntity CallCycle
        {
            get
            {
                if (callCycle == null)
                {
                    callCycle = CallCycleEntity.CreateObject();
                }

                return callCycle;
            }
            set
            {
                callCycle = value;
            }
        }

        /// <summary>
        /// Gets or sets the contact.
        /// </summary>
        /// <value>The contact.</value>
        public LeadCustomerViewEntity Contact
        {
            get
            {
                if (contact == null)
                {
                    contact = LeadCustomerViewEntity.CreateObject();
                }

                return contact;
            }
            set
            {
                contact = value;
            }
        }

        /// <summary>
        /// Gets or sets the originator.
        /// </summary>
        /// <value>The originator.</value>
        public string Originator { get; set; }

        /// <summary>
        /// Added By Darshaka
        /// Gets or sets the lead stage.
        /// </summary>
        /// <value>The lead stage.</value>
        public LeadStageEntity LeadStage
        {
            get
            {
                if (leadStage == null)
                {
                    leadStage = LeadStageEntity.CreateObject();
                }

                return leadStage;
            }
            set
            {
                leadStage = value;
            }
        }

        public int WeekDayId
        {
            get { return weekDayId; }
            set { weekDayId = value; }
        }

        public DateTime DueOn
        {
            get { return dueOn; }
            set { dueOn = value; }
        }

        public TimeSpan StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        public bool CreateActivity
        {
            get { return createActivity; }
            set { createActivity = value; }
        }

        public string PrefixCode
        {
            get { return prefixCode; }
            set { prefixCode = value; }
        }

        public int DayOrder
        {
            get { return dayOrder; }
            set { dayOrder = value; }
        }

        public int RouteId
        {
            get { return routeId; }
            set { routeId = value; }
        }
    }
}
