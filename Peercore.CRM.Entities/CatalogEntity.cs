﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CatalogEntity : BaseEntity
    {
        private CatalogEntity()
        {
            this.IsNew = true;
        }

        private CatalogEntity(int entityId)
        {
            //this.CallCycleID = entityId;
        }

        public static CatalogEntity CreateObject()
        {
            CatalogEntity entity = new CatalogEntity();
            return entity;
        }

        public static CatalogEntity CreateObject(int entityId)
        {
            CatalogEntity entity = new CatalogEntity(entityId);
            return entity;
        }

        public int CatlogId { get; set; }
        public string CatlogCode { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public DateTime? NextDue { get; set; }
        public String DeliveryFrequency { get; set; }
        public int OpportunityID { get; set; }
        public double Conversion { get; set; }


        public string ShortDesc { get; set; }
        public string CatlogType { get; set; }
        public string Category { get; set; }
        public double Cost { get; set; }
        public int LeadTime { get; set; }
        public int GstStatus { get; set; }
        public double GstRate { get; set; }
        public string UomOrder { get; set; }
        public string UomReceive { get; set; }
        public string UomStock { get; set; }
        public string UomIssue { get; set; }
        public string UomPallet { get; set; }
        public string GlLedger { get; set; }
        public string GlAccount { get; set; }
        public string GlNarration { get; set; }
        public string PalletType { get; set; }
        public double PalletWeight { get; set; }
        public string SuppCode { get; set; }
        public string ManCode { get; set; }
        public string PartNumber { get; set; }
        public string Brand { get; set; }
        public string Market { get; set; }
        public string BusArea { get; set; }
        public int FeedBanMsg { get; set; }
        public string BarCode { get; set; }
        public string TunCode { get; set; }
        public int UbdDuration { get; set; }
        public int ExportUbdDuration { get; set; }
        public int AheccNo { get; set; }
        public string GlAccountNo { get; set; }
        public string DelFlag { get; set; }
        public string Status { get; set; }
        public int Version { get; set; }
        public string BrowseAvailable { get; set; }
        public string SellingCode { get; set; }
        public string BaseCode { get; set; }
        public int LblCount { get; set; }
        public int LblCopiesPeer { get; set; }
        public int UnitsPerPallet { get; set; }
        public int LayersPerPallet { get; set; }
        public double TimeToMature { get; set; }
        public string DateDisplay { get; set; }
        public string UsebyOpt { get; set; }
        public string ExpusebyOpt { get; set; }
        public string CatSubGroup { get; set; }
        public string CatGroup { get; set; }
        public string ProdCat { get; set; }
        public string BulkBinType { get; set; }
        public string RoomType { get; set; }
        public string BoxCoding { get; set; }
        public string OwnerCode { get; set; }
        public string UnitCoding { get; set; }
        public int KitFlag { get; set; }
        public string KitWarehouse { get; set; }
        public int Exported { get; set; }
        public double MaxWaitingHrs { get; set; }
        public double HotRoomHrs { get; set; }
        public double TempRoomHrs { get; set; }
        public int ReminderAfterWaitingHrs { get; set; }
        public double Stage1RoomHrs { get; set; }
        public int ReminderAfterStage1 { get; set; }
        public int LabApprovalAfterStage1 { get; set; }
        public string RoomTypeStage2 { get; set; }
        public double Stage2RoomHrs { get; set; }
        public string OptionalRoomType { get; set; }
        public int DispInCrm { get; set; }
        public string PattiesCode { get; set; }
        public string SafetyStockMethod { get; set; }
        public int SafetyStockWeeks { get; set; }
        public int ForecastHorizon { get; set; }
        public int BulkColour { get; set; }



        public double Units { get; set; }
        public int ShelfLife { get; set; }
        public string PriceGroup { get; set; }
        public int TransportChilled { get; set; }
        public int QrHoldDays { get; set; }
        public string Uom { get; set; }

    }
}
