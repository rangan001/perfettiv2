﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CatalogGroupDescEntity :BaseEntity
    {
        public CatalogGroupDescEntity()
        {
        }

        public static CatalogGroupDescEntity CreateObject()
        {
            CatalogGroupDescEntity entity = new CatalogGroupDescEntity();
            return entity;
        }

        public string CatGrouping { get; set; }
        public string GroupType { get; set; }
        public string Description { get; set; }
    }
}
