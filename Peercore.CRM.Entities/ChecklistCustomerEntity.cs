﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ChecklistCustomerEntity : BaseEntity
    {

                private ChecklistCustomerEntity()
        {
            this.IsNew = true;
        }

                private ChecklistCustomerEntity(int entityId)
        {
            this.CheckListID = entityId;
        }

                public static ChecklistCustomerEntity CreateObject()
        {
            ChecklistCustomerEntity entity = new ChecklistCustomerEntity();
            return entity;
        }

                public static ChecklistCustomerEntity CreateObject(int entityId)
        {
            ChecklistCustomerEntity entity = new ChecklistCustomerEntity(entityId);
            return entity;
        }

        public int CheckListID { get; set; }
        public string CustomerCode { get; set; }
    }
}
