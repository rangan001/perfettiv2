﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ChecklistEntity : BaseEntity
    {
        private ChecklistEntity()
        {
            this.IsNew = true;
        }

        private ChecklistEntity(int entityId)
        {
            this.ChecklistID = entityId;
        }

        public static ChecklistEntity CreateObject()
        {
            ChecklistEntity entity = new ChecklistEntity();
            return entity;
        }

        public static ChecklistEntity CreateObject(int entityId)
        {
            ChecklistEntity entity = new ChecklistEntity(entityId);
            return entity;
        }
        public int ChecklistID { get; set; }
        public int ChecklistTypeId { get; set; }
        public string ChecklistName { get; set; }
        public string ChecklistTypeName { get; set; }
        public int PipelineStageID { get; set; }

        public int LeadID { get; set; }

    }
}
