﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ChecklistImageEntity : BaseEntity
    {
        private ChecklistImageEntity()
        {
            this.IsNew = true;
        }

        private ChecklistImageEntity(int entityId)
        {
            this.ChecklistId = entityId;
        }

        public static ChecklistImageEntity CreateObject()
        {
            ChecklistImageEntity entity = new ChecklistImageEntity();
            return entity;
        }

        public static ChecklistImageEntity CreateObject(int entityId)
        {
            ChecklistImageEntity entity = new ChecklistImageEntity(entityId);
            return entity;
        }

        public int Id { get; set; }
        public int ChecklistId { get; set; }
        public string ImageName { get; set; }
        public byte[] ImageContent { get; set; }
    }
}
