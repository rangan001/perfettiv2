﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ChecklistRouteEntity : BaseEntity
    {
        public int ChecklistId { get; set; }
        public int RouteId { get; set; }
    }
}
