﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CitiesEntity : BaseEntity
    {
        private CitiesEntity()
        {
            this.IsNew = true;
        }

        private CitiesEntity(int entityId)
        {
            this.CityID = entityId;
        }

        public static CitiesEntity CreateObject()
        {
            CitiesEntity entity = new CitiesEntity();
            return entity;
        }

        public static CitiesEntity CreateObject(int entityId)
        {
            CitiesEntity entity = new CitiesEntity(entityId);
            return entity;
        }

        private int? cityID;
        public int? CityID
        {
            get { return cityID; }
            set { cityID = value; }
        }

        private string cityName;
        public string CityName
        {
            get { return cityName; }
            set
            {
                cityName = value;
                IsDirty = true;
            }
        }

        private List<CityAreaEntity> areaList;
        public List<CityAreaEntity> AreaList
        {
            get
            {
                if (areaList == null)
                    areaList = new List<CityAreaEntity>();
                return areaList;
            }
            set
            {
                areaList = value;
            }
        }
    }
}
