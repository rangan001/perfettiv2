﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ClientOpportunityEntity :BaseEntity
    {
        private ClientOpportunityEntity()
        {
            this.IsNew = true;
        }

        private ClientOpportunityEntity(int entityId)
        {
            //this.ActivityID = entityId;
        }

        public static ClientOpportunityEntity CreateObject()
        {
            ClientOpportunityEntity entity = new ClientOpportunityEntity();

            return entity;
        }

        public static ClientOpportunityEntity CreateObject(int entityId)
        {
            ClientOpportunityEntity entity = new ClientOpportunityEntity(entityId);
            return entity;
        }

        public string OpportunityOwner { get; set; }

        public int Count { get; set; }

        public double Value { get; set; }

        public double Tonnes { get; set; }

        public double Units { get; set; }
    }
}
