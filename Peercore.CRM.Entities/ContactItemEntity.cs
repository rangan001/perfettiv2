﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ContactItemEntity : BaseEntity
    {
        private ContactItemEntity()
        {
            this.IsNew = true;
        }

        private ContactItemEntity(int entityId)
        {
            this.ContactItemId = entityId;
        }

        public static ContactItemEntity CreateObject()
        {
            ContactItemEntity entity = new ContactItemEntity();
            return entity;
        }

        public static ContactItemEntity CreateObject(int entityId)
        {
            ContactItemEntity entity = new ContactItemEntity(entityId);
            return entity;
        }

        private string _EntryID;
        public string EntryID
        {
            get { return _EntryID; }
            set { _EntryID = value; }
        }

        private int _ContactItemId;
        public int ContactItemId
        {
            get { return _ContactItemId; }
            set { _ContactItemId = value; }
        }

        private string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        private string _Address;
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        private string _City;
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        private string _State;
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        private string _ZipCode;
        public string ZipCode
        {
            get { return _ZipCode; }
            set { _ZipCode = value; }
        }

        private int _LeadId;
        public int LeadId
        {
            get { return _LeadId; }
            set { _LeadId = value; }
        }
    }
}
