﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ContactPersonTestEntity :BaseEntity
    {
        public ContactPersonTestEntity()
        {
            this.IsNew = true;
        }



        public static ContactPersonTestEntity CreateObject()
        {
            ContactPersonTestEntity entity = new ContactPersonTestEntity();
            return entity;
        }

        public string Title { get; set; }
        public string FirstName { get; set; }
        public string CustCode { get; set; }
        public string Ethnicity { get; set; }
        public List<string> Language { get; set; }

    }
}
