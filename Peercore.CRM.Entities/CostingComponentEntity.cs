﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CostingComponentEntity : BaseEntity
    {
        private CostingComponentEntity()
        {
            this.IsNew = true;
        }

        private CostingComponentEntity(int entityId)
        {
            this.ComponentId = entityId;
        }

        public static CostingComponentEntity CreateObject()
        {
            CostingComponentEntity entity = new CostingComponentEntity();
            return entity;
        }

        public static CostingComponentEntity CreateObject(int entityId)
        {
            CostingComponentEntity entity = new CostingComponentEntity(entityId);
            return entity;
        }

        private int? componentId;
        private string componentName;
        private bool isDirectValue;
        private string formula;
        private List<ComponentParameterEntity> componentParameterList;

        public int? ComponentId
        {
            get
            {
                return componentId;
            }
            set
            {
                componentId = value;
                IsDirty = true;
            }
        }
        public string ComponentName
        {
            get
            {
                return componentName;
            }
            set
            {
                componentName = value;
                IsDirty = true;
            }
        }
        public bool IsDirectValue
        {
            get
            {
                return isDirectValue;
            }
            set
            {
                isDirectValue = value;
                IsDirty = true;
            }
        }

        public string Formula
        {
            get
            {
                return formula;
            }
            set
            {
                formula = value;
                IsDirty = true;
            }
        }

        public string FormulaText { get; set; }

        public List<ComponentParameterEntity> ComponentParameterLst
        {
            get
            {
                return componentParameterList;
            }
            set
            {
                componentParameterList = value;
                IsDirty = true;
            }
        }
    }
}
