﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CustomerActivityEntity : BaseEntity
    {
        public CustomerActivityEntity()
        {
            this.IsNew = true;
        }

        private CustomerActivityEntity(int entityId)
        {
            this.ActivityID = entityId;
        }

        public static CustomerActivityEntity CreateObject()
        {
            CustomerActivityEntity entity = new CustomerActivityEntity();

            return entity;
        }

        public static CustomerActivityEntity CreateObject(int entityId)
        {
            CustomerActivityEntity entity = new CustomerActivityEntity(entityId);

            return entity;
        }

        public int ActivityID { get; set; }
        public string Subject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string AssignedTo { get; set; }
        public int LeadId { get; set; }
        public string Comments { get; set; }
        public string SentMail { get; set; }
        public string Priority { get; set; }
        public string SendReminder { get; set; }
        public DateTime ReminderDate { get; set; }
        public int PipelineStageID { get; set; }
        public string ActivityType { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        public string CustCode { get; set; }

        public string LeadName { get; set; }
        public string LeadStage { get; set; }

        public string StatusDesc { get; set; }
        public string TypeDesc { get; set; }

        public int NoOfActivities { get; set; }

        public int AppointmentId { get; set; }
        public string DelFlag { get; set; }
        public string EndUserCode { get; set; }
        public int RowCount { get; set; }
        public string RepName { get; set; }
        public string RepCode { get; set; }
        public int routeId { get; set; }
    }
}
