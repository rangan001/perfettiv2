﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CustomerEndUserEntity:BaseEntity
    {
        private CustomerEndUserEntity()
        {
            this.IsNew = true;
        }

        private CustomerEndUserEntity(int entityId)
        {
            this.CustEnduserId = entityId;
        }

        public static CustomerEndUserEntity CreateObject()
        {
            CustomerEndUserEntity entity = new CustomerEndUserEntity();

            return entity;
        }

        public static CustomerEndUserEntity CreateObject(int entityId)
        {
            CustomerEndUserEntity entity = new CustomerEndUserEntity(entityId);

            return entity;
        }

        private string custCode;
        public string CustCode
        {
            get { return custCode; }
            set { custCode = value; }
        }

        private int custEnduserId;
        public int CustEnduserId
        {
            get { return custEnduserId; }
            set { custEnduserId = value; }
        }

        private string enduserCode;
        public string EnduserCode
        {
            get { return enduserCode; }
            set { enduserCode = value; }
        }
    }
}
