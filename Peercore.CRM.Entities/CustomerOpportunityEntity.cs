﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CustomerOpportunityEntity:BaseEntity
    {
        private CustomerOpportunityEntity()
        {
            this.IsNew = true;
        }

        private CustomerOpportunityEntity(int entityId)
        {
           // this.CustEnduserId = entityId;
        }

        public static CustomerOpportunityEntity CreateObject()
        {
            CustomerOpportunityEntity entity = new CustomerOpportunityEntity();

            return entity;
        }

        public static CustomerOpportunityEntity CreateObject(int entityId)
        {
            CustomerOpportunityEntity entity = new CustomerOpportunityEntity(entityId);
            return entity;
        }

        public int OpportunityID { get; set; }
        public int LeadID { get; set; }
        public string Originator { get; set; }
        public int RepGroupID { get; set; }
        // NOT IN TABLE
        public string RepGroupName { get; set; }

        public string Name { get; set; }
        public DateTime CloseDate { get; set; }
        public int Stage { get; set; }
        public double Probability { get; set; }
        public double Amount { get; set; }
        public double Units { get; set; }
        public string Description { get; set; }
        public string CustCode { get; set; }

        // Lead Details
        public string LeadName { get; set; }
        public string Business { get; set; }
        public string Industry { get; set; }
        public string IndustryDescription { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string LeadStage { get; set; }
        public string PipelineStage { get; set; }

        public string EndUserCode { get; set; }
        public double Tonnes { get; set; }

        public int RowCount { get; set; }

        public double AmountSum { get; set; }
        public double UnitsSum { get; set; }
        public double TonnesSum { get; set; }
    }
}
