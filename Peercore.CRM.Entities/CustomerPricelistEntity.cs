﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CustomerPricelistEntity : BaseEntity
    {
        public CustomerPricelistEntity()
        {
            this.IsNew = true;
        }

        public static CustomerPricelistEntity CreateObject()
        {
            CustomerPricelistEntity entity = new CustomerPricelistEntity();
            return entity;
        }
        public string CustCode { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string CatlogCode { get; set; }
        public string ForCur { get; set; }
        public double ForRate { get; set; }
        public double FcPrice { get; set; }
        public double Price { get; set; }
        public double DiscPercent { get; set; }
        public string DelFlag { get; set; }
        public string Originator { get; set; }
        public DateTime DateModified { get; set; }
        public string FreightType { get; set; }
        public int Version { get; set; }
        public int MinQty { get; set; }
        public DateTime EndDate { get; set; }
    }
}
