﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DERSummaryReportSectionEntity
    {
        public int StartHeaderRowIndex { get; set; }
        public int StartDataRowIndex { get; set; }
        public int StartColumnIndex { get; set; }
        public int Total_ColumnCount { get; set; }
        public int DHV_ColumnCount { get; set; }

        public List<ExcelColumnEntity> ReportColumnList { get; set; }

    }
}
