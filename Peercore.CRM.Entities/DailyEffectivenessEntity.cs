﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DailyEffectivenessEntity:BaseEntity
    {
        public DailyEffectivenessEntity()
        {
            this.IsNew = true;
        }

        public DailyEffectivenessEntity(int entityId)
        {
            //this.OpportunityID = entityId;
        }

        public static DailyEffectivenessEntity CreateObject()
        {
            DailyEffectivenessEntity entity = new DailyEffectivenessEntity();
            return entity;
        }

        public static DailyEffectivenessEntity CreateObject(int entityId)
        {
            DailyEffectivenessEntity entity = new DailyEffectivenessEntity(entityId);
            return entity;
        }

        public string CustomerName { get; set; }
        public string CustomerCode { get; set;   }
        public bool CDR { get; set; }
        public string Display { get; set; }
        public string Remark { get; set; }
        public List<ProductEntity> ProductList { get; set; }
        public List<MerchandiseEntity> MerchadiseList { get; set; }
        public List<ObjectiveEntity> ObjectiveList { get; set; }
        public string RDF { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        //public string CreatedBy { get; set; }
        //public string LastModifiedBy { get; set; }
        public string Status { get; set; }
        public bool IsTLP { get; set; }
    }
    
}
