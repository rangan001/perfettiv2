﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DistributorEntity : BaseEntity
    {
        private DistributorEntity()
        {

        }
        public DistributorEntity(int entityId)
        {
            this.DistributorId = entityId;
        }

        public static DistributorEntity CreateObject()
        {
            DistributorEntity entity = new DistributorEntity();

            return entity;
        }

        public static DistributorEntity CreateObject(int entityId)
        {
            DistributorEntity entity = new DistributorEntity(entityId);

            return entity;
        }

        public int DistributorId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Originator { get; set; }
        public string Status { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public string PrefixCode { get; set; }
        public string ParentOriginator { get; set; }
        public string DeptString { get; set; }

        public string Password { get; set; }
        public string Adderess { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Holiday { get; set; }

        private List<string> holidaysList = new List<string>();
        public List<string> HolidaysList
        {
            get { return holidaysList; }
            set { holidaysList = value; }
        }

        private List<BankAccountEntity> bankAccountsList = new List<BankAccountEntity>();
        public List<BankAccountEntity> BankAccountsList
        {
            get { return bankAccountsList; }
            set { bankAccountsList = value; }
        }

        #region For the purpose of Target Allocation
        public double Target { get; set; }
        public int TargetId { get; set; }
        public DateTime EffStartDate { get; set; }
        public DateTime EffEndDate { get; set; }
        #endregion

        public string GroupCode { get; set; }
        public DateTime? JoinedDate { get; set; }
        public int DivisionId { get; set; }
        public int AreaId { get; set; }
        public bool IsNotEMEIUser { get; set; }
    }
}
