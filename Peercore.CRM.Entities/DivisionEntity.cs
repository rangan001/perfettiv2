﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DivisionEntity:BaseEntity
    {
        private DivisionEntity()
        {

        }

        private DivisionEntity(int entityId)
        {
            this.DivisionId = entityId;
        }

        public static DivisionEntity CreateObject()
        {
            DivisionEntity entity = new DivisionEntity();
            return entity;
        }

        public static DivisionEntity CreateObject(int entityId)
        {
            DivisionEntity entity = new DivisionEntity(entityId);
            return entity;
        }

        public int DivisionId { get; set; }
        public string DivisionCode { get; set; }
        public string DivisionName { get; set; }
        public bool IsActive { get; set; }
    }
}
