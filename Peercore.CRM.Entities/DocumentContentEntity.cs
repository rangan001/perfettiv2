﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DocumentContentEntity:BaseEntity
    {
        public DocumentContentEntity()
        {
            
        }

        public DocumentContentEntity(int entityId)
        {
            this.DocumentContentId = entityId;
        }

        public static DocumentContentEntity CreateObject()
        {
            DocumentContentEntity entity = new DocumentContentEntity();

            return entity;
        }

        public static DocumentContentEntity CreateObject(int entityId)
        {
            DocumentContentEntity entity = new DocumentContentEntity(entityId);

            return entity;
        }

        public int DocumentContentId { get; set; }

        public int DocumentId { get; set; }

        private string fileName;
        public string DocumentName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                IsDirty = true;
            }
        }

        public string DocumentType { get; set; }

        private byte[] fileContent;
        public byte[] Content
        {
            get { return fileContent; }
            set
            {
                fileContent = value;
                IsDirty = true;
            }
        }
    }
}
