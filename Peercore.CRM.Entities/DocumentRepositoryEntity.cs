﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DocumentRepositoryEntity:BaseEntity
    {
        public DocumentRepositoryEntity()
        {
            
        }

        public DocumentRepositoryEntity(int entityId)
        {
            this.RepositoryID = entityId;
        }

        public static DocumentRepositoryEntity CreateObject()
        {
            DocumentRepositoryEntity entity = new DocumentRepositoryEntity();

            return entity;
        }

        public static DocumentRepositoryEntity CreateObject(int entityId)
        {
            DocumentRepositoryEntity entity = new DocumentRepositoryEntity(entityId);

            return entity;
        }

        public int RepositoryID { get; set; }

        private string fileName;
        public string DocumentName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                IsDirty = true;
            }
        }

        private byte[] fileContent;
        public byte[] FileContent
        {
            get { return fileContent; }
            set
            {
                fileContent = value;
                IsDirty = true;
            }
        }

        public string Path { get; set; }
        public DateTime AttachedDate { get; set; }
        public string AttachedBy { get; set; }
    }
}
