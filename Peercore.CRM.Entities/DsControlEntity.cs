﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DsControlEntity : BaseEntity
    {
        public DsControlEntity()
        {            
        }

        public static DsControlEntity CreateObject()
        {
            DsControlEntity entity = new DsControlEntity();
            return entity;
        }

        public string ControlId { get; set; }
        public int CurrentYear { get; set; }
        public int CurrentPeriod { get; set; }
        public DateTime MinInvDate { get; set; }
        public double TaxRate { get; set; }
        public int SorderNoNext { get; set; }
        public int SorderNoReset { get; set; }
        public int SorderNoHigh { get; set; }
        public int PlistNoNext { get; set; }
        public int PlistNoReset { get; set; }
        public int PlistNoHigh { get; set; }
        public int IvceNoNext { get; set; }
        public int IvceNoReset { get; set; }
        public int IvceNoHigh { get; set; }
        public int CreditNoNext { get; set; }
        public int CreditNoReset { get; set; }
        public int CreditNoHigh { get; set; }
        public int PorderNoNext { get; set; }
        public int PorderNoReset { get; set; }
        public int PorderNoHigh { get; set; }
        public int DealNoNext { get; set; }
        public int DealNoReset { get; set; }
        public int DealNoHigh { get; set; }
        public int DebtorsYear { get; set; }
        public int DebtorsPeriod { get; set; }
    }
}
