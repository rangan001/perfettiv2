﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class EndUserContactPersonEntity:BaseEntity
    {
        public EndUserContactPersonEntity()
        {
            
        }

        public EndUserContactPersonEntity(int entityId)
        {
            this.ContactPersonID = entityId;
        }

        public static EndUserContactPersonEntity CreateObject()
        {
            EndUserContactPersonEntity entity = new EndUserContactPersonEntity();

            return entity;
        }

        public static EndUserContactPersonEntity CreateObject(int entityId)
        {
            EndUserContactPersonEntity entity = new EndUserContactPersonEntity(entityId);

            return entity;
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; IsDirty = true; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; IsDirty = true; }
        }

        public int ContactPersonID { get; set; }
        public string EndUserCode { get; set; }
        public string Originator { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string ReportsTo { get; set; }
        public string Likes { get; set; }
        public string DisLikes { get; set; }
        public string MailingAddress { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingPostcode { get; set; }
        public string MailingCountry { get; set; }
        public string OtherAddress { get; set; }
        public string OtherCity { get; set; }
        public string OtherState { get; set; }
        public string OtherPostCode { get; set; }
        public string OtherCountry { get; set; }
        public string Description { get; set; }
        public string EndUserName { get; set; }
        public string SpecialInterests { get; set; }
        public string ImagePath { get; set; }
        public string KeyContact { get; set; }

        public bool KeyContactChecked
        {
            get
            {
                if (KeyContact == "Y")
                    return true;
                else
                    return false;
            }
        }

        public string CustCode { get; set; }    // Not In Table
        public string Contact { get; set; }
        public string Origin { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }

        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }

        public string CompanyTelephone { get; set; }

        public string ContactType { get; set; }
        public string TypeDescription { get; set; }

        public int RowCount { get; set; }
    }
}
