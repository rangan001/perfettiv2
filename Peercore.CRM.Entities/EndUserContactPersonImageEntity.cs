﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class EndUserContactPersonImageEntity:BaseEntity
    {
        public EndUserContactPersonImageEntity()
        {
            
        }

        public EndUserContactPersonImageEntity(int entityId)
        {
            this.ContactPersonImageId = entityId;
        }

        public static EndUserContactPersonImageEntity CreateObject()
        {
            EndUserContactPersonImageEntity entity = new EndUserContactPersonImageEntity();

            return entity;
        }

        public static EndUserContactPersonImageEntity CreateObject(int entityId)
        {
            EndUserContactPersonImageEntity entity = new EndUserContactPersonImageEntity(entityId);

            return entity;
        }

        public int ContactPersonImageId { get; set; }

        public int ContactPersonId { get; set; }

        public int ContactNo { get; set; }

        public string FileName { get; set; }
        private byte[] fileContent;
        public byte[] FileContent
        {
            get { return fileContent; }
            set
            {
                fileContent = value;
                IsDirty = true;
            }
        }

        public string EndUserCode { get; set; }

        public string CustomerCode { get; set; }
    }
}
