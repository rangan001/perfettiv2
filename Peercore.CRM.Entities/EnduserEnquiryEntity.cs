﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class EnduserEnquiryEntity:BaseEntity
    {
        public EnduserEnquiryEntity()
        {
            
        }

       /* public EnduserEnquiryEntity(int entityId)
        {
            this.ContactPersonImageId = entityId;
        }
        */

        public static EnduserEnquiryEntity CreateObject()
        {
            EnduserEnquiryEntity entity = new EnduserEnquiryEntity();

            return entity;
        }

     /*  public static EnduserEnquiryEntity CreateObject(int entityId)
        {
            EnduserEnquiryEntity entity = new EnduserEnquiryEntity(entityId);

            return entity;
        }
        */
        public string CustomerCode { get; set; }

        public string EndUserCode { get; set; }

        public string CatlogCode { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public double CasesSupplied { get; set; }

        public DateTime RebateMonth { get; set; }

        public string CustomerName { get; set; }

        public string RepCode { get; set; }

        public string RepName { get; set; }

        public string EndUserName { get; set; }

        public double RebatePerc { get; set; }

        public double CurPrice { get; set; }

        public double TotalCases { get; set; }

        public double month1 { get; set; }
        public double month2 { get; set; }
        public double month3 { get; set; }
        public double month4 { get; set; }
        public double month5 { get; set; }
        public double month6 { get; set; }
        public double month7 { get; set; }
        public double month8 { get; set; }
        public double month9 { get; set; }
        public double month10 { get; set; }
        public double month11 { get; set; }
        public double month12 { get; set; }
    }
}
