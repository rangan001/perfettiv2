﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class EnquiryPriceEntity:BaseEntity
    {
        private EnquiryPriceEntity()
        {
            this.IsNew = true;
        }

        /*private EnquiryPriceEntity(int entityId)
        {
            this.ActivityID = entityId;
        }*/

        public static EnquiryPriceEntity CreateObject()
        {
            EnquiryPriceEntity entity = new EnquiryPriceEntity();

            return entity;
        }

       /* public static EnquiryPriceEntity CreateObject(int entityId)
        {
            EnquiryPriceEntity entity = new EnquiryPriceEntity(entityId);

            return entity;
        }*/

        public string CustomerCode { get; set; }

        public string EndUserCode { get; set; }

        public string CatlogCode { get; set; }

        public double Price { get; set; }

        public string Originator { get; set; }

        public int Version { get; set; }

        public DateTime EffectiveDate { get; set; }

        public DateTime LastUpdated { get; set; }
    }
}
