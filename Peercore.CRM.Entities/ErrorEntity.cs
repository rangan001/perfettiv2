﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ErrorEntity : BaseEntity
    {
        private ErrorEntity()
        {
            this.IsNew = true;
        }

        private ErrorEntity(int entityId)
        {
            this.ID = entityId;
        }

        public static ErrorEntity CreateObject()
        {
            ErrorEntity entity = new ErrorEntity();

            return entity;
        }

        public static ErrorEntity CreateObject(int entityId)
        {
            ErrorEntity entity = new ErrorEntity(entityId);

            return entity;
        }

        public DateTime? ErrorDate { get; set; }
        public string ErrorLineNumber { get; set; }
        public string Errorlink { get; set; }
        public string Host { get; set; }
        public int ID { get; set; }
        public string IpAddress { get; set; }
        public string LongDescription { get; set; }
        public string MethodName { get; set; }
        public int OriginatorID { get; set; }
        public string Referer { get; set; }
        public string ShortDescription { get; set; }
        public string UserLogin { get; set; }
    }
}
