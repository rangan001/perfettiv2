﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ExcelColumnEntity
    {
        public BaseExcelCellEntity Header { get; set; }
        public List<BaseExcelCellEntity> RowValues { get; set; }
        public BaseExcelCellEntity FooterTotal { get; set; }
    }
}
