﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class FlavorEntity : BaseEntity
    {
        private FlavorEntity()
        {
            this.IsNew = true;
        }

        private FlavorEntity(int entityId)
        {
            this.FlavorId = entityId;
        }

        public static FlavorEntity CreateObject()
        {
            FlavorEntity entity = new FlavorEntity();

            return entity;
        }

        public static FlavorEntity CreateObject(int entityId)
        {
            FlavorEntity entity = new FlavorEntity(entityId);

            return entity;
        }

        public int FlavorId { get; set; }
        public string FlavorName { get; set; }
    }
}
