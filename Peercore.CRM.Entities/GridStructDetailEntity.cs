﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class GridStructDetailEntity : BaseEntity
    {
        private GridStructDetailEntity()
        {
            this.IsNew = true;
        }

        private GridStructDetailEntity(int entityId)
        {
            this.IdNo = entityId;
        }

        public static GridStructDetailEntity CreateObject()
        {
            GridStructDetailEntity entity = new GridStructDetailEntity();
            return entity;
        }

        public static GridStructDetailEntity CreateObject(int entityId)
        {
            GridStructDetailEntity entity = new GridStructDetailEntity(entityId);
            return entity;
        }

        int idNo = 0;
        string propertyName = string.Empty;
        bool isVisible = false;
        double width = 0;
        int columnOrder = 0;
        string sortType= "";

        public int IdNo
        {
            get { return idNo; }
            set { idNo = value; }
        }

        public string PropertyName
        {
            get { return propertyName; }
            set { propertyName = value; }
        }

        public bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; }
        }

        public double Width
        {
            get { return width; }
            set { width = value; }
        }

        public int ColumnOrder
        {
            get { return columnOrder; }
            set { columnOrder = value; }
        }

        public string SortType
        {
            get { return sortType; }
            set { sortType = value; }
        }
    }
}