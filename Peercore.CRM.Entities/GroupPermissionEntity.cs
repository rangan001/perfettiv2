﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class GroupPermissionEntity : BaseEntity
    {
        private GroupPermissionEntity()
        {
            this.IsNew = true;
        }

        private GroupPermissionEntity(int entityId)
        {
            this.GroupPermissionId = entityId;
        }

        public static GroupPermissionEntity CreateObject()
        {
            GroupPermissionEntity entity = new GroupPermissionEntity();
            return entity;
        }

        public static GroupPermissionEntity CreateObject(int entityId)
        {
            GroupPermissionEntity entity = new GroupPermissionEntity(entityId);
            return entity;
        }

        private int? groupPermissionId;
        public int? GroupPermissionId
        {
            get { return groupPermissionId; }
            set
            {
                groupPermissionId = value;
                IsDirty = true;
            }
        }

        private int? authorizationGroupId;
        public int? AuthorizationGroupId
        {
            get { return authorizationGroupId; }
            set
            {
                authorizationGroupId = value;
                IsDirty = true;
            }
        }

        private string userControlCode;
        public string UserControlCode
        {
            get { return userControlCode; }
            set
            {
                userControlCode = value;
                IsDirty = true;
            }
        }

        private bool canCreate;
        public bool CanCreate
        {
            get { return canCreate; }
            set
            {
                canCreate = value;
                IsDirty = true;
            }
        }

        private bool canModifyOwn;
        public bool CanModifyOwn
        {
            get { return canModifyOwn; }
            set
            {
                canModifyOwn = value;
                IsDirty = true;
            }
        }

        private bool canModifyOther;
        public bool CanModifyOther
        {
            get { return canModifyOther; }
            set
            {
                canModifyOther = value;
                IsDirty = true;
            }
        }

        private bool canViewOwn;
        public bool CanViewOwn
        {
            get { return canViewOwn; }
            set
            {
                canViewOwn = value;
                IsDirty = true;
            }
        }

        private bool canViewOther;
        public bool CanViewOther
        {
            get { return canDeleteOther; }
            set
            {
                canDeleteOther = value;
                IsDirty = true;
            }
        }

        private bool canDeleteOwn;
        public bool CanDeleteOwn
        {
            get { return canDeleteOwn; }
            set
            {
                canDeleteOwn = value;
                IsDirty = true;
            }
        }

        private bool canDeleteOther;
        public bool CanDeleteOther
        {
            get { return canDeleteOther; }
            set
            {
                canDeleteOther = value;
                IsDirty = true;
            }
        }
    }
}
