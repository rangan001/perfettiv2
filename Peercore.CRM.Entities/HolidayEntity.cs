﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class HolidayEntity :BaseEntity 
    {
        public HolidayEntity()
        {
            this.IsNew = true;
        }

        public static HolidayEntity CreateObject()
        {
            HolidayEntity entity = new HolidayEntity();
            return entity;
        }

        //private HolidayEntity(int entityId)
        //{
        //    this.Date = entityId;
        //}

        //public static HolidayEntity CreateObject(int entityId)
        //{
        //    HolidayEntity entity = new HolidayEntity(entityId);
        //    return entity;
        //}

        private int holidayId;
        public int HolidayId
        {
            get { return holidayId; }
            set { holidayId = value; }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private DateTime endDate;
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        private string holidayType;
        public string HolidayType
        {
            get { return holidayType; }
            set { holidayType = value; }
        }


        private string typeDescription;
        public string TypeDescription
        {
            get { return typeDescription; }
            set { typeDescription = value; }
        }


        private string typeColor;
        public string TypeColor
        {
            get { return typeColor; }
            set { typeColor = value; }
        }

        public bool IsAllDay { get; set; }
    
       
    }
}
