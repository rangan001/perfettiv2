﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class IncentiveOptionLevelCombinationEntity:BaseEntity
    {
        public IncentiveOptionLevelCombinationEntity()
        {
            this.IsNew = true;
        }

        public IncentiveOptionLevelCombinationEntity(int entityId)
        {
            this.IpOptionLevelCombinationId = entityId;
        }

        public static IncentiveOptionLevelCombinationEntity CreateObject()
        {
            IncentiveOptionLevelCombinationEntity entity = new IncentiveOptionLevelCombinationEntity();
            return entity;
        }

        public static IncentiveOptionLevelCombinationEntity CreateObject(int entityId)
        {
            IncentiveOptionLevelCombinationEntity entity = new IncentiveOptionLevelCombinationEntity(entityId);
            return entity;
        }

        public int IpOptionLevelCombinationId { get; set; }
        public int IpOptionLevelId { get; set; }
        public int ProductId { get; set; }
        public bool IsExceptProduct { get; set; }
        public int ProductPackingId { get; set; }
        public int ProductCategoryId { get; set; }        
        public int BrandId { get; set; }
        public bool? IsHvp { get; set; }
        public int FlavorId { get; set; }
        public bool IsActive { get; set; }       

        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string ProductCatagoryName { get; set; }
        public string BrandName { get; set; }
        public string FlavorName { get; set; }     
    }
}
