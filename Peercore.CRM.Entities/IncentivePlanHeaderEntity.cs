﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class IncentivePlanHeaderEntity:BaseEntity
    {
        public IncentivePlanHeaderEntity()
        {
            this.IsNew = true;
        }

        public IncentivePlanHeaderEntity(int entityId)
        {
            this.IncentivePlanHeaderId = entityId;
        }

        public static IncentivePlanHeaderEntity CreateObject()
        {
            IncentivePlanHeaderEntity entity = new IncentivePlanHeaderEntity();
            return entity;
        }

        public static IncentivePlanHeaderEntity CreateObject(int entityId)
        {
            IncentivePlanHeaderEntity entity = new IncentivePlanHeaderEntity(entityId);
            return entity;
        }

        public int IncentivePlanHeaderId { get; set; }
        public string PlanName { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public bool IsActive { get; set; }

        public List<IncentivePlanDetailsEntity> IncentivePlanDetailsList { get; set; }
    }
}
