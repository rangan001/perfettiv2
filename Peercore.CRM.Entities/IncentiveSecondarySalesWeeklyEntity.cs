﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class IncentiveSecondarySalesWeeklyEntity:BaseEntity
    {
        public IncentiveSecondarySalesWeeklyEntity()
        {
            this.IsNew = true;
        }

        public IncentiveSecondarySalesWeeklyEntity(int entityId)
        {
            this.SecondarySalesWeeklyId = entityId;
        }

        public static IncentiveSecondarySalesWeeklyEntity CreateObject()
        {
            IncentiveSecondarySalesWeeklyEntity entity = new IncentiveSecondarySalesWeeklyEntity();
            return entity;
        }

        public static IncentiveSecondarySalesWeeklyEntity CreateObject(int entityId)
        {
            IncentiveSecondarySalesWeeklyEntity entity = new IncentiveSecondarySalesWeeklyEntity(entityId);
            return entity;
        }

        public int SecondarySalesWeeklyId { get; set; }
        public int IncentivePlanDetailsId { get; set; }        
        public string Header { get; set; }
        public double CumTarPerc { get; set; }
        public double IncPerc { get; set; }
        public bool IsActive { get; set; }
    }
}
