﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class InvoiceHeaderEntity : BaseEntity
    {
        public InvoiceHeaderEntity()
        {
        }

        public static InvoiceHeaderEntity CreateObject()
        {
            InvoiceHeaderEntity entity = new InvoiceHeaderEntity();
            return entity;
        }

        public int IvceNo { get; set; }
        public string CustCode { get; set; }
        public string IvceType { get; set; }
        public string IvceDate { get; set; }
        public string CustOrderNo { get; set; }
        public int AssigneeNo { get; set; }
        //public string SourceCode { get; set; }
        //public int SourceNo { get; set; }
        //public DateTime DateShipped { get; set; }
        //public string TaxCert { get; set; }
        public double Discount { get; set; }
        public double SalesTax { get; set; }
        public double SalesTaxAmount { get; set; }
        public string PriceList { get; set; }
        //public string RunNo { get; set; }
        //public string CreditPolicy { get; set; }
        public string SpecialInst { get; set; }
        public string AreaCode { get; set; }
        public string CarrierCode { get; set; }
        public string ConNote { get; set; }
        public string Freight { get; set; }
        public double FreightCost { get; set; }
        public string RepCode { get; set; }
        public int SorderNo { get; set; }
        public int PlistNo { get; set; }
        public int BatchNo { get; set; }
        public string Status { get; set; }
        public int Version { get; set; }
        public DateTime TransferDate { get; set; }
        // public int BasSeq { get; set; }
        //public string ForCur { get; set; }
        //public double ForRate { get; set; }
        public DateTime DueDate { get; set; }
        public int ConsignmentNo { get; set; }
        //public string DocDelStatus { get; set; }
        public DateTime DateDocSent { get; set; }
        //public string CustFrom { get; set; }
        public int IvceId { get; set; }
        public double Grandtotal { get; set; }
        public double GrossTotal { get; set; }
        public double ReturnsTotal { get; set; }
        public double Amount { get; set; }

        public int TerritoryId { get; set; }
        public int AreaId { get; set; }
        public int RegionId { get; set; }
        public int DistributorId { get; set; }
        public int AsmId { get; set; }
        public int RsmId { get; set; }
        public string OrderNo { get; set; }

        public List<InvoiceDetailEntity> InvoiceDetailList { get; set; }
        public List<InvoiceDetailWithDiscountsEntity> InvoiceDetailWithDiscountsList { get; set; }
        public List<InvoiceSchemeGroupEntity> InvoiceSchemeGroupList { get; set; }

        public string InvoiceNo { get; set; }
        public string CustomerName { get; set; }

        public InvoiceSettlementEntity InvoiceSettlement { get; set; }

        public List<InvoiceSettlementEntity> InvoiceSettlementList { get; set; }

        public ReturnInvoiceEntity ReturnInvoice { get; set; }

        public List<ReturnInvoiceEntity> ReturnInvoiceList { get; set; }

        public string Remark { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public string DistributorName { get; set; }

        public float QtyCasess { get; set; } // added by rangan

        public float QtyTonnage { get; set; } // added by rangan

        public string SelectedValue { get; set; }
    }

    public class RepsTrackingDetailsEntity : BaseEntity
    {
        public string CustomerCode { get; set; }

        public string InvoiceDate { get; set; }

        public double InvoiceTotal { get; set; }

        public double TotalDiscount { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public string DistributorName { get; set; }

        public string RepCode { get; set; }

        public string RepName { get; set; }

        public int PC { get; set; }

        public int VC { get; set; }
    }
}
