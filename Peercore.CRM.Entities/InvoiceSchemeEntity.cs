﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class InvoiceSchemeEntity : BaseEntity
    {
        public InvoiceSchemeEntity()
        {            
        }

        public static InvoiceSchemeEntity CreateObject()
        {
            InvoiceSchemeEntity entity = new InvoiceSchemeEntity();
            return entity;
        }
        public int InvoiceSchemeId { get; set; }
        //public int InvoiceId { get; set; }
        public int ProductId { get; set; }
        public double UsedQuantity { get; set; }
        public int SchemeGroupId { get; set; }
        public double DiscountVal { get; set; }
        public string ProductName { get; set; }
    }
}
