﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ItineraryHeaderEntity:BaseEntity
    {
        public ItineraryHeaderEntity()
        {
            this.IsNew = true;
        }

        public ItineraryHeaderEntity(int entityId)
        {
            this.ItineraryHeaderId = entityId;
        }

        public static ItineraryHeaderEntity CreateObject()
        {
            ItineraryHeaderEntity entity = new ItineraryHeaderEntity();
            return entity;
        }

        public static ItineraryHeaderEntity CreateObject(int entityId)
        {
            ItineraryHeaderEntity entity = new ItineraryHeaderEntity(entityId);
            return entity;
        }
        
        public int ItineraryHeaderId { get; set; }
        public int CallCycleId { get; set; }
        public string AssignedTo { get; set; }
        public string BaseTown { get; set; }
        public string ItineraryName { get; set; }
        public string Status { get; set; }
        public string Month { get; set; }
        public bool HasSentMail { get; set; }
        public string Comments { get; set; }
        public List<ItineraryDetailEntity> ItineraryDetailList { get; set; }
        public int ItineraryLogEntryId { get; set; }
        public DateTime EffDate { get; set; }
        public int PrivateField { get; set; }
        public int Contigency { get; set; }
    }
}
