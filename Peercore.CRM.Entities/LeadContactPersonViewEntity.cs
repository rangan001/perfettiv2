﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LeadContactPersonViewEntity:BaseEntity
    {
        private LeadContactPersonViewEntity()
        {
            this.IsNew = true;
        }

        private LeadContactPersonViewEntity(int entityId)
        {
           // this.SOrderNo = entityId;
        }

        public static LeadContactPersonViewEntity CreateObject()
        {
            LeadContactPersonViewEntity entity = new LeadContactPersonViewEntity();
            return entity;
        }

        public static LeadContactPersonViewEntity CreateObject(int entityId)
        {
            LeadContactPersonViewEntity entity = new LeadContactPersonViewEntity(entityId);
            return entity;
        }

        public int ContactPersonID { get; set; }
        public int LeadID { get; set; }
        public string CustCode { get; set; }

        public string Name { get; set; }        // Lead & Customer
        public string ShortName { get; set; }   // Lead & Customer
        public string FirstName { get; set; }   // Contact Person Only
        public string LastName { get; set; }    // Contact Person Only
        public string Company { get; set; }     // Lead & Customer
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string EndUserCode { get; set; }

        public string ContactType { get; set; }
        public string Origin { get; set; }
    }
}
