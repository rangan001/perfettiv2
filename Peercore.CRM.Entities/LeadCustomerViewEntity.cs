﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LeadCustomerViewEntity : BaseEntity
    {
        private LeadCustomerViewEntity()
        {
            this.IsNew = true;
        }

        private LeadCustomerViewEntity(int entityId)
        {
            //this.StageId = entityId;
        }

        public static LeadCustomerViewEntity CreateObject()
        {
            LeadCustomerViewEntity entity = new LeadCustomerViewEntity();
            return entity;
        }

        public static LeadCustomerViewEntity CreateObject(int entityId)
        {
            LeadCustomerViewEntity entity = new LeadCustomerViewEntity(entityId);
            return entity;
        }


        /// <summary>
        /// Gets or sets the source id.
        /// </summary>
        /// <value>The source id.</value>
        public int SourceId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the lead customer.
        /// </summary>
        /// <value>The type of the lead customer.</value>
        public LeadCustomerTypes LeadCustomerType { get; set; }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>The company.</value>
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets the business.
        /// </summary>
        /// <value>The business.</value>
        public string Business { get; set; }

        /// <summary>
        /// Gets or sets the industry.
        /// </summary>
        /// <value>The industry.</value>
        public string Industry { get; set; }

        /// <summary>
        /// Gets or sets the industry description.
        /// </summary>
        /// <value>The industry description.</value>
        public string IndustryDescription { get; set; }

        /// <summary>
        /// Gets or sets the lead status.
        /// </summary>
        /// <value>The lead status.</value>
        public string LeadStatus { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>The postal code.</value>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the telephone.
        /// </summary>
        /// <value>The telephone.</value>
        public string Telephone { get; set; }

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the customer code.
        /// </summary>
        /// <value>The customer code.</value>
        public string CustomerCode { get; set; }

        public string EndUserCode { get; set; }

        /// <summary>
        /// Gets or sets the lead stage.
        /// </summary>
        /// <value>The lead stage.</value>
        public string LeadStage { get; set; }

        public string Fax { get; set; }

        public string LeadSource { get; set; }
        public string Originator { get; set; }
        public double? AnnualRevenue { get; set; }
        public int? NoOfEmployees { get; set; }
        public int? Rating { get; set; }
        public string Webiste { get; set; }
        public string Description { get; set; }
        public string BusinessPotential { get; set; }
        public string ReferredBy { get; set; }
        public string Country { get; set; }
        public string PreferredContact { get; set; }
        public string PreferredContactDescription { get; set; }
        public string ChannelDescription { get; set; }
        //public string CreatedBy { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        public int NoOfLeadCustomers { get; set; }

        public string IsDeleted { get; set; }
        public string SecondaryRepCode { get; set; }

        public int RowCount { get; set; }

        public string MarketName { get; set; }
        public int MarketId { get; set; }
        public int VolumeId { get; set; }
        public int CategoryId { get; set; }
        public string Volume { get; set; }
        public int PeripheryId { get; set; }
        public string Periphery { get; set; }
        public int OutletTypeId { get; set; }
        public string OutletType { get; set; }
        public bool isTLP { get; set; }
        public double Longitude { get; set; }
        public double Latitiude { get; set; }
        public int PromotionId { get; set; }
        public string PromotionName { get; set; }
        public string Category { get; set; }
        public string RepCode { get; set; }
        public string TME { get; set; }
        public string CustType { get; set; }

        public int DistributorID { get; set; }
        public string DistributorCode { get; set; }
        public string DistributorName { get; set; }

        public string LastInvoiceDate { get; set; }
        public bool IsCheckList { get; set; }
        public string RouteName { get; set; }
        public string RepName { get; set; }
    }

    public enum LeadCustomerTypes : int
    {
        Lead = 0,

        Customer = 1,

        //ShihaFath
        EndUser = 2
    }
}
