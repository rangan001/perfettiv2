﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LeadStageEntity : BaseEntity
    {
        private LeadStageEntity()
        {
            this.IsNew = true;
        }

        private LeadStageEntity(int entityId)
        {
            this.StageId = entityId;
        }

        public static LeadStageEntity CreateObject()
        {
            LeadStageEntity entity = new LeadStageEntity();
            return entity;
        }

        public static LeadStageEntity CreateObject(int entityId)
        {
            LeadStageEntity entity = new LeadStageEntity(entityId);
            return entity;
        }

        public int StageId { get; set; }

        public string StageName { get; set; }

        public int StageOrder { get; set; }

        public bool OpportunityAllowed { get; set; }

        public int MaxOpportunities { get; set; }

        public string EmailAddress { get; set; }

        // NOT IN TABLE
        public bool IsNew { get; set; } // This is to use when saving

        public bool IsSelected { get; set; }

        public int LeadStageId { get; set; }
        public string LeadStage { get; set; }
        public int LeadsCount { get; set; }
    }
}
