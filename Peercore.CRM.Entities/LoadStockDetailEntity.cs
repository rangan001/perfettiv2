﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LoadStockDetailEntity : BaseEntity
    {

        private LoadStockDetailEntity()
        {
            this.IsNew = true;
        }

        private LoadStockDetailEntity(int entityId)
        {
            //this.ChecklistID = entityId;
        }

        public static LoadStockDetailEntity CreateObject()
        {
            LoadStockDetailEntity entity = new LoadStockDetailEntity();
            return entity;
        }

        public static LoadStockDetailEntity CreateObject(int entityId)
        {
            LoadStockDetailEntity entity = new LoadStockDetailEntity(entityId);
            return entity;
        }

        private int stockHeaderId;
        public int StockHeaderId
        {
            get { return stockHeaderId; }
            set { stockHeaderId = value; IsDirty = true; }
        }

        private string catalogCode;
        public string CatalogCode
        {
            get { return catalogCode; }
            set { catalogCode = value; IsDirty = true; }
        }

        private string catalogName;
        public string CatalogName
        {
            get { return catalogName; }
            set { catalogName = value; IsDirty = true; }
        }

        private double allocQty;
        public double AllocQty
        {
            get { return allocQty; }
            set { allocQty = value; IsDirty = true; }
        }

        private double adjustQty;
        public double AdjustQty
        {
            get { return adjustQty; }
            set { adjustQty = value; IsDirty = true; }
        }

        private double loadQty;
        public double LoadQty
        {
            get { return loadQty; }
            set { loadQty = value; IsDirty = true; }
        }

        private double balanceQty;
        public double BalanceQty
        {
            get { return balanceQty; }
            set { balanceQty = value; IsDirty = true; }
        }

        private double returnQty;
        public double ReturnQty
        {
            get { return returnQty; }
            set { returnQty = value; IsDirty = true; }
        }

        private string returnReason;
        public string ReturnReason
        {
            get { return returnReason; }
            set { returnReason = value; IsDirty = true; }
        }

        private string movementType;
        public string MovementType
        {
            get { return movementType; }
            set { movementType = value; IsDirty = true; }
        }

        private int sku;
        public int Sku
        {
            get { return sku; }
            set { sku = value; IsDirty = true; }
        }

        private int stockDetailId;
        public int StockDetailId
        {
            get { return stockDetailId; }
            set { stockDetailId = value; IsDirty = true; }
        }

        //To hold returning amount of units at a time: for the purpose of inserting to Stock Movement
        private double returningQty;
        public double ReturningQty
        {
            get { return returningQty; }
            set { returningQty = value; IsDirty = true; }
        }

        private double loadQtyOld;
        public double LoadQtyOld
        {
            get { return loadQtyOld; }
            set { loadQtyOld = value; IsDirty = true; }
        }

        private double vehicleBalanceQty;
        public double VehicleBalanceQty
        {
            get { return vehicleBalanceQty; }
            set { vehicleBalanceQty = value; IsDirty = true; }
        }

        private double vehicleReturnQty;
        public double VehicleReturnQty
        {
            get { return vehicleReturnQty; }
            set { vehicleReturnQty = value; IsDirty = true; }
        }

        private double vehicleReturningQty;
        public double VehicleReturningQty
        {
            get { return vehicleReturningQty; }
            set { vehicleReturningQty = value; IsDirty = true; }
        }

        private int stockTotalId;
        public int StockTotalId
        {
            get { return stockTotalId; }
            set { stockTotalId = value; }
        }

        private double allocationQtyTotal;
        public double AllocationQtyTotal
        {
            get { return allocationQtyTotal; }
            set { allocationQtyTotal = value; }
        }

        private int catalogId;
        public int CatalogId
        {
            get { return catalogId; }
            set { catalogId = value; }
        }
        private int recordId;
        public int RecordId
        {
            get { return recordId; }
            set { recordId = value; }
        }
    }
}
