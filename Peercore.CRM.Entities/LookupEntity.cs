﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LookupEntity : BaseEntity
    {
        private LookupEntity()
        {
            this.IsNew = true;
        }

        private LookupEntity(int entityId)
        {
            //this.ChecklistID = entityId;
        }

        public static LookupEntity CreateObject()
        {
            LookupEntity entity = new LookupEntity();
            return entity;
        }

        public static LookupEntity CreateObject(int entityId)
        {
            LookupEntity entity = new LookupEntity(entityId);
            return entity;
        }

        public object SourceId { get; set; }

        public object Code { get; set; }

        public string Description { get; set; }

        public List<KeyValuePair<string, object>> AdditionalFields { get; set; }

    }
}
