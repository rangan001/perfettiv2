﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MarketObjectiveEntity:BaseEntity
    {
        public MarketObjectiveEntity()
        {
            this.IsNew = true;
        }

        public MarketObjectiveEntity(int entityId)
        {
            this.MarketObjectiveId = entityId;
        }

        public static MarketObjectiveEntity CreateObject()
        {
            MarketObjectiveEntity entity = new MarketObjectiveEntity();
            return entity;
        }

        public static MarketObjectiveEntity CreateObject(int entityId)
        {
            MarketObjectiveEntity entity = new MarketObjectiveEntity(entityId);
            return entity;
        }

        private int marketObjectiveId;
        public int MarketObjectiveId
        {
            get { return marketObjectiveId; }
            set { marketObjectiveId = value; }
        }

        private MarketEntity market;
        public MarketEntity Market
        {
            get { return market; }
            set { market = value; }
        }

        private ObjectiveEntity objective;
        public ObjectiveEntity Objective
        {
            get { return objective; }
            set { objective = value; }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
    }
}
