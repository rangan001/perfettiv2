﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MerchandiseEntity : BaseEntity
    {
        public MerchandiseEntity()
        {
            this.IsNew = true;
        }

        public MerchandiseEntity(int entityId)
        {
            this.MerchandiseId = entityId;
        }

        public static MerchandiseEntity CreateObject()
        {
            MerchandiseEntity entity = new MerchandiseEntity();
            return entity;
        }

        public static MerchandiseEntity CreateObject(int entityId)
        {
            MerchandiseEntity entity = new MerchandiseEntity(entityId);
            return entity;
        }

        public int MerchandiseId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }

        public string IsMerchandising { get; set; }
        public string CustomerCode { get; set; }

    }
}
