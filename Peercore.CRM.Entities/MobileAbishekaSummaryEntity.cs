﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileAbishekaSummaryEntity
    {
        public MobileAbishekaSummaryEntity()
        {
        }

        public static MobileAbishekaSummaryEntity CreateObject()
        {
            MobileAbishekaSummaryEntity entity = new MobileAbishekaSummaryEntity();
            return entity;
        }

        public int Volume { get; set; }

        public int Target { get; set; }
        public int Actual { get; set; }
        public int Outlets { get; set; }
        public double AchievementPerc { get; set; }
        public int TargetAchivers { get; set; }
        public double Investment { get; set; }
        public double CostPerMille { get; set; }

        public int TargetTotal { get; set; }
        public int ActualTotal { get; set; }
        public int OutletsTotal { get; set; }
        public double AchievementPercTotal { get; set; }
        public int TargetAchiversTotal { get; set; }
        public double InvestmentTotal { get; set; }
        public double CostPerMilleTotal { get; set; }

        public int TargetBonus { get; set; }
        public int ActualBonus { get; set; }
    }
}
