﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileCustomerMerchandisingEntity: BaseEntity
    {
        public MobileCustomerMerchandisingEntity()
        {
            this.IsNew = true;
        }

        public static MobileCustomerMerchandisingEntity CreateObject()
        {
            MobileCustomerMerchandisingEntity entity = new MobileCustomerMerchandisingEntity();
            return entity;
        }

        private int merchandisingId;
        public int MerchandisingId
        {
            get { return merchandisingId; }
            set { merchandisingId = value; }
        }

        private string customerCode;
        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; }
        }

        private string display;
        public string Display
        {
            get { return display; }
            set { display = value; }
        }
    }
}
