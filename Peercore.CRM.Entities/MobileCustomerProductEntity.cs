﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileCustomerProductEntity:BaseEntity
    {
        public MobileCustomerProductEntity()
        {
            this.IsNew = true;
        }

        public static MobileCustomerProductEntity CreateObject()
        {
            MobileCustomerProductEntity entity = new MobileCustomerProductEntity();
            return entity;
        }

        private int productId;
        public int ProductId
        {
            get { return productId; }
            set { productId = value; }
        }

        private string customerCode;
        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; }
        }

        public int Quentity { get; set; }
        public string Type { get; set; }
        public DateTime EffectiveDate { get; set; }

    }
}
