﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileMarketObjectiveEntity:BaseEntity
    {
        public MobileMarketObjectiveEntity()
        {
            this.IsNew = true;
        }

        public static MobileMarketObjectiveEntity CreateObject()
        {
            MobileMarketObjectiveEntity entity = new MobileMarketObjectiveEntity();
            return entity;
        }

        private string marketName;
        public string MarketName
        {
            get { return marketName; }
            set { marketName = value; }
        }

        private string objectiveDescription;
        public string ObjectiveDescription
        {
            get { return objectiveDescription; }
            set { objectiveDescription = value; }
        }

        private string objectiveCode;
        public string ObjectiveCode
        {
            get { return objectiveCode; }
            set { objectiveCode = value; }
        }
    }
}
