﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileMonthlyActualsAndEarnings:BaseEntity
    {
        public MobileMonthlyActualsAndEarnings()
        {
            this.IsNew = true;
        }

        public static MobileMonthlyActualsAndEarnings CreateObject()
        {
            MobileMonthlyActualsAndEarnings entity = new MobileMonthlyActualsAndEarnings();
            return entity;
        }

        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }

        public double AchievedJanuary { get; set; }
        public double AchievedFebruary { get; set; }
        public double AchievedMarch { get; set; }
        public double AchievedApril { get; set; }
        public double AchievedMay { get; set; }
        public double AchievedJune { get; set; }
        public double AchievedJuly { get; set; }
        public double AchievedAugust { get; set; }
        public double AchievedSeptember { get; set; }
        public double AchievedOctober { get; set; }
        public double AchievedNovember { get; set; }
        public double AchievedDecember { get; set; }

        public double EarningsJanuary { get; set; }
        public double EarningsFebruary { get; set; }
        public double EarningsMarch { get; set; }
        public double EarningsApril { get; set; }
        public double EarningsMay { get; set; }
        public double EarningsJune { get; set; }
        public double EarningsJuly { get; set; }
        public double EarningsAugust { get; set; }
        public double EarningsSeptember { get; set; }
        public double EarningsOctober { get; set; }
        public double EarningsNovember { get; set; }
        public double EarningsDecember { get; set; }
    }
}
