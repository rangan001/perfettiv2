﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobilePerformanceSummaryEntity
    {

        public MobilePerformanceSummaryEntity()
        {
        }



        public static MobilePerformanceSummaryEntity CreateObject()
        {
            MobilePerformanceSummaryEntity entity = new MobilePerformanceSummaryEntity();
            return entity;
        }

        public List<MobileCustomerPerformanceEntity> PerformanceList {get; set;}
        public double TotalVolumeSold { get; set; }
        public double TotalSalesProfit { get; set; }
        public double AbishekaEarning { get; set; }
        public double OtherProEarning { get; set; }
    }
}
