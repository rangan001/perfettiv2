﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OpenItemsEntity : BaseEntity
    {
        public OpenItemsEntity()
        {            
        }

        public static OpenItemsEntity CreateObject()
        {
            OpenItemsEntity entity = new OpenItemsEntity();
            return entity;
        }

        public string CustCode { get; set; }
        public int AssigneeNo { get; set; }
        public string DocNo { get; set; }
        public DateTime DocDate { get; set; }
        public int TransType { get; set; }
        public double AmountDue { get; set; }
        public double OrigAmount { get; set; }
        public double GstAmount { get; set; }
        public double GstDiscAmount { get; set; }
        public double Discount { get; set; }
        public double AmountFc { get; set; }
        public double AmountDueFc { get; set; }
        public string TermsCode { get; set; }
        public DateTime DateDue { get; set; }
        public int ClaimNo { get; set; }
        public string CustRef { get; set; }
        public string Description { get; set; }
        public DateTime ImportDate { get; set; }
        public int Period { get; set; }
        public string DrForCur { get; set; }
        public double DrForRate { get; set; }
        public string CrForCur { get; set; }
        public double CrForRate { get; set; }
        public int BatchNo { get; set; }
        public int DepositNo { get; set; }
        public int IvceNo { get; set; }
        public int CreditNo { get; set; }
        public string GlCode { get; set; }
        public int ClosedPeriod { get; set; }
        public DateTime GraceDueDate { get; set; }
        public string Status { get; set; }
        public int Version { get; set; }
        public string CompanyCode { get; set; }

    }
}
