﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OriginatorRouteOutletsEntity:BaseEntity
    {
        private OriginatorRouteOutletsEntity()
        {
            
        }

        public static OriginatorRouteOutletsEntity CreateObject()
        {
            OriginatorRouteOutletsEntity entity = new OriginatorRouteOutletsEntity();

            return entity;
        }

        private OriginatorEntity originator = OriginatorEntity.CreateObject();
        public OriginatorEntity Originator
        {
            get { return originator; }
            set { originator = value; }
        }

        private RouteEntity route = RouteEntity.CreateObject();
        public RouteEntity Route
        {
            get { return route; }
            set { route = value; }
        }

        private List<CallCycleContactEntity> outlets = new List<CallCycleContactEntity>();
        public List<CallCycleContactEntity> Outlets
        {
            get { return outlets; }
            set { outlets = value; }
        }

        private string delFlag;
        public string DelFlag
        {
            get { return delFlag; }
            set { delFlag = value; }
        }

        private string ccType;
        public string CcType
        {
            get { return ccType; }
            set { ccType = value; }
        }
    }
}
