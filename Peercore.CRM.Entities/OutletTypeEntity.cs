﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OutletTypeEntity : BaseEntity
    {
        private OutletTypeEntity()
        {
            this.IsNew = true;
        }

        private OutletTypeEntity(int entityId)
        {
            this.OutletTypeId = entityId;
        }

        public static OutletTypeEntity CreateObject()
        {
            OutletTypeEntity entity = new OutletTypeEntity();

            return entity;
        }

        public static OutletTypeEntity CreateObject(int entityId)
        {
            OutletTypeEntity entity = new OutletTypeEntity(entityId);

            return entity;
        }

        public int OutletTypeId { get; set; }

        public string OutletTypeName { get; set; }
    }
}
