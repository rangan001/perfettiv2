﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PricelistEntity : BaseEntity
    {
        private PricelistEntity()
        {
            this.IsNew = true;
        }

        private PricelistEntity(int entityId)
        {
            this.PricelistID = entityId;
        }

        public static PricelistEntity CreateObject()
        {
            PricelistEntity entity = new PricelistEntity();
            return entity;
        }

        public static PricelistEntity CreateObject(int entityId)
        {
            PricelistEntity entity = new PricelistEntity(entityId);
            return entity;
        }

        public int PricelistID { get; set; }
        public string AreaFrom { get; set; }
        public string AreaTo { get; set; }
        public double Price { get; set; }
        public double Cost { get; set; }
    }
}
