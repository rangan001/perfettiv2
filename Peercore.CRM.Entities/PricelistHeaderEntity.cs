﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PricelistHeaderEntity : BaseEntity
    {
        private PricelistHeaderEntity()
        {
            this.IsNew = true;
        }

        private PricelistHeaderEntity(int entityId)
        {
            this.PricelistId = entityId;
        }

        public static PricelistHeaderEntity CreateObject()
        {
            PricelistHeaderEntity entity = new PricelistHeaderEntity();
            return entity;
        }

        public static PricelistHeaderEntity CreateObject(int entityId)
        {
            PricelistHeaderEntity entity = new PricelistHeaderEntity(entityId);
            return entity;
        }

        private int? pricelistId;
        private string pricelistName;
        private DateTime? effectiveDate;


        private List<PricelistDetailEntity> pricelistDetailList;

        public int? PricelistId 
        {
            get { return pricelistId; }
            set
            {
                pricelistId = value;
                IsDirty = true;
            }
        }

        public string PricelistName 
        {
            get { return pricelistName; }
            set
            {
                pricelistName = value;
                IsDirty = true;
            }
        }

        public DateTime? EffectiveDate 
        {
            get { return effectiveDate; }
            set
            {
                effectiveDate = value;
                IsDirty = true;
            }
        }

        public List<PricelistDetailEntity> PricelistDetailList
        {
            get
            {
                if (pricelistDetailList == null)
                    pricelistDetailList = new List<PricelistDetailEntity>();

                return pricelistDetailList;
            }
            set
            {
                pricelistDetailList = value;
            }
        }

        public int PlistNo { get; set; }
        public string SourceCode { get; set; }
        public int SourceNo { get; set; }
        public string CustCode { get; set; }
        public DateTime PlistDate { get; set; }
        public string SpecialInst { get; set; }
        public string AreaCode { get; set; }
        public string AuthCode { get; set; }
        public string BulkSched { get; set; }
        public string DefWarehouse { get; set; }
        public string ReleaseFlag { get; set; }
        public string TimeSlot { get; set; }
        public int SequenceNo { get; set; }
        public DateTime MixPalShedDate { get; set; }
        public string MixPalReadyFlag { get; set; }
        public DateTime BackorderedTime { get; set; }
        public string Status { get; set; }
        public int Version { get; set; }


    }
}
