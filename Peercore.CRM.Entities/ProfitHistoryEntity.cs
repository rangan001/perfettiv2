﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ProfitHistoryEntity : BaseEntity
    {
        private ProfitHistoryEntity()
        {
            this.IsNew = true;
        }



        public static ProfitHistoryEntity CreateObject()
        {
            ProfitHistoryEntity entity = new ProfitHistoryEntity();
            return entity;
        }


        public string State { get; set; }
        public string CustCode { get; set; }
        public string CatlogCode { get; set; }

        public double SalesVolP1 { get; set; }
        public double SalesVolP2 { get; set; }
        public double SalesVolP3 { get; set; }
        public double SalesVolP4 { get; set; }
        public double SalesVolP5 { get; set; }
        public double SalesVolP6 { get; set; }
        public double SalesVolP7 { get; set; }
        public double SalesVolP8 { get; set; }
        public double SalesVolP9 { get; set; }
        public double SalesVolP10 { get; set; }
        public double SalesVolP11 { get; set; }
        public double SalesVolP12 { get; set; }
        public double SalesVolP13 { get; set; }

        public double SalesValP1 { get; set; }
        public double SalesValP2 { get; set; }
        public double SalesValP3 { get; set; }
        public double SalesValP4 { get; set; }
        public double SalesValP5 { get; set; }
        public double SalesValP6 { get; set; }
        public double SalesValP7 { get; set; }
        public double SalesValP8 { get; set; }
        public double SalesValP9 { get; set; }
        public double SalesValP10 { get; set; }
        public double SalesValP11 { get; set; }
        public double SalesValP12 { get; set; }
        public double SalesValP13 { get; set; }

        public double SalesVolPp1 { get; set; }
        public double SalesVolPp2 { get; set; }
        public double SalesVolPp3 { get; set; }
        public double SalesVolPp4 { get; set; }
        public double SalesVolPp5 { get; set; }
        public double SalesVolPp6 { get; set; }
        public double SalesVolPp7 { get; set; }
        public double SalesVolPp8 { get; set; }
        public double SalesVolPp9 { get; set; }
        public double SalesVolPp10 { get; set; }
        public double SalesVolPp11 { get; set; }
        public double SalesVolPp12 { get; set; }
        public double SalesVolPp13 { get; set; }

        public double SalesValPp1 { get; set; }
        public double SalesValPp2 { get; set; }
        public double SalesValPp3 { get; set; }
        public double SalesValPp4 { get; set; }
        public double SalesValPp5 { get; set; }
        public double SalesValPp6 { get; set; }
        public double SalesValPp7 { get; set; }
        public double SalesValPp8 { get; set; }
        public double SalesValPp9 { get; set; }
        public double SalesValPp10 { get; set; }
        public double SalesValPp11 { get; set; }
        public double SalesValPp12 { get; set; }
        public double SalesValPp13 { get; set; }

        public double SalesVolYr1 { get; set; }
        public double SalesVolYr2 { get; set; }
        public double SalesVolYr3 { get; set; }
        public double SalesVolYr4 { get; set; }

        public double SalesValYr1 { get; set; }
        public double SalesValYr2 { get; set; }
        public double SalesValYr3 { get; set; }
        public double SalesValYr4 { get; set; }

        public double SalesCostsP1 { get; set; }
        public double SalesCostsP2 { get; set; }
        public double SalesCostsP3 { get; set; }
        public double SalesCostsP4 { get; set; }
        public double SalesCostsP5 { get; set; }
        public double SalesCostsP6 { get; set; }
        public double SalesCostsP7 { get; set; }
        public double SalesCostsP8 { get; set; }
        public double SalesCostsP9 { get; set; }
        public double SalesCostsP10 { get; set; }
        public double SalesCostsP11 { get; set; }
        public double SalesCostsP12 { get; set; }
        public double SalesCostsP13 { get; set; }

        public double SalesCostsPp1 { get; set; }
        public double SalesCostsPp2 { get; set; }
        public double SalesCostsPp3 { get; set; }
        public double SalesCostsPp4 { get; set; }
        public double SalesCostsPp5 { get; set; }
        public double SalesCostsPp6 { get; set; }
        public double SalesCostsPp7 { get; set; }
        public double SalesCostsPp8 { get; set; }
        public double SalesCostsPp9 { get; set; }
        public double SalesCostsPp10 { get; set; }
        public double SalesCostsPp11 { get; set; }
        public double SalesCostsPp12 { get; set; }
        public double SalesCostsPp13 { get; set; }

        public double SalesCostsYr1 { get; set; }
        public double SalesCostsYr2 { get; set; }
        public double SalesCostsYr3 { get; set; }
        public double SalesCostsYr4 { get; set; }

    }
}
