﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ProgramCustomerEntity :BaseEntity
    {
        public ProgramCustomerEntity()
        {
            this.IsNew = true;
        }

        public ProgramCustomerEntity(int entityId)
        {
            this.Id = entityId;
        }

        public static ProgramCustomerEntity CreateObject()
        {
            ProgramCustomerEntity entity = new ProgramCustomerEntity();
            return entity;
        }

        public static ProgramCustomerEntity CreateObject(int entityId)
        {
            ProgramCustomerEntity entity = new ProgramCustomerEntity(entityId);
            return entity;
        }

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public string CustCode { get; set; }
        public int Volume {get; set;}
        public string SelectionType { get; set; }
        public string ProgramName { get; set; }
    }
}
