﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PromotionEntity:BaseEntity
    {
        public PromotionEntity()
        {
        }

        public PromotionEntity(int entityId)
        {
        }

        public static PromotionEntity CreateObject()
        {
            PromotionEntity entity = new PromotionEntity();

            return entity;
        }

        public static PromotionEntity CreateObject(int entityId)
        {
            PromotionEntity entity = new PromotionEntity(entityId);

            return entity;
        }
        public int PromotionId { get; set; }
        public string PromotionName { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
        //public string Status { get; set; }
    }
}
