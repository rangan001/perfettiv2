﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PurchaseBehaviourEntity :BaseEntity
    {
        public PurchaseBehaviourEntity()
        {
            this.IsNew = true;
        }

        public static PurchaseBehaviourEntity CreateObject()
        {
            PurchaseBehaviourEntity entity = new PurchaseBehaviourEntity();
            return entity;
        }

        public int BehaviourId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
