﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class QuotationHeaderEntity : BaseEntity
    {
        private QuotationHeaderEntity()
        {
            this.IsNew = true;
        }

        private QuotationHeaderEntity(int entityId)
        {
            this.QuotationID = entityId;
        }

        public static QuotationHeaderEntity CreateObject()
        {
            QuotationHeaderEntity entity = new QuotationHeaderEntity();
            return entity;
        }

        public static QuotationHeaderEntity CreateObject(int entityId)
        {
            QuotationHeaderEntity entity = new QuotationHeaderEntity(entityId);
            return entity;
        }

        public int QuotationID { get; set; }
        public int ContactPersonID { get; set; }
        public int LeadID { get; set; }
        public int TemplateID { get; set; }
        public string ContainerLoad { get; set; }
        public string FreightType { get; set; }
        public int PricelistID { get; set; }
        public string DelCustomer { get; set; }
        public string DelAddress { get; set; }
        public string DelCity { get; set; }
        public string DelState { get; set; }
        public string DelPostcode { get; set; }
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string FromCity { get; set; }
        public string FromState { get; set; }
        public string FromPostcode { get; set; }
        public string CustomerCode { get; set; }
    }
}
