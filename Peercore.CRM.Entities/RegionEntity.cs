﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Entities
{
    public class RegionEntity : BaseEntity
    {
        public RegionEntity()
        {
        }

        public static RegionEntity CreateObject()
        {
            RegionEntity entity = new RegionEntity();
            return entity;
        }

        public static RegionEntity CreateObject(int entityId)
        {
            return RegionEntity.CreateObject(entityId);
        }

        public int RegionId { get; set; }
        public int CompId { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
    }

    public class RsmEntity : BaseEntity
    {
        public RsmEntity()
        {
        }

        public static RsmEntity CreateObject()
        {
            RsmEntity entity = new RsmEntity();
            return entity;
        }

        public int RsmId { get; set; }
        public int RegionId { get; set; }
        public string RsmCode { get; set; }
        public string RsmName { get; set; }
        public string Originator { get; set; }
        public string RsmTel1 { get; set; }
        public string RsmTel2 { get; set; }
        public string RsmEmail { get; set; }
    }
}
