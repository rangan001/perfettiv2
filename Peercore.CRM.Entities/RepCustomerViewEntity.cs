﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class RepCustomerViewEntity : BaseEntity
    {
        private RepCustomerViewEntity()
        {
            this.IsNew = true;
        }

        private RepCustomerViewEntity(int entityId)
        {
           // this.SOrderNo = entityId;
        }

        public static RepCustomerViewEntity CreateObject()
        {
            RepCustomerViewEntity entity = new RepCustomerViewEntity();
            return entity;
        }

        public static RepCustomerViewEntity CreateObject(int entityId)
        {
            RepCustomerViewEntity entity = new RepCustomerViewEntity(entityId);
            return entity;
        }

        public string State { get; set; }
        public string Originator { get; set; }
        public string Name { get; set; }
        public int NewCustomers { get; set; }
        public int CustomersInState { get; set; }
    }
}
