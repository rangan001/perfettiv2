﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class RepTargetEntity:BaseEntity
    {
        public RepTargetEntity()
        {
            this.IsNew = true;
        }

        public RepTargetEntity(int entityId)
        {
            this.RepTargetId = entityId;
        }

        public static RepTargetEntity CreateObject()
        {
            RepTargetEntity entity = new RepTargetEntity();
            return entity;
        }

        public static RepTargetEntity CreateObject(int entityId)
        {
            RepTargetEntity entity = new RepTargetEntity(entityId);
            return entity;
        }

        public int RepTargetId { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public double PrimaryTarAmt { get; set; }
        public double PrimaryIncAmt { get; set; }
        public DateTime? EffStartDate { get; set; }
        public DateTime? EffEndDate { get; set; }
        public string Status { get; set; }
        public int WeekNo { get; set; }

        public double ActualAmt { get; set; }

        public double CumActualAmt { get; set; }
        public double CumPrimaryTarAmt { get; set; }
        public string DescriptionforX { get; set; }
        public string DescriptionforY { get; set; }
    }
}
