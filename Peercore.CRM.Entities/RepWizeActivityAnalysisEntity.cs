﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class RepWizeActivityAnalysisEntity : BaseEntity
    {
         private RepWizeActivityAnalysisEntity()
        {
            this.IsNew = true;
        }

         private RepWizeActivityAnalysisEntity(int entityId)
        {
           // this.SOrderNo = entityId;
        }

         public static RepWizeActivityAnalysisEntity CreateObject()
        {
            RepWizeActivityAnalysisEntity entity = new RepWizeActivityAnalysisEntity();
            return entity;
        }

         public static RepWizeActivityAnalysisEntity CreateObject(int entityId)
        {
            RepWizeActivityAnalysisEntity entity = new RepWizeActivityAnalysisEntity(entityId);
            return entity;
        }

        public string RepName { get; set; }
        public Int64 LeadCount { get; set; }
        public Int64 ProspectCount { get; set; }
        public Int64 OppotunityCount { get; set; }
        public Int64 ActivityCount { get; set; }
        public Int64 ActivityCountNotCompleted { get; set; }
        public string ActivityStatus { get; set; }
        private int ratio = 0;

        public int Ratio
        {
            get { return ratio; }
            set { ratio = value; }
        }


        public List<Stages> StagesList { get; set; }


    }
    public class Stages
    {
        public string RepName { get; set; }
        public string StageName { get; set; }
        //public Int32 Count { get; set; }
        private int count = 0;

        public int Count
        {
            get
            {
                if (count == 0)
                    return 0;
                else
                    return count;
            }
            set { count = value; }
        }

    }
}
