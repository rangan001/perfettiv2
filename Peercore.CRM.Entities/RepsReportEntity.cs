﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class RepsOutletWiseDetailsEntity
    {
        public RepsOutletWiseDetailsEntity()
        {

        }

        public static RepsOutletWiseDetailsEntity CreateObject()
        {
            RepsOutletWiseDetailsEntity entity = new RepsOutletWiseDetailsEntity();
            return entity;
        }

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public string RouteName { get; set; }

        public string RepName { get; set; }

        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public double ProductQty { get; set; }

        public double ProductAmount { get; set; }

        //public List<RepsOutletWiseProductDetails> ProductDetails { get; set; }

        public int PaymentTypeCash { get; set; }

        public int PaymentTypeCredit { get; set; }

        public int PaymentTypeCheque { get; set; }

        public double DiscountTotal { get; set; }
    }

    public class RepsOutletWiseProductDetailsEntity
    {
        public RepsOutletWiseProductDetailsEntity()
        {

        }

        public static RepsOutletWiseProductDetailsEntity CreateObject()
        {
            RepsOutletWiseProductDetailsEntity entity = new RepsOutletWiseProductDetailsEntity();
            return entity;
        }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public double ProductQty { get; set; }

        public double ProductAmount { get; set; }
    }
    
    public class RepsSKUMasterEntity
    {
        public RepsSKUMasterEntity()
        {

        }

        public static RepsSKUMasterEntity CreateObject()
        {
            RepsSKUMasterEntity entity = new RepsSKUMasterEntity();
            return entity;
        }

        public string RepName { get; set; }

        public string RouteName { get; set; }

        public string BrandName { get; set; }

        public string CategoryName { get; set; }

        public string PackingName { get; set; }

        public string FlavourName { get; set; }

        public bool IsHighValue { get; set; }

        public string ProductCode { get; set; }

        public string ProductFGCode { get; set; }

        public string ProductName { get; set; }

        public double ProductQty { get; set; }

        public double ProductValue { get; set; }

        public double FreeIssues { get; set; }

        public double DiscountValue { get; set; }
    }
}
