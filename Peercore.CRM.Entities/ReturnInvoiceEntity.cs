﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ReturnInvoiceEntity
    {
        public ReturnInvoiceEntity()
        {
        }

        public static ReturnInvoiceEntity CreateObject()
        {
            ReturnInvoiceEntity entity = new ReturnInvoiceEntity();
            return entity;
        }

        public int Id { get; set; }

        public string returnno { get; set; }

        public double returntotal { get; set; }

        public string returndate { get; set; }

        public string canceled { get; set; }

        public int returntype { get; set; }

        public string remarks { get; set; }

        public double latitude { get; set; }

        public double longitude { get; set; }

        public List<ReturnInvoiceLineEntity> ReturnInvoiceLineList { get; set; }

        public List<ReturnInvoiceDetailsEntity> InvoiceDetailList { get; set; }

        public string RepCode { get; set; }

        public int RouteId { get; set; }

        public string RouteName { get; set; }
    }

    public class ReturnInvoiceLineEntity
    {
        public ReturnInvoiceLineEntity()
        {
        }

        public static ReturnInvoiceLineEntity CreateObject()
        {
            ReturnInvoiceLineEntity entity = new ReturnInvoiceLineEntity();
            return entity;
        }

        public int Id { get; set; }

        public int returnid { get; set; }

        public string invoiceno { get; set; }

        public double return_val { get; set; }

        public double return_amt { get; set; }
    }

    public class ReturnInvoiceDetailsEntity
    {
        public ReturnInvoiceDetailsEntity()
        {            
        }

        public static ReturnInvoiceDetailsEntity CreateObject()
        {
            ReturnInvoiceDetailsEntity entity = new ReturnInvoiceDetailsEntity();
            return entity;
        }

        public int Id { get; set; }

        public int returnid { get; set; }

        public string invoiceno { get; set; }

        public int productid { get; set; }

        public double itemprice { get; set; }

        public double qty { get; set; }

        public double total { get; set; }

        public string usable { get; set; }
    }
}
