﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SalesForceCustomerEntity: BaseEntity
    {
        public SalesForceCustomerEntity()
        {
            this.IsNew = true;
        }

        public SalesForceCustomerEntity(int entityId)
        {
        }

        public static SalesForceCustomerEntity CreateObject()
        {
            SalesForceCustomerEntity entity = new SalesForceCustomerEntity();

            return entity;
        }

        public static SalesForceCustomerEntity CreateObject(int entityId)
        {
            SalesForceCustomerEntity entity = new SalesForceCustomerEntity(entityId);

            return entity;
        }

        public string CustomerCode { get; set; }
        public string Name { get; set; }
    }
}
