﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Peercore.CRM.Entities
{
    public class SalesInfoDetailSearchCriteriaEntity : BaseEntity
    {
        private SalesInfoDetailSearchCriteriaEntity()
        {
            this.IsNew = true;
        }

        public static SalesInfoDetailSearchCriteriaEntity CreateObject()
        {
            SalesInfoDetailSearchCriteriaEntity entity = new SalesInfoDetailSearchCriteriaEntity();
            return entity;
        }

        /// <summary>
        /// month_select
        /// </summary>
        public string Month { get; set; }

        /// <summary>
        /// cost_period
        /// </summary>
        public int iCostPeriod { get; set; }

        /// <summary>
        /// Label catlog_type
        /// </summary>
        public string CatalogType { get; set; }

        /// <summary>
        /// Label cust_code
        /// </summary>
        public string CustomerCode { get; set; }

        /// <summary>
        /// Label cust_desc
        /// </summary>
        public string CustomerDescription { get; set; }

        /// <summary>
        /// Label rep_code
        /// </summary>
        public string RepCode { get; set; }

        /// <summary>
        /// Label rep_desc
        /// </summary>
        public string RepDescription { get; set; }

        /// <summary>
        /// full_names
        /// </summary>
        public bool DisplayFullName { get; set; }
        
        /// <summary>
        /// market
        /// </summary>
        public string Market { get; set; }

        /// <summary>
        /// brand
        /// </summary>
        public string Brand { get; set; }
        public string BrandDescription { get; set; }
        
        /// <summary>
        /// detail_type
        /// </summary>
        public string DetailType { get; set; }
        
        /// <summary>
        /// last_flag
        /// </summary>
        public string sLastFlag { get; set; }

        /// <summary>
        /// Lv_order
        /// </summary>
        public string SortField { get; set; }

        /// <summary>
        /// Gets or sets the c display option.
        /// </summary>
        /// <value>
        /// The cDisplayOption [ D-Dollar | T - Tonnes | U - Units ] 
        /// </value>
        public char cDisplayOption { get; set; }

        public bool showAllReps { get; set; }

        public string RepType { get; set; } // Rep Type (Bakery / Food Services)

        /// <summary>
        /// AdditionalCondition
        /// </summary>
        public string AdditionalCondition { get; set; }

        //public ArrayList RepCodes { get; set; }
        public string ChildReps { get; set; }

        public string Route { get; set; }
        public string RouteDescription { get; set; }

        public bool IsCMP { get; set; }
    }
}
