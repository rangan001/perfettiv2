﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SalesOrderEntity : BaseEntity
    {
        private SalesOrderEntity()
        {
            this.IsNew = true;
        }

        private SalesOrderEntity(int entityId)
        {
            this.SOrderNo = entityId;
        }

        public static SalesOrderEntity CreateObject()
        {
            SalesOrderEntity entity = new SalesOrderEntity();
            return entity;
        }

        public static SalesOrderEntity CreateObject(int entityId)
        {
            SalesOrderEntity entity = new SalesOrderEntity(entityId);
            return entity;
        }

        public int SOrderNo { get; set; }
        public int PListNo { get; set; }
        public DateTime DateRequired { get; set; }
        public string CatlogCode { get; set; }
        public string Description { get; set; }
        public DateTime OrderDate { get; set; }
        public double RequiredQty { get; set; }
        public double Price { get; set; }
    }
}
