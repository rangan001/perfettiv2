﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SessionEntity : BaseEntity
    {
        private SessionEntity()
        {
            this.IsNew = true;
        }

        private SessionEntity(int entityId)
        {
            this.SessionId = entityId;
        }

        public static SessionEntity CreateObject()
        {
            SessionEntity entity = new SessionEntity();
            return entity;
        }

        public static SessionEntity CreateObject(int entityId)
        {
            SessionEntity entity = new SessionEntity(entityId);
            return entity;
        }

        public int? SessionId { get; set; }
        public string UserName { get; set; }
        public DateTime LoginDateTime { get; set; }
        public DateTime LogoutDateTime { get; set; }
    }
}
