﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SorderDetailEntity : BaseEntity
    {
        public SorderDetailEntity()
        {            
        }

        public static SorderDetailEntity CreateObject()
        {
            SorderDetailEntity entity = new SorderDetailEntity();
            return entity;
        }

        public int SorderNo { get; set; }
        public int ItemNo { get; set; }
        public string CatlogCode { get; set; }
        public string SourceCode { get; set; }
        public int SourceNo { get; set; }
        public double OrderQty { get; set; }
        public double DespQty { get; set; }
        public double PalletQty { get; set; }
        public double BoQty { get; set; }
        public double Price { get; set; }
        public double Amount { get; set; }
        public double FcPrice { get; set; }
        public double FcAmount { get; set; }
        public string DiscType { get; set; }
        public double DiscPercent { get; set; }
        public double AggDiscPerc { get; set; }
        public double NetAmount { get; set; }
        public DateTime DateRequired { get; set; }
        public string RepCode { get; set; }
        public DateTime DateDespatched { get; set; }
        public int PlistNo { get; set; }
        public int IvceNo { get; set; }
        public string AnalysisCode { get; set; }
        public string Status { get; set; }
        public string ResStatus { get; set; }
        public string DealNo { get; set; }
        public string PickDc { get; set; }
        public string CustConNoKeyAcc { get; set; }
        public double ReservedQty { get; set; }
        public double NoChargeQty { get; set; }
    }
}
