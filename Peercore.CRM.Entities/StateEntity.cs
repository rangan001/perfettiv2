﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class StateEntity : BaseEntity
    {
        private StateEntity()
        {
            this.IsNew = true;
        }

        private StateEntity(int entityId)
        {
            this.StateID = entityId;
        }

        public static StateEntity CreateObject()
        {
            StateEntity entity = new StateEntity();
            return entity;
        }

        public static StateEntity CreateObject(int entityId)
        {
            StateEntity entity = new StateEntity(entityId);
            return entity;
        }

        private int? stateID;
        public int? StateID
        {
            get { return stateID; }
            set { stateID = value; }
        }

        private string stateName;
        public string StateName
        {
            get { return stateName; }
            set { stateName = value; }
        }

        private string countryCode;
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }

        private string stateAbbri;
        public string StateAbbri
        {
            get { return stateAbbri; }
            set { stateAbbri = value; }
        }

        private string stateCode;
        public string StateCode
        {
            get { return stateCode; }
            set { stateCode = value; }
        }
    }
}
