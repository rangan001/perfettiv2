﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class StockTotalEntity:BaseEntity
    {
        private StockTotalEntity()
        {
            this.IsNew = true;
        }

        private StockTotalEntity(int entityId)
        {
            this.stockTotalId = entityId;
        }

        public static StockTotalEntity CreateObject()
        {
            StockTotalEntity entity = new StockTotalEntity();
            return entity;
        }

        public static StockTotalEntity CreateObject(int entityId)
        {
            StockTotalEntity entity = new StockTotalEntity(entityId);
            return entity;
        }

        private int stockTotalId;
        public int StockTotalId
        {
            get { return stockTotalId; }
            set { stockTotalId = value; }
        }

        private int distributorId;
        public int DistributorId
        {
            get { return distributorId; }
            set { distributorId = value; }
        }

        private int catalogId;
        public int CatalogId
        {
            get { return catalogId; }
            set { catalogId = value; }
        }

        private string catalogCode;
        public string CatalogCode
        {
            get { return catalogCode; }
            set { catalogCode = value; IsDirty = true; }
        }

        private string catalogName;
        public string CatalogName
        {
            get { return catalogName; }
            set { catalogName = value; IsDirty = true; }
        }

        private double allocQty;
        public double AllocQty
        {
            get { return allocQty; }
            set { allocQty = value; IsDirty = true; }
        }

        private double loadQty;
        public double LoadQty
        {
            get { return loadQty; }
            set { loadQty = value; IsDirty = true; }
        }

        private double balanceQty;
        public double BalanceQty
        {
            get { return balanceQty; }
            set { balanceQty = value; IsDirty = true; }
        }

        private double returnQty;
        public double ReturnQty
        {
            get { return returnQty; }
            set { returnQty = value; IsDirty = true; }
        }

        private double vehicleBalanceQty;
        public double VehicleBalanceQty
        {
            get { return vehicleBalanceQty; }
            set { vehicleBalanceQty = value; IsDirty = true; }
        }

        private double vehicleReturnQty;
        public double VehicleReturnQty
        {
            get { return vehicleReturnQty; }
            set { vehicleReturnQty = value; IsDirty = true; }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public float CaseConfiguration { get; set; }
        public float TonnageConfiguration { get; set; }

    }
}
