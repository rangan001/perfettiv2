﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class TargetInfoEntity : BaseEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public string RepCode { get; set; }
        public string RepName { get; set; }

        public string CustCode { get; set; }
        public string CustName { get; set; }

        public List<int> SelectedMonth { get; set; }

        public double TargetMonth1 { get; set; }
        public double TargetMonth2 { get; set; }
        public double TargetMonth3 { get; set; }
        public double TargetMonth4 { get; set; }
        public double TargetMonth5 { get; set; }
        public double TargetMonth6 { get; set; }
        public double TargetMonth7 { get; set; }
        public double TargetMonth8 { get; set; }
        public double TargetMonth9 { get; set; }
        public double TargetMonth10 { get; set; }
        public double TargetMonth11 { get; set; }
        public double TargetMonth12 { get; set; }


        public double TargetYear1 { get; set; }
        public double TargetYear2 { get; set; }
        public double TargetTYTD { get; set; }
        public double TargetLYTD { get; set; }

        //This is use for grid binding.
        public double Target1 { get; set; }
        public double Target2 { get; set; }
        public double Target3 { get; set; }
        public double Target1Tot { get; set; }
        public double Target2Tot { get; set; }
        public double Target3Tot { get; set; }

        

        public double MDTot { get; set; }
        //public double TargetTot { get; set; }
        public double TargetYear1Tot { get; set; }
        public double TargetYear2Tot { get; set; }
        public double TargetTYTDTot { get; set; }
        public double TargetLYTDTot { get; set; }

        public double TargetTotal
        {
            get
            {
                return Math.Round(Target1 + Target2 + Target3 , 2);
            }
        }

        public double ActualTotal
        {
            get
            {
                return Math.Round(Actual1 + Actual2 + Actual3 , 2 );
            }
        }

        public double TargetPercentage
        {
            get
            {
                if (TargetLYTD == 0)
                    return 0;
                return Math.Round((TargetTYTD / TargetLYTD) * 100,2);
            }
        }
        public double ActualPercentage
        {
            get
            {
                if (ActualLYTD == 0)
                    return 0;
                return Math.Round((ActualTYTD / ActualLYTD) * 100, 2);
            }
        }

        public double ActualMonth1 { get; set; }
        public double ActualMonth2 { get; set; }
        public double ActualMonth3 { get; set; }
        public double ActualMonth4 { get; set; }
        public double ActualMonth5 { get; set; }
        public double ActualMonth6 { get; set; }
        public double ActualMonth7 { get; set; }
        public double ActualMonth8 { get; set; }
        public double ActualMonth9 { get; set; }
        public double ActualMonth10 { get; set; }
        public double ActualMonth11 { get; set; }
        public double ActualMonth12 { get; set; }


        public double ActualYear1 { get; set; }
        public double ActualYear2 { get; set; }
        public double ActualTYTD { get; set; }
        public double ActualLYTD { get; set; }

        public double Actual1 { get; set; }
        public double Actual2 { get; set; }
        public double Actual3 { get; set; }

        

        public double Actual1Tot { get; set; }
        public double Actual2Tot { get; set; }
        public double Actual3Tot { get; set; }



        //public double MDTot { get; set; }
        //public double ActualTot { get; set; }
        public double ActualYear1Tot { get; set; }
        public double ActualYear2Tot { get; set; }
        public double ActualTYTDTot { get; set; }
        public double ActualLYTDTot { get; set; }



        public double TargetMonth1Tot { get; set; }
        public double TargetMonth2Tot { get; set; }
        public double TargetMonth3Tot { get; set; }
        public double TargetMonth4Tot { get; set; }
        public double TargetMonth5Tot { get; set; }
        public double TargetMonth6Tot { get; set; }
        public double TargetMonth7Tot { get; set; }
        public double TargetMonth8Tot { get; set; }
        public double TargetMonth9Tot { get; set; }
        public double TargetMonth10Tot { get; set; }
        public double TargetMonth11Tot { get; set; }
        public double TargetMonth12Tot { get; set; }


        public double ActualMonth1Tot { get; set; }
        public double ActualMonth2Tot { get; set; }
        public double ActualMonth3Tot { get; set; }
        public double ActualMonth4Tot { get; set; }
        public double ActualMonth5Tot { get; set; }
        public double ActualMonth6Tot { get; set; }
        public double ActualMonth7Tot { get; set; }
        public double ActualMonth8Tot { get; set; }
        public double ActualMonth9Tot { get; set; }
        public double ActualMonth10Tot { get; set; }
        public double ActualMonth11Tot { get; set; }
        public double ActualMonth12Tot { get; set; }

        public double TargetTot
        {
            get
            {
                return Math.Round(Target1Tot + Target2Tot + Target3Tot ,2);
            }
        }

        public double ActualTot
        {
            get
            {
                return Math.Round(Actual1Tot + Actual2Tot + Actual3Tot ,2);
            }
        }


        public int RowCount { get; set; }


        public double TyTodayActual { get; set; }
        public double TyTtmtdActual { get; set; }
        public double TyLmtdActual { get; set; }
        public double LyTmtdActual { get; set; }

        public double TyTodayActualTot { get; set; }
        public double TyTtmtdActualTot { get; set; }
        public double TyLmtdActualTot { get; set; }
        public double LyTmtdActualTot { get; set; }


    }
}
