﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class TemplateComponentEntity : BaseEntity
    {
        private TemplateComponentEntity()
        {
            this.IsNew = true;
        }

        private TemplateComponentEntity(int entityId)
        {
            this.TemplateComponentId = entityId;
        }

        public static TemplateComponentEntity CreateObject()
        {
            TemplateComponentEntity entity = new TemplateComponentEntity();
            return entity;
        }

        public static TemplateComponentEntity CreateObject(int entityId)
        {
            TemplateComponentEntity entity = new TemplateComponentEntity(entityId);
            return entity;
        }

        private CostingComponentEntity component;
        private int? templateComponentId;
        private int? templateId;
        private int? componentId;


        public int? TemplateComponentId
        {
            get
            {
                return templateComponentId;
            }
            set
            {
                templateComponentId = value;
                IsDirty = true;
            }
        }

        public int? TemplateId
        {
            get
            {
                return templateId;
            }
            set
            {
                templateId = value;
                IsDirty = true;
            }
        }
        public int? ComponentId
        {
            get
            {
                return componentId;
            }
            set
            {
                componentId = value;
                IsDirty = true;
            }
        }
        public CostingComponentEntity Component
        {
            get
            {
                if (component == null)
                    component = CostingComponentEntity.CreateObject();

                return component;
            }
            set
            {
                component = value;
            }
        }
    }
}
