﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class UserPermissionEntity : BaseEntity
    {
        private UserPermissionEntity()
        {
            this.IsNew = true;
        }

        private UserPermissionEntity(int entityId)
        {
            this.UserPermissionId = entityId;
        }

        public static UserPermissionEntity CreateObject()
        {
            UserPermissionEntity entity = new UserPermissionEntity();
            return entity;
        }

        public static UserPermissionEntity CreateObject(int entityId)
        {
            UserPermissionEntity entity = new UserPermissionEntity(entityId);
            return entity;
        }

        private int? userPermissionId;
        public int? UserPermissionId
        {
            get { return userPermissionId; }
            set
            {
                userPermissionId = value;
                IsDirty = true;
            }
        }

        private string originatorCode;
        public string OriginatorCode
        {
            get { return originatorCode; }
            set
            {
                originatorCode = value; 
                IsDirty = true;
            }
        }

        private string userControlCode;
        public string UserControlCode
        {
            get { return userControlCode; }
            set
            {
                userControlCode = value; 
                IsDirty = true;
            }
        }

        private bool canCreate;
        public bool CanCreate
        {
            get { return canCreate; }
            set
            {
                canCreate = value;
                IsDirty = true;
            }
        }

        private bool canModifyOwn;
        public bool CanModifyOwn
        {
            get { return canModifyOwn; }
            set
            {
                canModifyOwn = value; 
                IsDirty = true;
            }
        }

        private bool canModifyOther;
        public bool CanModifyOther
        {
            get { return canModifyOther; }
            set
            {
                canModifyOther = value; 
                IsDirty = true;
            }
        }

        private bool canViewOwn;
        public bool CanViewOwn
        {
            get { return canViewOwn; }
            set
            {
                canViewOwn = value; 
                IsDirty = true;
            }
        }

        private bool canViewOther;
        public bool CanViewOther
        {
            get { return canDeleteOther; }
            set
            {
                canDeleteOther = value; 
                IsDirty = true;
            }
        }

        private bool canDeleteOwn;
        public bool CanDeleteOwn
        {
            get { return canDeleteOwn; }
            set
            {
                canDeleteOwn = value; 
                IsDirty = true;
            }
        }

        private bool canDeleteOther;
        public bool CanDeleteOther
        {
            get { return canDeleteOther; }
            set
            {
                canDeleteOther = value; 
                IsDirty = true;
            }
        }
    }
}
