﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class VisitChecklistMasterEntity:BaseEntity
    {
        public VisitChecklistMasterEntity()
        {
            this.IsNew = true;
        }

        public VisitChecklistMasterEntity(int entityId)
        {
            this.VisitChecklistMasterId = entityId;
        }

        public static VisitChecklistMasterEntity CreateObject()
        {
            VisitChecklistMasterEntity entity = new VisitChecklistMasterEntity();
            return entity;
        }

        public static VisitChecklistMasterEntity CreateObject(int entityId)
        {
            VisitChecklistMasterEntity entity = new VisitChecklistMasterEntity(entityId);
            return entity;
        }
        
        public int VisitChecklistMasterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public string Status { get; set; }
    }
}
