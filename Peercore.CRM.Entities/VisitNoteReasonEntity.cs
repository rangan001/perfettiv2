﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class VisitNoteReasonEntity : BaseEntity
    {
        private VisitNoteReasonEntity()
        {
            this.IsNew = true;
        }

        private VisitNoteReasonEntity(int entityId)
        {
            this.VisitNoteReasonID = entityId;
        }

        public static VisitNoteReasonEntity CreateObject()
        {
            VisitNoteReasonEntity entity = new VisitNoteReasonEntity();

            return entity;
        }

        public static VisitNoteReasonEntity CreateObject(int entityId)
        {
            VisitNoteReasonEntity entity = new VisitNoteReasonEntity(entityId);

            return entity;
        }

        public int VisitNoteReasonID { get; set; }
        public string Reason { get; set; }
    }
}
