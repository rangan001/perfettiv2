﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace Peercore.CRM.MobileService.Converters
{
    public class EncryptDecryptData
    {
        //public const string _secretPhrase = "Peercore";
        /// <summary>
        /// DES Encryption method - used to encryp password for the java.
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public string EncryptData(string plainText, string _secretPhrase)
        {
            DES des = new DESCryptoServiceProvider();
            des.Mode = CipherMode.ECB;
            des.Padding = PaddingMode.PKCS7;

            des.Key = Encoding.UTF8.GetBytes(_secretPhrase.Substring(0, 8));
            des.IV = Encoding.UTF8.GetBytes(_secretPhrase.Substring(0, 8));

            byte[] bytes = Encoding.UTF8.GetBytes(plainText);
            byte[] resultBytes = des.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length);

            return Convert.ToBase64String(resultBytes);
        }

        /// <summary>
        /// DES Decryption method - used the decrypt password encrypted in java
        /// </summary>
        /// <param name="encryptedText"></param>
        /// <returns></returns>
        public string DecryptData(string encryptedText, string _secretPhrase)
        {
            DES des = new DESCryptoServiceProvider();
            des.Mode = CipherMode.ECB;
            des.Padding = PaddingMode.PKCS7;
            des.Key = Encoding.UTF8.GetBytes(_secretPhrase.Substring(0, 8));
            des.IV = System.Text.Encoding.UTF8.GetBytes(_secretPhrase.Substring(0, 8));

            byte[] bytes = Convert.FromBase64String(encryptedText);
            byte[] resultBytes = des.CreateDecryptor().TransformFinalBlock(bytes, 0, bytes.Length);

            return Encoding.UTF8.GetString(resultBytes);
        }

        public string GenerateCoupon(int length)
        {
            int gbv = Int32.Parse(DateTime.Now.ToString("yyyyMMddHH"));
            //Random random = new Random(gbv);
            Random random = new Random((int)DateTime.Now.Ticks);
            string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }
        public string GetVoucherNumber(int length)
        {
            //int gbv = Int32.Parse(DateTime.Now.ToString("yyyyMMddHH"));
            //Random random = new Random(Int32.Parse(DateTime.Now.ToString("yyyyMMddHH")));
            Random random = new Random(GetIndex());

            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }
        public int GetIndex()
        {
            DateTime cusrrentDate = DateTime.Now;

            cusrrentDate = cusrrentDate.AddMinutes(-(5 + cusrrentDate.Minute % 5));

            int index = Int32.Parse(cusrrentDate.ToString("yyMMddHHmm"));

            return index;
        }
    }
}