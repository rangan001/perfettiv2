﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class OriginatorAttendanceReasonsResponse : ResponseBaseModel
    {
        public List<AttendanceReasons> reasons { get; set; }
    }

}