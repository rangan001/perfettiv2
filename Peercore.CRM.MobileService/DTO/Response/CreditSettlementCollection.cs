﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class CreditSettlementCollection : ResponseBaseModel
    {
        public List<CreditSettlement> CreditSettlements { get; set; }
    }
}