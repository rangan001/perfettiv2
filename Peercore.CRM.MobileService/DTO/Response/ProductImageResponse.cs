﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ProductImageResponse : ResponseBaseModel
    {
        //public byte[] Image { get; set; }
        public string Image { get; set; }
    }
}