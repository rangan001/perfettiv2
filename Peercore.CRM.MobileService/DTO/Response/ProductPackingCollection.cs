﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ProductPackingCollection : ResponseBaseModel
    {
        public List<ProductPacking> ProductPackings { get; set; }
    }
}