﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;
using Peercore.CRM.Model;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ProductUOMCollection : ResponseBaseModel
    {
        public List<ProductUOMModel> ProductUOMs { get; set; }

    }
}
