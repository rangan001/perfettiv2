﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.Model;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ProductUOMCollectionResponse : ResponseBaseModel

    {
        public List<Model.ProductUOMViewModel> UOMCollection { get; set; }
    }
}