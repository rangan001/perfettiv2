﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public abstract class ResponseBaseModel
    {
        public bool IsError { get; set; }

        public string ErrorMessage { get; set; }

        //public string ErrorDescription { get; set; }
    }
}