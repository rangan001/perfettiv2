﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.Entities;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class TargetVsActualResponse : ResponseBaseModel
    {
        public double Target { get; set; }
        public double Actual { get; set; }
        public double Balance { get; set; }
        //public int WeekNo { get; set; }
        public List<RepTargetEntity> repTargetList { get; set; }
    }
}