﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class UserLoginCollectionResponse : ResponseBaseModel
    {
        public List<UserLogin> UserLoginCollection { get; set; }
    }

    public class UserRouteCollectionResponse : ResponseBaseModel
    {
        public List<UserRoute> UserRouteCollection { get; set; }
    }

    public class InsertRepEMEIResponse : ResponseBaseModel
    {
        
    }
    
    public class UpdateRepEMEIResponse : ResponseBaseModel
    {
        
    }

    public class UpdateRepsSyncedStatusResponse : ResponseBaseModel
    {
        
    }

    public class UpdateDeviceIdByEmei : ResponseBaseModel
    {
        
    }

    public class OriginatorCollectionResponse : ResponseBaseModel
    {
        public List<Originator> RepsCollection { get; set; }
    } 
    
    public class RepLocationsResponse : ResponseBaseModel
    {
        public string LastLat { get; set; }

        public string LastLong { get; set; }
 
        public List<RepLocation> RepsLocations { get; set; }
    }

    public class DailyRepInvoiceCountResponse : ResponseBaseModel
    {
    }
}