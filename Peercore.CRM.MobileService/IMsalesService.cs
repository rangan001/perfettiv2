﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Peercore.CRM.MobileService.DTO.Response;
using Peercore.CRM.MobileService.Models;
using System.IO;
using Peercore.CRM.Model;

namespace Peercore.CRM.MobileService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMsalesService" in both code and config file together.
    [ServiceContract]
    public interface IMsalesService
    {
        [OperationContract]
        string ApkVersion();

        [OperationContract]
        UserLoginCollectionResponse GetUserLoginCollection(string accessToken);

        [OperationContract]
        UserRouteCollectionResponse GetDailyRoute(string accessToken);

        [OperationContract]
        CustomerCollectionResponse GetCustomerCollection(string accessToken);

        [OperationContract]
        ProductCollectionResponse GetProductCollection(string accessToken, string syncDate);

        [OperationContract]
        ProductImageResponse GetProductImage(string accessToken, int productID);

        [OperationContract]
        ProductBrandCollectionResponse GetProductBrandCollection(string accessToken, string syncDate);

        [OperationContract]
        ProductBrandImageResponse GetProductBrandImageResponse(string accessToken, int productBrandID);

        [OperationContract]
        ProductCategoryCollectionResponse GetProductCategoryCollection(string accessToken);

        [OperationContract]
        ProductPackingCollection GetProductPackingCollection(string accessToken);

        [OperationContract]
        ProductFlavourCollection GetProductFlavourCollection(string accessToken);

        [OperationContract]
        InvoiceCollection GetInvoiceCollection(string accessToken);

        [OperationContract]
        NewInvoiceResponse AddInvoice(string accessToken, Invoice newInvoice);

        [OperationContract]
        NewCreditSettlementResponse InsertCreditSettlement(string accessToken, CreditSettlement creditSettlement);

        [OperationContract]
        DiscountSchemeResponse GetDiscountSchemeResponse(string accessToken);

        [OperationContract]
        InvoiceResponse DeleteInvoiceHeader(string accessToken, string invoiceNo, string Latitude = "", string Longitude = "");

        [OperationContract]
        OutletTypeCollectionResponse GetOutletTypeCollection(string accessToken);

        [OperationContract]
        NewCustomerResponse AddCustomer(string accessToken, Customer newCustomer);

        [OperationContract]
        UserLoginCollectionResponse GetUserLoginDetails(string userName, string password, string emei);

        [OperationContract]
        UserLoginCollectionResponse GetUserEmeiStatus(string userName, string emei);

        //[OperationContract]
        //NewDocumentResponse AddCustomerImage( Stream image);

        //[OperationContract]
        //NewDocumentResponse AddCustomerImage1(string accessToken, InvoiceTest newCustomer);

        [OperationContract]
        NewDocumentResponse AddCustomerImage(string custCode, string accessToken, string imageByte);

        [OperationContract]
        VisitNoteReasonCollectionResponse GetVisitNoteReasonCollection(string accessToken);

        [OperationContract]
        NewVisitNoteResponse AddVisitNote(string accessToken, VisitNote newVisitNote);

        [OperationContract]
        TargetVsActualResponse GetRepTargetVsActual(string accessToken);

        [OperationContract]
        TargetVsActualResponse GetRepTargetVsActualNew(string accessToken);

        [OperationContract]
        StockTotalCollectionResponse GetStockTotalCollection(string accessToken);

        [OperationContract]
        InvoiceResponse CancelCreditSettlement(string accessToken, string settlementNo, string Latitude = "", string Longitude = "");

        [OperationContract]
        ReturnInvoicesResponse ReturnInvoices(string accessToken, ReturnInvoices newReturnInvoice);

        [OperationContract]
        ReturnInvoicesResponse CancelReturnInvoice(string accessToken, string returnno, string Latitude = "", string Longitude = "");

        [OperationContract]
        CheckListResponse GetChecklistItemCollection(string accessToken);

        [OperationContract]
        CustomerChecklistResponse AddCustomerChecklistDetails(string accessToken, CheckListDetail newCheckList);

        [OperationContract]
        UpdateRepEMEIResponse UpdateRepEMEI(string accessToken, string emei, string pin);

        [OperationContract]
        InsertRepEMEIResponse InsertRepEMEI(string userName, string emei, string pin);

        [OperationContract]
        InvoiceCollection GetInvoiceCollectionforASM(string accessToken, string date);

        [OperationContract]
        OriginatorCollectionResponse GetRepsCollection(string accessToken);

        [OperationContract]
        VisitNoteCollection GetVisitNoteCollectionforASM(string accessToken, string date);

        [OperationContract]
        VisitNoteCollection GetVisitNoteCollection(string accessToken, string date);

        [OperationContract]
        void InsertRepLocation(string accessToken, string CurrLat, string CurrLong);

        [OperationContract]
        UpdateDeviceIdByEmei UpdateDeviceIdByEmei(string accessToken, string emei, string deviceId);

        [OperationContract]
        string NewPinCode(string accessToken, string Latitude = "", string Longitude = "");

        [OperationContract]
        UpdateRepsSyncedStatusResponse UpdateRepsSyncedStatus(string RepCode, string emei, bool IsSync, string ApkVersion);

        [OperationContract]
        DailyRepInvoiceCountResponse InsertDailyRepInvoiceCount(string datetime, string repcode, string emei, int invoicecount, int delinvoicecount);

        [OperationContract]
        RepsTrackingForASM GetRepsTrackingForASM(string accessToken, string date);

        [OperationContract]
        ASMDashBoardResponse GetDailyRepsSalesforASM(string accessToken, string date);

        [OperationContract]
        OutletWiseReportResponse GetOutletWiseReportforRep(string accessToken, string date);

        [OperationContract]
        SKUWiseReportResponse GetSKUWiseReportforRep(string accessToken, string date);

        [OperationContract]
        NewOrderResponse AddOrder(string accessToken, OrderModel newOrder);

        [OperationContract]
        OrderCollection GetOrderCollection(string accessToken);

        [OperationContract]
        OrderResponse DeleteOrderHeader(string accessToken, string orderNo, string Latitude = "", string Longitude = "");

        [OperationContract]
        SRSyncResponse UpdateSRSyncDetail(string accessToken, SalesRepSyncDetailModel srSyncDet);

        [OperationContract]
        DistributerCollection GetDistributerList(string accessToken);

        [OperationContract]
        OrigivatorVisitResponse UpdateOrigivatorVisitDetail(string accessToken, OrigivatorVisitModel visitDet);

        [OperationContract]
        ASMVisitCollection GetAllASMVisitsList(string accessToken);

        [OperationContract]
        SKUWiseReportResponse GetSKUWiseOrderReportforRep(string accessToken, string date);

        [OperationContract]
        OutletWiseReportResponse GetOutletWiseOrderReportforRep(string accessToken, string date);

        [OperationContract]
        DiscountSchemeResponse GetDiscountSchemeForPreSalesResponse(string accessToken);

        [OperationContract]
        OrigivatorAttendanceResponse UpdateOriginatorAttendanceCheckOut(string accessToken, OriginatorAttendance attendanceDet);

        [OperationContract]
        OrigivatorAttendanceResponse UpdateOriginatorAttendanceCheckIn(string accessToken, OriginatorAttendance attendanceDet);

        [OperationContract]
        TransactionLogResponse CreateTransactionLogMobile(string accessToken, TransactionLogModel transLog);

        [OperationContract]
        OriginatorAttendanceReasonsResponse GetAttendanceReasons(string accessToken);

        [OperationContract]
        TransactionLogResponse InsertOriginatorAttendanceReason(string accessToken, OriginatorAttendanceReasons attenRequest);

        [OperationContract]
        ModernTradeResponse GetAllModernTradesByAccessToken(string accessToken, string date);

        [OperationContract]
        ModernTradeResponse GetAllModernTradesByAccessTokenAndPeriod(string accessToken, string startDate, string endDate);

        [OperationContract]
        ProductCollectionResponse GetProductCollectionForModernTrade(string accessToken, string syncDate);

        [OperationContract]
        ModernTradeInsertResponse InsertModernTrade(string accessToken, ModernTradeMasterModel request);

        [OperationContract]
        InvoiceResponse DeleteModernTradeHeader(string accessToken, ModernTradeMasterModel request);

        [OperationContract]
        UpdateCustomerDayOrderResponse UpdateCustomerDayOrderByRep(string accessToken, int routeId, List<CustomerSequence> customerCollection, string Latitude = "", string Longitude = "");

        [OperationContract]
        MTCustomerImageUploadResponse ImageUploadedByMTCustomers(string accessToken, MTCustomerImages mtImages);

        //Added by Rangan 14/09/2023
        [OperationContract]
        ProductUOMCollectionResponse GetProductUOMCollection(string accessToken);

        [OperationContract]
        CustomerECOCollectionResponse GetCustomerECOCumulative(string accessToken, string strDate, string endDate);


        [OperationContract]
        BrandECOCollectionResponse GetBrandECOCumulative(string accessToken, string strDate, string endDate);

        [OperationContract]
        CategoryECOCollectionResponse GetCategoryECOCumulative(string accessToken, string strDate, string endDate);
        [OperationContract]
        ProductECOCollectionResponse GetProductECOCumulative(string accessToken, string strDate, string endDate);
    }
}
