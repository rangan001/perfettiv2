﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class CheckListDetail
    {
        public int CheckListID { get; set; }
        public string Remarks { get; set; }
        public string CompletedOn { get; set; }
        public string CustomerCode { get; set; }
        public string RepCode { get; set; }
        public int routeId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}