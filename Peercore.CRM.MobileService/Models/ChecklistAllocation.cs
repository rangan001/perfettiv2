﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class ChecklistAllocation
    {
        public int CheckListID { get; set; }
        public string CustomerCode { get; set; }
    }
}