﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class CreditSettlement
    {
        public string AddedOn { get; set; }

        public double CashAmount { get; set; }

        public double CreditAmount { get; set; }

        public Boolean IsChequeReceived { get; set; } 

        public List<Cheque> ChequeLineItemCollection { get; set; }

        public string InvoiceNo { get; set; }

        public string DueDate { get; set; } 

        //Add Date : 2016/6/24
        public string CustCode { get; set; }

        public double ChequeAmount { get; set; }

        public string status_pay { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }

        public string remarks { get; set; }

        public string credit_settle_no { get; set; }

        public bool canceled { get; set; }

        public List<CreditSettlementDetails> CreditSettlementDetailsCollection { get; set; }

        public string RepCode { get; set; }

        public int routeId { get; set; }

        public string routeName { get; set; }
    }

    //Add Date : 2016/6/24
    public class CreditSettlementDetails
    {
        public string InvoiceNo { get; set; }

        public int id { get; set; }

        public int credit_settlement_id { get; set; }

        public int invoice_id { get; set; }

        public double outstand_amt { get; set; }

        public double pay_amt { get; set; }
    }

    public class CreditSettlementNew
    {
        public string AddedOn { get; set; }

        public double CashAmount { get; set; }

        public double CreditAmount { get; set; }
        
        public string InvoiceNo { get; set; }

        public string DueDate { get; set; }
    }
}