﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class Customer
    {
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CreatedOn { get; set; }
        public string OutletNo { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public double CreditLimit { get; set; }
        public double OutstandingCredit { get; set; }
        public double OutstandingBalance { get; set; }
        public bool IsRetail { get; set; }
        public int OutletTypeId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string ImageUrl { get; set; }
        public bool HasImage { get; set; }
        public bool HasUnlimitedCredit { get; set; }
        public string imageByte { get; set; }
        public string RepCode { get; set; }
        public int routeId { get; set; }
        public int custType { get; set; }
        public int dayOrder { get; set; }

        public string isPC { get; set; }

    }

    public class MTCustomerImages
    {
        public string custCode { get; set; }
        public string description { get; set; } = "";
        public List<string> imgContent { get; set; }
    }
}