﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class DiscountSchemeDetail
    {
        public int DiscountSchemeDetailID { get; set; }

        public DiscountSchemeDetailType SchemeDetailType { get; set; }

        public int SchemeTypeId { get; set; }

        public string SchemeDetail { get; set; }

        public double? DiscountValue { get; set; }

        public double? DiscountPercentage { get; set; }
        
        public string FreeIssueCondition { get; set; }

        public List<DiscountSchemeFreeProduct> FreeProductCollection { get; set; }

        public bool? IsRetail { get; set; }

        public List<DiscountSchemeCondition> ConditionCollection { get; set; }
    }
}