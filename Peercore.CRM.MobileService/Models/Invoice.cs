﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Peercore.CRM.MobileService.Models
{
    public class Invoice
    {
        public string InvoiceNo { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public double InvoiceTotal { get; set; }
        public double TotalDiscount { get; set; }
        public string InvoiceDate { get; set; }

        public List<InvoiceLineItem> InvoiceLineItemCollection { get; set; }
        public List<InvoiceDiscountSchemeGroup> DiscountSchemeGroupCollection { get; set; }
        public List<CreditSettlement> InvoiceCreditSettlementList { get; set; }
        public List<ReturnInvoices> ReturnInvoiceList { get; set; }
        public CreditSettlement InvoiceCreditSettlement { get; set; }
        public CreditSettlementNew InvoiceCreditSettlementNew { get; set; }
        public ReturnInvoices ReturnInvoice { get; set; }

        public bool Canceled { get; set; }
        public string Remark { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string RouteName { get; set; }
        public string DistributorName { get; set; }
        public string RepCode { get; set; }
        public int routeId { get; set; }
        public int territoryId { get; set; }
        public int areaId { get; set; }
        public int regionId { get; set; }
        public int distributorId { get; set; }
        public int asmId { get; set; }
        public int rsmId { get; set; }
        public string OrderNo { get; set; }
        public double QtyCasess { get; set; } // added by rangan

        public double QtyTonnage { get; set; } // added by rangan
    }

    //public class InvoiceTest
    //{
    //    public string ImageByte { get; set; }
    //    public string AccessToken { get; set; }
    //    public string CustCode { get; set; }
    //}

    public class RepsTrackingDetails
    {
        public string CustomerCode { get; set; }

        public string InvoiceDate { get; set; }

        public double InvoiceTotal { get; set; }

        public double TotalDiscount { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public string DistributorName { get; set; }

        public string RepCode { get; set; }

        public string RepName { get; set; }

        public int PC { get; set; }

        public int VC { get; set; }
    }

    public class RepsOutletWiseDetails
    {
        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public List<RepsOutletWiseProductDetails> ProductDetails { get; set; }

        public int PaymentTypeCash { get; set; }

        public int PaymentTypeCredit { get; set; }

        public int PaymentTypeCheque { get; set; }

        public double TotalDiscounts { get; set; }

        public string RepName { get; set; }

        public string RouteName { get; set; }
    }
    
    public class RepsOutletWiseProductDetails
    {
        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public double ProductQty { get; set; }

        public double ProductAmount { get; set; }
    }

    public class RepsSKUMaster
    {
        public string RepName { get; set; }

        public string RouteName { get; set; }

        public List<RepsSKUDetails> SKUDetails { get; set; }
    }

    public class RepsSKUDetails
    {
        public string BrandName { get; set; }

        public string CategoryName { get; set; }

        public string PackingName { get; set; }

        public string FlavourName { get; set; }

        public bool IsHighValue { get; set; }

        public string ProductCode { get; set; }

        public string ProductFGCode { get; set; }

        public string ProductName { get; set; }

        public double ProductQty { get; set; }

        public double ProductValue { get; set; }

        public double FreeIssues { get; set; }

        public double DiscountValue { get; set; }
    }


}