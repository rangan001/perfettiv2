﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class InvoiceDiscountScheme
    {
        public int ProductID { get; set; }
        
        public double UsedQty { get; set; }

        public double DiscountVal { get; set; }
    }
}