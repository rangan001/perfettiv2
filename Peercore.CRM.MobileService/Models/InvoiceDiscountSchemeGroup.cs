﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class InvoiceDiscountSchemeGroup
    {
        public int DiscountSchemeGroupID { get; set; }

        public double DiscountValue { get; set; }

        public int InvoiceID { get; set; }

        public int DiscountSchemeDetailID { get; set; }

        public List<InvoiceDiscountScheme> DiscountSchemeCollection { get; set; }
    }
}