﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class ProductBrand
    {
        public int BrandID { get; set; }

        public string BrandCode { get; set; }

        public string BrandName { get; set; }

        public string  ImageUrl { get; set; }
        
        public string  Status { get; set; }

        public string SyncDate { get; set; }
    }
}