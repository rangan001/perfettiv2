﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class ProductFlavour
    {
        public int FlavourID { get; set; }

        public string FlavourName { get; set; }
    }
}