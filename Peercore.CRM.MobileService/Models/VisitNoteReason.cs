﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class VisitNoteReason
    {
        public int VisitNoteReasonId { get; set; }

        public string Reason { get; set; }
    }
}