﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class AreaModel : BaseModel
    {
        public int AreaId { get; set; }
        //public int RegionId { get; set; }
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        public int RegionId { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string RegionStatus { get; set; }
    }

    public class ASMModel : BaseModel
    {
        public int RsmId { get; set; }
        public int RegionId { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string RsmCode { get; set; }
        public string RsmName { get; set; }
        public string RsmUserName { get; set; }
        public string RsmPassword { get; set; }
        public string RsmTel1 { get; set; }
        public string RsmTel2 { get; set; }
        public string RsmEmail { get; set; }
    }
}
