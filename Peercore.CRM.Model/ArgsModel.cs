﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class ArgsModel
    {
        private int startIndex;
        public int StartIndex
        {
            get { return startIndex; }
            set { startIndex = value; }
        }

        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; }
        }

        private int showCount;
        public int ShowCount
        {
            get { return showCount; }
            set { showCount = value; }
        }

        private string orderBy;
        public string OrderBy
        {
            get { return orderBy; }
            set { orderBy = value; }
        }

        private string additionalParams;
        public string AdditionalParams
        {
            get { return additionalParams; }
            set { additionalParams = value; }
        }

        private string sStartDate = string.Empty;
        public string SStartDate
        {
            get
            {
                return sStartDate;
            }
            set
            {
                if (sStartDate == value)
                    return;

                sStartDate = value;
            }
        }

        private string sEndDate = string.Empty;
        public string SEndDate
        {
            get
            {
                return sEndDate;
            }
            set
            {
                if (sEndDate == value)
                    return;

                sEndDate = value;
            }
        }

        private bool activeInactiveChecked;
        public bool ActiveInactiveChecked
        {
            get
            {
                return activeInactiveChecked;
            }
            set
            {
                if (activeInactiveChecked == value)
                    return;

                activeInactiveChecked = value;
            }
        }

        private string childOriginators;
        public string ChildOriginators
        {
            get { return childOriginators; }
            set { childOriginators = value; }
        }

        private string originator;
        public string Originator
        {
            get { return originator; }
            set { originator = value; }
        }

        private string sToday = string.Empty;
        public string SToday
        {
            get
            {
                return sToday;
            }
            set
            {
                if (sToday == value)
                    return;

                sToday = value;
            }
        }

        private string sSource = string.Empty;
        public string SSource
        {
            get
            {
                return sSource;
            }
            set
            {
                if (sSource == value)
                    return;

                sSource = value;
            }
        }

        private string floor;
        public string Floor
        {
            get
            {
                return floor;
            }
            set
            {
                if (floor == value)
                    return;

                floor = value;
            }
        }

        private string status = string.Empty;
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                if (status == value)
                    return;

                status = value;
            }
        }

        private string defaultDepartmentId;
        public string DefaultDepartmentId
        {
            get { return defaultDepartmentId; }
            set { defaultDepartmentId = value; }
        }
        public string RepCode { get; set; }

        private string sQuantitytype = string.Empty;
        public string SQuantitytype
        {
            get
            {
                return sQuantitytype;
            }
            set
            {
                if (sQuantitytype == value)
                    return;

                sQuantitytype = value;
            }
        }
    }
}
