﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class AsmModel : BaseModel
    {
        public int AsmId { get; set; }
        public int AreaId { get; set; }
        public string AreaStatus { get; set; }
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        public string AsmCode { get; set; }
        public string AsmName { get; set; }
        public string AsmUserName { get; set; }
        public string AsmPassword { get; set; }
        public string AsmTel1 { get; set; }
        public string AsmTel2 { get; set; }
        public string AsmEmail { get; set; }
        public bool IsNotIMEIuser { get; set; }
    }
}
