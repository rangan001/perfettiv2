﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class AssetModel : BaseModel
    {
        public int AssetId { get; set; }
        public int AssetTypeId { get; set; }
        public string AssetTypeName { get; set; }
        public string AssetCode { get; set; }
        public string Model { get; set; }
        public string IMEI_1 { get; set; }
        public string IMEI_2 { get; set; }
        public string SerialNo { get; set; }
        public string CompAssetCode { get; set; }
        public string PrinterPouch { get; set; }
        public string MonitorModel { get; set; }
        public string MonitorSerial { get; set; }
        public string SIM { get; set; }
        public string Supplier { get; set; }
        public string InvoiceNo { get; set; }
        public string ValueLKR { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string Warranty { get; set; }
        public int AsssetStatus { get; set; }
        public int OtherStatus { get; set; }
        public string Description { get; set; }
    }

    public class AssetTypeModel : BaseModel
    {
        public int AssetTypeId { get; set; }
        public string AssetTypeName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }

    public class AssetAssignModel : BaseModel
    {
        public int AssignId { get; set; }
        public string UserType { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public int AssetTypeId { get; set; }
        public string AssetTypeName { get; set; }
        public string AssetCode { get; set; }
        public double RefundCost { get; set; }
        public int Status { get; set; }
        public int OtherStatus { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public string Agreement { get; set; }
        public string Remark { get; set; }

    }

    public class AssetStatusModel : BaseModel
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }

}
