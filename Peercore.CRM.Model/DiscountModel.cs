﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class SchemeHeaderModel : BaseModel
    {
        public int SchemeHeaderId { get; set; }
        public string SchemeName { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SchemeDetailsModel> SchemeDetailsList { get; set; }
    }

    public class SchemeDetailsModel : BaseModel
    {
        public int SchemeDetailsId { get; set; }
        public int SchemeHeaderId { get; set; }
        public int DetailsTypeId { get; set; }
        public int SchemeTypeId { get; set; }
        public string SchemeDetails { get; set; }
        public double DiscountValue { get; set; }
        public double DiscountPercentage { get; set; }
        public bool? IsRetail { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public List<OptionLevelModel> OptionLevelList { get; set; }
        public string SchemeHeaderName { get; set; }
        public string DetailsTypeName { get; set; }
        public string SchemeTypeName { get; set; }
        public List<FreeProductModel> FreeProductList { get; set; }
        public int IndexId { get; set; }
    }

    public class OptionLevelModel : BaseModel
    {
        public int OptionLevelId { get; set; }
        public int SchemeDetailId { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductPackingId { get; set; }
        public int ProductId { get; set; }
        public int BrandId { get; set; }
        public bool IsHvp { get; set; }
        public int CatlogId { get; set; }
        public double MinQuantity { get; set; }
        public double MinValue { get; set; }
        public double MaxQuantity { get; set; }
        public double MaxValue { get; set; }
        public bool IsActive { get; set; }
        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string CatlogName { get; set; }
        public string BrandName { get; set; }
        public string ProductCatagoryName { get; set; }
        public string ItemTypeName { get; set; }
        public int IndexId { get; set; }
        public int OperatorId { get; set; }
        public bool IsBill { get; set; }
        public bool IsForEvery { get; set; }
        public List<OptionLevelCombinationModel> OptionLevelCombinationList { get; set; }
    }

    public class OptionLevelCombinationModel : BaseModel
    {
        public int OptionLevelCombinationId { get; set; }
        public int OptionLevelId { get; set; }
        public int ProductId { get; set; }
        public bool IsExceptProduct { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductPackingId { get; set; }
        public int BrandId { get; set; }
        public int FlavorId { get; set; }
        public bool IsActive { get; set; }
        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string FlavorName { get; set; }
        public string BrandName { get; set; }
        public string ProductCatagoryName { get; set; }
        public bool? IsHvp { get; set; }
        public int IndexId { get; set; }

        private string hvp = "Any";
        public string Hvp
        {
            get
            {
                if (IsHvp.HasValue)
                {
                    if (IsHvp.Value)
                        hvp = "Yes";
                    else
                        hvp = "No";
                }
                else
                {
                    hvp = "Any";
                }
                return hvp;
            }
            set
            {
            }
        }

        public string IsExcept { get; set; }
    }

    public class FreeProductModel : BaseModel
    {
        public int FreeProductId { get; set; }
        public int ProductId { get; set; }
        public double Qty { get; set; }
        public int ProductPackingId { get; set; }
        public int SchemeDetailsId { get; set; }
        public int CatlogId { get; set; }
        public int BrandId { get; set; }
        public int ProductCatagoryId { get; set; }
        public int ItemTypeId { get; set; }
        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string CatlogName { get; set; }
        public string BrandName { get; set; }
        public string ProductCatagoryName { get; set; }
        public string ItemTypeName { get; set; }
    }


}
