﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class KPIReports
    {
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public string EmailIds { get; set; }
        public string EmailTemplate { get; set; }
        public string Status { get; set; }
        public List<KPIReportUser> ReportUserList { get; set; }
    }

    public class KPIReportUser
    {
        public int ReportId { get; set; }
        public string OriginatorType { get; set; }
        public string Originator { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string IsActive { get; set; }
    }

    public class KPIReportModel : BaseModel
    {
        public decimal Id { get; set; }
        public decimal BatchId { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public int WeekId { get; set; }
        public string WeekName { get; set; }
        public DateTime TargetDate { get; set; }
        public string Status { get; set; }
        public float TotReadyStockVal { get; set; }
        public float TotPreInvoiceVal { get; set; }
        public int TotReadyStockPc { get; set; }
        public int TotPreInvoicePc { get; set; }
        public int TotBillCount { get; set; }
        public float TotBillValue { get; set; }
        public int TotWhSalesBillCount { get; set; }
        public int TotBrand1BillCount { get; set; }
        public float TotBrand1BillVal { get; set; }
        public int TotBrand1OutletCount { get; set; }
        public int TotBrand2BillCount { get; set; }
        public float TotBrand2BillVal { get; set; }
        public int TotBrand2OutletCount { get; set; }
        public int TotBrand3BillCount { get; set; }
        public float TotBrand3BillVal { get; set; }
        public int TotBrand3OutletCount { get; set; }
        public int TotBrand4BillCount { get; set; }
        public float TotBrand4BillVal { get; set; }
        public int TotBrand4OutletCount { get; set; }
        public int TotBrand5BillCount { get; set; }
        public float TotBrand5BillVal { get; set; }
        public int TotBrand5OutletCount { get; set; }
        public int TotMultiLineBill { get; set; }
        public int TotReadyStockMandays { get; set; }
        public int TotPreInvMandays { get; set; }
        public int TotECO { get; set; }
        public int TotNoSalesAcc { get; set; }
        public int TotNonVisitedOutlets { get; set; }
        public int TotMSSComplience { get; set; }

        public float AvgReadyStockVal { get; set; }
        public float AvgPreInvoiceVal { get; set; }
        public int AvgReadyStockPc { get; set; }
        public int AvgPreInvoicePc { get; set; }
        public int AvgBillCount { get; set; }
        public float AvgBillValue { get; set; }
        public int AvgWhSalesBillCount { get; set; }
        public int AvgBrand1BillCount { get; set; }
        public float AvgBrand1BillVal { get; set; }
        public int AvgBrand1OutletCount { get; set; }
        public int AvgBrand2BillCount { get; set; }
        public float AvgBrand2BillVal { get; set; }
        public int AvgBrand2OutletCount { get; set; }
        public int AvgBrand3BillCount { get; set; }
        public float AvgBrand3BillVal { get; set; }
        public int AvgBrand3OutletCount { get; set; }
        public int AvgBrand4BillCount { get; set; }
        public float AvgBrand4BillVal { get; set; }
        public int AvgBrand4OutletCount { get; set; }
        public int AvgBrand5BillCount { get; set; }
        public float AvgBrand5BillVal { get; set; }
        public int AvgBrand5OutletCount { get; set; }
        public int AvgMultiLineBill { get; set; }
        public int AvgReadyStockMandays { get; set; }
        public int AvgPreInvMandays { get; set; }
        public int AvgECO { get; set; }
        public int AvgNoSalesAcc { get; set; }
        public int AvgNonVisitedOutlets { get; set; }
        public int AvgMSSComplience { get; set; }

    }

    public class KPIReportSettingsModel : BaseModel
    {
        public int Id { get; set; }
        public int Brand1 { get; set; }
        public int Brand2 { get; set; }
        public int Brand3 { get; set; }
        public int Brand4 { get; set; }
        public int Brand5 { get; set; }
        public string Status { get; set; }

    }

    public class KPIReportEmailRecipientsModel
    {
        public string Email { get; set; }

    } 
    
    public class KPIReportBrandsModel
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string BrandDisplayName { get; set; }
    }

    public class KPIReportBrandItemModel : BaseModel
    {
        public int KpiBrandId { get; set; }
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductFgsCode { get; set; }
        public string ProductName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int FlavourId { get; set; }
        public string FlavourName { get; set; }
        public int PackingId { get; set; }
        public string PackingName { get; set; }
        public bool IsHighValue { get; set; }
        public bool IsAssigned { get; set; }
    }

}
