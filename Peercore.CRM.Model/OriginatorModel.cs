﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class OriginatorModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        private string clientType;

        public string ClientType
        {
            get { return clientType; }
            set { clientType = value; }
        }
        public int OriginatorId { get; set; }
        public int AuthorizationGroupId { get; set; }
        //public List<UserPermissionEntity> PermissionCollection { get; set; }

        public string RepCode { get; set; }
        public string RepType { get; set; }

        public bool ManagerMode { get; set; }
        public bool HasChildReps { get; set; }
        public int CRMAuthLevel { get; set; }

        public string DefaultDepartmentId { get; set; }

        public string Originator { get; set; }
        public string PrefixCode { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int AuthLevel { get; set; }
        public double OrderLimit { get; set; }
        public double InvoiceLimit { get; set; }
        public string ParentOriginator { get; set; }
        public int PersonalAuthNo { get; set; }
        public string DefaultDeptId { get; set; }
        public string DeliverTo { get; set; }
        public string PictureId { get; set; }
        public int ApproveMand { get; set; }
        public string Password { get; set; }
        public string PrintDevice { get; set; }
        public string RefInvPrinter { get; set; }
        public string RendInvPrinter { get; set; }
        public string InternetAdd { get; set; }
        public string BitmapPath { get; set; }
        public string EmployeeNo { get; set; }
        public string Alias { get; set; }
        public string DeptString { get; set; }
        public string Telephone { get; set; }
        public string Status { get; set; }
        public string LeaveDept { get; set; }
        public int LeaveDeptMgr { get; set; }
        public int CrmAuthLevel { get; set; }
        public string AccessToken { get; set; }
        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public string Adderess { get; set; }
        public string Mobile { get; set; }

        public int InvoiceCount { get; set; }

        public string distributorname { get; set; }
        public string distributoraddress { get; set; }
        public string distributortel { get; set; }

        public int user_type_id { get; set; }
        public string user_type { get; set; }
        public string type_des { get; set; }

        public string designation { get; set; }
        public int rowCount { get; set; }
    }

    public class OriginatorImeiModel : BaseModel
    {
        public int id { get; set; }
        public string originator { get; set; }
        public string mobile_no { get; set; }
        public string emei_no { get; set; }
        public string status { get; set; }
        public string last_sync_date { get; set; }
        public string apk_version { get; set; }
    }
    
    public class OrigivatorVisitModel : BaseModel
    {
        public string visitType { get; set; }
        public string fromOriginator { get; set; }
        public string toOriginator { get; set; }
        public string cinLatitude { get; set; }
        public string cinLogitude { get; set; }
        public string cinVisitTime { get; set; }
        public string coutLatitude { get; set; }
        public string coutLogitude { get; set; }
        public string coutVisitTime { get; set; }
        public string visitNote { get; set; }
    }

    public class OriginatorAttendance : BaseModel
    {
        public string originatorType { get; set; }
        public string originator { get; set; }
        public bool isCheckIn { get; set; }
        public string checkInTime { get; set; }
        public string checkInLat { get; set; }
        public string checkInLng { get; set; }
        public string checkInRouteId { get; set; }
        public string checkInAddress { get; set; }
        public bool isCheckOut { get; set; }
        public string checkOutTime { get; set; }
        public string checkOutLat { get; set; }
        public string checkOutLng { get; set; }
        public string checkOutAddress { get; set; }
        public string checkOutRouteId { get; set; }
    }

    public class AttendanceReasons : BaseModel
    {
        public int id { get; set; }
        public string reason { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        
    }

    public class OriginatorAttendanceReasons : BaseModel
    {
        public string originator { get; set; }
        public string time { get; set; }
        public string reason { get; set; } = "";
        public string description { get; set; } = "";
        public string Latitude { get; set; } = "";
        public string Longitude { get; set; } = "";

    }
}