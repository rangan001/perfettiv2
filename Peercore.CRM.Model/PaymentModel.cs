﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class PaymentModel : BaseModel
    {
        public int PaymentId { get; set; }
        public string RepCode { get; set; }
        public int PayYear { get; set; }
        public int PayMonth { get; set; }
        public string PayMonthName { get; set; }
        public DateTime PaymentDate { get; set; }
        public int BillNo { get; set; }
        public decimal BillAmount { get; set; }
        public decimal Allowance { get; set; }
        public int Status { get; set; }
        public string Remark { get; set; }
    }

    public class MonthlyDeductionModel : BaseModel
    {
        public int Id { get; set; }
        public string RepCode { get; set; }
        public string AssetCode { get; set; }
        public int DeductionMonth { get; set; }
        public string DeductionMonthName { get; set; }
        public decimal DeductionAmount { get; set; }
        public string Status { get; set; }
    }

}
