﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class RegionModel : BaseModel
    {
        public int RegionId { get; set; }
        public int CompId { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string RegionStatus { get; set; }
    }

    
}
