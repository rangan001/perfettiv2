﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class ResponseModel
    {
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }
}
