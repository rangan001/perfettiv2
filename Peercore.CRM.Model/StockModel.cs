﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class ProductStockModel
    {
        public DateTime AllocDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int TerritoryId { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public List<ProductStockDetailModel> LstLoadStockDetail { get; set; }
        public int RecordId { get; set; }
        public string RepCode { get; set; }
        public int RouteAssignID { get; set; }
        public int RouteId { get; set; }
        public string StockAllocType { get; set; }
        public int StockId { get; set; }
    }

    public class ProductStockDetailModel : BaseModel
    {
        public int TerritoryId { get; set; }
        public double VehicleBalanceQty { get; set; }
        public int StockTotalId { get; set; }
        public int StockHeaderId { get; set; }
        public int StockDetailId { get; set; }
        public int Sku { get; set; }
        public double ReturningQty { get; set; }
        public string ReturnReason { get; set; }
        public double ReturnQty { get; set; }
        public int RecordId { get; set; }
        public string MovementType { get; set; }
        public double LoadQtyOld { get; set; }
        public double LoadQty { get; set; }
        public string CatalogName { get; set; }
        public int CatalogId { get; set; }
        public string CatalogCode { get; set; }
        public double BalanceQty { get; set; }
        public double AllocationQtyTotal { get; set; }
        public double AllocQty { get; set; }
        public double AdjustQty { get; set; }
        public double VehicleReturnQty { get; set; }
        public double VehicleReturningQty { get; set; }
    }

    public class StockTotalModel : BaseModel
    {
        public int TerritoryId { get; set; }
        public int StockTotalId { get; set; }
        public int DistributorId { get; set; }
        public int CatalogId { get; set; }
        public string CatalogCode { get; set; }
        public string CatalogName { get; set; }
        public double AllocQty { get; set; }
        public double LoadQty { get; set; }
        public double BalanceQty { get; set; }
        public double ReturnQty { get; set; }
        public double VehicleBalanceQty { get; set; }
        public double VehicleReturnQty { get; set; }
        public string Status { get; set; }
    }
    
    public class StockMovementModel : BaseModel
    {
        public decimal Id { get; set; }
        public int TerritoryId { get; set; }
        public DateTime AllocDate { get; set; }
        public string TransType { get; set; }
        public decimal TransRefId { get; set; }
        public decimal CatalogId { get; set; }
        public double AllocQty { get; set; }
        public double CatalogPrice { get; set; }
    }
}
