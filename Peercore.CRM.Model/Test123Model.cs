﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class Test123Model : BaseModel
    {
        public class BrandECOModel

        {

            public string BrandName { get; set; }

            //public string ProductName { get; set; }

            public Boolean isPC { get; set; }


        }
        
        
        public class CategoryECOModel

        {

            public string CategoryCode { get; set; }

            public string CategoryName { get; set; }

            public Boolean isPC { get; set; }


        }


        public class ProductECOModel

        {

            public string ProductName { get; set; }

            public string ProductCode { get; set; }
            public string ProductFGCode { get; set; }


            public Boolean isPC { get; set; }


        }




        public class CustomerECOModel

        {

            public string CustomerCode { get; set; }

            public string CustomerName { get; set; }

            public Boolean isPC { get; set; }


        }

    }

    
}
