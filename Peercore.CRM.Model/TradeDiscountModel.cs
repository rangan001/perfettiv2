﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class TradeDiscountUploadModel : BaseModel
    {
        public decimal id { get; set; }
        public string date { get; set; }
        public string dbCode { get; set; }
        public string vouType { get; set; }
        public string vouNo { get; set; }
        public double saleVal { get; set; }
        public double discountVal { get; set; }
        public double tradeDiscountVal { get; set; }
        public string type { get; set; }
        public string uploadStatus { get; set; }
    }
}
