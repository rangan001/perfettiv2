﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class UploadMediaModel : BaseModel
    {
        public int MediaFileId { get; set; }
        public string RepCode { get; set; }
        public string CustCode { get; set; }
        public string Prefix { get; set; }
        public string MediaCategory { get; set; }

        public string MediaFileName { get; set; }
        public string FileExtention { get; set; }
        public string MediaFilePath { get; set; }
        public string ThumbnailPath { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }

    }

    public class MediaCategoryModel : BaseModel
    {
        public int Id { get; set; }
        public string CatName { get; set; }
        public string Description { get; set; }
        public string Prefix { get; set; }
        public string Status { get; set; }

    }

}
