﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.Reports.Models
{
    public class ReportTypeModel
    {
        public int rptTypeId { get; set; }
        public string rptType { get; set; }
    }

    public class ReportSubTypeModel
    {
        public int rptSubTypeId { get; set; }
        public int rptTypeId { get; set; }
        public string rptSubType { get; set; }
    }
}