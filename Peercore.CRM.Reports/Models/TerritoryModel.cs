﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Peercore.CRM.Reports.Models
{
    public class TerritoryModel
    {
        public string SelectedOption { get; set; }
        public IEnumerable<SelectListItem> SelectOptions { get; set; }
    }
}