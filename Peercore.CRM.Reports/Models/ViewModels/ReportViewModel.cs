﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.Reports.Models.ViewModels
{
    public class ReportViewModel
    {
        public List<ReportTypeModel> ReportTypeList { get; set; }
    }
}