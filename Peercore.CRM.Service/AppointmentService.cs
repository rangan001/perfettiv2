﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.ServiceContracts;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IAppointment
    {
        AppointmentAdapter appointmentAdapter =null;

        public List<AppointmentViewDTO> GetOncomingAppointments(ArgsDTO args)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetOncomingAppointments(args);
        }

        public List<KeyValuePair<string, int>> GetAppointmentsCountForHome(ArgsDTO args)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetAppointmentsCountForHome(args);
        }

        public bool AppointmentUpdate(AppointmentDTO appointment, bool isUpdateDTRangeOnly = false, bool isUpdateDelFlagOnly = false)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.AppointmentUpdate(appointment, isUpdateDTRangeOnly, isUpdateDelFlagOnly);
        }

        public bool InsertAppointmentList(List<CustomerActivitiesDTO> Listactivity, string originator)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.InsertAppointmentList(Listactivity, originator);
        }

        public bool AppointmentInsert(AppointmentDTO appointment, out int appointmentID, bool isUpdateActivity = false, int activityId = 0)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.AppointmentInsert(appointment, out appointmentID, isUpdateActivity, activityId);
        }

        public List<AppointmentViewDTO> GetTodaysAppointments(ArgsDTO ArgsDTO)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetTodaysAppointments(ArgsDTO);
        }

        public List<AppointmentViewDTO> GetPendingAppointments(ArgsDTO ArgsDTO)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetPendingAppointments(ArgsDTO);
        }

        public List<AppointmentViewDTO> GetAllAppointmentsByType(ArgsDTO ArgsDTO)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetAllAppointmentsByType(ArgsDTO);
        }

        public List<AppointmentCountViewDTO> GetAllAppointmentsByTypeCount(ArgsDTO args)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetAllAppointmentsByTypeCount(args);
        }
        /*
        public List<AppointmentDTO> GetAppointments(ArgsDTO ArgsDTO)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetAppointments(ArgsDTO);
        }

        public bool Insert(AppointmentDTO appointment, out int appointmentID, bool isUpdateActivity = false, int activityId = 0)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.Insert(appointment, out appointmentID, isUpdateActivity, activityId);
        }

        public bool Update(AppointmentDTO appointment, bool isUpdateDTRangeOnly = false, bool isUpdateDelFlagOnly = false)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.Update(appointment, isUpdateDTRangeOnly, isUpdateDelFlagOnly);
        }

        public bool DeleteResources(AppointmentDTO appointment)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.DeleteResources(appointment);
        }

        public List<AppointmentViewDTO> GetOncomingAppointments(ArgsDTO args)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetOncomingAppointments(args);
        }

        public List<AppointmentViewDTO> GetTodaysAppointments(ArgsDTO ArgsDTO)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetTodaysAppointments(ArgsDTO);
        }

        public List<AppointmentViewDTO> GetPendingAppointments(ArgsDTO ArgsDTO)
        {
            appointmentAdapter = new AppointmentAdapter();
            return appointmentAdapter.GetPendingAppointments(ArgsDTO);
        }

        

        */
    }
}
