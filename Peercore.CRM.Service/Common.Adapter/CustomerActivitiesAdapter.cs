﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.Service.DTO;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class CustomerActivitiesAdapter
    {
        #region - Converters -

        public static CustomerActivityEntity ConvertToCustomerActivities(CustomerActivitiesDTO CustomerActivitiesDto)
        {
            CustomerActivityEntity oCustomerActivities = CustomerActivityEntity.CreateObject();
            try
            {
                oCustomerActivities.ActivityID = CustomerActivitiesDto.ActivityID;
                oCustomerActivities.ActivityType = CustomerActivitiesDto.ActivityType;
                oCustomerActivities.AppointmentId = CustomerActivitiesDto.AppointmentId;
                oCustomerActivities.AssignedTo = CustomerActivitiesDto.AssignedTo;
                oCustomerActivities.Comments = CustomerActivitiesDto.Comments;
                oCustomerActivities.CreatedBy = CustomerActivitiesDto.CreatedBy;
                oCustomerActivities.CreatedDate = CustomerActivitiesDto.CreatedDate;
                oCustomerActivities.CustCode = CustomerActivitiesDto.CustCode;
                //oCustomerActivities.CustomerCity = CustomerActivitiesDto.CustomerCity;
                //oCustomerActivities.CustomerPostCode = CustomerActivitiesDto.CustomerPostCode;
                oCustomerActivities.DelFlag = CustomerActivitiesDto.DelFlag;
                oCustomerActivities.EndDate = CustomerActivitiesDto.EndDate;
                oCustomerActivities.LastModifiedBy = CustomerActivitiesDto.LastModifiedBy;
                oCustomerActivities.LastModifiedDate = CustomerActivitiesDto.LastModifiedDate;
                oCustomerActivities.LeadId = CustomerActivitiesDto.LeadId;
                oCustomerActivities.LeadName = CustomerActivitiesDto.LeadName;
                oCustomerActivities.LeadStage = CustomerActivitiesDto.LeadStage;
                oCustomerActivities.NoOfActivities = CustomerActivitiesDto.NoOfActivities;
                //oCustomerActivities.OriginatorName = CustomerActivitiesDto.OriginatorName;
                oCustomerActivities.PipelineStageID = CustomerActivitiesDto.PipelineStageID;
                oCustomerActivities.Priority = CustomerActivitiesDto.Priority;
                oCustomerActivities.ReminderDate = CustomerActivitiesDto.ReminderDate;
                oCustomerActivities.SendReminder = CustomerActivitiesDto.SendReminder;
                oCustomerActivities.SentMail = CustomerActivitiesDto.SentMail;
                oCustomerActivities.StartDate = CustomerActivitiesDto.StartDate;
                oCustomerActivities.Status = CustomerActivitiesDto.Status;
                oCustomerActivities.StatusDesc = CustomerActivitiesDto.StatusDesc;
                oCustomerActivities.Subject = CustomerActivitiesDto.Subject;
                //oCustomerActivities.TypeColour = CustomerActivitiesDto.TypeColour;
                oCustomerActivities.TypeDesc = CustomerActivitiesDto.TypeDesc;
                oCustomerActivities.RepCode = CustomerActivitiesDto.RepCode;
                oCustomerActivities.RepName = CustomerActivitiesDto.RepName;
            }
            catch (Exception)
            {

                throw;
            }
            return oCustomerActivities;
        }

        public static CustomerActivitiesDTO ConvertToCustomerActivitiesDTO(CustomerActivityEntity oCustomerActivities)
        {
            CustomerActivitiesDTO CustomerActivitiesDto = new CustomerActivitiesDTO();
            try
            {

                CustomerActivitiesDto.ActivityID = oCustomerActivities.ActivityID;
                CustomerActivitiesDto.ActivityType = oCustomerActivities.ActivityType;
                CustomerActivitiesDto.AppointmentId = oCustomerActivities.AppointmentId;
                CustomerActivitiesDto.AssignedTo = oCustomerActivities.AssignedTo;
                CustomerActivitiesDto.Comments = oCustomerActivities.Comments;
                CustomerActivitiesDto.CreatedBy = oCustomerActivities.CreatedBy;
                CustomerActivitiesDto.CreatedDate = oCustomerActivities.CreatedDate.Value;
                CustomerActivitiesDto.CustCode = oCustomerActivities.CustCode;
                //CustomerActivitiesDto.CustomerCity = oCustomerActivities.CustomerCity;
                //CustomerActivitiesDto.CustomerPostCode = oCustomerActivities.CustomerPostCode;
                CustomerActivitiesDto.DelFlag = oCustomerActivities.DelFlag;
                CustomerActivitiesDto.EndDate = oCustomerActivities.EndDate;
                CustomerActivitiesDto.LastModifiedBy = oCustomerActivities.LastModifiedBy;
                CustomerActivitiesDto.LastModifiedDate = oCustomerActivities.LastModifiedDate.Value;
                CustomerActivitiesDto.LeadId = oCustomerActivities.LeadId;
                CustomerActivitiesDto.LeadName = oCustomerActivities.LeadName;
                CustomerActivitiesDto.LeadStage = oCustomerActivities.LeadStage;
                CustomerActivitiesDto.NoOfActivities = oCustomerActivities.NoOfActivities;
                //CustomerActivitiesDto.OriginatorName = oCustomerActivities.OriginatorName;
                CustomerActivitiesDto.PipelineStageID = oCustomerActivities.PipelineStageID;
                CustomerActivitiesDto.Priority = oCustomerActivities.Priority;
                CustomerActivitiesDto.ReminderDate = oCustomerActivities.ReminderDate;
                CustomerActivitiesDto.SendReminder = oCustomerActivities.SendReminder;
                CustomerActivitiesDto.SentMail = oCustomerActivities.SentMail;
                CustomerActivitiesDto.StartDate = oCustomerActivities.StartDate;
                CustomerActivitiesDto.Status = oCustomerActivities.Status;
                CustomerActivitiesDto.StatusDesc = oCustomerActivities.StatusDesc;
                CustomerActivitiesDto.Subject = oCustomerActivities.Subject;
                //CustomerActivitiesDto.TypeColour = oCustomerActivities.TypeColour;
                CustomerActivitiesDto.TypeDesc = oCustomerActivities.TypeDesc;
                CustomerActivitiesDto.RowCount = oCustomerActivities.TotalCount;
                CustomerActivitiesDto.RepName = oCustomerActivities.RepName;
                CustomerActivitiesDto.RepCode = oCustomerActivities.RepCode;
            }
            catch (Exception)
            {

                throw;
            }
            return CustomerActivitiesDto;
        }

        #endregion

    }
}
