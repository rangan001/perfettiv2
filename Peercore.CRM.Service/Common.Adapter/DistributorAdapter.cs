﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class DistributorAdapter
    {
        #region Converters

        #region BankAccount

        public static BankAccountDTO ConvertToBankAccountDTO(BankAccountEntity bankAccountEntity)
        {
            BankAccountDTO bankAccountDTO = new BankAccountDTO();

            try
            {
                if (bankAccountDTO != null)
                {
                    bankAccountDTO.BankAccountId = bankAccountEntity.BankAccountId;
                    bankAccountDTO.SourceId = bankAccountEntity.SourceId;
                    bankAccountDTO.SourceType = bankAccountEntity.SourceType;
                    bankAccountDTO.AccountNumber = bankAccountEntity.AccountNumber;
                    bankAccountDTO.Bank = bankAccountEntity.Bank;
                    bankAccountDTO.Branch = bankAccountEntity.Branch;
                    bankAccountDTO.isDefault = bankAccountEntity.isDefault;
                    bankAccountDTO.Status = bankAccountEntity.Status;
                    bankAccountDTO.SourceTypeText = bankAccountEntity.SourceTypeText;

                    bankAccountDTO.CreatedBy = bankAccountEntity.CreatedBy;
                    bankAccountDTO.CreatedDate = bankAccountEntity.CreatedDate.Value;
                    bankAccountDTO.LastModifiedBy = bankAccountEntity.LastModifiedBy;
                    bankAccountDTO.LastModifiedDate = bankAccountEntity.LastModifiedDate.Value;

                    bankAccountDTO.RowCount = bankAccountEntity.TotalCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return bankAccountDTO;
        }

        public static BankAccountEntity ConvertToBankAccountEntity(BankAccountDTO bankAccountDTO)
        {
            BankAccountEntity bankAccountEntity = new BankAccountEntity();

            try
            {
                if (bankAccountEntity != null)
                {
                    bankAccountEntity.BankAccountId = bankAccountDTO.BankAccountId;
                    bankAccountEntity.SourceId = bankAccountDTO.SourceId;
                    bankAccountEntity.SourceType = bankAccountDTO.SourceType;
                    bankAccountEntity.AccountNumber = bankAccountDTO.AccountNumber;
                    bankAccountEntity.Bank = bankAccountDTO.Bank;
                    bankAccountEntity.Branch = bankAccountDTO.Branch;
                    bankAccountEntity.isDefault = bankAccountDTO.isDefault;
                    bankAccountEntity.Status = bankAccountDTO.Status;

                    bankAccountEntity.CreatedBy = bankAccountDTO.CreatedBy;
                    bankAccountEntity.CreatedDate = bankAccountDTO.CreatedDate;
                    bankAccountEntity.LastModifiedBy = bankAccountDTO.LastModifiedBy;
                    bankAccountEntity.LastModifiedDate = bankAccountDTO.LastModifiedDate;
                    bankAccountEntity.TotalCount = bankAccountDTO.RowCount;
                    bankAccountEntity.SourceTypeText = bankAccountDTO.SourceTypeText;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return bankAccountEntity;
        }

        public static List<BankAccountDTO> ConvertToBankAccountDTOList(List<BankAccountEntity> lstBankAccountEntity)
        {
            BankAccountDTO bankAccountDTO = null;
            List<BankAccountDTO> lstBankAccountDTO = new List<BankAccountDTO>();
            foreach (BankAccountEntity bankAccountEntity in lstBankAccountEntity)
            {
                bankAccountDTO = new BankAccountDTO();

                bankAccountDTO = ConvertToBankAccountDTO(bankAccountEntity);

                lstBankAccountDTO.Add(bankAccountDTO);
            }
            return lstBankAccountDTO;
        }

        public static List<BankAccountEntity> ConvertToBankAccountEntityList(List<BankAccountDTO> lstBankAccountDTO)
        {
            BankAccountEntity bankAccountEntity = null;
            List<BankAccountEntity> lstBankAccountEntity = new List<BankAccountEntity>();
            foreach (BankAccountDTO bankAccountDTO in lstBankAccountDTO)
            {
                bankAccountEntity = new BankAccountEntity();

                bankAccountEntity = ConvertToBankAccountEntity(bankAccountDTO);

                lstBankAccountEntity.Add(bankAccountEntity);
            }
            return lstBankAccountEntity;
        }

        #endregion

        #region Distributor

        public static DistributorDTO ConvertToDistributorDTO(DistributorEntity distributorEntity)
        {
            DistributorDTO distributorDTO = new DistributorDTO();

            try
            {
                if (distributorDTO != null)
                {
                    distributorDTO.DistributorId = distributorEntity.DistributorId;
                    distributorDTO.Code = distributorEntity.Code;
                    distributorDTO.Name = distributorEntity.Name;
                    distributorDTO.DisplayName = distributorEntity.DisplayName;
                    distributorDTO.Originator = distributorEntity.Originator;
                    distributorDTO.Status = distributorEntity.Status;
                    distributorDTO.Target = distributorEntity.Target != 0 ? distributorEntity.Target / 1000 : 0;
                    distributorDTO.TargetId = distributorEntity.TargetId;

                    distributorDTO.CreatedBy = distributorEntity.CreatedBy;
                    distributorDTO.CreatedDate = distributorEntity.CreatedDate;
                    distributorDTO.LastModifiedBy = distributorEntity.LastModifiedBy;
                    distributorDTO.LastModifiedDate = distributorEntity.LastModifiedDate;

                    distributorDTO.RowCount = distributorEntity.TotalCount;

                    distributorDTO.EffStartDate = distributorEntity.EffStartDate;
                    distributorDTO.EffEndDate = distributorEntity.EffEndDate;


                    distributorDTO.PrefixCode = distributorEntity.PrefixCode;
                    distributorDTO.ParentOriginator = distributorEntity.ParentOriginator;
                    distributorDTO.DeptString = distributorEntity.DeptString;
                    distributorDTO.Password = distributorEntity.Password;
                    distributorDTO.Adderess = distributorEntity.Adderess;
                    distributorDTO.Mobile = distributorEntity.Mobile;
                    distributorDTO.Telephone = distributorEntity.Telephone;
                    distributorDTO.Email = distributorEntity.Email;
                    distributorDTO.Holiday = distributorEntity.Holiday;
                    distributorDTO.HolidaysList = distributorEntity.HolidaysList;
                    if (distributorEntity.BankAccountsList != null)
                        if (distributorEntity.BankAccountsList.Count > 0)
                            distributorDTO.BankAccountsList = ConvertToBankAccountDTOList(distributorEntity.BankAccountsList);
                    distributorDTO.GroupCode = distributorEntity.GroupCode;
                    distributorDTO.JoinedDate = distributorEntity.JoinedDate;
                    distributorDTO.DivisionId = distributorEntity.DivisionId;
                    distributorDTO.PrimaryDist = distributorEntity.PrimaryDist;
                    distributorDTO.AreaId = distributorEntity.AreaId;
                    distributorDTO.IsNotEMEIUser = distributorEntity.IsNotEMEIUser;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return distributorDTO;
        }

        public static DistributorEntity ConvertToDistributorEntity(DistributorDTO distributorDTO)
        {
            DistributorEntity distributorEntity = DistributorEntity.CreateObject();

            try
            {
                if (distributorEntity != null)
                {
                    distributorEntity.DistributorId = distributorDTO.DistributorId;
                    distributorEntity.Code = distributorDTO.Code;
                    distributorEntity.Name = distributorDTO.Name;
                    distributorEntity.Originator = distributorDTO.Originator;
                    distributorEntity.Status = distributorDTO.Status;
                    distributorEntity.Target = distributorDTO.Target * 1000;
                    distributorEntity.TargetId = distributorDTO.TargetId;

                    distributorEntity.CreatedBy = distributorDTO.CreatedBy;
                    distributorEntity.CreatedDate = distributorDTO.CreatedDate;
                    distributorEntity.LastModifiedBy = distributorDTO.LastModifiedBy;
                    distributorEntity.LastModifiedDate = distributorDTO.LastModifiedDate;
                    distributorEntity.TotalCount = distributorDTO.RowCount;

                    distributorEntity.EffEndDate = distributorDTO.EffEndDate;
                    distributorEntity.EffStartDate = distributorDTO.EffStartDate;

                    distributorEntity.PrefixCode = distributorDTO.PrefixCode;
                    distributorEntity.ParentOriginator = distributorDTO.ParentOriginator;
                    distributorEntity.DeptString = distributorDTO.DeptString;
                    distributorEntity.Password = distributorDTO.Password;
                    distributorEntity.Adderess = distributorDTO.Adderess;
                    distributorEntity.Mobile = distributorDTO.Mobile;
                    distributorEntity.Telephone = distributorDTO.Telephone;
                    distributorEntity.Email = distributorDTO.Email;
                    distributorEntity.Holiday = distributorDTO.Holiday;
                    distributorEntity.HolidaysList = distributorDTO.HolidaysList;
                    if (distributorDTO.BankAccountsList != null)
                        if (distributorDTO.BankAccountsList.Count > 0)
                            distributorEntity.BankAccountsList = ConvertToBankAccountEntityList(distributorDTO.BankAccountsList);
                    distributorEntity.GroupCode = distributorDTO.GroupCode;
                    distributorEntity.JoinedDate = distributorDTO.JoinedDate;
                    distributorEntity.DivisionId = distributorDTO.DivisionId;
                    distributorEntity.PrimaryDist = distributorDTO.PrimaryDist;
                    distributorEntity.AreaId = distributorDTO.AreaId;
                    distributorEntity.IsNotEMEIUser = distributorDTO.IsNotEMEIUser;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return distributorEntity;
        }

        public static List<DistributorDTO> ConvertToDistributorDTOList(List<DistributorEntity> lstDistributorEntity)
        {
            DistributorDTO distributorDTO = null;
            List<DistributorDTO> lstDistributorDTO = new List<DistributorDTO>();
            foreach (DistributorEntity DistributorEntity in lstDistributorEntity)
            {
                distributorDTO = new DistributorDTO();

                distributorDTO = ConvertToDistributorDTO(DistributorEntity);

                lstDistributorDTO.Add(distributorDTO);
            }
            return lstDistributorDTO;
        }

        public static List<DistributorEntity> ConvertToDistributorEntityList(List<DistributorDTO> lstDistributorDTO)
        {
            DistributorEntity DistributorEntity = null;
            List<DistributorEntity> lstDistributorEntity = new List<DistributorEntity>();
            foreach (DistributorDTO DistributorDTO in lstDistributorDTO)
            {
                DistributorEntity = DistributorEntity.CreateObject();

                DistributorEntity = ConvertToDistributorEntity(DistributorDTO);

                lstDistributorEntity.Add(DistributorEntity);
            }
            return lstDistributorEntity;
        }

        #endregion

        #endregion

        public List<BankAccountDTO> GetBankAccountsForUser(string sourceTypeText, string originator)
        {
            List<BankAccountDTO> lstAccounts = new List<BankAccountDTO>();
            try
            {
                return ConvertToBankAccountDTOList(BankAccountBR.Instance.GetBankAccountsForUser(sourceTypeText, originator));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DistributorDTO GetDistributorTargetById(ArgsDTO args)
        {
            try
            {
                return ConvertToDistributorDTO(DistributorBR.Instance.GetDistributorTargetById(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public bool SaveDistributor(DistributorDTO distributor)
        //{
        //    try
        //    {
        //        return DistributorBR.Instance.SaveDistributor(ConvertToDistributorEntity(distributor));
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public bool SaveBankAccounts(BankAccountDTO account)
        {
          
            try
            {
                return BankAccountBR.Instance.SaveBankAccounts(ConvertToBankAccountEntity(account));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetDistributorHoliday(int originatorId, string originator, string repCode)
        {
            try
            {
                return DistributorBR.Instance.GetDistributorHoliday(originatorId, originator, repCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DistributorDTO GetDistributorDetails(string originator)
        {
            try
            {
                return ConvertToDistributorDTO(DistributorBR.Instance.GetDistributorDetails(originator));
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
