﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;
using System.IO;
using System.Web;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class DocumentAdapter
    {

        #region - Converters -

        public static DocumentEntity ConvertToDocument(DocumentDTO documentDto)
        {
            DocumentEntity Document = DocumentEntity.CreateObject();

            try
            {
                Document.AttachedBy = documentDto.AttachedBy;
                Document.AttachedDate = documentDto.AttachedDate;

                if (!string.IsNullOrEmpty(documentDto.FileLocation))
                    Document.Content = GlobalService.ConvertFile(documentDto.FileLocation);
                else
                    Document.Content = documentDto.Content;

                Document.CustCode = documentDto.CustCode;
                Document.DocumentID = documentDto.DocumentID;
                Document.DocumentName = documentDto.DocumentName;
                Document.LastModifiedBy = documentDto.LastModifiedBy;
                Document.LastModifiedDate = documentDto.LastModifiedDate;
                Document.LeadID = documentDto.LeadID;
                Document.Path = documentDto.Path;
                Document.RowCount = documentDto.RowCount;
                Document.EnduserCode = documentDto.EnduserCode;               
            }
            catch (Exception)
            {

                throw;
            }
            return Document;
        }

        public static DocumentDTO ConvertToDocumentDTO(DocumentEntity Document)
        {
            DocumentDTO documentDto = new DocumentDTO();

            try
            {
                documentDto.AttachedBy = Document.AttachedBy;
                documentDto.AttachedDate = Document.AttachedDate;
                documentDto.Content = Document.Content;
                documentDto.CustCode = Document.CustCode;
                documentDto.DocumentID = Document.DocumentID;
                documentDto.DocumentName = Document.DocumentName;
                documentDto.LastModifiedBy = Document.LastModifiedBy;
                documentDto.LastModifiedDate = Document.LastModifiedDate;
                documentDto.LeadID = Document.LeadID;
                documentDto.Path = Document.Path;
                documentDto.RowCount = Document.RowCount;
                documentDto.EnduserCode = Document.EnduserCode;  
            }
            catch (Exception)
            {

                throw;
            }
            return documentDto;
        }

        #endregion

        #region - Properties -

        //private brDocument oBrDocument = null;
        //private brDocumentRepository oBrDocumentRepository = null;

        #endregion

        #region - Constructor -

        public DocumentAdapter()
        {
            //if (oBrDocument == null)
            //    oBrDocument = new brDocument();
            //if (oBrDocumentRepository == null)
            //    oBrDocumentRepository = new brDocumentRepository();
        }

        #endregion

        #region - Document -
        public bool SaveDocument(DocumentDTO documentDto)
        {
            try
            {
                return DocumentBR.Instance.Save(ConvertToDocument(documentDto));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DocumentDTO> GetContactDocumentsForCustomer(ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            List<DocumentDTO> lstDocumentDTO = new List<DocumentDTO>();

            try
            {
                List<DocumentEntity> lstDocument = DocumentBR.Instance.GetContactDocumentsForCustomer(ActivityAdapter.ConvertToArgsEntity(args));

                foreach (DocumentEntity item in lstDocument)
                {
                    DocumentDTO docDTO = ConvertToDocumentDTO(DocumentBR.Instance.GetDocument(item.DocumentID));

                    docDTO.Extention = docDTO.Path.Contains(".") ? docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty;

                    if ((issavedocument) && ((docDTO.Extention.Equals(".doc")) || (docDTO.Extention.Equals(".docx")) || (docDTO.Extention.Equals(".xls"))
                            || (docDTO.Extention.Equals(".xlsx")) || (docDTO.Extention.Equals(".pdf")) || (docDTO.Extention.Equals(".csv"))))
                    {
                        string leadcustcode = string.Empty;
                        if ((string.IsNullOrWhiteSpace(docDTO.CustCode)) && (docDTO.LeadID != 0))
                            leadcustcode = docDTO.LeadID.ToString();
                        else
                            leadcustcode = docDTO.CustCode.Trim();

                        string new_tempFileName = GenerateFileName(leadcustcode, sessionID, docDTO.DocumentName.Trim()) + (docDTO.Path.Contains(".") ?
                                                docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty);

                       // string new_tempFileName = GenerateFileName(leadcustcode, sessionID, docDTO.DocumentName.Trim());

                        string new_path = path + new_tempFileName;

                        //Save the document to doc folder.
                        CreateDocument(docDTO, new_path);
                        docDTO.TempFileName = new_tempFileName;
                    }
                    docDTO.Content = null;

                    lstDocumentDTO.Add(docDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstDocumentDTO;
        }

        public List<DocumentDTO> GetLeadDocuments(ArgsDTO args)
        {
            List<DocumentDTO> lstDocumentDTO = new List<DocumentDTO>();
            try
            {
                List<DocumentEntity> lstDocument = DocumentBR.Instance.GetDocumentDetails(ActivityAdapter.ConvertToArgsEntity(args));

                foreach (DocumentEntity item in lstDocument)
                {
                    DocumentDTO docDTO = ConvertToDocumentDTO(item);
                    docDTO.Extention = docDTO.Path.Contains(".") ? docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty;
                    lstDocumentDTO.Add(docDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstDocumentDTO;
        }

        public DocumentDTO GetDocument(int documentID)
        {
            DocumentDTO documentDTO = new DocumentDTO();
            try
            {
                return ConvertToDocumentDTO(DocumentBR.Instance.GetDocument(documentID));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteLeadDocument(int documentId)
        {
            try
            {
                return DocumentBR.Instance.DeleteDocument(documentId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DocumentDTO> GetDocumentsForEntryPages(ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            List<DocumentDTO> lstDocumentDTO = new List<DocumentDTO>();
            try
            {
                List<DocumentEntity> documentList=null;
                if (args.CustomerType == "Lead")
                {
                    documentList = DocumentBR.Instance.GetDocumentDetails(ActivityAdapter.ConvertToArgsEntity(args));
                }
                else if (args.CustomerType == "Customer")
                {
                    documentList = DocumentBR.Instance.GetContactDocumentsForCustomer(ActivityAdapter.ConvertToArgsEntity(args));
                }
                else if (args.CustomerType == "EndUser")
                {
                    documentList = DocumentBR.Instance.GetContactDocumentsForEndUser(ActivityAdapter.ConvertToArgsEntity(args));
                }
                else
                {
                    documentList = new List<DocumentEntity>();
                }

                Random UniqueNo = new Random();
                
                
                 
                foreach (DocumentEntity item in documentList)
                {
                    DocumentDTO docDTO = ConvertToDocumentDTO(item);//DocumentBR.Instance.GetDocument(item.DocumentID)
                    docDTO.RowCount = item.RowCount;

                    docDTO.Extention = docDTO.Path.Contains(".") ? docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty;

                    /*if ((issavedocument) && (((docDTO.Extention.Equals(".doc")) || (docDTO.Extention.Equals(".docx")) || (docDTO.Extention.Equals(".xls"))
                            || (docDTO.Extention.Equals(".xlsx")) || (docDTO.Extention.Equals(".pdf"))) ||
                            ((docDTO.Extention.Equals(".DOC")) || (docDTO.Extention.Equals(".DOCX")) || (docDTO.Extention.Equals(".XLS"))
                            || (docDTO.Extention.Equals(".XLSX")) || (docDTO.Extention.Equals(".PDF")))))*/

                    if (issavedocument)
                    {
                        string leadcustcode = string.Empty;
                        string endusercode = string.Empty;

                        if (args.CustomerType == "Lead")
                        {
                            leadcustcode = docDTO.LeadID.ToString();
                        }
                        else if (args.CustomerType == "Customer")
                        {
                            leadcustcode = docDTO.CustCode.Trim();
                        }
                        else if (args.CustomerType == "EndUser")
                        {
                            leadcustcode = docDTO.CustCode.Trim() +"_"+ docDTO.EnduserCode.Trim();
                            //endusercode = docDTO.EnduserCode.Trim();
                        }
                        string a = Uri.EscapeDataString(leadcustcode);
                        string strUniCode = UniqueNo.Next().ToString();

                        string new_tempFileName = GenerateFileName(strUniCode, sessionID, docDTO.DocumentName.Trim()) + (docDTO.Path.Contains(".") ?
                                                docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty);

                        string new_path = path + new_tempFileName;

                        //Save the document to doc folder.
                        CreateDocument(docDTO, new_path);
                        docDTO.Path = new_tempFileName;
                    }
                    docDTO.Content = null;

                    lstDocumentDTO.Add(docDTO);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstDocumentDTO;
        }

        public List<DocumentDTO> GetLeadDocumentsForView(ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            List<DocumentDTO> lstDocumentDTO = new List<DocumentDTO>();

            try
            {
                List<DocumentEntity> lstDocument = DocumentBR.Instance.GetDocumentDetails(ActivityAdapter.ConvertToArgsEntity(args));

                foreach (DocumentEntity item in lstDocument)
                {
                    //DocumentDTO docDTO = ConvertToDocumentDTO(item);

                    DocumentDTO docDTO = ConvertToDocumentDTO(item);//DocumentBR.Instance.GetDocument(item.DocumentID)
                    docDTO.RowCount = item.RowCount;

                    docDTO.Extention = docDTO.Path.Contains(".") ? docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty;

                    if ((issavedocument) && ((docDTO.Extention.Equals(".doc")) || (docDTO.Extention.Equals(".docx")) || (docDTO.Extention.Equals(".xls"))
                            || (docDTO.Extention.Equals(".xlsx")) || (docDTO.Extention.Equals(".pdf")) ))
                    {
                        string leadcustcode = string.Empty;
                        if ((string.IsNullOrWhiteSpace(docDTO.CustCode)) && (docDTO.LeadID != 0))
                            leadcustcode = docDTO.LeadID.ToString();
                        else
                            leadcustcode = docDTO.CustCode.Trim();

                        //string new_tempFileName = GenerateFileName(leadcustcode, sessionID, docDTO.DocumentName.Trim()) + (docDTO.Path.Contains(".") ?
                        //                        docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty);

                        //string new_path = path + new_tempFileName;

                        //Save the document to doc folder.
                        //CreateDocument(docDTO, new_path);
                        docDTO.TempFileName = docDTO.Path;
                    }
                    docDTO.Content = null;

                    lstDocumentDTO.Add(docDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstDocumentDTO;
        }
        #endregion

        #region - Resource Document -

        public bool SaveResourceDocument(DocumentDTO documentDto)
        {
            try
            {
                return DocumentRepositoryBR.Instance.Save(ConvertToDocument(documentDto));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DocumentDTO> GetResourceDocumentDetails(ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            List<DocumentDTO> lstDocumentDTO = new List<DocumentDTO>();

            try
            {
                List<DocumentEntity> lstDocument = DocumentRepositoryBR.Instance.GetDocumentDetails();

                foreach (DocumentEntity item in lstDocument)
                {
                    DocumentDTO docDTO = ConvertToDocumentDTO(item);
                    //DocumentDTO docDTO = GetResourceDocument(item.DocumentID,path,sessionID,issavedocument);

                    //docDTO.Extention = docDTO.Path.Contains(".") ? docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty;

                    //if ((issavedocument) && ((docDTO.Extention.Equals(".doc")) || (docDTO.Extention.Equals(".docx")) || (docDTO.Extention.Equals(".xls"))
                    //        || (docDTO.Extention.Equals(".xlsx"))))
                    //{
                    //    string leadcustcode = docDTO.DocumentID.ToString();

                    //    string new_tempFileName = GenerateFileName(leadcustcode, sessionID, docDTO.DocumentName.Trim()) + (docDTO.Path.Contains(".") ?
                    //                            docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty);

                    //    string new_path = path + new_tempFileName;

                    //    //Save the document to doc folder.
                    //    CreateDocument(docDTO, new_path);
                    //    docDTO.TempFileName = new_tempFileName;
                    //}
                    //docDTO.Content = null;

                    lstDocumentDTO.Add(docDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstDocumentDTO;
        }

        public DocumentDTO GetResourceDocument(int documentID, string path, string sessionID, bool issavedocument = false)
        {
            DocumentDTO documentDTO = new DocumentDTO();

            try
            {
                documentDTO = ConvertToDocumentDTO(DocumentRepositoryBR.Instance.GetDocumentDetails(documentID));
                if (issavedocument)
                {
                    string leadcustcode = documentDTO.DocumentID.ToString();

                    string new_tempFileName = GenerateFileName(leadcustcode, sessionID, documentDTO.DocumentName.Trim()) + (documentDTO.Path.Contains(".") ?
                                            documentDTO.Path.Substring(documentDTO.Path.LastIndexOf(".")) : string.Empty);

                    string new_path = path + new_tempFileName;
                    documentDTO.TempFileName = new_tempFileName;
                    //Save the document to doc folder.
                    CreateDocument(documentDTO, new_path);
                }
                documentDTO.Content = null;
                documentDTO.Extention = documentDTO.Path.Contains(".") ? documentDTO.Path.Substring(documentDTO.Path.LastIndexOf(".")) : string.Empty;
                return documentDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private bool isFileOpen(string file)
        {
            try
            {
                //first we open the file with a FileStream
                using (FileStream stream = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
                {
                    try
                    {
                        stream.ReadByte();
                        return false;
                    }
                    catch (IOException)
                    {
                        return true;
                    }
                    finally
                    {
                        stream.Close();
                        stream.Dispose();
                    }
                }

            }
            catch (IOException)
            {
                return true;
            }
        }

        private void CreateDocument(DocumentDTO selectedDoc, string tempPath)
        {
            if (!isFileOpen(tempPath))
            {
                FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.ReadWrite);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(selectedDoc.Content);
                bw.Close();

                fs.Close();
            }
        }

        private string GenerateFileName(string leadid, string sessionid, string name)
        {
            /// TODO:
            /// Generate file name according to system path.
            string fn = "";
            DateTime d = DateTime.Now;
            fn += "tmp_" + sessionid.Trim() + "_" + name.Trim() + "_";
            fn += leadid.Trim();   //to generate a unique name
            return fn;
        }

        public bool DeleteResourceDocument(int documentId)
        {
            try
            {
                return DocumentRepositoryBR.Instance.DeleteDocument(documentId);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region - Methods -

        /*
        public List<DocumentDTO> GetLeadDocumentsForView(ArgsDTO args, int leadId, string path, string sessionID, bool issavedocument = false)
        {
            List<DocumentDTO> lstDocumentDTO = new List<DocumentDTO>();

            try
            {
                List<Document> lstDocument = oBrDocument.GetDocumentDetails(leadId, args.OrderBy, args.StartIndex, args.RowCount);

                foreach (Document item in lstDocument)
                {
                    //DocumentDTO docDTO = ConvertToDocumentDTO(item);

                    DocumentDTO docDTO = ConvertToDocumentDTO(oBrDocument.GetDocument(item.DocumentID));
                    docDTO.RowCount = item.RowCount;

                    docDTO.Extention = docDTO.Path.Contains(".") ? docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty;

                    if ((issavedocument) && ((docDTO.Extention.Equals(".doc")) || (docDTO.Extention.Equals(".docx")) || (docDTO.Extention.Equals(".xls"))
                            || (docDTO.Extention.Equals(".xlsx"))))
                    {
                        string leadcustcode = string.Empty;
                        if ((string.IsNullOrWhiteSpace(docDTO.CustCode)) && (docDTO.LeadID != 0))
                            leadcustcode = docDTO.LeadID.ToString();
                        else
                            leadcustcode = docDTO.CustCode.Trim();

                        string new_tempFileName = GenerateFileName(leadcustcode, sessionID, docDTO.DocumentName.Trim()) + (docDTO.Path.Contains(".") ?
                                                docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty);

                        string new_path = path + new_tempFileName;

                        //Save the document to doc folder.
                        CreateDocument(docDTO, new_path);
                        docDTO.TempFileName = new_tempFileName;
                    }
                    docDTO.Content = null;

                    lstDocumentDTO.Add(docDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstDocumentDTO;
        }

        public List<DocumentDTO> GetCustomerDocumentForCustomer(string customerCode, ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            List<DocumentDTO> lstDocumentDTO = new List<DocumentDTO>();

            try
            {
                List<Document> lstDocument = oBrDocument.GetContactDocumentForCustomer(customerCode, args.StartIndex, args.RowCount, args.OrderBy, args.AdditionalParams);

                foreach (Document item in lstDocument)
                {
                    DocumentDTO docDTO = ConvertToDocumentDTO(oBrDocument.GetDocument(item.DocumentID));

                    docDTO.Extention = docDTO.Path.Contains(".") ? docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty;

                    if ((issavedocument) && ((docDTO.Extention.Equals(".doc")) || (docDTO.Extention.Equals(".docx")) || (docDTO.Extention.Equals(".xls"))
                            || (docDTO.Extention.Equals(".xlsx")) || (docDTO.Extention.Equals(".pdf")) || (docDTO.Extention.Equals(".csv"))))
                    {
                        string leadcustcode = string.Empty;
                        if ((string.IsNullOrWhiteSpace(docDTO.CustCode)) && (docDTO.LeadID != 0))
                            leadcustcode = docDTO.LeadID.ToString();
                        else
                            leadcustcode = docDTO.CustCode.Trim();

                        string new_tempFileName = GenerateFileName(leadcustcode, sessionID, docDTO.DocumentName.Trim()) + (docDTO.Path.Contains(".") ?
                                                docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty);

                        string new_path = path + new_tempFileName;

                        //Save the document to doc folder.
                        CreateDocument(docDTO, new_path);
                        docDTO.TempFileName = new_tempFileName;
                    }
                    docDTO.Content = null;

                    lstDocumentDTO.Add(docDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstDocumentDTO;
        }

        public DocumentDTO GetDocumentForView(int documentID, string path, string sessionID, bool issavedocument = false)
        {
            DocumentDTO documentDTO = new DocumentDTO();

            try
            {
                documentDTO = ConvertToDocumentDTO(oBrDocument.GetDocument(documentID));
                if (issavedocument)
                {
                    string leadcustcode = string.Empty;
                    if ((string.IsNullOrWhiteSpace(documentDTO.CustCode)) && (documentDTO.LeadID != 0))
                        leadcustcode = documentDTO.LeadID.ToString();
                    else
                    {
                        //string[] marks = { "&", "'", "<",">"};
                        leadcustcode = documentID.ToString();
                        
                    }

                    string new_tempFileName = GenerateFileName(leadcustcode, sessionID, documentDTO.DocumentName.Trim()) + (documentDTO.Path.Contains(".") ?
                                            documentDTO.Path.Substring(documentDTO.Path.LastIndexOf(".")) : string.Empty);

                    string new_path = path + new_tempFileName;

                    //Save the document to doc folder.
                    CreateDocument(documentDTO, new_path);
                }
                documentDTO.Content = null;
                documentDTO.Extention = documentDTO.Path.Contains(".") ? documentDTO.Path.Substring(documentDTO.Path.LastIndexOf(".")) : string.Empty;
                return documentDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }

        

        
        */

        #endregion
    }
}
