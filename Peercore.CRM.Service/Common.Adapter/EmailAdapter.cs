﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;
using System.IO;
using System.Text.RegularExpressions;
using Peercore.CRM.Service.DTO.CompositeEntities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class EmailAdapter
    {
        #region - Converters -

        public static ImportMailEntity ConvertToImportMail(EmailDTO emailDto)
        {
            ImportMailEntity importMail = ImportMailEntity.CreateObject();

            try
            {
                importMail.ContactPersonName = emailDto.ContactPersonName;
                importMail.Content = emailDto.Content;
                importMail.CustCode = emailDto.CustCode;
                importMail.Date = emailDto.Date;
                importMail.EntryID = emailDto.EntryID;

                importMail.FileContent = emailDto.FileContent;
                importMail.FileName = emailDto.FileName;
                importMail.From = emailDto.From;
                importMail.ImportMailID = emailDto.EmailID;
                importMail.LeadID = emailDto.LeadID;

                importMail.LeadName = emailDto.LeadName;
                importMail.OutlookObject = emailDto.OutlookObject;
                importMail.Subject = emailDto.Subject;
                importMail.To = emailDto.To;

                importMail.RowCount = emailDto.RowCount;
            }
            catch (Exception)
            {

                throw;
            }
            return importMail;
        }

        public static EmailDTO ConvertToEmailDTO(ImportMailEntity importMail, string directoryPath, string sessionId)
        {
            EmailDTO emailDTO = new EmailDTO();

            try
            {
                emailDTO.ContactPersonName = importMail.ContactPersonName;
                emailDTO.Content = importMail.Content;
                emailDTO.CustCode = importMail.CustCode;
                emailDTO.Date = importMail.Date;
                emailDTO.EntryID = importMail.EntryID;

                emailDTO.FileContent = importMail.FileContent;
                emailDTO.FileName = importMail.FileName;
                emailDTO.From = importMail.From;
                emailDTO.EmailID = importMail.ImportMailID;
                emailDTO.LeadID = importMail.LeadID;

                emailDTO.LeadName = importMail.LeadName;
                emailDTO.OutlookObject = importMail.OutlookObject;
                emailDTO.Subject = importMail.Subject;
                emailDTO.To = importMail.To;
                emailDTO.RowCount = importMail.TotalCount;

                emailDTO.FileLocation = directoryPath + "\\tmp_emailbody_" + sessionId + "_" + emailDTO.FileName;

                if(!File.Exists(emailDTO.FileLocation))
                    GlobalService.CreateDocument(emailDTO.FileContent, directoryPath, emailDTO.FileLocation);
                
            }
            catch (Exception)
            {

                throw;
            }
            return emailDTO;
        }

        private EmailDTO ConvertTOEmailDTO(ImportMailEntity importMail, List<EmailAttachmentDTO> emailAttachments, string directoryPath, string sessionId)
        {
            EmailDTO emailDTO = new EmailDTO();
            try
            {
                List<KeyValuePair<string, string>> srcList = new List<KeyValuePair<string, string>>();
                string tempHtml = Encoding.ASCII.GetString(importMail.FileContent);
                List<string> imageHtmlList = GetImageList(tempHtml);

                foreach (EmailAttachmentDTO attachment in emailAttachments)
                {
                    string extension = System.IO.Path.GetExtension(attachment.FileName);

                    if (extension == ".jpg" || extension == ".png" || extension == ".jpeg" || extension == ".gif" || extension == ".bmp"
                        || extension == ".tif" || extension == ".tiff")
                    {
                        string src = "<img src=" + attachment.FilePath + "\">";
                        srcList.Add(new KeyValuePair<string, string>(attachment.FileName, src));
                    }
                }

                if (imageHtmlList.Count > 0)
                {
                    for (int i = 0; i < srcList.Count; i++)
                    {
                        foreach (string item in imageHtmlList)
                        {
                            if (item.Contains(srcList[i].Key))
                            {
                                tempHtml = tempHtml.Replace(item, srcList[i].Value.ToString());
                            }
                        }
                    }
                }

                tempHtml = tempHtml.Replace("</head>", AttachedHead(emailAttachments, importMail, directoryPath));

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                emailDTO.FileName = "tmp_emailbody_" + sessionId + "_" + importMail.ImportMailID + ".html";
                string emailHtmlBody = directoryPath + "\\" + emailDTO.FileName;

                if (!isFileOpen(emailHtmlBody))
                {
                    FileStream fs = new FileStream(emailHtmlBody, FileMode.Create, FileAccess.ReadWrite);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write(tempHtml);
                    sw.Close();
                }

                emailDTO.ContactPersonName = importMail.ContactPersonName;
                emailDTO.Content = importMail.Content;
                emailDTO.CustCode = importMail.CustCode;
                emailDTO.Date = importMail.Date;
                emailDTO.EmailID = importMail.ImportMailID;
                emailDTO.EntryID = importMail.EntryID;
                emailDTO.FileLocation = emailHtmlBody;
                emailDTO.From = importMail.From;
                emailDTO.LeadID = importMail.LeadID;
                emailDTO.LeadName = importMail.LeadName;
                emailDTO.Subject = importMail.Subject;
                emailDTO.To = importMail.To;
            }
            catch (Exception)
            {

                throw;
            }

            return emailDTO;
        }

        private List<string> GetImageList(string HtmlBody)
        {
            List<string> imageTags = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(HtmlBody))
                {
                    Regex expression = new Regex(@"<img[^>]*>");
                    foreach (var item in expression.Matches(HtmlBody))
                    {
                        imageTags.Add(item.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return imageTags;
        }

        private bool isFileOpen(string file)
        {
            try
            {
                //first we open the file with a FileStream
                using (FileStream stream = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
                {
                    try
                    {
                        stream.ReadByte();
                        return false;
                    }
                    catch (IOException)
                    {
                        return true;
                    }
                    finally
                    {
                        stream.Close();
                        stream.Dispose();
                    }
                }

            }
            catch (IOException)
            {
                return true;
            }
        }

        private string AttachedHead(List<EmailAttachmentDTO> attachments, ImportMailEntity oMailItem, string tempPath)
        {

            string HtmlString = "</head><div><div style=\"border-right: medium none; border-width: 1pt medium medium; border-style: solid none none; border-color: rgb(181, 196, 223) -moz-use-text-color -moz-use-text-color; padding: 3pt 0in 0in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: \"Tahoma\",\"sans-serif\";\">";

            if (!string.IsNullOrEmpty(oMailItem.From))
                HtmlString += "<b>From:&nbsp</b>" + oMailItem.From + "</br>";

            if (!string.IsNullOrEmpty(oMailItem.To))
                HtmlString += "<b>To:&nbsp</b>" + oMailItem.To + "</br>";

            if (oMailItem.Date != null)
                HtmlString += "<b>Sent:&nbsp</b>" + oMailItem.Date.ToString("dddd, MMMM dd, yyyy hh:mm tt") + "</br>";

            if (!string.IsNullOrEmpty(oMailItem.Subject))
                HtmlString += "<b>Subject:&nbsp</b>" + oMailItem.Subject + "</br>";

            if (attachments != null)
            {
                HtmlString += "<b>Attachments:&nbsp</b>";
                foreach (EmailAttachmentDTO item in attachments)
                {
                    HtmlString += "<a href=\"" + tempPath + item.FileName + "\" target=\"_blank\">" + item.FileName + ";&nbsp" + "</a>";
                }
            }

            HtmlString += "</br></br></span></p></div></div>";
            return HtmlString;

        }
        #endregion

        #region - Properties -

        //private brImportMail oBrImportMail = null;

        #endregion

        #region - Constructor -

        public EmailAdapter()
        {
            //if (oBrImportMail == null)
            //    oBrImportMail = new brImportMail();
        }

        #endregion

        #region - ImportMail -
        public List<EmailDTO> GetLeadEmails(ArgsDTO args, string directoryPath, string sessionId)
        {
            try
            {
                List<ImportMailEntity> mailList = ImportMailBR.Instance.GetLeadEmails(ActivityAdapter.ConvertToArgsEntity(args));
                List<EmailDTO> emails = new List<EmailDTO>();

                foreach (ImportMailEntity item in mailList)
                {
                    emails.Add(ConvertToEmailDTO(item, directoryPath, sessionId));
                }

                return emails;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<EmailDTO> GetCustomerEmails(string custCode,ArgsDTO args,string directoryPath, string sessionId)
        {
            List<ImportMailEntity> mailList = ImportMailBR.Instance.GetCustEmails(ActivityAdapter.ConvertToArgsEntity(args));
            List<EmailDTO> emails = new List<EmailDTO>();

            try
            {
                foreach (ImportMailEntity item in mailList)
                {
                    emails.Add(ConvertToEmailDTO(item, directoryPath, sessionId));
                }

                return emails;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public EmailAndAttachmentsDTO GetEmailbyId(int emailId, string directoryPath, string sessionId)
        {
            EmailAndAttachmentsDTO emailAndAttachments = new EmailAndAttachmentsDTO();

            try
            {
                CommonAdapter oBrImpotMail = new CommonAdapter();
                emailAndAttachments.Attachments = oBrImpotMail.GetEmailAttachment(emailId, directoryPath, sessionId);
                emailAndAttachments.Email = ConvertTOEmailDTO(ImportMailBR.Instance.GetEmailbyId(emailId), emailAndAttachments.Attachments, directoryPath, sessionId);
            }
            catch (Exception)
            {

                throw;
            }

            return emailAndAttachments;
        }

        #endregion
    }
}
