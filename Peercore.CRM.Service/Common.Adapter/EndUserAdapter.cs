﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.TransferComponents;
using System.Data;
using System.Collections.ObjectModel;
using System.IO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.Text.RegularExpressions;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class EndUserAdapter
    {
        #region - Converters -

        #region - End User -

        private EndUserDTO EndUserDTOConverters(EndUserEntity endUser)
        {
            if (endUser != null)
            {
                try
                {
                    return new EndUserDTO()
                    {
                        Address1 = endUser.Address1,
                        Address2 = endUser.Address2,
                        City = endUser.City,
                        Contact = endUser.Contact,
                        CustomerCode = endUser.CustomerCode,
                        CustomerName = endUser.CustomerName,
                        EmailAddress = endUser.EmailAddress,
                        EndUserCode = endUser.EndUserCode,
                        EndUserType = endUser.EndUserType,
                        IsActive = endUser.IsActive,
                        Name = endUser.Name,
                        PostCode = endUser.PostCode,
                        PrimaryDistributor = endUser.PrimaryDistributor,
                        PrimaryRepName = endUser.PrimaryRepName,
                        Rating = endUser.Rating,
                        RepCode = endUser.RepCode,
                        SecondaryRepName = endUser.SecondaryRepName,
                        State = endUser.State,
                        Telephone = endUser.Telephone,
                        Version = endUser.Version,
                        RowCount = endUser.TotalCount,
                        ViewRowCount = endUser.ViewRowCount
                    };
                }
                catch (Exception)
                {

                    throw;
                }
            }
            else {
                return new EndUserDTO();

            }
        }

        private EndUserEntity EndUserConverters(EndUserDTO endUser)
        {
            EndUserEntity item = EndUserEntity.CreateObject();
            item.Address1 = endUser.Address1;
            item.Address2 = endUser.Address2;
            item.City = endUser.City;
            item.Contact = endUser.Contact;
            item.CustomerCode = endUser.CustomerCode;
            item.CustomerName = endUser.CustomerName;
            item.EmailAddress = endUser.EmailAddress;
            item.EndUserCode = endUser.EndUserCode;
            item.EndUserType = endUser.EndUserType;
            item.IsActive = endUser.IsActive;
            item.Name = endUser.Name;
            item.PostCode = endUser.PostCode;
            item.PrimaryDistributor = endUser.PrimaryDistributor;
            item.PrimaryRepName = endUser.PrimaryRepName;
            item.Rating = endUser.Rating;
            item.RepCode = endUser.RepCode;
            item.SecondaryRepName = endUser.SecondaryRepName;
            item.State = endUser.State;
            item.Telephone = endUser.Telephone;
            item.Version = endUser.Version;
            item.CustomerName = endUser.CustomerName;
            return item;
        }

        private EndUserPriceDTO EndUserPriceDTOConverters(EndUserPriceEntity endUserPrice)
        {
            try
            {
                return new EndUserPriceDTO()
                {
                    CatlogCode = endUserPrice.CatlogCode,
                    CustCode = endUserPrice.CustCode,
                    CustomerName = endUserPrice.CustomerName,
                    Description = endUserPrice.Description,
                    EffectiveDate = endUserPrice.EffectiveDate,
                    EndUserCode = endUserPrice.EndUserCode,
                    EndUserName = endUserPrice.EndUserName,
                    LastUpdated = endUserPrice.LastUpdated.ToLocalTime(),
                    Originator = endUserPrice.Originator,
                    Price = endUserPrice.Price,
                    Version = endUserPrice.Version,
                    RowCount=endUserPrice.TotalCount,
                    EffectiveDateString=endUserPrice.EffectiveDateString,
                    LastUpdatedString=endUserPrice.LastUpdatedString
                };
            }
            catch (Exception)
            {

                throw;
            }
        }

        private EndUserPriceEntity EndUserPriceConverters(EndUserPriceDTO endUserPriceDto)
        {
            EndUserPriceEntity item = new EndUserPriceEntity();
            item.CatlogCode = endUserPriceDto.CatlogCode;
            item.CustCode = endUserPriceDto.CustCode;
            item.CustomerName = endUserPriceDto.CustomerName;
            item.Description = endUserPriceDto.Description;
            item.EffectiveDate = endUserPriceDto.EffectiveDate;
            item.EndUserCode = endUserPriceDto.EndUserCode;
            item.EndUserName = endUserPriceDto.EndUserName;
            item.LastUpdated = endUserPriceDto.LastUpdated;
            item.Originator = endUserPriceDto.Originator;
            item.Price = endUserPriceDto.Price;
            item.Version = endUserPriceDto.Version;
            return item;
        }

        private EnduserEnquiryDTO EnduserEnquiryDTOConverters(EnduserEnquiryEntity enduserEnquiry)
        {
            try
            {
                return new EnduserEnquiryDTO()
                {
                    CasesSupplied = enduserEnquiry.CasesSupplied,
                    CatlogCode = enduserEnquiry.CatlogCode,
                    CurPrice = enduserEnquiry.CurPrice,
                    CustomerCode = enduserEnquiry.CustomerCode,
                    CustomerName = enduserEnquiry.CustomerName,
                    Description = enduserEnquiry.Description,
                    EndUserCode = enduserEnquiry.EndUserCode,
                    EndUserName = enduserEnquiry.EndUserName,
                    Price = enduserEnquiry.Price,
                    RebateMonth = enduserEnquiry.RebateMonth,
                    RebatePerc = enduserEnquiry.RebatePerc,
                    RepCode = enduserEnquiry.RepCode,
                    RepName = enduserEnquiry.RepName,
                    TotalCases = enduserEnquiry.TotalCases,
                    Cases1 = enduserEnquiry.month1,
                    Cases2 = enduserEnquiry.month2,
                    Cases3 = enduserEnquiry.month3,
                    Cases4 = enduserEnquiry.month4,
                    Cases5 = enduserEnquiry.month5,
                    Cases6 = enduserEnquiry.month6,
                    Cases7 = enduserEnquiry.month7,
                    Cases8 = enduserEnquiry.month8,
                    Cases9 = enduserEnquiry.month9,
                    Cases10 = enduserEnquiry.month10,
                    Cases11 = enduserEnquiry.month11,
                    Cases12 = enduserEnquiry.month12,
                    RowCount= enduserEnquiry.TotalCount
                };
            }
            catch (Exception)
            {

                throw;
            }
        }

        private EnduserEnquiryEntity EnduserEnquiryConverters(EnduserEnquiryDTO enduserEnquiry)
        {
            EnduserEnquiryEntity item = EnduserEnquiryEntity.CreateObject();

            item.CasesSupplied = enduserEnquiry.CasesSupplied;
            item.CatlogCode = enduserEnquiry.CatlogCode;
            item.CurPrice = enduserEnquiry.CurPrice;
            item.CustomerCode = enduserEnquiry.CustomerCode;
            item.CustomerName = enduserEnquiry.CustomerName;
            item.Description = enduserEnquiry.Description;
            item.EndUserCode = enduserEnquiry.EndUserCode;
            item.EndUserName = enduserEnquiry.EndUserName;
            item.Price = enduserEnquiry.Price;
            item.RebateMonth = enduserEnquiry.RebateMonth;
            item.RebatePerc = enduserEnquiry.RebatePerc;
            item.RepCode = enduserEnquiry.RepCode;
            item.RepName = enduserEnquiry.RepName;
            return item;
        }

        private EndUserSalesDTO EndUserSalesDTOConverters(EndUserSalesEntity endUserSales)
        {
            try
            {
                return new EndUserSalesDTO()
                {
                    Cases = endUserSales.Cases,
                    Market = endUserSales.Market,
                    Month = endUserSales.Month,
                    Tonnes1 = endUserSales.Tonnes1,
                    Year = endUserSales.Year
                };
            }
            catch (Exception)
            {

                throw;
            }
        }

        private EndUserSalesEntity EndUserConverters(EndUserSalesDTO endUserSalesDTO)
        {
            EndUserSalesEntity item = EndUserSalesEntity.CreateObject();
            item.Cases = endUserSalesDTO.Cases;
            item.Market = endUserSalesDTO.Market;
            item.Month = endUserSalesDTO.Month;
            item.Tonnes1 = endUserSalesDTO.Tonnes;
            item.Year = endUserSalesDTO.Year;
            return item;
        }

        #endregion

        #region - EndUserContactPerson -
        private EndUserContactPersonEntity EndUserContactPersonConvert(ContactPersonDTO oContactPersonDTO)
        {
            EndUserContactPersonEntity contactPerson = null;
            try
            {
                contactPerson = EndUserContactPersonEntity.CreateObject();
                contactPerson.CompanyAddress = oContactPersonDTO.CompanyAddress;
                contactPerson.CompanyName = oContactPersonDTO.CompanyName;
                contactPerson.CompanyTelephone = oContactPersonDTO.CompanyTelephone;
                contactPerson.Contact = oContactPersonDTO.Contact;
                contactPerson.ContactPersonID = oContactPersonDTO.ContactPersonID;
                contactPerson.CreatedBy = oContactPersonDTO.CreatedBy;
                contactPerson.CreatedDate = oContactPersonDTO.CreatedDate;
                contactPerson.CustCode = oContactPersonDTO.CustCode;
                contactPerson.Description = oContactPersonDTO.Description;
                contactPerson.DisLikes = oContactPersonDTO.DisLikes;
                contactPerson.EmailAddress = oContactPersonDTO.EmailAddress;
                contactPerson.Fax = oContactPersonDTO.Fax;
                contactPerson.FirstName = oContactPersonDTO.FirstName;
                //contactPerson.IsOwner = oContactPersonDTO.IsOwner;
                contactPerson.KeyContact = oContactPersonDTO.KeyContact;
                //contactPerson.KeyContactChecked = oContactPersonDTO.KeyContactChecked;
                contactPerson.LastModifiedBy = oContactPersonDTO.LastModifiedBy;
                contactPerson.LastModifiedDate = oContactPersonDTO.LastModifiedDate;
                contactPerson.LastName = oContactPersonDTO.LastName;
                contactPerson.EndUserCode = oContactPersonDTO.EndUserCode;
                contactPerson.EndUserName = oContactPersonDTO.EndUserName;
                contactPerson.Likes = oContactPersonDTO.Likes;
                contactPerson.MailingAddress = oContactPersonDTO.MailingAddress;
                contactPerson.MailingCity = oContactPersonDTO.MailingCity;
                contactPerson.MailingCountry = oContactPersonDTO.MailingCountry;
                contactPerson.MailingPostcode = oContactPersonDTO.MailingPostcode;
                contactPerson.MailingState = oContactPersonDTO.MailingState;
                contactPerson.Mobile = oContactPersonDTO.Mobile;
                contactPerson.Note = oContactPersonDTO.Note;
                contactPerson.Origin = oContactPersonDTO.Origin;
                contactPerson.Originator = oContactPersonDTO.Originator;
                contactPerson.OtherAddress = oContactPersonDTO.OtherAddress;
                contactPerson.OtherCity = oContactPersonDTO.OtherCity;
                contactPerson.OtherCountry = oContactPersonDTO.OtherCountry;
                contactPerson.OtherPostCode = oContactPersonDTO.OtherPostCode;
                contactPerson.OtherState = oContactPersonDTO.OtherState;
                contactPerson.Position = oContactPersonDTO.Position;
                contactPerson.ReportsTo = oContactPersonDTO.ReportsTo;
                contactPerson.RowCount = oContactPersonDTO.RowCount;
                contactPerson.SpecialInterests = oContactPersonDTO.SpecialInterests;
                contactPerson.Status = oContactPersonDTO.Status;
                contactPerson.Telephone = oContactPersonDTO.Telephone;
                contactPerson.Title = oContactPersonDTO.Title;
                contactPerson.ImagePath = oContactPersonDTO.ImagePath; //Kavisha
                contactPerson.ContactType = oContactPersonDTO.ContactType; //kavisha
                contactPerson.CreatedBy = oContactPersonDTO.CreatedBy;
                contactPerson.CreatedDate = oContactPersonDTO.CreatedDate;
            }
            catch (Exception)
            {
                throw;
            }
            return contactPerson;
        }

        private ContactPersonDTO ContactPersonDTOConvert(EndUserContactPersonEntity endUserContactPerson)
        {
            ContactPersonDTO contactPerson = null;
            try
            {
                contactPerson = new ContactPersonDTO();
                contactPerson.CompanyAddress = endUserContactPerson.CompanyAddress;
                contactPerson.CompanyName = endUserContactPerson.CompanyName;
                contactPerson.CompanyTelephone = endUserContactPerson.CompanyTelephone;
                contactPerson.Contact = endUserContactPerson.Contact;
                contactPerson.ContactPersonID = endUserContactPerson.ContactPersonID;
                contactPerson.CreatedBy = endUserContactPerson.CreatedBy;
                contactPerson.CreatedDate = endUserContactPerson.CreatedDate;
                contactPerson.CustCode = endUserContactPerson.CustCode;
                contactPerson.Description = endUserContactPerson.Description;
                contactPerson.DisLikes = endUserContactPerson.DisLikes;
                contactPerson.EmailAddress = endUserContactPerson.EmailAddress;
                contactPerson.Fax = endUserContactPerson.Fax;
                contactPerson.FirstName = endUserContactPerson.FirstName;
                //contactPerson.IsOwner = oContactPersonDTO.IsOwner;
                contactPerson.KeyContact = endUserContactPerson.KeyContact;
                //contactPerson.KeyContactChecked = oContactPersonDTO.KeyContactChecked;
                contactPerson.LastModifiedBy = endUserContactPerson.LastModifiedBy;
                contactPerson.LastModifiedDate = endUserContactPerson.LastModifiedDate;
                contactPerson.LastName = endUserContactPerson.LastName;
                contactPerson.EndUserCode = endUserContactPerson.EndUserCode;
                contactPerson.EndUserName = endUserContactPerson.EndUserName;
                contactPerson.Likes = endUserContactPerson.Likes;
                contactPerson.MailingAddress = endUserContactPerson.MailingAddress;
                contactPerson.MailingCity = endUserContactPerson.MailingCity;
                contactPerson.MailingCountry = endUserContactPerson.MailingCountry;
                contactPerson.MailingPostcode = endUserContactPerson.MailingPostcode;
                contactPerson.MailingState = endUserContactPerson.MailingState;
                contactPerson.Mobile = endUserContactPerson.Mobile;
                contactPerson.Note = endUserContactPerson.Note;
                contactPerson.Origin = endUserContactPerson.Origin;
                contactPerson.Originator = endUserContactPerson.Originator;
                contactPerson.OtherAddress = endUserContactPerson.OtherAddress;
                contactPerson.OtherCity = endUserContactPerson.OtherCity;
                contactPerson.OtherCountry = endUserContactPerson.OtherCountry;
                contactPerson.OtherPostCode = endUserContactPerson.OtherPostCode;
                contactPerson.OtherState = endUserContactPerson.OtherState;
                contactPerson.Position = endUserContactPerson.Position;
                contactPerson.ReportsTo = endUserContactPerson.ReportsTo;
                contactPerson.RowCount = endUserContactPerson.RowCount;
                contactPerson.SpecialInterests = endUserContactPerson.SpecialInterests;
                contactPerson.Status = endUserContactPerson.Status;
                contactPerson.Telephone = endUserContactPerson.Telephone;
                contactPerson.Title = endUserContactPerson.Title;
                contactPerson.ContactType = endUserContactPerson.ContactType;

            }
            catch (Exception)
            {
                throw;
            }
            return contactPerson;
        }

        public static ContactPersonImageDTO ConvertToEndUserContactPersonDTO(EndUserContactPersonImageEntity contactPersonImage, string directoryPath, string sessionId)
        {
            ContactPersonImageDTO contactPersonImageDTO = new ContactPersonImageDTO();

            try
            {
                if (contactPersonImage != null)
                {
                    contactPersonImageDTO.ContactNo = contactPersonImage.ContactNo;
                    contactPersonImageDTO.ContactPersonId = contactPersonImage.ContactPersonId;
                    contactPersonImageDTO.ContactPersonImageId = contactPersonImage.ContactPersonImageId;
                    contactPersonImageDTO.CustomerCode = contactPersonImage.CustomerCode;

                    contactPersonImageDTO.FileName = contactPersonImage.FileName;
                    contactPersonImageDTO.EndUserCode = contactPersonImage.EndUserCode;

                    contactPersonImageDTO.FilePath = directoryPath + "\\contactperson_" + sessionId + "_" + contactPersonImage.FileName;

                    if (!File.Exists(contactPersonImageDTO.FilePath))
                        GlobalService.CreateDocument(contactPersonImage.FileContent, directoryPath, contactPersonImageDTO.FilePath);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return contactPersonImageDTO;
        }

        public static EndUserContactPersonImageEntity ConvertToEndUserContactPerson(ContactPersonImageDTO contactPersonImageDto)
        {
            EndUserContactPersonImageEntity contactPersonImage = new EndUserContactPersonImageEntity();

            try
            {
                if (contactPersonImageDto != null)
                {
                    contactPersonImage.ContactNo = contactPersonImageDto.ContactNo;
                    contactPersonImage.ContactPersonId = contactPersonImageDto.ContactPersonId;
                    contactPersonImage.ContactPersonImageId = contactPersonImageDto.ContactPersonImageId;
                    contactPersonImage.CustomerCode = contactPersonImageDto.CustomerCode;

                    contactPersonImage.FileName = contactPersonImageDto.FileName;
                    contactPersonImage.EndUserCode = contactPersonImageDto.EndUserCode;
                   // contactPersonImage.FileContent = GlobalService.ConvertFile(contactPersonImageDto.FilePath);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return contactPersonImage;
        }
        #endregion

        #endregion

        #region - End User -

        public bool EndUserSave(EndUserDTO endUserDto)
        {
            try
            {
                return EndUserBR.Instance.Save(EndUserConverters(endUserDto));
            }
            catch
            {
                throw;
            }
        }

        public EndUserDTO GetEndUser(ArgsDTO args)
        {
            try
            {
                return EndUserDTOConverters(EndUserBR.Instance.GetEndUser(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserDTO> GetEndUsersByCode(string enduserCodes)
        {
            try
            {
                List<EndUserDTO> endUserDtoList = new List<EndUserDTO>();
                List<EndUserEntity> endUserList = EndUserBR.Instance.GetEndUsersByCode(enduserCodes);
                foreach (EndUserEntity item in endUserList)
                {
                    endUserDtoList.Add(EndUserDTOConverters(item));
                }
                return endUserDtoList;
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserDTO> GetEndUsers(ArgsDTO args, string sStatus)
        {
            try
            {
                List<EndUserDTO> endUserDtoList = new List<EndUserDTO>();
                List<EndUserEntity> endUserList = EndUserBR.Instance.GetEndUsers(ActivityAdapter.ConvertToArgsEntity(args), sStatus);
                foreach (EndUserEntity item in endUserList)
                {
                    endUserDtoList.Add(EndUserDTOConverters(item));
                }
                return endUserDtoList;
            }
            catch
            {
                throw;
            }
        }

        public int SetActiveStatus(ArgsDTO args, bool isActive)
        {
            try
            {
                return EndUserBR.Instance.SetActiveStatus(ActivityAdapter.ConvertToArgsEntity(args), isActive);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserDTO> GetAllEndUsers(ArgsDTO args)
        {
            try
            {
                List<EndUserDTO> endUserDtoList = new List<EndUserDTO>();
                List<EndUserEntity> endUserList = EndUserBR.Instance.GetAllEndUsers(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (EndUserEntity item in endUserList)
                {
                    endUserDtoList.Add(EndUserDTOConverters(item));
                }
                return endUserDtoList;
            }
            catch
            {
                throw;
            }
        }

        public EndUserDTO IsEndUserExistForCustomer(ArgsDTO args)
        {
            try
            {
                return EndUserDTOConverters(EndUserBR.Instance.IsEndUserExistForCustomer(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserDTO> IsEndUserExistForOtherCustomers(ArgsDTO args)
        {
            try
            {
                List<EndUserDTO> endUserDtoList = new List<EndUserDTO>();
                List<EndUserEntity> endUserList = EndUserBR.Instance.IsEndUserExistForOtherCustomers(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (EndUserEntity item in endUserList)
                {
                    endUserDtoList.Add(EndUserDTOConverters(item));
                }
                return endUserDtoList;
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserPriceDTO> GetEndUserPricing(ArgsDTO args, string effectiveDate)
        {
            try
            {
                List<EndUserPriceDTO> endUserPriceDtoList = new List<EndUserPriceDTO>();
                List<EndUserPriceEntity> endUserPriceList = EndUserBR.Instance.GetEndUserPricing(ActivityAdapter.ConvertToArgsEntity(args), effectiveDate);
                foreach (EndUserPriceEntity item in endUserPriceList)
                {
                    endUserPriceDtoList.Add(EndUserPriceDTOConverters(item));
                }
                return endUserPriceDtoList;
            }
            catch
            {
                throw;
            }
        }

        //public List<EnduserEnquiryDTO> GetEnduserSalesWithAllProducts(ArgsDTO args, bool bIncludeProductsWithNoSales, bool bActiveOnly)
        //{
        //    try
        //    {
        //        List<EnduserEnquiryDTO> EnduserEnquiryDtoList = new List<EnduserEnquiryDTO>();

        //        int fromyear = 0;
        //        int frommonth = 0;

        //        ProcessYearAndMonth(args.SYear, args.Period, ref fromyear, ref frommonth);
        //        args.SMonth = frommonth;

        //        DateTime _startdate = DateTime.Now;
        //        DateTime _enddate = DateTime.Now;

        //        GetPeriods(args.SMonth, args.SYear, ref _startdate, ref _enddate);

        //        args.DtStartDate = _startdate;
        //        args.DtEndDate = _enddate;

        //        List<EnduserEnquiryEntity> EnduserEnquiryList = EndUserBR.Instance.GetEnduserSalesWithAllProducts(ActivityAdapter.ConvertToArgsEntity(args)
        //                                                , bIncludeProductsWithNoSales, bActiveOnly);

        //        int i = 0;
        //        foreach (EnduserEnquiryEntity item in EnduserEnquiryList)
        //        {
        //            EnduserEnquiryDtoList.Add(EnduserEnquiryDTOConverters(item));
        //            EnduserEnquiryDTO enduserEnquiryDTO = EnduserEnquiryDtoList[i] as EnduserEnquiryDTO;
        //            GetMonth(frommonth, fromyear, ref enduserEnquiryDTO);
        //            i++;
        //        }

        //        return EnduserEnquiryDtoList;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public List<EnduserEnquiryDTO> GetEnduserSalesWithAllProducts(ArgsDTO args, bool bIncludeProductsWithNoSales, bool bActiveOnly)
        {
            try
            {
                List<EnduserEnquiryDTO> EnduserEnquiryDtoList = new List<EnduserEnquiryDTO>();

                //int fromyear = 0;
                //int frommonth = 0;

                //ProcessYearAndMonth(args.SYear, args.Period, ref fromyear, ref frommonth);
                //args.SMonth = frommonth;

                //DateTime _startdate = DateTime.Now;
                //DateTime _enddate = DateTime.Now;

                //GetPeriods(args.SMonth, args.SYear, ref _startdate, ref _enddate);

                //args.DtStartDate = _startdate;
                //args.DtEndDate = _enddate;

                List<EnduserEnquiryEntity> EnduserEnquiryList = EndUserBR.Instance.GetEnduserSalesWithAllProducts(ActivityAdapter.ConvertToArgsEntity(args)
                                                        , bIncludeProductsWithNoSales, bActiveOnly);

                int i = 0;
                foreach (EnduserEnquiryEntity item in EnduserEnquiryList)
                {
                    EnduserEnquiryDtoList.Add(EnduserEnquiryDTOConverters(item));
                    //EnduserEnquiryDTO enduserEnquiryDTO = EnduserEnquiryDtoList[i] as EnduserEnquiryDTO;
                    //GetMonth(frommonth, fromyear, ref enduserEnquiryDTO);
                    i++;
                }

                return EnduserEnquiryDtoList;
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserSalesDTO> GetEnduserSales(ArgsDTO args, bool bActiveOnly)
        {
            try
            {
                List<EndUserSalesDTO> EndUserSalesDtoList = new List<EndUserSalesDTO>();
                List<EndUserSalesEntity> endUserPriceList = EndUserBR.Instance.GetEnduserSales(ActivityAdapter.ConvertToArgsEntity(args), bActiveOnly);
                foreach (EndUserSalesEntity item in endUserPriceList)
                {
                    EndUserSalesDtoList.Add(EndUserSalesDTOConverters(item));
                }
                return EndUserSalesDtoList;
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerDTO> GetEndUserCustomers(ArgsDTO args)
        {
            try
            {
                List<CustomerDTO> customerDtoList = new List<CustomerDTO>();
                List<CustomerEntity> customerList = EndUserBR.Instance.GetEndUserCustomers(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (CustomerEntity item in customerList)
                {
                    customerDtoList.Add(CustomerAdapter.ConvertToCustomerDTO(item));
                }
                return customerDtoList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Transfer(EndUserDTO endUser, ArgsDTO args)
        {
            try
            {
                return EndUserBR.Instance.Transfer(EndUserConverters(endUser), ActivityAdapter.ConvertToArgsEntity(args));
            }
            catch
            {
                throw;
            }
        }

        public EndUserPriceDTO GetEndUserPrice(ArgsDTO args, DateTime effectiveDate, string catalogCode)
        {
            try
            {
                return EndUserPriceDTOConverters(EndUserBR.Instance.GetEndUserPrice(ActivityAdapter.ConvertToArgsEntity(args), effectiveDate, catalogCode));
            }
            catch
            {
                throw;
            }
        }

        private void ProcessYearAndMonth(int costyear, int costperiod, ref int fromyear, ref int frommonth)
        {
            for (int Li = (costyear - 1); Li <= costyear; Li++)
            {
                for (int Li_row = 1; Li_row <= 12; Li_row++)
                {
                    if (((Li * 12 + Li_row) > ((costyear - 1) * 12 + costperiod)))
                    {
                        fromyear = Li;
                        frommonth = Li_row;
                        break;
                    }
                }
            }
        }

        private void GetPeriods(int Li_cost_period, int Li_cost_year, ref DateTime startdate, ref DateTime enddate)
        {
            if (Li_cost_period == 1)
                startdate = new DateTime(Li_cost_year - 1, 7, 1);
            else if (Li_cost_period == 2)
                startdate = new DateTime(Li_cost_year - 1, 8, 1);
            else if (Li_cost_period == 3)
                startdate = new DateTime(Li_cost_year - 1, 9, 1);
            else if (Li_cost_period == 4)
                startdate = new DateTime(Li_cost_year - 1, 10, 1);
            else if (Li_cost_period == 5)
                startdate = new DateTime(Li_cost_year - 1, 11, 1);
            else if (Li_cost_period == 6)
                startdate = new DateTime(Li_cost_year - 1, 12, 1);
            else if (Li_cost_period == 7)
                startdate = new DateTime(Li_cost_year, 1, 1);
            else if (Li_cost_period == 8)
                startdate = new DateTime(Li_cost_year, 2, 1);
            else if (Li_cost_period == 9)
                startdate = new DateTime(Li_cost_year, 3, 1);
            else if (Li_cost_period == 10)
                startdate = new DateTime(Li_cost_year, 4, 1);
            else if (Li_cost_period == 11)
                startdate = new DateTime(Li_cost_year, 5, 1);
            else if (Li_cost_period == 12)
                startdate = new DateTime(Li_cost_year, 6, 1);

            enddate = startdate.AddMonths(1).AddDays(-1);
        }

        private string GetMonth(int Li_period, int lv_year, ref EnduserEnquiryDTO enduserEnquiryDTO)
        {
            if (Li_period == 1)
                enduserEnquiryDTO.Header1= "Jul" + Environment.NewLine + " " + Convert.ToString(lv_year - 1).Substring(2);
            else if (Li_period == 2)
                enduserEnquiryDTO.Header2 = "Aug" + Environment.NewLine + " " + Convert.ToString(lv_year - 1).Substring(2);
            else if (Li_period == 3)
                enduserEnquiryDTO.Header3 = "Sep" + Environment.NewLine + " " + Convert.ToString(lv_year - 1).Substring(2);
            else if (Li_period == 4)
                enduserEnquiryDTO.Header4 = "Oct" + Environment.NewLine + " " + Convert.ToString(lv_year - 1).Substring(2);
            else if (Li_period == 5)
                enduserEnquiryDTO.Header5 = "Nov" + Environment.NewLine + " " + Convert.ToString(lv_year - 1).Substring(2);
            else if (Li_period == 6)
                enduserEnquiryDTO.Header6 = "Dec" + Environment.NewLine + " " + Convert.ToString(lv_year - 1).Substring(2);
            else if (Li_period == 7)
                enduserEnquiryDTO.Header7 = "Jan" + Environment.NewLine + " " + Convert.ToString(lv_year).Substring(2);
            else if (Li_period == 8)
                enduserEnquiryDTO.Header8 = "Feb" + Environment.NewLine + " " + Convert.ToString(lv_year).Substring(2);
            else if (Li_period == 9)
                enduserEnquiryDTO.Header9 = "Mar" + Environment.NewLine + " " + Convert.ToString(lv_year).Substring(2);
            else if (Li_period == 10)
                enduserEnquiryDTO.Header10 = "Apr" + Environment.NewLine + " " + Convert.ToString(lv_year).Substring(2);
            else if (Li_period == 11)
                enduserEnquiryDTO.Header11 = "May" + Environment.NewLine + " " + Convert.ToString(lv_year).Substring(2);
            else if (Li_period == 12)
                enduserEnquiryDTO.Header12 = "Jun" + Environment.NewLine + " " + Convert.ToString(lv_year).Substring(2);

            return "";
        }

        #endregion

        #region - EndUserContactPerson -



        public bool SaveEndUserContact(ContactPersonDTO endUserContactPerson, ref int contactPersonId)
        {
            try
            {
                return EndUserContactPersonBR.Instance.SaveEndUserContact(EndUserContactPersonConvert(endUserContactPerson), ref contactPersonId);
            }
            catch
            {
                throw;
            }
        }

        public ContactPersonDTO GetEndUserContactPerson(int iContactPersonID)
        {
            try
            {
                return ContactPersonDTOConvert(EndUserContactPersonBR.Instance.GetEndUserContactPerson(iContactPersonID));
            }
            catch
            {
                throw;
            }
        }

        public List<ContactPersonDTO> GetEndUserContactPersons(ArgsDTO args)
        {
            try
            {
                List<EndUserContactPersonEntity> contactList = EndUserContactPersonBR.Instance.GetEndUserContactPerson(ActivityAdapter.ConvertToArgsEntity(args));
                List<ContactPersonDTO> contactDtoList = new List<ContactPersonDTO>();
                foreach (EndUserContactPersonEntity item in contactList)
                {
                    contactDtoList.Add(ContactPersonDTOConvert(item));
                }
                return contactDtoList;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveEndUserContactPersonImage(ContactPersonImageDTO contactPersonImage, ref int contactPersonImageId)
        {
            try
            {

                return EndUserContactPersonImageBR.Instance.Save(ConvertToEndUserContactPerson(contactPersonImage), ref contactPersonImageId) ;
            }

            catch (Exception)
            {

                throw;
            }
        }

        public ContactPersonImageDTO GetEndUserContactPersonImage(int contactPersonId, string endUserCode, string directoryPath, string sessionId)
        {
            try
            {
                return ConvertToEndUserContactPersonDTO(EndUserContactPersonImageBR.Instance.GetContactPersonImage(contactPersonId, endUserCode), directoryPath, sessionId);
            }

            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Price List
        public bool SavePriceList(ArgsDTO args, DateTime dtmEffectiveDate, List<EndUserPriceDTO> listEndUserPrice)
        {
            try
            {
                List<EndUserPriceEntity> EndUserPriceList = new List<EndUserPriceEntity>();

                foreach (EndUserPriceDTO item in listEndUserPrice)
                {
                    EndUserPriceList.Add(EndUserPriceConverters(item));
                }

                return EndUserBR.Instance.SavePriceList(ActivityAdapter.ConvertToArgsEntity(args),
                    dtmEffectiveDate, EndUserPriceList);
            }

            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
