﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Entities;
using Peercore.CRM.Service.DTO;
using System.Collections.ObjectModel;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class LeadCustomerAdapter
    {
        #region - Converters -

        public static LeadCustomerViewEntity ConvertToLeadCustomerView(LeadCustomerDTO lead)
        {
            LeadCustomerViewEntity oLeadCustomerView = LeadCustomerViewEntity.CreateObject();
            try
            {
                oLeadCustomerView.Address = lead.Address;
                oLeadCustomerView.AnnualRevenue = lead.AnnualRevenue;
                oLeadCustomerView.Business = lead.Business;
                oLeadCustomerView.BusinessPotential = lead.BusinessPotential;
                oLeadCustomerView.ChannelDescription = lead.ChannelDescription;
                oLeadCustomerView.City = lead.City;
                oLeadCustomerView.Company = lead.Company;
                oLeadCustomerView.Country = lead.Country;
                oLeadCustomerView.CustomerCode = lead.CustomerCode;
                //oLeadCustomer.CustomerGroup = lead.CustomerGroup;
                oLeadCustomerView.Description = lead.Description;
                oLeadCustomerView.Email = lead.Email;
                oLeadCustomerView.Fax = lead.Fax;
                oLeadCustomerView.Industry = lead.Industry;
                oLeadCustomerView.IndustryDescription = lead.IndustryDescription;
                oLeadCustomerView.IsDeleted = lead.IsDeleted;
                //oLeadCustomer.LastCalledDate = lead.LastCalledDate;
                oLeadCustomerView.LeadCustomerType = lead.LeadCustomerType;
                oLeadCustomerView.LeadSource = lead.LeadSource;
                oLeadCustomerView.LeadStage = lead.LeadStage;
                oLeadCustomerView.LeadStatus = lead.LeadStatus;
                oLeadCustomerView.Mobile = lead.Mobile;
                oLeadCustomerView.Name = lead.Name;
                oLeadCustomerView.NoOfEmployees = lead.NoOfEmployees;
                oLeadCustomerView.NoOfLeadCustomers = lead.NoOfLeadCustomers;
                oLeadCustomerView.Originator = lead.Originator;
                oLeadCustomerView.PostalCode = lead.PostalCode;
                oLeadCustomerView.PreferredContact = lead.PreferredContact;
                oLeadCustomerView.PreferredContactDescription = lead.PreferredContactDescription;
                oLeadCustomerView.Rating = lead.Rating;
                oLeadCustomerView.ReferredBy = lead.ReferredBy;
                //oLeadCustomer.Selection = lead.Selection;
                //oLeadCustomer.ServiceChecklistColor = lead.ServiceChecklistColor;
                //oLeadCustomer.ServiceChecklistTooltip = lead.ServiceChecklistTooltip;
                oLeadCustomerView.SourceId = lead.SourceId;
                //oLeadCustomer.StartDate = lead.StartDate;
                oLeadCustomerView.State = lead.State;
                oLeadCustomerView.Telephone = lead.Telephone;
                oLeadCustomerView.Webiste = lead.Webiste;
                oLeadCustomerView.RowCount = lead.RowCount;
                oLeadCustomerView.EndUserCode = lead.EndUserCode;

            }
            catch (Exception)
            {

                throw;
            }
            return oLeadCustomerView;
        }

        public static LeadCustomerDTO ConvertToLeadCustomer(LeadCustomerViewEntity lead)
        {
            LeadCustomerDTO oLeadCustomer = new LeadCustomerDTO();
            try
            {
                oLeadCustomer.Address = lead.Address;
                oLeadCustomer.AnnualRevenue = lead.AnnualRevenue;
                oLeadCustomer.Business = lead.Business;
                oLeadCustomer.BusinessPotential = lead.BusinessPotential;
                oLeadCustomer.ChannelDescription = lead.ChannelDescription;
                oLeadCustomer.City = lead.City;
                oLeadCustomer.Company = lead.Company;
                oLeadCustomer.Country = lead.Country;
                oLeadCustomer.CustomerCode = lead.CustomerCode;
                //oLeadCustomer.CustomerGroup = lead.CustomerGroup;
                oLeadCustomer.Description = lead.Description;
                oLeadCustomer.Email = lead.Email;
                oLeadCustomer.Fax = lead.Fax;
                oLeadCustomer.Industry = lead.Industry;
                oLeadCustomer.IndustryDescription = lead.IndustryDescription;
                oLeadCustomer.IsDeleted = lead.IsDeleted;
                //oLeadCustomer.LastCalledDate = lead.LastCalledDate;
                oLeadCustomer.LeadCustomerType = lead.LeadCustomerType;
                oLeadCustomer.LeadSource = lead.LeadSource;
                oLeadCustomer.LeadStage = lead.LeadStage;
                oLeadCustomer.LeadStatus = lead.LeadStatus;
                oLeadCustomer.Mobile = lead.Mobile;
                oLeadCustomer.Name = lead.Name;
                oLeadCustomer.NoOfEmployees = lead.NoOfEmployees;
                oLeadCustomer.NoOfLeadCustomers = lead.NoOfLeadCustomers;
                oLeadCustomer.Originator = lead.Originator;
                oLeadCustomer.PostalCode = lead.PostalCode;
                oLeadCustomer.PreferredContact = lead.PreferredContact;
                oLeadCustomer.PreferredContactDescription = lead.PreferredContactDescription;
                oLeadCustomer.Rating = lead.Rating;
                oLeadCustomer.ReferredBy = lead.ReferredBy;
                //oLeadCustomer.Selection = lead.Selection;
                //oLeadCustomer.ServiceChecklistColor = lead.ServiceChecklistColor;
                //oLeadCustomer.ServiceChecklistTooltip = lead.ServiceChecklistTooltip;
                oLeadCustomer.SourceId = lead.SourceId;
                //oLeadCustomer.StartDate = lead.StartDate;
                oLeadCustomer.State = lead.State;
                oLeadCustomer.Telephone = lead.Telephone;
                oLeadCustomer.Webiste = lead.Webiste;
                oLeadCustomer.RowCount = lead.RowCount;
                oLeadCustomer.EndUserCode = lead.EndUserCode;
            }
            catch (Exception)
            {

                throw;
            }
            return oLeadCustomer;
        }

        public static List<LeadCustomerDTO> ConvertToLeadCustomerList(List<LeadCustomerViewEntity> lstLead)
        {
            List<LeadCustomerDTO> lstLeadCustomer = new List<LeadCustomerDTO>();
            LeadCustomerDTO LeadCustomerDTO = null;

            try
            {
                foreach (LeadCustomerViewEntity obj in lstLead)
                {
                    LeadCustomerDTO = new LeadCustomerDTO();
                    LeadCustomerDTO.Address = obj.Address;
                    LeadCustomerDTO.AnnualRevenue = obj.AnnualRevenue;
                    LeadCustomerDTO.Business = obj.Business;
                    LeadCustomerDTO.BusinessPotential = obj.BusinessPotential;
                    LeadCustomerDTO.ChannelDescription = obj.ChannelDescription;
                    LeadCustomerDTO.City = obj.City;
                    LeadCustomerDTO.Company = obj.Company;
                    LeadCustomerDTO.Country = obj.Country;
                    LeadCustomerDTO.CustomerCode = obj.CustomerCode;
                    //LeadCustomerDTO.CustomerGroup = obj.CustomerGroup;
                    LeadCustomerDTO.Description = obj.Description;
                    LeadCustomerDTO.Email = obj.Email;
                    LeadCustomerDTO.Fax = obj.Fax;
                    LeadCustomerDTO.Industry = obj.Industry;
                    LeadCustomerDTO.IndustryDescription = obj.IndustryDescription;
                    LeadCustomerDTO.IsDeleted = obj.IsDeleted;
                    //LeadCustomerDTO.LastCalledDate = obj.LastCalledDate;
                    LeadCustomerDTO.LeadCustomerType = obj.LeadCustomerType;
                    LeadCustomerDTO.LeadSource = obj.LeadSource;
                    LeadCustomerDTO.LeadStage = obj.LeadStage;
                    LeadCustomerDTO.LeadStatus = obj.LeadStatus;
                    LeadCustomerDTO.Mobile = obj.Mobile;
                    LeadCustomerDTO.Name = obj.Name;
                    LeadCustomerDTO.NoOfEmployees = obj.NoOfEmployees;
                    LeadCustomerDTO.NoOfLeadCustomers = obj.NoOfLeadCustomers;
                    LeadCustomerDTO.Originator = obj.Originator;
                    LeadCustomerDTO.PostalCode = obj.PostalCode;
                    LeadCustomerDTO.PreferredContact = obj.PreferredContact;
                    LeadCustomerDTO.PreferredContactDescription = obj.PreferredContactDescription;
                    LeadCustomerDTO.Rating = obj.Rating;
                    LeadCustomerDTO.ReferredBy = obj.ReferredBy;
                    //LeadCustomerDTO.Selection = obj.Selection;
                    //LeadCustomerDTO.ServiceChecklistColor = obj.ServiceChecklistColor;
                    //LeadCustomerDTO.ServiceChecklistTooltip = obj.ServiceChecklistTooltip;
                    LeadCustomerDTO.SourceId = obj.SourceId;
                    //LeadCustomerDTO.StartDate = obj.StartDate;
                    LeadCustomerDTO.State = obj.State;
                    LeadCustomerDTO.Telephone = obj.Telephone;
                    LeadCustomerDTO.Webiste = obj.Webiste;
                    LeadCustomerDTO.TME = obj.TME;
                    LeadCustomerDTO.RepCode = obj.RepCode;
                    LeadCustomerDTO.CustType = obj.CustType;
                    LeadCustomerDTO.RowCount = obj.RowCount;
                    LeadCustomerDTO.Market = obj.MarketName;

                    LeadCustomerDTO.DistributorId = obj.DistributorID;
                    LeadCustomerDTO.DistributorCode = obj.DistributorCode;
                    LeadCustomerDTO.DistributorName = obj.DistributorName;

                    LeadCustomerDTO.LastInvoiceDate = obj.LastInvoiceDate;
                    LeadCustomerDTO.IsCheckList = obj.IsCheckList;
                    LeadCustomerDTO.HasSelect = obj.IsCheckList;
                    LeadCustomerDTO.RouteName = obj.RouteName;
                    LeadCustomerDTO.RepCode = obj.RepCode;
                    LeadCustomerDTO.RepName = obj.RepName;
                    lstLeadCustomer.Add(LeadCustomerDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstLeadCustomer;
        }

        #endregion

        #region - Properties -

        //brLead oBrLead = null;
        //brActivity obrActivity = null;

        #endregion

        #region - Constructor -

        public LeadCustomerAdapter()
        {
            //if (oBrLead == null || obrActivity == null)
            //{
            //    oBrLead = new brLead();
            //    obrActivity = new brActivity();
            //}
        }

        #endregion

        #region - Methods -
        #region - Activity -
        public List<LeadCustomerDTO> GetNotCalledCustomers(ArgsDTO args)
        {
            try
            {
                List<LeadCustomerDTO> customerActivityDtos = new List<LeadCustomerDTO>();

                List<LeadCustomerViewEntity> CustomerActivities = ActivityBR.Instance.GetNotCalledCustomers(ActivityAdapter.ConvertToArgsEntity(args));

                foreach (LeadCustomerViewEntity item in CustomerActivities)
                {
                    customerActivityDtos.Add(ConvertToLeadCustomer(item));
                }

                return customerActivityDtos;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetCalledCustomers(ArgsDTO args)
        {
            try
            {
                List<LeadCustomerDTO> customerActivityDtos = new List<LeadCustomerDTO>();

                List<LeadCustomerViewEntity> CustomerActivities = ActivityBR.Instance.GetCalledCustomers(ActivityAdapter.ConvertToArgsEntity(args));

                foreach (LeadCustomerViewEntity item in CustomerActivities)
                {
                    customerActivityDtos.Add(ConvertToLeadCustomer(item));
                }

                return customerActivityDtos;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region - Lead -

        public List<LeadCustomerDTO> GetCombinedCollection(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetCombinedCollection(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCollectionForCheckList(int CheckListId, ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetLeadCollectionForCheckList(CheckListId, ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetPendingCustomerCollection(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(CustomerBR.Instance.GetPendingCustomerCollection(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }



        public List<LeadCustomerDTO> GetCombinedCollectionbyleadStageName(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetCombinedCollectionbyleadStageName(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCountByState(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetLeadCustomerCountByState(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetCombinedCollectionByState(ArgsDTO args, string sLeadOrCustomer = "")
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetCombinedCollectionByState(ActivityAdapter.ConvertToArgsEntity(args), sLeadOrCustomer));
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCountByRep(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetLeadCustomerCountByRep(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCollectionByRep(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetLeadCustomerCollectionByRep(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> LeadExist(LeadDTO lead, ArgsDTO args)
        {
            List<LeadCustomerDTO> lcExistingLeads = new List<LeadCustomerDTO>();
            try
            {
                List<LeadCustomerViewEntity> lcvExistingLeads = LeadBR.Instance.LeadExist(LeadAdapter.ConvertToLead(lead), args.DefaultDepartmentId);
                foreach (LeadCustomerViewEntity item in lcvExistingLeads)
                {
                    lcExistingLeads.Add(ConvertToLeadCustomer(item));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return lcExistingLeads;
        }
        #endregion

        public List<ColumnSettingDTO> testMethod()
        {
            List<ColumnSettingDTO> test =null;
            try
            {
                test = new List<ColumnSettingDTO>();
            }
            catch (Exception)
            {
                
                throw;
            }
            return test;
        }

        public int GetPendingCustomersCount(ArgsDTO args)
        {
            try
            {
                return LeadBR.Instance.GetPendingCustomersCount(CommonAdapter.ConvertToArgsEntity(args));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetOutletsForLookupInScheduleEntry(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetOutletsForLookupInScheduleEntry(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UnassignedRoutefromRep(int RouteId, string RepCode)
        {
            try
            {
                return LeadBR.Instance.UnassignedRoutefromRep(RouteId, RepCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetAllNotInvoicedUsers(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadCustomerList(LeadBR.Instance.GetAllNotInvoicedUsers(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion
    }
}
