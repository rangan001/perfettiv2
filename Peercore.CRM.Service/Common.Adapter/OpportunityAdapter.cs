﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;
using System.Data;
using System.Collections.ObjectModel;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class OpportunityAdapter
    {

        #region - Converters -

        public static OpportunityEntity ConvertToOpportunity(OpportunityDTO opportunityDto)
        {
            OpportunityEntity Opportunity = OpportunityEntity.CreateObject();
            try
            {
                Opportunity.Amount = opportunityDto.Amount;
                Opportunity.CloseDate = opportunityDto.CloseDate;
                Opportunity.CreatedBy = opportunityDto.CreatedBy;
                Opportunity.CreatedDate = opportunityDto.CreatedDate;
                Opportunity.CustCode = opportunityDto.CustCode;
                Opportunity.EndUserCode = opportunityDto.EndUserCode;
                Opportunity.Description = opportunityDto.Description;
                Opportunity.LastModifiedBy = opportunityDto.LastModifiedBy;
                Opportunity.LastModifiedDate = opportunityDto.LastModifiedDate;
                Opportunity.LeadID = opportunityDto.LeadID;
                Opportunity.Name = opportunityDto.Name;
                //Opportunity.OpportunityCount = opportunityDto.OpportunityCount;
                Opportunity.OpportunityID = opportunityDto.OpportunityID;
                Opportunity.Originator = opportunityDto.Originator;
                Opportunity.PipelineStage = opportunityDto.PipelineStage;
                Opportunity.Probability = opportunityDto.Probability;
                Opportunity.RepGroupID = opportunityDto.RepGroupID;
                Opportunity.Stage = opportunityDto.Stage;
                Opportunity.Units = opportunityDto.Units;
                Opportunity.Tonnes = opportunityDto.Tonnes;

            }
            catch (Exception)
            {

                throw;
            }
            return Opportunity;
        }

        public static OpportunityDTO ConvertToOpportunityDTO(OpportunityEntity opportunity)
        {
            OpportunityDTO opportunityDto = new OpportunityDTO();

            try
            {
                 opportunityDto.Amount = opportunity.Amount;
                 opportunityDto.CloseDate = opportunity.CloseDate;
                 opportunityDto.CreatedBy = opportunity.CreatedBy;
                 opportunityDto.CreatedDate = opportunity.CreatedDate;
                 opportunityDto.CustCode = opportunity.CustCode;
                 opportunityDto.Description = opportunity.Description;
                 opportunityDto.LastModifiedBy = opportunity.LastModifiedBy;
                 opportunityDto.LastModifiedDate = opportunity.LastModifiedDate;
                 opportunityDto.LeadID = opportunity.LeadID;
                 opportunityDto.Name = opportunity.Name;
                 //opportunityDto.OpportunityCount = opportunity.OpportunityCount;
                 opportunityDto.OpportunityID = opportunity.OpportunityID;
                 opportunityDto.Originator = opportunity.Originator;
                 opportunityDto.PipelineStage = opportunity.PipelineStage;
                 opportunityDto.Probability = opportunity.Probability;
                 opportunityDto.RepGroupID = opportunity.RepGroupID;
                 opportunityDto.Stage = opportunity.Stage;
                 opportunityDto.Units = opportunity.Units;
                 opportunityDto.RowCount = opportunity.TotalCount;
                 opportunityDto.Tonnes = opportunity.Tonnes;
            }
            catch (Exception)
            {

                throw;
            }
            return opportunityDto;
        }

        public static List<OpportunityDTO> ConvertToOpportunityDTOList(ObservableCollection<CustomerOpportunityEntity> lstopportunity)
        {
            List<OpportunityDTO> lstopportunityDto = new List<OpportunityDTO>();
            OpportunityDTO opportunityDto = null;

            try
            {
                foreach (CustomerOpportunityEntity opportunity in lstopportunity)
                {
                    opportunityDto = new OpportunityDTO();
                    opportunityDto.Amount = opportunity.Amount;
                    opportunityDto.CloseDate = opportunity.CloseDate;
                    opportunityDto.CreatedBy = opportunity.CreatedBy;
                    opportunityDto.CreatedDate = opportunity.CreatedDate;
                    opportunityDto.CustCode = opportunity.CustCode;
                    opportunityDto.Description = opportunity.Description;
                    opportunityDto.LastModifiedBy = opportunity.LastModifiedBy;
                    opportunityDto.LastModifiedDate = opportunity.LastModifiedDate;
                    opportunityDto.LeadID = opportunity.LeadID;
                    opportunityDto.LeadName = opportunity.LeadName;
                    opportunityDto.Name = opportunity.Name;
                    opportunityDto.OpportunityID = opportunity.OpportunityID;
                    opportunityDto.Originator = opportunity.Originator;
                    opportunityDto.PipelineStage = opportunity.PipelineStage;
                    opportunityDto.Probability = opportunity.Probability;
                    opportunityDto.RepGroupID = opportunity.RepGroupID;
                    opportunityDto.Stage = opportunity.Stage;

                    opportunityDto.UnitString = String.Format("{0:0.00}", opportunity.Units);

                    opportunityDto.Units = opportunity.Units;
                    //opportunityDto.RowCount = opportunity.RowCount;
                    //opportunityDto.LastActivityDate = opportunity.LastActivityDate;
                    lstopportunityDto.Add(opportunityDto);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return lstopportunityDto;
        }

        public static CustomerOpportunitiesDTO ConvertToCustomerOpportunityDTO(CustomerOpportunityEntity opportunity)
        {
            CustomerOpportunitiesDTO lstCustomerOpportunitiesDTO = new CustomerOpportunitiesDTO();

            try
            {
                lstCustomerOpportunitiesDTO.Address = opportunity.Address;
                lstCustomerOpportunitiesDTO.Amount = opportunity.Amount;
                lstCustomerOpportunitiesDTO.Business = opportunity.Business;
                lstCustomerOpportunitiesDTO.City = opportunity.City;
                lstCustomerOpportunitiesDTO.CloseDate = opportunity.CloseDate.ToLocalTime();
                lstCustomerOpportunitiesDTO.CreatedBy = opportunity.CreatedBy;
                lstCustomerOpportunitiesDTO.CreatedDate = opportunity.CreatedDate;
                lstCustomerOpportunitiesDTO.CustCode = opportunity.CustCode;
                lstCustomerOpportunitiesDTO.Description = opportunity.Description;
                lstCustomerOpportunitiesDTO.Industry = opportunity.Industry;
                lstCustomerOpportunitiesDTO.IndustryDescription = opportunity.IndustryDescription;
                //lstCustomerOpportunitiesDTO.LastActivityDate = opportunity.LastActivityDate;
                lstCustomerOpportunitiesDTO.LastModifiedBy = opportunity.LastModifiedBy;
                lstCustomerOpportunitiesDTO.LastModifiedDate = opportunity.LastModifiedDate;
                lstCustomerOpportunitiesDTO.LeadID = opportunity.LeadID;
                lstCustomerOpportunitiesDTO.LeadName = opportunity.LeadName;
                lstCustomerOpportunitiesDTO.LeadStage = opportunity.LeadStage;
                lstCustomerOpportunitiesDTO.Name = opportunity.Name;
                lstCustomerOpportunitiesDTO.OpportunityID = opportunity.OpportunityID;
                lstCustomerOpportunitiesDTO.Originator = opportunity.Originator;
                lstCustomerOpportunitiesDTO.PipelineStage = opportunity.PipelineStage;
                //lstCustomerOpportunitiesDTO.PipelineStageCount = opportunity.PipelineStageCount;
                //lstCustomerOpportunitiesDTO.PipelineStageId = opportunity.PipelineStageId;
                lstCustomerOpportunitiesDTO.PostCode = opportunity.PostCode;
                lstCustomerOpportunitiesDTO.Probability = opportunity.Probability;
                lstCustomerOpportunitiesDTO.RepGroupID = opportunity.RepGroupID;
                lstCustomerOpportunitiesDTO.RepGroupName = opportunity.RepGroupName;
                lstCustomerOpportunitiesDTO.Stage = opportunity.Stage;
                lstCustomerOpportunitiesDTO.State = opportunity.State;
                //lstCustomerOpportunitiesDTO.RowCount = opportunity.RowCount;
                lstCustomerOpportunitiesDTO.Units = opportunity.Units;
                lstCustomerOpportunitiesDTO.RowCount = opportunity.TotalCount;
                lstCustomerOpportunitiesDTO.AmountSum = Math.Round(opportunity.AmountSum, 2);
                lstCustomerOpportunitiesDTO.UnitsSum = Math.Round(opportunity.UnitsSum, 2);
                lstCustomerOpportunitiesDTO.TonnesSum = Math.Round(opportunity.TonnesSum, 2);
                lstCustomerOpportunitiesDTO.Tonnes = opportunity.Tonnes;
                lstCustomerOpportunitiesDTO.EndUserCode = opportunity.EndUserCode;
            }
            catch (Exception)
            {
                
                throw;
            }
            return lstCustomerOpportunitiesDTO;
        }

        public static List<CustomerOpportunitiesDTO> ConvertToCustomerOpportunityDTOList(List<CustomerOpportunityEntity> opportunitylist)
        {
            List<CustomerOpportunitiesDTO> lstCustomerOpportunitiesDTOlist = new List<CustomerOpportunitiesDTO>();
            CustomerOpportunitiesDTO lstCustomerOpportunitiesDTO = null;

            try
            {
                foreach (CustomerOpportunityEntity opportunity in opportunitylist)
                {

                    lstCustomerOpportunitiesDTO.Address = opportunity.Address;
                    lstCustomerOpportunitiesDTO.Amount = opportunity.Amount;
                    lstCustomerOpportunitiesDTO.Business = opportunity.Business;
                    lstCustomerOpportunitiesDTO.City = opportunity.City;
                    lstCustomerOpportunitiesDTO.CloseDate = opportunity.CloseDate;
                    lstCustomerOpportunitiesDTO.CreatedBy = opportunity.CreatedBy;
                    lstCustomerOpportunitiesDTO.CreatedDate = opportunity.CreatedDate;
                    lstCustomerOpportunitiesDTO.CustCode = opportunity.CustCode;
                    lstCustomerOpportunitiesDTO.Description = opportunity.Description;
                    lstCustomerOpportunitiesDTO.Industry = opportunity.Industry;
                    lstCustomerOpportunitiesDTO.IndustryDescription = opportunity.IndustryDescription;
                    //lstCustomerOpportunitiesDTO.LastActivityDate = opportunity.LastActivityDate;
                    lstCustomerOpportunitiesDTO.LastModifiedBy = opportunity.LastModifiedBy;
                    lstCustomerOpportunitiesDTO.LastModifiedDate = opportunity.LastModifiedDate;
                    lstCustomerOpportunitiesDTO.LeadID = opportunity.LeadID;
                    lstCustomerOpportunitiesDTO.LeadName = opportunity.LeadName;
                    lstCustomerOpportunitiesDTO.LeadStage = opportunity.LeadName;
                    lstCustomerOpportunitiesDTO.Name = opportunity.Name;
                    lstCustomerOpportunitiesDTO.OpportunityID = opportunity.OpportunityID;
                    lstCustomerOpportunitiesDTO.Originator = opportunity.Originator;
                    lstCustomerOpportunitiesDTO.PipelineStage = opportunity.PipelineStage;
                    //lstCustomerOpportunitiesDTO.PipelineStageCount = opportunity.PipelineStageCount;
                    //lstCustomerOpportunitiesDTO.PipelineStageId = opportunity.PipelineStageId;
                    lstCustomerOpportunitiesDTO.PostCode = opportunity.PostCode;
                    lstCustomerOpportunitiesDTO.Probability = opportunity.Probability;
                    lstCustomerOpportunitiesDTO.RepGroupID = opportunity.RepGroupID;
                    lstCustomerOpportunitiesDTO.RepGroupName = opportunity.RepGroupName;
                    lstCustomerOpportunitiesDTO.Stage = opportunity.Stage;
                    lstCustomerOpportunitiesDTO.State = opportunity.State;
                    lstCustomerOpportunitiesDTO.RowCount = opportunity.RowCount;
                    lstCustomerOpportunitiesDTO.Units = opportunity.Units;

                    lstCustomerOpportunitiesDTOlist.Add(lstCustomerOpportunitiesDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstCustomerOpportunitiesDTOlist;
        }

        public static ArgsEntity ConvertToArgsEntity(ArgsDTO argsDto)
        {
            ArgsEntity args = new ArgsEntity();
            try
            {
                args = new ArgsEntity()
                {
                    ActiveInactiveChecked = argsDto.ActiveInactiveChecked,
                    ActivityStatus = argsDto.ActivityStatus,
                    AdditionalParams = argsDto.AdditionalParams,
                    AddressType = argsDto.AddressType,
                    ChildOriginators = argsDto.ChildOriginators,
                    CustomerCode = argsDto.CustomerCode,
                    DefaultDepartmentId = argsDto.DefaultDepartmentId,
                    DtEndDate = argsDto.DtEndDate,
                    DtStartDate = argsDto.DtStartDate,
                    EnduserCode = argsDto.EnduserCode,
                    Floor = argsDto.Floor,
                    IsIncludeContact = argsDto.IsIncludeContact,
                    IsIncludeCustomer = argsDto.IsIncludeCustomer,
                    IsIncludeLead = argsDto.IsIncludeLead,
                    IsOpportunityAutoGenerated = argsDto.IsOpportunityAutoGenerated,
                    LeadId = argsDto.LeadId,
                    LeadStage = argsDto.LeadStage,
                    LeadStageId = argsDto.LeadStageId,
                    LeadStageName = argsDto.LeadStageName,
                    ManagerMode = argsDto.ManagerMode,
                    OrderBy = argsDto.OrderBy,
                    Originator = argsDto.Originator,
                    OriginatorId = argsDto.OriginatorId,
                    RepCode = argsDto.RepCode,
                    RepType = argsDto.RepType,
                    ReqSentIsChecked = argsDto.ReqSentIsChecked,
                    ROriginator = argsDto.ROriginator,
                    RowCount = argsDto.RowCount,
                    Sector = argsDto.Sector,
                    SEndDate = argsDto.SEndDate,
                    SFilter = argsDto.SFilter,
                    SMonth = argsDto.SMonth,
                    SNoofMonths = argsDto.SNoofMonths,
                    SSource = argsDto.SSource,
                    SStartDate = argsDto.SStartDate,
                    StartIndex = argsDto.StartIndex,
                    Status = argsDto.Status,
                    SToday = argsDto.SToday,
                    SYear = argsDto.SYear
                };
            }
            catch (Exception)
            {

                throw;
            }
            return args;
        }

        public static ClientOpportunityDTO ConvertToCustomerOpportunityDTO(ClientOpportunityEntity opportunity)
        {
            ClientOpportunityDTO lstCustomerOpportunitiesDTO = new ClientOpportunityDTO();

            try
            {
                lstCustomerOpportunitiesDTO.Tonnes = opportunity.Tonnes;
                lstCustomerOpportunitiesDTO.Units = opportunity.Units;
                lstCustomerOpportunitiesDTO.Count = opportunity.Count;
                lstCustomerOpportunitiesDTO.Value = opportunity.Value;
                lstCustomerOpportunitiesDTO.OpportunityOwner = opportunity.OpportunityOwner;
            }
            catch (Exception)
            {

                throw;
            }
            return lstCustomerOpportunitiesDTO;
        }
        #endregion

        #region - Properties -

        //private brOpportunity oBrOpportunity = null;

        #endregion

        #region - Constructor -

        public OpportunityAdapter()
        {
            //if (oBrOpportunity == null)
            //    oBrOpportunity = new brOpportunity();
        }

        #endregion

        public bool InsertOpportunity(OpportunityDTO opportunity, out int opportunityID)
        {           
            try
            {
                return OpportunityBR.Instance.InsertOpportunity(ConvertToOpportunity(opportunity), out opportunityID);
            }
            catch (Exception)
            {

                throw;
            }            
        }

        public bool UpdateOpportunity(OpportunityDTO opportunity)
        {
            try
            {
                return OpportunityBR.Instance.UpdateOpportunity(ConvertToOpportunity(opportunity));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OpportunityDTO> GetOpportunities(ArgsDTO args, int contactID)
        {
            List<OpportunityDTO> OpportunityDTOList = new List<OpportunityDTO>();

            try
            {
                List<OpportunityEntity> OpportunityEntityList = OpportunityBR.Instance.GetOpportunities(ConvertToArgsEntity(args), contactID);

                foreach (OpportunityEntity opportunityEntity in OpportunityEntityList)
                {
                    OpportunityDTOList.Add(ConvertToOpportunityDTO(opportunityEntity));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return OpportunityDTOList;
        }

        public List<OpportunityDTO> GetOpportunitiesForCustomer(string customerCode, ArgsDTO args)
        {
            List<OpportunityDTO> OpportunityDTOList = new List<OpportunityDTO>();

            try
            {
                List<OpportunityEntity> OpportunityEntityList = OpportunityBR.Instance.GetOpportunitiesForCustomer(customerCode ,ConvertToArgsEntity(args));

                foreach (OpportunityEntity opportunityEntity in OpportunityEntityList)
                {
                    OpportunityDTOList.Add(ConvertToOpportunityDTO(opportunityEntity));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return OpportunityDTOList;
        }

        public bool DeleteOpportunity(int opportunityId)
        {
            try
            {
                return OpportunityBR.Instance.Delete(opportunityId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetOpportunityCountForHome(int pipelineStageID, ArgsDTO args){
            try
            {
                return OpportunityBR.Instance.GetOpportunityCountForHome(pipelineStageID,ActivityAdapter.ConvertToArgsEntity(args));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetAllOpportunities(int pipelineStageID, ArgsDTO args)
        {
            try
            {
                List<CustomerOpportunitiesDTO> leadCustomerOppDaoList = new List<CustomerOpportunitiesDTO>();
                List<CustomerOpportunityEntity> customerOpportunityList = OpportunityBR.Instance.GetAllOpportunities(pipelineStageID, ActivityAdapter.ConvertToArgsEntity(args));
                foreach(CustomerOpportunityEntity item in customerOpportunityList){
                    leadCustomerOppDaoList.Add(ConvertToCustomerOpportunityDTO(item));
                }                
                return leadCustomerOppDaoList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ClientOpportunityDTO> GetAllClientOpportunity(int pipelineStageID, ArgsDTO args)
        {
            try
            {
                List<ClientOpportunityDTO> clientOppDaoList = new List<ClientOpportunityDTO>();
                List<ClientOpportunityEntity> clientOpportunityList = OpportunityBR.Instance.GetAllClientOpportunity(pipelineStageID, ActivityAdapter.ConvertToArgsEntity(args));
                foreach (ClientOpportunityEntity item in clientOpportunityList)
                {
                    clientOppDaoList.Add(ConvertToCustomerOpportunityDTO(item));
                }
                return clientOppDaoList;
            }
            catch
            {
                throw;
            }
        }

        public List<CatalogDTO> GetOppProducts(int opportunityId, ArgsDTO args)
        {
            try
            {
                List<CatalogDTO> catalogDaoList = new List<CatalogDTO>();
                List<CatalogEntity> catalogList = OpportunityBR.Instance.GetOppProducts(opportunityId, ActivityAdapter.ConvertToArgsEntity(args));
                foreach (CatalogEntity item in catalogList)
                {
                    catalogDaoList.Add(CustomerAdapter.ConvertToCatalogDTO(item));
                }
                return catalogDaoList;
            }
            catch
            {
                throw;
            }
        }

        public CustomerOpportunitiesDTO GetOpportunity(int opportunityId)
        {
            try
            {
                return ConvertToCustomerOpportunityDTO(OpportunityBR.Instance.GetOpportunity(opportunityId));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool SaveOpportunity(OpportunityDTO opportunity, List<CatalogDTO> lstProducts,out int opportunityId )
        {
            bool save = false;

            try
            {
                List<CatalogEntity> catalogs = new List<CatalogEntity>();
                foreach (CatalogDTO item in lstProducts)
                {
                    catalogs.Add(CustomerAdapter.ConvertToCatalog(item));
                }

                save = OpportunityBR.Instance.SaveOpportunity(ConvertToOpportunity(opportunity), catalogs, out opportunityId);
            }
            catch (Exception)
            {

                throw;
            }
            return save;
        }

        public List<CustomerOpportunitiesDTO> GetOpportunitiesForEndUser(ArgsDTO args)
        {
            List<CustomerOpportunitiesDTO> OpportunityDTOList = new List<CustomerOpportunitiesDTO>();

            try
            {
                List<CustomerOpportunityEntity> OpportunityEntityList = OpportunityBR.Instance.GetOpportunitiesForEndUser(ConvertToArgsEntity(args));

                foreach (CustomerOpportunityEntity opportunityEntity in OpportunityEntityList)
                {
                    OpportunityDTOList.Add(ConvertToCustomerOpportunityDTO(opportunityEntity));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return OpportunityDTOList;
        }

        /*
        #region - Methods -

        

        public List<OpportunityDTO> GetAllOpportunities(int iPipelineStageID, string sOriginator, ArgsDTO args, DateTime? dtmCloseDate = null)
        {
            try
            {
                return ConvertToOpportunityDTOList(oBrOpportunity.GetAllOpportunities(iPipelineStageID,
                    sOriginator,
                    dtmCloseDate,
                    args.StartIndex,
                    args.RowCount,
                    args.OrderBy,
                    args.ChiildOriginators,
                    args.DefaultDepartmentId,
                    args.AdditionalParams));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Save(OpportunityDTO opportunity, List<CatalogDTO> lstProducts)
        {
            int save = 0;
            
            try
            {
                List<Catalog> catalogs = new List<Catalog>();
                foreach (CatalogDTO item in lstProducts)
                {
                    catalogs.Add(CustomerAdapter.ConvertToCatalog(item));
                }

                save = oBrOpportunity.Save(ConvertToOpportunity(opportunity), catalogs);
            }
            catch (Exception)
            {
                
                throw;
            }
            return save;
        }

        public CustomerOpportunitiesDTO GetOpportunity(int opportunityId)
        {
            brOpportunity oBrOpporunity = new brOpportunity();
            try
            {
                return ConvertToCustomerOpportunityDTO(oBrOpportunity.GetOpportunity(opportunityId));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CatalogDTO> GetProducts(int iOpportunityID)
        {
            try
            {
                List<CatalogDTO> catalogDaoList = new List<CatalogDTO>();
                List<Catalog> catalogList = oBrOpportunity.GetProducts(iOpportunityID);
                foreach (Catalog item in catalogList)
                {
                    catalogDaoList.Add( CustomerAdapter.ConvertToCatalogDTO( item));
                }
                return catalogDaoList;
            }
            catch
            {
                throw;
            }
        }

        public List<CatalogDTO> GetAllProducts(string sCatlogCode)
        {
            try
            {
                List<Catalog> catalogList = oBrOpportunity.GetAllProducts(sCatlogCode);
                List<CatalogDTO> catalogDaoList = new List<CatalogDTO>();
                foreach (Catalog item in catalogList)
                {
                    catalogDaoList.Add(CustomerAdapter.ConvertToCatalogDTO(item));
                }
                return catalogDaoList;
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetOpportunitiesForCustomer(string customerCode, ArgsDTO args)
        {
            List<CustomerOpportunitiesDTO> lstCustomerOpportunitiesDTO = new List<CustomerOpportunitiesDTO>();
            try
            {
                List<CustomerOpportunities> lstCustomerOpportunities = oBrOpportunity.GetOpportunitiesForCustomer(customerCode, args.StartIndex, args.RowCount, args.OrderBy, args.AdditionalParams);
                foreach (CustomerOpportunities CustomerOpportunities in lstCustomerOpportunities)
                {
                    lstCustomerOpportunitiesDTO.Add(ConvertToCustomerOpportunityDTO(CustomerOpportunities));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstCustomerOpportunitiesDTO;
        }

        public bool DeleteOpportunity(int opportunityId)
        {
            try
            {
                return oBrOpportunity.DeleteOpportunity(opportunityId) > 0 ? true : false;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetOpportunitiesForEndUser(string customerCode, string endUserCode)
        {
            try
            {
                List<CustomerOpportunitiesDTO> lstCustomerOpportunitiesDTO = new List<CustomerOpportunitiesDTO>();
                List<CustomerOpportunities> lstCustomerOpportunities = oBrOpportunity.GetOpportunitiesForEndUser(customerCode, endUserCode);
                foreach (CustomerOpportunities CustomerOpportunities in lstCustomerOpportunities)
                {
                    lstCustomerOpportunitiesDTO.Add(ConvertToCustomerOpportunityDTO(CustomerOpportunities));
                }

                return lstCustomerOpportunitiesDTO;
            }
            catch
            {
                throw;
            }
        }




        /*
        public List<OpportunityDTO> GetOpportunitiesForPipeGraph(ArgsDTO args, int pipeLineId, bool pipeFlag)
        {
            ObservableCollection<CustomerOpportunities> lstCustomerOpportunities = new ObservableCollection<CustomerOpportunities>();
            List<OpportunityDTO> lstCustomerOpportunitiesDTO = new List<OpportunityDTO>();

            try
            {
                if (pipeFlag == true)
                {
                    lstCustomerOpportunities = oBrOpportunity.GetCustomerOpportunities(args.Originator, pipeLineId, args.DefaultDepartmentId,
                            args.StartIndex, args.RowCount, args.ChiildOriginators, args.AdditionalParams, args.OrderBy);
                }
                else
                {
                    lstCustomerOpportunities = oBrOpportunity.GetCustomerOpportunities(args.Originator, args.DefaultDepartmentId,
                            args.StartIndex, args.RowCount, args.ChiildOriginators, args.AdditionalParams,args.OrderBy);
                }
                

                if (lstCustomerOpportunities.Count > 0)
                {
                    lstCustomerOpportunitiesDTO = ConvertToOpportunityDTOList(lstCustomerOpportunities);
                    //foreach (CustomerOpportunities obj in lstCustomerOpportunities)
                    //{
                    //    lstCustomerOpportunitiesDTO.Add(ConvertToCustomerOpportunityDTO(obj));
                    //}
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return lstCustomerOpportunitiesDTO;
        }
        

        public int GetOpportunityForWindow(ArgsDTO args, DateTime? dtmCloseDate = null)
        {
            try
            {
                return oBrOpportunity.GetOpportunityForWindow(
                    args.Originator,
                    dtmCloseDate,
                    args.ChiildOriginators,
                    args.DefaultDepartmentId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetAllOpportunitiesCount(string sOriginator, string defaultDepartmentID)
        {
            List<CustomerOpportunitiesDTO> lstCustomerOpportunitiesDTO = new List<CustomerOpportunitiesDTO>();
            try
            {
                List<CustomerOpportunities> lstCustomerOpportunities = oBrOpportunity.GetAllOpportunitiesCount(sOriginator,defaultDepartmentID).ToList();
                foreach (CustomerOpportunities CustomerOpportunities in lstCustomerOpportunities)
                {
                    lstCustomerOpportunitiesDTO.Add(ConvertToCustomerOpportunityDTO(CustomerOpportunities));
                }
            }
            catch (Exception)
            {


                throw;
            }
            return lstCustomerOpportunitiesDTO;
        }

        public List<CatalogDTO> GetOppProducts(int oppId)
        {
            try
            {
                brOpportunity oOpportunity = new brOpportunity();
                List<Catalog> opportunityCatlogs = oOpportunity.GetOppProducts(oppId);
                List<CatalogDTO> opportunityCatlogDtos = new List<CatalogDTO>();

                foreach (Catalog item in opportunityCatlogs)
                {
                    opportunityCatlogDtos.Add(CustomerAdapter.ConvertToCatalogDTO(item));
                }

                return opportunityCatlogDtos;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
              


        //    GetAllOpportunitiesCount
        //    GetOpportunitiesForCustomerCount

        public List<CustomerOpportunitiesDTO> GetOpportunitiesForCustomerCount(string orginator, string defDepId)
        {
            brOpportunity oBrOpportunity = new brOpportunity();

            try
            {
                List<CustomerOpportunities> allOpportunities = oBrOpportunity.GetOpportunitiesForCustomerCount(orginator, defDepId).ToList();
                List<CustomerOpportunitiesDTO> allOpportunitiesDto = new List<CustomerOpportunitiesDTO>();

                foreach (CustomerOpportunities item in allOpportunities)
                {
                    allOpportunitiesDto.Add(ConvertToCustomerOpportunityDTO(item));
                }

                return allOpportunitiesDto;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public double GetOpportunitySum(ArgsDTO args)
        {
            brOpportunity oBrOpportunity = new brOpportunity();
            try
            {
                return oBrOpportunity.GetOpportunitySum(args.ChiildOriginators, args.DefaultDepartmentId, args.AdditionalParams);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        

        

        //public List<OpportunityDTO> GetCustomerOpportunities(ArgsDTO args, int iPipelineStageId)
        //{
        //    brOpportunity oBrOpporunity = new brOpportunity();

        //    try
        //    {
        //        //List<Opportunity> opportunities = oBrOpporunity.GetOpportunities(
        //        //List<OpportunityDTO> opportunitiesDTO = new List<OpportunityDTO>();
        //        //foreach (Opportunity item in opportunities)
        //        //{
        //        //    opportunitiesDTO.Add(ConvertToOpportunityDTO(item));
        //        //}
        //        //return opportunitiesDTO;
        //    }
        //    catch (Exception)
        //    {
                
        //        throw;
        //    } 
        //}
        * /

        #endregion
        */
    }
}
