﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class PaymentSettlementAdapter
    {
        #region converters

        public static PaymentSettlementDTO ConvertToPaymentSettlementDTO(PaymentSettlementEntity paymentSettlementEntity)
        {
            PaymentSettlementDTO paymentSettlementDTO = new PaymentSettlementDTO();

            try
            {
                if (paymentSettlementEntity != null)
                {
                    paymentSettlementDTO.PaymentSettlementId = paymentSettlementEntity.PaymentSettlementId;
                    paymentSettlementDTO.CustCode = paymentSettlementEntity.CustCode;
                    paymentSettlementDTO.ChequeNumber = paymentSettlementEntity.ChequeNumber;
                    paymentSettlementDTO.OutletBank = paymentSettlementEntity.OutletBank;
                    paymentSettlementDTO.PaymentType = paymentSettlementEntity.PaymentType;
                    paymentSettlementDTO.Amount = paymentSettlementEntity.Amount;
                    paymentSettlementDTO.Status = paymentSettlementEntity.Status;
                    paymentSettlementDTO.IvceNo = paymentSettlementEntity.IvceNo;
                    paymentSettlementDTO.DistributorAccountId = paymentSettlementEntity.DistributorAccountId;
                    paymentSettlementDTO.ChequeDate = paymentSettlementEntity.ChequeDate;

                    paymentSettlementDTO.CreatedBy = paymentSettlementEntity.CreatedBy;
                    paymentSettlementDTO.CreatedDate = paymentSettlementEntity.CreatedDate.Value;
                    paymentSettlementDTO.LastModifiedBy = paymentSettlementEntity.LastModifiedBy;
                    paymentSettlementDTO.LastModifiedDate = paymentSettlementEntity.LastModifiedDate.Value;

                    paymentSettlementDTO.RowCount = paymentSettlementEntity.TotalCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return paymentSettlementDTO;
        }

        public static PaymentSettlementEntity ConvertToPaymentSettlementEntity(PaymentSettlementDTO paymentSettlementDTO)
        {
            PaymentSettlementEntity paymentSettlementEntity = new PaymentSettlementEntity();

            try
            {
                if (paymentSettlementDTO != null)
                {
                    paymentSettlementEntity.PaymentSettlementId = paymentSettlementDTO.PaymentSettlementId;
                    paymentSettlementEntity.CustCode = paymentSettlementDTO.CustCode;
                    paymentSettlementEntity.ChequeNumber = paymentSettlementDTO.ChequeNumber;
                    paymentSettlementEntity.OutletBank = paymentSettlementDTO.OutletBank;
                    paymentSettlementEntity.PaymentType = paymentSettlementDTO.PaymentType;
                    paymentSettlementEntity.Amount = paymentSettlementDTO.Amount;
                    paymentSettlementEntity.Status = paymentSettlementDTO.Status;
                    paymentSettlementEntity.IvceNo = paymentSettlementDTO.IvceNo;
                    paymentSettlementEntity.DistributorAccountId = paymentSettlementDTO.DistributorAccountId;
                    paymentSettlementEntity.ChequeDate = paymentSettlementDTO.ChequeDate;

                    paymentSettlementEntity.CreatedBy = paymentSettlementDTO.CreatedBy;
                    paymentSettlementEntity.CreatedDate = paymentSettlementDTO.CreatedDate;
                    paymentSettlementEntity.LastModifiedBy = paymentSettlementDTO.LastModifiedBy;
                    paymentSettlementEntity.LastModifiedDate = paymentSettlementDTO.LastModifiedDate;

                    paymentSettlementEntity.TotalCount = paymentSettlementDTO.RowCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return paymentSettlementEntity;
        }

        public static List<PaymentSettlementDTO> ConvertToPaymentSettlementDTOList(List<PaymentSettlementEntity> lstPaymentSettlementEntity)
        {
            PaymentSettlementDTO paymentSettlementDTO = null;
            List<PaymentSettlementDTO> lstPaymentSettlementDTO = new List<PaymentSettlementDTO>();
            try
            {
                foreach (PaymentSettlementEntity paymentSettlementEntity in lstPaymentSettlementEntity)
                {
                    paymentSettlementDTO = new PaymentSettlementDTO();

                    paymentSettlementDTO = ConvertToPaymentSettlementDTO(paymentSettlementEntity);

                    lstPaymentSettlementDTO.Add(paymentSettlementDTO);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstPaymentSettlementDTO;
        }

        public static List<PaymentSettlementEntity> ConvertToPaymentSettlementEntityList(List<PaymentSettlementDTO> lstPaymentSettlementDTO)
        {
            PaymentSettlementEntity paymentSettlementEntity = null;
            List<PaymentSettlementEntity> lstPaymentSettlementEntity = new List<PaymentSettlementEntity>();
            try
            {
                foreach (PaymentSettlementDTO paymentSettlementDTO in lstPaymentSettlementDTO)
                {
                    paymentSettlementEntity = new PaymentSettlementEntity();

                    paymentSettlementEntity = ConvertToPaymentSettlementEntity(paymentSettlementDTO);

                    lstPaymentSettlementEntity.Add(paymentSettlementEntity);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstPaymentSettlementEntity;
        }

        #endregion

        public List<PaymentSettlementDTO> GetCheques(string createdBy, DateTime createdDate, DateTime toDate, int distributerAccountId, string status, ArgsDTO args)
        {
            try
            {

                return ConvertToPaymentSettlementDTOList(PaymentSettlementBR.Instance.GetChequePaymentSettlements(createdBy, createdDate, toDate, distributerAccountId, status, ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SavePaymentSettlements(List<PaymentSettlementDTO> paymentSettlementsList)
        {
            try
            {
                return PaymentSettlementBR.Instance.SavePaymentSettlements(ConvertToPaymentSettlementEntityList(paymentSettlementsList));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<PaymentSettlementDTO> GetDatedCheques(string createdBy, DateTime createdDate, DateTime toDate, ArgsDTO args)
        {
            try
            {

                return ConvertToPaymentSettlementDTOList(PaymentSettlementBR.Instance.GetDatedChequePaymentSettlements(createdBy, createdDate, toDate, ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
