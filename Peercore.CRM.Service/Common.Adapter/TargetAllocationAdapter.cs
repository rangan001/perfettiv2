﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class TargetAllocationAdapter
    {
        #region Converters        

        #endregion

        #region Methods

        public bool SaveTargets(TargetAllocationDTO targetAllocationDTO)
        {
            bool isSuccess = false;

            if (targetAllocationDTO.DistributorTargetList != null && targetAllocationDTO.DistributorTargetList.Count > 0)
                isSuccess = DistributorBR.Instance.SaveDistributorTargets(DistributorAdapter.ConvertToDistributorEntityList(targetAllocationDTO.DistributorTargetList));
            if (targetAllocationDTO.MarketTargetList != null && targetAllocationDTO.MarketTargetList.Count > 0)
                isSuccess = MarketBR.Instance.SaveMarketTargets(CommonAdapter.ConvertToMarketDTOList(targetAllocationDTO.MarketTargetList));
            if (targetAllocationDTO.RouteTargetList != null && targetAllocationDTO.RouteTargetList.Count > 0)
                isSuccess = RouteMasterBR.Instance.SaveRouteTargets(CallCycleAdapter.ConvertToRouteMasterEntityList(targetAllocationDTO.RouteTargetList));

            return isSuccess;
        }

        public List<CustomerDTO> GetCustomerTargetsByRouteId(int routeId, ArgsDTO args)
        {
            try
            {
                List<CustomerDTO> custometDTOList = new List<CustomerDTO>();
                foreach (CustomerEntity item in CustomerBR.Instance.GetCustomerTargetsByRouteId(routeId, ActivityAdapter.ConvertToArgsEntity(args)))
                {
                    custometDTOList.Add(CustomerAdapter.ConvertToCustomerDTO(item));
                }
                return custometDTOList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool SaveCustomerTargets(List<CustomerDTO> customerTargetsDTOList)
        {
            bool isSuccess = false;

            if (customerTargetsDTOList.Count > 0)
            {
                List<CustomerEntity> customerEntityList = new List<CustomerEntity>();
                foreach (CustomerDTO item in customerTargetsDTOList)
                    customerEntityList.Add(CustomerAdapter.ConvertToCustomer(item));

                isSuccess = CustomerBR.Instance.SaveCustomerTargets(customerEntityList);
            }

            return isSuccess;
        }

        #endregion
    }
}
