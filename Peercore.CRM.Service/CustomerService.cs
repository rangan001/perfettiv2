﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;
using System.Data;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ICustomer
    {

        CustomerAdapter customerAdapter = null;

        #region - Customer -
        public bool SaveCustomerAddress(LeadAddressDTO customerAddressDto)
        {
            customerAdapter = new CustomerAdapter();

            try
            {
                return customerAdapter.SaveCustomerAddress(customerAddressDto);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public CustomerDTO GetCustomer(string customerCode)
        {
            customerAdapter = new CustomerAdapter();

            try
            {
                return customerAdapter.GetCustomer(customerCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public CustomerDTO GetCustomerPending(string customerCode)
        {
            customerAdapter = new CustomerAdapter();

            try
            {
                return customerAdapter.GetCustomerPending(customerCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadAddressDTO> GetCustomerAddresses(string customerCode, ArgsDTO args)
        {
            customerAdapter = new CustomerAdapter();

            try
            {
                return customerAdapter.GetCustomerAddresses(customerCode, args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<RepCustomerDTO> GetNewCustomers(ArgsDTO args)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetNewCustomers(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public LeadCustomerDTO GetCustomerView(ArgsDTO args)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetCustomerView(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public LeadAddressDTO GetCustomerTableAddresses(string custCode)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetCustomerTableAddresses(custCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SalesOrderDTO> GetOutstandingDeliveries(string custCode)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetOutstandingDeliveries(custCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int GetOutstandingDeliveriesCount(string custCode)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetOutstandingDeliveriesCount(custCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DateTime GetLastDelivery(string custCode)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetLastDelivery(custCode);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region - ImportMail -
        public List<EmailDTO> GetCustomerEmails(string custCode, ArgsDTO args, string directoryPath, string sessionId)
        {
            EmailAdapter emailAdapter = new EmailAdapter();

            try
            {
                return emailAdapter.GetCustomerEmails(custCode, args, directoryPath, sessionId);
                
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region - Contact Person Image -
        public ContactPersonImageDTO GetCustomerContactPersonImage(int contactPersonId, string custCode, string directoryPath, string sessionId)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetContactPersonImage(contactPersonId, custCode, directoryPath, sessionId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Promotions

    // TODO coplete this function
        public CustomerDTO GetCustomerPromotions(string customerCode)
        {
            customerAdapter = new CustomerAdapter();

            try
            {
                return customerAdapter.GetCustomer(customerCode);
            }
            catch (Exception)
            {

                throw;
            }
        }


        #endregion

        #region - Targets - 

        public List<TargetInfoDTO> GetCustomerMonthleyTargetsByDistributor(string username) {
            customerAdapter = new CustomerAdapter();

            try
            {
                return customerAdapter.GetCustomerMonthleyTargetsByDistributor(username);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerDTO> GetCustomerTargetsByRouteId(int routeId, ArgsDTO args)
        {
            TargetAllocationAdapter targetAllocationAdapter = new TargetAllocationAdapter();
            try
            {
                return targetAllocationAdapter.GetCustomerTargetsByRouteId(routeId, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveCustomerTargets(List<CustomerDTO> customerTargetsDTOList)
        {
            TargetAllocationAdapter targetAllocationAdapter = new TargetAllocationAdapter();

            try
            {
                return targetAllocationAdapter.SaveCustomerTargets(customerTargetsDTOList);
            }
            catch
            {
                throw;
            }
        }

        #endregion


        /*
        public DebtorsBarometerDTO GetDebtorsDetails(string customerCode)
        {
            SalesAdapter salesAdapter = new SalesAdapter();
            try
            {
                return salesAdapter.GetDebtorsDetails(customerCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public SalesGraphDataDTO GetSalesDataForCompleteMonths(string customerCode)
        {
            SalesAdapter salesAdapter = new SalesAdapter();
            try
            {
                return salesAdapter.GetSalesDataForCompleteMonths(customerCode);
            }
            catch (Exception)
            {
                //throw;
                return null;
            }
        }

        public List<CatalogDTO> GetRecentProducts(string customerCode)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetRecentProducts(customerCode);
            }
            catch (Exception)
            {

                throw;
            }
        }*/

        public bool SaveCustomer(CustomerDTO customerDto)
        {
            customerAdapter = new CustomerAdapter();

            try
            {
                return customerAdapter.SaveCustomer(customerDto);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<KeyValuePair<int, string>> GetAllOutletTypes()
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetAllOutletTypes();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<KeyValuePair<string, string>> GetAllTowns()
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.GetAllTowns();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /*
        public bool IsWasteCustomer(string custCode)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.IsWasteCustomer(custCode);
            }
            catch (Exception)
            {

                throw;
            }
        }
        */

        //public bool UploadExcelfile(string FilePath, string Extension, string UserName, string ASEOriginator, string DistOriginator, string RepOriginator)
        //{
        //    customerAdapter = new CustomerAdapter();
        //    try
        //    {
        //        return customerAdapter.UploadExcelfile(FilePath, Extension, UserName, ASEOriginator, DistOriginator, RepOriginator);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        
        public bool UploadExcelfile(string FilePath, string Extension, string UserName, string TerritoryId)
        {
            customerAdapter = new CustomerAdapter();
            try
            {
                return customerAdapter.UploadExcelfile(FilePath, Extension, UserName, TerritoryId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteCustomerByCustomerCode(CustomerDTO delCustomer)
        {
            CustomerAdapter custAdapter = new CustomerAdapter();
            try
            {
                return custAdapter.DeleteCustomerByCustomerCode(delCustomer);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public DataTable ReviewExcelfile(string FilePath, string Extension, string UserName)
        //{
        //    customerAdapter = new CustomerAdapter();
        //    try
        //    {
        //        return customerAdapter.ReviewExcelfile(FilePath, Extension, UserName);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        #region "Perfetti 2nd Phase"

        public List<CustomerModel> GetAllPendingCustomers(ArgsModel args, string IsReject, string Originator, string Asm, string Territory)
        {
            try
            {
                return CustomerBR.Instance.GetAllPendingCustomers(args, IsReject, Originator, Asm, Territory);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerModel> GetAllPendingCustomersForSalesConfirm(ArgsModel args, string IsReject, string Originator, string Asm, string Territory)
        {
            try
            {
                return CustomerBR.Instance.GetAllPendingCustomersForSalesConfirm(args, IsReject, Originator, Asm, Territory);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerModel> GetAllAllCustomerByRouteMasterId(ArgsModel args, string RouteMasterId)
        {
            try
            {
                return CustomerBR.Instance.GetAllAllCustomerByRouteMasterId(args, RouteMasterId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteBulkCust(string userType, string userName, string custList)
        {
            try
            {
                return CustomerBR.Instance.DeleteBulkCust(userType, userName, custList);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool AcceptCustomer(string Originator, string CustCode)
        {
            try
            {
                return CustomerBR.Instance.AcceptCustomer(Originator, CustCode);
            }
            catch (Exception)
            {
                throw;
            }
        } 
        
        public bool AcceptSalesCustomer(string Originator, string CustCode)
        {
            try
            {
                return CustomerBR.Instance.AcceptSalesCustomer(Originator, CustCode);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool RejectCustomer(string Originator, string CustCode)
        {
            try
            {
                return CustomerBR.Instance.RejectCustomer(Originator, CustCode);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool InsertCustomerCategory(CustomerCategory custCategory)
        {
            try
            {
                return CustomerBR.Instance.InsertCustomerCategory(custCategory);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteCustomerCategory(CustomerCategory custCategory)
        {
            try
            {
                return CustomerBR.Instance.DeleteCustomerCategory(custCategory);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion



    }
}
