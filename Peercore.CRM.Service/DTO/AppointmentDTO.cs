﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class AppointmentDTO : BaseDTO
    {
        private int appointmentID;
        public int AppointmentID
        {
            get { return appointmentID; }
            set { appointmentID = value; IsDirty = true; }
        }

        private string subject;
        public string Subject
        {
            get { return subject; }
            set { subject = value; IsDirty = true; }
        }

        private string body;
        public string Body
        {
            get { return body; }
            set { body = value; IsDirty = true; }
        }

        private DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; IsDirty = true; }
        }

        private DateTime endTime;
        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; IsDirty = true; }
        }

        private string category;
        public string Category
        {
            get { return category; }
            set { category = value; IsDirty = true; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; IsDirty = true; IsDirty = true; }
        }

        private string leadName;
        public string LeadName
        {
            get { return leadName; }
            set { leadName = value; IsDirty = true; }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name == value)
                    return;

                name = value;
                IsDirty = true;
            }
        }

        // Extra Para
        private DateTime start;
        public DateTime Start
        {
            get
            {
                return start;
            }
            set
            {
                if (start == value)
                    return;

                start = value;
            }
        }

        private string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                if (description == value)
                    return;

                description = value;
            }
        }

        private DateTime end;
        public DateTime End
        {
            get
            {
                return end;
            }
            set
            {
                if (end == value)
                    return;

                end = value;
            }
        }

        private int iD;
        public int ID
        {
            get
            {
                return iD;
            }
            set
            {
                if (iD == value)
                    return;

                iD = value;
            }
        }

        private string _LeadState;
        public string LeadState
        {
            get { return _LeadState; }
            set { _LeadState = value; }
        }

        private string _TypeDescription;
        public string TypeDescription
        {
            get { return _TypeDescription; }
            set { _TypeDescription = value; }
        }

        private string _RepCode;
        public string RepCode
        {
            get { return _RepCode; }
            set { _RepCode = value; }
        }

        private string _LeadStage;
        public string LeadStage
        {
            get { return _LeadStage; }
            set { _LeadStage = value; }
        }

        private int _ActivityId;
        public int ActivityId
        {
            get { return _ActivityId; }
            set { _ActivityId = value; }
        }

        private int _LeadId;
        public int LeadId
        {
            get { return _LeadId; }
            set { _LeadId = value; }
        }

        private string _CustomerCode;
        public string CustomerCode
        {
            get { return _CustomerCode; }
            set { _CustomerCode = value; }
        }

        private string _EnduserCode;
        public string EnduserCode
        {
            get { return _EnduserCode; }
            set { _EnduserCode = value; }
        }
    }
}
