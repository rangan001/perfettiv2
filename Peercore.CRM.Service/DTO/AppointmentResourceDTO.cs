﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class AppointmentResourceDTO : BaseDTO
    {
        public int AppointmentID { get; set; }
        public int ResourceID { get; set; }
    }
}
