﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class AtblDTO : BaseDTO
    {
        public string period { get; set; }

        public double sales_vol_p { get; set; }
        public double sales_vol_pp { get; set; }

        public double sales_val_p { get; set; }
        public double sales_val_pp { get; set; }

        public double sales_val_tonval_p { get; set; }
        public double sales_val_tonval_pp { get; set; }

        public double sales_profit_tonval_p { get; set; }
        public double sales_profit_tonval_pp { get; set; }

        public double sales_profit_perc_p { get; set; }
        public double sales_profit_perc_pp { get; set; }

        public double sales_profit_p { get; set; }
        public double sales_profit_pp { get; set; }

        public double budget_val_p { get; set; }
        public double budget_val_pp { get; set; }

        public double budget_vol_pp { get; set; }
        public double budget_vol_p { get; set; }
    }
}
