﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class BDMWideSalesAnalysisDTO
    {
        private string customerCode;

        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; }
        }
        private string customerName;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        //  SUM(o.units) as potential_units,
        //  0 as month_value,
        //  0 as month_units
        private double potentialLiters;

        public double PotentialLiters
        {
            get { return potentialLiters; }
            set { potentialLiters = value; }
        }
        private double actualLiters;

        public double ActualLiters
        {
            get { return actualLiters; }
            set { actualLiters = value; }
        }

        private decimal actualValue;

        public decimal ActualValue
        {
            get { return actualValue; }
            set { actualValue = value; }
        }

        private string bDM;

        public string BDM
        {
            get { return bDM; }
            set { bDM = value; }
        }

        private double acltr;

        public double AcLtr
        {
            get { return acltr; }
            set { acltr = value; }
        }

        private double totalLiters;

        public double TotalLiters
        {
            get { return totalLiters; }
            set { totalLiters = value; }
        }

        private double t1;

        public double T1
        {
            get { return t1; }
            set { t1 = value; }
        }
        private double t2;

        public double T2
        {
            get { return t2; }
            set { t2 = value; }
        }
        private double t3;

        public double T3
        {
            get { return t3; }
            set { t3 = value; }
        }

        private double units;

        public double Units
        {
            get { return units; }
            set { units = value; }
        }

        private DateTime? startDate;

        public DateTime? StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }
        private int noofweeks;

        public int NoOfWeeks
        {
            get { return noofweeks; }
            set { noofweeks = value; }
        }
        private DateTime reqSentDate;

        public DateTime ReqSentDate
        {
            get { return reqSentDate; }
            set { reqSentDate = value; }
        }

        public int RowCount { get; set; }
    }
}
