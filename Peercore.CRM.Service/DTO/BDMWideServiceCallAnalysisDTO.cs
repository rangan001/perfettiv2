﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class BDMWideServiceCallAnalysisDTO : BaseDTO
    {
        private string customerCode;

        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; }
        }

        private string customerName;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        private List<BDMWideServiceCallAnalysisStagesDTO> servicaCallStageList;

        public List<BDMWideServiceCallAnalysisStagesDTO> ServicaCallStageList
        {
            get { return servicaCallStageList; }
            set { servicaCallStageList = value; }
        }

        private int stageCountTotal;

        public int StageCountTotal
        {
            get { return stageCountTotal; }
            set { stageCountTotal = value; }
        }

        private string assignTo;

        public string AssignTo
        {
            get { return assignTo; }
            set { assignTo = value; }
        }

        private int activityId;

        public int ActivityId
        {
            get { return activityId; }
            set { activityId = value; }
        }

        private string city;

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        private string postCode;

        public string PostCode
        {
            get { return postCode; }
            set { postCode = value; }
        }

        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; }
        }

        public int StageCount1 { get; set; }
        public int StageCount2 { get; set; }
        public int StageCount3 { get; set; }
        public int StageCount4 { get; set; }
        public int StageCount5 { get; set; }
        public int StageCount6 { get; set; }
        public int StageCount7 { get; set; }
        public int StageCount8 { get; set; }

        public int StageCount9 { get; set; }
        public int StageCount10 { get; set; }
        public int StageCount11 { get; set; }
        public int StageCount12 { get; set; }
        public int StageCount13 { get; set; }
        public int StageCount14 { get; set; }
        public int StageCount15 { get; set; }
        public int StageCount16 { get; set; }

        public int StageCount17 { get; set; }
        public int StageCount18 { get; set; }
        public int StageCount19 { get; set; }
        public int StageCount20 { get; set; }
        public int StageCount21 { get; set; }
        public int StageCount22 { get; set; }
        public int StageCount23 { get; set; }
        public int StageCount24 { get; set; }

    }
}
