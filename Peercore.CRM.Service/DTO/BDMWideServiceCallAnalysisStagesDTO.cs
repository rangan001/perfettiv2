﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class BDMWideServiceCallAnalysisStagesDTO : BaseDTO
    {
        private string stageName;

        public string StageName
        {
            get { return stageName; }
            set { stageName = value; }
        }
        private int stageCount = 0;

        public int StageCount
        {
            get { return stageCount; }
            set { stageCount = value; }
        }

        private int stageOrder;

        public int StageOrder
        {
            get { return stageOrder; }
            set { stageOrder = value; }
        }

        private DateTime fromDate;

        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }

        private DateTime toDate;

        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
    }
}
