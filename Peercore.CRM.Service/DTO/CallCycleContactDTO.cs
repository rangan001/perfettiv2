﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO.CompositeEntities;

namespace Peercore.CRM.Service.DTO
{
    public class CallCycleContactDTO : BaseDTO
    {
        private int weekDayId = 0;
        private int routeId = 0;
        private CallCycleDTO callCycle = null;
        private LeadCustomerDTO contact = null;
        private string prefixCode;
        private DateTime dueOn = DateTime.Today;
        private TimeSpan startTime = TimeSpan.FromHours(5);
        private bool createActivity = true;
        private int dayOrder = 0;

        public int ItemIndex { get; set; }

        /// <summary>
        /// Gets or sets the call cycle.
        /// </summary>
        /// <value>The call cycle.</value>
        public CallCycleDTO CallCycle
        {
            get
            {
                if (callCycle == null)
                {
                    callCycle = new CallCycleDTO();
                }

                return callCycle;
            }
            set
            {
                callCycle = value;
            }
        }

        /// <summary>
        /// Gets or sets the originator.
        /// </summary>
        /// <value>The originator.</value>
        public string Originator { get; set; }

        public LeadCustomerDTO Contact
        {
            get
            {
                if (contact == null)
                {
                    contact = new LeadCustomerDTO();
                }

                return contact;
            }
            set
            {
                contact = value;
            }
        }
        

        public int WeekDayId
        {
            get { return weekDayId; }
            set { weekDayId = value; }
        }

        public DateTime DueOn
        {
            get { return dueOn; }
            set { dueOn = value; }
        }

        public TimeSpan StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        public bool CreateActivity
        {
            get { return createActivity; }
            set { createActivity = value; }
        }

        public string PrefixCode
        {
            get { return prefixCode; }
            set { prefixCode = value; }
        }

        public int DayOrder
        {
            get { return dayOrder; }
            set { dayOrder = value; }
        }

        public LeadStageDTO LeadStage { get; set; }

        public int RouteId
        {
            get { return routeId; }
            set { routeId = value; }
        }

        private bool hasSelect;
        public bool HasSelect
        {
            get { return hasSelect; }
            set { hasSelect = value; }
        }
    }
}
