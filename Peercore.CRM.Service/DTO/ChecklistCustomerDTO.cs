﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ChecklistCustomerDTO : BaseDTO
    {
        public int CheckListID { get; set; }
        public string CustomerCode { get; set; }
    }
}
