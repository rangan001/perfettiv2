﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ChecklistMasterDTO : BaseDTO
    {
        public int ChecklistId { get; set; }
        public int ChecklistTypeId { get; set; }
        public string ChecklistType { get; set; }
        public string Task { get; set; }
        public int IsHidden { get; set; }
        public string CustomerCode { get; set; }

        public List<ChecklistCustomerDTO> CustomerList { get; set; }
        public List<ChecklistRepDTO> RepList { get; set; }
        public List<ChecklistRouteDTO> RouteList { get; set; }
        public List<ChecklistImageDTO> ImageList { get; set; }
    }
}
