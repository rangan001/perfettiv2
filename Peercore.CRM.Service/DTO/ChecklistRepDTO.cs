﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ChecklistRepDTO : BaseDTO
    {
        public int ChecklistId { get; set; }
        public string RepCode { get; set; }
    }
}
