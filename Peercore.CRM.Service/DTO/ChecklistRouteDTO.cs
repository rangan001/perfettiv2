﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ChecklistRouteDTO : Base
    {
        public int ChecklistId { get; set; }
        public int RouteId { get; set; }
    }
}
