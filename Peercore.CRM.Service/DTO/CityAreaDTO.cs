﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CityAreaDTO : BaseDTO
    {
        private int? areaID;
        public int? AreaID
        {
            get { return areaID; }
            set { areaID = value; }
        }

        private int? cityID;
        public int? CityID
        {
            get { return cityID; }
            set { cityID = value; }
        }

        private string areaName;
        public string AreaName
        {
            get { return areaName; }
            set
            {
                areaName = value;
            }
        }

        public string AreaCode { get; set; }

        private List<AreaPostcodeDTO> areaPostcodeList;
        public List<AreaPostcodeDTO> AreaPostcodeList
        {
            get
            {
                if (areaPostcodeList == null)
                    areaPostcodeList = new List<AreaPostcodeDTO>();
                return areaPostcodeList;
            }
            set
            {
                areaPostcodeList = value;
            }
        }
    }
}
