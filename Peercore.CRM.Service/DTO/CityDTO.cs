﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CityDTO
    {
        private int? cityID;
        public int? CityID
        {
            get { return cityID; }
            set { cityID = value; }
        }

        private string cityName;
        public string CityName
        {
            get { return cityName; }
            set
            {
                cityName = value;
            }
        }

        private List<CityAreaDTO> areaList;
        public List<CityAreaDTO> AreaList
        {
            get
            {
                if (areaList == null)
                    areaList = new List<CityAreaDTO>();
                return areaList;
            }
            set
            {
                areaList = value;
            }
        }
    }
}
