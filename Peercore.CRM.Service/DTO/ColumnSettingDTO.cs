﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    [Serializable]
    public class ColumnSettingDTO : BaseDTO
    {
        private string sessionId;
        public string SessionId
        {
            get
            {
                return sessionId;
            }
            set
            {
                if (sessionId == value)
                    return;

                sessionId = value;
            }
        }

        private string propertyName;
        public string PropertyName
        {
            get
            {
                return propertyName;
            }
            set
            {
                if (propertyName == value)
                    return;

                propertyName = value;
            }
        }

        private string uniqueName;
        public string UniqueName
        {
            get
            {
                return uniqueName;
            }
            set
            {
                if (uniqueName == value)
                    return;

                uniqueName = value;
            }
        }

        private string header;
        public string Header
        {
            get
            {
                return header;
            }
            set
            {
                if (header == value)
                    return;

                header = value;
            }
        }

        private bool visible;
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                if (visible == value)
                    return;

                visible = value;
            }
        }

        private int idNo;
        public int IdNo
        {
            get
            {
                return idNo;
            }
            set
            {
                if (idNo == value)
                    return;

                idNo = value;
            }
        }

        private string originator;
        public string Originator
        {
            get
            {
                return originator;
            }
            set
            {
                if (originator == value)
                    return;

                originator = value;
            }
        }


        private string userControl;
        public string UserControl
        {
            get
            {
                return userControl;
            }
            set
            {
                if (userControl == value)
                    return;

                userControl = value;
            }
        }

        private string gridName;
        public string GridName
        {
            get
            {
                return gridName;
            }
            set
            {
                if (gridName == value)
                    return;

                gridName = value;
            }
        }

        private string filterName;
        public string FilterName
        {
            get
            {
                return filterName;
            }
            set
            {
                if (filterName == value)
                    return;

                filterName = value;
            }
        }

        private string isvisible;
        public string Isvisible
        {
            get
            {
                return isvisible;
            }
            set
            {
                if (isvisible == value)
                    return;

                isvisible = value;
            }
        }

        private double width;
        public double Width
        {
            get
            {
                return width;
            }
            set
            {
                if (width == value)
                    return;

                width = value;
            }
        }

        private int columnOrder;
        public int ColumnOrder
        {
            get
            {
                return columnOrder;
            }
            set
            {
                if (columnOrder == value)
                    return;

                columnOrder = value;
            }
        }

        private string sortType;
        public string SortType
        {
            get
            {
                return sortType;
            }
            set
            {
                if (sortType == value)
                    return;

                sortType = value;
            }
        }

        private string sortName;
        public string SortName
        {
            get
            {
                return sortName;
            }
            set
            {
                if (sortName == value)
                    return;

                sortName = value;
            }
        }

        private List<ColumnSettingDTO> lstColumnSetting;
        public List<ColumnSettingDTO> LstColumnSetting
        {
            get
            {
                return lstColumnSetting;
            }
            set
            {
                if (lstColumnSetting == value)
                    return;

                lstColumnSetting = value;
            }
        }

    }
}
