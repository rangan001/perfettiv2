﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO.CompositeEntities
{
    public class CallCycleViewDTO : BaseDTO
    {
        public int CallCycleID { get; set; }
        public int SourceId { get; set; }
        public string Description { get; set; }
        public DateTime DueOn { get; set; }
        public string Name { get; set; }
        public string Originator { get; set; }

        public string LeadStage { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
    }
}
