﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO.CompositeEntities
{
    public class EmailAndAttachmentsDTO
    {
        private EmailDTO email;
        public EmailDTO Email
        {
            get { return email; }
            set { email = value; }
        }

        private List<EmailAttachmentDTO> attachments;
        public List<EmailAttachmentDTO> Attachments
        {
            get { return attachments; }
            set { attachments = value; }
        }
    }
}
