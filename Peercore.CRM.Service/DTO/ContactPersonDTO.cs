﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ContactPersonDTO : BaseDTO
    {
        #region - bool -
        private bool isOwner = true;
        public bool IsOwner
        {
            get { return isOwner; }
            set { isOwner = value; IsDirty = true;  }
        } 

        private bool keyContactChecked;
        public bool KeyContactChecked
        {
            get { return keyContactChecked; }
            set { keyContactChecked = value; IsDirty = true; }
        }
        #endregion

        #region - DateTime -

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; IsDirty = true; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; IsDirty = true; }
        }
        
        #endregion

        #region - int -
        private int contactPersonID;
        public int ContactPersonID
        {
            get { return contactPersonID; }
            set { contactPersonID = value; IsDirty = true; }
        }

        private int leadID;
        public int LeadID
        {
            get { return leadID; }
            set { leadID = value; IsDirty = true; }
        }

        //private int rowCount;
        //public int RowCount
        //{
        //    get { return rowCount; }
        //    set { rowCount = value; IsDirty = true; }
        //}
        #endregion

        #region - string -
        private string companyAddress;
        public string CompanyAddress
        {
            get { return companyAddress; }
            set { companyAddress = value; IsDirty = true; }
        }

        private string companyName;
        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; IsDirty = true; }
        }

        private string companyTelephone;
        public string CompanyTelephone
        {
            get { return companyTelephone; }
            set { companyTelephone = value; IsDirty = true; }
        }

        private string contact;
        public string Contact
        {
            get { return contact; }
            set { contact = value; IsDirty = true; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; IsDirty = true; }
        }

        private string custCode;    // Not In Table
        public string CustCode
        {
            get { return custCode; }
            set { custCode = value; IsDirty = true; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; IsDirty = true; }
        }

        private string disLikes;
        public string DisLikes
        {
            get { return disLikes; }
            set { disLikes = value; IsDirty = true; }
        }

        private string emailAddress;
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; IsDirty = true; }
        }

        private string fax;
        public string Fax
        {
            get { return fax; }
            set { fax = value; IsDirty = true; }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; IsDirty = true; }
        }

        private string imagePath;
        public string ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; IsDirty = true; }
        }

        private string keyContact;
        public string KeyContact
        {
            get { return keyContact; }
            set { keyContact = value; IsDirty = true; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; IsDirty = true; }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; IsDirty = true; }
        }

        private string leadName;
        public string LeadName
        {
            get { return leadName; }
            set { leadName = value; IsDirty = true; }
        }

        private string likes;
        public string Likes
        {
            get { return likes; }
            set { likes = value; IsDirty = true; }
        }

        private string mailingAddress;
        public string MailingAddress
        {
            get { return mailingAddress; }
            set { mailingAddress = value; IsDirty = true; }
        }

        private string mailingCity;
        public string MailingCity
        {
            get { return mailingCity; }
            set { mailingCity = value; IsDirty = true; }
        }

        private string mailingCountry;
        public string MailingCountry
        {
            get { return mailingCountry; }
            set { mailingCountry = value; IsDirty = true; }
        }

        private string mailingPostcode;
        public string MailingPostcode
        {
            get { return mailingPostcode; }
            set { mailingPostcode = value; IsDirty = true; }
        }

        private string mailingState;
        public string MailingState
        {
            get { return mailingState; }
            set { mailingState = value; IsDirty = true; }
        }

        private string mobile;
        public string Mobile
        {
            get { return mobile; }
            set { mobile = value; IsDirty = true; }
        }

        private string note;
        public string Note
        {
            get { return note; }
            set { note = value; IsDirty = true; }
        }

        private string origin;
        public string Origin
        {
            get { return origin; }
            set { origin = value; IsDirty = true; }
        }

        private string originator;
        public string Originator
        {
            get { return originator; }
            set { originator = value; IsDirty = true; }
        }

        private string otherAddress;
        public string OtherAddress
        {
            get { return otherAddress; }
            set { otherAddress = value; IsDirty = true; }
        }

        private string otherCity;
        public string OtherCity
        {
            get { return otherCity; }
            set { otherCity = value; IsDirty = true; }
        }

        private string otherCountry;
        public string OtherCountry
        {
            get { return otherCountry; }
            set { otherCountry = value; IsDirty = true; }
        }

        private string otherPostCode;
        public string OtherPostCode
        {
            get { return otherPostCode; }
            set { otherPostCode = value; IsDirty = true; }
        }

        private string otherState;
        public string OtherState
        {
            get { return otherState; }
            set { otherState = value; IsDirty = true; }
        }

        private string position;
        public string Position
        {
            get { return position; }
            set { position = value; IsDirty = true; }
        }

        private string reportsTo;
        public string ReportsTo
        {
            get { return reportsTo; }
            set { reportsTo = value; IsDirty = true; }
        }

        private string specialInterests;
        public string SpecialInterests
        {
            get { return specialInterests; }
            set { specialInterests = value; IsDirty = true; }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; IsDirty = true; }
        }

        private string telephone;
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; IsDirty = true; }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; IsDirty = true; }
        }

        public string EndUserName { get; set; }
        public string EndUserCode { get; set; }
        public string ContactType { get; set; }
        public string TypeDescription { get; set; }
        #endregion

    }
}
