﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ContactPersonImageDTO:BaseDTO
    {
        private int contactNo;
        public int ContactNo
        {
            get { return contactNo; }
            set { contactNo = value; IsDirty = true; }
        }

        private int contactPersonId;
        public int ContactPersonId
        {
            get { return contactPersonId; }
            set { contactPersonId = value; IsDirty = true; }
        }

        private int contactPersonImageId;
        public int ContactPersonImageId
        {
            get { return contactPersonImageId; }
            set { contactPersonImageId = value; IsDirty = true; }
        }

        private string customerCode;
        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; IsDirty = true; }
        }

        private string filePath;
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; IsDirty = true; }
        }

        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; IsDirty = true; }
        }

        private int leadID;
        public int LeadID
        {
            get { return leadID; }
            set { leadID = value; IsDirty = true; }
        }

        public string EndUserCode { get; set; }

    }
}
