﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CustomizedTableDTO : BaseDTO
    {
        public string MC { get; set; }

        public double M1D { get; set; }
        public double M1T { get; set; }
        public double M1U { get; set; }

        public double M2D { get; set; }
        public double M2T { get; set; }
        public double M2U { get; set; }

        public double M3D { get; set; }
        public double M3T { get; set; }
        public double M3U { get; set; }

        public double M4D { get; set; }
        public double M4T { get; set; }
        public double M4U { get; set; }

        public double M5D { get; set; }
        public double M5T { get; set; }
        public double M5U { get; set; }

        public double M6D { get; set; }
        public double M6T { get; set; }
        public double M6U { get; set; }

        private double mtd = 0;
        public double MTD
        {
            get
            {
                return M1D + M2D + M3D + M4D + M5D + M6D;
            }
            set
            {
                mtd = value;
            }
        }
        private double mtt = 0;
        public double MTT
        {
            get
            {
                return M1T + M2T + M3T + M4T + M5T + M6T;
            }
            set
            {
                mtt = value;
            }
        }
        private double mtu = 0;
        public double MTU
        {
            get
            {
                return M1U + M2U + M3U + M4U + M5U + M6U;
            }
            set
            {
                mtu = value;
            }
        }
        //public double MTT { get; set; }
        //public double MTU { get; set; }

        public string MD { get; set; }

        public double MTYTDD { get; set; }
        public double MTYTDT { get; set; }
        public double MTYTDU { get; set; }

        public double MLYTDD { get; set; }
        public double MLYTDT { get; set; }
        public double MLYTDU { get; set; }

        public double MPD { get; set; }
        public double MPT { get; set; }
        public double MPU { get; set; }

        public double MY1D { get; set; }
        public double MY1T { get; set; }
        public double MY1U { get; set; }

        public double MY2D { get; set; }
        public double MY2T { get; set; }
        public double MY2U { get; set; }

        public double MY3D { get; set; }
        public double MY3T { get; set; }
        public double MY3U { get; set; }


        public double M1DTot { get; set; }
        public double M1TTot { get; set; }
        public double M1UTot { get; set; }

        public double M2DTot { get; set; }
        public double M2TTot { get; set; }
        public double M2UTot { get; set; }

        public double M3DTot { get; set; }
        public double M3TTot { get; set; }
        public double M3UTot { get; set; }

        public double M4DTot { get; set; }
        public double M4TTot { get; set; }
        public double M4UTot { get; set; }

        public double M5DTot { get; set; }
        public double M5TTot { get; set; }
        public double M5UTot { get; set; }

        public double M6DTot { get; set; }
        public double M6TTot { get; set; }
        public double M6UTot { get; set; }

        private double mtdTot = 0;
        public double MTDTot
        {
            get
            {
                return M1DTot + M2DTot + M3DTot + M4DTot + M5DTot + M6DTot;
            }
            set
            {
                mtdTot = value;
            }
        }
        private double mttTot = 0;
        public double MTTTot
        {
            get
            {
                return M1TTot + M2TTot + M3TTot + M4TTot + M5TTot + M6TTot;
            }
            set
            {
                mttTot = value;
            }
        }
        private double mtuTot = 0;
        public double MTUTot
        {
            get
            {
                return M1UTot + M2UTot + M3UTot + M4UTot + M5UTot + M6UTot;
            }
            set
            {
                mtuTot = value;
            }
        }

        public double MTYTDDTot { get; set; }
        public double MTYTDTTot { get; set; }
        public double MTYTDUTot { get; set; }

        public double MLYTDDTot { get; set; }
        public double MLYTDTTot { get; set; }
        public double MLYTDUTot { get; set; }

        public double MPDTot { get; set; }
        public double MPTTot { get; set; }
        public double MPUTot { get; set; }

        public double MY1DTot { get; set; }
        public double MY1TTot { get; set; }
        public double MY1UTot { get; set; }

        public double MY2DTot { get; set; }
        public double MY2TTot { get; set; }
        public double MY2UTot { get; set; }

        public double MY3DTot { get; set; }
        public double MY3TTot { get; set; }
        public double MY3UTot { get; set; }

        public int MDTot { get; set; }

        public string FlowContent { get; set; }

        //public List<string> ColumnHeaders { get; set; }
    }
}
