﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class EmailAttachmentDTO : BaseDTO
    {
        private int attachmentID;
        public int AttachmentID
        {
            get { return attachmentID; }
            set { attachmentID = value; }
        }

        private int emailId;
        public int EmailId
        {
            get { return emailId; }
            set { emailId = value; }
        }

        private string path;
        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; IsDirty = true; }
        }

        private string filePath;
        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                IsDirty = true;
            }
        }
    }
}
