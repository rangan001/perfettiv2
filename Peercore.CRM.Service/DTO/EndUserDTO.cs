﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class EndUserDTO : BaseDTO
    {
        public string CustomerCode { get; set; }

        public string EndUserCode { get; set; }

        public string Name { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string Telephone { get; set; }

        public string Contact { get; set; }

        public string RepCode { get; set; }

        public string EmailAddress { get; set; }

        public int Version { get; set; }

        public bool IsActive { get; set; }

        public string PrimaryDistributor { get; set; }

        public int Rating { get; set; }

        public string EndUserType { get; set; }

        // Not in table
        public string CustomerName { get; set; }

        public string PrimaryRepName { get; set; }

        public string SecondaryRepName { get; set; }

        public int ViewRowCount { get; set; }

        public List<EndUserPriceDTO> CategoryList { get; set; }

        private string selectedToCall = "";
        public string SelectedToCall
        {
            get
            {
                return selectedToCall;
            }
            set
            {
                if (selectedToCall == value)
                    return;

                selectedToCall = value;
            }
        }

    }
}
