﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class FlavorDTO : BaseDTO
    {
        public int FlavorId { get; set; }
        public string FlavorName { get; set; }
    }
}
