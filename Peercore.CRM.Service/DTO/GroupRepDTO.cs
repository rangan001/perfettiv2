﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class GroupRepDTO : BaseDTO
    {

        private int? repGroupId;
        public int? RepGroupId
        {
            get { return repGroupId; }
            set
            {
                repGroupId = value;
            }
        }

        private string originator;
        public string Originator
        {
            get { return originator; }
            set { originator = value; IsDirty = true; }
        }
    }
}
