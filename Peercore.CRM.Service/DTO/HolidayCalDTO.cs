﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class HolidayCalDTO:BaseDTO
    {

        public int TaskID { get; set; }
        public int OwnerID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string StartTimezone { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public int RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }

        public int HolidayId { get; set; }
        public DateTime Date { get; set; }
      ///  public string Description { get; set; }
        public string CreatedBy { get; set; }
     //   public string Title { get; set; }
        // public DateTime Start { get; set; }
        public DateTime EndDate { get; set; }
        public string HolidayType { get; set; }
        public string TypeDescription { get; set; }
        public string TypeColor { get; set; }
    }
}
