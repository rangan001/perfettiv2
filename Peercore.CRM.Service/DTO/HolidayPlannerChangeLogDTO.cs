﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class HolidayPlannerChangeLogDTO:BaseDTO
    {
        public int HolidayPlannerChangeLogId { get; set; }
        public int KeyId { get; set; }
        public string FormType { get; set; }

        public DateTime ChangedFromDate { get; set; }
        public DateTime ChangedToDate { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
