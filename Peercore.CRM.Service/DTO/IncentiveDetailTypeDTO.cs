﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class IncentiveDetailTypeDTO:BaseDTO
    {
        public int IpDetailsTypeId { get; set; }
        public string SchemeType { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }
    }
}
