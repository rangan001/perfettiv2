﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class IncentiveOptionLevelCombinationDTO:BaseDTO
    {
        public int IpOptionLevelCombinationId { get; set; }
        public int IpOptionLevelId { get; set; }
        public int ProductId { get; set; }
        public bool IsExceptProduct { get; set; }
        public int ProductPackingId { get; set; }
        public int ProductCategoryId { get; set; }
        public int BrandId { get; set; }
        public bool? IsHvp { get; set; }
        public int FlavorId { get; set; }
        public bool IsActive { get; set; }

        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string ProductCatagoryName { get; set; }
        public string BrandName { get; set; }
        public string FlavorName { get; set; }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }
    }
}
