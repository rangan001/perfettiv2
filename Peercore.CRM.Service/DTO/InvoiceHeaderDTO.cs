﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class InvoiceHeaderDTO : BaseDTO 
    {
        public int InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public string CustCode { get; set; }
        public string InvoiceDate { get; set; }
        public double GrossTotal { get; set; }
        public double Discount { get; set; }
        public double ReturnsTotal { get; set; }
        public double GrandTotal { get; set; }
        public string Status { get; set; }
        public string RepCode { get; set; }
        public string CustomerName { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }

        public List<InvoiceDetailDTO> InvoiceDetailList { get; set; }
        public List<InvoiceSchemeGroupDTO> InvoiceSchemeGroupList { get; set; }

    }
}
