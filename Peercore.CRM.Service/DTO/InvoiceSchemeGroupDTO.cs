﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class InvoiceSchemeGroupDTO : BaseDTO
    {
        public int SchemeGroupId { get; set; }
        public int InvoiceId { get; set; }
        public int SchemeDetailsId { get; set; }
        public double DiscountValue { get; set; }

        public string SchemeDetailName { get; set; }

        public List<InvoiceSchemeDTO> InvoiceSchemeList { get; set; }
    }
}
