﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ItineraryHeaderDTO:BaseDTO
    {
        public int ItineraryHeaderId { get; set; }
        public int CallCycleId { get; set; }
        public string AssignedTo { get; set; }
        public string BaseTown { get; set; }
        public string ItineraryName { get; set; }
        public string Status { get; set; }
        public bool HasSentMail { get; set; }
        public string Comments { get; set; }
        public string Month { get; set; }
        public List<ItineraryDetailDTO> ItineraryDetailList { get; set; }
        public int ItineraryLogEntryId { get; set; }
        public string CreatedBy{ get; set; }
        public DateTime? CreatedDate{ get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime EffDate { get; set; }
        public int PrivateField { get; set; }
        public int Contigency { get; set; }
        public int OfficialMiles { get; set; }
    }
}
