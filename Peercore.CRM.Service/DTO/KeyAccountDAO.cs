﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class KeyAccountDAO : BaseDTO
    {
        public string AccountName { get; set; }
        public double Amount { get; set; }
    }
}
