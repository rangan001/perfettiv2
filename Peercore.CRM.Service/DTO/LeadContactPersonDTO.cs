﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LeadContactPersonDTO : BaseDTO
    {

        #region - int -
        public int ContactPersonID { get; set; }
        public int LeadID { get; set; }
        public int RowCount { get; set; } 
        #endregion

        #region - string -
        public string Company { get; set; }
        public string CustCode { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Telephone { get; set; }
        public string Website { get; set; }

        public string LinkUrl { get; set; } 
        #endregion

    }
}
