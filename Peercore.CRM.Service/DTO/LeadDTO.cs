﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LeadDTO : BaseDTO
    {
        #region - Windows -

        #region - string -

        private string address;
        public string Address
        {
            get { return address; }
            set { address = value; IsDirty = true; }
        }

        private string business;
        public string Business
        {
            get { return business; }
            set { business = value; IsDirty = true; }
        }

        private string businessPotential;
        public string BusinessPotential
        {
            get { return businessPotential; }
            set { businessPotential = value; IsDirty = true; }
        }

        private string city;
        public string City
        {
            get { return city; }
            set { city = value; IsDirty = true; }
        }

        private string company;
        public string Company
        {
            get { return company; }
            set { company = value; IsDirty = true; }
        }

        private string country;
        public string Country
        {
            get { return country; }
            set { country = value; IsDirty = true; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; IsDirty = true; }
        }

        private string custCode;
        public string CustCode
        {
            get { return custCode; }
            set { custCode = value; IsDirty = true; }
        }

        private string delFalg;
        public string DelFalg
        {
            get { return delFalg; }
            set { delFalg = value; IsDirty = true; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; IsDirty = true; }
        }

        private string emailAddress;
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; IsDirty = true; }
        }

        private string fax;
        public string Fax
        {
            get { return fax; }
            set { fax = value; IsDirty = true; }
        }

        private string industry;
        public string Industry
        {
            get { return industry; }
            set { industry = value; IsDirty = true; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; IsDirty = true; }
        }

        private string leadName;
        public string LeadName
        {
            get { return leadName; }
            set { leadName = value; IsDirty = true; }
        }

        private string leadSource;
        public string LeadSource
        {
            get { return leadSource; }
            set { leadSource = value; IsDirty = true; }
        }

        private string leadStatus;
        public string LeadStatus
        {
            get { return leadStatus; }
            set { leadStatus = value; IsDirty = true; }
        }

        private string litersBy;
        public string LitersBy
        {
            get { return litersBy; }
            set { litersBy = value; IsDirty = true; }
        }

        private string mobile;
        public string Mobile
        {
            get { return mobile; }
            set { mobile = value; IsDirty = true; }
        }

        private string originator;
        public string Originator
        {
            get { return originator; }
            set { originator = value; IsDirty = true; }
        }

        private string postCode;
        public string PostCode
        {
            get { return postCode; }
            set { postCode = value; IsDirty = true; }
        }

        private string preferredContact;
        public string PreferredContact
        {
            get { return preferredContact; }
            set { preferredContact = value; IsDirty = true; }
        }

        private string previousCustomerCode;
        public string PreviousCustomerCode
        {
            get { return previousCustomerCode; }
            set { previousCustomerCode = value; IsDirty = true; }
        }

        private string referredBy;
        public string ReferredBy
        {
            get { return referredBy; }
            set { referredBy = value; IsDirty = true; }
        }

        private string repGroupName;
        public string RepGroupName
        {
            get { return repGroupName; }
            set { repGroupName = value; IsDirty = true; }
        }

        private string state;
        public string State
        {
            get { return state; }
            set { state = value; IsDirty = true; }
        }

        private string telephone;
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; IsDirty = true; }
        }

        private string website;
        public string Website
        {
            get { return website; }
            set { website = value; IsDirty = true; }
        } 

        #endregion

        #region - int -
        private int leadID;
        public int LeadID
        {
            get { return leadID; }
            set { leadID = value; IsDirty = true; }
        }

        private int noOfEmployees;
        public int NoOfEmployees
        {
            get { return noOfEmployees; }
            set { noOfEmployees = value; IsDirty = true; }
        }

        private int leadStageID;
        public int LeadStageID
        {
            get { return leadStageID; }
            set { leadStageID = value; IsDirty = true; }
        }

        private int rating;
        public int Rating
        {
            get { return rating; }
            set { rating = value; IsDirty = true; }
        }

        private int repGroupID;
        public int RepGroupID
        {
            get { return repGroupID; }
            set { repGroupID = value; IsDirty = true; }
        }
        #endregion

        #region - DateTime -

        private DateTime createdDate;
        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private DateTime? lastCalledDate;
        public DateTime? LastCalledDate
        {
            get { return lastCalledDate; }
            set { lastCalledDate = value; }
        }

        private DateTime lastModifiedDate;
        public DateTime LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }

        private DateTime? startDate;
        public DateTime? StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        #endregion

        #region - double -

        private double annualRevenue;
        public double AnnualRevenue
        {
            get { return annualRevenue; }
            set { annualRevenue = value; }
        }

        private double potentialLiters;
        public double PotentialLiters
        {
            get { return potentialLiters; }
            set { potentialLiters = value; }
        }

        private double probability;
        public double Probability
        {
            get { return probability; }
            set { probability = value; }
        }

        #endregion

        #endregion
    }
}
