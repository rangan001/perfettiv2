﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LookupTableDTO : BaseDTO
    {
        public string TableID { get; set; }
        public string TableCode { get; set; }
        public string TableDescription { get; set; }
        public string Description { get; set; }
        public string DefaultValue { get; set; }
    }
}
