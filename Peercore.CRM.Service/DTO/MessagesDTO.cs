﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class MessagesDTO : BaseDTO 
    {
        private int messageID;
        public int MessageID
        {
            get
            {
                return messageID;
            }
            set
            {
                if (messageID == value)
                    return;

                messageID = value;
                IsDirty = true;
            }
        }

        private string message;
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                if (message == value)
                    return;

                message = value;
                IsDirty = true;
            }
        }

        private string originator;
        public string Originator
        {
            get
            {
                return originator;
            }
            set
            {
                if (originator == value)
                    return;

                originator = value;
            }
        }

        private DateTime messageDate;
        public DateTime MessageDate
        {
            get
            {
                return messageDate;
            }
            set
            {
                if (messageDate == value)
                    return;

                messageDate = value;
                IsDirty = true;
            }
        }
    }
}
