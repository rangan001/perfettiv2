﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OpportunityDTO : BaseDTO
    {

        private int opportunityID;
        public int OpportunityID
        {
            get
            {
                return opportunityID;
            }
            set
            {
                if (opportunityID == value)
                    return;

                opportunityID = value;
                IsDirty = true;
            }
        }
        private int leadID;
        public int LeadID
        {
            get
            {
                return leadID;
            }
            set
            {
                if (leadID == value)
                    return;

                leadID = value;
                IsDirty = true;
            }
        }

        private string originator;
        public string Originator
        {
            get
            {
                return originator;
            }
            set
            {
                if (originator == value)
                    return;

                originator = value;
                IsDirty = true;
            }
        }

        private int repGroupID;
        public int RepGroupID
        {
            get
            {
                return repGroupID;
            }
            set
            {
                if (repGroupID == value)
                    return;

                repGroupID = value;
                IsDirty = true;
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name == value)
                    return;

                name = value;
                IsDirty = true;
            }
        }

        private DateTime closeDate;
        public DateTime CloseDate
        {
            get
            {
                return closeDate;
            }
            set
            {
                if (closeDate == value)
                    return;

                closeDate = value;
                IsDirty = true;
            }
        }

        private int stage;
        public int Stage
        {
            get
            {
                return stage;
            }
            set
            {
                if (stage == value)
                    return;

                stage = value;
                IsDirty = true;
            }
        }

        private double probability;
        public double Probability
        {
            get
            {
                return probability;
            }
            set
            {
                if (probability == value)
                    return;

                probability = value;
                IsDirty = true;
            }
        }
        private double amount;
        public double Amount
        {
            get
            {
                return amount;
            }
            set
            {
                if (amount == value)
                    return;

                amount = value;
                IsDirty = true;
            }
        }
        private string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                if (description == value)
                    return;

                description = value;
                IsDirty = true; 
            }
        }
        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                if (createdDate == value)
                    return;

                createdDate = value;
                IsDirty = true;
            }
        }
        private string createdBy;
        public string CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                if (createdBy == value)
                    return;

                createdBy = value;
                IsDirty = true;
            }
        }
        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get
            {
                return lastModifiedDate;
            }
            set
            {
                if (lastModifiedDate == value)
                    return;

                lastModifiedDate = value;
                IsDirty = true;
            }
        }
        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                if (lastModifiedBy == value)
                    return;

                lastModifiedBy = value;
                IsDirty = true;
            }
        }
        private string custCode;
        public string CustCode
        {
            get
            {
                return custCode;
            }
            set
            {
                if (custCode == value)
                    return;

                custCode = value;
                IsDirty = true;
            }
        }
        private string pipelineStage;
        public string PipelineStage
        {
            get
            {
                return pipelineStage;
            }
            set
            {
                if (pipelineStage == value)
                    return;

                pipelineStage = value;
                IsDirty = true;
            }
        }
        private int opportunityCount;
        public int OpportunityCount
        {
            get
            {
                return opportunityCount;
            }
            set
            {
                if (opportunityCount == value)
                    return;

                opportunityCount = value;
                IsDirty = true;
            }
        }
        private double units;
        public double Units
        {
            get
            {
                return units;
            }
            set
            {
                if (units == value)
                    return;

                units = value;
                IsDirty = true;
            }
        }


        private string leadName;
        public string LeadName
        {
            get
            {
                return leadName;
            }
            set
            {
                if (leadName == value)
                    return;

                leadName = value;
                IsDirty = true;
            }
        }
        private string business;
        public string Business
        {
            get
            {
                return business;
            }
            set
            {
                if (business == value)
                    return;

                business = value;
                IsDirty = true;
            }
        }
        private string industry;
        public string Industry
        {
            get
            {
                return industry;
            }
            set
            {
                if (industry == value)
                    return;

                industry = value;
                IsDirty = true;
            }
        }
        private string industryDescription;
        public string IndustryDescription
        {
            get
            {
                return industryDescription;
            }
            set
            {
                if (industryDescription == value)
                    return;

                industryDescription = value;
                IsDirty = true;
            }
        }
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address == value)
                    return;

                address = value;
                IsDirty = true;
            }
        }
        private string city;
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                if (city == value)
                    return;

                city = value;
                IsDirty = true;
            }
        }
        private string state;
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                if (state == value)
                    return;

                state = value;
                IsDirty = true;
            }
        }
        private string postCode;
        public string PostCode
        {
            get
            {
                return postCode;
            }
            set
            {
                if (postCode == value)
                    return;

                postCode = value;
                IsDirty = true;
            }
        }
        private string leadStage;
        public string LeadStage
        {
            get
            {
                return leadStage;
            }
            set
            {
                if (leadStage == value)
                    return;

                leadStage = value;
                IsDirty = true;
            }
        }
        private int pipelineStageCount;
        public int PipelineStageCount
        {
            get
            {
                return pipelineStageCount;
            }
            set
            {
                if (pipelineStageCount == value)
                    return;

                pipelineStageCount = value;
                IsDirty = true;
            }
        }
        private int pipelineStageId;
        public int PipelineStageId
        {
            get
            {
                return pipelineStageId;
            }
            set
            {
                if (pipelineStageId == value)
                    return;

                pipelineStageId = value;
                IsDirty = true;
            }
        }
        private DateTime? lastActivityDate;
        public DateTime? LastActivityDate
        {
            get
            {
                return lastActivityDate;
            }
            set
            {
                if (lastActivityDate == value)
                    return;

                lastActivityDate = value;
                IsDirty = true;
            }
        }

        //private int rowCount;
        //public int RowCount
        //{
        //    get { return rowCount; }
        //    set { rowCount = value; IsDirty = true; }
        //}


        private string unitString;
        public string UnitString
        {
            get
            {
                return unitString;
            }
            set
            {
                if (unitString == value)
                    return;

                unitString = value;
                IsDirty = true;
            }
        }


        private double tonnes;
        public double Tonnes
        {
            get
            {
                return tonnes;
            }
            set
            {
                if (tonnes == value)
                    return;

                tonnes = value;
                IsDirty = true;
            }
        }

        public string EndUserCode { get; set; }
        
    }
}
