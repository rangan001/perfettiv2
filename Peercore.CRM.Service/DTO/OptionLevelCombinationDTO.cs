﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OptionLevelCombinationDTO : BaseDTO
    {
        public int OptionLevelCombinationId { get; set; }
        public int OptionLevelId { get; set; }
        public int ProductId { get; set; }
        public bool IsExceptProduct { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductPackingId { get; set; }
        public int BrandId { get; set; }
        public int FlavorId { get; set; }
        public bool IsActive { get; set; }

        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string FlavorName { get; set; }
        public string BrandName { get; set; }
        public string ProductCatagoryName { get; set; }
        public bool? IsHvp { get; set; }
        public int IndexId { get; set; }

        private string hvp = "Any";
        public string Hvp
        {
            get
            {
                if (IsHvp.HasValue)
                {
                    if(IsHvp.Value)
                        hvp = "Yes";
                    else
                        hvp = "No";
                }
                else
                {
                    hvp = "Any";
                }
                return hvp;
            }
            set
            {
            }
        }

        public string IsExcept { get; set; }
    }
}
