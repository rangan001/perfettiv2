﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OriginatorDTO : BaseDTO
    {
        private int userId;
        public int UserId
        {
            get
            {
                return userId;
            }
            set
            {
                if (userId == value)
                    return;

                userId = value;
                IsDirty = true;
            }
        }

        private string userName;
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                if (userName == value)
                    return;

                userName = value;
                IsDirty = true;
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name == value)
                    return;

                name = value;
                IsDirty = true;
            }
        }

        private string email;
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                if (email == value)
                    return;

                email = value;
                IsDirty = true;
            }
        }

        private string clientType;
        public string ClientType
        {
            get
            {
                return clientType;
            }
            set
            {
                if (clientType == value)
                    return;

                clientType = value;
                IsDirty = true;
            }
        }

        private int originatorId;
        public int OriginatorId
        {
            get
            {
                return originatorId;
            }
            set
            {
                if (originatorId == value)
                    return;

                originatorId = value;
                IsDirty = true;
            }
        }

        private string repCode;
        public string RepCode
        {
            get
            {
                return repCode;
            }
            set
            {
                if (repCode == value)
                    return;

                repCode = value;
                IsDirty = true;
            }
        }

        private bool managerMode;
        public bool ManagerMode
        {
            get { return managerMode; }
            set { managerMode = value; IsDirty = true; }
        }

        private bool hasChildReps;
        public bool HasChildReps
        {
            get { return hasChildReps; }
            set { hasChildReps = value; IsDirty = true; }
        }

        private string childOriginators;
        public string ChildOriginators
        {
            get { return childOriginators; }
            set { childOriginators = value; IsDirty = true; }
        }

        private string bDMList;
        public string BDMList
        {
            get { return bDMList; }
            set { bDMList = value; IsDirty = true; }
        }

        private int crmAuthLevel;
        public int CRMAuthLevel
        {
            get
            {
                return crmAuthLevel;
            }
            set
            {
                crmAuthLevel = value; IsDirty = true;
            }
        }

        private int authorizationGroupId;
        public int AuthorizationGroupId
        {
            get { return authorizationGroupId; }
            set { authorizationGroupId = value; IsDirty = true;}
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; IsDirty = true; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }

        private string repType;
        public string RepType
        {
            get { return repType; }
            set { repType = value; }
        }

        private string defaultDepartmentId;
        public string DefaultDepartmentId
        {
            get { return defaultDepartmentId; }
            set { defaultDepartmentId = value; }
        }

        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public string DeptString { get; set; }
        public string Adderess { get; set; }
        public string Mobile { get; set; }

        public string AccessToken { get; set; }

        public string TFAPin { get; set; }
        public bool IsTFAUser { get; set; }
        public bool TFAIsAuthenticated { get; set; }
    }
}
