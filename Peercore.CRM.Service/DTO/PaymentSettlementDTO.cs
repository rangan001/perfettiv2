﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class PaymentSettlementDTO: BaseDTO
    {
        public int PaymentSettlementId { get; set; }
        public string CustCode { get; set; }
        public string ChequeNumber { get; set; }
        public string OutletBank { get; set; }
        public string PaymentType { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public int IvceNo { get; set; }
        public int DistributorAccountId { get; set; }
        public DateTime ChequeDate { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public bool HasSelect { get; set; }
    }
}
