﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class PipelineStageDTO : BaseDTO
    {
        private int? pipelineStageID;
        public int? PipelineStageID
        {
            get
            {
                return pipelineStageID;
            }
            set
            {
                if (pipelineStageID == value)
                    return;

                pipelineStageID = value;
                IsDirty = true;
            }
        }

        private string pipelineStageName;
        public string PipelineStageName
        {
            get
            {
                return pipelineStageName;
            }
            set
            {
                if (pipelineStageName == value)
                    return;

                pipelineStageName = value;
                IsDirty = true;
            }
        }

        private int order;
        public int Order
        {
            get
            {
                return order;
            }
            set
            {
                if (order == value)
                    return;

                order = value;
                IsDirty = true;
            }
        }


        public double TotalAmount { get; set; }

        public double TotalUnits { get; set; }
    }
}
