﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ProgramCustomerDTO : BaseDTO
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public string CustCode { get; set; }
        public int Volume { get; set; }
        public string SelectionType { get; set; }
        public string ProgramName { get; set; }
    }
}
