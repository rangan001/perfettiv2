﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ProgramDTO:BaseDTO
    {
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string Status { get; set; }
        public string SelectionType { get; set; }
        public List<int> VolumeList { get; set; }
        public List<string> CustomerList { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public string Volume
        {
            get
            {
                var sb = new StringBuilder(100);
                if (VolumeList != null)
                {
                    sb = VolumeList.Aggregate(sb, (b, d) => b.Append(d).Append(','));
                    if (VolumeList.Count > 0) sb.Length--;
                }
                return sb.ToString();
            }
            set
            {
            }
        }
    }
}
