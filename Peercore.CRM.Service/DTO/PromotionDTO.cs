﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class PromotionDTO
    {

        private int promotionid;
        public int PromotionId
        {
            get { return promotionid; }
            set { promotionid = value; }
        }

        private string promotionName;
        public string PromotionName
        {
            get { return promotionName; }
            set { promotionName = value; }
        }


    }
}
