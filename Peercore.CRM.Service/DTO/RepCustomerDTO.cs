﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class RepCustomerDTO : BaseDTO
    {
        public string State { get; set; }
        public string Originator { get; set; }
        public string Name { get; set; }
        public int NewCustomers { get; set; }
        public int CustomersInState { get; set; }
        public double Target { get; set; }
        public List<LeadDTO> CustomerList { get; set; }
    }
}
