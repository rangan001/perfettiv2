﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class RouteMasterDTO: BaseDTO
    {
        public int RouteMasterId { get; set; }
        public string RouteName { get; set; }
        public string Status { get; set; }
        public string RouteType { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public double Target { get; set; }
        public int TargetId { get; set; }

        public DateTime EffStartDate { get; set; }
        public DateTime EffEndDate { get; set; }

        public string RepCode { get; set; }
        public string RepName { get; set; }
        public string CreatedByName { get; set; }
        public int CallCycleId { get; set; }
        public bool HasSelect { get; set; }
    }
}
