﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SalesGraphDataDTO:BaseDTO
    {       
        private double yValue_FirstMonth;
        public double YValue_FirstMonth
        {
            get { return yValue_FirstMonth; }
            set { yValue_FirstMonth = value; IsDirty = true; }
        }

        private string xLabel_FirstMonth;
        public string XLabel_FirstMonth
        {
            get { return xLabel_FirstMonth; }
            set { xLabel_FirstMonth = value; IsDirty = true; }
        }


        private double yValue_SecondMonth;
        public double YValue_SecondMonth
        {
            get { return yValue_SecondMonth; }
            set { yValue_SecondMonth = value; IsDirty = true; }
        }

        private string xLabel_SecondMonth;
        public string XLabel_SecondMonth
        {
            get { return xLabel_SecondMonth; }
            set { xLabel_SecondMonth = value; IsDirty = true; }
        }

        private double yValue_ThirdMonth;
        public double YValue_ThirdMonth
        {
            get { return yValue_ThirdMonth; }
            set { yValue_ThirdMonth = value; IsDirty = true; }
        }

        private string xLabel_ThirdMonth;
        public string XLabel_ThirdMonth
        {
            get { return xLabel_ThirdMonth; }
            set { xLabel_ThirdMonth = value; IsDirty = true; }
        }

        private double totalSales;
        public double TotalSales
        {
            get { return totalSales; }
            set { totalSales = value; IsDirty = true; }
        }

        private double salesSummary;
        public double SalesSummary
        {
            get { return salesSummary; }
            set { salesSummary = value; IsDirty = true; }
        }

        
        
    }
}
