﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SalesInfoDetailBackDTO : BaseDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description{ get; set; }
        public string DetailType { get; set; }
        public string Type { get; set; }
        public string MarketId { get; set; }
    }
}
