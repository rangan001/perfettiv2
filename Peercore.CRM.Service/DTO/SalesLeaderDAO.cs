﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SalesLeaderDAO : BaseDTO
    {
        public string OwnerName { get; set; }
        public double Amount { get; set; }
        public double Units { get; set; }
        public string Data { get; set; }
    }
}
