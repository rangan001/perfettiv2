﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SalesOrderDTO : BaseDTO
    {
        public int SOrderNo { get; set; }
        public int PListNo { get; set; }
        public DateTime DateRequired { get; set; }
        public string CatlogCode { get; set; }
        public string Description { get; set; }
        public DateTime OrderDate { get; set; }
        public double RequiredQty { get; set; }
        public double Price { get; set; }
    }
}
