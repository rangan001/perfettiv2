﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SchedularTransferComponentDTO : BaseDTO
    {
        private DateTime selectedDate;
        public DateTime SelectedDate
        {
            get { return selectedDate; }
            set { selectedDate = value; }
        }

        private TimeSpan selectedTime;
        public TimeSpan SelectedTime
        {
            get { return selectedTime; }
            set { selectedTime = value; }
        }

        private bool createActivity;
        public bool CreateActivity
        {
            get { return createActivity; }
            set { createActivity = value; }
        }

        private int weekDay;
        public int WeekDay
        {
            get { return weekDay; }
            set { weekDay = value; }
        }
    }
}
