﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SchemeHeaderDTO : BaseDTO
    {
        public int SchemeHeaderId { get; set; }
        public string SchemeName { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }

        public List<SchemeDetailsDTO> SchemeDetailsList { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
