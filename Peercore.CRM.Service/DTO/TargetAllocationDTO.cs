﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.DTO
{
    public class TargetAllocationDTO
    {
        private List<DistributorDTO> distributorTargetList;
        public List<DistributorDTO> DistributorTargetList
        {
            get { return distributorTargetList; }
            set { distributorTargetList = value; }
        }

        private List<MarketDTO> marketTargetList;
        public List<MarketDTO> MarketTargetList
        {
            get { return marketTargetList; }
            set { marketTargetList = value; }
        }

        private List<RouteMasterDTO> routeTargetList;
        public List<RouteMasterDTO> RouteTargetList
        {
            get { return routeTargetList; }
            set { routeTargetList = value; }
        }
    }
}
