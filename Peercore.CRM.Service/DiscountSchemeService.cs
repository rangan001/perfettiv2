﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Model;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IDiscountScheme
    {
        DiscountSchemeAdapter discountSchemeAdapter = null;

        public List<SchemeHeaderDTO> GetAllSchemeHeader(ArgsDTO args)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetAllSchemeHeader(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SchemeHeaderDTO> GetAllSchemeHeaderForCopy()
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetAllSchemeHeader();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SchemeHeaderDTO GetSchemeHeaderById(ArgsDTO args, int schemeHeaderId)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetSchemeHeaderById(args,schemeHeaderId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SchemeHeaderDTO> GetSchemeHeaderByName(string name, bool isActive)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetSchemeHeaderByName(name, isActive);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateSchemeHeader(SchemeHeaderDTO schemeHeader)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.UpdateSchemeHeader(schemeHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertSchemeHeader(out int schemeHeaderId, SchemeHeaderDTO schemeHeader)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.InsertSchemeHeader(out schemeHeaderId,schemeHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SchemeHeaderSave(out int schemeHeaderId, SchemeHeaderDTO schemeHeader)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.SchemeHeaderSave(out schemeHeaderId, schemeHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateSchemeDetails(SchemeDetailsDTO schemeDetails)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.UpdateSchemeDetails(schemeDetails);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool InsertSchemeDetails(out int schemeDetailsId, SchemeDetailsDTO schemeDetails)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.InsertSchemeDetails(out schemeDetailsId, schemeDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SchemeDetailsDTO> GetSchemeDetailsBySchemeHeaderId(ArgsDTO args, int schemeHeaderId)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetSchemeDetailsBySchemeHeaderId(args,schemeHeaderId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OptionLevelDTO> GetOptionLevelByDetailsId(int detailsId)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetOptionLevelByDetailsId(detailsId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OptionLevelCombinationDTO> GetOptionLevelCombinationByOptionLevelId(int optionLevelId)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetOptionLevelCombinationByOptionLevelId(optionLevelId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<FreeProductDTO> GetAllFreeProducts(ArgsDTO args)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetAllFreeProducts(args);
            }
            catch (Exception)
            {
                throw;
            }
        }



        public bool IsSchemeHeaderNameExists(SchemeHeaderDTO schemeHeader)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.IsSchemeHeaderNameExists(schemeHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateSchemeHeaderIsActive(SchemeHeaderDTO schemeHeader)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.UpdateSchemeHeaderIsActive(schemeHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region - Division -

        public bool SaveDivision(out int divisionId, DivisionDTO divisionEntity) 
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.SaveDivision(out  divisionId,  divisionEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DivisionDTO> GetAllDivisions(ArgsDTO args)
        {
            discountSchemeAdapter = new DiscountSchemeAdapter();
            try
            {
                return discountSchemeAdapter.GetAllDivisions(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion


        #region "For 2nd Phase"

        public List<SchemeHeaderModel> GetAllDiscountSchemeHeader(ArgsModel args)
        {
            try
            {
                return SchemeHeaderBR.Instance.GetAllDiscountSchemeHeader(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveUpdateDiscountTerritory(string originator,
                                                       string scheme_header_id,
                                                       string territory_id,
                                                       string is_checked)
        {
            try
            {
                bool status = SchemeHeaderBR.Instance.SaveUpdateDiscountTerritory(originator,
                                                       scheme_header_id,
                                                       territory_id,
                                                       is_checked);
                return status;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
