﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using Peercore.CRM.Service.DTO.TransferComponents;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.ServiceModel;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IEndUser
    {
        #region - End User -

        public bool EndUserSave(EndUserDTO endUserDto)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.EndUserSave(endUserDto);
            }
            catch
            {
                throw;
            }
        }

        public EndUserDTO GetEndUser(ArgsDTO args)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUser(args);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserDTO> GetEndUsersByCode(string enduserCodes)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUsersByCode(enduserCodes);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserDTO> GetEndUsers(ArgsDTO args, string sStatus)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUsers(args, sStatus);
            }
            catch
            {
                throw;
            }
        }

        public int SetActiveStatus(ArgsDTO args, bool isActive)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.SetActiveStatus(args, isActive);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserDTO> GetAllEndUsers(ArgsDTO args)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetAllEndUsers(args); ;
            }
            catch
            {
                throw;
            }
        }

        public EndUserDTO IsEndUserExistForCustomer(ArgsDTO args)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.IsEndUserExistForCustomer(args);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserDTO> IsEndUserExistForOtherCustomers(ArgsDTO args)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.IsEndUserExistForOtherCustomers(args);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserPriceDTO> GetEndUserPricing(ArgsDTO args, string effectiveDate)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUserPricing(args, effectiveDate);
            }
            catch
            {
                throw;
            }
        }

        public List<EnduserEnquiryDTO> GetEnduserSalesWithAllProducts(ArgsDTO args, bool bIncludeProductsWithNoSales, bool bActiveOnly)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEnduserSalesWithAllProducts(args, bIncludeProductsWithNoSales, bActiveOnly);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserSalesDTO> GetEnduserSales(ArgsDTO args, bool bActiveOnly)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEnduserSales(args, bActiveOnly);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerDTO> GetEndUserCustomers(ArgsDTO args)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUserCustomers(args); ;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Transfer(EndUserDTO endUser, ArgsDTO args)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.Transfer(endUser, args);
            }
            catch
            {
                throw;
            }
        }

        public EndUserPriceDTO GetEndUserPrice(ArgsDTO args, DateTime effectiveDate, string catalogCode)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUserPrice(args, effectiveDate, catalogCode);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region - EndUserContactPerson -

        public bool SaveEndUserContact(ContactPersonDTO endUserContactPerson, ref int contactPersonId)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.SaveEndUserContact(endUserContactPerson, ref contactPersonId);
            }
            catch
            {
                throw;
            }
        }

        public ContactPersonDTO GetEndUserContactPerson(int iContactPersonID)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUserContactPerson(iContactPersonID);
            }
            catch
            {
                throw;
            }
        }

        public List<ContactPersonDTO> GetEndUserContactPersons(ArgsDTO args)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUserContactPersons(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveEndUserContactPersonImage(ContactPersonImageDTO contactPersonImage, ref int contactPersonImageId)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {

                return objCommonAdapter.SaveEndUserContactPersonImage(contactPersonImage, ref contactPersonImageId);
            }

            catch (Exception)
            {

                throw;
            }
        }

        public ContactPersonImageDTO GetEndUserContactPersonImage(int contactPersonId, string endUserCode, string directoryPath, string sessionId)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.GetEndUserContactPersonImage(contactPersonId, endUserCode, directoryPath, sessionId);
            }

            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region - price List -
        public bool SavePriceList(ArgsDTO args, DateTime dtmEffectiveDate, List<EndUserPriceDTO> listEndUserPrice)
        {
            EndUserAdapter objCommonAdapter = new EndUserAdapter();
            try
            {
                return objCommonAdapter.SavePriceList(args, dtmEffectiveDate, listEndUserPrice);
            }

            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
