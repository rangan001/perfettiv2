﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using Peercore.CRM.Service.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IKPIReport
    {
        public List<KPIReportEmailRecipientsModel> GetAllEmailRecipients(ArgsModel args)
        {
            try
            {
                List<KPIReportEmailRecipientsModel> objList = KPIReportBR.Instance.GetAllEmailRecipients(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        #region - KPIReports -

        public List<KPIReportModel> GetAllKPIs(ArgsModel args, int month, int year)
        {
            try
            {
                List<KPIReportModel> objList = KPIReportBR.Instance.GetAllKPIReports(args, month, year);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<KPIReportModel> GetAllKPIReportWeeklyTargetsByMonth(ArgsModel args, int month, int year)
        {
            try
            {
                List<KPIReportModel> objList = KPIReportBR.Instance.GetAllKPIReportWeeklyTargetsByMonth(args, month, year);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool UploadKPIReport(string fileName, string originator, string key, DateTime targetDate)
        {
            try
            {
                return KPIReportBR.Instance.UploadKPIReportExcel(fileName, originator, key, targetDate);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool UploadKPIReportTargetWeekly(string fileName, string originator, string key, DateTime targetDate)
        {
            try
            {
                return KPIReportBR.Instance.UploadKPIReportTargetWeekly(fileName, originator, key, targetDate);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region - KPIReportBrands -

        public List<KPIReportSettingsModel> GetKPIReportBrands(ArgsModel args)
        {
            try
            {
                List<KPIReportSettingsModel> objList = KPIReportBR.Instance.GetKPIReportBrands(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateKPIReportBrands(KPIReportSettingsModel brandsDetail)
        {
            try
            {
                bool isTrue = KPIReportBR.Instance.UpdateKPIReportBrands(brandsDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<KPIReportSettingsModel> KPIReportBrands()
        {
            try
            {
                List<KPIReportSettingsModel> objList = KPIReportBR.Instance.KPIReportBrands();

                return objList;
            }
            catch
            {
                throw;
            }
        }

        #endregion

        public bool UpdateKPIEmailRecipients(string emailRecipints)
        {
            try
            {
                bool isTrue = KPIReportBR.Instance.UpdateKPIEmailRecipients(emailRecipints);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteKPIEmailRecipient(string emailRecipint)
        {
            try
            {
                bool isTrue = KPIReportBR.Instance.DeleteKPIEmailRecipient(emailRecipint);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<KPIReportBrandsModel> GetAllKPIReportsBrands()
        {
            try
            {
                List<KPIReportBrandsModel> objList = KPIReportBR.Instance.GetAllKPIReportsBrands();

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<KPIReportBrandItemModel> GetKPIBrandItemsByBrandId(ArgsModel args, int kpiBrandId)
        {
            try
            {
                List<KPIReportBrandItemModel> objList = KPIReportBR.Instance.GetKPIBrandItemsByBrandId(args, kpiBrandId);
                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool AssignKPIBrandItems(string originator, string kpi_brand_id, string product_id, string is_checked)
        {
            try
            {
                bool isTrue = KPIReportBR.Instance.AssignKPIBrandItems(originator,
                                        kpi_brand_id,
                                        product_id,
                                        is_checked);

                return isTrue;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateKPIBrandDisplayName(string kpi_brand_id, string displayName)
        {
            try
            {
                bool isTrue = KPIReportBR.Instance.UpdateKPIBrandDisplayName(kpi_brand_id,
                                        displayName);

                return isTrue;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public KPIReportBrandsModel GetKPIReportBrandByBrandId(string kpi_brand_id)
        {
            try
            {
                return KPIReportBR.Instance.GetKPIReportBrandByBrandId(kpi_brand_id); 
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
