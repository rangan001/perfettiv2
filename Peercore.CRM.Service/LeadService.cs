﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Entities;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ILead
    {
        LeadAdapter leadAdapter = null;
        #region Lead

        public bool SaveLead(LeadDTO lead, ArgsDTO args, out int leadid)
        {
            leadAdapter = new LeadAdapter();

            try
            {
                return leadAdapter.Save(lead, args, out leadid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public LeadDTO GetLead(string sLeadID)
        {
            leadAdapter = new LeadAdapter();
            try
            {
                return leadAdapter.GetLead(sLeadID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string UpdatePipelineStageQuery(int iLeadID, int iPipelineStageID)
        {
            leadAdapter = new LeadAdapter();
            try
            {
                return leadAdapter.UpdatePipelineStageQuery(iLeadID, iPipelineStageID);
            }
            catch
            {
                throw;
            }

        }

        public int GetTotalCustomerCount(ArgsDTO args)
        {
            leadAdapter = new LeadAdapter();
            try
            {
                return leadAdapter.GetTotalCustomerCount(args);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteLead(LeadDTO lead, ActivityDTO activity)
        {
            leadAdapter = new LeadAdapter();
            try
            {
                return leadAdapter.DeleteLead(lead, activity);
            }
            catch (Exception)
            {
                throw;
            }
        } 

        public bool ConvertLeadToCustomer(List<string> sSqls)
        {
            leadAdapter = new LeadAdapter();
            try
            {
                return leadAdapter.ConvertLeadToCustomer(sSqls);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ActivateLead(int leadId, ActivityDTO activity)
        {
            leadAdapter = new LeadAdapter();

            try
            {
                return leadAdapter.ActivateLead(leadId, activity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadDTO> GetLeadMiniViewCollection(ArgsDTO args)
        {
            leadAdapter = new LeadAdapter();

            try
            {
                return leadAdapter.GetLeadMiniViewCollection(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region - ImportMail -
        public List<EmailDTO> GetLeadEmails(ArgsDTO args, string directoryPath, string sessionId)
        {
            EmailAdapter emailAdapter = new EmailAdapter();
            try
            {
                return emailAdapter.GetLeadEmails(args, directoryPath, sessionId);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        #endregion

        #region - Contact Person Image -
        public ContactPersonImageDTO GetLeadContactPersonImage(int contactPersonId, int leadId, string directoryPath, string sessionId)
        {
            leadAdapter = new LeadAdapter();

            try
            {
                return leadAdapter.GetContactPersonImage(contactPersonId, leadId, directoryPath, sessionId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        //#region CheckList
        //public bool Save(List<CheckListDTO> checklist)
        //{
        //    leadAdapter = new LeadAdapter();

        //    try
        //    {
        //        return leadAdapter.Save(checklist);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public List<CheckListDTO> GetChecklist(int iLeadStageID)
        //{
        //    leadAdapter = new LeadAdapter();

        //    try
        //    {
        //        return leadAdapter.GetChecklist(iLeadStageID);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        //#endregion
    }
}
