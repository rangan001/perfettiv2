﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ILeadStage
    {
        LeadStageAdapter LeadStageAdapter = null;

        #region - Lead Stage -
        public List<LeadStageDTO> GetLeadStages(ArgsDTO args)
        {
            LeadStageAdapter = new LeadStageAdapter();

            try
            {
                return LeadStageAdapter.GetLeadStages(args);
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        #endregion

        
        public List<LeadStageDTO> GetLeadStageChartData(ArgsDTO args)
        {
            LeadStageAdapter = new LeadStageAdapter();
            List<LeadStageDTO> lst = new List<LeadStageDTO>();
            try
            {
                return LeadStageAdapter.GetLeadStageChartData(args);
            }
            catch (Exception)
            {
                return lst;
            }
        }

        /*
        public bool SaveLeadStage(List<LeadStageDTO> lstStages, string defaultDeptID)
        {
            LeadStageAdapter = new LeadStageAdapter();

            try
            {
                return LeadStageAdapter.Save(lstStages, defaultDeptID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int SaveLeadStageList(List<LeadStageDTO> leadStages, List<LeadStageDTO> deletedStages, string defaultDeptID)
        {
            LeadStageAdapter = new LeadStageAdapter();
            try
            {
                return LeadStageAdapter.Save(leadStages, deletedStages, defaultDeptID);
            }
            catch
            {
                throw;
            }
        }

        public bool CanDeleteLeadStage(int leadStageId)
        {
            LeadStageAdapter = new LeadStageAdapter();
            try
            {
                return LeadStageAdapter.CanDeleteLeadStage(leadStageId);
            }
            catch
            {
                throw;
            }
        }

        public LeadStageDTO GetLeadStage(int SourceId)
        {
            LeadStageAdapter = new LeadStageAdapter();
            try
            {
                return LeadStageAdapter.GetLeadStage(SourceId);
            }
            catch
            {
                throw;
            }
        }
        */
    }
}
