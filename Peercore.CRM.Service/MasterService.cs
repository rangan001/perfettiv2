﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Service.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IMaster
    {
        #region "Region"

        public List<RegionModel> GetAllRegions(ArgsModel args)
        {
            try
            {
                List<RegionModel> regionList = RegionBR.Instance.GetAllRegions(args);

                return regionList;
            }
            catch
            {
                throw;
            }
        }

        public List<RegionModel> GetAllRegionsByRegionCode(ArgsModel args, string RegionCode)
        {
            try
            {
                List<RegionModel> regionList = RegionBR.Instance.GetAllRegionsByRegionCode(args, RegionCode);

                return regionList;
            }
            catch
            {
                throw;
            }
        }

        public List<RegionModel> GetAllRegionsByRegionName(ArgsModel args, string RegionName)
        {
            try
            {
                List<RegionModel> regionList = RegionBR.Instance.GetAllRegionsByRegionName(args, RegionName);

                return regionList;
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteRegion(string region_id)
        {
            bool status = false;

            try
            {
                if (RegionBR.Instance.DeleteRegion(region_id))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        public bool SaveRegion(string comp_id, string region_id, string region_code, string region_name)
        {
            bool status = false;

            try
            {
                if (RegionBR.Instance.SaveRegion(comp_id, region_id, region_code, region_name))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        #endregion

        #region "RSM"

        public List<RsmModel> GetAllRSM(ArgsModel args)
        {
            try
            {
                List<RsmModel> regionList = RegionBR.Instance.GetAllRsm(args);

                return regionList;
            }
            catch
            {
                throw;
            }
        }

        public List<RsmModel> GetAllRsmByRsmCode(ArgsModel args, string RsmCode)
        {
            try
            {
                List<RsmModel> rsmList = RegionBR.Instance.GetAllRsmByRsmCode(args, RsmCode);

                return rsmList;
            }
            catch
            {
                throw;
            }
        }
        
        public List<RsmModel> GetRsmDetailsByRegionId(ArgsModel args, string RegionId)
        {
            try
            {
                List<RsmModel> rsmList = RegionBR.Instance.GetRsmDetailsByRegionId(args, RegionId);

                return rsmList;
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteRsmByRsmId(string rsm_id)
        {
            bool status = false;

            try
            {
                if (RegionBR.Instance.DeleteRsmByRsmId(rsm_id))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        public bool SaveRsm(string region_id, 
                            string rsm_id, 
                            string rsm_code,
                            string rsm_name,
                            string rsm_username,
                            string rsm_tel1,
                            string rsm_email,
                            string created_by)
        {
            bool status = false;

            try
            {
                if (RegionBR.Instance.SaveRsm(region_id,
                            rsm_id,
                            rsm_code,
                            rsm_name,
                            rsm_username,
                            rsm_tel1,
                            rsm_email,
                            created_by))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        public bool UpdateRsmPassword(string rsmId, string password)
        {
            bool status = false;

            try
            {
                if (RegionBR.Instance.UpdateRsmPassword(rsmId, password))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        #endregion

        #region "Area"

        public List<AreaModel> GetAllAreaNew(ArgsModel args)
        {
            try
            {
                List<AreaModel> regionList = AreaBR.Instance.GetAllAreaNew(args);

                return regionList;
            }
            catch
            {
                throw;
            }
        }

        public List<AreaModel> GetAllAreaByAreaCode(ArgsModel args, string AreaCode)
        {
            try
            {
                List<AreaModel> regionList = AreaBR.Instance.GetAllAreaByAreaCode(args, AreaCode);
                return regionList;
            }
            catch
            {
                throw;
            }
        }
        
        public List<AreaModel> GetAllAreaByAreaName(ArgsModel args, string AreaName)
        {
            try
            {
                List<AreaModel> regionList = AreaBR.Instance.GetAllAreaByAreaName(args, AreaName);
                return regionList;
            }
            catch
            {
                throw;
            }
        }

        public List<AreaModel> GetAllAreaByRegionId(ArgsModel args, string RegionId)
        {
            try
            {
                List<AreaModel> regionList = AreaBR.Instance.GetAllAreaByRegionId(args, RegionId);
                return regionList;
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteAreaFromRegionByAreaId(int areaId)
        {
            try
            {
                bool status = AreaBR.Instance.DeleteAreaFromRegionByAreaId(areaId);
                return status;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertUpdateArea(string region_id, string area_id, string area_code, string area_name)
        {
            bool status = false;

            try
            {
                if (AreaBR.Instance.InsertUpdateArea(region_id, area_id, area_code, area_name))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        #endregion

        #region "Territory"

        public List<TerritoryModel> GetAllTerritory(ArgsModel args)
        {
            try
            {
                List<TerritoryModel> territoryList = TerritoryBR.Instance.GetAllTerritory(args);

                return territoryList;
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoryByOriginator(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                List<TerritoryModel> territoryList = TerritoryBR.Instance.GetAllTerritoryByOriginator(args, OriginatorType, Originator);

                return territoryList;
            }
            catch
            {
                throw;
            }
        }
        
        public List<TerritoryModel> GetAllTerritoryByDistributerId(ArgsModel args, string DistributerId)
        {
            try
            {
                List<TerritoryModel> territoryList = TerritoryBR.Instance.GetAllTerritoryByDistributerId(args, DistributerId);

                return territoryList;
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoryByTerritoryCode(ArgsModel args, string TerritoryCode)
        {
            try
            {
                List<TerritoryModel> territoryList = TerritoryBR.Instance.GetAllTerritoryByTerritoryCode(args, TerritoryCode);

                return territoryList;
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoryByTerritoryName(ArgsModel args, string TerritoryName)
        {
            try
            {
                List<TerritoryModel> territoryList = TerritoryBR.Instance.GetAllTerritoryByTerritoryName(args, TerritoryName);

                return territoryList;
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoryByAreaId(ArgsModel args, string AreaId)
        {
            try
            {
                List<TerritoryModel> territoryList = TerritoryBR.Instance.GetAllTerritoryByAreaId(args, AreaId);

                return territoryList;
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteTerritory(string territory_id)
        {
            try
            {
                return TerritoryBR.Instance.DeleteTerritory(territory_id);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteTerritoryFromAreaByTerritoryId(string territory_id)
        {
            try
            {
                return TerritoryBR.Instance.DeleteTerritoryFromAreaByTerritoryId(territory_id);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveTerritory(string area_id, string territory_id, string territory_code, string territory_name, string created_by)
        {
            try
            {
                return TerritoryBR.Instance.SaveTerritory(area_id,
                    territory_id,
                    territory_code,
                    territory_name,
                    created_by);
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoryByDiscountScheme(ArgsModel args, string ShemeHeaderId)
        {
            try
            {
                List<TerritoryModel> territoryList = TerritoryBR.Instance.GetAllTerritoryByDiscountScheme(args, ShemeHeaderId);

                return territoryList;
            }
            catch
            {
                throw;
            }
        }

        public bool IsTerritoryAssigned(string territoryId, out string RepCode)
        {
            try
            {
                return TerritoryBR.Instance.IsTerritoryAssigned(territoryId, out RepCode);
            }
            catch
            {
                throw;
            }
        }
        
        public bool DeleteConfirmSRFromTerritory(string territoryId, string repId)
        {
            try
            {
                return TerritoryBR.Instance.DeleteConfirmSRFromTerritory(territoryId, repId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateTerritoryCanAddOutlet(string territory_id, bool can_add_outlet)
        {
            bool status = false;

            try
            {
                status = TerritoryBR.Instance.UpdateTerritoryCanAddOutlet(territory_id, can_add_outlet);
            }
            catch (Exception)
            {
                status = false;
            }

            return status;
        }

        public bool UpdateTerritoryGeoFence(string territory_id, bool is_geofence)
        {
            bool status = false;

            try
            {
                status = TerritoryBR.Instance.UpdateTerritoryGeoFence(territory_id, is_geofence);
            }
            catch (Exception)
            {
                status = false;
            }

            return status;
        }

        #endregion

        #region "Asm"

        public List<AsmModel> GetAllAsm(ArgsModel args)
        {
            try
            {
                List<AsmModel> regionList = AsmBR.Instance.GetAllAsm(args);

                return regionList;
            }
            catch
            {
                throw;
            }
        }

        public List<AsmModel> GetAllAsmByAreaId(ArgsModel args, string AreaId)
        {
            try
            {
                List<AsmModel> regionList = AsmBR.Instance.GetAllAsmByAreaId(args, AreaId);

                return regionList;
            }
            catch
            {
                throw;
            }
        }
        
        public List<OriginatorImeiModel> GetAllOriginatorImeiByAsmId(ArgsModel args, string asmId)
        {
            try
            {
                List<OriginatorImeiModel> objList = AsmBR.Instance.GetAllOriginatorImeiByAsmId(args, asmId);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAsm(string area_id,
                           string asm_id,
                           string asm_code,
                           string asm_name,
                           string asm_username,
                           string asm_tel1,
                           string asm_email,
                           string created_by)
        {
            bool status = false;

            try
            {
                if (AsmBR.Instance.InsertUpdateArea(area_id,
                            asm_id,
                            asm_code,
                            asm_name,
                            asm_username,
                            asm_tel1,
                            asm_email,
                            created_by))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        public bool DeleteAsmByAsmId(string asm_id)
        {
            bool status = false;

            try
            {
                if (AsmBR.Instance.DeleteAsmByAsmId(asm_id))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }
        
        public bool DeleteIMEIByAsmId(string asm_id, string emei_no)
        {
            bool status = false;

            try
            {
                if (AsmBR.Instance.DeleteIMEIByAsmId(asm_id, emei_no))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }
        
        public bool UpdateOriginatorIsIMEIByAsmId(string asmId, bool isImei)
        {
            bool status = false;

            try
            {
                if (AsmBR.Instance.UpdateOriginatorIsIMEIByAsmId(asmId, isImei))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }
        
        public bool UpdateAsmPassword(string asmId, string password)
        {
            bool status = false;

            try
            {
                if (AsmBR.Instance.UpdateAsmPassword(asmId, password))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        #endregion

        #region "Route Master"
        public List<RouteMasterModel> GetAllRoutesMaster(ArgsModel args)
        {
            try
            {
                List<RouteMasterModel> objList = RouteMasterBR.Instance.GetAllRouteMaster(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<RouteMasterModel> GetAllRouteMasterByTerritoryId(ArgsModel args, string TerritoryId)
        {
            try
            {
                List<RouteMasterModel> objList = RouteMasterBR.Instance.GetAllRouteMasterByTerritoryId(args, TerritoryId);

                return objList;
            }
            catch
            {
                throw;
            }
        }
        
        public RouteMasterModel GetRouteByRouteCode(string RouteCode)
        {
            try
            {
                return RouteMasterBR.Instance.GetRouteByRouteCode(RouteCode);
            }
            catch
            {
                throw;
            }
        }
        
        public RouteMasterModel GetRouteByRouteName(string RoutName)
        {
            try
            {
                return RouteMasterBR.Instance.GetRouteByRouteName(RoutName);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteRouteFromTerritoryByRouteId(string routeId)
        {
            try
            {
                bool status = RouteMasterBR.Instance.DeleteRouteFromTerritoryByRouteId(routeId);
                return status;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertUpdateRouteMaster(RouteMasterModel routeMaster)
        {
            bool status = false;

            try
            {
                if (RouteMasterBR.Instance.InsertUpdateRouteMaster(routeMaster))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }
        
        public bool DeleteRouteMasterByRouteId(string routeId, string DeletedBy)
        {
            bool status = false;

            try
            {
                if (RouteMasterBR.Instance.DeleteRouteMasterByRouteId(routeId, DeletedBy))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        #endregion

        #region "Sales Rep"

        public List<SRModel> GetAllSalesRepsByTerritoryId(ArgsModel args, string TerritoryId)
        {
            try
            {
                List<SRModel> objList = RepsBR.Instance.GetAllSalesRepsByTerritoryId(args, TerritoryId);

                return objList;
            }
            catch
            {
                throw;
            }
        } 
        
        public List<SRModel> GetAllSalesRepsByAreaId(ArgsModel args, string AreaId)
        {
            try
            {
                List<SRModel> objList = RepsBR.Instance.GetAllSalesRepsByAreaId(args, AreaId);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<SRModel> GetAllSalesReps(ArgsModel args)
        {
            try
            {
                List<SRModel> objList = RepsBR.Instance.GetAllSalesReps(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<SRModel> GetAllSalesRepsByOriginator(ArgsModel args, string Originator)
        {
            try
            {
                List<SRModel> objList = RepsBR.Instance.GetAllSalesRepsByOriginator(args, Originator);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<SRModel> GetAllSalesRepsForRouteAssign(ArgsModel args)
        {
            try
            {
                List<SRModel> objList = RepsBR.Instance.GetAllSalesRepsForRouteAssign(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }
        
        public List<SRModel> GetAllSRTerritoryByOriginator(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                List<SRModel> objList = RepsBR.Instance.GetAllSRTerritoryByOriginator(args, OriginatorType, Originator);

                return objList;
            }
            catch
            {
                throw;
            }
        }
        
        public List<DistributerTerritoryModel> GetAllTerritoriesForDistributer(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                List<DistributerTerritoryModel> objList = TerritoryBR.Instance.GetAllTerritoriesForDistributer(args, OriginatorType, Originator);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<SRModel> GetAllSRDetails()
        {
            try
            {
                return RepsBR.Instance.GetAllSRDetails();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SRModel GetSRDetailsByOriginator(string originator)
        {
            try
            {
                return RepsBR.Instance.GetSRDetailsByOriginator(originator);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveSR(SRModel srEntity)
        {
            try
            {
                return RepsBR.Instance.SaveSR(srEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SalesRepSalesTypeModel> GetAllSalesRepWithSalesType(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                List<SalesRepSalesTypeModel> objList = RepsBR.Instance.GetAllSalesRepWithSalesType(args, OriginatorType, Originator);

                return objList;
            }
            catch
            {
                throw;
            }
        }
        
        public List<SalesRepSyncModel> GetAllSalesRepSyncData(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                List<SalesRepSyncModel> objList = SyncBR.Instance.GetAllSalesRepSyncData(args, OriginatorType, Originator);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateSRSalesType(string RepId, string SalesType, string Originator)
        {
            try
            {
                return RepsBR.Instance.UpdateSRSalesType(RepId, SalesType, Originator);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region "Distributer"

        public List<DistributerModel> GetAllDistributerByOriginatorAndOriginatorType(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                return DistributorBR.Instance.GetAllDistributerByOriginatorAndOriginatorType(args, OriginatorType, Originator);
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
