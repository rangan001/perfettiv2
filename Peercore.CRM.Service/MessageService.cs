﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using System.ServiceModel;
namespace Peercore.CRM.Service
{
    public partial class CRMService : IMessage
    {
        public bool SaveMessages(MessagesDTO message, List<OriginatorDTO> chldOriginatorList, out int newrecordid)
        {
            MessageAdapter objMessageAdapter = new MessageAdapter();

            try
            {
                return objMessageAdapter.SaveMessages(message, chldOriginatorList, out newrecordid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MessagesDTO> GetMessageByOriginator(string originator)
        {
            MessageAdapter objMessageAdapter = new MessageAdapter();

            try
            {
                return objMessageAdapter.GetMessageByOriginator(originator);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public MessagesDTO GetMessageCount(string originator, string datestring)
        {
            MessageAdapter objMessageAdapter = new MessageAdapter();

            try
            {
                return objMessageAdapter.GetMessageCount(originator, datestring);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public MessagesDTO GetMessage(int messageId)
        {
            MessageAdapter objMessageAdapter = new MessageAdapter();

            try
            {
                return objMessageAdapter.GetMessage(messageId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteMessage(int messageId)
        {
            MessageAdapter objMessageAdapter = new MessageAdapter();

            try
            {
                return objMessageAdapter.DeleteMessage(messageId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UpdateMessageRep(string username)
        {
            MessageAdapter objMessageAdapter = new MessageAdapter();
            try
            {
                return objMessageAdapter.UpdateMessageRep(username);
            }
            catch (Exception)
            {

                throw;
            }
        }

        
    }
}
