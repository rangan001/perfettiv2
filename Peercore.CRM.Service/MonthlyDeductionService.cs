﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using Peercore.CRM.Service.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IMonthlyDeduction
    {
        #region - Monthly Deduction -

        public bool DeleteDeduction(int deductId, string originator)
        {
            try
            {
                bool isTrue = MonthlyDeductionBR.Instance.DeleteDeduction(deductId, originator);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool excelDeductionData(string fileName, string originator, string logPath)
        {
            try
            {
                return MonthlyDeductionBR.Instance.UploadDeductionExcelfile(fileName, originator, logPath);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MonthlyDeductionModel> GetAllDeductions(ArgsModel args)
        {
            try
            {
                List<MonthlyDeductionModel> objList = MonthlyDeductionBR.Instance.GetAllDeductions(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool IsRepCodeAvailable(string repcode)
        {
            try
            {
                bool isTrue = MonthlyDeductionBR.Instance.IsRepCodeAvailable(repcode);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveDeduction(MonthlyDeductionModel deductionDetail)
        {
            try
            {
                bool isTrue = MonthlyDeductionBR.Instance.SaveDeduction(deductionDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateDeduction(MonthlyDeductionModel deductionDetail)
        {
            try
            {
                bool isTrue = MonthlyDeductionBR.Instance.UpdateDeduction(deductionDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
