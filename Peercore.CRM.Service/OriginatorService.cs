﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IOriginator
    {
        OriginatorAdapter OriginatorAdapter = null;

        #region Reps
        public List<OriginatorDTO> GetReps()
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetReps();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        public string GetChildOriginators(string sOriginator, bool managerMode = false)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetChildOriginators(sOriginator, managerMode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<string> GetChildReps(string sOriginator, bool managerMode, ref string sWhereCls, string childOriginators = null)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetChildReps(sOriginator, managerMode, ref sWhereCls, childOriginators);
            }
            catch
            {
                throw;
            }
        }

        public OriginatorDTO GetLoginDetails(string sOriginator, string sPassword)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetLoginDetails(sOriginator, sPassword);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public OriginatorDTO GetOriginator(string originator)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetOriginator(originator);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetRepCode(string originator)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetRepCode(originator);
            }
            catch
            {
                throw;
            }
        }

        public string GetBDMString()
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return "OriginatorAdapter.GetBDMString()";
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<KeyValuePair<string, string>> GetChildRepsList(string childOriginators)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetChildRepsList(childOriginators);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetAllRepCodes(string sOriginator)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetAllRepCodes(sOriginator);
            }
            catch
            {
                throw;
            }
        }

        public List<StateDTO> GetDistinctRepStates()
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetDistinctRepStates();
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetRepGroupOriginators(string sOriginator)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetRepGroupOriginators(sOriginator);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetCRMReps()
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetCRMReps();
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetChildOriginatorsList(ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetChildOriginatorsList(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetRepForKeyAccounts(string distributor)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetRepForKeyAccounts(distributor);
            }
            catch
            {
                return new List<OriginatorDTO>();
            }
        }

        public List<OriginatorDTO> GetOriginatorsByCatergory(ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetOriginatorsByCatergory(args);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public List<OriginatorDTO> GetDistributersByASMOriginator(ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetDistributersByASMOriginator(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetAllSRByOriginator(ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetAllSRByOriginator(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetAllDistributorList(ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetAllDistributorList(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetAllOriginatorsByCatergory(ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetAllOriginatorsByCatergory(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetDistributorsByCatergoryWithNameAndCode(ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetDistributorsByCatergoryWithNameAndCode(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetRepsByOriginator_For_Route_Assign(ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetRepsByOriginator_For_Route_Assign(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetRepsByOriginator(string rep, string username)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetRepsByOriginator(rep, username);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetRepsByASEOriginator(string ASE)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetRepsByASEOriginator(ASE);
            }
            catch
            {
                throw;
            }
        }


        public List<OriginatorDTO> GetDistributorsByLikeName(string username)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetDistributorsByLikeName(username);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEMEIDTO> GetAllOriginatorEMEINos(string originator)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetAllOriginatorEMEINos(originator);
            }
            catch
            {
                throw;
            }
        }

        //public List<RepDTO> GetRepTargetsByDistributor(string username)
        //{
        //    OriginatorAdapter = new OriginatorAdapter();
        //    try
        //    {
        //        return OriginatorAdapter.GetRepTargetsByDistributor(username);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        // public List<TargetInfoDTO> GetRepMonthleyTargetsByDistributor(string username, string detailType, ArgsDTO args)
        public SalesInfoDetailViewStateDTO GetRepMonthleyTargetsByDistributor(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetRepMonthleyTargetsByDistributor(busSalesEnqSrc, args);
            }
            catch
            {
                throw;
            }
        }

        public SalesInfoDetailViewStateDTO GetTodaySalesDrilldown(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetTodaySalesDrilldown(busSalesEnqSrc, args);
            }
            catch
            {
                throw;
            }
        }
        
        /*                                   
       public List<OriginatorDTO> GetFilterChildOriginatorsList(string filterByUserName, ArgsDTO args)
       {
           OriginatorAdapter = new OriginatorAdapter();

           try
           {
               return OriginatorAdapter.GetFilterChildOriginatorsList(filterByUserName, args);
           }
           catch (Exception)
           {

               throw;
           }
       }
       */

        //public List<OriginatorDTO> GetChildOriginatorsListForMessage(ArgsDTO args)
        //{
        //    OriginatorAdapter = new OriginatorAdapter();

        //    try
        //    {
        //        return OriginatorAdapter.GetChildOriginatorsListForMessage(args);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}


        public List<OriginatorDTO> GetParentOriginators(string sOriginator)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetParentOriginators(sOriginator);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OriginatorDTO> GetLevelThreeParentOriginators(string sOriginator)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetLevelThreeParentOriginators(sOriginator);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetUserNameByRepCode(string sRepCode)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.GetUserNameByRepCode(sRepCode);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdatePassword(string originator, string password)
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.UpdatePassword(originator, password);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateMobilePin()
        {
            OriginatorAdapter = new OriginatorAdapter();
            try
            {
                return OriginatorAdapter.UpdateMobilePin();
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateOriginatorTFAAuthenticationSettings(string originator, string tfaPin, bool isAuthenticated)
        {
            bool status = false;

            try
            {
                if (OriginatorBR.Instance.UpdateOriginatorTFAAuthenticationSettings(originator, tfaPin, isAuthenticated))
                {
                    status = true;
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        #region "Report"

        public List<OriginatorDTO> GetASEsByDateForReport(ArgsDTO args, DateTime startDate, DateTime endDate)
        {
            OriginatorAdapter = new OriginatorAdapter();

            try
            {
                return OriginatorAdapter.GetASEsByDateForReport(args, startDate, endDate);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
