﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using Peercore.CRM.Service.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IPayments
    {
        public List<PaymentModel> GetAllPayments(ArgsModel args)
        {
            try
            {
                List<PaymentModel> objList = MonthlyBillPayemntsBR.Instance.GetAllPayments(args);
                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool DeletePayment(int paymentId, string originator)
        {
            try
            {
                bool isTrue = MonthlyBillPayemntsBR.Instance.DeletePayment(paymentId, originator);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdatePayment(PaymentModel paymentDetail)
        {
            try
            {
                bool isTrue = MonthlyBillPayemntsBR.Instance.UpdatePayment(paymentDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SavePayment(PaymentModel paymentDetail)
        {
            try
            {
                bool isTrue = MonthlyBillPayemntsBR.Instance.SavePayment(paymentDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UploadPayments(string fileName, string originator)
        {
            try
            {
                return MonthlyBillPayemntsBR.Instance.UploadPaymentExcel(fileName, originator);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
