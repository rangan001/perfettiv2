﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Service.DTO;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IPipelineStage
    {
        PipelineStageAdapter PipelineStageAdapter = new PipelineStageAdapter();


        public List<PipelineStageDTO> GetPipelineStage(ArgsDTO args)
        {
            try
            {
                return PipelineStageAdapter.GetPipelineStage(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PipelineStageDTO> GetPipelineChart(ArgsDTO args)
        {
            try
            {
                return PipelineStageAdapter.GetPipelineChart(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SavePipelineStage(List<PipelineStageDTO> pipelineStageList, List<int> deletedStageList, ArgsDTO args)
        {
            try
            {
                return PipelineStageAdapter.SavePipelineStage(pipelineStageList, deletedStageList, args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool PipelineStageCanDelete(int pipelineStageId)
        {
            try
            {
                return PipelineStageAdapter.PipelineStageCanDelete(pipelineStageId);
            }
            catch
            {
                throw;
            }
        }

        /*
        public List<PipelineStageGraphDTO> GetPipelineStageGraph(string sOriginator, bool IsCustomerPipeline, string childOriginators, string defaultDeptID)
        {
            try
            {
                return PipelineStageAdapter.GetPipelineStageGraph(sOriginator, IsCustomerPipeline, childOriginators, defaultDeptID);
            }
            catch (Exception)
            {

                throw;
            }

        }
        */
    }
}
