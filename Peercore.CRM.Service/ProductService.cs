﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using Peercore.CRM.Service.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IProduct
    {

        #region "Product Territory"

        public ProductModel GetProductByProductCode(string productCode)
        {
            try
            {
                ProductModel product = ProductBR.Instance.GetProductByProductCode(productCode);

                return product;
            }
            catch
            {
                throw;
            }
        }

        public List<ProductModel> GetAllProducts(ArgsModel args)
        {
            try
            {
                List<ProductModel> objList = ProductBR.Instance.GetAllProducts(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<ProductModel> GetAllProductsByTerritoryId(ArgsModel args, string TerritoryId)
        {
            try
            {
                List<ProductModel> objList = ProductBR.Instance.GetAllProductsByTerritoryId(args, TerritoryId);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoriesByProductId(ArgsModel args, string ProductId)
        {
            try
            {
                List<TerritoryModel> objList = ProductBR.Instance.GetAllTerritoriesByProductId(args, ProductId);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveUpdateTerritoryProducts(string originator,
                                                       string territory_id,
                                                       string product_id,
                                                       string is_checked)
        {
            try
            {
                bool status = ProductBR.Instance.SaveUpdateTerritoryProducts(originator,
                                                       territory_id,
                                                       product_id,
                                                       is_checked);
                return status;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
