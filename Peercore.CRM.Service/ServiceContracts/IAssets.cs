﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.Collections.ObjectModel;
using Peercore.CRM.Model;
using System.Data;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Assets")]
    public interface IAssets
    {
        #region - Asset Type - 

        [OperationContract]
        List<AssetTypeModel> GetAllAssetTypes(ArgsModel args);

        [OperationContract]
        bool SaveAssetType(AssetTypeModel assetTypeDetail);

        [OperationContract]
        bool UpdateAssetType(int assetTypeId, string typeName, string description, string ModifiedBy);

        [OperationContract]
        bool DeleteAssetType(int assetTypeId, string originator);

        [OperationContract]
        bool IsAssetTypeAvailable(string assetType);

        #endregion

        #region - Asset Master - 

        [OperationContract]
        List<AssetModel> GetAllAssets(ArgsModel args);

        [OperationContract]
        bool SaveAsset(AssetModel assetDetail);

        [OperationContract]
        bool UpdateAsset(AssetModel assetDetail);

        [OperationContract]
        bool DeleteAsset(int assetId, string originator);

        [OperationContract]
        bool excelData(string fileName, string originator, string logPath);

        [OperationContract]
        bool IsAssetCodeAvailable(string assetCode);

        #endregion

        #region - Asset Assign -

        [OperationContract]
        List<AssetAssignModel> GetAllAssignAssets(ArgsModel args, string Token);

        [OperationContract]
        bool SaveAssignAsset(AssetAssignModel assignDetail);

        [OperationContract]
        bool UpdateAssignAsset(AssetAssignModel assignDetail);

        [OperationContract]
        bool DeleteAssignAsset(int assignId, string originator);

        [OperationContract]
        bool UploadAssetAssignData(string fileName, string originator, string logPath, bool IsCustomer);

        [OperationContract]
        List<OriginatorModel> GetUserCodesByUserType(string userType);

        [OperationContract]
        List<AssetModel> GetAssetsCodesByAssetType(string assetType);

        [OperationContract]
        List<AssetAssignModel> GetAllAssignAssetsByUserCode(string userCode);

        [OperationContract]
        bool SaveAssignAssetList(string Originator, List<AssetAssignModel> data);

        [OperationContract]
        string LoadCustomerName(string custCode);

        [OperationContract]
        bool IsAssetAvailable(string assetCode, string cusCode);

        [OperationContract]
        List<AssetAssignModel> GetAllAssignAssetsByAssetCode(string assetCode);

        #endregion

        [OperationContract]
        bool UploadAssetExcel(string fileName, string originator, string logPath);

        [OperationContract]
        bool IsLostOrDamaged(string assetCode, string cusCode);

        [OperationContract]
        List<AssetStatusModel> GetAssetStatus();

        [OperationContract]
        List<AssetAssignModel> GetAllAssignAssetsByAssetId(int assetId);
    }
}
