﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Entities;
using System.Data;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Common")]
    public interface ICommon
    {
        [OperationContract]
        EmailAndAttachmentsDTO GetEmailbyId(int emailId, string directoryPath, string sessionId);

        #region - report -
        [OperationContract]
        List<RepWizeActivityAnalysisDTO> GetRepWideActivity(DateTime FromDate,
            DateTime ToDate,
            List<LeadStageDTO> leadStageList,
            ArgsDTO args,
            List<OriginatorDTO> OriginatorList);


        /*
        [OperationContract]
        List<LeadCustomerDTO> GetCombinedCollectionCountByState(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "");

        [OperationContract]
        List<LeadCustomerDTO> GetCombinedCollectionByState(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "");

        [OperationContract]
        List<LeadCustomerDTO> GetLeadCustomerCountByRep(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "");

        [OperationContract]
        List<LeadCustomerDTO> GetLeadCustomerCollectionByRep(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "");

        //[OperationContract]
        //List<CustomerActivitiesDTO> GetActivityCountByType(ArgsDTO args,
        //    string originator,
        //    string sFromDate = "",
        //    string sEndDate = "",
        //    string activityStatus = "");

        //[OperationContract]
        //List<CustomerActivitiesDTO> GetAllActivitiesByType(ArgsDTO args,
        //    string originator,
        //    string sFromDate = "",
        //    string sEndDate = "",
        //    string activityStatus = "");

        [OperationContract]
        List<CallCycleGraphDTO> GetCallCycleList(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "");

        [OperationContract]
        List<CallCycleGraphDTO> GetCallCycleCountList(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "");

        

        [OperationContract]
        List<BDMWideServiceCallAnalysisDTO> GetCustomerServiceCallActivities(ArgsDTO args,
            DateTime? FromDate ,
            DateTime? ToDate ,
            bool IsAllCustomers,
            bool IsNotServiceCalledCustomers,
            string activityStatus = "",
            string filterText = "");

        [OperationContract]
        List<LeadDTO> GetNewCustomersForSales(ArgsDTO args, 
            int year, 
            int month);

        [OperationContract]
        List<BDMWideSalesAnalysisDTO> GeActualLiters(ArgsDTO args);

        [OperationContract]
        List<CustomerOpportunitiesDTO> GetCustomerOpportunities(ArgsDTO args);

        [OperationContract]
        List<CustomerOpportunitiesDTO> GetCustomerOpportunitiesCount(ArgsDTO args);

        [OperationContract]
        List<CustomerActivitiesDTO> GetA20Customers(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args);

        [OperationContract]
        List<CustomerActivitiesDTO> GetA20CustomersCount(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args);
        */

        #endregion

        #region - Column Setting -
        [OperationContract]
        bool InserColumnSetting(ColumnSettingDTO objColumnSettings);

        [OperationContract]
        ColumnSettingDTO GetColumnSetting(string originator, string gridName);

        [OperationContract]
        ColumnSettingDTO GetSortItems(string originator, string gridName);
        #endregion

        #region - Export -
        /* [OperationContract]
        string GetCustomerServiceCallActivitiesExport(ArgsDTO args,
            DateTime? FromDate,
            DateTime? ToDate,
            bool IsAllCustomers,
            bool IsNotServiceCalledCustomers,
            string activityStatus = "",
            string filterText = "",
            string path="");

        [OperationContract]
        string GetCombinedCollectionByStateExport(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "",
            string path = "");

        [OperationContract]
        string GetLeadCustomerCollectionByRepExport(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "",
            string path = "");

        [OperationContract]
        string GetCallCycleListExport(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "",
            string path = "");

        [OperationContract]
        string GetAllActivitiesByTypeExport(ArgsDTO args,
            string originator,
            string sFromDate = "",
            string sEndDate = "",
            string activityStatus = "",
            string path = "");

        [OperationContract]
        string GeActualLitersExport(ArgsDTO args, string path);

        [OperationContract]
        string GetA20CustomersExport(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args,
            string path = "");*/

        #endregion

        #region - Resource Document -
        [OperationContract]
        bool SaveResourceDocument(DocumentDTO documentDto);

        [OperationContract]
        bool DeleteResourceDocument(int documentId);

        [OperationContract]
        List<DocumentDTO> GetResourceDocumentDetails(ArgsDTO args, string path, string sessionID, bool issavedocument = false);

        [OperationContract]
        DocumentDTO GetResourceDocument(int documentID, string path, string sessionID, bool issavedocument = false);
        #endregion

        #region - Error -
        [OperationContract]
        int GetErrorCount(ExceptionDTO exceptionDTO);

        [OperationContract]
        bool SaveError(ExceptionDTO exceptionDTO);
        #endregion

        #region - Email Attachment -
        [OperationContract]
        List<EmailAttachmentDTO> GetEmailAttachment(int emailId, string emailAttachmentLocation, string sessionId);
        #endregion

        #region - Group Rep -

        [OperationContract]
        List<GroupRepDTO> GetGroupRepByGroup(int RepGroupId);

        #endregion

        #region - Home Page Summary
        [OperationContract]
        List<OpenDealDTO> GetTopFiveOpportunities(string sBusiness, ArgsDTO args);

        [OperationContract]
        List<OpportunityOwnerDTO> GetLeaderBoard(string cutoffDate, string closeDate, string businessCode, string sPeriod, ArgsDTO args);

        [OperationContract]
        List<LeadCustomerOpportunityDAO> GetLeaderCustomerOpportunity(ArgsDTO args);

        [OperationContract]
        List<LeadCustomerOpportunityDAO> GetLeaderCustomerOpportunityCount(ArgsDTO args);

        [OperationContract]
        List<SalesTrendDAO> GetSalesTrend(string sBusiness);

        [OperationContract]
        List<KeyAccountDAO> GetSalesKeyAccounts(string repCode, ArgsDTO args);

        [OperationContract]
        List<SalesLeaderDAO> GetSalesLeaderBoard(string businessCode, string sPeriod, ArgsDTO args);

        [OperationContract]
        List<KeyAccountDAO> GetTopSalesReps(ArgsDTO args);

        [OperationContract]
        List<KeyAccountDAO> GetTopSalesDistributors(ArgsDTO args);

        [OperationContract]
        List<KeyAccountDAO> GetTopSalesProducts(ArgsDTO args);

        [OperationContract]
        List<KeyAccountDAO> GetTopSalesMarkets(ArgsDTO args);

        #endregion

        #region - Area -

        [OperationContract]
        List<CityAreaDTO> GetAllAreaCodes();

        [OperationContract]
        List<CityAreaDTO> GetAllCityAreas(ArgsDTO args);

        #endregion

        #region - CheckList Methods -
        [OperationContract]
        List<CheckListDTO> GetChecklist(int iLeadStageID);

        [OperationContract]
        bool SaveCheckList(List<CheckListDTO> leadChecklistDto);

        [OperationContract]
        string SaveQuery(CheckListDTO leadChecklistDto);
        #endregion

        #region - Contact Person Image -

        [OperationContract]
        bool SaveContactPersonImage(ContactPersonImageDTO contactPersonImage);

        #endregion

        #region - Checklist -
        [OperationContract]
        List<ChecklistTypeDTO> GetChecklistTypeList();

        [OperationContract]
        bool SaveChecklistMaster(ChecklistMasterDTO checklistMasterDTO);

        #endregion

        #region - Country -
        [OperationContract]
        List<CountryDTO> GetCountry();

        [OperationContract]
        List<CatalogDTO> GetAllCatalog(ArgsDTO args);
        #endregion

        #region - States -
        [OperationContract]
        List<StateDTO> GetStates();

        [OperationContract]
        List<StateDTO> GetAllStates();

        [OperationContract]
        StateDTO GetStateByCode(string sStateCode);
        #endregion

        #region - Rep Group -
        [OperationContract]
        List<RepGroupDTO> GetAllRepGroups();

        #endregion

        #region - Lookup -

        [OperationContract]
        List<LookupDTO> GetLookups(string lookupSqlKey, string whereCls = "", object[] parameters = null, ArgsDTO args = null, bool hasArgs = false);

        [OperationContract]
        List<LookupDTO> GetValuesbySql(string sql);

        [OperationContract]
        List<LookupDTO> GetPeercoreLookupValues(string sTableId);

        [OperationContract]
        List<LookupDTO> GetLookupsForBDM(string tableName, KeyValuePair<string, string> descriptionColumn, KeyValuePair<string, string>? codeColumn,
            string whereCls, ArgsDTO args, string primaryKey = "");

        #endregion

        #region - LookupTable -
        [OperationContract]
        List<LookupTableDTO> GetLookupTables(string tableId, ArgsDTO args);

        [OperationContract]
        LookupTableDTO GetDefaultLookupEntry(string sTableID, string defaultDeptID);

        [OperationContract]
        List<LookupTableDTO> GetRepTerritories(ArgsDTO args);

        [OperationContract]
        List<LookupTableDTO> GetSalesMarkets();
        #endregion - LookupTable -

        #region - Promotion -
        [OperationContract]
        List<PromotionDTO> GetAllPromotions();
        #endregion

        [OperationContract]
        List<CatalogDTO> GetProductsLookup(ArgsDTO args);

        [OperationContract]
        SalesInfoDetailBackDTO TestSalesInfoDetailBack();

        #region - Originator -

        [OperationContract]
        List<OriginatorDTO> GetAllOriginators(string name);

        #endregion


        #region - MarketTable -

        [OperationContract]
        List<MarketDTO> GetAllMarket(ArgsDTO args);

        [OperationContract]
        bool SaveMarket(MarketDTO marketDTO);

        [OperationContract]
        bool AssignTMEMarkets(List<MarketDTO> marketDTOList);

        [OperationContract]
        bool DeleteTMEMarkets(MarketDTO marketDTO);

        [OperationContract]
        List<MarketDTO> GetMarketTargetsByDistributorId(ArgsDTO args);

        [OperationContract]
        bool IsMarketExists(MarketDTO marketDTO);

        #endregion - MarketTable -


        #region - Product -

        //[OperationContract]
        //LoadStockDTO GetAllProductsForStockLoad(string allocDate, int distributorId, int divisionId, ArgsDTO args);

        //[OperationContract]
        //bool SaveLoadStock(LoadStockDTO loadStockDTO);

        [OperationContract]
        List<RouteDTO> GetAllRoutes(string name);

        [OperationContract]
        List<OriginatorDTO> GetRepsByRouteMasterId(string name, int routeMasterId);

        //[OperationContract]
        //bool UnLoadStock(LoadStockDetailDTO loadStockDetailDTO, DateTime allocDate);

        [OperationContract]
        List<RouteDTO> GetRouteRepLikeRouteName(string name, int isTemp, string allocationDate);

        [OperationContract]
        List<BrandDTO> GetAllBrandsDataAndCount(ArgsDTO args);

        [OperationContract]
        List<ProductCategoryDTO> GetAllProductCategoryDataAndCount(ArgsDTO args);

        [OperationContract]
        bool SaveBrand(BrandDTO brandDTO);

        [OperationContract]
        bool IsBrandCodeExists(BrandDTO brandDTO);

        [OperationContract]
        bool IsProductFlavorNameExists(FlavorDTO flavorDTO);

        [OperationContract]
        bool IsBrandNameExists(BrandDTO brandDTO);

        [OperationContract]
        List<ProductDTO> GetAllProductsDataAndCount(ArgsDTO args);

        [OperationContract]
        List<ProductCategoryDTO> GetAllProductCategories(ArgsDTO args);

        [OperationContract]
        bool SaveProduct(ProductDTO productDTO);

        [OperationContract]
        bool IsProductCodeExists(ProductDTO productDTO);

        [OperationContract]
        bool IsProductNameExists(ProductDTO productDTO);

        [OperationContract]
        bool IsProductCategoryCodeExists(ProductCategoryDTO productcategoryDTO);

        [OperationContract]
        bool IsProductCategoryNameExists(ProductCategoryDTO productcategoryDTO);

        [OperationContract]
        bool SaveProductCategory(ProductCategoryDTO productcategoryDTO);

        [OperationContract]
        List<ProductDTO> GetAllProductsWithNoPrice(ArgsDTO args);

        [OperationContract]
        List<ProductDTO> GetAllProductPricesDataAndCount(ArgsDTO args);

        [OperationContract]
        bool SaveProductPrice(ProductDTO productDTO);

        [OperationContract]
        List<ProductPackingDTO> GetAllProductPackings(ArgsDTO args);

        [OperationContract]
        ProductDTO GetProductById(int productId);

        [OperationContract]
        bool DeleteProductCategory(ProductCategoryDTO productcategoryDTO);

        [OperationContract]
        bool DeleteInvoice(InvoiceHeaderDTO invoiceDTO);

        [OperationContract]
        bool DeleteInvoiceHeaderByInvoiceNo(string originator, string invoiceNo);

        [OperationContract]
        bool SaveProductFlavor(FlavorDTO flavorDTO);

        #endregion


        #region - Holiday -

        [OperationContract]
        bool InsertHoliday(CalendarHolidayDTO holidaydto);

        [OperationContract]
        List<CalendarHolidayDTO> GetAllHolidays(ArgsDTO args);

        [OperationContract]
        CalendarHolidayDTO GetHolidayTypeByCode(string holidayType);

        [OperationContract]
        bool DeleteHoliday(int holidayId);

        [OperationContract]
        bool DeleteProductDevision(int divisionId, int prodId);

        [OperationContract]
        List<CalendarHolidayDTO> GetAllHolidaytypes();

        [OperationContract]
        bool UpdateHoliday(CalendarHolidayDTO holidayDTO);

        [OperationContract]
        CalendarHolidayDTO Test();

        [OperationContract]
        bool IsHolidayExistsForTheDay(string createdBy, DateTime effDate);

        [OperationContract]
        bool UpdateHolidayLastChangedDate(CalendarHolidayDTO calendarHolidayDTO);

        [OperationContract]
        bool UpdateHolidayDate(CalendarHolidayDTO calendarHolidayDTO);

        [OperationContract]
        int InsertHolidayReturnId(CalendarHolidayDTO holidayDTO);
        #endregion

        #region TargetAllcoation

        [OperationContract]
        bool SaveTargetAllocation(TargetAllocationDTO targetAllocationDTO);

        #endregion

        #region - Program -

        [OperationContract]
        List<ProgramTargetsDTO> GetProgramTargetsByProgram(string programNameLike, int programTargetId, int programId);


        [OperationContract]
        List<ProductDTO> GetProductsByName(string productNameLike);

        [OperationContract]
        bool SaveProgramTargets(List<ProgramTargetsDTO> programTargetsDTOList);

        [OperationContract]
        bool SaveProgram(ProgramDTO program);

        [OperationContract]
        bool DeleteProgram(int programId);


        [OperationContract]
        List<ProgramDTO> GetAllProgram(ArgsDTO args);

        [OperationContract]
        List<ProgramCustomerDTO> GetAllProgramCustomer(ArgsDTO args);

        [OperationContract]
        ProgramDTO GetProgramDetailsById(int programId);


        [OperationContract]
        List<ProgramTargetsDTO> GetProgramTargets(int programId, DateTime fromDate, DateTime toDate, ArgsDTO args);

        [OperationContract]
        bool IsProgramNameExists(ProgramDTO programDTO);

        [OperationContract]
        bool SaveProductForDivision(int divisionId, ProductDTO productDTO);

        [OperationContract]
        bool SaveProductDivisions(int divisionId, List<ProductDTO> productDTO);
        #endregion

        [OperationContract]
        List<RouteDTO> GetRouteRepLikeRouteNameForCustomer(string name, int isTemp, string originator, string chlidOriginators);

        [OperationContract]
        List<CatalogDTO> GetCatalogLookup(ArgsDTO args);

        [OperationContract]
        List<ProductDTO> GetProductsByDivisionId(int divisionId, ArgsDTO args);

        [OperationContract]
        List<FlavorDTO> GetAllFlavor();

        [OperationContract]
        List<InvoiceHeaderDTO> GetAllInvoicesDataAndCount(ArgsDTO args);

        [OperationContract]
        List<OutletTypeDTO> GetAllOutletType();

        #region - Holiday Planner Change Log -

        [OperationContract]
        int InsertHolidayPlannerChangeLog(HolidayPlannerChangeLogDTO holidayPlannerChangeLogDTO);

        [OperationContract]
        bool UpdateHolidayPlannerChangeLogStatus(HolidayPlannerChangeLogDTO holidayPlannerChangeLogDTO);

        [OperationContract]
        HolidayPlannerChangeLogDTO GetHolidayPlannerChangeLogById(int holidayPlannerChangeLogId);

        #endregion

        #region - Rep Target -

        [OperationContract]
        bool SaveRepTarget(List<RepTargetDTO> repTargetDTOList);

        [OperationContract]
        List<RepTargetDTO> GetAllRepTargets(string Originator, string OriginatorType, ArgsDTO args);

        [OperationContract]
        bool DeactivateRepTarget(RepTargetDTO repTarget);

        #region - Incentive Plan -

        [OperationContract]
        List<IncentivePlanHeaderDTO> GetAllIncentivePlans(ArgsDTO args);

        [OperationContract]
        bool SaveIncentivePlans(List<IncentivePlanHeaderDTO> incentivePlanHeaderList);

        [OperationContract]
        bool DeactivateSecondarySalesWeekly(IncentiveSecondarySalesWeeklyDTO weeklyDTO);

        [OperationContract]
        bool DeactivateSecondarySalesPartial(IncentiveSecondarySalesPartialDTO partialDTO);
        #endregion

        #endregion

        [OperationContract]
        bool DeleteCustomerByCusCode(CustomerDTO delCustomer);

        [OperationContract]
        bool UploadStockToDistributor(List<ProductDTO> lstProduct, int distributorId, LoadStockDTO loadStockDTO);

        [OperationContract]
        bool SaveProductPacking(ProductPackingDTO packingDTO);

        [OperationContract]
        bool IsProductPackingNameExists(ProductPackingDTO packingDTO);

        [OperationContract]
        bool IsAreaNameExists(CityAreaDTO areaDTO);

        [OperationContract]
        bool IsOutletTypeExists(OutletTypeDTO outlettypeDTO);

        [OperationContract]
        bool SaveArea(CityAreaDTO areaDTO);

        [OperationContract]
        bool SaveOutletType(OutletTypeDTO outlettypeDTO);

        [OperationContract]
        bool DeleteAddCustomerRoute(CustomerDTO delCustomer);

        [OperationContract]
        List<ProductDTO> GetAllProductsDataAndCountWithOutImage(ArgsDTO args);

        [OperationContract]
        bool InactiveProduct(ProductDTO productDTO);

        [OperationContract]
        bool DeleteProductFlavor(int flavourId);

        [OperationContract]
        bool DeleteOutletType(int outletTypeId);

        [OperationContract]
        bool GetAdminPassword(string password);

        [OperationContract]
        bool DeleteBrand(int brandId);

        [OperationContract]
        bool DeletePacking(int packingId);

        [OperationContract]
        bool UpdateCheckListCustomer(int CheckListId, string CustomerCode, bool HasSelect);

        [OperationContract]
        bool DeleteDivision(int divisionId);

        [OperationContract]
        bool DeleteArea(int areaId);

        [OperationContract]
        List<SalesDataEntity> GetAllSales(ArgsDTO args, string IsASMOrProduct, int ProductId);

        [OperationContract]
        List<SalesDataEntity> getASMSalesData(ArgsDTO args);

        [OperationContract]
        List<SalesDataEntity> GetRepSales(ArgsDTO args);

        [OperationContract]
        List<CheckListDTO> GetAllCheckListDataAndCount(ArgsDTO args);

        [OperationContract]
        bool UpdateCheckListRoute(int CheckListId, int RouteId, bool HasSelect);

        [OperationContract]
        List<ChecklistImageDTO> GetChecklistImageById(int CheckListId);

        [OperationContract]
        List<OriginatorEntity> GetOriginators(ArgsDTO args);

        [OperationContract]
        List<OriginatorEntity> GetOriginatorsFor2FA(ArgsDTO args);

        [OperationContract]
        bool UpdateIsTFAUser(string originator, bool isTFA);

        [OperationContract]
        bool UpdateIsTFAAuth(string originator, bool isTFAAuth);

        [OperationContract]
        string addNewUser(string originator, string userName, string designation, string password, string userType, string mobile);

        [OperationContract]
        string updateUser(string originator, string userName, string designation, string password, string userType, int UserID, string mobile);

        [OperationContract]
        string deleteUser(int UserId);

        [OperationContract]
        List<OriginatorEntity> getAllOriginatorTypesForDropdown();

        [OperationContract]
        List<SalesDataEntity> getRepsLiveSales(string originator);

        [OperationContract]
        int IsProductExsistInDiscountScheme(int product_id);

        //[OperationContract]
        //bool UploadStockToBulkDistributor(List<ProductDTO> lstProduct, LoadStockDTO loadStockDTO); 

        [OperationContract]
        bool SaveTargetAndIncentiveNew(string TargetBreakDay, string First14Days, string Last14Days);

        [OperationContract]
        bool PushNotifyGetSyncDetails();

        [OperationContract]
        List<TargetEntity> LoadTargetMaster();

        [OperationContract]
        LoadStockDTO GetProductForStockAdjustment(string allocDate, int distributorId, int divisionId, ArgsDTO args);

        [OperationContract]
        List<ProductDTO> GetAllProductsDataAndCountWithOutImageForMaster(ArgsDTO args);

        #region "Sync Info"

        [OperationContract]
        int UpdateRepSyncData(SyncRequestModel request);

        #endregion

        #region "Reports"

        [OperationContract]
        List<SalesRepRouteSequenceModel> GetAllSRRouteLocations(SRRouteSequenceRequestModel request);

        #endregion

        [OperationContract]
        bool DeleteBulkInvoices(string userType, string userName, string invList);

        [OperationContract]
        bool UpdateOrderIsInvoiceActivate(OrderIsInvoiceActivate request);




        #region "3rd Phase"

        [OperationContract]
        bool CreateTransactionLog(string originator, string transTime, string transType, string transModule, string transDesc);

        [OperationContract]
        List<SalesRepAttendanceHoursModel> GetRepsAttendanceHours(string originator, string date);

        [OperationContract]
        List<AttendanceReasons> GetAttendanceReasons();

        [OperationContract]
        bool InsertAttendanceReason(AttendanceReasons attendanceDet);

        [OperationContract]
        bool deleteAttendanceReason(string originator, int id);

        [OperationContract]
        List<DashboardAttendanceByRepType> GetDashboardAttendanceByRepType(string originator, string date);

        [OperationContract]
        List<DashboardAttendanceDetails> GetDashboardAttendanceDetails(ArgsModel args, string originator, string date, string salesType, string attendType);

        [OperationContract]
        MobileRepsDashboardModel LoadMobileRepsDashboard();

        [OperationContract]
        bool SaveMobileRepDashboard(MobileRepsDashboardModel mobileDashboard);

        [OperationContract]
        List<KPIReports> GetAllKPIReports();

        [OperationContract]
        List<KPIReportUser> GetAllKPIReportUsersByReportId(int ReportId);

        [OperationContract]
        bool UpdateKPIReport(KPIReports kpiReport);

        [OperationContract]
        List<CustomerCategory> GetAllCustomerCategory();

        [OperationContract]
        bool UploadOutletBulkTransfer(string Originator, int NewRouteId, string CustomerCode, string CustomerName);

        #endregion
    }
}
