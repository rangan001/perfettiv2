﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.Data;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service.ServiceContracts
{
    public enum Status
    {
        Delete,
        Activate
    }

    [ServiceContract(Name = "Customer")]
    public interface ICustomer
    {
        #region - Customer -
        [OperationContract]
        bool SaveCustomerAddress(LeadAddressDTO customerAddressDto);

        [OperationContract]
        CustomerDTO GetCustomer(string customerCode);

        [OperationContract]
        List<LeadAddressDTO> GetCustomerAddresses(string customerCode, ArgsDTO args);

        [OperationContract]
        List<RepCustomerDTO> GetNewCustomers(ArgsDTO args);

        [OperationContract]
        LeadCustomerDTO GetCustomerView(ArgsDTO args);

        [OperationContract]
        LeadAddressDTO GetCustomerTableAddresses(string custCode);

        [OperationContract]
        List<SalesOrderDTO> GetOutstandingDeliveries(string custCode);

        [OperationContract]
        int GetOutstandingDeliveriesCount(string custCode);

        [OperationContract]
        DateTime GetLastDelivery(string custCode);
        #endregion

        #region - ContactPerson -

        [OperationContract]
        bool SaveContactPerson(ContactPersonDTO oContactPersonDTO, out int contactPersonId);

        [OperationContract]
        ContactPersonDTO GetContactPerson(int contactPersonId);

        [OperationContract]
        ContactPersonDTO GetCustomerContactPerson(int contactPersonId);

        [OperationContract]
        List<ContactPersonDTO> GetCustomerContacts(string sCustCode, ArgsDTO args, string clientType, bool includeChildReps);

        [OperationContract]
        ContactPersonDTO GetCustomerContact(string custCode, string contactType, string origin);

        [OperationContract]
        bool DeleteContactPerson(int contactPersonId);
        #endregion         

        #region - ImportMail -
        [OperationContract]
        List<EmailDTO> GetCustomerEmails(string custCode, ArgsDTO args, string directoryPath, string sessionId);
        #endregion 

        #region - Contact Person Image -
        [OperationContract]
        ContactPersonImageDTO GetCustomerContactPersonImage(int contactPersonId, string custCode, string imgLocation, string sessionId);

        #endregion

        #region - Document -

        [OperationContract]
        List<DocumentDTO> GetContactDocumentsForCustomer(ArgsDTO args, string path, string sessionID, bool issavedocument = false);

        [OperationContract]
        List<DocumentDTO> GetDocumentsForEntryPages(ArgsDTO args, string path, string sessionID, bool issavedocument = false);
        #endregion

        #region -Targets-

        [OperationContract]
        List<TargetInfoDTO> GetCustomerMonthleyTargetsByDistributor(string username);

        [OperationContract]
        List<CustomerDTO> GetCustomerTargetsByRouteId(int routeId, ArgsDTO args);

        [OperationContract]
        bool SaveCustomerTargets(List<CustomerDTO> customerTargetsDTOList);

        #endregion

        /*
        [OperationContract]
        List<DocumentDTO> GetContactDocumentForCustomer(string customerCode, ArgsDTO args);

        [OperationContract]
        List<EquipmentDTO> GetCustomerEquipments(string custCode, ArgsDTO args);
        */
        [OperationContract]
        bool SaveCustomer(CustomerDTO customerDto);

        //[OperationContract]
        //DataTable ReviewExcelfile(string FilePath, string Extension, string UserName);

        //[OperationContract]
        //bool UploadExcelfile(string FilePath, string Extension, string UserName, string ASEOriginator, string DistOriginator, string RepOriginator);

        [OperationContract]
        bool UploadExcelfile(string FilePath, string Extension, string UserName, string TerritoryId);
        /*
        [OperationContract]
        DebtorsBarometerDTO GetDebtorsDetails(string custCode);

        [OperationContract]
        SalesGraphDataDTO GetSalesDataForCompleteMonths(string custCode);

        [OperationContract]
        List<CatalogDTO> GetRecentProducts(string custCode);

        [OperationContract]
        bool UpdateEquipmentStatus(string assetNumber,Status oStatus, string CustomerCode);

        [OperationContract]
        EquipmentDTO GetEquipment(string assetNumber, string CustomerCode);

        [OperationContract]
        List<DocumentDTO> GetContactDocumentsForView(string customerCode, ArgsDTO args, string path, string sessionID, bool issavedocument = false);

        [OperationContract]
        DocumentDTO GeContactDocumentForView(int documentID, string path, string sessionID, bool issavedocument = false);
        
        [OperationContract]
        bool SaveEquipment(EquipmentDTO equipment);

        [OperationContract]
        bool IsWasteCustomer(string custCode);
         */

        [OperationContract]
        CustomerDTO GetCustomerPending(string customerCode);

        [OperationContract]
        List<KeyValuePair<int, string>> GetAllOutletTypes();

        [OperationContract]
        List<KeyValuePair<string, string>> GetAllTowns();

        [OperationContract]
        bool DeleteCustomerByCustomerCode(CustomerDTO delCustomer);




        #region "Perfetti 2nd Phase"

        [OperationContract]
        List<CustomerModel> GetAllAllCustomerByRouteMasterId(ArgsModel args, string RouteMasterId);

        [OperationContract]
        bool DeleteBulkCust(string userType, string userName, string custList);

        [OperationContract]
        List<CustomerModel> GetAllPendingCustomers(ArgsModel args, string IsReject, string Originator, string Asm, string Territory);

        [OperationContract]
        List<CustomerModel> GetAllPendingCustomersForSalesConfirm(ArgsModel args, string IsReject, string Originator, string Asm, string Territory);


        [OperationContract]
        bool AcceptCustomer(string Originator, string CustCode);

        [OperationContract]
        bool RejectCustomer(string Originator, string CustCode);

        [OperationContract]
        bool AcceptSalesCustomer(string Originator, string CustCode);

        [OperationContract]
        bool InsertCustomerCategory(CustomerCategory custCategory);
        
        [OperationContract]
        bool DeleteCustomerCategory(CustomerCategory custCategory);

        #endregion
    }
}
