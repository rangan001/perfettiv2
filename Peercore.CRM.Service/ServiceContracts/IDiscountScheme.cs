﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "DiscountScheme")]
    public interface IDiscountScheme
    {
        [OperationContract]
        List<SchemeHeaderDTO> GetAllSchemeHeader(ArgsDTO args);
        
        [OperationContract]
        List<SchemeHeaderDTO> GetAllSchemeHeaderForCopy();

        [OperationContract]
        SchemeHeaderDTO GetSchemeHeaderById(ArgsDTO args,int schemeHeaderId);

        [OperationContract]
        bool UpdateSchemeHeader(SchemeHeaderDTO schemeHeader);

        [OperationContract]
        bool InsertSchemeHeader(out int schemeHeaderId, SchemeHeaderDTO schemeHeader);

        [OperationContract]
        bool SchemeHeaderSave(out int schemeHeaderId, SchemeHeaderDTO schemeHeader);

        [OperationContract]
        bool UpdateSchemeDetails(SchemeDetailsDTO schemeDetails);

        [OperationContract]
        bool InsertSchemeDetails(out int schemeDetailsId, SchemeDetailsDTO schemeDetails);

        [OperationContract]
        List<SchemeDetailsDTO> GetSchemeDetailsBySchemeHeaderId(ArgsDTO args, int schemeHeaderId);

        [OperationContract]
        List<OptionLevelDTO> GetOptionLevelByDetailsId(int detailsId);

        [OperationContract]
        List<OptionLevelCombinationDTO> GetOptionLevelCombinationByOptionLevelId(int optionLevelId);

        [OperationContract]
        List<FreeProductDTO> GetAllFreeProducts(ArgsDTO args);

        [OperationContract]
        List<DivisionDTO> GetAllDivisions(ArgsDTO args);
    
        [OperationContract]
        List<SchemeHeaderDTO> GetSchemeHeaderByName(string name, bool isActive);

        [OperationContract]
        bool IsSchemeHeaderNameExists(SchemeHeaderDTO schemeHeader);

        [OperationContract]
        bool UpdateSchemeHeaderIsActive(SchemeHeaderDTO schemeHeader);

        [OperationContract]
        bool SaveDivision(out int divisionId, DivisionDTO divisionEntity);


        #region "For 2nd Phase"

        [OperationContract]
        List<SchemeHeaderModel> GetAllDiscountSchemeHeader(ArgsModel args);
        
        [OperationContract]
        bool SaveUpdateDiscountTerritory(string originator,
                                                       string scheme_header_id,
                                                       string territory_id,
                                                       string is_checked);

        #endregion
    }
}
