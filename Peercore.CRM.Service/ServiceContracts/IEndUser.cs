﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.Collections.ObjectModel;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "EndUser")]
    public interface IEndUser
    {
        #region - End User -

        [OperationContract]
        bool EndUserSave(EndUserDTO endUserDto);

        [OperationContract]
        EndUserDTO GetEndUser(ArgsDTO args);

        [OperationContract]
        List<EndUserDTO> GetEndUsers(ArgsDTO args, string sStatus);

        [OperationContract]
        List<EndUserDTO> GetEndUsersByCode(string enduserCodes);

        [OperationContract]
        int SetActiveStatus(ArgsDTO args, bool isActive);

        [OperationContract]
        List<EndUserDTO> GetAllEndUsers(ArgsDTO args);

        [OperationContract]
        EndUserDTO IsEndUserExistForCustomer(ArgsDTO args);

        [OperationContract]
        List<EndUserDTO> IsEndUserExistForOtherCustomers(ArgsDTO args);

        [OperationContract]
        List<EndUserPriceDTO> GetEndUserPricing(ArgsDTO args, string effectiveDate);

        [OperationContract]
        List<EnduserEnquiryDTO> GetEnduserSalesWithAllProducts(ArgsDTO args, bool bIncludeProductsWithNoSales, bool bActiveOnly);

        [OperationContract]
        List<EndUserSalesDTO> GetEnduserSales(ArgsDTO args, bool bActiveOnly);

        [OperationContract]
        List<CustomerDTO> GetEndUserCustomers(ArgsDTO args);

        [OperationContract]
        bool Transfer(EndUserDTO endUser, ArgsDTO args);

        [OperationContract]
        EndUserPriceDTO GetEndUserPrice(ArgsDTO args, DateTime effectiveDate, string catalogCode);


        #endregion

        #region - EndUserContactPerson -

        [OperationContract]
        bool SaveEndUserContact(ContactPersonDTO endUserContactPerson, ref int contactPersonId);

        [OperationContract]
        ContactPersonDTO GetEndUserContactPerson(int iContactPersonID);

        [OperationContract]
        List<ContactPersonDTO> GetEndUserContactPersons(ArgsDTO args);

        [OperationContract]
        bool SaveEndUserContactPersonImage(ContactPersonImageDTO contactPersonImage, ref int contactPersonImageId);

        [OperationContract]
        ContactPersonImageDTO GetEndUserContactPersonImage(int contactPersonId, string endUserCode, string directoryPath, string sessionId);

        #endregion
        
        #region - price List -

        [OperationContract]
        bool SavePriceList(ArgsDTO args, DateTime dtmEffectiveDate, List<EndUserPriceDTO> listEndUserPrice);

        #endregion
    }
}
