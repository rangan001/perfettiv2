﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Service.DTO;
using System.ServiceModel;
using System.Collections.ObjectModel;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "LeadCustomer")]
    public interface ILeadCustomer
    {
        #region - Activity -
        [OperationContract]
        List<LeadCustomerDTO> GetNotCalledCustomers(ArgsDTO args);

        [OperationContract]
        List<LeadCustomerDTO> GetCalledCustomers(ArgsDTO args);

        #endregion

        #region - Lead -

        [OperationContract]
        List<LeadCustomerDTO> GetCombinedCollection(ArgsDTO args);

        [OperationContract]
        List<LeadCustomerDTO> GetCombinedCollectionbyleadStageName(ArgsDTO args);

        [OperationContract]
        List<LeadCustomerDTO> GetLeadCustomerCountByState(ArgsDTO args);

        [OperationContract]
        List<LeadCustomerDTO> GetCombinedCollectionByState(ArgsDTO args, string sLeadOrCustomer = "");

        [OperationContract]
        List<LeadCustomerDTO> GetLeadCustomerCountByRep(ArgsDTO args);

        [OperationContract]
        List<LeadCustomerDTO> GetLeadCustomerCollectionByRep(ArgsDTO args);

        [OperationContract]
        List<LeadCustomerDTO> LeadExist(LeadDTO lead, ArgsDTO args);


        [OperationContract]
        List<LeadCustomerDTO> GetPendingCustomers(ArgsDTO args);
        #endregion

        [OperationContract]
        List<ColumnSettingDTO> testMethod();

        [OperationContract]
        List<ObjectiveDTO> GetAllMarketObjectives(int marketId, ArgsDTO args);

        [OperationContract]
        bool SaveMarketObjective(MarketObjectiveDTO marketObjectiveDTO);

        [OperationContract]
        int GetPendingCustomersCount(ArgsDTO args);

        [OperationContract]
        bool IsObjectiveCodeExists(ObjectiveDTO objectiveDTO);

        [OperationContract]
        List<LeadCustomerDTO> GetOutletsForLookupInScheduleEntry(ArgsDTO args);

        [OperationContract]
        bool UnassignedRoutefromRep(int RouteId, string RepCode);

        [OperationContract]
        List<LeadCustomerDTO> GetAllNotInvoicedUsers(ArgsDTO args);

        [OperationContract]
        List<LeadCustomerDTO> GetLeadCollectionForCheckList(int CheckListId, ArgsDTO args);
    }
}
