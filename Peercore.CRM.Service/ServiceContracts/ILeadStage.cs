﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "LeadStage")]
    public interface ILeadStage
    {
        #region - LeadStages - 
        [OperationContract]
        List<LeadStageDTO> GetLeadStages(ArgsDTO args);
        
        #endregion

        
        [OperationContract]
        List<LeadStageDTO> GetLeadStageChartData(ArgsDTO args);

        /*
        [OperationContract]
        bool SaveLeadStage(List<LeadStageDTO> lstStages, string defaultDeptID);

        [OperationContract]
        int SaveLeadStageList(List<LeadStageDTO> leadStages, List<LeadStageDTO> deletedStages, string defaultDeptID);

        [OperationContract]
        bool CanDeleteLeadStage(int leadStageId);

        [OperationContract]
        LeadStageDTO GetLeadStage(int SourceId);
        */
    }
}
