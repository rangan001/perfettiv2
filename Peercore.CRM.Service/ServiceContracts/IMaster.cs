﻿using Peercore.CRM.Model;
using Peercore.CRM.Service.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Master")]
    public interface IMaster
    {
        //[OperationContract]
        //List<CompanyDTO> GetAllCompany(ArgsModel args);

        #region "Regions"

        [OperationContract]
        List<RegionModel> GetAllRegions(ArgsModel args);

        [OperationContract]
        List<RegionModel> GetAllRegionsByRegionCode(ArgsModel args, string RegionCode);

        [OperationContract]
        List<RegionModel> GetAllRegionsByRegionName(ArgsModel args, string RegionName);
        
        [OperationContract]
        bool DeleteRegion(string region_id);

        [OperationContract]
        bool SaveRegion(string comp_id, string region_id, string region_code, string region_name);

        #endregion
        
        #region "RSM"

        [OperationContract]
        List<RsmModel> GetAllRSM(ArgsModel args);
        
        [OperationContract]
        bool DeleteRsmByRsmId(string rsm_id);
        
        [OperationContract]
        List<RsmModel> GetAllRsmByRsmCode(ArgsModel args, string RsmCode);
        
        [OperationContract]
        List<RsmModel> GetRsmDetailsByRegionId(ArgsModel args, string RegionId);

        [OperationContract]
        bool SaveRsm(string region_id,
                            string rsm_id,
                            string rsm_code,
                            string rsm_name,
                            string rsm_username,
                            string rsm_tel1,
                            string rsm_email,
                            string created_by);

        [OperationContract]
        bool UpdateRsmPassword(string rsmId, string password);

        #endregion

        #region "Area"

        [OperationContract]
        List<AreaModel> GetAllAreaNew(ArgsModel args);

        [OperationContract]
        List<AreaModel> GetAllAreaByAreaCode(ArgsModel args, string AreaCode);

        [OperationContract]
        List<AreaModel> GetAllAreaByRegionId(ArgsModel args, string RegionId);

        [OperationContract]
        bool DeleteAreaFromRegionByAreaId(int areaId);

        //[OperationContract]
        //bool DeleteArea(string area_id);

        [OperationContract]
        bool InsertUpdateArea(string region_id, string area_id, string area_code, string area_name);

        #endregion

        #region "Territory"

        [OperationContract]
        List<TerritoryModel> GetAllTerritory(ArgsModel args);
        
        [OperationContract]
        List<TerritoryModel> GetAllTerritoryByOriginator(ArgsModel args, string OriginatorType, string Originator);

        [OperationContract]
        List<TerritoryModel> GetAllTerritoryByDistributerId(ArgsModel args, string DistributerId); 
        
        [OperationContract]
        List<TerritoryModel> GetAllTerritoryByTerritoryCode(ArgsModel args, string TerritoryCode);

        [OperationContract]
        List<TerritoryModel> GetAllTerritoryByTerritoryName(ArgsModel args, string TerritoryName);

        [OperationContract]
        List<TerritoryModel> GetAllTerritoryByAreaId(ArgsModel args, string AreaId);

        [OperationContract]
        bool DeleteTerritory(string territory_id);

        [OperationContract]
        bool DeleteTerritoryFromAreaByTerritoryId(string territory_id);

        [OperationContract]
        bool SaveTerritory(string area_id, string territory_id, string territory_code, string territory_name, string created_by);
        
        [OperationContract]
        List<TerritoryModel> GetAllTerritoryByDiscountScheme(ArgsModel args, string ShemeHeaderId);
        
        [OperationContract]
        bool IsTerritoryAssigned(string territoryId, out string RepCode);
        
        [OperationContract]
        bool DeleteConfirmSRFromTerritory(string territoryId, string repId);
        
        [OperationContract]
        bool UpdateTerritoryCanAddOutlet(string territory_id, bool can_add_outlet);
        
        [OperationContract]
        bool UpdateTerritoryGeoFence(string territory_id, bool is_geofence);

        [OperationContract]
        List<SalesRepSyncModel> GetAllSalesRepSyncData(ArgsModel args, string OriginatorType, string Originator);

        #endregion

        #region "Asm"

        [OperationContract]
        List<AsmModel> GetAllAsm(ArgsModel args);

        [OperationContract]
        List<OriginatorImeiModel> GetAllOriginatorImeiByAsmId(ArgsModel args, string asmId);

        [OperationContract]
        List<AsmModel> GetAllAsmByAreaId(ArgsModel args, string AreaId);

        [OperationContract]
        bool DeleteAsmByAsmId(string asm_id);
        
        [OperationContract]
        bool DeleteIMEIByAsmId(string asm_id, string emei_no);

        [OperationContract]
        bool UpdateOriginatorIsIMEIByAsmId(string asmId, bool isImei);
        
        [OperationContract]
        bool UpdateAsmPassword(string asmId, string password);

        [OperationContract]
        bool SaveAsm(string area_id,
            string asm_id,
            string asm_code,
            string asm_name,
            string asm_username,
            string asm_tel1,
            string asm_email,
            string created_by);

        #endregion

        #region "Route Master"

        //[OperationContract]
        //List<AreaModel> GetAllAreaNew(ArgsModel args);

        [OperationContract]
        List<RouteMasterModel> GetAllRoutesMaster(ArgsModel args);

        [OperationContract]
        List<RouteMasterModel> GetAllRouteMasterByTerritoryId(ArgsModel args, string TerritoryId);

        [OperationContract]
        bool DeleteRouteFromTerritoryByRouteId(string routeId);

        [OperationContract]
        RouteMasterModel GetRouteByRouteCode(string RoutCode);

        [OperationContract]
        RouteMasterModel GetRouteByRouteName(string RoutName);

        [OperationContract]
        bool InsertUpdateRouteMaster(RouteMasterModel routeMaster);
        
        [OperationContract]
        bool DeleteRouteMasterByRouteId(string routeId, string DeletedBy);

        #endregion

        #region "Sales Reps"

        [OperationContract]
        List<SRModel> GetAllSRDetails();

        //[OperationContract]
        //List<AreaModel> GetAllAreaByAreaCode(ArgsModel args, string AreaCode);

        [OperationContract]
        List<SRModel> GetAllSalesRepsByTerritoryId(ArgsModel args, string TerritoryId);
        
        [OperationContract]
        List<SRModel> GetAllSalesRepsByAreaId(ArgsModel args, string AreaId);
        
        [OperationContract]
        List<SRModel> GetAllSalesReps(ArgsModel args);
        
        [OperationContract]
        List<SRModel> GetAllSalesRepsByOriginator(ArgsModel args, string Originator);
        
        [OperationContract]
        List<SRModel> GetAllSalesRepsForRouteAssign(ArgsModel args);
        
        [OperationContract]
        List<SRModel> GetAllSRTerritoryByOriginator(ArgsModel args, string OriginatorType, string Originator);
        
        [OperationContract]
        List<DistributerTerritoryModel> GetAllTerritoriesForDistributer(ArgsModel args, string OriginatorType, string Originator);

        [OperationContract]
        SRModel GetSRDetailsByOriginator(string originator);

        [OperationContract]
        bool SaveSR(SRModel srEntity);
        
        [OperationContract]
        List<SalesRepSalesTypeModel> GetAllSalesRepWithSalesType(ArgsModel args, string OriginatorType, string Originator);
        
        [OperationContract]
        bool UpdateSRSalesType(string RepId, string SalesType, string Originator);

        #endregion

        #region "Distributer"

        [OperationContract]
        List<DistributerModel> GetAllDistributerByOriginatorAndOriginatorType(ArgsModel args, string OriginatorType, string Originator);

        #endregion
    }
}
