﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Messages")]
    public interface IMessage
    {
        [OperationContract]
        bool SaveMessages(MessagesDTO message, List<OriginatorDTO> chldOriginatorList, out int newrecordid);

        [OperationContract]
        List<MessagesDTO> GetMessageByOriginator(string originator);

        [OperationContract]
        MessagesDTO GetMessage(int messageId);

        [OperationContract]
        MessagesDTO GetMessageCount(string originator, string datestring);

        [OperationContract]
        bool DeleteMessage(int messageId);

        [OperationContract]
        bool UpdateMessageRep(string username);
    }
}
