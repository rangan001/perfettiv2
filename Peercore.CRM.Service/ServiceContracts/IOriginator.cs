﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using System.ServiceModel;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Originator")]
    public interface IOriginator
    {
        #region New Methods

        [OperationContract]
        List<OriginatorDTO> GetReps();

        [OperationContract]
        string GetChildOriginators(string sOriginator, bool managerMode = false);

        [OperationContract]
        List<string> GetChildReps(string sOriginator, bool managerMode , ref string sWhereCls, string childOriginators = null);

        [OperationContract]
        OriginatorDTO GetLoginDetails(string originator, string password);
        #endregion

        [OperationContract]
        OriginatorDTO GetOriginator(string originator);

        [OperationContract]
        string GetRepCode(string originator);

        [OperationContract]
        string GetBDMString();

        [OperationContract]
        List<KeyValuePair<string, string>> GetChildRepsList(string childOriginators);

        [OperationContract]
        List<OriginatorDTO> GetAllRepCodes(string sOriginator);

        [OperationContract]
        List<StateDTO> GetDistinctRepStates();

        [OperationContract]
        List<OriginatorDTO> GetRepGroupOriginators(string sOriginator);

        [OperationContract]
        List<OriginatorDTO> GetCRMReps();

        [OperationContract]
        List<OriginatorDTO> GetChildOriginatorsList(ArgsDTO args);

        [OperationContract]
        List<OriginatorDTO> GetRepForKeyAccounts(string distributor);

        [OperationContract]
        List<OriginatorDTO> GetOriginatorsByCatergory(ArgsDTO args);

        [OperationContract]
        List<OriginatorDTO> GetRepsByOriginator_For_Route_Assign(ArgsDTO args);

        [OperationContract]
        List<OriginatorDTO> GetRepsByOriginator(string rep, string username);

        [OperationContract]
        List<OriginatorDTO> GetRepsByASEOriginator(string ASE);

        [OperationContract]
        List<OriginatorDTO> GetDistributorsByLikeName(string username);

        //[OperationContract]
        //List<RepDTO> GetRepTargetsByDistributor(string username);

        [OperationContract]
        SalesInfoDetailViewStateDTO GetRepMonthleyTargetsByDistributor(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args);

        [OperationContract]
        SalesInfoDetailViewStateDTO GetTodaySalesDrilldown(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args);

        [OperationContract]
        List<OriginatorDTO> GetParentOriginators(string sOriginator);

        [OperationContract]
        List<OriginatorDTO> GetLevelThreeParentOriginators(string sOriginator);
        
        [OperationContract]
        List<OriginatorEMEIDTO> GetAllOriginatorEMEINos(string originator);

        /*               
        [OperationContract]
        List<OriginatorDTO> GetChildOriginatorsList(ArgsDTO args);
                                                                    
        */
        //[OperationContract]
        //List<OriginatorDTO> GetChildOriginatorsListForMessage(ArgsDTO args);

        //[OperationContract]
        //List<OriginatorDTO> GetFilterChildOriginatorsList(string filterByUserName, ArgsDTO args);

        [OperationContract]
        string GetUserNameByRepCode(string sRepCode);

        [OperationContract]
        bool UpdatePassword(string originator, string password);
        
        [OperationContract]
        bool DeleteEMEIFromOriginator(string originator, string emei_no);
        
        [OperationContract]
        bool ClearDeviceEMEIFromOriginator(string originator, string emei_no);
        
        [OperationContract]
        bool UpdateMobilePin();
        
        [OperationContract]
        List<OriginatorDTO> GetDistributorsByCatergoryWithNameAndCode(ArgsDTO args);
        
        [OperationContract]
        List<OriginatorDTO> GetAllOriginatorsByCatergory(ArgsDTO args);
        
        [OperationContract]
        List<OriginatorDTO> GetAllDistributorList(ArgsDTO args);

        [OperationContract]
        bool UpdateOriginatorTFAAuthenticationSettings(string originator, string tfaPin, bool isAuthenticated);


        #region "Sales Rep Interface"

        [OperationContract]
        List<OriginatorDTO> GetDistributersByASMOriginator(ArgsDTO args);


        #endregion


        #region "Sales Rep Interface"

        [OperationContract]
        List<OriginatorDTO> GetAllSRByOriginator(ArgsDTO args);

        #endregion

        #region Report

        [OperationContract]
        List<OriginatorDTO> GetASEsByDateForReport(ArgsDTO args, DateTime startDate, DateTime endDate);

        #endregion
    }
}
