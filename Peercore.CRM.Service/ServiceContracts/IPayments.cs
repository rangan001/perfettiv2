﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Payments")]
    public interface IPayments
    {
        #region - Monthly Payments -

        [OperationContract]
        List<PaymentModel> GetAllPayments(ArgsModel args);

        [OperationContract]
        bool SavePayment(PaymentModel paymentDetail);

        [OperationContract]
        bool UpdatePayment(PaymentModel paymentDetail);

        [OperationContract]
        bool DeletePayment(int paymentId, string originator);

        [OperationContract]
        bool UploadPayments(string fileName, string originator);

        #endregion
    }
}
