﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "PipelineStage")]
    public interface IPipelineStage
    {
        [OperationContract]
        List<PipelineStageDTO> GetPipelineStage(ArgsDTO args);

        [OperationContract]
        List<PipelineStageDTO> GetPipelineChart(ArgsDTO args);

        [OperationContract]
        bool SavePipelineStage(List<PipelineStageDTO> pipelineStageList, List<int> deletedStageList, ArgsDTO args);

        [OperationContract]
        bool PipelineStageCanDelete(int pipelineStageId);
        
        //[OperationContract]
        //List<PipelineStageGraphDTO> GetPipelineStageGraph(string sOriginator, bool IsCustomerPipeline, string childOriginators, string defaultDeptID);

        
    }
}
