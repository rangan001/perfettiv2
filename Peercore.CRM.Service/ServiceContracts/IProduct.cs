﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Product")]
    public interface IProduct
    {
        [OperationContract]
        List<ProductModel> GetAllProductsByTerritoryId(ArgsModel args, string TerritoryId);

        [OperationContract]
        List<TerritoryModel> GetAllTerritoriesByProductId(ArgsModel args, string ProductId);

        [OperationContract]
        List<ProductModel> GetAllProducts(ArgsModel args);

        [OperationContract]
        bool SaveUpdateTerritoryProducts(string originator,
                                                       string territory_id,
                                                       string product_id,
                                                       string is_checked);
        
        [OperationContract]
        ProductModel GetProductByProductCode(string productCode);
    }
}
