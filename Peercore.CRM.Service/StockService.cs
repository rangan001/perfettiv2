﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using Peercore.CRM.Service.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IStock
    {

        #region "Territory Products Stock"

        public ProductStockModel GetAllProductStockByTerritoryId(ArgsModel args, DateTime allocDate, int territoryId)
        {
            try
            {
                ProductStockModel obj = LoadStockBR.Instance.GetAllProductStockByTerritoryId(args, allocDate, territoryId);

                return obj;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveLoadStock(ProductStockModel loadStockModel)
        {
            try
            {
                return LoadStockBR.Instance.SaveLoadStock(loadStockModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UploadStockToBulkTerritory(List<ProductModel> lstProduct, ProductStockModel loadStock)
        {
            try
            {
                return LoadStockBR.Instance.UploadStockToBulkTerritory(lstProduct, loadStock);
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        public List<DBClaimInvoiceDetailsModel> GetAllDBClaimInvoiceDetailsByInvoiceId(ArgsModel args, string invoiceId)
        {
            try
            {
                return LoadStockBR.Instance.GetAllDBClaimInvoiceDetailsByInvoiceId(args, invoiceId);
            }
            catch
            {
                throw;
            }
        }
    }
}
