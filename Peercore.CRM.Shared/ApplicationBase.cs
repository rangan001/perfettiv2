﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Peercore.CRM.Shared.Constants;


namespace Peercore.CRM.Shared
{
    public class ApplicationBase
    {
        private static ApplicationBase instance;
        public static ApplicationBase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ApplicationBase();
                }

                return instance;
            }
        }

        private int startRecordNumber;
        public int StartRecordNumber
        {
            get { return startRecordNumber; }
            set { startRecordNumber = value; }
        }

        private int numberofrecord;
        public int NumberOfRecord
        {
            get { return numberofrecord; }
            set { numberofrecord = value; }
        }
    }
}
