﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Shared
{
    public class TransactionModules
    {
        public static string AttendanceReason = "Attendance Reason";
        public static string Invoice = "Invoice";
        public static string Order = "Order";
        public static string User = "User";
        public static string Customer = "Customer";
        //Route
        public static string RouteSequence = "Route Sequence";
        public static string RouteSchedule = "Route Schedule";
        //Discount 
        public static string DiscountSchemeTerritory = "Discount Scheme Territory";
        public static string DiscountScheme = "Discount Scheme";
        //Asset Management
        public static string AssetType = "Asset Type";
        public static string AssetMaster = "Asset Master";
        public static string AssetAssignEmployee = "Asset Assign Employee";
        public static string AssetAssignCustomer = "Asset Assign Customer";
        public static string UserTFA = "TFA User";
        public static string TradeDiscount = "Trade Discount";
    }

    public class TransactionTypeModules
    {
        public static string Insert = "Insert";
        public static string Update = "Update";
        public static string Delete = "Delete";
        public static string Login = "Login";
        public static string LogOut = "LogOut";
        public static string Assign = "Assign";
    }
}
