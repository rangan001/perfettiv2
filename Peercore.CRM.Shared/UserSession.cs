﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Peercore.CRM.Shared.Constants;

namespace Peercore.CRM.Shared
{
    public class UserSession
    {
        private static UserSession instance;
        public static UserSession Instance
        {
            get
            {
                if (HttpContext.Current.Session[GlobalConstants.GlobalValuesInstance] == null)
                    HttpContext.Current.Session[GlobalConstants.GlobalValuesInstance] = new UserSession();

                return HttpContext.Current.Session[GlobalConstants.GlobalValuesInstance] as UserSession;
            }
        }

        private string childOriginatorsString;
        public string ChildOriginatorsString
        {
            get { return childOriginatorsString; }
            set { childOriginatorsString = value; }
        }

        //private StringBuilder childOriginators;
        //public StringBuilder ChildOriginators = new StringBuilder();

        private string defaultDepartmentId;
        public string DefaultDepartmentId
        {
            get
            {
                return defaultDepartmentId;
            }
            set
            {
                if (defaultDepartmentId == value)
                    return;

                defaultDepartmentId = value;
            }
        }

        private string departmentId = string.Empty;
        public string DepartmentId
        {
            get
            {
                return departmentId;
            }
            set
            {
                if (departmentId == value)
                    return;

                departmentId = value;
            }
        }

        private string originatorString;
        public string OriginatorString
        {
            get { return originatorString; }
            set { originatorString = value; }
        }

        private string repGroups;
        public string RepGroups
        {
            get
            {
                return repGroups;
            }
            set
            {
                if (repGroups == value)
                    return;

                repGroups = value;
            }
        }

        private string originator = string.Empty;
        public string Originator
        {
            get
            {
                return originator;
            }
            set
            {
                if (originator == value)
                    return;

                originator = value;
            }
        }

        private string userName;
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                if (userName == value)
                    return;

                userName = value;
            }
        }

        private bool hasChildReps;
        public bool HasChildReps
        {
            get
            {
                return hasChildReps;
            }
            set
            {
                if (hasChildReps == value)
                    return;

                hasChildReps = value;
            }
        }

        private string bDMList;
        public string BDMList
        {
            get
            {
                return bDMList;
            }
            set
            {
                if (bDMList == value)
                    return;

                bDMList = value;
            }
        }

        private string child;
        public string Child
        {
            get
            {
                return child;
            }
            set
            {
                if (child == value)
                    return;

                child = value;
            }
        }

        private string unitsAbbriviation;
        public string UnitsAbbriviation
        {
            get { return unitsAbbriviation; }
            set { unitsAbbriviation = value; }
        }

        private bool isAmountVisible;
        public bool IsAmountVisible
        {
            get { return isAmountVisible; }
            set { isAmountVisible = value; }
        }

        private int originatorId;
        public int OriginatorId
        {
            get
            {
                return originatorId;
            }
            set
            {
                if (originatorId == value)
                    return;

                originatorId = value;
            }
        }

        private bool isCheckManagerMode;
        public bool IsCheckManagerMode
        {
            get { return isCheckManagerMode; }
            set { isCheckManagerMode = value; }
        }

        public string ClientType { get; set; }
        public string RepCode { get; set; }
        public string RepType { get; set; }         // Rep Type - Bakery / Food Services
        public List<string> RepCodeList { get; set; } // Originator can have multiple Rep Codes

        private string childReps = string.Empty;
        public string ChildReps
        {
            get
            {
                return childReps;
            }
            set
            {
                childReps = value;
            }
        }

        public string FilterByRepType { get; set; }     // Rep Type - of the Filter By Rep
        public string AccessToken { get; set; }

        public string TFAPin { get; set; }
        public bool IsTFAUser { get; set; }
        public bool TFAIsAuthenticated { get; set; }

        #region Params for Cookers

        private string defaultDepartmentIdCookers = string.Empty;
        public string DefaultDepartmentIdCookers
        {
            get
            {
                return defaultDepartmentIdCookers;
            }
            set
            {
                if (defaultDepartmentIdCookers == value)
                    return;

                defaultDepartmentIdCookers = value;
            }
        }

        private string originatorCookers = string.Empty;
        public string OriginatorCookers
        {
            get
            {
                return originatorCookers;
            }
            set
            {
                if (originatorCookers == value)
                    return;

                originatorCookers = value;
            }
        }

        private string childOriginators;
        public string ChildOriginators
        {
            get
            {
                return childOriginators;
            }
            set
            {
                if (childOriginators == value)
                    return;

                childOriginators = value;
            }
        }

        private bool managerMode = true;
        public bool ManagerMode
        {
            get
            {
                return managerMode;
            }
            set
            {
                if (managerMode == value)
                    return;

                managerMode = value;
            }
        }

        public string FilterByUserName { get; set; }

        public int FilterByRepOriginatorId { get; set; }

        private string originalUserName;
        public string OriginalUserName
        {
            get
            {
                return originalUserName;
            }
            set
            {
                if (originalUserName == value)
                    return;

                originalUserName = value;
            }
        }

        private string addressType = "A";
        public string AddressType
        {
            get
            {
                return addressType;
            }
            set
            {
                if (addressType == value)
                    return;

                addressType = value;
            }
        }

        private string originatorEmail;
        public string OriginatorEmail
        {
            get
            {
                return originatorEmail;
            }
            set
            {
                if (originatorEmail == value)
                    return;

                originatorEmail = value;
            }
        }

        private int crmAuthLevel;
        public int CRMAuthLevel
        {
            get
            {
                return crmAuthLevel;
            }
            set
            {
                if (crmAuthLevel == value)
                    return;

                crmAuthLevel = value;
            }
        }

        #endregion
    }
}
