﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Telerik.Web.UI;
using System.ComponentModel;
using System.Text;
using System.Globalization;
//using Outlook = Microsoft.Office.Interop.Outlook;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net.Mail;
using System.Threading;
using System.Net.Mime;
using System.IO;
using System.Web.UI;
using Peercore.CRM.Shared;

namespace Peercore.CRM.Common
{
    /// <summary>
    /// Summary description for CommonUtility
    /// </summary>
    public class CommonUtility
    {
        #region Constant
        public const string LEAD_DATA = "LEAD_DATA";
        public const string GLOBAL_SETTING = "GLOBAL_SETTING";
        public const string CUSTOMER_DATA = "CUSTOMER_DATA";
        public const string ENDUSER_DATA = "ENDUSER_DATA";
        public const string SAVE_OBJECT = "SAVE_OBJECT";
        public const string CUSTOMER_CODE = "CUSTOMER_CODE";
        public const string LEAD_CODE = "LEAD_CODE";
        public const string ACTIVITY_ID = "ACTIVITY_ID";
        public const string APPOINTMENT_ID = "APPOINTMENT_ID";
        public const string PARENTACTIVITY_ID = "PARENTACTIVITY_ID";
        public const string LEAD_DETAILS = "LEAD_DETAILS";
        public const string CUSTOMER_DETAILS = "CUSTOMER_DETAILS";
        public const string LSTUNSRNT_ACTIVITY = "LSTUNSRNT_ACTIVITY";
        public const string ACTIVITY_DELETE = "ACTIVITY_DELETE";
        public const string APPOINTMENT_DELETE = "APPOINTMENT_DELETE";
        public const string LEAD_ID = "LEAD_ID";
        public const string LEADSTAGE_ID = "LEADSTAGE_ID";
        public const string CONTACTPERSON_ID = "CONTACTPERSON_ID";
        public const string ORIGINATOR_LIST = "ORIGINATOR_LIST";
        public const string MESSAGE_LIST = "MESSAGE_LIST";
        public const string CLIENT_COLUMN_SETTING = "CLIENT_COLUMN_SETTING";
        public const string SORT_ORDER = "SORT_ORDER";
        public const string SORT_NAME = "SORT_NAME";
        public const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        public const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";
        public const string CLIENT_SORT_FLAG = "CLIENT_SORT_FLAG";
        public const string STRINGTIMEFORMAT = "yyyyMMdd\\THHmmss\\Z";
        public const string CLEAR_FLAG = "CLEAR_FLAG";
        public const string ORDER_BY = "ORDER_BY";
        public const string BDM_LIST = "BDM_LIST";
        public const string BACK_BUTTON = "BACK_BUTTON";
        public const string BACK_BUTTON_FOR_ALL = "BACK_BUTTON_FOR_ALL";
        public const string PIPELINE_DATA = "PIPELINE_DATA";
        public const string SELECTED_CUSTOMER = "SELECTED_CUSTOMER";
        public const string PIPELINE_HEADER = "PIPELINE_HEADER";
        public const string ERROR_LOG = "ERROR_LOG";
        public const string ERROR_MESSAGE = "Error Occurred";
        public const string BREADCRUMB = "BREADCRUMB";
        public const string ACTIVE_MODULE = "ACTIVE_MODULE";

        public const string CALL_CYCLE_ID = "CALL_CYCLE_ID";
        public const string CALL_CYCLE_CONTACTS = "CALL_CYCLE_CONTACTS";
        public const string SCHEDULE_LOG = "SCHEDULE_LOG";
        public const string SCHEDULETIME_LOG = "SCHEDULETIME_LOG";
        public const string ENDUSER_CODE = "ENDUSER_CODE";

        public const string SALES_INFO_DETAIL = "SALES_INFO_DETAIL";
        public const string SALES_INFO_DETAIL_ENDUSER = "SALES_INFO_DETAIL_ENDUSER";
        public const string SALES_INFO_DETAIL_BACK = "SALES_INFO_DETAIL_BACK";

        public const string ORIGINATOR_MARKETS = "ORIGINATOR_MARKETS";
        public const string STOCK_ALLOCATION_DATE = "STOCK_ALLOCATION_DATE";

        public const string CUSTOMER_TARGETS = "CUSTOMER_TARGETS";
        public const string REP_TARGETS = "REP_TARGETS";
        public const string DISTRIBUTOR_DATA = "DISTRIBUTOR_DATA";

        public const string CUSTOMER_PROGRAM_CONTACTS = "CUSTOMER_PROGRAM_CONTACTS";
        public const string VOLUME_PROGRAM_CONTACTS = "VOLUME_PROGRAM_CONTACTS";
        public const string SCHEME_HEADER = "SCHEME_HEADER";
        //public const string SCHEME_FREE_PRODUCT = "SCHEME_FREE_PRODUCT";
        public const string SCHEME_DETAILS = "SCHEME_DETAILS";
        public const string SCHEME_DETAIL_ID = "SCHEME_DETAIL_ID";
        public const string DIVISION_PRODUCTS = "DIVISION_PRODUCTS";
        public const string ITINERARY_HEADER_DATA = "ITINERARY_HEADER_DATA";

        #endregion

        public CommonUtility()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        //public string GridFilterString(Filter filterExpression)
        //{
        //    StringBuilder sb = new StringBuilder();


        //    if (filterExpression..DataTypeName.Trim().Equals("System.String"))
        //    {
        //        sb.Append("lower(" + expression.ColumnUniqueName + ")" + GetFilterMarks(((GridKnownFunction)Enum.Parse(typeof(GridKnownFunction),
        //                        expression.FilterFunction)), expression.DataTypeName.Trim(), expression.FieldValue) + " and ");
        //    }
        //    else
        //    {
        //        sb.Append(expression.ColumnUniqueName + GetFilterMarks(((GridKnownFunction)Enum.Parse(typeof(GridKnownFunction),
        //                        expression.FilterFunction)), expression.DataTypeName.Trim(), expression.FieldValue) + " and ");
        //    }

        //    return sb.ToString();
        //}

        //private string GetFilterMarks(GridKnownFunction filterfuntion,string dataTypeName,string fieldValue)
        // {
        //     StringBuilder sb = new StringBuilder();
        //     if (dataTypeName == "System.String")
        //     {
        //         if (filterfuntion == GridKnownFunction.Contains)
        //         {
        //             sb.Append(" like lower('%" + fieldValue + "%') ");
        //         }
        //         else if (filterfuntion == GridKnownFunction.DoesNotContain)
        //         {
        //             sb.Append(" not like lower('%" + fieldValue + "%') ");
        //         }
        //         else if (filterfuntion == GridKnownFunction.StartsWith)
        //         {
        //             sb.Append(" like " + "lower('" + fieldValue + "%')");
        //         }
        //         else if (filterfuntion == GridKnownFunction.EndsWith)
        //         {
        //             sb.Append(" like lower('%" + fieldValue + "')");
        //         }
        //         else if (filterfuntion == GridKnownFunction.EqualTo)
        //         {
        //             sb.Append(" in " + "lower(('" + fieldValue + "'))");
        //         }
        //         else if (filterfuntion == GridKnownFunction.NotEqualTo)
        //         {
        //             sb.Append(" not in " + "lower(('" + fieldValue + "'))");
        //         }
        //         else if (filterfuntion == GridKnownFunction.IsEmpty)
        //         {
        //             sb.Append(" in " + "lower(('" + fieldValue + "'))");
        //         }
        //         else if (filterfuntion == GridKnownFunction.NotIsEmpty)
        //         {
        //             sb.Append(" not in " + "lower(('" + fieldValue + "'))");
        //         }
        //         else if (filterfuntion == GridKnownFunction.GreaterThan)
        //         {
        //             sb.Append(" > " + "'" + fieldValue + "'");
        //         }
        //         else if (filterfuntion == GridKnownFunction.GreaterThanOrEqualTo)
        //         {
        //             sb.Append(" >= " + "'" + fieldValue + "'");
        //         }
        //         else if (filterfuntion == GridKnownFunction.LessThan)
        //         {
        //             sb.Append(" < " + "'" + fieldValue + "'");
        //         }
        //         else if (filterfuntion == GridKnownFunction.LessThanOrEqualTo)
        //         {
        //             sb.Append(" <= " + "'" + fieldValue + "'");
        //         }
        //     }
        //     else if (dataTypeName == "System.Boolean")
        //     {
        //         if (filterfuntion == GridKnownFunction.EqualTo)
        //         {
        //             sb.Append(" in " + "(" + fieldValue + ")");
        //         }
        //         else if (filterfuntion == GridKnownFunction.NotEqualTo)
        //         {
        //             sb.Append(" not in " + "(" + fieldValue + ")");
        //         }
        //     }
        //     else if (dataTypeName == "System.DateTime")
        //     {
        //         //string date = fieldValue.ToString("dd-MMM-yyyy HH:mm:ss");

        //         CultureInfo culture = new CultureInfo("en-AU", false); // use your culture info

        //         //Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-AU");

        //         DateTime datetime = DateTime.ParseExact(fieldValue, culture.DateTimeFormat.ShortDatePattern, CultureInfo.InvariantCulture); 

        //         //DateTime datetime = DateTime.ParseExact(fieldValue.Trim(), "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);



        //         if (filterfuntion == GridKnownFunction.EqualTo)
        //         {
        //             sb.Append(" = '" + datetime.ToString()+ "'");
        //         }
        //         else if (filterfuntion == GridKnownFunction.NotEqualTo)
        //         {
        //             sb.Append(" != '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //         }
        //         else if (filterfuntion == GridKnownFunction.GreaterThan)
        //         {
        //             sb.Append(" > '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //         }
        //         else if (filterfuntion == GridKnownFunction.GreaterThanOrEqualTo)
        //         {
        //             sb.Append(" >= '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //         }
        //         else if (filterfuntion == GridKnownFunction.LessThan)
        //         {
        //             sb.Append(" < '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //         }
        //         else if (filterfuntion == GridKnownFunction.LessThanOrEqualTo)
        //         {
        //             sb.Append(" <= '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //         }
        //     }
        //     else if (dataTypeName == "System.Int32" || dataTypeName == "System.Int64" || dataTypeName == "System.Double")
        //     {
        //         if (filterfuntion == GridKnownFunction.EqualTo)
        //         {
        //             sb.Append(" in " + "(" + fieldValue + ")");
        //         }
        //         else if (filterfuntion == GridKnownFunction.NotEqualTo)
        //         {
        //             sb.Append(" not in " + "(" + fieldValue + ")");
        //         }
        //         else if (filterfuntion == GridKnownFunction.GreaterThan)
        //         {
        //             sb.Append(" > " + fieldValue);
        //         }
        //         else if (filterfuntion == GridKnownFunction.GreaterThanOrEqualTo)
        //         {
        //             sb.Append(" >= " + fieldValue);
        //         }
        //         else if (filterfuntion == GridKnownFunction.LessThan)
        //         {
        //             sb.Append(" < " + fieldValue);
        //         }
        //         else if (filterfuntion == GridKnownFunction.LessThanOrEqualTo)
        //         {
        //             sb.Append(" <= " + fieldValue);
        //         }
        //     }

        //     if (filterfuntion == GridKnownFunction.IsNull)
        //     {
        //        // sb.Append(" = ISNULL");
        //        // return value.Equals(null);
        //     }
        //     else if (filterfuntion == GridKnownFunction.NotIsNull)
        //     {
        //        // return !value.Equals(null);
        //     }

        //     return sb.ToString();
        // }

        public void SetFilterField(ref Filter filter, string page, string leadstage = "")
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = SplitMethodForGetFieldName(item.Field, page, leadstage);
                }
            }
        }

        private string SplitMethodForGetFieldName(string fieldName, string page, string leadstage = "")
        {
            StringBuilder sb = new StringBuilder();

            switch (page)
            {
                case "leadcontact":
                    sb.Append(GetLeadContactField(fieldName, leadstage));
                    break;
                case "customeraddress":
                    sb.Append(GetCustomerAddressField(fieldName));
                    break;
                case "distributor":
                    sb.Append(GetDistributorField(fieldName));
                    break;
                case "enduser":
                    sb.Append(GetEndUserField(fieldName));
                    break;
                case "callcycles":
                    sb.Append(GetCallCyclesField(fieldName));
                    break;
                case "catalog":
                    sb.Append(GetCatalogField(fieldName));
                    break;

                case "market":
                    sb.Append(GetMarketField(fieldName));
                    break;

                case "paymentsettlements":
                    sb.Append(GetPaymentSettlementField(fieldName));
                    break;

                case "assignedroutes":
                    sb.Append(GetAssignedRoutesField(fieldName));
                    break;

                case "allroutemaster":
                    sb.Append(GetAllRouteMaster(fieldName));
                    break;

                case "customertargets":
                    sb.Append(GetCustomerTargetsField(fieldName));
                    break;

                case "allmarketobjectives":
                    sb.Append(GetMarketObjectivesField(fieldName));
                    break;
                case "programtargets":
                    sb.Append(GetProgramTargetsField(fieldName));
                    break;
                case "allbrandsdata":
                    sb.Append(GetBrandsDataField(fieldName));
                    break;
                case "allproductsdata":
                    sb.Append(GetProductsDataField(fieldName));
                    break;
                case "outletsforschedule":
                    sb.Append(GetOutltsForScheduleDataField(fieldName));
                    break;
                case "allproductpricesdata":
                    sb.Append(GetProductPricesDataField(fieldName));
                    break;
                case "schemeheaders":
                    sb.Append(GetSchemeHeaderDataField(fieldName));
                    break;
                case "allPackingTypedata":
                    sb.Append(GetPackingTypeDataField(fieldName));
                    break;
                case "allProductCategorydata":
                    sb.Append(GetProductCategoryDataField(fieldName));
                    break;
                case "DivisionProduct":
                    sb.Append(GetDivisionProductDataField(fieldName));
                    break;
                case "invoices":
                    sb.Append(GetInvoicesDataField(fieldName));
                    break;
                case "getallusersbydeptstring":
                    sb.Append(GetAllUsersDataField(fieldName));
                    break;
                case "reptargets":
                    sb.Append(GetRepTargetsDataField(fieldName));
                    break;
                case "allchecklistdata":
                    sb.Append(GetCheckListDataField(fieldName));
                    break;

                //For Product Discount Filter
                case "ProductTerritoryDiscount":
                    sb.Append(GetProductTerritoryDiscount(fieldName));
                    break;

                //For Distributer Territory Assign Filter
                case "DistributerTerritoryAssignFilter":
                    sb.Append(GetDistributerTerritoryAssignFilter(fieldName));
                    break;
                    
                //For Distributer Territory Assign Filter
                case "SRTerritoryAssignFilter":
                    sb.Append(GetSRTerritoryAssignFilter(fieldName));
                    break;
                
                //For Distributer Territory Assign Filter
                case "customer":
                    sb.Append(GetCustomerFilter(fieldName));
                    break; 
                    
                //For Distributer Territory Assign Filter
                case "confirmpendingcustomer":
                    sb.Append(GetConfirmPendingCustomerFilter(fieldName));
                    break; 
                    
                //For Distributer Territory Assign Filter
                case "GetAllSalesRepWithSalesType":
                    sb.Append(GetAllSalesRepWithSalesType(fieldName));
                    break;
                    
                //For Distributer Territory Assign Filter
                case "ordersview":
                    sb.Append(OrdersView(fieldName));
                    break;
            }

            return sb.ToString();
        }

        #region FieldNameMethods
        private string GetLeadContactField(string fieldName, string leadStage)
        {
            string realfield = "";

            try
            {
                if (leadStage.ToLower() != "enduser")
                {
                    if (fieldName == "Name")
                    {
                        realfield = "Name";
                    }
                    else if (fieldName == "BDM")
                    {
                        realfield = "Originator";
                    }
                    else if (fieldName == "LeadStage")
                    {
                        realfield = "lead_stage";
                    }
                    else if (fieldName == "CustomerCode")
                    {
                        realfield = "cust_code";
                    }
                    else if (fieldName == "State")
                    {
                        realfield = "state";
                    }
                    else if (fieldName == "StartDate")
                    {
                        realfield = "StartDate";
                    }
                    else if (fieldName == "City")
                    {
                        realfield = "City";
                    }
                    else if (fieldName == "Channel")
                    {
                        realfield = "ChannelDescription";
                    }
                    else if (fieldName == "Address")
                    {
                        realfield = "address_1";
                    }
                    else if (fieldName == "SourceId")
                    {
                        realfield = "lead_id";
                    }
                    else if (fieldName == "DistributorCode")
                    {
                        realfield = "dis_code";
                    }
                    else if (fieldName == "DistributorName")
                    {
                        realfield = "dis_name";
                    }
                    else if (fieldName == "LastInvoiceDate")
                    {
                        realfield = "Last_invoice_date";
                    }
                    else if (fieldName == "RouteName")
                    {
                        realfield = "route_name";
                    }
                    else if (fieldName == "RepCode")
                    {
                        realfield = "originator";
                    }
                    else if (fieldName == "RepName")
                    {
                        realfield = "rep_name";
                    }
                }
                else
                {
                    if (fieldName == "CustomerCode")
                    {
                        realfield = "cust_code";
                    }
                    else if (fieldName == "EndUserCode")
                    {
                        realfield = "enduser_code";
                    }
                    else if (fieldName == "Name")
                    {
                        realfield = "name";
                    }
                    else if (fieldName == "Address1")
                    {
                        realfield = "address_1";
                    }
                    else if (fieldName == "Address2")
                    {
                        realfield = "address_2";
                    }

                    else if (fieldName == "City")
                    {
                        realfield = "city";
                    }
                    else if (fieldName == "PostCode")
                    {
                        realfield = "postcode";
                    }
                    else if (fieldName == "State")
                    {
                        realfield = "state";
                    }
                    else if (fieldName == "Telephone")
                    {
                        realfield = "telephone";
                    }
                    else if (fieldName == "Contact")
                    {
                        realfield = "contact";
                    }

                    else if (fieldName == "RepCode")
                    {
                        realfield = "rep_code";
                    }
                    else if (fieldName == "EmailAddress")
                    {
                        realfield = "email_address";
                    }
                    else if (fieldName == "Version")
                    {
                        realfield = "version";
                    }

                    else if (fieldName == "CustomerName")
                    {
                        realfield = "cust_name";
                    }
                    else if (fieldName == "LastInvoiceDate")
                    {
                        realfield = "Last_invoice_date";
                    }
                }
                return realfield;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private string GetCustomerAddressField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "Address1":
                    realfield = "address_1";
                    break;
                case "Address2":
                    realfield = "address_2";
                    break;
                case "City":
                    realfield = "city";
                    break;
                case "State":
                    realfield = "state";
                    break;
                case "PostCode":
                    realfield = "postcode";
                    break;
                case "Country":
                    realfield = "country";
                    break;
                case "Name":
                    realfield = "name";
                    break;
                case "Contact":
                    realfield = "contact";
                    break;
                case "Telephone":
                    realfield = "telephone";
                    break;
                case "Fax":
                    realfield = "fax";
                    break;
            }

            return realfield;
        }

        private string GetDistributorField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "Code":
                    realfield = "c.cust_code ";
                    break;
                default:
                    realfield = "c.name";
                    break;
            }

            return realfield;
        }
        
        private string GetCustomerFilter(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "OutletCode":
                    realfield = "cu.cust_code ";
                    break;
                case "OutletName":
                    realfield = "cu.name ";
                    break;
                case "RouteMasterId":
                    realfield = " cu.route_assign_id ";
                    break;
                default:
                    realfield = "c.name";
                    break;
            }

            return realfield;
        }
        
        private string GetConfirmPendingCustomerFilter(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "OutletCode":
                    realfield = "cu.cust_code ";
                    break;
                case "Name":
                    realfield = "cu.name ";
                    break;
                case "Address":
                    realfield = " cu.address_1 ";
                    break;
                case "RouteName":
                    realfield = " crm_route_master.name ";
                    break;
                case "TerritoryName":
                    realfield = " crm_mast_territory.territory_name ";
                    break;
                case "OutletTypeName":
                    realfield = " crm_outlet_type.outlettype_name ";
                    break;
                default:
                    realfield = "cu.name";
                    break;
            }

            return realfield;
        }

        private string GetCallCyclesField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "Code":
                    realfield = "callcycle_id";
                    break;
                case "Description":
                    realfield = "description";
                    break;
                default:
                    realfield = "";
                    break;
            }

            return realfield;
        }

        private string GetCatalogField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "CatlogId":
                    realfield = "catlog_id";
                    break;
                case "CatlogCode":
                    realfield = "catlog_code";
                    break;
                case "Description":
                    realfield = "description";
                    break;
                case "DeliveryFrequency":
                    realfield = "product_type";
                    break;
                default:
                    realfield = "";
                    break;
            }

            return realfield;
        }

        private string GetEndUserField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "Code":
                    realfield = "de.enduser_code ";
                    break;
                default:
                    realfield = "de.name";
                    break;
            }

            return realfield;
        }

        private string GetMarketField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "MarketId":
                    realfield = "market_id ";
                    break;
                case "MarketName":
                    realfield = "market_name";
                    break;
                default:
                    realfield = "realfield";
                    break;
            }

            return realfield;
        }

        private string GetPaymentSettlementField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "ChequeNumber":
                    realfield = "c.cheque_number ";
                    break;
                case "OutletBank":
                    realfield = "c.outlet_bank";
                    break;
                case "Status":
                    realfield = "c.status";
                    break;
                case "CreatedBy":
                    realfield = "c.created_by";
                    break;
                case "Amount":
                    realfield = "c.amount";
                    break;
                case "ChequeDate":
                    realfield = "c.cheque_date";
                    break;
                default:
                    realfield = "c.cheque_number";
                    break;
            }

            return realfield;
        }

        private string GetAssignedRoutesField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "RouteName":
                    realfield = " rm.name ";
                    break;
                case "RepCode":
                    realfield = " rt.rep_code";
                    break;   
                case "RepName":
                    realfield = " rp.name";
                    break;              
                default:
                    realfield = " rm.name";
                    break;
            }
            return realfield;
        }

        private string GetAllRouteMaster(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "RouteMasterCode":
                    realfield = " crm_route_master.code ";
                    break;
                case "RouteMasterName":
                    realfield = " crm_route_master.name ";
                    break; 
                case "TerritoryId":
                    realfield = " crm_route_master.territory_id ";
                    break;
                case "TerritoryName":
                    realfield = " crm_mast_territory.territory_name ";
                    break;
                case "RepCode":
                    realfield = " rp.rep_code ";
                    break;
                case "RepName":
                    realfield = " rp.name";
                    break;
                default:
                    realfield = " rm.name";
                    break;
            }
            return realfield;
        }

        private string GetCustomerTargetsField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "Name":
                    realfield = " c.Name ";
                    break;
                case "StickTarget":
                    realfield = " ct.stick_target";
                    break;
                default:
                    realfield = " c.Name";
                    break;
            }
            return realfield;
        }

        private string GetMarketObjectivesField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "Code":
                    realfield = " ob.code ";
                    break;
                case "Description":
                    realfield = " ob.description";
                    break;
                default:
                    realfield = " ob.description";
                    break;
            }
            return realfield;
        }

        private string GetProgramTargetsField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "Program.Name":
                    realfield = " p.name ";
                    break;
                case "EffStartDate":
                    realfield = " pt.eff_start_date";
                    break;
                case "EffEndDate":
                    realfield = " pt.eff_end_date";
                    break;
                default:
                    realfield = " p.name";
                    break;
            }
            return realfield;
        }

        private string GetBrandsDataField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "BrandCode":
                    realfield = " code ";
                    break;
                case "BrandName":
                    realfield = " name";
                    break;
                default:
                    realfield = " name";
                    break;
            }
            return realfield;
        }

        private string GetProductsDataField(string fieldName)
        {
            string realfield = "";

            switch (fieldName)
            {
                case "Code":
                    realfield = " p.code ";
                    break;
                case "Product Code":
                    realfield = " p.code ";
                    break;
                case "FgCode":
                    realfield = " fg_code ";
                    break;
                case "Name":
                    realfield = " p.name ";
                    break;
                case "Product Name":
                    realfield = " p.name ";
                    break;
                case "BrandName":
                    realfield = " b.name";
                    break;
                case "CategoryName":
                    realfield = " pc.description ";
                    break;
                default:
                    realfield = " name";
                    break;
            }
            return realfield;
        }

        private string GetOutltsForScheduleDataField(string fieldName)
        {
            string realfield = "lead_name";

            switch (fieldName)
            {
                case "Contact.CustomerCode":
                    realfield = " cust_code ";
                    break;
                case "Contact.Name":
                    realfield = " lead_name";
                    break;
                case "Contact.Address":
                    realfield = " address";
                    break;
                case "Contact.City":
                    realfield = " city";
                    break;
                case "Contact.State":
                    realfield = " State";
                    break;
                case "Contact.PostalCode":
                    realfield = " postcode";
                    break;
                default:
                    realfield = " lead_name";
                    break;
            }
            return realfield;
        }

        private string GetProductPricesDataField(string fieldName)
        {
            string realfield = "name";

            switch (fieldName)
            {
                case "Name":
                    realfield = " name ";
                    break;
                case "Code":
                    realfield = " code";
                    break;
                case "RetailPrice":
                    realfield = " outlet_retail_price";
                    break;
                case "OutletPrice":
                    realfield = " outlet_consumer_price";
                    break;
                case "DistributorPrice":
                    realfield = " distributor_price ";
                    break;
                default:
                    realfield = " name";
                    break;
            }
            return realfield;
        }

        private string GetSchemeHeaderDataField(string fieldName)
        {
            string realfield = "sh.scheme_name";

            switch (fieldName)
            {
                case "SchemeName":
                    realfield = " sh.scheme_name ";
                    break;
                case "StartDate":
                    realfield = " sh.start_date ";
                    break;
                case "EndDate":
                    realfield = " sh.end_date ";
                    break;
                case "DivisionName":
                    realfield = " d.division_name ";
                    break;
                default:
                    realfield = " sh.scheme_name ";
                    break;
            }
            return realfield;
        }

        private string GetPackingTypeDataField(string fieldName)
        {
            string realfield = "product_packing_id";

            switch (fieldName)
            {
                case "Code":
                    realfield = " product_packing_id ";
                    break;
                case "Description":
                    realfield = " packing_name ";
                    break;
                default:
                    realfield = " packing_name ";
                    break;
            }
            return realfield;
        }

        private string GetProductCategoryDataField(string fieldName)
        {
            string realfield = "id";

            switch (fieldName)
            {
                case "Code":
                    realfield = " id ";
                    break;
                case "Description":
                    realfield = " description ";
                    break;
                default:
                    realfield = " description ";
                    break;
            }
            return realfield;
        }

        private string GetDivisionProductDataField(string fieldName)
        {
            string realfield = "id";

            switch (fieldName)
            {
                case "DivisionName":
                    realfield = " division_name ";
                    break;
                case "Name":
                    realfield = " name ";
                    break;
                case "FgCode":
                    realfield = " fg_code ";
                    break;
                default:
                    realfield = " name ";
                    break;
            }
            return realfield;
        }

        private string GetInvoicesDataField(string fieldName)
        {
            string realfield = " ih.invoice_no ";

            switch (fieldName)
            {
                case "InvoiceNo":
                    realfield = " ih.invoice_no ";
                    break;
                case "CustCode":
                    realfield = " ih.cust_code ";
                    break;
                case "InvoiceDate":
                    realfield = " ih.invoice_date ";
                    break;
                case "GrossTotal":
                    realfield = " ih.gross_total ";
                    break;

                case "DiscountTotal":
                    realfield = " ih.discount_total ";
                    break;
                case "ReturnsTotal":
                    realfield = " returns_total ";
                    break;
                case "GrandTotal":
                    realfield = " ih.grand_total ";
                    break;
                case "RepCode":
                    realfield = " ih.rep_code ";
                    break;
                case "CustomerName":
                    realfield = " c.name ";
                    break;
                case "Status":
                    realfield = " ih.status ";
                    break;
                default:
                    realfield = " ih.invoice_no ";
                    break;
            }
            return realfield;
        }

        private string GetRepTargetsDataField(string fieldName)
        {
            string realfield = " r.territory_name ";

            switch (fieldName)
            {
                case "RepCode":
                    realfield = " r.territory_code ";
                    break;
                case "RepName":
                    realfield = " r.territory_name ";
                    break;
                case "PrimaryTarAmt":
                    realfield = " rt.primary_tar_amt ";
                    break;
                case "PrimaryIncAmt":
                    realfield = " rt.primary_inc_amt ";
                    break;
                default:
                    realfield = " r.territory_name ";
                    break;
            }
            return realfield;
        }

        private string GetCheckListDataField(string fieldName)
        {
            string realfield = " r.name ";

            switch (fieldName)
            {
                case "RepName":
                    realfield = " r.name ";
                    break;
                case "PrimaryTarAmt":
                    realfield = " rt.primary_tar_amt ";
                    break;
                case "PrimaryIncAmt":
                    realfield = " rt.primary_inc_amt ";
                    break;
                default:
                    realfield = " r.name ";
                    break;
            }
            return realfield;
        }
        
        private string GetProductTerritoryDiscount(string fieldName)
        {
            string realfield = " crm_mast_territory.territory_name ";

            switch (fieldName)
            {
                case "TerritoryCode":
                    realfield = " crm_mast_territory.territory_code ";
                    break;
                case "TerritoryName":
                    realfield = " crm_mast_territory.territory_name ";
                    break;
                case "AreaName":
                    realfield = " crm_mast_area.area_name ";
                    break;
                default:
                    realfield = " crm_mast_territory.territory_name ";
                    break;
            }
            return realfield;
        } 
        
        private string GetAllSalesRepWithSalesType(string fieldName)
        {
            string realfield = " crm_mast_territory.territory_name ";

            switch (fieldName)
            {
                case "RepCode":
                    realfield = " rep.rep_code ";
                    break;
                case "RepName":
                    realfield = " rep.name ";
                    break;
                case "TerritoryCode":
                    realfield = " crm_mast_territory.territory_code ";
                    break;
                case "TerritoryName":
                    realfield = " crm_mast_territory.territory_name ";
                    break;
                case "AsmName":
                    realfield = " crm_mast_asm.asm_name ";
                    break;
                case "DistributerName":
                    realfield = " crm_distributor.name ";
                    break;
                default:
                    realfield = " rep.name ";
                    break;
            }
            return realfield;
        } 
        
        private string OrdersView(string fieldName)
        {
            string realfield = " ";

            switch (fieldName)
            {
                case "OrderNo":
                    realfield = " oh.order_no ";
                    break;
                case "CustomerCode":
                    realfield = " oh.cust_code ";
                    break;
                case "OrderDate":
                    realfield = " oh.order_date ";
                    break;
                case "OrderTotal":
                    realfield = " oh.gross_total ";
                    break;
                case "DayCount":
                    realfield = " DATEDIFF(day, oh.order_date, GETDATE()) ";
                    break;
                default:
                    realfield = " crm_mast_territory.territory_name ";
                    break;
            }
            return realfield;
        } 
        
        private string GetDistributerTerritoryAssignFilter(string fieldName)
        {
            string realfield = " crm_mast_territory.territory_name ";

            switch (fieldName)
            {
                case "TerritoryCode":
                    realfield = " crm_mast_territory.territory_code ";
                    break;
                case "TerritoryName":
                    realfield = " crm_mast_territory.territory_name ";
                    break;
                case "AreaName":
                    realfield = " crm_mast_area.area_name ";
                    break;
                case "AsmName":
                    realfield = " crm_mast_asm.asm_name ";
                    break;
                case "DistributerName":
                    realfield = " crm_distributor.name ";
                    break;
                default:
                    realfield = " crm_mast_territory.territory_name ";
                    break;
            }
            return realfield;
        }

        private string GetSRTerritoryAssignFilter(string fieldName)
        {
            string realfield = " rep.rep_code ";

            switch (fieldName)
            {
                case "RepCode":
                    realfield = " rep.rep_code ";
                    break;
                case "RepName":
                    realfield = " rep.name ";
                    break;
                case "TerritoryId":
                    realfield = " crm_mast_territory.territory_name ";
                    break;
                default:
                    realfield = " rep.rep_code ";
                    break;
            }
            return realfield;
        }
        

        #endregion

        //public bool SendMail(string from, string to, string subject, string body,
        //    string sAttachmentPath = "", bool bSendAsHTML = false)
        //{
        //    string _message = "";
        //    try
        //    {
        //        string[] emailArray = to.Split(';');

        //        foreach (string email in emailArray)
        //        {
        //            // validate the email address
        //            string sTextToValidate = email.Trim();
        //            Regex expression = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$");
        //            // Regex expression = new Regex(@"\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}");

        //            // if the email address is invalid, return message
        //            if (!expression.IsMatch(sTextToValidate))
        //            {
        //                _message = "Invalid recipient email address: " + to;
        //            }
        //        }

        //        System.Net.Mail.SmtpClient smtp = new SmtpClient(ConfigUtil.SmtpServer);
        //        smtp.Port = ConfigUtil.SmtpPort;

        //        if (from != null && to != null && from != "" && to != "")
        //        {
        //            MailMessage email = new MailMessage(from, to, subject, body);
        //            email.IsBodyHtml = true;

        //            smtp.Send(email);
        //        }

        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public bool SendMail(string from, string to, string subject, string body,
            string sAttachmentPath = "", bool bSendAsHTML = false)
        {
            string _message = "";
            try
            {
                string[] emailArray = to.Split(';');

                foreach (string email in emailArray)
                {
                    // validate the email address
                    string sTextToValidate = email.Trim();
                    Regex expression = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$");
                    // Regex expression = new Regex(@"\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}");

                    // if the email address is invalid, return message
                    if (!expression.IsMatch(sTextToValidate))
                    {
                        _message = "Invalid recipient email address: " + to;
                    }
                }

                System.Net.Mail.SmtpClient smtp = new SmtpClient(ConfigUtil.SmtpServer);
                //System.Net.Mail.SmtpClient smtp = new SmtpClient("192.9.208.23");
                smtp.Port = ConfigUtil.SmtpPort;
                //smtp.Credentials = new System.Net.NetworkCredential(from, "pwd");
                //if (from != null && to != null && from != "" && to != "")
                //{
                //    MailMessage email = new MailMessage("indunilu@peercore.com.au", "indunilu@peercore.com.au", subject, body);
                //    email.IsBodyHtml = true;

                //    smtp.Send(email);

                //}

                if (from != null && to != null && from != "" && to != "")
                {
                    /*MailMessage email = new MailMessage(from, to, subject, body);
                    email.IsBodyHtml = true;

     email.To.Clear();
                    foreach (string item in emailArray)
                    {
                        email.To.Add(new MailAddress(item.Trim()));
                    }
                    smtp.Send(email);*/

                    MailMessage email = new MailMessage();
                    MailAddress fromAddress = new MailAddress(from);
                    email.From = fromAddress;
                    email.IsBodyHtml = true;

                    // To address collection of MailAddress
                    foreach (string toAddress in emailArray)
                    {
                        email.To.Add(toAddress);
                    }
                    email.Subject = subject + " (Web)";

                    // Message body contentb
                    email.Body = body;

                    // Send SMTP mail
                    smtp.Send(email);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SendMail(string from, string to, string[] cc, string[] bcc, string subject, string body, MailPriority mailPriority,
            string sAttachmentPath = "", bool bSendAsHTML = false)
        {
            string _message = "";
            try
            {
                string[] emailArray = to.Split(';');

                foreach (string email in emailArray)
                {
                    // validate the email address
                    string sTextToValidate = email.Trim();
                    Regex expression = new Regex(@"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$");

                    // if the email address is invalid, return message
                    if (!expression.IsMatch(sTextToValidate))
                    {
                        _message = "Invalid recipient email address: " + to;
                    }
                }

                System.Net.Mail.SmtpClient smtp = new SmtpClient(ConfigUtil.SmtpServer);
                smtp.Port = ConfigUtil.SmtpPort;

                if (from != null && to != null && from != "" && to != "")
                {
                    MailMessage email = new MailMessage(from, to, subject, body);

                    if (cc != null)
                    {
                        if (cc.Length > 0)
                        {
                            foreach (string address in cc)
                            {
                                if (!string.IsNullOrEmpty(address))
                                {
                                    email.CC.Add(new MailAddress(address, address));
                                }
                            }
                        }
                    }

                    if (bcc != null)
                    {
                        if (bcc.Length > 0)
                        {
                            foreach (string address in bcc)
                            {
                                if (!string.IsNullOrEmpty(address))
                                {
                                    email.Bcc.Add(new MailAddress(address, address));
                                }
                            }
                        }
                    }

                    email.IsBodyHtml = true;
                    email.Priority = mailPriority;
                    smtp.Send(email);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }


        public bool SendEmail(List<string> toEmailList, List<string> ccEmailList, string subject, string body, List<string> attachmentpathList)
        {
            bool status = false;

            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(ConfigUtil.FromMail);
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Priority = MailPriority.High;

                    //Adding recepients Email List
                    if (toEmailList != null)
                    {
                        foreach (string addressText in toEmailList)
                        {
                            if (!string.IsNullOrWhiteSpace(addressText))
                            {
                                mailMessage.To.Add(new MailAddress(addressText));
                            }
                        }
                    }

                    //Adding to Email List
                    if (ccEmailList != null)
                    {
                        foreach (string ccText in ccEmailList)
                        {
                            if (!string.IsNullOrWhiteSpace(ccText))
                            {
                                mailMessage.CC.Add(new MailAddress(ccText));
                            }
                        }
                    }


                    //Adding attachments List
                    if (attachmentpathList != null)
                    {
                        foreach (string path in attachmentpathList)
                        {
                            mailMessage.Attachments.Add(new Attachment(path));
                        }
                    }

                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = ConfigUtil.SmtpServer;
                    smtp.Port = ConfigUtil.SmtpPort;
                    //smtp.EnableSsl = Convert.ToBoolean(ConfigUtil.EnableSSL);

                    //System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    //NetworkCred.UserName = ConfigUtil.FromMail;
                    //NetworkCred.Password = ConfigUtil.FromPassword; 

                    //smtp.UseDefaultCredentials = true;
                    //smtp.Credentials = NetworkCred;

                    smtp.Send(mailMessage);

                    //messageText = "Email sent sucessfully";
                    status = true;
                }
            }
            catch (Exception ex)
            {
                //messageText = "Email sending Failed..!";
                status = false;
                throw;

            }

            return status;
        }

        public bool SendEmailOut(List<string> toEmailList, List<string> ccEmailList, string subject, string body, List<string> attachmentpathList)
        {
            bool hassent = false;

            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(ConfigUtil.FromMail, "Nuca2272");
            client.Port = ConfigUtil.SmtpPort; // You can use Port 25 if 587 is blocked (mine is!)
            client.Host = ConfigUtil.SmtpServer;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;

            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(ConfigUtil.FromMail);
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Priority = MailPriority.High;

                    //Adding recepients Email List
                    if (toEmailList != null)
                    {
                        foreach (string addressText in toEmailList)
                        {
                            if (!string.IsNullOrWhiteSpace(addressText))
                            {
                                mailMessage.To.Add(new MailAddress(addressText));
                            }
                        }
                    }

                    //Adding to Email List
                    if (ccEmailList != null)
                    {
                        foreach (string ccText in ccEmailList)
                        {
                            if (!string.IsNullOrWhiteSpace(ccText))
                            {
                                mailMessage.CC.Add(new MailAddress(ccText));
                            }
                        }
                    }

                    //Adding attachments List
                    if (attachmentpathList != null)
                    {
                        foreach (string path in attachmentpathList)
                        {
                            mailMessage.Attachments.Add(new Attachment(path));
                        }
                    }

                    client.Send(mailMessage);
                    hassent = true;
                }
            }
            catch (Exception ex)
            {
                hassent = false;
            }
            return hassent;
        }

        public void SendToOutlookAsTask(string Subject, string Body, DateTime Start, DateTime End, int iReminderActivityId)
        {

            //Microsoft.Office.Interop.Outlook.Application outlookApp = new Microsoft.Office.Interop.Outlook.Application();
            //Microsoft.Office.Interop.Outlook.TaskItem oTask = (Microsoft.Office.Interop.Outlook.TaskItem)
            //    outlookApp.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olTaskItem);

            //oTask.Subject = Subject;
            //oTask.Body = Body;

            //// Set the start date            
            //oTask.StartDate = Start;

            //// End date 
            ////oAppointment.Duration = 60;
            //oTask.DueDate = End;
            //// Set the reminder 15 minutes before start
            //oTask.ReminderSet = true;
            //oTask.ReminderTime = Start;

            ////Setting the sound file for a reminder: 
            //oTask.ReminderPlaySound = true;
            ////set ReminderSoundFile to a filename. 

            ////Setting the importance: 
            ////use OlImportance enum to set the importance to low, medium or high

            //oTask.Importance = Microsoft.Office.Interop.Outlook.OlImportance.olImportanceHigh;

            ///* OlBusyStatus is enum with following values:
            //    olBusy
            //    olFree
            //    olOutOfOffice
            //    olTentative
            //    */
            //oTask.Status = Microsoft.Office.Interop.Outlook.OlTaskStatus.olTaskNotStarted;

            //oTask.UserProperties.Add("ActivityId", Microsoft.Office.Interop.Outlook.OlUserPropertyType.olText,
            //    true, Microsoft.Office.Interop.Outlook.OlFormatText.olFormatTextText);
            //oTask.UserProperties["ActivityId"].Value = iReminderActivityId;

            //// Save the appointment
            //oTask.Save();

            //// When you call the Save () method, the appointment is saved in Outlook. Another useful method is 
            ////ForwardAsVcal () which can be used to send the Vcs file via email. 

            ////Outlook.MailItem mailItem = oAppointment.ForwardAsVcal();
            ////mailItem.To = "hussain@peercore.com.au";
            ////mailItem.Send();
        }

        public bool SendToOutlookAsAppointment(string subject, string body, DateTime starttime, DateTime endtime, string organizerEmail,
            string organizerName,
            string attendeeName, string attendeeEmail)
        {
            bool hasSent = false;


            string strStartTime = "";
            string strEndTime = "";
            string strTimeStamp = "";
            string strTempStartTime = "";
            string strTempEndTime = "";
            string vCalendarFile = "";
            string filePath = "";

            System.Net.Mail.SmtpClient smtp = new SmtpClient(ConfigUtil.SmtpServer);
            smtp.Port = ConfigUtil.SmtpPort;

            // VCalendar Format.
            const string VCAL_FILE =
            "BEGIN:VCALENDAR\n" +
            "VERSION:1.0\n" +
            "BEGIN:VEVENT\n" +
            "DTSTART{0}\n" +
            "DTEND{1}\n" +
                //"LOCATION;ENCODING=QUOTED-PRINTABLE:{2}\n" +
            "DESCRIPTION;ENCODING=QUOTED-PRINTABLE:{2}\n" +
            "SUMMARY;ENCODING=QUOTED-PRINTABLE:{3}\n" +
            "TRIGGER:-PT15M\n" +
            "PRIORITY:3\n" +
            "END:VEVENT\n" +
            "END:VCALENDAR";

            strTempStartTime = string.Format("{0} {1}",
            starttime.ToShortDateString(), starttime.ToLongTimeString());
            strTempEndTime = string.Format("{0} {1}",
            starttime.ToShortDateString(), endtime.ToLongTimeString());
            strTimeStamp = (DateTime.Parse(strTempStartTime)).ToUniversalTime().ToString(STRINGTIMEFORMAT);
            strStartTime = string.Format(":{0}", strTimeStamp);
            strEndTime = string.Format(":{0}",
            (DateTime.Parse(strTempEndTime)).ToUniversalTime().ToString(STRINGTIMEFORMAT));

            vCalendarFile =
            String.Format(VCAL_FILE, strStartTime, strEndTime, body, subject,
            attendeeEmail.Trim(), attendeeName, organizerEmail.Trim(), organizerName.Trim());

            filePath += "\\" + subject + ".ics";
            TextWriter tw = new StreamWriter(filePath);

            // write a line of text to the file
            tw.WriteLine(vCalendarFile.ToString());

            // close the stream
            tw.Close();

            try
            {
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(organizerEmail);
                mail.To.Add(attendeeEmail);

                mail.Subject = subject;

                // create the attachment
                System.Net.Mail.Attachment attachment = new Attachment(filePath, MediaTypeNames.Application.Octet);
                // Attach
                mail.Attachments.Add(attachment);

                smtp.Send(mail);
            }
            catch (Exception)
            {
                hasSent = false;
                throw;
            }
            return hasSent;
        }

        public MailMessage CreateMeetingRequest(DateTime start, DateTime end, string subject, string body,
        string organizerEmail, string organizerName, string attendeeName, string attendeeEmail)
        {
            MailAddressCollection col = new MailAddressCollection();
            col.Add(new MailAddress(attendeeEmail, attendeeName));
            return CreateMeetingRequest(start, end, subject, body, organizerEmail, organizerName, col);
        }

        public MailMessage CreateMeetingRequest(DateTime start, DateTime end, string subject, string body,
           string organizerEmail, string organizerName, MailAddressCollection attendeeList)
        {
            MailMessage msg = new MailMessage();

            //  Set up the different mime types contained in the message
            System.Net.Mime.ContentType textType = new System.Net.Mime.ContentType("text/plain");
            System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
            System.Net.Mime.ContentType calendarType = new System.Net.Mime.ContentType("text/calendar");

            //  Add parameters to the calendar header
            calendarType.Parameters.Add("method", "REQUEST");
            calendarType.Parameters.Add("name", "meeting.ics");

            //  Create message body parts
            //  create the Body in text format
            //string bodyText = "Type:Single Meeting\r\nOrganizer: {0}\r\nStart Time:{1}\r\nEnd Time:{2}\r\nTime Zone:{3}\r\nLocation: {4}\r\n\r\n*~*~*~*~*~*~*~*~*~*\r\n\r\n{5}";
            string bodyText = "Type:Single Meeting\r\nStart Time:{1}\r\nEnd Time:{2}\r\nTime Zone:{2}\r\n\r\n*~*~*~*~*~*~*~*~*~*\r\n\r\n{3}";
            bodyText = string.Format(bodyText,
                start.ToLongDateString() + " " + start.ToLongTimeString(),
                end.ToLongDateString() + " " + end.ToLongTimeString(),
                System.TimeZone.CurrentTimeZone.StandardName,
                body);

            AlternateView textView = AlternateView.CreateAlternateViewFromString(bodyText, textType);
            msg.AlternateViews.Add(textView);

            //create the Body in HTML format
            string bodyHTML = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\r\n<HTML>\r\n<HEAD>\r\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-8\">\r\n<META NAME=\"Generator\" CONTENT=\"MS Exchange Server version 6.5.7652.24\">\r\n<TITLE>{0}</TITLE>\r\n</HEAD>\r\n<BODY>\r\n<!-- Converted from text/plain format -->\r\n<P><FONT SIZE=2>Type:Single Meeting<BR>\r\nStart Time:{1}<BR>\r\nEnd Time:{2}<BR>\r\nTime Zone:{3}<BR>\r\n<BR>\r\n*~*~*~*~*~*~*~*~*~*<BR>\r\n<BR>\r\n{4}<BR>\r\n</FONT>\r\n</P>\r\n\r\n</BODY>\r\n</HTML>";
            bodyHTML = string.Format(bodyHTML,
                body,
                start.ToLongDateString() + " " + start.ToLongTimeString(),
                end.ToLongDateString() + " " + end.ToLongTimeString(),
                System.TimeZone.CurrentTimeZone.StandardName,
                body);

            AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(bodyHTML, HTMLType);
            msg.AlternateViews.Add(HTMLView);

            //create the Body in VCALENDAR format
            string calDateFormat = "yyyyMMddTHHmmssZ";
            string bodyCalendar = "BEGIN:VCALENDAR\r\nMETHOD:REQUEST\r\nPRODID:Microsoft CDO for Microsoft Exchange\r\nVERSION:2.0\r\nBEGIN:VTIMEZONE\r\nTZID:(GMT-06.00) Central Time (US & Canada)\r\nX-MICROSOFT-CDO-TZID:11\r\nBEGIN:STANDARD\r\nDTSTART:16010101T020000\r\nTZOFFSETFROM:-0500\r\nTZOFFSETTO:-0600\r\nRRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=11;BYDAY=1SU\r\nEND:STANDARD\r\nBEGIN:DAYLIGHT\r\nDTSTART:16010101T020000\r\nTZOFFSETFROM:-0600\r\nTZOFFSETTO:-0500\r\nRRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=3;BYDAY=2SU\r\nEND:DAYLIGHT\r\nEND:VTIMEZONE\r\nBEGIN:VEVENT\r\nDTSTAMP:{7}\r\nDTSTART:{0}\r\nSUMMARY:{6}\r\nUID:{4}\r\nATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=\"{8}\":MAILTO:{8}\r\nACTION;RSVP=TRUE;CN=\"{3}\":MAILTO:{3}\r\nORGANIZER;CN=\"{2}\":mailto:{3}\r\nDTEND:{1}\r\nDESCRIPTION:{6}\\N\r\nSEQUENCE:1\r\nPRIORITY:5\r\nCLASS:\r\nCREATED:{7}\r\nLAST-MODIFIED:{7}\r\nSTATUS:CONFIRMED\r\nTRANSP:OPAQUE\r\nX-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\nX-MICROSOFT-CDO-INSTTYPE:0\r\nX-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\r\nX-MICROSOFT-CDO-ALLDAYEVENT:FALSE\r\nX-MICROSOFT-CDO-IMPORTANCE:1\r\nX-MICROSOFT-CDO-OWNERAPPTID:-1\r\nX-MICROSOFT-CDO-ATTENDEE-CRITICAL-CHANGE:{7}\r\nX-MICROSOFT-CDO-OWNER-CRITICAL-CHANGE:{7}\r\nBEGIN:VALARM\r\nACTION:DISPLAY\r\nDESCRIPTION:REMINDER\r\nTRIGGER;RELATED=START:-PT00H15M00S\r\nEND:VALARM\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n";
            bodyCalendar = string.Format(bodyCalendar,
                start.ToUniversalTime().ToString(calDateFormat),
                end.ToUniversalTime().ToString(calDateFormat),
                organizerName,
                organizerEmail,
                Guid.NewGuid().ToString("B"),
                body,
                subject,
                DateTime.Now.ToUniversalTime().ToString(calDateFormat),
                attendeeList.ToString());

            AlternateView calendarView = AlternateView.CreateAlternateViewFromString(bodyCalendar, calendarType);
            calendarView.TransferEncoding = TransferEncoding.SevenBit;
            msg.AlternateViews.Add(calendarView);

            //  Adress the message
            msg.From = new MailAddress(organizerEmail);
            foreach (MailAddress attendee in attendeeList)
            {
                msg.To.Add(attendee);
            }
            msg.Subject = subject;
            msg.IsBodyHtml = true;
            return msg;
        }

        public string GetErrorMessage(string errormessage)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<div id=\"messagered\"  class=\"message-red\" rel=\"error\">");
            sb.Append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
            sb.Append("<tr>");
            sb.Append("<td class=\"red-left\">" + errormessage.Trim() + "</td>");
            sb.Append("<td class=\"red-right\"><a class=\"close-red\"><img src=\"" + ConfigUtil.ApplicationPath + "assets/images/icon_close_red.gif\" onclick=\"divHide()\"   alt=\"\" /></a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");

            return sb.ToString();
        }

        public string GetInfoMessage(string errormessage)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<div id=\"messageblue\"  class=\"message-blue\" rel=\"error\">");
            sb.Append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
            sb.Append("<tr>");
            sb.Append("<td class=\"blue-left\">" + errormessage.Trim() + "</td>");
            sb.Append("<td class=\"blue-right\"><a class=\"close-blue\"><img src=\"" + ConfigUtil.ApplicationPath + "assets/images/icon_close_blue.PNG\" onclick=\"divHide()\"   alt=\"\" /></a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");

            return sb.ToString();
        }

        public string GetLengthyErrorMessage(string errormessage)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<div id=\"messagered\"  class=\"message-red-length_height\" rel=\"error\">");
            sb.Append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
            sb.Append("<tr>");
            sb.Append("<td class=\"red-length_height-left\">" + errormessage.Trim() + "</td>");
            sb.Append("<td class=\"red-length_height-right\"><a class=\"close-red\"><img src=\"" + ConfigUtil.ApplicationPath + "assets/images/icon_close_red_length_height.png\" onclick=\"divHide()\"   alt=\"\" /></a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");

            return sb.ToString();
        }

        public string GetSucessfullMessage(string message)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<div id=\"messagegreen\"  class=\"message-green\" rel=\"sucess\">");
            //sb.Append("<div class=\"green-left\">Error. <a>\"" + message.Trim() + "\"</a></div>");
            //sb.Append("<div class=\"green-right\"><a class=\"close-green\"><img src=\"" + ConfigUtil.ApplicationPath + "assets/images/icon_close_green.gif\"   alt=\"\" /></a></div>");
            sb.Append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
            sb.Append("<tr>");
            sb.Append("<td class=\"green-left\">" + message.Trim() + "</td>");
            sb.Append("<td class=\"green-right\"><a class=\"close-green\"><img src=\"" + ConfigUtil.ApplicationPath + "assets/images/icon_close_green.gif\" onclick=\"divHide()\"   alt=\"\" /></a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");

            return sb.ToString();
        }

        public string GetWarningMessage(string message)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<div id=\"messageyellow\"  class=\"message-yellow\" rel=\"sucess\">");
            //sb.Append("<div class=\"green-left\">Error. <a>\"" + message.Trim() + "\"</a></div>");
            //sb.Append("<div class=\"green-right\"><a class=\"close-green\"><img src=\"" + ConfigUtil.ApplicationPath + "assets/images/icon_close_green.gif\"   alt=\"\" /></a></div>");
            sb.Append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
            sb.Append("<tr>");
            sb.Append("<td class=\"yellow-left\">" + message.Trim() + "</td>");
            sb.Append("<td class=\"yellow-right\"><a class=\"close-yellow\"><img src=\"" + ConfigUtil.ApplicationPath + "assets/images/icon_close_yellow.png\" onclick=\"divHide()\"   alt=\"\" /></a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</div>");

            return sb.ToString();
        }
        
        public string GetQuestionMessage(string message)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("return confirm('");
            sb.Append(message);
            sb.Append("');");

            return sb.ToString();
            
        }

        public int PageCount(int allCount, int rowCount)
        {
            if ((allCount % rowCount) < rowCount && (allCount % rowCount) != 0)
                return (allCount / rowCount) + 1;
            else
                return (allCount / rowCount);
        }

        private string GetAllUsersDataField(string fieldName)
        {
            string realfield = " ori.name ";

            switch (fieldName)
            {
                case "Name":
                    realfield = " ori.name ";
                    break;
                //for originator grid
                case "Originator":
                    realfield = " ori.originator ";
                    break;
                case "UserName":
                    realfield = " ori.name ";
                    break;
                case "designation":
                    realfield = " ori.designation ";
                    break;
                case "type_des":
                    realfield = " ut.type_des ";
                    break;
                case "Mobile":
                    realfield = " ori.otp_mobile ";
                    break;
                default:
                    realfield = " ori.name ";
                    break;
            }

            return realfield;
        }
    }
}