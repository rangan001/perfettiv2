﻿using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;
using CRMServiceReference;
using Peercore.CRM.Shared;
using System.Drawing;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;



[WebService]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class activity_service : System.Web.Services.WebService {
    private WebServiceAppointmentController _controller;

    /// <summary>
    /// The WebServiceAppointmentController class is used as a facade to the SchedulerProvider.
    /// </summary>
    //private WebServiceAppointmentController Controller
    //{
    //    get
    //    {
    //        if (_controller == null)
    //        {
    //            _controller = new WebServiceAppointmentController(new XmlSchedulerProvider(Server.MapPath("~/App_Code/Appointments_Outlook.xml"), true));
    //        }

    //        return _controller;
    //    }
    //}

    [WebMethod(EnableSession = true)]
    public IEnumerable<Telerik.Web.UI.AppointmentData> GetAppointments(SchedulerInfo schedulerInfo)
    {        
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ArgsModel args = new ArgsModel();
        ArgsDTO argsDTO = new ArgsDTO();
        ActivityClient activityClient = new ActivityClient();
        List<Telerik.Web.UI.AppointmentData> appointmentDataList = null;
        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            //args.Originator = UserSession.Instance.UserName;
            args.Originator = string.IsNullOrEmpty(UserSession.Instance.FilterByUserName) ? UserSession.Instance.UserName : UserSession.Instance.FilterByUserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.SToday = string.IsNullOrEmpty(argsDTO.SToday) ? "" : argsDTO.SToday;
            args.SStartDate = schedulerInfo.ViewStart.ToString("yyyy/MM/dd");
            args.SEndDate = schedulerInfo.ViewEnd.ToString("yyyy/MM/dd");
            args.SSource = "Planner";
            args.Floor = string.IsNullOrEmpty(argsDTO.Floor) ? "" : argsDTO.Floor;
            args.Status = string.IsNullOrEmpty(argsDTO.Status) ? "" : argsDTO.Status;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.RepCode = argsDTO.RepCode;
            appointmentDataList = LoadActivitiesToShedularNew(activityClient.GetAllActivitiesNew(args, null));
        }

        return appointmentDataList;
    }

    [WebMethod(EnableSession = true)]
    public IEnumerable<Telerik.Web.UI.AppointmentData> GetAppointmentsKendo(SchedulerInfo schedulerInfo)
    {

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ArgsDTO args = new ArgsDTO();
        ActivityClient activityClient = new ActivityClient();
        List<Telerik.Web.UI.AppointmentData> appointmentDataList = null;
        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            //args.Originator = UserSession.Instance.UserName;
            args.Originator = string.IsNullOrEmpty(UserSession.Instance.FilterByUserName) ? UserSession.Instance.UserName : UserSession.Instance.FilterByUserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.SToday = string.IsNullOrEmpty(args.SToday) ? "" : args.SToday;
            args.SStartDate = schedulerInfo.ViewStart.ToString("yyyy/MM/dd");
            args.SEndDate = schedulerInfo.ViewEnd.ToString("yyyy/MM/dd");
            args.SSource = "Planner";
            args.Floor = string.IsNullOrEmpty(args.Floor) ? "" : args.Floor;
            args.Status = string.IsNullOrEmpty(args.Status) ? "" : args.Status;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.RepCode = "";

            appointmentDataList = LoadActivitiesToShedular(activityClient.GetAllActivities(args, args.ActivityStatus));
        }
        return appointmentDataList;
    }

    private List<Telerik.Web.UI.AppointmentData> LoadActivitiesToShedularNew(List<CustomerActivityModel> customerActivityList)
    {
        List<Telerik.Web.UI.AppointmentData> lstAppoinment = new List<Telerik.Web.UI.AppointmentData>();

        if (customerActivityList != null)
        {
            foreach (CustomerActivityModel activity in customerActivityList)
            {
                Telerik.Web.UI.AppointmentData newApp = new Telerik.Web.UI.AppointmentData();
                //ContactAppointment newApp = new ContactAppointment();
                //newApp.ContactName = !string.IsNullOrEmpty(activity.LeadName) ? activity.LeadName + " - " : string.Empty;
                newApp.Start = activity.StartDate;
                newApp.End = activity.EndDate;
                newApp.Subject = activity.Subject;
                //newApp.Body = activity.Comments;
                newApp.ID = activity.ActivityID.ToString();

                //if (activity.LeadName != "")
                //    newApp.Subject = activity.LeadName + " - " + activity.Subject;
                //else
                newApp.Subject = activity.Subject;
                newApp.Description = activity.RepName;
                newApp.RecurrenceParentID = activity.routeId;
                newApp.EncodedID = activity.RepCode;

                // newApp.
                //Dictionary<string, string> ff = new Dictionary<string, string>();
                //ff.Add("BackColor", activity.TypeColour);

                newApp.Resources.Add(new ResourceData() { Type = "ActivityTypes", Text = activity.ActivityType, Key = activity.ActivityType });// ColorTranslator.FromHtml(activity.TypeColour);

                //if (activity.ActivityType != "")
                //    newApp.Category = rsActivity.Categories.GetCategoryByName(activity.ActivityType);

                // Using Location to store Lead / Customer (L / C)
                // Using Url to store LeadID / CustCode
                if (!string.IsNullOrWhiteSpace(activity.EndUserCode))   // END USER
                {

                    newApp.Attributes.Add("Location", "E");
                    newApp.Attributes.Add("Url", activity.EndUserCode + ":" + activity.CustCode);

                }
                else if (string.IsNullOrWhiteSpace(activity.EndUserCode) && !string.IsNullOrWhiteSpace(activity.CustCode))   // CUSTOMER
                {
                    newApp.Attributes.Add("Location", "C");
                    newApp.Attributes.Add("Url", activity.CustCode);
                }
                else if (activity.LeadId > 0)   // LEAD
                {
                    newApp.Attributes.Add("Location", "L");
                    newApp.Attributes.Add("Url", activity.LeadId.ToString());
                }
                else    // ACTIVITY WITHOUT A CUSTOMER OR A LEAD
                {
                    newApp.Attributes.Add("Location", "");
                    newApp.Attributes.Add("Url", "");
                }
                newApp.Attributes.Add("LeadName", activity.LeadName);
                lstAppoinment.Add(newApp);
            }
        }

        return lstAppoinment;
    }


    private List<Telerik.Web.UI.AppointmentData> LoadActivitiesToShedular(List<CustomerActivitiesDTO> customerActivityList)
    {
        List<Telerik.Web.UI.AppointmentData> lstAppoinment = new List<Telerik.Web.UI.AppointmentData>();

        if (customerActivityList != null)
        {
            foreach (CustomerActivitiesDTO activity in customerActivityList)
            {
                Telerik.Web.UI.AppointmentData newApp = new Telerik.Web.UI.AppointmentData();
                //ContactAppointment newApp = new ContactAppointment();
                //newApp.ContactName = !string.IsNullOrEmpty(activity.LeadName) ? activity.LeadName + " - " : string.Empty;
                newApp.Start = activity.StartDate;
                newApp.End = activity.EndDate;
                newApp.Subject = activity.Subject;
                //newApp.Body = activity.Comments;
                newApp.ID = activity.ActivityID.ToString();

                //if (activity.LeadName != "")
                //    newApp.Subject = activity.LeadName + " - " + activity.Subject;
                //else
                newApp.Subject = activity.Subject;
                newApp.Description = activity.RepName;
                newApp.RecurrenceParentID = activity.routeId;
                newApp.EncodedID = activity.RepCode;

                // newApp.
                //Dictionary<string, string> ff = new Dictionary<string, string>();
                //ff.Add("BackColor", activity.TypeColour);

                newApp.Resources.Add(new ResourceData() { Type = "ActivityTypes", Text = activity.ActivityType, Key = activity.ActivityType });// ColorTranslator.FromHtml(activity.TypeColour);

                //if (activity.ActivityType != "")
                //    newApp.Category = rsActivity.Categories.GetCategoryByName(activity.ActivityType);

                // Using Location to store Lead / Customer (L / C)
                // Using Url to store LeadID / CustCode
                if (!string.IsNullOrWhiteSpace(activity.EndUserCode))   // END USER
                {

                    newApp.Attributes.Add("Location", "E");
                    newApp.Attributes.Add("Url", activity.EndUserCode + ":" + activity.CustCode);

                }
                else if (string.IsNullOrWhiteSpace(activity.EndUserCode) && !string.IsNullOrWhiteSpace(activity.CustCode))   // CUSTOMER
                {
                    newApp.Attributes.Add("Location", "C");
                    newApp.Attributes.Add("Url", activity.CustCode);
                }
                else if (activity.LeadId > 0)   // LEAD
                {
                    newApp.Attributes.Add("Location", "L");
                    newApp.Attributes.Add("Url", activity.LeadId.ToString());
                }
                else    // ACTIVITY WITHOUT A CUSTOMER OR A LEAD
                {
                    newApp.Attributes.Add("Location", "");
                    newApp.Attributes.Add("Url", "");
                }
                newApp.Attributes.Add("LeadName", activity.LeadName);
                lstAppoinment.Add(newApp);
            }
        }

        return lstAppoinment;
    }


    [WebMethod(EnableSession = true)]
    public IEnumerable<AppointmentData> InsertAppointment(SchedulerInfo schedulerInfo, AppointmentData appointmentData)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        AppointmentAdded(appointmentData);
        return GetAppointments(schedulerInfo);
    }

    private void AppointmentAdded(AppointmentData app)
    {

        bool iNoRecs = false;
        CommonClient commonClient = new CommonClient();
        LookupTableDTO activityStatus = commonClient.GetDefaultLookupEntry("ACST", "PS");
        LookupTableDTO activityPriority = commonClient.GetDefaultLookupEntry("ACPR", "PS");

        ActivityClient oActivity = new ActivityClient();
        ActivityDTO activity = new ActivityDTO();
        activity.Subject = app.Subject;
        activity.Comments = app.Description; // app.Body
        activity.StartDate = app.Start;
        activity.EndDate = app.End;
        activity.ReminderDate = app.Start;

        if (activityStatus != null)
            activity.Status = activityStatus.TableCode;

        if (activityPriority != null)
            activity.Priority = activityPriority.TableCode;

        // When there is no activity set the activity as Cold Call.
       if (app.Resources.Count != 0)
            activity.ActivityType = app.Resources[0].Key.ToString();
        else
           activity.ActivityType = "COLD";
  

        //if (app.Category == null && defaultActivity != null)
        //    activity.ActivityType = defaultActivity.Code;
        //else if (app.Category != null)
        //    activity.ActivityType = app.Category.CategoryName;
        //else
        //    activity.ActivityType = string.Empty;

        //activity.ActivityType = app.Category.CategoryName;
        activity.LastModifiedBy = activity.CreatedBy = UserSession.Instance.UserName;
        activity.LastModifiedDate = activity.CreatedDate = DateTime.Now;
        activity.AssignedTo = UserSession.Instance.UserName;
        activity.SendReminder = "N";

        try
        {
            int oActivityid = 0;
            iNoRecs = oActivity.SaveActivity(activity, ref oActivityid);

            if (iNoRecs)
            {
                //app.UniqueId = activity.ActivityID.ToString();
                //app.Category = rsActivity.Categories.GetCategoryByName(activity.ActivityType);
                //GlobalValues.GetInstance().ShowMessage("Successfully Saved.", GlobalValues.MessageImageType.Information);
                //MessageBox.Show("Successfully Saved.");
            }
        }
        catch (Exception oException)
        {
        }
    }

    [WebMethod(EnableSession = true)]
    public IEnumerable<AppointmentData> UpdateAppointment(SchedulerInfo schedulerInfo, AppointmentData appointmentData)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ChangeAppointment(appointmentData);
        return GetAppointments(schedulerInfo);
        //return Controller.UpdateAppointment(schedulerInfo, appointmentData);
    }

    public static String Decode(String query)
    {
        return query.Replace("_", " ").Replace("@46@", ".");
    }

    private void ChangeAppointment(AppointmentData editedAppt)
    {
        bool iNoRecs = false;

        ActivityClient oActivity = new ActivityClient();
        ActivityDTO activity = new ActivityDTO();
        activity.ActivityID = Convert.ToInt32(editedAppt.ID);

        //string[] aSubject = editedAppt.Subject.Split('-');
        //if (aSubject.Length == 2)
        //{
        //    activity.Subject = aSubject[1];
        //}
        //else if (aSubject.Length == 1)
        //{
        //    activity.Subject = aSubject[0];
        //}
        //else
        //{
        //    activity.Subject = "";
        //}

        activity.Subject = editedAppt.Subject;
        activity.Comments = editedAppt.Description;
        activity.StartDate = editedAppt.Start;
        activity.EndDate = editedAppt.End;
        activity.ActivityType = editedAppt.Resources[0].Key.ToString();
        activity.LastModifiedBy = UserSession.Instance.UserName;
        activity.LastModifiedDate = DateTime.Now;
        AppointmentClient oAppointment = new AppointmentClient();
        //CRMAppointment appointment = new CRMAppointment();
        AppointmentDTO appointment = new AppointmentDTO();

        appointment.StartTime = editedAppt.Start; ;
        appointment.EndTime = editedAppt.End;
        appointment.Category = editedAppt.Resources[0].Key.ToString();

        appointment.Subject = activity.Subject;

        appointment.Body = editedAppt.Description;

        try
        {
            iNoRecs = oActivity.UpdateActivityFromPlanner(activity,UserSession.Instance.OriginatorString);

            // If there is a related appointment synchronize ONLY the comments
            if (iNoRecs)
            {
                CustomerActivitiesDTO ActivityWithApp = oActivity.GetActivityAppoitmentId(int.Parse(editedAppt.ID.ToString()));

                if (ActivityWithApp.AppointmentId > 0)
                {
                    appointment.Body = editedAppt.Description;
                    appointment.AppointmentID = ActivityWithApp.AppointmentId;

                    oAppointment.AppointmentUpdate(appointment, true, false);

                }
                if (ActivityWithApp.AppointmentId > 0)
                {
                    //appointment.Body = editedAppt.Description;
                    //appointment.AppointmentID = ActivityWithApp.AppointmentId;

                    //oAppointment.Update(appointment, false, false, true);
                }
            }


            //if (iNoRecs > 0)
            //{
            //    GlobalValues.GetInstance().ShowMessage("Successfully Saved.", GlobalValues.MessageImageType.Information);
            //}
        }
        catch (Exception oException)
        {

        }

    }

    [WebMethod]
    public IEnumerable<AppointmentData> CreateRecurrenceException(SchedulerInfo schedulerInfo, AppointmentData recurrenceExceptionData)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        return GetAppointments(schedulerInfo);
    }

    [WebMethod]
    public IEnumerable<AppointmentData> RemoveRecurrenceExceptions(SchedulerInfo schedulerInfo, AppointmentData masterAppointmentData)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        return GetAppointments(schedulerInfo);
    }

    [WebMethod(EnableSession = true)]
    public IEnumerable<AppointmentData> DeleteAppointment(SchedulerInfo schedulerInfo, AppointmentData appointmentData, bool deleteSeries)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        //CustomerActivitiesDTO customerActivities = new CustomerActivitiesDTO();
        //ActivityDTO activity = new ActivityDTO();
        //activity.ActivityID = int.Parse(appointmentData.ID.ToString());
        //ActivityClient oActivity = new ActivityClient();
        //AppointmentClient oAppointment = new AppointmentClient();
        //customerActivities = oActivity.GetActivityAppoitmentId(activity.ActivityID);

        //bool iNoofRecs = oActivity.DeleteActivity(activity);

        //if (iNoofRecs)
        //{
        //    if (customerActivities.AppointmentId > 0 && customerActivities.DelFlag != "Y")
        //    {
        //        activity.AppointmentID = customerActivities.AppointmentId;

        //        //if (clsGlobal.GetInstance().ConfirmMessageBox("Do you want to delete the related appointment for this activity?") == true)
        //        //{
        //            iNoofRecs = oActivity.ResetAppointment(activity);
        //            if (iNoofRecs)
        //            {
        //                AppointmentDTO crmAppointment = new AppointmentDTO();
        //                crmAppointment.AppointmentID = customerActivities.AppointmentId;
        //                iNoofRecs = oAppointment.AppointmentUpdate(crmAppointment, false, true);
        //                if (iNoofRecs)
        //                    ;//GlobalValues.GetInstance().ShowMessage("Successfully deleted.", GlobalValues.MessageImageType.Information);
        //                else
        //                    ;// GlobalValues.GetInstance().ShowMessage("Delete failed.", GlobalValues.MessageImageType.Error);
        //            }
        //            else
        //                ;// GlobalValues.GetInstance().ShowMessage("Delete failed.", GlobalValues.MessageImageType.Error);

        //        //}
        //    }
        //    else
        //        ;// GlobalValues.GetInstance().ShowMessage("Successfully deleted.", GlobalValues.MessageImageType.Information);
        //}

        return GetAppointments(schedulerInfo);
    }

    [WebMethod(EnableSession = true)]
    public IEnumerable<ResourceData> GetResources(SchedulerInfo schedulerInfo)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<ResourceData> resourceDataList = new List<ResourceData>();
        ActivityClient activityClient = new ActivityClient();
       // List<ActivityDTO> activity = activityClient.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, true);
        List<ActivityDTO> activity = activityClient.GetAllActivityTypes("PS", true);

        foreach (ActivityDTO item in activity)
        {
           // if (item.ShowInPlanner == "Y")
            //{
                ResourceData resourceData = new ResourceData();
                resourceData.Text = item.Description;
                resourceData.Key = item.Code;
                resourceData.Available = true;
                resourceData.Type = "ActivityTypes";

                //Dictionary<string, System.Drawing.Color> dictionary = new Dictionary<string, System.Drawing.Color>();
                //dictionary.Add("BackColor", ColorTranslator.FromHtml(item.ActivityTypeColour));

                resourceDataList.Add(resourceData);
           // }
        }
        return resourceDataList;
        //return Controller.GetResources(schedulerInfo);
    }

    //[WebMethod(Description = "Your Description")]
    //[ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    [WebMethod]
    public static string FunctionName()
    {
        //List<LeadAddressDTO> addressList = new List<LeadAddressDTO>();
        //LeadClient leadAddress = new LeadClient();
        //addressList = leadAddress.GetLeadAddreses(int.Parse("110"));

        //var retval = new JavaScriptSerializer()
        //    .Serialize(addressList);

        //Context.Response.Write(retval);

        // Return JSON data
        //JavaScriptSerializer js = new JavaScriptSerializer();
        //string retJSON = js.Serialize(addressList);

        //return retJSON;

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var jsonData = new
        {
            total = 1, // we'll implement later 
            page = 1,
            records = 3, // implement later 
            rows = new[]{
              new {id = 1, cell = new[] {"1", "-7", "Is this a good question?", "yay"}},
              new {id = 2, cell = new[] {"2", "15", "Is this a blatant ripoff?", "yay"}},
              new {id = 3, cell = new[] {"3", "23", "Why is the sky blue?", "yay"}}
            }
        };

        return ser.Serialize(jsonData); //products.ToString();
    }
}
