﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for asset_service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class asset_service : System.Web.Services.WebService
{

    public asset_service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region - Asset Type -

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllAssetTypes(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        AssetsClient assetClient = new AssetsClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllAssetTypeSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " asset_type_name asc";
            }
        }
        else
        {
            args.OrderBy = " asset_type_name asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterAssetTypeField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<AssetTypeModel> data = new List<AssetTypeModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = assetClient.GetAllAssetTypes(args);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveAssetType(string assetType, string description, string Originator, string id)
    {
        ArgsModel args = new ArgsModel();
        AssetsClient assetService = new AssetsClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (assetService.GetAllAssetTypes(args).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            AssetTypeModel model = new AssetTypeModel();
            model.AssetTypeName = assetType;
            model.Description = description;
            model.CreatedBy = Originator;

            bool delstatus = false;

            if (String.IsNullOrEmpty(id))
            {
                delstatus = assetService.SaveAssetType(model);

                if (delstatus)
                {
                    try
                    {
                        CommonClient cClient = new CommonClient();
                        cClient.CreateTransactionLog(Originator,
                            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            TransactionTypeModules.Login,
                            "Asset Type",
                            Originator + " save new asset type " + assetType);
                    }
                    catch { }
                }
            }
            else
            {
                delstatus = assetService.UpdateAssetType(Int32.Parse(id), assetType, description, Originator);

                if (delstatus)
                {
                    try
                    {
                        CommonClient cClient = new CommonClient();
                        cClient.CreateTransactionLog(Originator,
                            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            TransactionTypeModules.Login,
                            "Asset Type",
                            Originator + " update asset type " + assetType);
                    }
                    catch { }
                }
            }

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    private static string GetAllAssetTypeSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " AssetTypeName asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Description":
                sortStr = " description " + sortoption;
                break;
            case "AssetTypeName":
                sortStr = " asset_type_name " + sortoption;
                break;
            default:
                sortStr = " asset_type_id asc ";
                break;
        }

        return sortStr;
    }

    private string GetFilterAssetTypeField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "AssetTypeName":
                realfield = "asset_type_name";
                break;
            case "Description":
                realfield = "description";
                break;
        }

        return realfield;
    }

    [WebMethod(EnableSession = true)]
    public Peercore.CRM.Model.ResponseModel IsAssetTypeAvailable(string assetType)
    {
        AssetsClient assetService = new AssetsClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        response.Status = assetService.IsAssetTypeAvailable(assetType);
        response.StatusCode = "Exsist";

        return response;
    }

    #endregion

    #region - Asset Master -

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllAssets(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        AssetsClient assetClient = new AssetsClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllAssetSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " asset_type_id asc";
            }
        }
        else
        {
            args.OrderBy = " asset_type_id asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterAssetField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<AssetModel> data = new List<AssetModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = assetClient.GetAllAssets(args);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveAsset(string id, string assetType, string assetCode, string assetModel, string imei_1, string imei_2,
        string serialNo, string compAssetCode, string printerPouch, string monitorModel, string monitorSerial, string SIM, string invNo,
        string supplier, string value, string purchasedate, string warranty, string status, string description, string Originator)
    {
        ArgsModel args = new ArgsModel();
        AssetsClient assetService = new AssetsClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (assetService.GetAllAssets(args).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            AssetModel model = new AssetModel();
            model.AssetTypeId = Int32.Parse(assetType);
            model.AssetCode = assetCode;
            model.Model = assetModel;
            model.IMEI_1 = imei_1;
            model.IMEI_2 = imei_2;
            model.SerialNo = serialNo;
            model.CompAssetCode = compAssetCode;
            model.PrinterPouch = printerPouch;
            model.MonitorModel = monitorModel;
            model.MonitorSerial = monitorSerial;
            model.SIM = SIM;
            model.Supplier = supplier;
            model.InvoiceNo = invNo;
            model.ValueLKR = value;
            model.PurchaseDate = Convert.ToDateTime((purchasedate == "") ? "1900/01/01" : purchasedate);
            model.Warranty = warranty;
            model.AsssetStatus = Int32.Parse(status);
            model.Description = description;

            bool delstatus = false;

            if (String.IsNullOrEmpty(id))
            {
                model.CreatedBy = Originator;
                if (model.AsssetStatus == 0)
                    model.AsssetStatus = 1;
                delstatus = assetService.SaveAsset(model);

                if (delstatus)
                {
                    try
                    {
                        CommonClient cClient = new CommonClient();
                        cClient.CreateTransactionLog(Originator,
                            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            TransactionTypeModules.Insert,
                            TransactionModules.AssetMaster,
                            Originator + " save new asset " + assetCode);
                    }
                    catch { }
                }
            }
            else
            {
                model.AssetId = Int32.Parse(id);
                model.LastModifiedBy = Originator;
                delstatus = assetService.UpdateAsset(model);

                if (delstatus)
                {
                    try
                    {
                        CommonClient cClient = new CommonClient();
                        cClient.CreateTransactionLog(Originator,
                            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            TransactionTypeModules.Update,
                            TransactionModules.AssetMaster,
                            Originator + " update asset " + assetCode);
                    }
                    catch { }
                }
            }

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    private static string GetAllAssetSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " AssetTypeName asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "AssetId":
                sortStr = " asset_id " + sortoption;
                break;
            case "AssetTypeName":
                sortStr = " asset_type_name " + sortoption;
                break;
            default:
                sortStr = " asset_id asc ";
                break;
        }

        return sortStr;
    }

    private string GetFilterAssetField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "AssetCode":
                realfield = "asset_code";
                break;
            case "Model":
                realfield = "asset_model";
                break;
            case "IMEI_1":
                realfield = "imei_1";
                break;
            case "IMEI_2":
                realfield = "imei_2";
                break;
            case "SerialNo":
                realfield = "serial_no";
                break;
            case "CompAssetCode":
                realfield = "company_asset_code";
                break;
        }

        return realfield;
    }

    [WebMethod(EnableSession = true)]
    public Peercore.CRM.Model.ResponseModel IsAssetCodeAvailable(string assetCode)
    {
        AssetsClient assetService = new AssetsClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        response.Status = assetService.IsAssetCodeAvailable(assetCode);
        response.StatusCode = "Exsist";

        return response;
    }


    #endregion

    #region - Assign Assets -

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllAssignAsset(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string Token)
    {
        AssetsClient assetClient = new AssetsClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllAssignAssetSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " asset_type_name asc";
            }
        }
        else
        {
            args.OrderBy = " asset_type_name asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetAllAssignAssetTypes(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<AssetAssignModel> data = new List<AssetAssignModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = assetClient.GetAllAssignAssets(args, Token);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult AssetsByUserCode(string userCode)
    {
        AssetsClient assetClient = new AssetsClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<AssetAssignModel> data = new List<AssetAssignModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = assetClient.GetAllAssignAssetsByUserCode(userCode);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public Peercore.CRM.Model.ResponseModel SaveAssignAssets(string Originator, List<AssetAssignModel> data, string Token)
    {
        ArgsModel args = new ArgsModel();
        AssetsClient assetService = new AssetsClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();
        List<AssetAssignModel> dataModel = new List<AssetAssignModel>();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (assetService.GetAllAssignAssets(args, Token).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            bool delstatus = false;

            delstatus = assetService.SaveAssignAssetList(Originator, data);

            if (delstatus)
            {
                try
                {
                    CommonClient cClient = new CommonClient();
                    cClient.CreateTransactionLog(Originator,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Assign,
                        TransactionModules.AssetAssignEmployee,
                        Originator + " assign asset");
                }
                catch { }
            }

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public List<OriginatorModel> LoadUserCodes(string userType)
    {
        AssetsClient assetClient = new AssetsClient();
        List<OriginatorModel> originatorList = new List<OriginatorModel>();

        try
        {
            originatorList = assetClient.GetUserCodesByUserType(userType);
        }
        catch (Exception)
        {
            throw;
        }

        return originatorList;
    }

    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public List<AssetModel> LoadAssets(string assetType)
    {
        AssetsClient assetClient = new AssetsClient();
        List<AssetModel> assetList = new List<AssetModel>();

        assetList = assetClient.GetAssetsCodesByAssetType(assetType);

        return assetList;
    }

    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public string LoadCustomerName(string custCode)
    {
        AssetsClient assetClient = new AssetsClient();

        string name = assetClient.LoadCustomerName(custCode);

        return name;
    }

    [WebMethod(EnableSession = true)]
    public Peercore.CRM.Model.ResponseModel CheckAssetIsAvailable(string assetCode, string cusCode)
    {

        AssetsClient assetService = new AssetsClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        response.Status = assetService.IsAssetAvailable(assetCode, cusCode);
        response.StatusCode = "Exsist";

        return response;
    }

    [WebMethod(EnableSession = true)]
    public Peercore.CRM.Model.ResponseModel CheckIsLostItem(string assetCode, string cusCode)
    {

        AssetsClient assetService = new AssetsClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        response.Status = assetService.IsLostOrDamaged(assetCode, cusCode);
        response.StatusCode = "Lost/Damaged";

        return response;
    }

    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public List<AssetStatusModel> LoadOtherStatus(string status)
    {
        AssetsClient assetClient = new AssetsClient();
        List<AssetStatusModel> originatorList = new List<AssetStatusModel>();

        try
        {
            originatorList = assetClient.GetAssetStatus();
        }
        catch (Exception)
        {
            throw;
        }

        return originatorList;
    }


    private static string GetAllAssignAssetSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " user_code asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "UserCode":
                sortStr = " user_code " + sortoption;
                break;
            case "UserType":
                sortStr = " user_type " + sortoption;
                break;
            case "AssetCode":
                sortStr = " asset_code " + sortoption;
                break;
            default:
                sortStr = " assign_id asc ";
                break;
        }

        return sortStr;
    }

    private string GetAllAssignAssetTypes(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "UserCode":
                realfield = "user_code";
                break;
            case "UserType":
                realfield = "user_type";
                break;
            case "AssetCode":
                realfield = "asset_code";
                break;
            case "UserName":
                realfield = "name";
                break;
            case "AssetTypeName":
                realfield = "asset_type_name";
                break;
            case "AssignDate":
                realfield = "assign_date";
                break;
        }

        return realfield;
    }


    #endregion


}
