﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Peercore.CRM.Entities;
//using Telerik.Web.UI;
using CRMServiceReference;
using System.ComponentModel;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.UI;
using System.IO;

/// <summary>
/// Summary description for pipelines_stage_service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
[ScriptService]
public class load_stock : System.Web.Services.WebService {

    public load_stock () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    } 
    
    //[WebMethod(EnableSession = true)]
    //public DataSourceResult GetProduct(string allocDate, string distributorId, string divisionId, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    //{
    //    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    HttpContext.Current.Response.CacheControl = "no-cache";
    //    LoadStockDTO lstLoadStockDetail = new LoadStockDTO();
    //    DataSourceResult dataSourceResult = new DataSourceResult();
    //    List<Sort> sortExpression = (List<Sort>)sort;
    //    CommonClient commonClient = new CommonClient();

    //    ArgsDTO args = new ArgsDTO();
    //    args.StartIndex = 0;
    //    args.RowCount = take;

    //    if (!string.IsNullOrEmpty(pgindex))
    //    {
    //        if (((int.Parse(pgindex) - 2) * take) != skip)
    //        {
    //            args.StartIndex = (skip / take) + 1;
    //        }
    //        else
    //        {
    //            args.StartIndex = int.Parse(pgindex) - 1;
    //        }
    //    }
    //    else
    //    {
    //        args.StartIndex = (skip / take) + 1;
    //    }

    //    if (distributorId == "")
    //    {
    //        if (divisionId == "")
    //        {
    //            lstLoadStockDetail = commonClient.GetAllProductsForStockLoad(allocDate, 0, 0, args);
    //        }
    //        else
    //        {
    //            lstLoadStockDetail = commonClient.GetAllProductsForStockLoad(allocDate, 0, Convert.ToInt32(divisionId), args);
    //        }
    //    }
    //    else
    //    {
    //        if (divisionId == "")
    //        {
    //            lstLoadStockDetail = commonClient.GetAllProductsForStockLoad(allocDate, Convert.ToInt32(distributorId), 0, args);
    //        }
    //        else
    //        {
    //            lstLoadStockDetail = commonClient.GetAllProductsForStockLoad(allocDate, Convert.ToInt32(distributorId), Convert.ToInt32(divisionId), args);
    //        }
    //    }

    //    dataSourceResult.Data = lstLoadStockDetail.LstLoadStockDetail;

    //    try
    //    {
    //        dataSourceResult.Total = lstLoadStockDetail.LstLoadStockDetail[0].rowCount;
    //    }
    //    catch { dataSourceResult.Total = 0; }

    //    return dataSourceResult;
    //}

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetProductForStockAdjustment(string allocDate, string distributorId, string divisionId, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        LoadStockDTO lstLoadStockDetail = new LoadStockDTO();
        DataSourceResult dataSourceResult = new DataSourceResult();
        List<Sort> sortExpression = (List<Sort>)sort;
        CommonClient commonClient = new CommonClient();

        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        if (distributorId == "")
        {
            if (divisionId == "")
            {
                lstLoadStockDetail = commonClient.GetProductForStockAdjustment(allocDate, 0, 0, args);
            }
            else
            {
                lstLoadStockDetail = commonClient.GetProductForStockAdjustment(allocDate, 0, Convert.ToInt32(divisionId), args);
            }
        }
        else
        {
            if (divisionId == "")
            {
                lstLoadStockDetail = commonClient.GetProductForStockAdjustment(allocDate, Convert.ToInt32(distributorId), 0, args);
            }
            else
            {
                lstLoadStockDetail = commonClient.GetProductForStockAdjustment(allocDate, Convert.ToInt32(distributorId), Convert.ToInt32(divisionId), args);
            }
        }

        dataSourceResult.Data = lstLoadStockDetail.LstLoadStockDetail;

        try
        {
            dataSourceResult.Total = lstLoadStockDetail.LstLoadStockDetail[0].rowCount;
        }
        catch { dataSourceResult.Total = 0; }

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public List<OriginatorDTO> GetAllOriginator(string name)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();
        List<OriginatorDTO> returnList = new List<OriginatorDTO>();
        DataSourceResult dataSourceResult = new DataSourceResult();
        try
        {
            lstOriginatorDTO = commonClient.GetAllOriginators(name);
            foreach (OriginatorDTO item in lstOriginatorDTO)
            {
                //result.Add(item.CustomerCode+ " "+item.Name);
                returnList.Add(item);
            }
            return returnList;
        }
        catch (Exception)
        {

            return null;
        }
    }


    [WebMethod(EnableSession = true)]
    public List<RouteDTO> GetAllRoutes(string name) //not being currently used - milinda
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        List<RouteDTO> lstRouteDTO = new List<RouteDTO>();
        List<RouteDTO> returnList = new List<RouteDTO>();
        DataSourceResult dataSourceResult = new DataSourceResult();
        try
        {
            lstRouteDTO = commonClient.GetAllRoutes(name);
            foreach (RouteDTO item in lstRouteDTO)
            {
                //result.Add(item.CustomerCode+ " "+item.Name);
                returnList.Add(item);
            }
            return returnList;
        }
        catch (Exception)
        {

            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public List<OriginatorDTO> GetRepsByRouteMasterId(string name) 
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();

        List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();
        List<OriginatorDTO> returnList = new List<OriginatorDTO>();

        DataSourceResult dataSourceResult = new DataSourceResult();

        int RouteMasterID = 0;

        try
        {
            lstOriginatorDTO = commonClient.GetRepsByRouteMasterId(name, Convert.ToInt32(RouteMasterID));
            foreach (OriginatorDTO item in lstOriginatorDTO)
            {
                returnList.Add(item);
            }
            return returnList;
        }
        catch (Exception)
        {
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public List<RouteDTO> GetRouteRepLikeRouteName(string name, string isTemp)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        string allocationDate = (Session["STOCK_ALLOCATION_DATE"]).ToString();

        CommonClient commonClient = new CommonClient();
        List<RouteDTO> lstRouteDTO = new List<RouteDTO>();
        List<RouteDTO> returnList = new List<RouteDTO>();
        try
        {
            int iIsTemp = Convert.ToInt32(isTemp);

            lstRouteDTO = commonClient.GetRouteRepLikeRouteName(name, iIsTemp, allocationDate);
            foreach (RouteDTO item in lstRouteDTO)
            {
                returnList.Add(item);
            }
            return returnList;
        }
        catch (Exception)
        {

            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllFlavorsDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<FlavorDTO> data = new List<FlavorDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            //ArgsDTO args = new ArgsDTO();
            //args.StartIndex = 0;
            //args.RowCount = take;
            //args.ROriginator = UserSession.Instance.OriginatorString;
            //args.Originator = UserSession.Instance.OriginalUserName;
            //args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            ////args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            //if (!string.IsNullOrEmpty(pgindex))
            //{
            //    if (((int.Parse(pgindex) - 2) * take) != skip)
            //    {
            //        args.StartIndex = (skip / take) + 1;
            //    }
            //    else
            //    {
            //        args.StartIndex = int.Parse(pgindex) - 1;
            //    }
            //}
            //else
            //{
            //    args.StartIndex = (skip / take) + 1;
            //}

            //List<Sort> sortExpression = (List<Sort>)sort;

            //if (sortExpression != null)
            //{
            //    if (sortExpression.Count != 0)
            //    {
            //        args.OrderBy = GetAllBrandsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            //    }
            //    else
            //    {
            //        args.OrderBy = " name desc";
            //    }
            //}
            //else
            //{
            //    args.OrderBy = " name desc";
            //}

            //if (filter != null)
            //{
            //    new CommonUtility().SetFilterField(ref filter, "allbrandsdata");
            //    args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            //}

            data = commonClient.GetAllFlavor();
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data.Count;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllPackingMethodDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<ProductPackingDTO> data = new List<ProductPackingDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllProductPackingsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " product_packing_id asc";
                }
            }
            else
            {
                args.OrderBy = " product_packing_id asc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allbrandsdata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllProductPackings(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllBrandsDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<BrandDTO> data = new List<BrandDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllBrandsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allbrandsdata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllBrandsDataAndCount(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllProductCategoryDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<ProductCategoryDTO> data = new List<ProductCategoryDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllProductCategorySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " description desc";
                }
            }
            else
            {
                args.OrderBy = " description desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allproductcategorydata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllProductCategoryDataAndCount(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllBrandsSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "BrandCode":
                sortStr = " code " + sortoption;
                break;
            case "BrandName":
                sortStr = " name " + sortoption;
                break;
            default:
                sortStr = " name desc ";
                break;
        }

        return sortStr;
    }

    private static string GetAllProductPackingsSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " packing_name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "PackingName":
                sortStr = " packing_name " + sortoption;
                break;
            default:
                sortStr = " packing_name desc ";
                break;
        }

        return sortStr;
    }

    private static string GetAllProductCategorySortString(string sortby, string gridSortOrder)
    {
        string sortStr = " description desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "CategoryCode":
                sortStr = " category_code " + sortoption;
                break;
            case "CategoryName":
                sortStr = " description " + sortoption;
                break;
            default:
                sortStr = " description desc ";
                break;
        }

        return sortStr;
    }

    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string DeleteSelectedProductListformGrid(List<int> DeleteProductListDTO)
    {
        DataSourceResult dataSourceResult = new DataSourceResult();
        bool status = false;
        bool isDiscount = false;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        string jsonString = string.Empty;
        ProductDTO productDTO = new ProductDTO();
        CommonClient commonClient = new CommonClient();
        CommonUtility commonUtility = new CommonUtility();

        foreach (int item in DeleteProductListDTO)
        {
            if (commonClient.IsProductExsistInDiscountScheme(item) == 0)
            {
                productDTO.ProductId = item;
                productDTO.Status = "D";
                productDTO.CreatedBy = UserSession.Instance.UserName;
                productDTO.LastModifiedBy = UserSession.Instance.UserName;

                status = true;
                status = commonClient.InactiveProduct(productDTO);
            }
            else
            {
                isDiscount = true;
            }
        }

        string returnvalue = string.Empty;

        if (status)
        {
            if (isDiscount == false)
                returnvalue = "Deleted";
            else
                returnvalue = "DeletedDiscount";
        }
        else
        {
            returnvalue = "DeletedDiscount";
        }

        return returnvalue;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllProductsDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<ProductDTO> data = new List<ProductDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.AdditionalParams = "";
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllProductsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allproductsdata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllProductsDataAndCountWithOutImage(args);
            //if (data.Count > 0)
            //{
            //    foreach (ProductDTO item in data)
            //    {
            //        if (item.ImageContent != null)
            //        {
            //            string new_tempFileName = item.Code;
            //            string new_path = ConfigUtil.ApplicationPath + "docs/product_images/" + new_tempFileName + ".jpg";

            //            //Create Document
            //            if (!isFileOpen(Server.MapPath(new_path)))
            //            {
            //                FileStream fs = new FileStream(Server.MapPath(new_path), FileMode.Create, FileAccess.ReadWrite);
            //                BinaryWriter bw = new BinaryWriter(fs);
            //                bw.Write(item.ImageContent);
            //                bw.Close();
            //                fs.Close();
            //            }

            //            item.ImageName = new_tempFileName + ".jpg";
            //            item.ImagePath = new_path;
            //        }
            //    }
            //}

            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllProductsDataAndCountForMaster(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<ProductDTO> data = new List<ProductDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.AdditionalParams = "";
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllProductsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allproductsdata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllProductsDataAndCountWithOutImageForMaster(args);
            //if (data.Count > 0)
            //{
            //    foreach (ProductDTO item in data)
            //    {
            //        if (item.ImageContent != null)
            //        {
            //            string new_tempFileName = item.Code;
            //            string new_path = ConfigUtil.ApplicationPath + "docs/product_images/" + new_tempFileName + ".jpg";

            //            //Create Document
            //            if (!isFileOpen(Server.MapPath(new_path)))
            //            {
            //                FileStream fs = new FileStream(Server.MapPath(new_path), FileMode.Create, FileAccess.ReadWrite);
            //                BinaryWriter bw = new BinaryWriter(fs);
            //                bw.Write(item.ImageContent);
            //                bw.Close();
            //                fs.Close();
            //            }

            //            item.ImageName = new_tempFileName + ".jpg";
            //            item.ImagePath = new_path;
            //        }
            //    }
            //}

            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllProductsSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Code":
                sortStr = " code " + sortoption;
                break;
            case "Product Code":
                sortStr = " code " + sortoption;
                break;
            case "Name":
                sortStr = " name " + sortoption;
                break;
            case "Product Name":
                sortStr = " name " + sortoption;
                break;
            case "FgCode":
                sortStr = " fg_code " + sortoption;
                break;
            case "CategoryName":
                sortStr = " category " + sortoption;
                break;
            case "BrandName":
                sortStr = " brand_name " + sortoption;
                break;
            default:
                sortStr = " name desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllProductPricesDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();

        List<ProductDTO> data = new List<ProductDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.AdditionalParams = "";

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllProductPricesSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allproductpricesdata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllProductPricesDataAndCount(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllProductPricesSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Code":
                sortStr = " code " + sortoption;
                break;
            case "Name":
                sortStr = " name " + sortoption;
                break;
            case "RetailPrice":
                sortStr = " outlet_retail_price " + sortoption;
                break;
            case "OutletPrice":
                sortStr = " outlet_consumer_price " + sortoption;
                break;
            case "DistributorPrice":
                sortStr = " distributor_price " + sortoption;
                break;
            default:
                sortStr = " name desc ";
                break;
        }

        return sortStr;
    }

    #region "2nd Phase development"

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllProductStockByTerritoryId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string allocDate, string territoryId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ProductStockModel lstLoadStockDetail = new ProductStockModel();
        DataSourceResult dataSourceResult = new DataSourceResult();
        List<Sort> sortExpression = (List<Sort>)sort;
        StockClient stockClient = new StockClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        if (territoryId == "")
        {
            lstLoadStockDetail = stockClient.GetAllProductStockByTerritoryId(args, Convert.ToDateTime(allocDate), 0);
        }
        else
        {
            lstLoadStockDetail = stockClient.GetAllProductStockByTerritoryId(args, Convert.ToDateTime(allocDate), Convert.ToInt32(territoryId));
        }

        dataSourceResult.Data = lstLoadStockDetail.LstLoadStockDetail;

        try
        {
            dataSourceResult.Total = lstLoadStockDetail.LstLoadStockDetail[0].TotalCount;
        }
        catch { dataSourceResult.Total = 0; }

        return dataSourceResult;
    }



    #endregion

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllClaimInvoiceDetailsByInvoiceId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string invoiceId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<DBClaimInvoiceDetailsModel> lstLoadStockDetail = new List<DBClaimInvoiceDetailsModel>();
        DataSourceResult dataSourceResult = new DataSourceResult();
        List<Sort> sortExpression = (List<Sort>)sort;
        StockClient stockClient = new StockClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        if (invoiceId == "")
        {
            lstLoadStockDetail = stockClient.GetAllDBClaimInvoiceDetailsByInvoiceId(args, "0");
        }
        else
        {
            lstLoadStockDetail = stockClient.GetAllDBClaimInvoiceDetailsByInvoiceId(args, invoiceId);
        }

        dataSourceResult.Data = lstLoadStockDetail;

        try
        {
            dataSourceResult.Total = 0;//lstLoadStockDetail[0].TotalCount;
        }
        catch { dataSourceResult.Total = 0; }

        return dataSourceResult;
    }
}
