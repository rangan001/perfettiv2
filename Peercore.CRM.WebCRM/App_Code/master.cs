﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.ServiceModel.Web;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;
using System.IO;
using System.Collections;
using System.Net;
using System.Text;
using Newtonsoft.Json;
/// <summary>
/// Summary description for master
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

[ScriptService]

public class master : System.Web.Services.WebService
{
    CommonClient commonClient = new CommonClient();

    public master()
    {
    }

    #region "Region"

    [WebMethod]
    public List<RegionModel> GetAllRegion()
    {
        MasterClient masterlient = new MasterClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 10000;
        args.OrderBy = " region_id asc";

        return masterlient.GetAllRegions(args);
    }

    [WebMethod]
    public DataSourceResult GetAllRegions(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        MasterClient masterlient = new MasterClient();
        List<RegionModel> regionList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllRegionsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " region_id asc";
            }
        }
        else
        {
            args.OrderBy = " region_id asc";
        }

        if (filter != null)
        {
            new FilterUtility().SetFilterField(ref filter, "regionmaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        regionList = masterlient.GetAllRegions(args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (regionList != null && regionList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = regionList;
            dataSourceResult.Total = regionList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = regionList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetAllRegionsSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " region_id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RegionID":
                sortStr = " region_id " + sortoption;
                break;
            case "RegionCode":
                sortStr = " region_code " + sortoption;
                break;
            case "RegionName":
                sortStr = " region_name " + sortoption;
                break;
            default:
                sortStr = " region_id asc ";
                break;
        }

        return sortStr;
    }

    [WebMethod]
    public DataSourceResult DeleteRegion(string region_id)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteRegion(region_id);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }
    
    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveRegion(string comp_id,
                                                        string region_id, 
                                                        string region_code, 
                                                        string region_name)
    {
        ArgsModel args = new ArgsModel();
        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (masterlient.GetAllRegionsByRegionCode(args, region_code).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else if (masterlient.GetAllRegionsByRegionName(args, region_name).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            bool delstatus = masterlient.SaveRegion(comp_id, region_id, region_code, region_name);

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    #endregion

    #region "RSM"

    [WebMethod]
    public DataSourceResult GetAllRSM(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        MasterClient masterlient = new MasterClient();
        List<RsmModel> rsmList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllRSMSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " rsm_id asc";
            }
        }
        else
        {
            args.OrderBy = " rsm_id asc";
        }

        if (filter != null)
        {
            new FilterUtility().SetFilterField(ref filter, "rsmmaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        rsmList = masterlient.GetAllRSM(args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (rsmList != null && rsmList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = rsmList;
            dataSourceResult.Total = rsmList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = rsmList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetRsmDetailsByRegionId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string RegionId)
    {
        MasterClient masterlient = new MasterClient();
        List<RsmModel> rsmList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllRSMSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " rsm_id asc";
            }
        }
        else
        {
            args.OrderBy = " rsm_id asc";
        }

        rsmList = masterlient.GetRsmDetailsByRegionId(args, RegionId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (rsmList != null && rsmList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = rsmList;
            dataSourceResult.Total = rsmList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = rsmList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel DeleteRsmByRsmId(string rsm_id)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteRsmByRsmId(rsm_id);

        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();
        if (delstatus)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveRSM(string region_id,
                                                        string rsm_id,
                                                        string rsm_code,
                                                        string rsm_name,
                                                        string rsm_username,
                                                        string rsm_tel1,
                                                        string rsm_email,
                                                        string created_by)
    {
        ArgsModel args = new ArgsModel();
        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (masterlient.GetAllRsmByRsmCode(args, rsm_code).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            bool delstatus = masterlient.SaveRsm(region_id,
                                                        rsm_id,
                                                        rsm_code,
                                                        rsm_name,
                                                        rsm_username,
                                                        rsm_tel1,
                                                        rsm_email,
                                                        created_by);
            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel UpdateRsmPassword(string rsm_id, string password)
    {
        ArgsModel args = new ArgsModel();
        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";


        bool retstatus = masterlient.UpdateRsmPassword(rsm_id,
                                            password);
        if (retstatus)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }


        return response;
    }

    #endregion

    #region "Area"

    [WebMethod]
    public DataSourceResult GetAllArea(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        MasterClient masterlient = new MasterClient();
        List<AreaModel> areaList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllAreaSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " area_id asc";
            }
        }
        else
        {
            args.OrderBy = " area_id asc";
        }

        if (filter != null)
        {
            new FilterUtility().SetFilterField(ref filter, "areamaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        areaList = masterlient.GetAllAreaNew(args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (areaList != null && areaList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = areaList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllAreaByRegionId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string RegionId)
    {
        MasterClient masterlient = new MasterClient();
        List<AreaModel> areaList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllAreaSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " area_id asc";
            }
        }
        else
        {
            args.OrderBy = " area_id asc";
        }

        if (filter != null)
        {
            new FilterUtility().SetFilterField(ref filter, "areamaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        areaList = masterlient.GetAllAreaByRegionId(args, RegionId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (areaList != null && areaList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = areaList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult DeleteAreaFromRegionByAreaId(int area_id)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteAreaFromRegionByAreaId(area_id);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    //[WebMethod]
    //public Peercore.CRM.Model.ResponseModel DeleteRSM(string region_id)
    //{
    //    MasterClient masterlient = new MasterClient();

    //    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    HttpContext.Current.Response.CacheControl = "no-cache";

    //    var delstatus = masterlient.DeleteRegion(region_id);

    //    Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();
    //    if (delstatus)
    //    {
    //        response.Status = true;
    //        response.StatusCode = "Success";
    //    }
    //    else
    //    {
    //        response.Status = false;
    //        response.StatusCode = "Error";
    //    }

    //    return response;
    //}

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveUpdateArea(string region_id,
                                                        string area_id,
                                                        string area_code_old,
                                                        string area_name_old,
                                                        string area_code,
                                                        string area_name)
    {
        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 100000;
        args.OrderBy = " area_id asc";

        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (area_code.ToLower() != area_code_old.ToLower() && masterlient.GetAllAreaByAreaCode(args, area_code).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        //else if (area_name.ToLower() != area_name_old.ToLower() && masterlient.GetAllAreaByAreaName(args, area_code).Count > 0)
        //{
        //    response.Status = false;
        //    response.StatusCode = "Exsist";
        //}
        else
        {
            bool delstatus = masterlient.InsertUpdateArea(region_id, area_id, area_code, area_name);

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    #endregion

    #region "Territory"

    [WebMethod]
    public List<TerritoryModel> GetAllTerritories()
    {
        MasterClient masterlient = new MasterClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 1000000;
        args.OrderBy = " territory_id asc";

        return masterlient.GetAllTerritory(args);
    }

    [WebMethod]
    public List<TerritoryModel> GetAllTerritoriesByOriginator(string OriginatorType, string Originator)
    {
        MasterClient masterlient = new MasterClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 1000000;
        args.OrderBy = " territory_id asc";

        if (OriginatorType == "ADMIN")
        {
            return masterlient.GetAllTerritory(args);
        }
        else
        {
            return masterlient.GetAllTerritoryByOriginator(args, OriginatorType, Originator);
        }
    }

    public DataSourceResult GetAllTerritoriesByOriginator(int skip, int take, IEnumerable<Sort> sort, Filter filter, 
                                                                string pgindex, string OriginatorType, string Originator)
    {
        MasterClient masterlient = new MasterClient();
        List<TerritoryModel> areaList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllTerritorySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " territory_name asc";
            }
        }
        else
        {
            args.OrderBy = " territory_name asc";
        }

        if (filter != null)
        {
            new FilterUtility().SetFilterField(ref filter, "territorymaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        if (OriginatorType == "ADMIN")
        {
            areaList = masterlient.GetAllTerritory(args);
        }
        else
        {
            areaList = masterlient.GetAllTerritoryByOriginator(args, OriginatorType, Originator);
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (areaList != null && areaList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = areaList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllTerritory(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        MasterClient masterlient = new MasterClient();
        List<TerritoryModel> areaList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllTerritorySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " territory_name asc";
            }
        }
        else
        {
            args.OrderBy = " territory_name asc";
        }

        if (filter != null)
        {
            new FilterUtility().SetFilterField(ref filter, "territorymaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        areaList = masterlient.GetAllTerritory(args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (areaList != null && areaList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = areaList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllTerritoryByAreaId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string AreaId)
    {
        MasterClient masterlient = new MasterClient();
        List<TerritoryModel> areaList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllTerritorySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " territory_id asc";
            }
        }
        else
        {
            args.OrderBy = " territory_id asc";
        }

        if (filter != null)
        {
            new FilterUtility().SetFilterField(ref filter, "territorymaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        areaList = masterlient.GetAllTerritoryByAreaId(args, AreaId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (areaList != null && areaList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = areaList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllTerritoryByDistributerId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string DistributerId)
    {
        MasterClient masterlient = new MasterClient();
        List<TerritoryModel> areaList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllTerritorySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " territory_id asc";
            }
        }
        else
        {
            args.OrderBy = " territory_id asc";
        }

        areaList = masterlient.GetAllTerritoryByDistributerId(args, DistributerId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (areaList != null && areaList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = areaList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetAllTerritorySortString(string sortby, string gridSortOrder)
    {
        string sortStr = " territory_id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "TerritoryId":
                sortStr = " territory_id " + sortoption;
                break;
            case "TerritoryCode":
                sortStr = " territory_code " + sortoption;
                break;
            case "TerritoryName":
                sortStr = " territory_name " + sortoption;
                break;
            default:
                sortStr = " territory_id asc ";
                break;
        }

        return sortStr;
    }

    //[WebMethod]
    //public DataSourceResult DeleteAreaFromRegionByAreaId(int area_id)
    //{
    //    MasterClient masterlient = new MasterClient();

    //    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    HttpContext.Current.Response.CacheControl = "no-cache";

    //    var delstatus = masterlient.DeleteAreaFromRegionByAreaId(area_id);

    //    DataSourceResult dataSourceResult = new DataSourceResult();
    //    if (delstatus)
    //    {
    //        dataSourceResult = new DataSourceResult();
    //        dataSourceResult.Status = true;
    //    }
    //    else
    //    {
    //        dataSourceResult = new DataSourceResult();
    //        dataSourceResult.Status = false;
    //    }

    //    return dataSourceResult;
    //}

    ////[WebMethod]
    ////public Peercore.CRM.Model.ResponseModel DeleteRSM(string region_id)
    ////{
    ////    MasterClient masterlient = new MasterClient();

    ////    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    ////    HttpContext.Current.Response.CacheControl = "no-cache";

    ////    var delstatus = masterlient.DeleteRegion(region_id);

    ////    Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();
    ////    if (delstatus)
    ////    {
    ////        response.Status = true;
    ////        response.StatusCode = "Success";
    ////    }
    ////    else
    ////    {
    ////        response.Status = false;
    ////        response.StatusCode = "Error";
    ////    }

    ////    return response;
    ////}

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveUpdateTerritory(string area_id,
                                                        string territory_id,
                                                        string territory_code_old,
                                                        string territory_code,
                                                        string territory_name_old,
                                                        string territory_name,
                                                        string created_by)
    {
        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 100000;
        args.OrderBy = " territory_id asc";

        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (territory_code.ToLower() != territory_code_old.ToLower() && masterlient.GetAllTerritoryByTerritoryCode(args, territory_code).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else if (territory_name.ToLower() != territory_name_old.ToLower() && masterlient.GetAllTerritoryByTerritoryName(args, territory_name).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            bool delstatus = masterlient.SaveTerritory(area_id, territory_id, territory_code, territory_name, created_by);

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    [WebMethod]
    public DataSourceResult DeleteTerritoryFromAreaByTerritoryId(string territory_id)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteTerritoryFromAreaByTerritoryId(territory_id);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel DeleteTerritory(string territory_id)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteTerritory(territory_id);

        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();
        if (delstatus)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }

    #endregion

    #region "ASM"

    [WebMethod]
    public DataSourceResult GetAllAsm(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        MasterClient masterlient = new MasterClient();
        List<AsmModel> asmList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllASMSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " asm_id asc";
            }
        }
        else
        {
            args.OrderBy = " asm_id asc";
        }

        if (filter != null)
        {
            new FilterUtility().SetFilterField(ref filter, "asmmaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        asmList = masterlient.GetAllAsm(args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (asmList != null && asmList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = asmList;
            dataSourceResult.Total = asmList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = asmList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllAsmByAreaId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string AreaId)
    {
        MasterClient masterlient = new MasterClient();
        List<AsmModel> asmList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllASMSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " asm_id asc";
            }
        }
        else
        {
            args.OrderBy = " asm_id asc";
        }

        asmList = masterlient.GetAllAsmByAreaId(args, AreaId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (asmList != null && asmList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = asmList;
            dataSourceResult.Total = asmList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = asmList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllOriginatorImeiByAsmId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string AsmId)
    {
        MasterClient masterlient = new MasterClient();
        List<OriginatorImeiModel> asmImeiList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = " mobile_no asc"; //GetAllASMSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " mobile_no asc";
            }
        }
        else
        {
            args.OrderBy = " mobile_no asc";
        }

        asmImeiList = masterlient.GetAllOriginatorImeiByAsmId(args, AsmId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (asmImeiList != null && asmImeiList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = asmImeiList;
            dataSourceResult.Total = asmImeiList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = asmImeiList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetAllASMSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " asm_id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "AsmId":
                sortStr = " asm_id " + sortoption;
                break;
            case "AsmCode":
                sortStr = " asm_code " + sortoption;
                break;
            case "AsmName":
                sortStr = " asm_name " + sortoption;
                break;
            default:
                sortStr = " asm_id asc ";
                break;
        }

        return sortStr;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveAsm(string area_id,
                                                        string asm_id,
                                                        string asm_code,
                                                        string asm_name,
                                                        string asm_username,
                                                        string asm_tel1,
                                                        string asm_email,
                                                        string created_by)
    {
        ArgsModel args = new ArgsModel();
        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (masterlient.GetAllRsmByRsmCode(args, asm_code).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            bool retstatus = masterlient.SaveAsm(area_id,
                                                asm_id,
                                                asm_code,
                                                asm_name,
                                                asm_username,
                                                asm_tel1,
                                                asm_email,
                                                created_by);
            if (retstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel UpdateAsmIsIMEI(string asm_id, bool is_imei)
    {
        ArgsModel args = new ArgsModel();
        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";


        bool retstatus = masterlient.UpdateOriginatorIsIMEIByAsmId(asm_id,
                                            is_imei);
        if (retstatus)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }


        return response;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel UpdateAsmPassword(string asm_id, string password)
    {
        ArgsModel args = new ArgsModel();
        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";


        bool retstatus = masterlient.UpdateAsmPassword(asm_id,
                                            password);
        if (retstatus)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }


        return response;
    }

    [WebMethod]
    public DataSourceResult DeleteAsmByAsmId(string asm_id)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteAsmByAsmId(asm_id);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    } 
    
    [WebMethod]
    public DataSourceResult DeleteIMEIByAsmId(string asm_id, string emei_no)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteIMEIByAsmId(asm_id, emei_no);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    #endregion

    #region "Sales Rep"

    [WebMethod]
    public DataSourceResult GetAllSRByTerritoryId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string TerritoryId)
    {
        MasterClient masterlient = new MasterClient();
        List<SRModel> srList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllSRSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " rep_id asc";
            }
        }
        else
        {
            args.OrderBy = " rep_id asc";
        }

        srList = masterlient.GetAllSalesRepsByTerritoryId(args, TerritoryId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (srList != null && srList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = srList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllSRByAreaId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string AreaId)
    {
        MasterClient masterlient = new MasterClient();
        List<SRModel> srList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllSRSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " rep_id asc";
            }
        }
        else
        {
            args.OrderBy = " rep_id asc";
        }

        srList = masterlient.GetAllSalesRepsByAreaId(args, AreaId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (srList != null && srList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = srList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetSRDetailsByOriginator(string originator)
    {
        Session[CommonUtility.DISTRIBUTOR_DATA] = "";
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        MasterClient masterClient = new MasterClient();
        SRModel srModel = new SRModel();

        string returnvalue = string.Empty;

        try
        {
            srModel = masterClient.GetSRDetailsByOriginator(originator);
            Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>(srModel.Originator, "DR");
        }
        catch (Exception)
        {
            returnvalue = "error";
        }

        return serializer.Serialize(srModel);
    }

    [WebMethod]
    public DataSourceResult DeleteConfirmSRFromTerritory(string territoryId, string repId)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteConfirmSRFromTerritory(territoryId, repId);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllSalesRepSyncData(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string OriginatorType, string Originator, bool IsClear)
    {
        MasterClient masterlient = new MasterClient();
        List<SalesRepSyncModel> srList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllSRSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " rep_name asc";
            }
        }
        else
        {
            args.OrderBy = " rep_name asc";
        }

        srList = masterlient.GetAllSalesRepSyncData(args, OriginatorType, Originator);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (srList != null && srList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = srList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    #endregion

    #region "Distributer"

    [WebMethod]
    public List<DistributerModel> GetAllDistributerByOriginator(string OriginatorType, string Originator)
    {
        MasterClient masterlient = new MasterClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 1000000;
        args.OrderBy = " territory_id asc";

        //if (OriginatorType == "ADMIN")
        //{
        //    return masterlient.GetAllDis(args);
        //}
        //else
        //{
            return masterlient.GetAllDistributerByOriginatorAndOriginatorType(args, OriginatorType, Originator);
        //}
    }

    #endregion

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllSalesRepWithSalesType(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string OriginatorType, string Originator)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        MasterClient masterClient = new MasterClient();

        List<SalesRepSalesTypeModel> srList = new List<SalesRepSalesTypeModel>();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllSalesRepWithSalesTypeSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " territory_code asc";
            }
        }
        else
        {
            args.OrderBy = " territory_code asc";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "GetAllSalesRepWithSalesType");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        srList = masterClient.GetAllSalesRepWithSalesType(args, OriginatorType, Originator);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (srList != null && srList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = srList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult UpdateSRSalesType(string RepId, string SalesType, string Originator)
    {
        MasterClient masterclient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterclient.UpdateSRSalesType(RepId, SalesType, Originator);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult UpdateTerritoryCanAddOutlet(string territory_id, bool can_add_outlet)
    {
        MasterClient masterclient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterclient.UpdateTerritoryCanAddOutlet(territory_id, can_add_outlet);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult UpdateTerritoryGeoFence(string territory_id, bool is_geofence)
    {
        MasterClient masterclient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterclient.UpdateTerritoryGeoFence(territory_id, is_geofence);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    //Update Rwpsa Sysnc Data
    [WebMethod]
    public bool UpdateRepsSyncData(string apiUri, string OriginatorType, string Originator, string DeviceIMEI)
    {
        var delstatus = false;
        //string apiUrl = apiUri;// "https://localhost:44362/api/sync";
        //object request = new
        //{
        //    originatorType = OriginatorType,
        //    originator = Originator,
        //    DeviceIMEI = DeviceIMEI
        //};
        //string inputJson = (new JavaScriptSerializer()).Serialize(request);
        //WebClient client = new WebClient();
        //client.Headers["Content-type"] = "application/json";
        //client.Encoding = Encoding.UTF8;
        //string json = client.UploadString(apiUrl + "/UpdateRepSyncData", inputJson);
        //return true;

        CommonClient commonclient = new CommonClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        SyncRequestModel model = new SyncRequestModel();
        model.originator = Originator;
        model.originatorType = OriginatorType;
        model.DeviceIMEI = DeviceIMEI;
        var delstat = commonclient.UpdateRepSyncData(model);

        
        if (delstat > 0)
            delstatus = true;

        return delstatus;
    }

    [WebMethod]
    public bool UpdateOrderIsInvoice(string OriginatorType, string Originator, string ActiveOrderId, bool IsActive)
    {
        var delstatus = false;

        CommonClient commonclient = new CommonClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        OrderIsInvoiceActivate model = new OrderIsInvoiceActivate();
        model.originator = Originator;
        model.originatorType = OriginatorType;
        model.orderId = ActiveOrderId;
        model.isActive = IsActive;
        var delstat = commonclient.UpdateOrderIsInvoiceActivate(model);


        if (delstat == true)
            delstatus = true;

        return delstatus;
    }

    [WebMethod]
    public DataSourceResult UpdateOrderIsInvoiceAct(string OriginatorType, string Originator, string ActiveOrderId, bool IsActive)
    {
        CommonClient commonclient = new CommonClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        OrderIsInvoiceActivate model = new OrderIsInvoiceActivate();
        model.originator = Originator;
        model.originatorType = OriginatorType;
        model.orderId = ActiveOrderId;
        model.isActive = IsActive;
        var delstatus = commonclient.UpdateOrderIsInvoiceActivate(model);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult LoadSRRouteLocations(string apiUri, string selectRep, string selectDate)
    {
        //MasterClient masterlient = new MasterClient();

        //List<Peercore.CRM.Model.SalesRepRouteSequenceModel> srRouteSeqList = new List<Peercore.CRM.Model.SalesRepRouteSequenceModel>();

        //string apiUrl = apiUri;
        //object request = new
        //{
        //    selectRep = selectRep,
        //    selectDate = selectDate
        //};

        //string inputJson = (new JavaScriptSerializer()).Serialize(request);
        //WebClient client = new WebClient();
        //client.Headers["Content-type"] = "application/json";
        //client.Encoding = Encoding.UTF8;

        //string json = client.UploadString(apiUrl + "/GetAllSRRouteLocations", inputJson);
        //srRouteSeqList = JsonConvert.DeserializeObject<List<Peercore.CRM.Model.SalesRepRouteSequenceModel>>(json);

        //DataSourceResult dataSourceResult = new DataSourceResult();

        //if (srRouteSeqList != null && srRouteSeqList.Count != 0)
        //{
        //    dataSourceResult = new DataSourceResult();
        //    dataSourceResult.Data = srRouteSeqList;
        //    dataSourceResult.Total = srRouteSeqList.Count;
        //}
        //else
        //{
        //    dataSourceResult = new DataSourceResult();
        //    dataSourceResult.Data = srRouteSeqList;
        //    dataSourceResult.Total = 0;
        //}

        //return dataSourceResult;

        CommonClient masterlient = new CommonClient();

        List<SalesRepRouteSequenceModel> srRouteSeqList = new List<SalesRepRouteSequenceModel>();

        SRRouteSequenceRequestModel model = new SRRouteSequenceRequestModel();
        model.selectDate = selectDate;
        model.selectRep = selectRep;

        srRouteSeqList = masterlient.GetAllSRRouteLocations(model);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (srRouteSeqList != null && srRouteSeqList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srRouteSeqList;
            dataSourceResult.Total = srRouteSeqList.Count;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srRouteSeqList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }
}
