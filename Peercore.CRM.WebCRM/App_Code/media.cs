﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for media
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
[ScriptService]
public class media : System.Web.Services.WebService
{
    public media()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region - Upload Media Files -

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllMedia(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string repCode, string category, string custCode)
    {
        MediaClient mediaClient = new MediaClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllMediaSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " MediaFileName asc";
            }
        }
        else
        {
            args.OrderBy = " MediaFileName asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterMediaField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }


        List<UploadMediaModel> data = new List<UploadMediaModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = mediaClient.GetAllMedia(repCode, category, custCode, args);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public string LoadCustomerName(string custCode)
    {
        MediaClient mediaClient = new MediaClient();

        string name = mediaClient.LoadMTCustomerName(custCode);

        return name;
    }


    private static string GetAllMediaSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " MediaFileName asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "MediaFileName":
                sortStr = " MediaFileName " + sortoption;
                break;
            case "MediaCategory":
                sortStr = " category_name " + sortoption;
                break;
            default:
                sortStr = " MediaFileId asc ";
                break;
        }

        return sortStr;
    }

    private string GetFilterMediaField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "MediaFileName":
                realfield = "MediaFileName";
                break;
            case "MediaCategory":
                realfield = "category_name";
                break;
        }

        return realfield;
    }

    #endregion

    #region - Media Category -

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllMediaCategories(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        MediaClient mediaClient = new MediaClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllCategorySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " category_name asc";
            }
        }
        else
        {
            args.OrderBy = " category_name asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterCategoryField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<MediaCategoryModel> data = new List<MediaCategoryModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = mediaClient.GetAllCategories(args);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    private static string GetAllCategorySortString(string sortby, string gridSortOrder)
    {
        string sortStr = " AssetTypeName asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Description":
                sortStr = " description " + sortoption;
                break;
            case "AssetTypeName":
                sortStr = " asset_type_name " + sortoption;
                break;
            default:
                sortStr = " asset_type_id asc ";
                break;
        }

        return sortStr;
    }

    private string GetFilterCategoryField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "AssetTypeName":
                realfield = "asset_type_name";
                break;
            case "Description":
                realfield = "description";
                break;
        }

        return realfield;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveMediaCategory(string catName, string description, string Originator, string id)
    {
        ArgsModel args = new ArgsModel();
        MediaClient mediaService = new MediaClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (mediaService.GetAllCategories(args).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            MediaCategoryModel model = new MediaCategoryModel();
            model.CatName = catName;
            model.Description = description;
            model.CreatedBy = Originator;

            bool delstatus = false;

            if (String.IsNullOrEmpty(id))
            {
                delstatus = mediaService.SaveMediaCategory(model);
            }
            else
            {
                delstatus = mediaService.UpdateMediaCategory(Int32.Parse(id), catName, description, Originator);
            }

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }


    #endregion
}
