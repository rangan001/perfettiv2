﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for mothly_deduction_service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class mothly_deduction_service : System.Web.Services.WebService
{

    public mothly_deduction_service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllDeductions(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        MonthlyDeductionClient deductionClient = new MonthlyDeductionClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllDeductionSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " id asc";
            }
        }
        else
        {
            args.OrderBy = " id asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterDeductionField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<MonthlyDeductionModel> data = new List<MonthlyDeductionModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = deductionClient.GetAllDeductions(args);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveDeduction(string deductId, string repCode, string deductMonth, decimal deductAmount, string Originator)
    {
        ArgsModel args = new ArgsModel();
        MonthlyDeductionClient deductionService = new MonthlyDeductionClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (deductionService.GetAllDeductions(args).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            MonthlyDeductionModel model = new MonthlyDeductionModel();
            model.RepCode = repCode;
            model.DeductionMonthName = deductMonth;
            model.DeductionAmount = deductAmount;

            bool delstatus = false;

            if (string.IsNullOrEmpty(deductId))
            {
                model.CreatedBy = Originator;
                delstatus = deductionService.SaveDeduction(model);
            }
            else
            {
                model.Id = Int32.Parse(deductId);
                model.LastModifiedBy = Originator;
                delstatus = deductionService.UpdateDeduction(model);
            }

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }

    [WebMethod(EnableSession = true)]
    public Peercore.CRM.Model.ResponseModel IsRepCodeAvailable(string repCode)
    {
        MonthlyDeductionClient deductionService = new MonthlyDeductionClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        response.Status = deductionService.IsRepCodeAvailable(repCode);
        response.StatusCode = "Exsist";

        return response;
    }


    private static string GetAllDeductionSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " DeductionMonthName asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "DeductionMonthName":
                sortStr = " deduction_month " + sortoption;
                break;
            case "DeductionAmount":
                sortStr = " deduction_amount " + sortoption;
                break;
            default:
                sortStr = " payment_id asc ";
                break;
        }

        return sortStr;
    }

    private string GetFilterDeductionField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "RepCode":
                realfield = "rep_code";
                break;
            case "DeductionMonth":
                realfield = "deduction_month";
                break;
            case "DeductionMonthName":
                realfield = "deduction_month";
                break;
            case "DeductionAmount":
                realfield = "deduction_amount";
                break;

        }

        return realfield;
    }



}
