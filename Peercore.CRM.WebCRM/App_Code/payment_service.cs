﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for asset_service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class payment_service : System.Web.Services.WebService
{

    public payment_service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllPayments(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        PaymentsClient paymentClient = new PaymentsClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllPaymentSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " payment_id asc";
            }
        }
        else
        {
            args.OrderBy = " payment_id asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterPaymentField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<PaymentModel> data = new List<PaymentModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = paymentClient.GetAllPayments(args);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SavePayment(int payId, string repCode, int payYear, string payMonth, DateTime payDate,
        int billNo, decimal billAmount, decimal allowance, string remark, string Originator)
    {
        ArgsModel args = new ArgsModel();
        PaymentsClient paymentService = new PaymentsClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (paymentService.GetAllPayments(args).Count > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            PaymentModel model = new PaymentModel();
            model.PaymentId = payId;
            model.RepCode = repCode;
            model.PayYear = payYear;
            model.PayMonthName = payMonth;
            model.PaymentDate = Convert.ToDateTime(payDate);
            model.BillNo = billNo;
            model.BillAmount = billAmount;
            model.Allowance = allowance;
            model.Remark = remark;
            //model.Status = Int32.Parse(status);


            bool delstatus = false;

            if ((payId == 0))
            {
                model.CreatedBy = Originator;
                delstatus = paymentService.SavePayment(model);
            }
            else
            {
                //model.PaymentId = payId;
                model.LastModifiedBy = Originator;
                delstatus = paymentService.UpdatePayment(model);
            }

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }


    private static string GetAllPaymentSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " PaymentId asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "PayYear":
                sortStr = " payment_year " + sortoption;
                break;
            case "PayMonth":
                sortStr = " payment_month " + sortoption;
                break;
            case "BillNo":
                sortStr = " mobile_bill_no " + sortoption;
                break;
            case "BillAmount":
                sortStr = " bill_amount " + sortoption;
                break;
            case "Allowance":
                sortStr = " allowance " + sortoption;
                break;
            default:
                sortStr = " payment_id asc ";
                break;
        }

        return sortStr;
    }

    private string GetFilterPaymentField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "RepCode":
                realfield = "rep_code";
                break;
            case "PayYear":
                realfield = "payment_year";
                break;
            case "PayMonthName":
                realfield = "payment_month";
                break;
            case "BillNo":
                realfield = "mobile_bill_no";
                break;
            case "BillAmount":
                realfield = "bill_amount";
                break;
            case "Allowance":
                realfield = "allowance";
                break;
            case "PaymentDate":
                realfield = "payment_date";
                break;
            case "Remark":
                realfield = "remark";
                break;
        }

        return realfield;
    }


}
