﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Peercore.CRM.Entities;
//using Telerik.Web.UI;
using CRMServiceReference;
using System.ComponentModel;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.UI;

/// <summary>
/// Summary description for pipelines_stage_service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
[ScriptService]
public class pipelines_stage_service : System.Web.Services.WebService
{
    public pipelines_stage_service()
    {
        //CommonClient test = new CommonClient();

        //MessagesDTO kk = test.GetMessageCount("", "");
    }

    #region Constant
    
    private const string OPPORTUNITY_ID ="OPPORTUNITY_ID";
    private const string CATALOGLIST = "CATALOGLIST";
    
    #endregion Constant

    #region Properties

    private KeyValuePair<string, string> PipelineChart
    {
        get
        {
            if (Session[CommonUtility.PIPELINE_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.PIPELINE_DATA];
            }
            return new KeyValuePair<string, string>("0", CommonUtility.PIPELINE_DATA);
        }
        set
        {
            Session[CommonUtility.PIPELINE_DATA] = value;
        }
    }

    private int OpportunityID
    {
        get
        {
            if (Session[OPPORTUNITY_ID] != null)
            {
                return int.Parse( Session[OPPORTUNITY_ID].ToString());
            }
            return 0;
        }
        set
        {
            Session[OPPORTUNITY_ID] = value;
        }
    }

    public List<string> BackButtonForAll
    {
        get
        {
            if (Session[CommonUtility.BACK_BUTTON_FOR_ALL] != null)
                return ((List<string>)(Session[CommonUtility.BACK_BUTTON_FOR_ALL]));

            return null;
        }
        set
        {
            if (value != null)
                Session[CommonUtility.BACK_BUTTON_FOR_ALL] = value;
            else
                Session.Remove(CommonUtility.BACK_BUTTON_FOR_ALL);
        }
    }
    #endregion Properties

    OpportunityClient OpportunityService = new OpportunityClient();

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetOppDataAndCount(string pipelineStageId, string status, string clientType, int take, int skip, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        List<CustomerOpportunitiesDTO> lstOpportunity = null;
        List<ClientOpportunityDTO> lstClientOpportunity = null;
        ArgsDTO argsDTO = new ArgsDTO();
        CommonUtility commonUtility = new CommonUtility();
        KeyValuePair<string, string> pipelineChart = PipelineChart;
       
        if (filter != null)
        {
            SetFilterField(filter);
            argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        argsDTO.Originator = UserSession.Instance.UserName;
        if (sortExpression != null && sortExpression.Count > 0)
        {
            foreach (Sort item in sortExpression)
            {
                item.Field = GetSortType(item.Field);
                if (string.IsNullOrEmpty(argsDTO.OrderBy))
                    argsDTO.OrderBy = item.ToExpression();
                else
                    argsDTO.OrderBy = ", " + argsDTO.OrderBy + item.ToExpression();
            }
        }
        else
        {
            argsDTO.OrderBy = "Name ASC, close_date DESC";
        }
        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = take;

        if (status == "0" && !string.IsNullOrEmpty(clientType))
        {
            if (clientType == "Lead")
                clientType = "lead_stage != 'Customer' AND lead_stage != 'End User'" ;
            else if (clientType == "Customer")
                clientType = "lead_stage = 'Customer'" ;
            else if (clientType == "End User")
                clientType = "lead_stage = 'End User'" ;
            else
                clientType = " 0=0 ";
            if (string.IsNullOrEmpty(argsDTO.AdditionalParams))
            {
                argsDTO.AdditionalParams = clientType;
            }
            else
            {
                argsDTO.AdditionalParams = argsDTO.AdditionalParams + " AND " + clientType;
            }
        }

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                argsDTO.StartIndex = (skip / take) + 1;
            }
            else
            {
                argsDTO.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            argsDTO.StartIndex = (skip / take) + 1;
        }

        List<string> back = new List<string>();
        back.Add(argsDTO.StartIndex.ToString());
        BackButtonForAll = back;

       // int isCustomerPipeline = int.Parse(Session[CommonUtility.SELECTED_CUSTOMER].ToString());

        if (status == "0")
        {
            
            lstOpportunity = OpportunityService.GetAllOpportunities(int.Parse(pipelineStageId), argsDTO);
        }
        else
        {
            lstClientOpportunity = OpportunityService.GetAllClientOpportunity(int.Parse(pipelineStageId), argsDTO);
        }
        //if (isCustomerPipeline == 0 && int.Parse(pipelineChart.Key) > 0)
        //{
        //    lstOpportunity = OpportunityService.GetAllOpportunities(int.Parse(pipelineChart.Key), UserSession.Instance.UserName, ArgsDTO, null);
        //}
        //else if (isCustomerPipeline == 1 && int.Parse(pipelineChart.Key) > 0)
        //{
        //    ArgsDTO.OrderBy = ArgsDTO.OrderBy == "Name ASC, close_date DESC" ? "Name , close_date ASC" : ArgsDTO.OrderBy; 
        //    lstOpportunity = OpportunityService.GetOpportunitiesForPipeGraph(ArgsDTO, int.Parse(pipelineChart.Key), true);
        //}
        //else if (isCustomerPipeline == 1 && int.Parse(pipelineChart.Key) == 0)
        //{
        //    ArgsDTO.OrderBy = ArgsDTO.OrderBy == "Name ASC, close_date DESC" ? "Name , close_date ASC" : ArgsDTO.OrderBy; 
        //    lstOpportunity = OpportunityService.GetOpportunitiesForPipeGraph(ArgsDTO, int.Parse(pipelineChart.Key), false);
        //}
        //else
        //{
        //    lstOpportunity = OpportunityService.GetAllOpportunities(int.Parse(pipelineChart.Key), UserSession.Instance.UserName, ArgsDTO, null);
        //}

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (lstOpportunity != null && lstOpportunity.Count > 0)
        {
            dataSourceResult.Data = lstOpportunity;
            dataSourceResult.Total = lstOpportunity[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = lstClientOpportunity;
            dataSourceResult.Total = lstClientOpportunity.Count;
            
        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    #region Home Page Opportunities
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetOpportunitiesForHome(int take, int skip)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        #endregion

        List<CustomerOpportunitiesDTO> data = new List<CustomerOpportunitiesDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {

            args.StartIndex = skip / take + 1;
            args.RowCount = take;
            args.SStartDate = DateTime.Now.ToString(ConfigUtil.DateTimePatternForSql);

            args.OrderBy = " close_date desc";

            data = OpportunityService.GetAllOpportunities(0, args);

            if (data != null)
            {
                if (data.Count != 0)
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = data[0].rowCount;
                }
                else
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = 0;
                }
            }
            else
            {
                obj_dataSourceResult = new DataSourceResult();
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }
    #endregion

    private string GetSortType(string sortName)
    {
        string sortingType = "";

        try
        {
            if (sortName == "OpportunityID")
            {
                sortingType = "opportunity_id";
            }
            else if (sortName == "LeadID")
            {
                sortingType = "lead_id";
            }
            else if (sortName == "Originator")
            {
                sortingType = "Originator";
            }
            else if (sortName == "LeadName")
            {
                sortingType = "ContactName";
            }
            else if (sortName == "Name")
            {
                sortingType = "name";
            }
            else if (sortName == "Description")
            {
                sortingType = "description";
            }
            else if (sortName == "CloseDate")
            {
                sortingType = "close_date";
            }
            else if (sortName == "Probability")
            {
                sortingType = "probability";
            }
            else if (sortName == "Amount")
            {
                sortingType = "amount";
            }
            else if (sortName == "Units")
            {
                sortingType = "units";
            }

            else if (sortName == "Tonnes")
            {
                sortingType = "tonnes";
            }
            else if (sortName == "LeadStage")
            {
                sortingType = "lead_stage";
            }
            else if (sortName == "Business")
            {
                sortingType = "business";
            }
            else if (sortName == "IndustryDescription")
            {
                sortingType = "industry_description";
            }
            else if (sortName == "Address")
            {
                sortingType = "address";
            }
            else if (sortName == "City")
            {
                sortingType = "city";
            }
            else if (sortName == "State")
            {
                sortingType = "state";
            }
            else if (sortName == "PostCode")
            {
                sortingType = "postcode";
            }

            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void SetFilterField(Filter filter)
    {
        if (filter.Filters != null)
        {
            foreach (Filter item in filter.Filters)
            {
                item.Field = GetSortType(item.Field);
                if (item.Filters != null)
                {
                    foreach (Filter itemTo in item.Filters)
                    {
                        SetFilterField(itemTo);
                    }
                }
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public List<LeadDTO> GetLeadNameList(string name)
    {
        try
        {
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.CacheControl = "no-cache";
            //Setting Args
            IList<string> result = new List<string>();
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.Originator;
            args.AdditionalParams = name;

            LeadClient leadClient = new LeadClient();

            List<LeadDTO> leadList = leadClient.GetLeadMiniViewCollection(args);
            foreach (LeadDTO item in leadList)
            {
                result.Add(item.LeadName);
            }
            return leadList;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
}
