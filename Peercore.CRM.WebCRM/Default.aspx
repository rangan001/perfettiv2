﻿<%@ Page Title="mSales | Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Src="~/usercontrols/salesinfo.ascx" TagPrefix="ucl1" TagName="salesinfo" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
	<script src="https://kendo.cdn.telerik.com/2016.2.607/js/jszip.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
	<asp:HiddenField ID="HiddenFieldRepCode" runat="server" />
	<asp:HiddenField ID="hfPageIndex" runat="server" />
	<asp:HiddenField ID="HiddenField1" runat="server" />
	<asp:HiddenField ID="userName" runat="server" />
	<asp:HiddenField ID="duration" runat="server" Value="monthly" />
	<asp:HiddenField ID="GraphTitle" runat="server" />
	<asp:HiddenField ID="pieChartTitle" runat="server" />
	<asp:HiddenField ID="GraphDuration" runat="server" />
	<asp:HiddenField ID="ISProductOrASM" runat="server" />
	<asp:HiddenField ID="hfOriginatorString" runat="server" />
	<asp:HiddenField ID="hfOriginatorTypeString" runat="server" />
	<script type="text/javascript">
		$(document).ready(function () {
			//loadHomePageTabs();
		});
	</script>
	<style type="text/css">
		#tabstrip {
			height: 100%;
			border-width: 0;
		}

		.ButtonEnableASMProduct {
			background: rgb(50, 88, 140) none repeat scroll 0% 0%;
			color: rgb(255, 255, 255);
		}

		.ButtonDisableASMProduct {
			background: #ddd;
			color: #333;
		}
	</style>
	<div id="div_salesinfo" style="display: none">
		<ucl1:salesinfo ID="custsalesinfo" runat="server" />
	</div>

	<div class="tile-box-wrap" style="display: none;">

		<div class="tile-box">
			<div class="card mb-3 widget-content bg-pink">
				<div class="widget-content-wrapper text-white">
					<div class="widget-content-left">
						<div class="widget-heading">Total Orders</div>
						<div class="widget-subheading">Last year expenses</div>
					</div>
					<div class="widget-content-right">
						<div class="widget-numbers text-white"><span>1896</span></div>
					</div>
				</div>
			</div>
		</div>

		<div class="tile-box">
			<div class="card mb-3 widget-content bg-green">
				<div class="widget-content-wrapper text-white">
					<div class="widget-content-left">
						<div class="widget-heading">Clients</div>
						<div class="widget-subheading">Total Clients Profit</div>
					</div>
					<div class="widget-content-right">
						<div class="widget-numbers text-white"><span>$ 560</span></div>
					</div>
				</div>
			</div>
		</div>

		<div class="tile-box">
			<div class="card mb-3 widget-content bg-blue">
				<div class="widget-content-wrapper text-white">
					<div class="widget-content-left">
						<div class="widget-heading">Followers</div>
						<div class="widget-subheading">People Interested</div>
					</div>
					<div class="widget-content-right">
						<div class="widget-numbers text-white"><span>56%</span></div>
					</div>
				</div>
			</div>
		</div>

		<div class="tile-box">
			<div class="card mb-3 widget-content bg-yellow">
				<div class="widget-content-wrapper text-white">
					<div class="widget-content-left">
						<div class="widget-heading">All Product</div>
						<div class="widget-subheading">Last Campaign</div>
					</div>
					<div class="widget-content-right">
						<div class="widget-numbers text-white"><span>200</span></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="barchart" runat="server">
		<div class="row">
			<div class="divcontectmainforms">
				<div id="div2" class="barchart-btn">
					<asp:Button ID="btnASMSales" runat="server" Visible="true" Text="ASM Sales"
						OnClientClick="LoadASMSalesBarChart();return false;" Style="border: medium none; margin: 0px; height: 32px;" />
					<asp:Button ID="btnProductSales" runat="server" Visible="true" Text="Product Sales"
						OnClientClick="LoadProductSalesBarChart();return false;" Style="border: medium none; margin: 0px; height: 32px;" />
				</div>
				<div style="padding-top: 1px;">
					<h3>
						<div id="Label4">
							ASM Sales Summary
						</div>
					</h3>
				</div>
				<div class="toolbar_container">
					<div class="toolbar_left" id="div_content" style="width: 60% !important">
						<span id="span_cmbase" runat="server" visible="true">
							<%--<asp:Label ID="lblDDLASMProduct" runat="server" Text="ASM : " Visible="true" Style="float: left;
                                padding-top: 7px; padding-left: 5px; padding-right: 10px;"></asp:Label>--%>
							<asp:DropDownList ID="cmbase" runat="server" Visible="true" AutoPostBack="false"
								Width="190px" onchange="cmboase_SelectedIndexChanged()" Style="display: block">
							</asp:DropDownList>
							<asp:DropDownList ID="ddlProduct" runat="server" Visible="true" AutoPostBack="false"
								Width="190px" onchange="ddlProduct_SelectedIndexChanged()" Style="display: none">
							</asp:DropDownList>
						</span>
					</div>
					<div class="toolbar_right" id="div_content_2" style="width: 18% !important">
						<asp:Button ID="getYearSales" runat="server" Visible="true" Text="Last 12 Months"
							OnClientClick="getLastYearSalesClick();return false;" Style="line-height: 10px;" />
						<asp:Button ID="getMonthSales" runat="server" Visible="false" Text="Current Month"
							OnClientClick="getMonthSalesClick();return false;" Style="line-height: 10px;" />
					</div>
				</div>
				<div id="sales_info">
				</div>
			</div>
		</div>
	</div>

	<%-- Rep Attendance Bar Chart --%>
	<div id="bottom_div">
		<div id="attendanceChart" runat="server" style="width: 100%; border: black 2px; margin-left: 10px; margin-right: 10px; display: block;">
			<div class="row">
				<div class="divcontectmainforms">
					<div style="padding-top: 1px;">
						<h3>
							<div id="DivAttendance1">
								Sales Reps Attendance
							</div>
						</h3>
					</div>
					<div class="toolbar_container">
						<div class="toolbar_left" id="DivAttendance2" style="width: 60% !important">
							<span id="span3" runat="server" visible="true">
								<%--<asp:Label ID="Label5" runat="server" Text="ASM : " Visible="true" Style="float: left;
                                padding-top: 7px; padding-left: 5px; padding-right: 10px;"></asp:Label>--%>
								<asp:DropDownList ID="ddlASMforAttendance" runat="server" Visible="true" AutoPostBack="false"
									Width="190px" onchange="ddlASMforAttendance_SelectedIndexChanged()" Style="display: block">
								</asp:DropDownList>
								<asp:TextBox ID="txtAttendanceDate" runat="server" Style="width: 200px;" ClientIDMode="Static"></asp:TextBox>
							</span>
						</div>
					</div>
					<div id="reps_attendance">
					</div>
					<div style="overflow-x:scroll">
					<div id="repAttendanceDetails" style="border-bottom: medium none navy; border-top: medium none; float: left; border-top: 1px solid #ddd; ">
					</div>

					</div>
				</div>


			</div>
			<div class="row">
				<div id="attendanceDetails" runat="server" style="border: black 2px; margin-left: 10px; margin-right: 10px; " class="divcontectmainforms">
					
				</div>
			</div>
		</div>

	</div>

	<script type="text/javascript">
		var monthdata = [];
		var salesdata = [];
		var ASMSales = [];
		var repcodedata = [];
		var targetdata = [];
		var GraphTitle = $("#<%= GraphTitle.ClientID %>").val();
		var GraphDuration;
		var GraphDuration = $("#<%= GraphDuration.ClientID %>").val();
		var pieChartTitle = $("#<%= pieChartTitle.ClientID %>").val();
		var selectedPieChartTitle;
		var ASM;
		var selectedDate;
		var selectedASM = 'ALL';
		var slaesType = 'ALL';
		var attendType = 'ALL';

		$(document).ready(function () {

			selectedDate = new Date();

			selectedASM = $("#MainContent_ddlASMforAttendance option:selected").val();

			$("#MainContent_btnASMSales").addClass("ButtonEnableASMProduct");
			$("#MainContent_btnProductSales").addClass("ButtonDisableASMProduct");

			$("#txtAttendanceDate").kendoDatePicker({
				// defines the start view
				start: "month",

				// defines when the calendar should return date
				depth: "month",

				// display month and year in the input
				format: "MMMM dd yyyy",
				change: function (e) {
					selectedDate = $("#txtAttendanceDate").data("kendoDatePicker").value();
					LoadRepsAttendanceChart(selectedASM, selectedDate);
					loadRepAttendanceDetails(selectedDate, selectedASM, slaesType, attendType);
				},
				value: new Date()
			});



			//load data to barchart
			LoadASMSalesBarChart();

			LoadRepsAttendanceChart(selectedASM, selectedDate);
			loadRepAttendanceDetails(selectedDate, selectedASM, slaesType, attendType);

			$("#top_right_navigation_button").removeAttr("style");
		});

		function getSalesForBarChart() {
			selectedPieChartTitle = new Date().getFullYear() + " " + monthNames[new Date().getMonth()] + " ASM Sales";

			GraphTitle = $("#<%= GraphTitle.ClientID %>").val();

			var userName = $("#<%= userName.ClientID %>").val();
			var uName = $("#<%= cmbase.ClientID %>").val();
			var duration = $("#<%= duration.ClientID %>").val();
			var isasmorproduct = $("#<%= ISProductOrASM.ClientID %>").val();
			var productid = $("#<%= ddlProduct.ClientID %>").val();

			if (isasmorproduct == 'A') {
				param = { "UserName": uName, "duration": duration, "IsASMOrProduct": isasmorproduct, "ProductId": productid };
			}
			else if (isasmorproduct == 'P') {
				param = { "UserName": "", "duration": duration, "IsASMOrProduct": isasmorproduct, "ProductId": productid };
			}

			$.ajax({
				type: "POST",
				url: "service/lead_customer/common.asmx/getSalesData",
				data: JSON.stringify(param),
				contentType: "application/json; charset=utf-8",
				dataType: "json", // dataType is json format
				beforeSend: function () {
					// Handle the beforeSend event
				},
				success: function (response) {
					var data = response.d
					salesdata = [];

					$.each(data, function (i) {
						monthdata[i] = data[i].MonthName;
						salesdata[i] = data[i].sales;
					});

					setTimeout(
						function () {

							var barChartData = {
								chart: {
									renderTo: 'sales_info',
									type: 'column'
								},
								title: {
									text: GraphTitle
								},
								xAxis: {
									categories: monthdata,
									title: {
										text: GraphDuration
									}
								},
								yAxis: {
									title: {
										text: 'Total Sales value'
									}
								},
								series: [{
									data: salesdata,
									name: 'Sales',
									color: '#31578C'
								}],

								legend: {
									enabled: true
								},
								credits: {
									enabled: false
								},
								plotOptions: {
									series: {
										cursor: 'pointer',
										point: {
											events: {
												click: function () {
													//alert('X-value: ' + this.x + ', Y-value: ' + this.y);
												}
											}
										}
									}
								}
							};

							var barChart = new Highcharts.Chart(barChartData);
						}, 1000);

				},
				error: OnErrorCall
			});

			function OnErrorCall(response) { }
		}

		function ASMData(name, sales, id) {
			this.name = name;
			this.y = sales;
			this.id = id;
		}

		var monthNames = ["January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December"
		];

		function cmboase_SelectedIndexChanged() {
			if ($("#MainContent_cmbase").val() == '') {
				var username = document.getElementById('<%= userName.ClientID %>');
				username.value = "";
				if ($("#<%= duration.ClientID %>").val() == "daily") {
					// set graph title 
					GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
					GraphTitle.value = "This Month sales";
				}
				else {
					// set graph title 
					GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
					GraphTitle.value = "Last 12 Months sales";
				}

				getSalesForBarChart();
			}
			else {
				var seletedASM = $("#MainContent_cmbase  option:selected").text();
				var username = document.getElementById('<%= userName.ClientID %>');

				username.value = seletedASM;

				if ($("#<%= duration.ClientID %>").val() == "daily") {
					// set graph title 
					GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
					GraphTitle.value = "This Month sales For " + seletedASM + "";
				}
				else {
					// set graph title 
					GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
					GraphTitle.value = "Last 12 Months sales For " + seletedASM + "";
				}

				getSalesForBarChart();
			}
		}

		function ddlProduct_SelectedIndexChanged() {
			var seletedASM = $("#MainContent_ddlProduct option:selected").text();

			GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
			GraphTitle.value = "Last 12 Months sales For " + seletedASM + "";

			getSalesForBarChart();
		}

		function getMonthSalesClick() {
			if ($("#MainContent_cmbase").val() == '') {
				var duration = document.getElementById('<%= duration.ClientID %>');
				duration.value = "daily";
				var username = document.getElementById('<%= userName.ClientID %>');
				username.value = "";
				//set graph X title
				GraphDuration = "Days";
				//set graph title 
				GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
				GraphTitle.value = "This Month sales";
			}
			else {
				var seletedASM = $("#MainContent_cmbase").val()
				userName = seletedASM;
				var duration = document.getElementById('<%= duration.ClientID %>');
				duration.value = "daily";
				//set graph X title
				GraphDuration = "Days";
				// set graph title 
				GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
				GraphTitle.value = "This Month sales For " + seletedASM + "";
			}

			getSalesForBarChart();
		}

		function getLastYearSalesClick() {
			if ($("#MainContent_cmbase").val() == '') {

				var duration = document.getElementById('<%= duration.ClientID %>');
				duration.value = "monthly";
				userName = "";
				//set graph X title
				GraphDuration = "Months";
				//set graph title 
				GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
				GraphTitle.value = "Last 12 Months sales";
			}
			else {

				var seletedASM = $("#MainContent_cmbase").val()
				userName = seletedASM;
				var duration = document.getElementById('<%= duration.ClientID %>');
				duration.value = "monthly";
				//set graph X title
				GraphDuration = "Months";
				//set graph title 
				GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
				GraphTitle.value = "Last 12 Months sales For " + seletedASM + "";
			}
			getSalesForBarChart();
		}

		function getMonthSalesClick() {
			if ($("#MainContent_cmbase").val() == '') {
				var duration = document.getElementById('<%= duration.ClientID %>');
				duration.value = "daily";
				var username = document.getElementById('<%= userName.ClientID %>');
				username.value = "";
				//set graph X title
				GraphDuration = "Days";
				//set graph title 
				GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
				GraphTitle.value = "This Month sales";
			}
			else {
				var seletedASM = $("#MainContent_cmbase").val()
				userName = seletedASM;
				var duration = document.getElementById('<%= duration.ClientID %>');
				duration.value = "daily";
				//set graph X title
				GraphDuration = "Days";
				// set graph title 
				GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
				GraphTitle.value = "This Month sales For " + seletedASM + "";
			}
			getSalesForBarChart();
		}

		function LoadASMSalesBarChart() {
			$("#MainContent_ISProductOrASM").val("A");
			document.getElementById("Label4").innerHTML = "ASM Sales Summary";
			//document.getElementById("MainContent_lblDDLASMProduct").innerHTML = "ASM  : ";

			$("#MainContent_cmbase").css("display", "block");
			$("#MainContent_ddlProduct").css("display", "none");

			$("#MainContent_btnASMSales").addClass("ButtonEnableASMProduct");
			$("#MainContent_btnProductSales").addClass("ButtonDisableASMProduct");
			$("#MainContent_btnASMSales").removeClass("ButtonDisableASMProduct");
			$("#MainContent_btnProductSales").removeClass("ButtonEnableASMProduct");

			GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
			GraphTitle.value = "This Month sales";

			getSalesForBarChart();
		}

		function LoadProductSalesBarChart() {
			$("#MainContent_ISProductOrASM").val("P");
			document.getElementById("Label4").innerHTML = "Product Sales Summary";
			//document.getElementById("MainContent_lblDDLASMProduct").innerHTML = "Product  : ";

			$("#MainContent_ddlProduct").css("display", "block");
			$("#MainContent_cmbase").css("display", "none");

			$("#MainContent_btnASMSales").addClass("ButtonDisableASMProduct");
			$("#MainContent_btnProductSales").addClass("ButtonEnableASMProduct");
			$("#MainContent_btnASMSales").removeClass("ButtonEnableASMProduct");
			$("#MainContent_btnProductSales").removeClass("ButtonDisableASMProduct");

			GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
			GraphTitle.value = "This Month sales";

			getSalesForBarChart();
		}

		function getLastYearSalesClick() {
			if ($("#MainContent_cmbase").val() == '') {

				var duration = document.getElementById('<%= duration.ClientID %>');
				duration.value = "monthly";
				userName = "";
				//set graph X title
				GraphDuration = "Months";
				// set graph title 
				GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
				GraphTitle.value = "Last 12 Months sales";
			}

			else {

				var seletedASM = $("#MainContent_cmbase").val()
				userName = seletedASM;
				var duration = document.getElementById('<%= duration.ClientID %>');
				duration.value = "monthly";
				//set graph X title
				GraphDuration = "Months";
				// set graph title 
				GraphTitle = document.getElementById('<%= GraphTitle.ClientID %>');
				GraphTitle.value = "Last 12 Months sales For " + seletedASM + "";

			}

			GraphTitle.value = "Last 12 Months sales";
			getSalesForBarChart();

		}

		function ddlASMforAttendance_SelectedIndexChanged() {
			selectedASM = $("#MainContent_ddlASMforAttendance option:selected").val();

			slaesType = 'ALL';
			attendType = 'ALL';

			LoadRepsAttendanceChart(selectedASM, selectedDate);
			loadRepAttendanceDetails(selectedDate, selectedASM, slaesType, attendType);
		}

		//For Reps Attendance 
		function LoadRepsAttendanceChart(seletedASM, date) {
			var pieChartTitle = '';//$("#<%= pieChartTitle.ClientID %>").val();
            //var userName = $("#<%= userName.ClientID %>").val();

			var param = { "Originator": seletedASM, "SelectDate": date };

			$.ajax({
				type: "POST",
				url: "service/lead_customer/common.asmx/getDashboardAttendanceByRepType",
				data: JSON.stringify(param),
				contentType: "application/json; charset=utf-8",
				dataType: "json", // dataType is json format
				success: OnSuccess1,
				error: OnErrorCall
			});

			function OnSuccess1(response1) {

				var ASMdata = response1.d;

				if (ASMdata.length > 0) {
					$.each(ASMdata, function (i) {
						ASMSales[i] = new ASMData(ASMdata[i].description, ASMdata[i].attendanceCount, ASMdata[i].repType);
					});

					var pieChartData = {
						chart: {
							renderTo: 'reps_attendance',
							type: 'pie'
						},
						title: {
							text: pieChartTitle
						},
						plotOptions: {
							pie: {
								showInLegend: true,
								dataLabels: {
									enabled: true,
									format: '{point.name} : {point.y} '/*format: '{point.name} : {point.percentage:.1f} '*/
								}
							}
						},
						credits: {
							enabled: false
						},
						series: [{
							name: 'Attendance',
							colorByPoint: true,
							data: ASMSales,
							size: '80%',
							colors: ['#4773AD', '#ED561B', '#DDDF00'],
							point: {
								events: {
									click: function (event) {
										if (this.id == 'van') {
											salesType = 'van';
											attendType = 'ALL';
										}
										else if (this.id == 'pre') {
											salesType = 'pre';
											attendType = 'ALL';
										}
										else if (this.id == 'leave') {
											salesType = 'ALL';
											attendType = 'leave';
										}
										else {
											salesType = 'ALL';
											attendType = 'ALL';
										}

										loadRepAttendanceDetails(date, seletedASM, salesType, attendType);
									}
								}
							}
						}]
					};

					var pieChart = new Highcharts.Chart(pieChartData);
				}
			}
			function OnErrorCall(response) { }
		}

		//load sales repsales and discount grid 
		function loadRepAttendanceDetails(date, ASM, salesType, attendType) {

			var take_grid = $("#MainContent_hfPageIndex").val();

			$("#repAttendanceDetails").html("");
			$("#repAttendanceDetails").kendoGrid({
				height: 355,
				columns: [
					{ field: "repCode", title: "Rep Code", width: "90px" },
					{ field: "repName", title: "Rep Name", width: "220px" },
					{ field: "salesType", title: "Sales Type", width: "90px" },
					{ field: "attendType", title: "Attendance Type", width: "90px" },
					{ field: "checkInTime", title: "Clock-In Time", width: "130px" },
					{ field: "checkOutTime", title: "Clock-Out Time", width: "130px" },
					{ field: "checkInLat", title: "Clock-In Lat", width: "130px" },
					{ field: "checkInLng", title: "Clock-In Lng", width: "130px" },
					{ field: "checkOutLat", title: "Clock-Out Lat", width: "130px" },
					{ field: "checkOutLng", title: "Clock-Out Lng", width: "130px" },
					{ field: "comment", title: "Comment", width: "200px" }
				],
				scrollable: true,
				sortable: false,
				selectable: true,
				filterable: false,
				pageable: false,
				scrollable: true,
				toolbar: ["excel"],
				//pdf: {
				//    author: "Lohith G N",
				//    creator: "Telerik India",
				//    date: new Date(),
				//    fileName: "Northwind Product List.pdf",
				//    keywords: "northwind products",
				//    landscape: false,
				//    margin: {
				//        left: 10,
				//        right: "10pt",
				//        top: "10mm",
				//        bottom: "1in"
				//    },
				//    paperSize: "A4",
				//    subject: "Northwind Products",
				//    title: "Northwind Products"
				//},
				excelExport: exportGridWithTemplatesContent,
				dataSource: {
					serverSorting: true,
					serverPaging: true,
					serverFiltering: true,
					pageSize: 10,
					schema: {
						data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
						total: "d.Total",
						model: { // define the model of the data source. Required for validation and property types.
							id: "repCode",
							fields: {
								repCode: { validation: { required: true } },
								repName: { editable: false, nullable: true },
								salesType: { editable: false, nullable: true },
								attendType: { editable: false, nullable: true },
								checkInLat: { editable: false, nullable: true },
								checkInLng: { editable: false, nullable: true },
								checkInAddress: { editable: false, nullable: true },
								checkOutLat: { editable: false, nullable: true },
								checkOutLng: { editable: false, nullable: true },
								checkOutAddress: { editable: false, nullable: true },
								comment: { editable: false, nullable: true }
							}
						}
					},
					transport: {
						read: {
							url: ROOT_PATH + "service/lead_customer/common.asmx/GetDashboardAttendanceDetails", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
							contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
							data: { pgindex: take_grid, date: date, originator: ASM, salesType: salesType, attendType: attendType },
							type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
							complete: function (jqXHR, textStatus) {
								if (textStatus == "success") {
									var entityGrid = $("#repAttendanceDetails").data("kendoGrid");
									if ((take_grid != '') && (gridindex == '0')) {
										entityGrid.dataSource.page((take_grid - 1));
										gridindex = '1';
									}
								}
							}
						},
						parameterMap: function (data, operation) {
							if (operation != "read") {
								return JSON.stringify({ products: data.models })
							} else {
								data = $.extend({ sort: null, filter: null }, data);
								return JSON.stringify(data);
							}
						}
					}
				}
			});
		}

		function exportGridWithTemplatesContent(e) {
			var data = e.data;
			var gridColumns = e.sender.columns;
			var sheet = e.workbook.sheets[0];
			var visibleGridColumns = [];
			var columnTemplates = [];
			var dataItem;
			// Create element to generate templates in.
			var elem = document.createElement('div');

			// Get a list of visible columns
			for (var i = 0; i < gridColumns.length; i++) {
				if (!gridColumns[i].hidden) {
					visibleGridColumns.push(gridColumns[i]);
				}
			}

			// Create a collection of the column templates, together with the current column index
			for (var i = 0; i < visibleGridColumns.length; i++) {
				if (visibleGridColumns[i].template) {
					columnTemplates.push({ cellIndex: i, template: kendo.template(visibleGridColumns[i].template) });
				}
			}

			// Traverse all exported rows.
			for (var i = 1; i < sheet.rows.length; i++) {
				var row = sheet.rows[i];
				// Traverse the column templates and apply them for each row at the stored column position.

				// Get the data item corresponding to the current row.
				var dataItem = data[i - 1];
				for (var j = 0; j < columnTemplates.length; j++) {
					var columnTemplate = columnTemplates[j];
					// Generate the template content for the current cell.
					elem.innerHTML = columnTemplate.template(dataItem);
					if (row.cells[columnTemplate.cellIndex] != undefined)
						// Output the text content of the templated cell into the exported cell.
						row.cells[columnTemplate.cellIndex].value = elem.textContent || elem.innerText || "";
				}
			}
		}

	</script>

</asp:Content>
