﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class Invoice_transaction_InvoiceView : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                ArgsDTO args = new ArgsDTO();
                args.OrderBy = "name ASC";
                args.AdditionalParams = " dept_string = 'ASE' ";
                args.StartIndex = 1;
                args.RowCount = 500;
                
                OriginatorClient originatorClient = new OriginatorClient();
                List<OriginatorDTO> aseList = new List<OriginatorDTO>();
                aseList = originatorClient.GetOriginatorsByCatergory(args);

                if (aseList.Count > 0)
                    aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

                SetAses(aseList, "UserName");

                






                DateTime today = DateTime.Today;
                txtStartDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
                txtEndDate.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Invoices", "#", "");

        OriginatorString.Value = UserSession.Instance.OriginatorString;
    }

    public void SetAses(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        ddlASEList.DataSource = listOriginator;
        ddlASEList.DataTextField = "Name";
        ddlASEList.DataValueField = valueField;
        ddlASEList.DataBind();
    }

   
    protected void fileuploadBulkDeleteInvoice_Click(object sender, EventArgs e)
    {
        //string path = fileuploadBulkDeleteOutlet.PostedFile.FileName;
        string fileName = fileuploadBulkDeleteInvoice.FileName;
        fileName = "Invoice-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + fileName;
        string errorLog = "";

        string folderPath = "~/uploads/bulkdelete/";
        if (!Directory.Exists(MapPath(folderPath)))
        {
            Directory.CreateDirectory(MapPath(folderPath));
        }

        try
        {
            string filePath = folderPath + fileName;
            string savePath = MapPath(filePath);
            fileuploadBulkDeleteInvoice.SaveAs(savePath);

            if (File.Exists(savePath))
            {
                string[] readText = File.ReadAllLines(savePath);
                StringBuilder strbuild = new StringBuilder();

                InvoiceBulkDeleteErrorLog(fileName, "Error log: Invoice Bulk Delete");
                InvoiceBulkDeleteErrorLog(fileName, "==============================");

                foreach (string s in readText)
                {
                    try
                    {
                        if (!DeleteInvoiceByInvoiceNo(s))
                        {
                            InvoiceBulkDeleteErrorLog(fileName, s + " - Error");
                        }
                    }
                    catch (Exception ex) { InvoiceBulkDeleteErrorLog(fileName, s + " - " + ex.Message); }
                }

                errorLog = InvoiceBulkDeleteErrorLog(fileName, "=============End=============");

                try
                {
                    CommonClient cClient = new CommonClient();
                    cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Delete,
                        TransactionModules.Invoice,
                        UserSession.Instance.OriginalUserName + " bulk delete invoice list " + fileName);
                }
                catch { }
            }
        }
        catch (Exception ex)
        {
            errorLog = InvoiceBulkDeleteErrorLog(fileName, ex.Message);
        }

        linkBulkDeleteErrorLog.NavigateUrl = errorLog;
        linkBulkDeleteErrorLog.Visible = true;
    }

    private bool DeleteInvoiceByInvoiceNo(string invoiceNo)
    {
        try
        {
            CommonClient commService = new CommonClient();

            bool status = commService.DeleteInvoiceHeaderByInvoiceNo(UserSession.Instance.UserName, invoiceNo);

            return status;
        }
        catch (Exception)
        {
            return false;
        }
    }

    private string InvoiceBulkDeleteErrorLog(string fileName, string msg)
    {
        string folderPath = "~/docs/errorlogs/";
        if (!Directory.Exists(MapPath(folderPath)))
        {
            Directory.CreateDirectory(MapPath(folderPath));
        }

        string filePath = folderPath + fileName;
        string savePath = MapPath(filePath);

        File.AppendAllText(savePath,
                   msg + Environment.NewLine);

        return filePath;
    }



}