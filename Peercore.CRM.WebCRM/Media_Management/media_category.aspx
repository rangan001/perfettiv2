﻿<%@ Page Title="mSales | Media Category" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="media_category.aspx.cs" Inherits="Media_Management_media_category" %>

<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="hdnSelectedCatId" runat="server" ClientIDMode="Static" />

    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="textalignbottom">Category Name 
                                    </td>
                                    <td colspan="4">
                                        <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtname" maxlength="30" />
                                            <span style="color: Red">*</span></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="textalignbottom">Description 
                                    </td>
                                    <td colspan="4">
                                        <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtdescrption" maxlength="200" />
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="textalignbottom">
                                        <button class="k-button" id="buttonSaveAssetType" type="button">Save</button>
                                        <button class="k-button" id="buttonClear">Clear</button>
                                        <asp:HiddenField ID="hdnSelectedId" runat="server" ClientIDMode="Static" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>

        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <a id="id_add_media_category" class="sprite_button" style="float: left; margin-left: 5px; margin-top: 0px;">
                        <span class="plus icon"></span>Add Category</a>
                    &nbsp;
                </div>
            </div>

            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back">
                        </div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">

                <div id="grd_media_category"></div>

            </div>
        </div>
    </div>

    <div id="CategoryWindow" style="display: none">
        <div id="div_Confirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <script type="text/javascript">
        var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {
            LoadCategories();

        });

        //Load popup for Add Categories
        $("#id_add_media_category").click(function () {
            $("#window").css("display", "block");
            ClearControls();
            $("#hdnSelectedId").val('');
            $("#txtname").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Media Category",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtname").focus();
            wnd.center().open();
        });

        //Save button click
        $("#buttonSaveAssetType").click(function () {
            saveCategory();
        });

        $("#buttonClear").click(function () {
            ClearControls();
        });

        function deleteCategory(catId, catName) {
            $("#CategoryWindow").show()
            deleteMediaCategory(catId, catName);
        }

        function deleteMediaCategory(catId, catName) {

            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete '" + catName + "' ?";

            var wnd = $("#CategoryWindow").kendoWindow({
                title: "Delete Media Category",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_Confirm_Message").text(message);
            wnd.center().open();

            $("#yes").click(function () {

                var root = 'Media_Management/media_category.aspx?fm=deleteCategory&type=delete&catId=' + catId;
                delete_media_catelog(root);

            });

            $("#no").click(function () {
                closepopupAsset();
            });

        }

        function delete_media_catelog(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;

            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Media category was successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopupAsset();
                        LoadCategories();
                        hideStatusDiv("MainContent_div_message");
                        $("#div_loader").hide();
                        return;
                    }
                    else {
                        var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        closepopupAsset();
                        hideStatusDiv("MainContent_div_message_popup");
                        return;
                    }
                },
                error: function (msg) {
                    if (msg == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function saveCategory() {

            var selectedCatId = $("#hdnSelectedCatId").val();

            if (($("#txtname").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter Catelog Name.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if ($("#txtname").val()) {

                var Originator = $("#<%= OriginatorString.ClientID %>").val();
                var catName = document.getElementById('txtname').value;
                var description = document.getElementById('txtdescrption').value;

                var param = { "catName": catName, "description": description, "Originator": Originator, "id": selectedCatId };

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/Media/media.asmx/SaveMediaCategory",
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var sucessMsg = GetSuccesfullMessageDiv("Media category was successfully Saved.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopup();
                        LoadCategories();
                        ClearControls();
                        hideStatusDiv("MainContent_div_message");

                    },
                    error: function (response) {
                        if (textStatus == "error") {
                            var msg = msg;
                        }
                    }
                });
            }

        }

        function LoadCategories() {

            var url = "javascript:EditCategory('#=Id#','#=CatName#','#=Description#');";

            var urlCatId = "<a href=\"" + url + "\">#=Id#</a>";
            var urlCatName = "<a href=\"" + url + "\">#=CatName#</a>";
            var urlCatDescription = "<a href=\"" + url + "\">#=Description#</a>";

            $("#grd_media_category").html("");
            $("#grd_media_category").kendoGrid({
                columns: [
                    { template: urlCatId, field: "Id", hidden: true },
                    { template: urlCatName, field: "CatName", title: "Category Name", width: "100px" },
                    { template: urlCatDescription, field: "Description", title: "Description", width: "200px" },
                    
                    {
                        template: '<a href="javascript:deleteCategory(\'#=Id#\', \'#=CatName#\');">Delete</a>',
                        field: "",
                        width: "60px"
                    }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                navigatable: true,
                editable: "popup",
                dataBound: onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    batch: true,
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            fields: {
                                Id: { type: "number" },
                                CatName: { type: "string" },
                                Description: { type: "string" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/Media/media.asmx/GetAllMediaCategories",
                            contentType: "application/json; charset=utf-8",
                            data: { pgindex: take_grid },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    var entityGrid = $("#grd_media_category").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }

            });
        }

        function EditCategory(id, name, description) {

            $("#hdnSelectedCatId").val(id);
            $("#txtname").val(name);
            $("#txtdescrption").val(description);

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Edit Media Category",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            $("#txtname").focus();

            wnd.center().open();
        }

        function closepopupAsset() {
            var wnd = $("#CategoryWindow").kendoWindow({
                title: "Add/Edit Media Category",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#CategoryWindow").css("display", "none");
        }

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Media Category",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        function ClearControls() {
            $("#txtname").val('');
            $("#txtdescrption").val('');
            $("#hdnSelectedCatId").val('');
        }


    </script>

</asp:Content>


