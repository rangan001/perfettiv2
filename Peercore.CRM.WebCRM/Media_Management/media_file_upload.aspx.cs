﻿using CRMServiceReference;
using Microsoft.WindowsAPICodePack.Shell;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web.UI.WebControls;

public partial class Media_Management_media_file_upload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
            {
                if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                {
                    if (Request.QueryString["type"] == "delete")
                    {
                        if (Request.QueryString["fm"].ToLower() == "deletemedia")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteMedia();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                    }
                }
            }

            if (!IsPostBack)
            {
                LoadReps();
                LoadMediaCategoryDropdown();
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Media Upload", "#", "");
        OriginatorString.Value = UserSession.Instance.UserName;
        OriginatorTypeString.Value = UserSession.Instance.OriginatorString;
    }

    //private void ControlsVisibility(string userType)
    //{
    //    if (userType == "ASM")
    //}

    protected void buttonMediaUpload_Click(object sender, EventArgs e)
    {
        CommonUtility commonUtility = new CommonUtility();
        string mediaName = "";
        string fileExtention = "";
        int fileSize = 0;
        string folder = "";
        string thumbPath = "";


        if (FileUploader.HasFile)
        {
            mediaName = Path.GetFileName(FileUploader.FileName);
            fileSize = FileUploader.PostedFile.ContentLength; //1073741824
            fileExtention = Path.GetExtension(mediaName).ToLower();
        }
        else
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please select a Media File.");
            ClearPage();
            return;
        }

        string mediaCategory = ddlCategory.SelectedValue.ToString();
        if (mediaCategory == "0")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please select media category.");
            ClearPage();
            return;
        }
        string userType = ddlRepList.SelectedValue.ToString();
        var validextImg = new[] { ".jpg", ".png", "jpeg", ".gif", ".jfif" };//.png,.jpg,.jpeg,.gif,.jfif
        var validextVedio = new[] { ".mp4", ".avi", ".flv" };//

        string targetPath = Server.MapPath("~/MediaFiles/Thumbnails");
        string Location = Server.MapPath("~/MediaFiles");


        if (validextImg.Contains(fileExtention))
        {
            folder = Location + "\\IMG\\";
        }
        else if (validextVedio.Contains(fileExtention))
        {
            folder = Location + "\\VIDEO\\";
        }
        else
        {
            //Not valied File type
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Not a valied file type ");
            ClearPage();
            return;
        }


        if (!Directory.Exists(folder))
        {
            Directory.CreateDirectory(folder);
        }

        userType = userType + "\\" + mediaCategory;
        if (!(string.IsNullOrEmpty(userType)))
        {
            folder = folder + userType + "\\";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
        }

        string SaveLocation = folder + mediaName;

        try
        {
            FileUploader.PostedFile.SaveAs(SaveLocation);
        }
        catch (Exception)
        {
            div_message.Attributes.Add("style", "display:block;margin-top:60px;");
            div_message.InnerHtml = commonUtility.GetErrorMessage("File Upload Error");
            return;
        }

        /** Thubnail Genarate **/
        if (validextImg.Contains(fileExtention))
        {
            thumbPath = copyImage(SaveLocation);
        }
        else if (validextVedio.Contains(fileExtention))
        {
            //targetPath = ConfigurationManager.AppSettings["ThumbnailPath"];  //@"C:\Thumbnail";
            //WindowsAPICodePack-Shell - Nuget Package manager
            ShellFile shellFile = ShellFile.FromFilePath(SaveLocation);
            Bitmap bm = shellFile.Thumbnail.Bitmap;
            Session["imgName"] = DateTime.Now.ToString();
            bm.Save(targetPath + "\\" + mediaName + ".jpg", ImageFormat.Jpeg);
        }
        else
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Invalied Thumbnail Path");
            return;
        }

        string imagePath = @"..\MediaFiles\";
        imagePath = imagePath + (RelativePath(Location, SaveLocation).ToString());
        string thumbImgPath = @"..\MediaFiles\Thumbnails\" + mediaName + ".jpg";
        //string thumbImgPath = @"..\MediaFiles\Thumbnails\" + Session["imgName"] + ".jpg";


        SaveMediaDetail(mediaName, imagePath, thumbImgPath);
        ClearPage();
    }

    public string RelativePath(string absPath, string relTo)
    {
        string[] absDirs = absPath.Split('\\');
        string[] relDirs = relTo.Split('\\');

        // Get the shortest of the two paths
        int len = absDirs.Length < relDirs.Length ? absDirs.Length :
        relDirs.Length;

        // Use to determine where in the loop we exited
        int lastCommonRoot = -1;
        int index;

        // Find common root
        for (index = 0; index < len; index++)
        {
            if (absDirs[index] == relDirs[index]) lastCommonRoot = index;
            else break;
        }

        // If we didn't find a common prefix then throw
        if (lastCommonRoot == -1)
        {
            throw new ArgumentException("Paths do not have a common base");
        }

        // Build up the relative path
        StringBuilder relativePath = new StringBuilder();

        // Add on the ..
        for (index = lastCommonRoot + 1; index < absDirs.Length; index++)
        {
            if (absDirs[index].Length > 0) relativePath.Append("..\\");
        }

        // Add on the folders
        for (index = lastCommonRoot + 1; index < relDirs.Length - 1; index++)
        {
            relativePath.Append(relDirs[index] + "\\");
        }
        relativePath.Append(relDirs[relDirs.Length - 1]);

        return relativePath.ToString();
    }

    public void SaveMediaDetail(string mediaName, string saveLocation, string targetPath)
    {
        UploadMediaModel mediaModel = new UploadMediaModel();
        CommonUtility commonUtility = new CommonUtility();
        MediaClient mediaClient = new MediaClient();

        mediaModel.RepCode = ddlRepList.SelectedValue.ToString();
        mediaModel.Prefix = ddlCategory.SelectedValue.ToString();
        mediaModel.MediaFileName = mediaName.ToString();
        mediaModel.FileExtention = Path.GetExtension(mediaName).ToLower();
        mediaModel.MediaFilePath = saveLocation.ToString();
        mediaModel.ThumbnailPath = targetPath.ToString();
        mediaModel.Description = txtDescription.Text.ToString();
        mediaModel.CreatedBy = UserSession.Instance.UserName;

        if (ddlRepList.SelectedValue == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please select a Rep");
            return;
        }

        if (mediaName == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please select a Media File");
            return;
        }

        bool iNoRecs = false;
        iNoRecs = mediaClient.SaveUpdateMedia(mediaModel); ;

        if (iNoRecs)
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");

            ClearPage();
        }
        else
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured.");
        }
    }

    protected void ClearPage()
    {
        try
        {
            ddlRepList.SelectedIndex = 0;
            ddlCategory.SelectedIndex = 0;
            txtDescription.Text = "";
            FileUploader.Dispose();
        }
        catch (Exception)
        {

            throw;
        }
    }

    private string copyImage(string SaveLocation)
    {
        string targetPath = Server.MapPath("~/MediaFiles/Thumbnails/");
        string imgName = Path.GetFileName(FileUploader.FileName);
        string imgPath = SaveLocation; //Server.MapPath("~/FILE/") + imgName;
        byte[] bytes = Convert.FromBase64String(GetThumbNail(imgPath).Split(',')[1]);
        System.Drawing.Image image;
        using (MemoryStream ms = new MemoryStream(bytes))
        {
            image = System.Drawing.Image.FromStream(ms);
        }
        Session["imgName"] = DateTime.Now.ToString();

        if (!Directory.Exists(targetPath))
        {
            Directory.CreateDirectory(targetPath);
        }

        image.Save(targetPath + "\\" + imgName + ".jpg");

        return targetPath = String.Format("{0} {1} .jpg", targetPath, imgName); ;
    }

    //Convert Uploaded file to Thumbnail
    private string GetThumbNail(string url)
    {
        string path = url;
        System.Drawing.Image image = System.Drawing.Image.FromFile(path);
        using (System.Drawing.Image thumbnail = image.GetThumbnailImage(40, 50, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero))
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                thumbnail.Save(memoryStream, ImageFormat.Png);
                Byte[] bytes = new Byte[memoryStream.Length];
                memoryStream.Position = 0;
                memoryStream.Read(bytes, 0, (int)bytes.Length);
                string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                return "data:image/png;base64," + base64String;
            }
        }
    }

    private bool ThumbnailCallback()
    {
        return false;
    }

    private void LoadReps()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        args.AdditionalParams = " originator.dept_string = 'DR' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetAllSRByOriginator(args);
        if (repList.Count > 0)
            repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });
        //repList.Insert(1, new OriginatorDTO() { Name = "00396", UserName = "00396" });

        SetReps(repList, "UserName");
    }

    public void SetReps(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        if (UserSession.Instance.OriginatorString == "ADMIN")
        {
            ddlRepList.DataSource = listOriginator;
            ddlRepList.DataTextField = "Name";
            ddlRepList.DataValueField = valueField;
            ddlRepList.DataBind();
        }
        else if (UserSession.Instance.OriginatorString == "SLC")
        {
            ddlRepList.DataSource = listOriginator;
            ddlRepList.DataTextField = "Name";
            ddlRepList.DataValueField = valueField;
            ddlRepList.DataBind();
        }
        else if (UserSession.Instance.OriginatorString == "ASE")
        {
            ddlRepList.DataSource = listOriginator;
            ddlRepList.DataTextField = "Name";
            ddlRepList.DataValueField = valueField;
            ddlRepList.DataBind();
        }
    }

    private string DeleteMedia()
    {
        try
        {
            int Id = 0;
            MediaClient mediaService = new MediaClient();

            if (Request.QueryString["mediaid"] != null && Request.QueryString["mediaid"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["mediaid"].ToString());
            }

            bool status = mediaService.DeleteMediaFiles(Id, UserSession.Instance.UserName);

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private void LoadMediaCategoryDropdown()
    {
        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 50;
        args.OrderBy = "  category_name asc";

        MediaClient mediaClient = new MediaClient();
        List<MediaCategoryModel> data = new List<MediaCategoryModel>();
        data = mediaClient.GetAllCategoriesForddl(args);

        if (data.Count != 0)
        {
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add(new ListItem("- Select -", "0"));
            foreach (MediaCategoryModel item in data)
            {
                ddlCategory.Items.Add(new ListItem(item.CatName.ToString(), item.Prefix));
            }
            ddlCategory.Enabled = true;
        }
        else
        {
            ddlCategory.Items.Add(new ListItem("- Select -", "0"));
            ddlCategory.Enabled = false;
        }
    }


}
