﻿<%@ Page Title="mSales | Trade Discount" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="TradeDiscountViwer.aspx.cs" Inherits="TradeDiscount_TradeDiscountViwer" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        input, textarea, .uneditable-input {
            margin-bottom: 4px !important;
        }

        .toolbar_left {
            padding: 10px;
            /* width: 80%; */
            float: right;
        }

        .formright {
            width: 100%; 
            float: left;
            /* padding-top: 15px; */
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="hdnSelectedTrdId" runat="server" />

    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>

                </div>
            </div>

            <div class="formright" style="margin-bottom: 20px;">
                <fieldset>
                    <table>
                        <tbody>
                            <tr>
                                <td class="textalignbottom">Date 
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtDate"/>
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">DB Code 
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtDBCode"/>
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Voucher Type 
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtVouType"/>
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Voucher No 
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtVouNo"/>
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Sales Value
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtSalesValue" />
                                        <%-- <span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Discount Value
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtDiscountValue" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Trade Discount
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtTradeDiscount" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="4" class="textalignbottom">
                                <button class="k-button" id="buttonSave" type="button">
                                    Save</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </fieldset>
            </div>
        </div>

        <div class="toolbar_container" id="id_div_toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div style="display: none" id="id_div_buttonBar">
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>

                    <a id="modelUploadExcel" class="sprite_button"
                        style="float: right; margin-left: 0px; margin-top: 0px;"
                        href="TradeDiscountExcelUpload.aspx"><span class="plus icon"></span>Upload Excel Data</a>
                    
                    <a id="btnReport" class="sprite_button"
                        style="float: right; margin-left: 5px; margin-right: 5px; margin-top: 0px;"
                        href="TradeDiscountReport.aspx"><span class="plus icon"></span>Print</a>

                </div>
            </div>

            <div class="clearall">
            </div>
            <div id="div_message" runat="server" style="display: none;">
            </div>

            <div id="div_assetss" runat="server" style="display: block">
                <div style="min-width: 200px; overflow: auto">
                    <span>Select Distributor : </span>
                    <asp:DropDownList ID="ddlDistributor" runat="server" ClientIDMode="Static"  onchange="ddlDistributor_SelectedIndexChanged()">
                    </asp:DropDownList>
                    <div style="float: left; width: 100%; overflow: auto; margin: 0px;">
                        <div id="divTradeDiscountGrid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="TDWindow" style="display: none">
        <div id="div_TDWindowConfirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <script type="text/javascript">

        var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {
            LoadTradeDiscountByDistributor();
        });

        function ddlDistributor_SelectedIndexChanged() {
            LoadTradeDiscountByDistributor();
        }

        //Save button click
        $("#buttonSave").click(function () {
            UpdateTradeDiscount();
        });

        $("#buttonCancel").click(function () {
            ClearControls();
            //closepopup();
        });

        function EditTradeDiscount(id, date, dbCode, vouType, vouNo, saleVal, discountVal, tradeDiscountVal) {

            $("#MainContent_hdnSelectedTrdId").val(id);

            $("#txtDate").val(formatDate(date));
            $("#txtDBCode").val(dbCode);
            $("#txtVouType").val(vouType);
            $("#txtVouNo").val(vouNo);
            $("#txtSalesValue").val(Number(saleVal).toLocaleString('en-US', { maximumFractionDigits: 2 }));
            $("#txtDiscountValue").val(Number(discountVal).toLocaleString('en-US', { maximumFractionDigits: 2 }));
            $("#txtTradeDiscount").val(Number(tradeDiscountVal).toLocaleString('en-US', { maximumFractionDigits: 2 }));
            
            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Edit Trade Discount",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            wnd.center().open();
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function LoadTradeDiscountByDistributor() {
            var selectedDBCode = document.getElementById('ddlDistributor').value;

            var url = "javascript:EditTradeDiscount('#=id#','#=date#','#=dbCode#','#=vouType#','#=vouNo#','#=saleVal#','#=discountVal#','#=tradeDiscountVal#');";

            var urltrdid = "<a href=\"" + url + "\">#=id#</a>";
            var urldate = "<a href=\"" + url + "\">#= kendo.toString(date,'dd/MM/yyyy') #</a>";
            var urldbCode = "<a href=\"" + url + "\">#=dbCode#</a>";
            var urlvouType = "<a href=\"" + url + "\">#=vouType#</a>";
            var urlvouNo = "<a href=\"" + url + "\">#=vouNo#</a>";
            var urlsalesVal = "<a href=\"" + url + "\">#if (saleVal) { # #= kendo.toString(saleVal, 'n2') # # }#</a>";
            var urldiscountVal = "<a href=\"" + url + "\">#if (discountVal) { # #= kendo.toString(discountVal, 'n2') # # }#</a>";
            var urltradeDiscountVal = "<a href=\"" + url + "\">#if (tradeDiscountVal) { # #= kendo.toString(tradeDiscountVal, 'n2') # # }#</a>";

            $("#divTradeDiscountGrid").html("");
            $("#divTradeDiscountGrid").kendoGrid({
                columns: [
                    { field: "id", width: "80px", hidden: true },
                    { template: urldate, field: "date", title: "Date ", width: "80px" },
                    { template: urldbCode, field: "dbCode", title: "DB Code", width: "100px", filterable: false, hidden: true },
                    { template: urlvouType, field: "vouType", title: "Vou. Type", width: "200px", filterable: false },
                    { template: urlvouNo, field: "vouNo", title: "Vou. No", width: "80px" },
                    { template: urlsalesVal, field: "saleVal", title: "Sales Value", width: "100px", filterable: false },
                    { template: urldiscountVal, field: "discountVal", title: "Discount Value", width: "100px", filterable: false, format: "{0:c2}" },
                    { template: urltradeDiscountVal, field: "tradeDiscountVal", title: "Trade Discount", width: "100px", filterable: false, format: "{0:c2}" },
                    {
                        template: '<a href="javascript:deleteTradeDiscountPopup(\'#=id#\');">Delete</a>',
                        field: "",
                        width: "60px"
                    }
                ],
                editable: false,
                pageable: true,
                sortable: false,
                filterable: true,
                navigatable: true,
                dataBound: onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    batch: true,
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            fields: {
                                id: { type: "number" },
                                date: { type: "Date" },
                                dbCode: { type: "string" },
                                vouType: { type: "string" },
                                vouNo: { type: "string" },
                                saleVal: { type: "number" },
                                discountVal: { type: "number" },
                                tradeDiscountVal: { type: "number" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/distributor_Service.asmx/GetAllTradeDiscountByDistributor",
                            contentType: "application/json; charset=utf-8",
                            data: { pgindex: take_grid, dbCode: selectedDBCode },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    var entityGrid = $("#divTradeDiscountGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            });
        }

        function UpdateTradeDiscount() {
            var selectedId = $("#MainContent_hdnSelectedTrdId").val();

            if (($("#txtDBCode").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("DB Code cannot be blank.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            if ($("#txtDBCode").val() != '') {
                var txtDate = $("#txtDate").val();
                var txtDBCode = $("#txtDBCode").val();
                var txtVouType = $("#txtVouType").val();
                var txtVouNo = $("#txtVouNo").val();
                var txtSalesValue = $("#txtSalesValue").val();
                var txtDiscountValue = $("#txtDiscountValue").val();
                var txtTradeDiscount = $("#txtTradeDiscount").val();

                var param = {
                    "id": selectedId,
                    "date": txtDate,
                    "dbCode": txtDBCode,
                    "vouType": txtVouType,
                    "vouNo": txtVouNo,
                    "saleVal": txtSalesValue,
                    "discountVal": txtDiscountValue,
                    "tradeDiscountVal": txtTradeDiscount
                };

                console.log(param);

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/lead_customer/distributor_Service.asmx/UpdateTradeDiscount",
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var sucessMsg = GetSuccesfullMessageDiv("successfully Updated.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopup();
                        LoadTradeDiscountByDistributor();
                        ClearControls();
                        hideStatusDiv("MainContent_div_message");

                    },
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                            $("#div_loader").hide();
                        }
                    }
                });
            }
        }

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Edit Asset",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        function deleteTradeDiscountPopup(trdid) {
            $("#TDWindow").show();
            deleteTradeDiscount(trdid);
        }

        function deleteTradeDiscount(trdid) {
            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete selected?";

            var wnd = $("#TDWindow").kendoWindow({
                title: "Delete Trade Discount",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_TDWindowConfirm_Message").text(message);
            wnd.center().open();

            $("#yes").click(function () {
                var root = 'TradeDiscount/TradeDiscountViwer.aspx?fm=deletetd&type=delete&trdid=' + trdid;
                deleteTDiscount(root);
            });

            $("#no").click(function () {
                closepopupTradeDiscount();
            });
        }

        function closepopupTradeDiscount() {
            var wnd = $("#TDWindow").kendoWindow({
                title: "Delete Trade Discount",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#TDWindow").css("display", "none");
        }

        function deleteTDiscount(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;

            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    $('#MainContent_div_message').css('display', 'none');
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Selected was successfully deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        LoadTradeDiscountByDistributor();
                    }
                    else {
                        var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(existsMsg);
                        $("#div_loader").hide();
                    }

                    $("#div_loader").hide();
                    hideStatusDiv("MainContent_div_message");
                    closepopupTradeDiscount();
                    return;
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function ClearControls() {
            $("#hdnSelectedAssetId").val("");

            $("#txtAssetCode").val("");
            $("#txtModel").val("");
            $("#txtimei1").val("");
            $("#txtimei2").val("");
            $("#txtSerial").val("");
            $("#txtCompAssetCode").val("");
            $("#txtPrinterPouch").val("");
            $("#txtMonitorModel").val("");
            $("#txtMonitorSerial").val("");
            $("#txtSIM").val("");
            $("#txtSupplier").val("");
            $("#txtInvNo").val("");
            $("#txtValue").val("");
            $("#txtPurchDate").val("");
            $("#txtWarranty").val("");

            $("#txtdescrption").val("");
        }

    </script>
</asp:Content>

