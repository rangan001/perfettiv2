﻿using CRMServiceReference;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TradeDiscount_TradeDiscountViwer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
            {
                if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                {
                    if (Request.QueryString["type"] == "delete")
                    {
                        if (Request.QueryString["fm"].ToLower() == "deletetd")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteAsset();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                    }
                }
            }

            if (!IsPostBack)
            {
                LoadDistributors();
            }

            OriginatorString.Value = UserSession.Instance.UserName;

            Master.SetBreadCrumb("Distributor Trade Discount ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'DIST' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();

        distList = originatorClient.GetDistributersByASMOriginator(args);

        if (distList.Count != 0)
        {
            ddlDistributor.Items.Clear();
            foreach (OriginatorDTO item in distList)
            {
                ddlDistributor.Items.Add(new ListItem(item.Name.ToString(), item.UserName));
            }

            ddlDistributor.Enabled = true;
        }
        else
        {
            ddlDistributor.Items.Add(new ListItem("- Select -", "0"));
            ddlDistributor.Enabled = false;
        }
    }

    private string DeleteAsset()
    {
        try
        {
            int Id = 0;
            DistributorClient dbService = new DistributorClient();

            if (Request.QueryString["trdid"] != null && Request.QueryString["trdid"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["trdid"].ToString());
            }

            bool status = dbService.DeleteTradeDiscount(Id, UserSession.Instance.UserName);
            if (status)
                return "true";

            return "false";
        }
        catch (Exception)
        {
            return "false";
        }
    }
}