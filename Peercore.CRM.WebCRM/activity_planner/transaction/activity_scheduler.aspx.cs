﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using Telerik.Web.UI;
using System.Drawing;
using System.Text;
using System.Web.Services;
using System.Data;
using context = System.Web.HttpContext;
using System.IO;

public partial class activity_planner_transaction_activity_scheduler : PageBase
{
    private const string LSTUNSRNT_ACTIVITY = "LSTUNSRNT_ACTIVITY";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        SendLogToText("Start PageLoad");

        radActivityPlanner.FormCreated += new SchedulerFormCreatedEventHandler(radActivityPlanner_FormCreated);
        buttonbarNavi.onButtonPrevious = new usercontrols_buttonbar_navigation.ButtonPrevious(buttonPrevious_Click);
        //   buttonbar.onButtonItinerary = new usercontrols_buttonbar.ButtonItinerary(buttonItinerary_Click);
        buttonbarNavi.VisibleNext(false);
        //  buttonbar.VisibleItinerary(true);
        // radActivityPlanner_Form_ .InnerHtml = SetAutocomplete_Temp();

        ViewState["sChildRep"] = Request.QueryString["id"];

        if (!IsPostBack)
        {
            // radActivityPlanner_Form_ .InnerHtml = SetAutocomplete_Temp();

            ViewState["sChildRep"] = Request.QueryString["id"];
            BindLeadCustomers();
            SendLogToText("Bind lead customer success");

            if (CurrentUserSettings != null)
            {
                CurrentUserSettings.RepCode = Request.QueryString["id"];
            }

            ActivityClient activityClient = new ActivityClient();
            List<ActivityDTO> activity = activityClient.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, true);
            SendLogToText("Load activity types success");
            //int key = 0;
            radActivityPlanner.ResourceStyles.Clear();
            foreach (ActivityDTO item in activity)
            {
                ResourceStyleMapping resourceStyleMapping = new ResourceStyleMapping();
                resourceStyleMapping.Type = "ActivityTypes";
                resourceStyleMapping.Text = item.Code;
                resourceStyleMapping.Key = item.Code;
                resourceStyleMapping.BackColor = ColorTranslator.FromHtml(item.ActivityTypeColour);
                radActivityPlanner.ResourceStyles.Add(resourceStyleMapping);
                // key++;
            }

            LoadOptions();
            LoadUI();
            AddCategories(activity);
            //LoadSchedulerData(DropDownListRep.SelectedValue);

            //if (Request.QueryString["id"] != null)
            //{
            //    GetScheduledActivities(Request.QueryString["id"]);
            //}
            //else
            //{
            //    GetScheduledActivities();
            //}

            ViewState["isControlLoaded"] = true;

            /*if (Request.QueryString["cht"] != null)
            {
                if (Request.QueryString["cht"] != string.Empty)
                {
                    //Master.SetCommonHeader(Request.QueryString["cht"].ToString());
                }
            }
            else
            {
                //Master.SetCommonHeader("home");
            }*/

            if (Request.QueryString["fm"] == "home")
            {
                if (!string.IsNullOrEmpty(Request.QueryString["sdate"]))
                {
                    radActivityPlanner.SelectedView = SchedulerViewType.DayView;

                    radActivityPlanner.SelectedDate = DateTime.Parse(Request.QueryString["sdate"]);
                }
            }


            radActivityPlanner.TimeZoneID = TimeZoneInfo.Local.Id;
            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Route Planner ", ConfigUtil.ApplicationPath + "/activity_planner/transaction/activity_scheduler.aspx", "");
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //radActivityPlanner.VisibleRangeStart=
        //  radActivityPlanner.TimeZoneID = TimeZoneInfo.Local.Id;
        radActivityPlanner.SelectedDate = DateTime.Now;
    }

    private void BindLeadCustomers()
    {
        if (CurrentUserSettings == null)
            CurrentUserSettings = new ArgsDTO();
        CurrentUserSettings.ChildOriginators = UserSession.Instance.ChildOriginators;
        CurrentUserSettings.Originator = UserSession.Instance.UserName;
        CurrentUserSettings.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        CurrentUserSettings.RepType = UserSession.Instance.RepType;

        CurrentUserSettings.LeadStage = "";
        CurrentUserSettings.AdditionalParams = "0 = 0";
        CurrentUserSettings.ActiveInactiveChecked = true;

        CurrentUserSettings.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;
    }

    private void LoadOptions()
    {
        CommonClient commonClient = new CommonClient();
        List<LookupTableDTO> lstLookupTableDTO = new List<LookupTableDTO>();
        List<KeyValuePair<int, string>> lstPeriod = new List<KeyValuePair<int, string>>();
        ArgsDTO ArgsDTO = new ArgsDTO();

        try
        {
            radStatus.Items.Clear();
            ArgsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            lstLookupTableDTO = commonClient.GetLookupTables("ACST", ArgsDTO);

            radStatus.Items.Add(new ListItem("-All-", "-All-"));
            foreach (LookupTableDTO obj in lstLookupTableDTO)
            {
                radStatus.Items.Add(new ListItem(obj.TableDescription, obj.TableCode));
            }

            radStatus.SelectedIndex = 0;

            radTimeRange.Items.Clear();
            lstPeriod = GetTimePeriods();
            foreach (KeyValuePair<int, string> ValuePair in lstPeriod)
            {
                radTimeRange.Items.Add(new ListItem(ValuePair.Value.ToString(), ValuePair.Key.ToString()));
            }
            radTimeRange.Items[0].Selected = true;

            ArgsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;

            lstLookupTableDTO = commonClient.GetRepTerritories(ArgsDTO);
            lstLookupTableDTO.Insert(0, new LookupTableDTO() { TableCode = "- All -", TableDescription = "- All -" });

            radRepTerritory.DataSource = lstLookupTableDTO;
            radRepTerritory.DataTextField = "TableCode";
            radRepTerritory.DataValueField = "TableCode";
            radRepTerritory.DataBind();
        }
        catch (Exception)
        {
            throw;
        }
    }

    private void AddCategories(List<ActivityDTO> activityTypeList)
    {
        ActivityClient ActivityClient = new ActivityClient();
        StringBuilder txt = new StringBuilder();
        int iIndex = 1;
        int iNoOfCategories = 0;

        try
        {
            //activityTypeList = ActivityClient.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentIdCookers, true);
            ViewState["activityColors"] = activityTypeList;

            if (activityTypeList != null)
            {
                iNoOfCategories = activityTypeList.Count;

                txt.Append("<div style=\"min-width:200px; overflow:auto\">");
                txt.Append("<table border=\"0\" bgcolor=\"#3a3b3d\" width=\"1200px\">");
                txt.Append("<tr>");

                foreach (ActivityDTO ActivityDTO in activityTypeList)
                {
                    //if (iIndex > (iNoOfCategories / 2))
                    //{

                    //}
                    //else
                    //{
                    Color col = ColorTranslator.FromHtml(ActivityDTO.ActivityTypeColour);
                    string color = Color.FromArgb(col.A, col.R, col.G, col.B).Name;
                    color = color.Substring(2, 6);
                    txt.Append("<td valign=\"top\"><div style=\"background-color:#" + color + "; width:15px; height:15px; border:1px solid #999\"></div></td><td style=\"color:#FFF;\" valign=\"top\">&nbsp;" + ActivityDTO.Description + "&nbsp;</td>");
                    //}
                    //iIndex++;
                }

                txt.Append("</tr>");
                txt.Append("</table>");
                txt.Append("</div>");
                div_activity.InnerHtml = txt.ToString();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    private List<KeyValuePair<int, string>> GetTimePeriods()
    {
        List<KeyValuePair<int, string>> timeLines = new List<KeyValuePair<int, string>>();
        timeLines.Add(new KeyValuePair<int, string>(0, "- All -"));
        timeLines.Add(new KeyValuePair<int, string>(2, "02 Months"));
        timeLines.Add(new KeyValuePair<int, string>(6, "06 Months"));
        timeLines.Add(new KeyValuePair<int, string>(12, "01 Year"));

        return timeLines;
    }

    protected void radTimeRange_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SelectedView = HiddenFieldSelectedView.Value;
        if (SelectedView == "DayView")
            radActivityPlanner.SelectedView = SchedulerViewType.DayView;
        else if (SelectedView == "WeekView")
            radActivityPlanner.SelectedView = SchedulerViewType.WeekView;
        else if (SelectedView == "MonthView")
            radActivityPlanner.SelectedView = SchedulerViewType.MonthView;
        else if (SelectedView == "TimelineView")
            radActivityPlanner.SelectedView = SchedulerViewType.TimelineView;

        DateTime a = radActivityPlanner.VisibleRangeStart;
        DateTime b = radActivityPlanner.VisibleRangeEnd;

        //  radActivityPlanner.DataStartField =
        if (Convert.ToBoolean(ViewState["isControlLoaded"]).Equals(true))
        {
            if (!radTimeRange.SelectedValue.Equals("0"))
            {
                DateTime date = FloorDate(radTimeRange.SelectedValue);
                string tempFrom = DateTime.Parse(date.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");
                CurrentUserSettings.Floor = tempFrom;
            }
            else
            {
                CurrentUserSettings.Floor = "";
            }
            radActivityPlanner.Rebind();
            //GetScheduledActivities();
        }
    }

    private DateTime FloorDate(string selectedDate)
    {
        DateTime Date = new DateTime();

        if (selectedDate == "2")
        {
            Date = DateTime.Now.AddMonths(-2);
        }
        else if (selectedDate == "6")
        {
            Date = DateTime.Now.AddMonths(-6);
        }
        else if (selectedDate == "12")
        {
            Date = DateTime.Now.AddMonths(-12);
        }
        else if (selectedDate == "0")
        {
            Date = DateTime.Now.AddMonths(-0);
        }

        return Date;
    }

    protected void radStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            FilterbyStatus();
        }
        catch (Exception)
        {
            throw;
        }
    }

    private void FilterbyStatus()
    {
        List<CustomerActivitiesDTO> cActivity = new List<CustomerActivitiesDTO>();
        cActivity = ViewState["cActivity"] as List<CustomerActivitiesDTO>;
        try
        {
            string status = radStatus.SelectedValue.ToString().Trim();
            if (status.Trim() == "")
                return;
            List<CustomerActivitiesDTO> activitiesList = new List<CustomerActivitiesDTO>();
            if (status.Trim().Equals("-All-"))
                status = "";
            CurrentUserSettings.Status = status;
            radActivityPlanner.Rebind();
            //if (status.Trim().Equals("-All-") == false)
            //{
            //    activitiesList = cActivity.FindAll(x => x.Status == status);
            //    LoadActivitiesToShedular(activitiesList);
            //}
            //else
            //    LoadActivitiesToShedular(cActivity);
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void radRepTerritory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (radRepTerritory.SelectedValue == null)
                radRepTerritory.SelectedValue = "- All -";

            GetScheduledActivities(string.Empty, !radRepTerritory.SelectedValue.ToString().Equals("- All -") ?
                radRepTerritory.SelectedValue.ToString() :
                string.Empty);
        }
        catch (Exception ex)
        {
        }
    }

    public void GetScheduledActivities(string sChildRep = "", string repTerritory = "")
    {
        try
        {
            CurrentUserSettings.ActivityStatus = repTerritory;
            radActivityPlanner.Rebind();
        }
        catch (Exception oException)
        {
        }
    }

    protected void radActivityPlanner_AppointmentUpdate(object sender, AppointmentUpdateEventArgs e)
    {
        //brActivity oActivity = new brActivity();
        ActivityDTO activity = new ActivityDTO();
        List<CustomerActivitiesDTO> cActivity = new List<CustomerActivitiesDTO>();
        StringBuilder txt = new StringBuilder();
        int iNoRecs = 0;

        Telerik.Web.UI.Appointment editedAppt = e.ModifiedAppointment as Telerik.Web.UI.Appointment;
        Telerik.Web.UI.Appointment editedApptcolor = e.Appointment as Telerik.Web.UI.Appointment;

        DateTime Start = Convert.ToDateTime(editedAppt.Start);
        string sStart = Start.ToString("dd-MMM-yyyy HH:mm:ss");
        DateTime StartDate = Convert.ToDateTime(sStart);

        DateTime End = Convert.ToDateTime(editedAppt.End);
        string sEnd = End.ToString("dd-MMM-yyyy HH:mm:ss");
        DateTime EndDate = Convert.ToDateTime(sEnd);

        List<ActivityDTO> activityTypeList = ViewState["activityColors"] as List<ActivityDTO>;
        ActivityDTO cctivity = activityTypeList.Find(c => c.ActivityTypeColour == ("#" + editedApptcolor.BackColor.Name).ToUpper());
        cActivity = ViewState["cActivity"] as List<CustomerActivitiesDTO>;

        activity.ActivityID = Convert.ToInt32(editedAppt.ID);
        activity.Subject = editedAppt.Subject;
        activity.Comments = editedAppt.Description;
        activity.ActivityType = cctivity.Code;
        activity.LastModifiedBy = UserSession.Instance.UserName;

        //brAppointment oAppointment = new brAppointment();
        //CRMAppointment appointment = new CRMAppointment();
        CustomerActivitiesDTO customerActivities = cActivity.FirstOrDefault(x => x.ActivityID == Convert.ToInt32(editedAppt.ID));
        if (customerActivities != null)
        {
            //appointment.AppointmentID = customerActivities.AppointmentId;
        }

        //iNoRecs = oActivity.ChangeActivityDate(activity, StartDate, EndDate, DateTime.Now);
        if (iNoRecs > 0 && customerActivities != null)
            //oAppointment.Update(appointment, StartDate, EndDate, true);

            if (iNoRecs > 0)
            {
                Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=success");
            }
    }

    public void radActivityPlanner_FormCreated(object sender, SchedulerFormCreatedEventArgs e)
    {
        RadDatePicker startDatec = e.Container.FindControl("StartInput") as RadDatePicker;
        if (startDatec != null)
        {
            // advanced form is shown        
            startDatec.ClientEvents.OnDateSelected = "changeEndDate";
        }

        ActivityClient oBrActivityType = new ActivityClient();
        List<CustomerActivitiesDTO> cActivity = new List<CustomerActivitiesDTO>();

        List<ActivityDTO> lstActivityType = oBrActivityType.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, true);
        StringBuilder txt = new StringBuilder();

        txt.Append("<table id=\"color_tble\">");

        foreach (ActivityDTO obj in lstActivityType)
        {
            txt.Append("<tr>");
            txt.Append("<td>");
            txt.Append("<input type=\"radio\" name=\"group1\" value=" + obj.Code + " id=\"a\" onclick=\"onclickActivity()\" style=\"float:left;\">");
            Color activityColor = System.Drawing.ColorTranslator.FromHtml(obj.ActivityTypeColour);
            String rtn = "#" + activityColor.R.ToString("X2") + activityColor.G.ToString("X2") + activityColor.B.ToString("X2");
            txt.Append("<div style=\"background-color:" + rtn + ";height:20px;width:20px;float:right;\" runat=\"server\"> </div>");
            txt.Append("<label style=\"width:200px;\">" + obj.Description + "</label>");
            txt.Append("</td>");
            txt.Append("</tr>");
        }
        txt.Append("</table>");

        HtmlGenericControl colorDiv = (HtmlGenericControl)e.Container.FindControl("div_color");
        colorDiv.InnerHtml = txt.ToString();

        if (e.Container.Mode == SchedulerFormMode.Insert || e.Container.Mode == SchedulerFormMode.AdvancedInsert)
        {
            RadDateTimePicker startDate = (RadDateTimePicker)e.Container.FindControl("StartInput");
            startDate.SelectedDate = e.Appointment.Start;
            RadDateTimePicker endDate = (RadDateTimePicker)e.Container.FindControl("EndInput");
            endDate.SelectedDate = e.Appointment.End;
        }
        if (e.Container.Mode == SchedulerFormMode.Edit || e.Container.Mode == SchedulerFormMode.AdvancedEdit)
        {
            Telerik.Web.UI.Appointment appoinment = e.Appointment as Telerik.Web.UI.Appointment;
            ActivityDTO activityDescription = lstActivityType.Find(c => c.ActivityTypeColour == ("#" + appoinment.BackColor.Name).ToUpper());

            cActivity = ViewState["cActivity"] as List<CustomerActivitiesDTO>;
            CustomerActivitiesDTO customerAct = cActivity.Find(x => x.ActivityID == Convert.ToInt32(appoinment.ID));

            if (customerAct != null)
            {
                HiddenField appoIdCRM = (HiddenField)e.Container.FindControl("txt_appoIdCRM");
                appoIdCRM.Value = customerAct.AppointmentId.ToString();

                Label activityHedding = (Label)e.Container.FindControl("lbl_hedding");
                activityHedding.Text = appoinment.Subject;

                Label activitydescription = (Label)e.Container.FindControl("lbl_description");
                activitydescription.Text = activityDescription.Description;

                HiddenField appoId = (HiddenField)e.Container.FindControl("txt_appoId");
                appoId.Value = appoinment.ID.ToString();

                TextBox subject = (TextBox)e.Container.FindControl("txtSubject");
                subject.Text = appoinment.Subject;

                TextBox description = (TextBox)e.Container.FindControl("txtDescription");
                description.Text = appoinment.Description;

                RadDateTimePicker startDate = (RadDateTimePicker)e.Container.FindControl("StartInput");
                startDate.SelectedDate = appoinment.Start;

                RadDateTimePicker endDate = (RadDateTimePicker)e.Container.FindControl("EndInput");
                endDate.SelectedDate = appoinment.End;

                HtmlGenericControl colorType = (HtmlGenericControl)e.Container.FindControl("div_color_type");
                Color activityColor = System.Drawing.ColorTranslator.FromHtml("#" + appoinment.BackColor.Name);
                String rtn = "#" + activityColor.R.ToString("X2") + activityColor.G.ToString("X2") + activityColor.B.ToString("X2");
                colorType.Style["background-color"] = rtn;
            }
        }
    }

    protected void btnSendToCalender_Click(object sender, EventArgs e)
    {
        try
        {
            string SelectedView = HiddenFieldSelectedView.Value;
            if (SelectedView == "DayView")
                radActivityPlanner.SelectedView = SchedulerViewType.DayView;
            else if (SelectedView == "WeekView")
                radActivityPlanner.SelectedView = SchedulerViewType.WeekView;
            else if (SelectedView == "MonthView")
                radActivityPlanner.SelectedView = SchedulerViewType.MonthView;
            else if (SelectedView == "TimelineView")
                radActivityPlanner.SelectedView = SchedulerViewType.TimelineView;

            if (ViewState["sChildRep"] != null)
                SendAllToCalendar(ViewState["sChildRep"].ToString());
            else
                SendAllToCalendar("");

        }
        catch (Exception)
        {

        }
    }

    //public void SendAllToCalendar()
    //{
    //    string sChildRep = Convert.ToString(ViewState["sChildRep"]);
    //    CommonUtility commonUtilityPara = new CommonUtility();
    //    List<CustomerActivitiesDTO> activityList = null;
    //    List<CustomerActivitiesDTO> unsentActivityList = null;
    //    List<CustomerActivitiesDTO> filteredactivityList = null;
    //    ArgsDTO args = new ArgsDTO();
    //    ActivityClient bractivity = new ActivityClient();
    //    StringBuilder txt = new StringBuilder();

    //    DateTime startDate = radActivityPlanner.VisibleRangeStart;
    //    DateTime endDate = radActivityPlanner.VisibleRangeEnd;

    //    int sd = startDate.Day;
    //    int sm = startDate.Month;
    //    int sy = startDate.Year;

    //    DateTime start = Convert.ToDateTime(sd + "/" + sm + "/" + sy + " " + "07:00:00");

    //    int ed = endDate.Day;
    //    int em = endDate.Month;
    //    int ey = endDate.Year;

    //    DateTime end = Convert.ToDateTime(ed + "/" + em + "/" + ey + " " + "00:00:00").AddDays(-1);

    //    DateTime date = FloorDate(radTimeRange.SelectedValue);
    //    string tempFrom = DateTime.Parse(date.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");

    //    if (sChildRep != "")
    //    {
    //        args.Originator = sChildRep;
    //    }
    //    else
    //    {
    //        // args.Originator = UserSession.Instance.UserName;
    //        args.Originator = ((string.IsNullOrEmpty(UserSession.Instance.FilterByUserName)) ? UserSession.Instance.UserName : UserSession.Instance.FilterByUserName);
    //    }

    //    activityList = bractivity.GetScheduledActivities(UserSession.Instance.DefaultDepartmentIdCookers, true, args.Originator, "Planner",
    //           tempFrom, "", Convert.ToString(start), Convert.ToString(end), "");

    //    string status = radStatus.SelectedValue.ToString().Trim();

    //    if (status.Trim() != "")
    //    {
    //        if (status.Trim().Equals("-All-") == false)
    //        {
    //            filteredactivityList = activityList.FindAll(x => x.Status == status);
    //            unsentActivityList = filteredactivityList.FindAll(x => x.AppointmentId == 0);
    //            activityList = filteredactivityList;
    //        }
    //        else
    //        {
    //            unsentActivityList = activityList.FindAll(x => x.AppointmentId == 0);
    //            Session[LSTUNSRNT_ACTIVITY] = unsentActivityList;
    //        }
    //    }

    //    if (activityList.Count == 0)
    //    {
    //        Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=notsent");
    //    }
    //    else if (unsentActivityList.Count == 0)
    //    {
    //        Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=alreadysent");
    //    }
    //    else if (unsentActivityList.Count > 0)
    //    {
    //        string str = "Do you want to send " + unsentActivityList.Count + " Activities to Calendar ? Total No. of Activities in Current View -" + activityList.Count + " Activities already sent to Calendar - " + (activityList.Count - unsentActivityList.Count);
    //        this.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "ConfirmApproval('" + str + "');", true);
    //    }
    //}

    public void SendAllToCalendar(string sChildRep = "")
    {
        ActivityClient oActivity = new ActivityClient();

        DateTime startDate = radActivityPlanner.VisibleRangeStart.AddDays(1);
        DateTime endDate = radActivityPlanner.VisibleRangeEnd.AddDays(0);

        List<CustomerActivitiesDTO> activityList = null;
        List<CustomerActivitiesDTO> unsentActivityList = null;
        List<CustomerActivitiesDTO> filteredactivityList = null;

        ArgsDTO args = new ArgsDTO();
        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.Originator = UserSession.Instance.UserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.SToday = string.IsNullOrEmpty(args.SToday) ? "" : args.SToday;
            args.SStartDate = startDate.ToString("yyyy/MM/dd");
            args.SEndDate = endDate.ToString("yyyy/MM/dd");
            args.SSource = "Planner";
            args.Floor = string.IsNullOrEmpty(args.Floor) ? "" : args.Floor;
            args.Status = string.IsNullOrEmpty(args.Status) ? "" : args.Status;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        }

        if (sChildRep != "")
        {
            args.Originator = sChildRep;
            activityList = oActivity.GetAllActivities(args, "");
        }
        else
            activityList = oActivity.GetAllActivities(args, "");


        string status = radStatus.SelectedValue.ToString().Trim();
        if (status.Trim() != "")
        {
            if (status.Trim().Equals("-All-") == false)
            {
                //var varfilteredactivityList = from al in activityList where al.Status==status && al.StartDate >= startDate && al.EndDate <= endDate && al.AppointmentId == 0 select al;
                filteredactivityList = activityList.FindAll(x => x.Status == status);
                unsentActivityList = filteredactivityList.FindAll(x => x.AppointmentId == 0);
                activityList = filteredactivityList;
                //unsentActivityList = varfilteredactivityList.ToList();
            }
            else
                unsentActivityList = activityList.FindAll(x => x.AppointmentId == 0);

            Session[LSTUNSRNT_ACTIVITY] = unsentActivityList;
        }

        CommonUtility commonUtilityPara = new CommonUtility();
        if (activityList.Count == 0)
        {
            string str = commonUtilityPara.GetInfoMessage("There are no activities to send.");
            this.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "SetMessage('" + str + "');", true);
            //Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=notsent");
        }
        else if (unsentActivityList.Count == 0)
        {
            string str = commonUtilityPara.GetInfoMessage("All activities have already been sent.");
            this.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "SetMessage('" + str + "');", true);
            // Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=alreadysent");
        }
        else if (unsentActivityList.Count > 0)
        {
            string str = "Do you want to send " + unsentActivityList.Count + " Activities to Calendar ? Total No. of Activities in Current View -" + activityList.Count + " Activities already sent to Calendar - " + (activityList.Count - unsentActivityList.Count);
            this.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "ConfirmApproval('" + str + "');", true);
        }

        //if (activityList.Count == 0)
        //{
        //    GlobalValues.GetInstance().ShowMessage("There are no activities to send.", GlobalValues.MessageImageType.Information);
        //    return;
        //}
        //else if (unsentActivityList.Count == 0)
        //{
        //    GlobalValues.GetInstance().ShowMessage("All activities have already been sent.", GlobalValues.MessageImageType.Information);
        //    return;
        //}

        //if (clsGlobal.GetInstance().ConfirmMessageBox("Do you want to send " + unsentActivityList.Count + " Activities to Calendar ?\nTotal No. of Activities in Current View - " + activityList.Count + "\nActivities already sent to Calendar - " + (activityList.Count - unsentActivityList.Count)) == true)
        //{
        //    SendToAppointments(unsentActivityList);
        //}
        SetCurrentDate();
    }

    protected void radActivityPlanner_AppointmentContextMenuItemClicked(object sender, AppointmentContextMenuItemClickedEventArgs e)
    {
        string headerText = e.MenuItem.Value;

        Telerik.Web.UI.Appointment objAppointment = e.Appointment as Telerik.Web.UI.Appointment;

        DateTime Start = (DateTime)objAppointment.Start;
        string sStart = Start.ToString("dd-MMM-yyyy HH:mm:ss");
        DateTime StartDate = Convert.ToDateTime(sStart);

        DateTime End = (DateTime)objAppointment.End;
        string sEnd = End.ToString("dd-MMM-yyyy HH:mm:ss");
        DateTime EndDate = Convert.ToDateTime(sEnd);

        switch (headerText)
        {
            //case "OpenContact":
            //    OpenContactEntry(e.Appointment);
            //    break;
            //case "SendToCalendar":
            //    SendToAppointments(e.Appointment, StartDate, EndDate);
            //    break;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string SendToAppointments(int id, DateTime startTime, DateTime EndTime)
    {
        ActivityClient oActivity = new ActivityClient();
        StringBuilder txt = new StringBuilder();
        CommonUtility commonUtilityPara = new CommonUtility();

        try
        {
            //if (sendtoAppointments != null)
            //{
            CustomerActivitiesDTO activity = oActivity.GetActivityAppoitmentId(Convert.ToInt32(id));
            if (activity.AppointmentId > 0)
            {
                string str = commonUtilityPara.GetSucessfullMessage("Activity has already been sent to calendar.");
                return str;
                //Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=alreadysentactivity");
            }
            bool iNoRecs = false;

            ActivityDTO existingActivity = oActivity.GetActivity(Convert.ToInt32(id));
            //List<CustomerActivitiesDTO> cActivity = ViewState["cActivity"] as List<CustomerActivitiesDTO>;

            //CustomerActivitiesDTO customerActivitiesType = cActivity.Find(c => c.TypeColour == "#" + Convert.ToString(sendtoAppointments.BackColor.Name).ToUpper());

            // Create a new appointment for this ACTIVITY
            AppointmentClient oAppointment = new AppointmentClient();
            AppointmentDTO appointment = new AppointmentDTO();
            appointment.Subject = existingActivity.Subject;
            appointment.Body = existingActivity.Description;
            appointment.Start = startTime;
            appointment.End = EndTime;
            appointment.Category = existingActivity.ActivityType;
            appointment.CreatedBy = UserSession.Instance.UserName;
            int ff = 0;
            iNoRecs = oAppointment.AppointmentInsert(appointment, true, Convert.ToInt32(id), out ff);

            if (iNoRecs)
            {
                string str = commonUtilityPara.GetSucessfullMessage("Sent to Calendar");
                return str;
                //this.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "SetMessage('" + str + "');", true);
                //Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=sentcalander");
            }
            //}
        }
        catch (Exception)
        {

            throw;
        }
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string SendToAppointments()
    {
        StringBuilder txt = new StringBuilder();
        List<CustomerActivitiesDTO> customerActivityList = HttpContext.Current.Session[LSTUNSRNT_ACTIVITY] as List<CustomerActivitiesDTO>;
        CommonUtility commonUtilityPara = new CommonUtility();

        try
        {
            bool iNoRecs = false;
            AppointmentClient oAppointment = new AppointmentClient();
            iNoRecs = oAppointment.InsertAppointmentList(customerActivityList, UserSession.Instance.UserName);

            if (iNoRecs)
            {
                txt.Append(commonUtilityPara.GetSucessfullMessage("Information. All activities Sent to Calendar."));
            }
        }
        catch (Exception)
        {
            throw;
        }
        return txt.ToString();
    }

    private void SetCurrentDate()
    {
        //if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        //{
        //    ArgsDTO args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;

        //    //args.SStartDate = startDate.ToString("yyyy/MM/dd");
        //    //args.SEndDate = endDate.ToString("yyyy/MM/dd");
        //    if (!string.IsNullOrEmpty(args.SEndDate))
        //    {
        //        radActivityPlanner.SelectedDate = DateTime.Parse(args.SEndDate);
        //        radActivityPlanner.TimelineView.SlotDuration = TimeSpan.Parse("7");
        //    }
        //}
        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            ArgsDTO args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;

            //args.SStartDate = startDate.ToString("yyyy/MM/dd");
            //args.SEndDate = endDate.ToString("yyyy/MM/dd");
            if (radActivityPlanner.SelectedView == SchedulerViewType.DayView)
            {
                if (!string.IsNullOrEmpty(args.SEndDate))
                {
                    radActivityPlanner.SelectedDate = DateTime.Parse(args.SStartDate);
                    radActivityPlanner.TimelineView.SlotDuration = TimeSpan.Parse("7");
                }
            }
            else if (radActivityPlanner.SelectedView == SchedulerViewType.WeekView)
            {
                if (!string.IsNullOrEmpty(args.SStartDate))
                {
                    radActivityPlanner.SelectedDate = DateTime.Parse(args.SStartDate);
                    radActivityPlanner.TimelineView.SlotDuration = TimeSpan.Parse("7");
                }
            }
            else if (radActivityPlanner.SelectedView == SchedulerViewType.MonthView)
            {
                if (!string.IsNullOrEmpty(args.SStartDate))
                {
                    //radActivityPlanner.SelectedDate = DateTime.Parse(args.SStartDate);
                    //radActivityPlanner.VisibleRangeEnd= DateTime.Parse(args.SEndDate);
                    //radActivityPlanner.TimelineView.SlotDuration = TimeSpan.Parse("7");
                }
            }
        }
    }

    private string SetAutocomplete_Temp()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/call_cycle/call_cycle_service.asmx/GetRepsByOriginator\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Name,");
        sb.Append("                         desc: item.RepCode");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");

        sb.Append("                  var selectedObj = ui.item;");

        //sb.Append("                 $('#MainContent_HiddenFieldCustomerIndustry').val(selectedObj.industry);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustomerCode').val(selectedObj.label);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustCode').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_txtDRName').val(selectedObj.label);");
        sb.Append("                 setRepCode(selectedObj.desc);");
        //sb.Append("                 setcuscode(selectedObj.label,selectedObj.desc,'0');");
        //k   sb.Append("                 loadproductlist(selectedObj.label);");
        //sb.Append("                 $('ul.ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all').css('display','none');");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }

    protected void LoadUI()
    {
        if (UserSession.Instance.OriginatorString.Equals("ASE") || UserSession.Instance.OriginatorString.Equals("ADMIN") || UserSession.Instance.OriginatorString.Equals("SLC"))
        {
            List<KeyValuePair<string, string>> repLines = new List<KeyValuePair<string, string>>();
            repLines = GetRepsForDropdown();
            Label6.Text = "Rep :";
            foreach (KeyValuePair<string, string> ValuePair in repLines)
            {
                DropDownListRep.Items.Add(new ListItem(ValuePair.Value.ToString(), ValuePair.Key.ToString()));
            }
            DropDownListRep.SelectedIndex = 0;
            if (repLines.Count > 0)
            {
                ArgsDTO args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                args.RepCode = DropDownListRep.SelectedValue;
            }
        }
        else
        {

            Label6.Text = "DSR :";
            DropDownListRep.Items.Add(new ListItem(UserSession.Instance.Originator, UserSession.Instance.UserName));
        }

    }

    private List<KeyValuePair<string, string>> GetRepsForDropdown()
    {
        List<KeyValuePair<string, string>> repLines = new List<KeyValuePair<string, string>>();
        //OriginatorClient originatorService = new OriginatorClient();
        List<OriginatorDTO> originatorList = new List<OriginatorDTO>();
        OriginatorClient originatorClient = new OriginatorClient();

        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.Originator = UserSession.Instance.UserName;
        args.OriginatorType = UserSession.Instance.OriginatorString;
        args.AdditionalParams = " dept_string = 'DR'";
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 1000;// ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        originatorList = originatorClient.GetRepsByOriginator_For_Route_Assign(args);

        //originatorList = originatorService.GetRepsByOriginator("", UserSession.Instance.UserName); 
        //originatorList = originatorService.GetRepsByOriginator("", UserSession.Instance.ChildOriginators.Replace("originator", "o.originator"));


        if (originatorList != null)
        {
            foreach (OriginatorDTO reps in originatorList)
            {
                repLines.Add(new KeyValuePair<string, string>(reps.RepCode, reps.Name));
            }
        }
        //repLines.Add(new KeyValuePair<string, string>(6, "06 Months"));
        //repLines.Add(new KeyValuePair<string, string>(12, "01 Year"));
        //repLines.Add(new KeyValuePair<string, string>(0, "- All -"));

        return repLines;
    }

    protected void DropDownListRep_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //if (radRepTerritory.SelectedValue == null)
            //    radRepTerritory.SelectedValue = "- All -";

            //GetScheduledActivities(string.Empty, !radRepTerritory.SelectedValue.ToString().Equals("- All -") ?
            //    radRepTerritory.SelectedValue.ToString() :
            //    string.Empty);

            //  CurrentUserSettings.RepCode = DropDownListRep.SelectedValue;

            ArgsDTO args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.RepCode = DropDownListRep.SelectedValue;
            // LoadSchedulerData(CurrentUserSettings.RepCode);
            //   radActivityPlanner.Rebind();
        }
        catch (Exception ex)
        {
        }

    }

    private void LoadSchedulerData(string repCode)
    {
        AppointmentClient AppointmentClient = new AppointmentClient();
        ActivityClient activityClient = new ActivityClient();
        List<CustomerActivitiesDTO> actitvityList = new List<CustomerActivitiesDTO>();
        ArgsDTO ArgsDTO = new ArgsDTO();
        //   List<Telerik.Web.UI.Appointment> lstAppoinment = new List<Telerik.Web.UI.Appointment>();
        //  brAppointment oBrAppointment = new brAppointment();
        DataTable dtAppointment = new DataTable();
        ArgsDTO args = new ArgsDTO();

        List<ActivityDTO> activityTypeList = ViewState["activityType"] as List<ActivityDTO>;

        try
        {
            if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
            {
                args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                //args.Originator = UserSession.Instance.UserName;
                args.Originator = string.IsNullOrEmpty(UserSession.Instance.FilterByUserName) ? UserSession.Instance.UserName : UserSession.Instance.FilterByUserName;
                args.ChildOriginators = UserSession.Instance.ChildOriginators;
                args.SToday = string.IsNullOrEmpty(args.SToday) ? "" : args.SToday;
                args.SStartDate = "2014/05/01";//radActivityPlanner.vi.ToString("yyyy/MM/dd");
                args.SEndDate = "2014/05/31";//schedulerInfo.ViewEnd.ToString("yyyy/MM/dd");
                args.SSource = "Planner";
                args.Floor = string.IsNullOrEmpty(args.Floor) ? "" : args.Floor;
                args.Status = string.IsNullOrEmpty(args.Status) ? "" : args.Status;
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                args.RepCode = repCode;

                actitvityList = activityClient.GetAllActivities(args, args.ActivityStatus);
            }


            //AppointmentDTO appointment = null;
            //foreach (DataRow item in dtAppointment.Rows)
            //{
            //    appointment = new AppointmentDTO();

            //    appointment.ID = Convert.ToInt32(item["appointment_id"]);
            //    appointment.Subject = Convert.ToString(item["subject"]);

            //    appointment.Description = Convert.ToString(item["body"]);
            //    appointment.Start = Convert.ToDateTime(item["start_time"]);
            //    appointment.End = Convert.ToDateTime(item["end_time"]);

            //    appointment.Category = Convert.ToString(item["category"]);
            //    appointment.CreatedBy = Convert.ToString(item["created_by"]);
            //    appointment.LeadName = Convert.ToString(item["lead_name"]);

            //    appointmentList.Add(appointment);
            //}
            List<Telerik.Web.UI.AppointmentData> lstAppoinment = new List<Telerik.Web.UI.AppointmentData>();

            if (actitvityList != null)
            {

                foreach (CustomerActivitiesDTO activity in actitvityList)
                {
                    Telerik.Web.UI.AppointmentData newApp = new Telerik.Web.UI.AppointmentData();
                    //ContactAppointment newApp = new ContactAppointment();
                    //newApp.ContactName = !string.IsNullOrEmpty(activity.LeadName) ? activity.LeadName + " - " : string.Empty;
                    newApp.Start = activity.StartDate;
                    newApp.End = activity.EndDate;
                    newApp.Subject = activity.Subject;
                    //newApp.Body = activity.Comments;
                    newApp.ID = activity.ActivityID.ToString();

                    //if (activity.LeadName != "")
                    //    newApp.Subject = activity.LeadName + " - " + activity.Subject;
                    //else
                    newApp.Subject = activity.Subject;
                    newApp.Description = activity.RepName;
                    newApp.RecurrenceParentID = activity.routeId;
                    newApp.EncodedID = activity.RepCode;


                    // newApp.
                    //Dictionary<string, string> ff = new Dictionary<string, string>();
                    //ff.Add("BackColor", activity.TypeColour);

                    newApp.Resources.Add(new ResourceData() { Type = "ActivityTypes", Text = activity.ActivityType, Key = activity.ActivityType });// ColorTranslator.FromHtml(activity.TypeColour);

                    //if (activity.ActivityType != "")
                    //    newApp.Category = rsActivity.Categories.GetCategoryByName(activity.ActivityType);

                    // Using Location to store Lead / Customer (L / C)
                    // Using Url to store LeadID / CustCode
                    if (!string.IsNullOrWhiteSpace(activity.EndUserCode))   // END USER
                    {

                        newApp.Attributes.Add("Location", "E");
                        newApp.Attributes.Add("Url", activity.EndUserCode + ":" + activity.CustCode);

                    }
                    else if (string.IsNullOrWhiteSpace(activity.EndUserCode) && !string.IsNullOrWhiteSpace(activity.CustCode))   // CUSTOMER
                    {
                        newApp.Attributes.Add("Location", "C");
                        newApp.Attributes.Add("Url", activity.CustCode);
                    }
                    else if (activity.LeadId > 0)   // LEAD
                    {
                        newApp.Attributes.Add("Location", "L");
                        newApp.Attributes.Add("Url", activity.LeadId.ToString());
                    }
                    else    // ACTIVITY WITHOUT A CUSTOMER OR A LEAD
                    {
                        newApp.Attributes.Add("Location", "");
                        newApp.Attributes.Add("Url", "");
                    }
                    newApp.Attributes.Add("LeadName", activity.LeadName);
                    lstAppoinment.Add(newApp);
                }
            }
            radActivityPlanner.DataSource = lstAppoinment;
            radActivityPlanner.Rebind();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            if (dtAppointment != null)
                dtAppointment.Dispose();
        }

    }

    protected void buttonPrevious_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "call_cycle/transaction/template_entry.aspx");
    }

    protected void buttonItinerary_Click(object sender, EventArgs e)
    {
        String month = "";
        String year = "";
        if (Session["Month"] != null)
            month = Session["Month"].ToString();
        if (Session["Year"] != null)
            year = Session["Year"].ToString();
        Response.Redirect(ConfigUtil.ApplicationPath + "call_cycle/transaction/itinerary_entry.aspx?mon=" + month + "&yr=" + year);

    }

    private static String ErrorlineNo, Errormsg, extype, exurl, hostIp, ErrorLocation, HostAdd;

    public static void SendErrorToText(Exception ex)
    {
        var line = Environment.NewLine + Environment.NewLine;

        ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
        Errormsg = ex.GetType().Name.ToString();
        extype = ex.GetType().ToString();
        exurl = context.Current.Request.Url.ToString();
        ErrorLocation = ex.Message.ToString();

        try
        {
            string filepath = context.Current.Server.MapPath("~/LogFiles/");  //Text File Path

            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);

            }
            filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + "-RoutePlanner.txt";   //Text File Name
            if (!File.Exists(filepath))
            {
                File.Create(filepath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(filepath))
            {
                string error = "Log Written Date:" + " " + DateTime.Now.ToString() + line + "Error Line No :" + " " + ErrorlineNo + line + "Error Message:" + " " + Errormsg + line + "Exception Type:" + " " + extype + line + "Error Location :" + " " + ErrorLocation + line + " Error Page Url:" + " " + exurl + line + "User Host IP:" + " " + hostIp + line;
                sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now.ToString() + "-----------------");
                sw.WriteLine("-------------------------------------------------------------------------------------");
                sw.WriteLine(line);
                sw.WriteLine(error);
                sw.WriteLine("--------------------------------*End*------------------------------------------");
                sw.WriteLine(line);
                sw.Flush();
                sw.Close();

            }

        }
        catch (Exception e)
        {
            e.ToString();
        }
    }

    public static void SendLogToText(string ex)
    {
        try
        {
            string filepath = context.Current.Server.MapPath("~/LogFiles/");  //Text File Path

            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);

            }
            filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + "-RoutePlanner.txt";   //Text File Name
            if (!File.Exists(filepath))
            {
                File.Create(filepath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(filepath))
            {
                string error = "Log Written Date:" + " " + DateTime.Now.ToString() + "Message: " + ex;
                sw.WriteLine(error);
                sw.Flush();
                sw.Close();

            }
        }
        catch (Exception e)
        {
            e.ToString();
        }
    }
}
