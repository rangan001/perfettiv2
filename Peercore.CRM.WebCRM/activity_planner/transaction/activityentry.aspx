﻿<%@ Page Title="mSales" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="activityentry.aspx.cs" Inherits="activity_planner_transaction_activityentry" %>

<%@ Register Src="~/usercontrols/buttonbar_activity.ascx" TagPrefix="ucl1" TagName="buttonbarforActivity" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="AddEditActivityEntry" runat="server" />
    <asp:HiddenField ID="HiddenFieldLeadId" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustomerCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUser" runat="server" />
    <asp:HiddenField ID="HiddenFieldactivitytype" runat="server" />
    <asp:HiddenField ID="HiddenFieldactivitytypeUpdate" runat="server" />
    <asp:HiddenField ID="HiddenFieldStartDate" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndDate" runat="server" />
    <asp:HiddenField ID="HiddenFieldReminderDate" runat="server" />
    <div id="modalWindow" style="display: none">
        <div id="div_confirm_message">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
        <button id="save" class="k-button">
            Save</button>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                    <ucl1:buttonbarforActivity ID="buttonbarforActivity_Id" runat="server" />
                    <div class="clearall">
                    </div>

            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/leadentry.aspx">
                        
                        <div class="back">
                        </div></asp:HyperLink>
                </div>
            </div>
        </div>
        
        <div class="clearall">
        </div>
        <div class="clearall">
            &nbsp;</div>
        <div id="div_activityentry" runat="server">
            <div id="div_message" runat="server" style="display: none">
            </div>
            <div id="div_info" runat="server" style="display: none">
            </div>
            <div>
            <div class="formleft">
                <div class="formtextdiv_extended">
                    Subject</div>
                <div class="formdetaildiv_right">
                    <span class="wosub mand" style="height: 20px">
                        <asp:TextBox ID="txtsubject" runat="server" Width="235px"></asp:TextBox>
                        <span style="color: Red">*</span> </span>
                </div>
                <div class="clearall">
                </div> 
                <div class="formtextdiv_extended">
                    Activity Type</div>
                <div class="formdetaildiv_right">
                    <div id="div_activitytype" runat="server" visible="false" CssClass="input-large1">
                    </div>
                    <asp:DropDownList ID="radactivitytype" runat="server">
                    </asp:DropDownList>
                </div>                            
                <div class="clearall">
                </div>
                 <div class="formtextdiv_extended">
                    Start Date</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="startDatePicker" runat="server"></asp:TextBox>
                </div>  
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    End Date</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="endDatePicker" runat="server"></asp:TextBox>
                </div>                                             
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    Assigned To</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="txtAssignto" runat="server" ReadOnly="true" Width="235px"></asp:TextBox>
                    <a href="#" id="id_assign">
                        <img src="../../assets/images/popicon.png" border="0" /></a>
                </div>
                <div class="clearall"></div>
                <%--
        <div class="formtextdiv">BDM Group</div>
        <div class="formdetaildiv"><asp:TextBox ID="txtRepgroup" runat="server" ReadOnly="true"></asp:TextBox>
        <a href="#" id="id_bdmgroup"><img src="../../assets/images/popicon.png" border="0" /></a>
        </div>
        <div class="clearall"></div>--%>
                <div class="formtextdiv_extended">
                    Comments</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="txtcomments" runat="server" TextMode="MultiLine" MaxLength="500" onbeforepaste="doBeforePaste(this);" onkeypress="doKeypress(this);" onpaste="doPaste(this);" 
                        Rows="5" CssClass="input-large1" onfocus="ClearText('MainContent_txtcomments');"></asp:TextBox></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    &nbsp;</div>
                <div class="formdetaildiv_right">
                    <input id="checkemail" type="checkbox" runat="server" />
                    Send Notification Email
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    &nbsp;</div>
                <div class="formdetaildiv_mini">
                    <input id="checkreminder" type="checkbox" runat="server" />
                    Reminder</div><asp:TextBox ID="reminderDatePicker" runat="server" Style="width: 160px;"></asp:TextBox>
                                                           
            </div>




            <div class="formright">
                <div class="formtextdiv_extended">
                    Organisation</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="txtcontact" runat="server" ReadOnly="true" Enabled="false" CssClass="input-large1"></asp:TextBox>
                    <span style="color: Red">*</span> <a href="#" id="btncontact">
                        <img src="../../assets/images/popiconbw.png" border="0" /></a></div>
                <div class="clearall">
                </div> 
                <div class="formtextdiv_extended">
                    Customer</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="txtcustomer" runat="server" ReadOnly="true" Enabled="false" CssClass="input-large1"></asp:TextBox>
                    <a href="#" id="btncustomer">
                        <img src="../../assets/images/popiconbw.png" border="0" /></a>
                </div>
                <div class="clearall">
                </div> 
                <div class="formtextdiv_extended">
                    End User</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="txtEndUser" runat="server" ReadOnly="true" Enabled="false" CssClass="input-large1"></asp:TextBox>
                    <img src="../../assets/images/popiconbw.png" />
                </div>
                <div class="clearall">
                </div>   
                <div class="formtextdiv_extended">
                    Priority</div>
                <div class="formdetaildiv_right">
                    <asp:DropDownList ID="radpriority" runat="server" CssClass="input-large1">
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    Status</div>
                <div class="formdropdiv">
                    <asp:DropDownList ID="radstatus" runat="server" CssClass="input-large1">
                    </asp:DropDownList>
                </div>
                                                            
                                     
            </div>
                            <div class="clearall">
                </div>
                <div class="specialevents" align="right">
                    <asp:Label ID="tbModified" runat="server"></asp:Label>
                </div>
                <div class="clearall">
                </div>
                <div class="specialevents" align="right">
                    <asp:Label ID="tbCreated" runat="server"></asp:Label>
                </div>
                <div class="clearall">
                </div>
                <div style="float: right; margin-top:20px; width: 115px; padding-right: 10px">
                    <div style="height: 15px; width: 15px; background-color: #D8BFD8; border: 1px solid #000000;
                        float: left">
                    </div>
                    <div>
                        <asp:Label ID="Label1" runat="server" Text="Sent To Calendar"></asp:Label></div>
                </div>
            </div>
        </div>
        <div class="clearall">
            &nbsp;</div>
        <div style="min-width: 200px; margin:10px 10px;">
        <div id="LeadActivityGrid">
        </div>
    </div>
    </div>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelActivity" runat="server" />
    <asp:HiddenField ID="HiddenFieldUsertype" runat="server" />
    <script type="text/javascript">
        jQuery(function () {
            jQuery("#MainContent_txtsubject").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter Subject"
            });
        });

        $("#MainContent_txtcomments").blur(function () {
            var text = $("#MainContent_txtcomments").val();
            if (text == "" || text == null || text == undefined) {
                $("#MainContent_txtcomments").val('(Max 500 Characters)');
            } else if (text.indexOf('(Max 500 Characters)') == 0) {
                $("#MainContent_txtcomments").val();
            } else {
                $("#MainContent_txtcomments").val(text);
            }
        });
    </script>
    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");
        $(document).ready(function () {
            var type = $("#MainContent_HiddenFieldUsertype").val();
            var cust_code = $("#MainContent_HiddenFieldCustomerCode").val();

            if (type == 'Enduser') {
                var enduserid = $("#MainContent_HiddenFieldEndUser").val();
                loadEnduserActivityGrid(cust_code,enduserid, 'LeadActivityGrid');
            }
            else if (type == 'customer') {
                loadCustomerActivityGrid(cust_code, 'LeadActivityGrid');
            }
            else {
                loadLeadActivityGrid("LeadActivityGrid");
            }

            $("#id_assign").click(function () {
                originator('activity_planner/process_forms/processmaster.aspx?fm=activityentry&type=query&querytype=originator');
                //originator('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=originator');
            });

            //$("#MainContent_txtsubject").kendoValidator().data("kendoValidator");

            $("#MainContent_reminderDatePicker").kendoDateTimePicker({
                value: new Date(),
                format: "dd-MMM-yyyy HH:mm",
                timeFormat: "HH:mm",
                change: reminder_onChange
            });

            $("#MainContent_startDatePicker").kendoDateTimePicker({
                value: new Date(),
                format: "dd-MMM-yyyy HH:mm",
                timeFormat: "HH:mm",
                change: startDate_onChange
            });

            $("#MainContent_endDatePicker").kendoDateTimePicker({
                value: new Date(),
                format: "dd-MMM-yyyy HH:mm",
                timeFormat: "HH:mm",
                change: endDate_onChange
            });

            var datePicker = $("#MainContent_startDatePicker").data("kendoDateTimePicker");
            datePicker.value($("#MainContent_HiddenFieldStartDate").val());

            datePicker = $("#MainContent_endDatePicker").data("kendoDateTimePicker");
            datePicker.value($("#MainContent_HiddenFieldEndDate").val());

            datePicker = $("#MainContent_reminderDatePicker").data("kendoDateTimePicker");
            datePicker.value($("#MainContent_HiddenFieldReminderDate").val());

            function reminder_onChange() {
                var dateTimePicker = $("#MainContent_startDatePicker").data("kendoDateTimePicker");
                var reminderDateTimePicker = $("#MainContent_reminderDatePicker").data("kendoDateTimePicker");

                if (this.value() == null) {
                    var startDatePicker = dateTimePicker.value();
                    reminderDateTimePicker.value(startDatePicker);
                    showinvaliddateMessage();
                }
            }

            function endDate_onChange() {
                var dateTimePicker = $("#MainContent_startDatePicker").data("kendoDateTimePicker");
                var endDateTimePicker = $("#MainContent_endDatePicker").data("kendoDateTimePicker");

                if (this.value() == null) {
                    var startDatePicker = dateTimePicker.value();
                    endDateTimePicker.value(startDatePicker);
                    showinvaliddateMessage();
                }
                else {
                    var startDatePicker = dateTimePicker.value();
                    var endDatePicker = this.value();

                    if (endDatePicker < startDatePicker) {
                        this.value(startDatePicker);
                    }
                }
            }

            function startDate_onChange() {
                var dateTimePicker = $("#MainContent_endDatePicker").data("kendoDateTimePicker");
                var startdateTimePicker = $("#MainContent_startDatePicker").data("kendoDateTimePicker");

                if (this.value() == null) {
                    var endDatePicker = dateTimePicker.value();
                    startdateTimePicker.value(endDatePicker);
                    showinvaliddateMessage();
                }
                else {
                    var endDatePicker = dateTimePicker.value();
                    var startDatePicker = this.value();
                    if (endDatePicker < startDatePicker) {
                        dateTimePicker.value(startDatePicker);
                    }

                    dateTimePicker = $("#MainContent_reminderDatePicker").data("kendoDateTimePicker");
                    var reminderDatePicker = dateTimePicker.value();

                    if (reminderDatePicker < startDatePicker) {
                        dateTimePicker.value(startDatePicker);
                    }
                }
            }

        });   
    </script>
</asp:Content>
