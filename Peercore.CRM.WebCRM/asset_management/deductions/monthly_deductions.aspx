﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="monthly_deductions.aspx.cs" Inherits="asset_management_deductions_monthly_deductions" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />

    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                </div>
            </div>
            <%--  Content --%>
            <table>
                <tr>
                    <td colspan="2" class="textalignbottom">Rep Code 
                    </td>
                    <td colspan="4">:<span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtRepCode" maxlength="30" onchange="ValidateRepCodeExist()" />
                        <span style="color: Red">*</span></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="textalignbottom">Deduction Amount 
                    </td>
                    <td colspan="4">:<span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtAmount" maxlength="30" />
                        <span style="color: Red">*</span></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="textalignbottom">Deduction Month 
                    </td>
                    <td colspan="4">:<span class="wosub mand">&nbsp;<input id="monthpicker" class="textboxwidth" data-role="datepicker" data-start="year" data-depth="year" data-format="MMMM" />
                        <span style="color: Red">*</span></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="textalignbottom">
                        <button class="k-button" id="buttonSave" type="button">Save</button>
                        <button class="k-button" id="buttonClear">Clear</button>
                        <asp:HiddenField ID="hdnSelectedId" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="toolbar_container" id="id_div_toolbar_container">
        <div class="toolbar_left" id="div_content">
            <div class="hoributton">
                <div style="display: none" id="id_div_buttonBar">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
                <a id="id_add_deduction" class="sprite_button" style="float: left; margin-left: 5px; margin-top: 0px;">
                    <span class="plus icon"></span>Add Deduction</a>
                &nbsp;
               
            <asp:Button ID="excelUpload" runat="server" ToolTip="Upload" Text="Upload" CssClass="savebtn" OnClick="excelUpload_Click" />
            </div>

        </div>

        <div class="toolbar_right" id="div3" style="display: none;">
            <div class="leadentry_title_bar">
                <div style="float: right; width: 65%;" id="div_backButton">
                    <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back"></div>
                    </asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="formleft" style="width: 100%">
            <div class="formtextdiv">
                Select Excel File
           
            </div>
            <div class="formdetaildiv_right" style="line-height: 26px;">
                :
                   
                <div id="div_upload" runat="server" style="background-color: #FFFFFF; float: right; overflow: hidden;">
                    <asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
                    <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
                        ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red" Style="float: left;"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
            </div>
            <div class="formdetaildiv_right">
                <a href="../../docs/templates/MonthlyDeduction.xlsx">Sample Upload File</a>
            </div>
        </div>


        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="gridDeduction"></div>
            </div>
        </div>

    </div>

    <div id="DeductionWindow" style="display: none">
        <div id="div_Confirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <script type="text/javascript"> 
        var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {
            LoadAllDeductions();

        });

        //Load popup for Add Deduction
        $("#id_add_deduction").click(function () {
            $("#window").css("display", "block");

            // $("#txtRepCode").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add Deduction ",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtRepCode").focus();
            wnd.center().open();
            ClearControls();

        });

        $("#buttonSave").click(function () {
            saveDeduction();
        });

        $("#monthpicker").kendoDatePicker({
            start: "year",
            depth: "year",
            format: "yyyy MMMM"
        });

        $("#buttonClear").click(function () {
            ClearControls();
        });

        function LoadAllDeductions() {

            var url = "javascript:EditDeductions('#=Id#','#=RepCode#','#=DeductionMonthName#','#=DeductionAmount#');";

            var urlDeductId = "<a href=\"" + url + "\">#=Id#</a>";
            var urlRepCode = "<a href=\"" + url + "\">#=RepCode#</a>";
            var urlpayMonth = "<a href=\"" + url + "\">#=DeductionMonthName#</a>";
            var urlDeductAmount = "<a href=\"" + url + "\">#=DeductionAmount#</a>";

            $("#gridDeduction").html("");
            $("#gridDeduction").kendoGrid({
                columns: [
                    { template: urlDeductId, field: "Id", hidden: true },
                    { template: urlpayMonth, field: "DeductionMonthName", title: "Deduction Month", width: "100px" },
                    { template: urlRepCode, field: "RepCode", title: "Rep Code", width: "100px" },
                    { template: urlDeductAmount, field: "DeductionAmount", title: "Deduction Amount", width: "100px" },
                    {
                        template: '<a href="javascript:deleteDeduction(\'#=Id#\', \'#=RepCode#\', \'#=DeductionAmount#\');">Delete</a>',
                        field: "",
                        width: "60px"
                    }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                navigatable: true,
                editable: "popup",
                dataBound: onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    batch: true,
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            fields: {
                                Id: { type: "number" },
                                RepCode: { type: "string" },
                                DeductionMonth: { type: "string" },
                                DeductionAmount: { type: "number" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/asset_management/mothly_deduction_service.asmx/GetAllDeductions",
                            contentType: "application/json; charset=utf-8",
                            data: { pgindex: take_grid },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    var entityGrid = $("#gridDeduction").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }

            });
        }

        function EditDeductions(id, repCode, deductMonth, deductAmount) {

            $("#hdnSelectedId").val(id);
            $("#txtRepCode").val(repCode);
            $("#monthpicker").val(deductMonth);
            $("#txtAmount").val(deductAmount);

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Edit Deduction",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            $("#txtRepCode").focus();

            wnd.center().open();
        }

        function saveDeduction() {
            var paymentId = $("#hdnSelectedId").val();

            if (($("#txtRepCode").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter Rep Code.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (($("#txtAmount").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter Deduction Amount.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (($("#monthpicker").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Select Month.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if ($("#txtRepCode").val() != '' && $("#txtAmount").val() != '' && $("#monthpicker").val() != '') {
                var Originator = $("#<%= OriginatorString.ClientID %>").val();
                var repCode = $("#txtRepCode").val();
                var deductAmount = $("#txtAmount").val();
                var deductMonth = $("#monthpicker").val();
                var deductId = $("#hdnSelectedId").val();

                console.log(Originator, " ", repCode, " ", deductAmount, " ", deductMonth, " ", deductId);

                var param = {
                    "deductId": deductId, "repCode": repCode, "deductMonth": deductMonth, "deductAmount": deductAmount, "Originator": Originator
                };

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/asset_management/mothly_deduction_service.asmx/SaveDeduction",
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var sucessMsg = GetSuccesfullMessageDiv("Deduction was successfully Updated.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopup();
                        LoadAllDeductions();
                        ClearControls();
                        closeDeductionpopup();
                        hideStatusDiv("MainContent_div_message");

                    },
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                            $("#div_loader").hide();
                        }
                    }
                });
            }
        }

        function deleteDeduction(id, repCode, deductAmount) {
            $("#DeductionWindow").show()
            deleteDeductionbyRep(id, repCode, deductAmount);
        }

        function deleteDeductionbyRep(id, repCode, deductAmount) {

            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete '" + deductAmount + "' amount assign to '" + repCode + "'?";

            var wnd = $("#DeductionWindow").kendoWindow({
                title: "Delete Deduction",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_Confirm_Message").text(message);
            wnd.center().open();

            $("#yes").click(function () {

                var root = 'asset_management/deductions/monthly_deductions.aspx?fm=deleteDeduction&type=delete&id=' + id;
                delete_deduction(root);

            });

            $("#no").click(function () {
                closeDeductionpopup();
            });

        }

        function delete_deduction(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;

            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Deduction was successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopup();
                        LoadAllDeductions();
                        ClearControls();
                        closeDeductionpopup();
                        hideStatusDiv("MainContent_div_message");
                        $("#div_loader").hide();
                        return;
                    }
                    else {
                        var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        closepopup();
                        hideStatusDiv("MainContent_div_message_popup");
                        return;
                    }
                },
                error: function (msg) {
                    if (msg == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function ValidateRepCodeExist() {
            var repCode = document.getElementById("txtRepCode").value;

            var params = "{'repCode':'" + repCode + "'}";
            console.log(params);
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/mothly_deduction_service.asmx/IsRepCodeAvailable",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response.d.Status);
                    if (!response.d.Status) {
                        alert("Rep Code '" + repCode + "' Not Exist.");
                        $("#txtRepCode").val("");
                        $("#txtRepCode").focus();
                    }

                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function closepopup() {
            var wnd = $("#DeductionWindow").kendoWindow({
                title: "Deductions",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#DeductionWindow").css("display", "none");
        }

        function closeDeductionpopup() {
            var wnd = $("#window").kendoWindow({
                title: "Deductions",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        function ClearControls() {
            $("#hdnSelectedId").val("");
            $("#txtRepCode").val("");
            $("#txtAmount").val("");
            $("#monthpicker").val("");
        }


    </script>

</asp:Content>


