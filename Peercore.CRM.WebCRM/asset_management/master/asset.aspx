﻿<%@ Page Title="mSales | Assets" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="asset.aspx.cs" Inherits="asset_management_master_asset" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        input, textarea, .uneditable-input {
            margin-bottom: 4px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="saveUpdateOption" runat="server" />
    <asp:HiddenField ID="hdnSelectedAssetId" runat="server" ClientIDMode="Static" />

    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 1000px">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>

                </div>
            </div>

            <div class="formright" style="margin-bottom: 20px;">
                <fieldset>
                    <legend>Asset Details</legend>
                    <table>
                        <tbody>
                            <tr>
                                <td class="textalignbottom">Asset Type
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<asp:DropDownList ID="ddlAssetTypes" runat="server" class="input-large1" Width="262px">
                                    </asp:DropDownList><span style="color: Red">&nbsp;*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Asset Code 
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtAssetCode" onchange="ValidateCodeExist(this)"/>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Asset Model
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtModel" />
                                        <%-- <span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">IMEI Number 1
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtimei1" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">IMEI Number 2
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtimei2" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Serial Number
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtSerial" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Company Asset Code 
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtCompAssetCode" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Printer Pouch 
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtPrinterPouch" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Monitor Seria
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtMonitorSerial" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Monitor Model
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtMonitorModel" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">SIM Number 
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtSIM" />
                                        <%--<span style="color: Red">*</span>--%></span>
                                </td>
                            </tr>


                            <tr>
                                <td class="textalignbottom">Status 
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="input-large1" Width="262px">
                                        <asp:ListItem Value="1" Text="Active" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="Inactive"></asp:ListItem>
                                        <%--<asp:ListItem Value="2" Text="Assigned"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </fieldset>

            </div>
            <div class="formright" style="margin-bottom: 20px;">
                <fieldset>
                    <legend>Supplier Details</legend>
                    <table>
                        <tr>
                            <td class="textalignbottom">Supplier 
                            </td>
                            <td colspan="3">
                                <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtSupplier" />
                                    <%--<span style="color: Red">*</span>--%></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="textalignbottom">Invoice Number 
                            </td>
                            <td colspan="3">
                                <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtInvNo" />
                                    <%--<span style="color: Red">*</span>--%></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="textalignbottom">Value LKR  
                            </td>
                            <td colspan="3">
                                <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtValue" />
                                    <%--<span style="color: Red">*</span>--%></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="textalignbottom">Purchase Date 
                            </td>
                            <td colspan="3">
                                <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtPurchDate" />
                                    <%--<span style="color: Red">*</span>--%></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="textalignbottom">Warranty  
                            </td>
                            <td colspan="3">
                                <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtWarranty" />
                                    <%--<span style="color: Red">*</span>--%></span>
                            </td>
                        </tr>

                    </table>
                </fieldset>
                <table>
                    <tbody>
                        <tr>
                            <td class="textalignbottom">Description  
                            </td>
                            <td colspan="3">
                                <span class="wosub mand">&nbsp;<textarea class="textboxwidth" id="txtdescrption" ></textarea>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="textalignbottom">
                                <button class="k-button" id="buttonSave" type="button">
                                    Save</button>
                                <button class="k-button" id="buttonCancel">
                                    Clear</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="toolbar_container" id="id_div_toolbar_container">
        <div class="toolbar_left" id="div_content">
            <div class="hoributton">
                <div style="display: none" id="id_div_buttonBar">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>

                <a id="id_add_asset" class="sprite_button" style="float: left; margin-left: 5px; margin-top: 0px;">
                    <span class="plus icon"></span>Add Asset</a>
                &nbsp;
                    <asp:Button ID="excelUpload" runat="server" ToolTip="Upload" Text="Upload" CssClass="savebtn" OnClick="excelUpload_Click" />
            </div>
        </div>

        <div class="toolbar_right" id="div3" style="display: none;">
            <div class="leadentry_title_bar">
                <div style="float: right; width: 65%;" id="div_backButton">
                    <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back"></div>
                    </asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="formleft" style="width: 100%">
            <div class="formtextdiv">
                Select Excel File
            </div>
            <div class="formdetaildiv_right" style="line-height: 26px;">
                :
                    <div id="div_upload" runat="server" style="background-color: #FFFFFF; float: right; overflow: hidden;">
                        <asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
                        <%--<asp:Button Text="Review" runat="server" ID="btnReview" Style="float: left;" Visible="false" />--%>
                        <%--<asp:Button Text="Upload" runat="server" ID="excelUpload" Style="float: left;" OnClick="excelUpload_Click" />--%>
                        <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
                            ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red" Style="float: left;"></asp:RegularExpressionValidator>
                    </div>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
            </div>
            <div class="formdetaildiv_right">
                <a href="../../docs/templates/AssetesMaster.xlsx">Sample Upload File</a>
            </div>
        </div>


        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="formleft" style="width: 95%"> 
            <asp:HyperLink ID="linkLog" runat="server" Visible="false"><p style="text-align:end;">Excel upload log..</p></asp:HyperLink>
        </div>

        <div class="clearall">
        </div>
        <div id="div_assetss" runat="server" style="display: block">
            <div style="min-width: 200px; overflow: auto">
                <div style="float: left; width: 98.5%; overflow: auto; margin: 0px 10px;">
                    <div id="divAssetGrid">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="AssetWindow" style="display: none">
        <div id="div_AssetConfirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <script type="text/javascript">

        var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {
            LoadAllAsset();
            $("#AssetCode").focus();

        });

        //Load popup for Add new Asset Type
        $("#id_add_asset").click(function () {
            $("#window").css("display", "block");
            
            // $("#txtassettype").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Asset ",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtassettype").focus();
            wnd.center().open();
            ClearControls();

        });

        //Save button click
        $("#buttonSave").click(function () {
            saveAsset();
        });

        $("#buttonCancel").click(function () {
            ClearControls();
            //closepopup();
        });

        function EditAsset(id, typeId, code, model, imei1, imei2, serial, compCode, printerPouch, monitorModel, monitorSerial, sim, supplier, invNo, value, purchDate, warranty, status, description) {

            var puDate = kendo.toString(kendo.parseDate(purchDate, 'yyyy-MM-dd'), 'yyyy-MM-dd');

            $("#hdnSelectedAssetId").val(id);
            $("#<%= ddlAssetTypes.ClientID %>").val(typeId);
            $("#txtAssetCode").val(code);
            $("#txtModel").val(model);
            $("#txtimei1").val(imei1);
            $("#txtimei2").val(imei2);
            $("#txtSerial").val(serial);
            $("#txtCompAssetCode").val(compCode);
            $("#txtPrinterPouch").val(printerPouch);
            $("#txtMonitorModel").val(monitorModel);
            $("#txtMonitorSerial").val(monitorSerial);
            $("#txtSIM").val(sim);
            $("#txtSupplier").val(supplier);
            $("#txtInvNo").val(invNo);
            $("#txtValue").val(value);
            $("#txtPurchDate").val(puDate);
            $("#txtWarranty").val(warranty);
            $("#<%= ddlStatus.ClientID %>").val(status);
            $("#txtdescrption").val(description);

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Edit Asset",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            $("#txtAssetCode").focus();

            wnd.center().open();
        }

        function LoadAllAsset() {

            var url = "javascript:EditAsset('#=AssetId#','#=AssetTypeId#','#=AssetCode#','#=Model#','#=IMEI_1#','#=IMEI_2#','#=SerialNo#','#=CompAssetCode#','#=PrinterPouch#','#=MonitorModel#','#=MonitorSerial#','#=SIM#','#=Supplier#','#=InvoiceNo#','#=ValueLKR#','#=PurchaseDate#','#=Warranty#','#=AsssetStatus#','#=Description#');";

            var urlAssetId = "<a href=\"" + url + "\">#=AssetId#</a>";
            var urlAssetTypeName = "<a href=\"" + url + "\">#=AssetTypeName#</a>";
            var urlAssetCode = "<a href=\"" + url + "\">#=AssetCode#</a>";
            var urlAssetModel = "<a href=\"" + url + "\">#=Model#</a>";
            var urlAssetIMEI1 = "<a href=\"" + url + "\">#=IMEI_1#</a>";
            var urlAssetIMEI2 = "<a href=\"" + url + "\">#=IMEI_2#</a>";
            var urlAssetSERIAL = "<a href=\"" + url + "\">#=SerialNo#</a>";
            var urlAssetCompCode = "<a href=\"" + url + "\">#=CompAssetCode#</a>";
            var urlAssetDescription = "<a href=\"" + url + "\">#=Description#</a>";

            $("#divAssetGrid").html("");
            $("#divAssetGrid").kendoGrid({
                columns: [
                    { field: "AssetId", hidden: true },
                    { template: urlAssetTypeName, field: "AssetTypeName", title: "Asset Type ", filterable: false, width: "100px" },
                    { template: urlAssetCode, field: "AssetCode", title: "Asset Code", width: "100px" },
                    { template: urlAssetModel, field: "Model", title: "Model", width: "100px" },
                    { template: urlAssetIMEI1, field: "IMEI_1", title: "IMEI 01", width: "100px" },
                    { template: urlAssetIMEI2, field: "IMEI_2", title: "IMEI 02", width: "100px" },
                    { template: urlAssetSERIAL, field: "SerialNo", title: "Serial No", width: "100px" },
                    { template: urlAssetCompCode, field: "CompAssetCode", title: "Company Asset Code", width: "100px" },
                    { template: urlAssetDescription, field: "Description", title: "Description", filterable: false, width: "200px" },
                    { field: "createdDate", title: "Created Date", filterable: false, width: "200px", hidden: true },
                    {
                        template: '<a href="javascript:deleteAssets(\'#=AssetId#\', \'#=AssetCode#\');">Delete</a>',
                        field: "",
                        width: "60px"
                    }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                navigatable: true,
                dataBound: onDataBound,
                //editable: "popup",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    batch: true,
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            fields: {
                                AssetId: { type: "number" },
                                AssetTypeName: { type: "string" },
                                AssetCode: { type: "string" },
                                Model: { type: "string" },
                                IMEI_1: { type: "string" },
                                IMEI_2: { type: "string" },
                                SerialNo: { type: "string" },
                                CompAssetCode: { type: "string" },
                                PrinterPouch: { type: "string" },
                                MonitorModel: { type: "string" },
                                MonitorSerial: { type: "string" },
                                SIM: { type: "string" },
                                Supplier: { type: "string" },
                                Description: { type: "string" },
                                CreatedDate: { type: "date", format: "{0:yyyy-MM-dd}" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/asset_management/asset_service.asmx/GetAllAssets",
                            contentType: "application/json; charset=utf-8",
                            data: { pgindex: take_grid },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    var entityGrid = $("#divAssetGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }

            });
        }

        function saveAsset() {
            var selectedId = $("#hdnSelectedAssetId").val();

            if (($("#txtAssetCode").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter Asset Code.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            //if (($("#txtCompAssetCode").val()).trim() == '') {
            //    var sucessMsg = GetErrorMessageDiv("Please Enter Asset Code.", "MainContent_div_message");
            //    $('#MainContent_div_message').css('display', 'block');
            //    $("#MainContent_div_message").html(sucessMsg);
            //    hideStatusDiv("MainContent_div_message");
            //    return;
            //}

            //if (($("#txtSupplier").val()).trim() == '') {
            //    var sucessMsg = GetErrorMessageDiv("Please Enter Supplier.", "MainContent_div_message");
            //    $('#MainContent_div_message').css('display', 'block');
            //    $("#MainContent_div_message").html(sucessMsg);
            //    hideStatusDiv("MainContent_div_message");
            //    return;
            //}

            //if (($("#txtInvNo").val()).trim() == '') {
            //    var sucessMsg = GetErrorMessageDiv("Please Enter Invoice No.", "MainContent_div_message");
            //    $('#MainContent_div_message').css('display', 'block');
            //    $("#MainContent_div_message").html(sucessMsg);
            //    hideStatusDiv("MainContent_div_message");
            //    return;
            //}

            //if (($("#txtValue").val()).trim() == '') {
            //    var sucessMsg = GetErrorMessageDiv("Please Enter Value(LKR).", "MainContent_div_message");
            //    $('#MainContent_div_message').css('display', 'block');
            //    $("#MainContent_div_message").html(sucessMsg);
            //    hideStatusDiv("MainContent_div_message");
            //    return;
            //}

            //if (($("#txtPurchDate").val()).trim() == '') {
            //    var sucessMsg = GetErrorMessageDiv("Please Enter Purchase Date.", "MainContent_div_message");
            //    $('#MainContent_div_message').css('display', 'block');
            //    $("#MainContent_div_message").html(sucessMsg);
            //    hideStatusDiv("MainContent_div_message");
            //    return;
            //}

            if ($("#ddlAssetTypes").val() != '' && $("#txtAssetCode").val() != '') {

                console.log(selectedId);
                var Originator = $("#<%= OriginatorString.ClientID %>").val(); 
                var assetType = $("#<%= ddlAssetTypes.ClientID %>").val();
                var assetCode = document.getElementById('txtAssetCode').value;
                var model = document.getElementById('txtModel').value;
                var imei1 = document.getElementById('txtimei1').value;
                var imei2 = document.getElementById('txtimei2').value;
                var serial = document.getElementById('txtSerial').value;
                var compCode = document.getElementById('txtCompAssetCode').value;
                var printerPouch = document.getElementById('txtPrinterPouch').value;
                var monitorModel = document.getElementById('txtMonitorModel').value;
                var monitorSerial = document.getElementById('txtMonitorSerial').value;
                var sim = document.getElementById('txtSIM').value;
                var supplier = document.getElementById('txtSupplier').value;
                var invNo = document.getElementById('txtInvNo').value;
                var valueLKR = document.getElementById('txtValue').value;
                var purchDate = document.getElementById('txtPurchDate').value;
                var warranty = document.getElementById('txtWarranty').value;
                var status = $("#<%= ddlStatus.ClientID %>").val();
                var description = document.getElementById('txtdescrption').value;

                var param = {
                    "id": selectedId, "assetType": assetType, "assetCode": assetCode, "assetModel": model, "imei_1": imei1, "imei_2": imei2, "serialNo": serial, "compAssetCode": compCode,
                    "printerPouch": printerPouch, "monitorModel": monitorModel, "monitorSerial": monitorSerial, "SIM": sim, "supplier": supplier, "invNo": invNo,
                    "value": valueLKR, "purchasedate": purchDate, "warranty": warranty,
                    "status": status, "description": description, "Originator": Originator
                };

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/asset_management/asset_service.asmx/SaveAsset",
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var sucessMsg = GetSuccesfullMessageDiv("Asset Type was successfully Saved.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopup();
                        LoadAllAsset();
                        ClearControls();
                        hideStatusDiv("MainContent_div_message");

                    },
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                            $("#div_loader").hide();
                        }
                    }
                });
            }
        }

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Edit Asset",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        function deleteAssets(asset_id, assetCode) {
            $("#AssetWindow").show();
            deleteAsset(asset_id, assetCode);
        }

        function deleteAsset(asset_id, assetCode) {

            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete '" + assetCode + "' ?";

            var wnd = $("#AssetWindow").kendoWindow({
                title: "Delete Asset",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_AssetConfirm_Message").text(message);
            wnd.center().open();

            $("#yes").click(function () {
                var root = 'asset_management/master/asset.aspx?fm=deleteAsset&type=delete&assetid=' + asset_id;
                delete_asset(root);
            });

            $("#no").click(function () {
                closepopupAsset();
            });
        }

        function delete_asset(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;

            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    $('#MainContent_div_message').css('display', 'none');
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Asset was successfully deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopupAsset();
                        LoadAllAsset();
                        $("#div_loader").hide();
                        hideStatusDiv("MainContent_div_message");
                        return;
                    }
                    else if (msg == "assigned") {
                        var existsMsg1 = GetErrorMessageDiv("Already assigned.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(existsMsg1);
                        closepopupAsset();
                        $("#div_loader").hide();
                        hideStatusDiv("MainContent_div_message");
                        return;
                    }
                    else {
                        var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(existsMsg);
                        closepopupAsset();
                        $("#div_loader").hide();
                        hideStatusDiv("MainContent_div_message");
                        return;
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function ValidateCodeExist() {
            var assetCode = document.getElementById("txtAssetCode").value;
           
            var params = "{'assetCode':'" + assetCode + "'}";
            console.log(params);
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/IsAssetCodeAvailable",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response.d.Status);
                    if (response.d.Status) {
                        alert("Asset code '" + assetCode + "' already exist.");
                        $("#txtAssetCode").val("");
                        $("#txtAssetCode").focus();
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function closepopupAsset() {
            var wnd = $("#AssetWindow").kendoWindow({
                title: "Add/Edit Asset Type",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#AssetWindow").css("display", "none");
        }

        function ClearControls() {
            $("#hdnSelectedAssetId").val("");
            $("#<%= ddlAssetTypes.ClientID %>").val(0);
            $("#txtAssetCode").val("");
            $("#txtModel").val("");
            $("#txtimei1").val("");
            $("#txtimei2").val("");
            $("#txtSerial").val("");
            $("#txtCompAssetCode").val("");
            $("#txtPrinterPouch").val("");
            $("#txtMonitorModel").val("");
            $("#txtMonitorSerial").val("");
            $("#txtSIM").val("");
            $("#txtSupplier").val("");
            $("#txtInvNo").val("");
            $("#txtValue").val("");
            $("#txtPurchDate").val("");
            $("#txtWarranty").val("");
            $("#<%= ddlStatus.ClientID %>").val(1);
            $("#txtdescrption").val("");
        }

    </script>

</asp:Content>
