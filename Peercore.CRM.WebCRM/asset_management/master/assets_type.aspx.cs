﻿using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;

public partial class asset_management_master_assets_type : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
            {
                if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                {
                    if (Request.QueryString["type"] == "delete")
                    {
                        if (Request.QueryString["fm"].ToLower() == "deleteassettype")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteAssetType();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                    }
                }
            }

            if (!IsPostBack)
            {
                //
            }

            Master.SetBreadCrumb("Asset Types ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.UserName;
    }

    private string DeleteAssetType()
    {
        try
        {
            int Id = 0;
            AssetsClient assetService = new AssetsClient();

            if (Request.QueryString["assetid"] != null && Request.QueryString["assetid"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["assetid"].ToString());
            }

            bool status = assetService.DeleteAssetType(Id, UserSession.Instance.UserName);

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }



}
