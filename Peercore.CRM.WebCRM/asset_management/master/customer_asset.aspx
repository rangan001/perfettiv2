﻿<%@ Page Title="mSales | Asset Assign" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="customer_asset.aspx.cs" Inherits="asset_management_master_customer_asset" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        input, textarea, .uneditable-input {
            margin-bottom: 3px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="saveUpdateOption" runat="server" />
    <asp:HiddenField ID="hdnSelectedAssetAssignId" runat="server" ClientIDMode="Static" />


    <div id="div_main" class="divcontectmainforms">

        <div id="window" style="display: none; width: 1150px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                </div>
            </div>
            <div class="formright" style="margin-bottom: 20px;">
                <fieldset style="width: 550px;">
                    <legend>Outlet Detail</legend>
                    <table border="0">
                        <tbody>
                            <tr style="display: none;">
                                <td colspan="2" class="textalignbottom">User Type 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList ID="ddluserTypes" runat="server" class="input-large1" Width="262px">
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Outlet Code 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtCusCode" maxlength="200" onchange="GetCustomerName()" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Outlet Name 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtCusName" maxlength="200" readonly />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <div id="assetgrid"></div>
                </fieldset>
            </div>

            <div class="formright" style="margin-bottom: 20px;">
                <fieldset style="width: 450px;">
                    <legend>Assign Assets</legend>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Asset Type  
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList ID="ddlAssetType" runat="server" class="input-large1" Width="262px" onchange="LoadAssets(this)">
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Asset Code  
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList ID="ddlAssetCode" runat="server" class="input-large1" Width="262px" onchange="CheckAlreadyAssigned(this)">  <%----%>
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Asset Refund Cost 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtCost" maxlength="200" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Status 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList ID="ddlStatus" runat="server" class="input-large1" Width="262px" onchange="LoadOtherStatus(this)">
                                            <asp:ListItem Value="1" Text="Assign"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Un-Assign"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Other"></asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr id="trOther">
                                <td colspan="2" class="textalignbottom">Other Reason 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList ID="ddlOtherStatus" runat="server" class="input-large1" Width="262px">
                                        </asp:DropDownList>
                                    </span><span style="color: Red">*</span>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" class="textalignbottom">Assign Date 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="date" class="textboxwidth" id="txtAssignDate" />
                                    </span><span style="color: Red">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Returned Date 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="date" class="textboxwidth" id="txtReturnedDate" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Agreement 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<textarea id="txtAgreement" name="" cols="36" rows="2"></textarea>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Remark 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<textarea id="txtRemark" name="" cols="36" rows="2"></textarea>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="btnAdd" type="button">Add</button>
                                    <button class="k-button" id="btnClear">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
            </div>

            <div style="width: 100%">
                <table>
                    <tr>
                        <td colspan="4" class="textalignbottom">
                            <button class="k-button" id="buttonSaveAssetAssign" type="button">Save</button>
                            <button class="k-button" id="buttonClear">Clear</button>
                            <asp:HiddenField ID="hdnSelectedId" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="toolbar_left" id="div_content">
            <div class="hoributton">
                <div style="display: none" id="id_div_buttonBar">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>

                <a id="id_assign_asset" class="sprite_button" style="float: left; margin-left: 5px; margin-top: 0px;">
                    <span class="plus icon"></span>Add Outlet Asset</a>
                &nbsp;
                <asp:Button ID="excelUpload" runat="server" ToolTip="Upload" Text="Upload" CssClass="savebtn" OnClick="excelUpload_Click" />
            </div>
        </div>

        <div class="toolbar_container" style="display: none;">
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back">
                            </div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>

        <div class="formleft" style="width: 100%">
            <div class="formtextdiv">
                Select Excel File
            </div>
            <div class="formdetaildiv_right" style="line-height: 26px;">
                :
                    <div id="div_upload" runat="server" style="background-color: #FFFFFF; float: right; overflow: hidden;">
                        <asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
                        <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
                            ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red" Style="float: left;">
                        </asp:RegularExpressionValidator>
                    </div>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
            </div>
            <div class="formdetaildiv_right">
                <a href="../../docs/templates/AssetsTransaction.xlsx">Sample Upload File</a>
            </div>
        </div>

    </div>

    <div class="clearall">
    </div>
    <div id="div_message" runat="server" style="display: none;">
    </div>
    <div class="formleft" style="width: 95%">
        <asp:HyperLink ID="linkLog" runat="server" Visible="false"><p style="text-align:end;">Excel upload log..</p></asp:HyperLink>
    </div>
    <div class="clearall">
    </div>
    <div style="min-width: 200px; overflow: auto">
        <div style="float: left; width: 100%; overflow: auto">
            <div id="grid_assign_assets"></div>
        </div>
    </div>


    <div id="AssetAssignmodalWindow" style="display: none">
        <div id="div_AssignConfirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>


    <script type="text/javascript">

        var take_grid = $("#MainContent_hfPageIndex").val();
        var Token = "Cust";

        $(document).ready(function () {
            CustomerAssetHelper.GenarateAssetGrid();
        });

        var CustomerAssetHelper = {
            GenarateAssetGrid: function () {
                $("#grid_assign_assets").kendoGrid({
                    //dataSource: CustomerAssetManager.gridDataSource(),
                    editable: false, // enable editing
                    pageable: true,
                    sortable: true,
                    filterable: true,
                    navigatable: true,
                    editable: "popup",
                    dataBound: onDataBound,
                    columns: CustomerAssetHelper.GenarateAssetGridColumn(),
                    dataSource: {
                        serverSorting: true,
                        serverPaging: true,
                        serverFiltering: true,
                        pageSize: 15,
                        batch: true,
                        schema: {
                            data: "d.Data",
                            total: "d.Total",
                            model: {
                                fields: {
                                    //AssignId: { type: "number" },
                                    UserType: { type: "string" },
                                    UserCode: { type: "string" },
                                    UserName: { type: "string" },
                                    AssetTypeName: { type: "string" },
                                    AssetCode: { type: "string" }
                                    //AssignDate: { type: "date", format: "{0:MM/dd/yyyy}" }
                                }
                            }
                        },
                        transport: {
                            read: {
                                url: ROOT_PATH + "service/asset_management/asset_service.asmx/GetAllAssignAsset",
                                contentType: "application/json; charset=utf-8",
                                data: { pgindex: take_grid, Token: Token },
                                type: "POST",
                                complete: function (jqXHR, textStatus) {
                                    if (textStatus == "success") {

                                        var entityGrid = $("#grid_assign_assets").data("kendoGrid");
                                        if ((take_grid != '') && (gridindex == '0')) {
                                            entityGrid.dataSource.page((take_grid - 1));
                                            gridindex = '1';
                                        }
                                    }
                                }
                            },
                            parameterMap: function (data, operation) {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                });
            },
            GenarateAssetGridColumn: function () {
                return columns = [
                    { field: "AssignId", hidden: true },
                    { field: "UserType", title: "User Type", width: "100px", hidden: true },
                    { field: "UserCode", title: "Outlet Code", width: "100px" },
                    { field: "UserName", title: "Outlet Name", width: "200px" },
                    { field: "AssetTypeName", title: "Asst Type", width: "100px" },
                    { field: "AssetCode", title: "Asst Code", width: "100px" },
                    { template: "#= kendo.toString(kendo.parseDate(AssignDate, 'yyyy-MM-dd'), 'MM/dd/yyyy') #", field: "AssignDate", title: "Assign Date", width: "200px" },
                    {
                        template: '<a href="javascript:deleteAssignedAsset(\'#=AssignId#\', \'#=UserCode#\', \'#=AssetTypeName#\');">Delete</a>',
                        field: "",
                        width: "60px"
                    }
                ];
            }
        };

        //window assign asset table load - Start
        //Load popup for Assign Assets
        $("#id_assign_asset").click(function () {
            $("#window").css("display", "block");

            var tb = document.getElementById("trOther");
            tb.style.display = "none"; 

            var wnd = $("#window").kendoWindow({
                title: "Assign Assets ",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().open();
            AssetHelper.GenarateAssignAssetGrid();
            ClearControls();
        });

        var AssetManager = {
            gridDataSource: function () {
                var gridDataSource = new kendo.data.DataSource({
                    pageSize: 15,
                    batch: true,
                    transport: {
                        read: {
                            contentType: "application/json; charset=utf-8",
                            type: "POST",
                            dataType: "json"
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    },
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            fields: {
                                AssignId: { type: "number" },
                                UserType: { type: "string" },
                                UserCode: { type: "string" },
                                UserName: { type: "string" },
                                AssetTypeName: { type: "string" },
                                AssetTypeId: { type: "string" },
                                AssetCode: { type: "string" },
                                RefundCost: { type: "number" },
                                Status: { type: "string" },
                                AssignDate: { type: "date", format: "{0:yyyy-MM-dd}"},
                                ReturnDate: { type: "date", format: "{0:yyyy-MM-dd}"},
                                Agreement: { type: "string" },
                                Remark: { type: "string" }
                            }
                        }
                    },
                    //parse: function (response) {
                    //    $.each(response, function (idx, elem) {
                    //        if (elem.ReturnDate && typeof elem.ReturnDate === "string") {
                    //            elem.ReturnDate = kendo.toString(kendo.parseDate(elem.ReturnDate, 'yyyy-MM-dd'), 'yyyy-MM-dd'); //kendo.parseDate(elem.AssignDate, "yyyy-MM-ddTHH:mm:ss.fffZ");
                    //        }
                    //    });
                    //    return response;
                    //}
                });
            },
            SaveItemInfo: function () {
                var itemObj = AssetHelper.CreateItemList();
                var Originator = $("#<%= OriginatorString.ClientID %>").val();
                var param = { "Originator": Originator, "data": itemObj, "Token": Token };
                console.log(itemObj);

                var serviceURL = ROOT_PATH + "service/asset_management/asset_service.asmx/SaveAssignAssets";
                AssetManager.PostJson(serviceURL, param, onSuccess, onFail);
                function onSuccess(jsonData) {
                    //alert("Save Success");
                    var sucessMsg = GetSuccesfullMessageDiv("Asset Assign detail was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    //ClearControls();
                    closepopup();
                    CustomerAssetHelper.GenarateAssetGrid();
                    hideStatusDiv("MainContent_div_message");
                }
                function onFail(error) {
                    alert("Save Fail");
                }
            },
            ReadItemList: function () {
                var custCode = document.getElementById('txtCusCode').value;
                var param = { "userCode": custCode };

                var serviceURL = ROOT_PATH + "service/asset_management/asset_service.asmx/AssetsByUserCode";
                AssetManager.PostJson(serviceURL, param, onSuccess, onFail);
                function onSuccess(jsonData) {
                    console.log(jsonData.d.Data);
                    $("#assetgrid").data("kendoGrid").dataSource.data(jsonData.d.Data);
                }
                function onFail(error) {
                    alert("read Fail");
                }
            },
            PostJson: function (serviceUrl, jsonParam, successCallback, errorCallback) {
                jQuery.ajax({
                    url: serviceUrl,
                    async: false,
                    type: "POST",
                    data: JSON.stringify(jsonParam), /*"{" + jsonParam + "}",*/
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: successCallback,
                    error: errorCallback
                });
            }
        };

        var AssetHelper = {
            GenarateAssignAssetGrid: function () {
                $("#assetgrid").kendoGrid({
                    dataSource: AssetManager.gridDataSource(),
                    editable: false, // enable editing
                    pageable: false,
                    sortable: false,
                    filterable: false,
                    navigatable: true,
                    columns: AssetHelper.GenarateGridColumn()

                });
            },
            GenarateGridColumn: function () {

                var url = "javascript:EditAsset('#=AssetTypeName#','#=AssetCode#','#=RefundCost#','#=AssignDate#','#=ReturnDate#','#=Agreement#','#=Remark#','#=Status#','#=OtherStatus#');";

                var urlAssetTypeName = "<a href=\"" + url + "\">#=AssetTypeName#</a>";
                var urlAssetCode = "<a href=\"" + url + "\">#=AssetCode#</a>";
                var urlAssignDate = "<a href=\"" + url + "\">#=AssignDate#</a>";

                return columns = [
                    { template: urlAssetTypeName, field: "AssetTypeName", title: "Asset Type", filterable: false, width: "80px" },
                    { template: urlAssetCode, field: "AssetCode", title: "Code", filterable: false, width: "80px" },
                    { field: "RefundCost", title: "Refund Cost", filterable: false, width: "50px", hidden: true },
                    { field: "Status", title: "Status", filterable: false, width: "50px", hidden: true },
                    { template: "#= kendo.toString(kendo.parseDate(AssignDate, 'yyyy-MM-dd'), 'yyyy-MM-dd') #", field: "AssignDate", title: "Assign Date", filterable: false, width: "80px" },
                    { template: "#= kendo.toString(kendo.parseDate(ReturnDate, 'yyyy-MM-dd'), 'yyyy-MM-dd') #", field: "ReturnDate", title: "Return Date", filterable: false, width: "80px" },
                    { field: "Agreement", title: "Agreement", filterable: false, width: "50px", hidden: true },
                    { field: "Remark", title: "Remark", filterable: false, width: "50px", hidden: true },
                    { field: "Status", title: "Status", filterable: false, width: "60px" },
                    { field: "OtherStatus", title: "OtherStatus", filterable: false, width: "20px", hidden: true }
                ];
            },
            CreateItemList: function () {
                var gridData = $("#assetgrid").data("kendoGrid").dataSource.data();
                console.log(gridData);
                return gridData;
            }
        };

        //window assign asset table load - End
        function EditAsset(typeName, code, refundCost, assignDate, returnDate, agreement, remark, status, otherStatus) {
            var assigndt = kendo.toString(kendo.parseDate(assignDate, 'yyyy-MM-dd'), 'yyyy-MM-dd');
            var rtndt = kendo.toString(kendo.parseDate(returnDate, 'yyyy-MM-dd'), 'yyyy-MM-dd');

            $("#<%= ddlAssetType.ClientID %>").val(typeName);
            $("#<%= ddlAssetCode.ClientID %>").empty();

            LoadAssetsWithSetAssetCode(typeName, code);
            
            $("#<%= hdnSelectedId.ClientID %>").val(code);
            $("#<%= ddlAssetCode.ClientID %>").text(code);
            console.log(code + "+++++");
            console.log($("#<%= hdnSelectedId.ClientID %>").val());
            console.log($("#<%= hdnSelectedId.ClientID %>").text());

            $("#txtCost").val(refundCost);
            $("#<%= ddlStatus.ClientID %>").val(status);
            LoadOtherStatusWithSetOtherStatus(status, otherStatus);
            $("#txtAssignDate").val(assigndt);
            $("#txtReturnedDate").val(rtndt);
            $("#txtAgreement").val(agreement);
            $("#txtRemark").val(remark);
        }

        function LoadAssetsWithSetAssetCode(ddlAssetType, assetCode) {

            var type = document.getElementById('<%=ddlAssetType.ClientID%>').value;
            var params = "{'assetType':'" + type + "'}";

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/LoadAssets",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    var ddlAssetCode = $("[id*=ddlAssetCode]");
                    console.log(ddlAssetCode);
                    ddlAssetCode.empty().append($("<option></option>").val(0).text('- Select -'));
                    $.each(r.d, function () {
                        ddlAssetCode.append($("<option></option>").val(this['AssetCode']).text(this['AssetCode'] + " - " + this['SerialNo']));
                    });
                },
                error: function () {
                    alert("An error has occurred during processing your request.");
                }
            }).done(function () {
                $("#<%= ddlAssetCode.ClientID %>").val(assetCode);
            });
        }

        function LoadOtherStatusWithSetOtherStatus(ddlStatus, otherStatus) {
            var strStatus = document.getElementById('<%=ddlStatus.ClientID%>').value;

            //IF status = 3 only visible other status ddl and execute ajax
            if (strStatus == 3) {

                var params = "{'status':'" + strStatus + "'}";

                var tb = document.getElementById("trOther");
                tb.style.display = "table-row";

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/asset_management/asset_service.asmx/LoadOtherStatus",
                    data: params,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        var ddlUserCode = $("[id*=ddlOtherStatus]");
                        ddlUserCode.empty().append('<option selected="selected" value="0">- select -</option>');
                        $.each(r.d, function () {
                            ddlUserCode.append($("<option></option>").val(this['StatusId']).html(this['StatusName']));
                        });
                    },
                    error: function () {
                        //alert("An error has occurred during processing your request.");
                        var existsMsg = GetErrorMessageDiv("An error has occurred during processing your request.", "MainContent_div_message_popup");
                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        closepopupAsset();
                        hideStatusDiv("MainContent_div_message_popup");
                        return;
                    }
                }).done(function () {
                    $("#<%= ddlOtherStatus.ClientID %>").val(otherStatus);
                });;

            }
            else {
                var tb = document.getElementById("trOther");
                tb.style.display = "none";
            }
        }

        $("#buttonSaveAssetAssign").click(function () {
            var userType = 'CUST';//$("#<%= ddluserTypes.ClientID %>").val();
            var userCode = document.getElementById('txtCusCode').value;

           <%-- if (($("#<%= ddluserTypes.ClientID %>")[0].selectedIndex) == 0) {
                var sucessMsg = GetErrorMessageDiv("Please Select User Type.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }--%>

            if (userCode == null || userCode == 0) {
                var sucessMsg = GetErrorMessageDiv("Please Enter Outlet Code.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            var assetGrid = $("#assetgrid").data("kendoGrid");
            var assetdata = assetGrid.dataSource.data();
            var rowCount = $("#assetgrid").data("kendoGrid").dataSource.data().length;

            if (rowCount > 0) {
                CreateNewDataSource();
                AssetManager.SaveItemInfo();
                ClearControls();
            }
            else {
                var sucessMsg = GetErrorMessageDiv("Please assign assets to save.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

        });

        function deleteAssignedAsset(assignId, userCode, assetType) {

            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete '" + assetType + "' assign to '" + userCode + "' ?";

            var wnd = $("#AssetAssignmodalWindow").kendoWindow({
                title: "Delete Assign Asset",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_AssignConfirm_Message").text(message);
            wnd.center().open();

            $("#yes").click(function () {

                var root = 'asset_management/master/customer_asset.aspx?fm=deleteAssignAsset&type=delete&assignId=' + assignId;
                delete_asset(root);

            });

            $("#no").click(function () {
                closepopupAsset();
            });
        }

        function delete_asset(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;
            console.log("delete" + url);
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Assigned Asset was successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopupAsset();
                        CustomerAssetHelper.GenarateAssetGrid();
                        $("#div_loader").hide();
                        hideStatusDiv("MainContent_div_message");
                        return;
                    }
                    else {
                        var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        closepopupAsset();
                        hideStatusDiv("MainContent_div_message_popup");
                        return;
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        $("#btnAdd").on("click", function (e) {
            e.preventDefault();

            var userCode = document.getElementById('txtCusCode').value;

            if (userCode == null || userCode == 0) {
                var sucessMsg = GetErrorMessageDiv("Please Enter Outlet Code.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            addAsset();
        });

        $("#btnClear").click(function () {
            ClearAssetDetail();
        });

        $("#buttonClear").click(function () {
            ClearControls();
        });

        function LoadAssets(ddlAssetType) {

            var type = document.getElementById('<%=ddlAssetType.ClientID%>').value;
            var params = "{'assetType':'" + type + "'}";

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/LoadAssets",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    var ddlAssetCode = $("[id*=ddlAssetCode]");
                    ddlAssetCode.empty().append($("<option></option>").val(0).text('- Select -'));
                    $.each(r.d, function () {
                        ddlAssetCode.append($("<option></option>").val(this['AssetCode']).text(this['AssetCode'] + " - " + this['SerialNo']));
                    });
                },
                error: function () {
                    alert("An error has occurred during processing your request.");
                }
            });
        }

        function CheckAlreadyAssigned(ddlAssetCode) {
            var code = document.getElementById('<%=ddlAssetCode.ClientID%>').value;
            var cusCode = document.getElementById('txtCusCode').value;

            var params = "{'assetCode':'" + code + "',cusCode:'" + cusCode +"'}";
            console.log(params);
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/CheckAssetIsAvailable",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response.d.Status);
                    if (!response.d.Status) {
                        alert("Asset " + code + " Already assigned.");
                        $("#<%= ddlAssetCode.ClientID %>").val(0);
                    }

                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });

            if (code != 0) {
                CheckIsLostItem();
            }
        }

        function CheckIsLostItem() {
            var code = document.getElementById('<%=ddlAssetCode.ClientID%>').value;
            var cusCode = document.getElementById('txtCusCode').value;

            var params = "{'assetCode':'" + code + "',cusCode:'" + cusCode + "'}";
            console.log(params);
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/CheckIsLostItem",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response + " Lost/Damaged");
                    if (response.d.Status) {
                        alert("Asset " + code + " Damaged or Lost.");
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function addAsset() {

            var assetType = $("#<%= ddlAssetType.ClientID %>").val();
            var assetCode = $("#<%= ddlAssetCode.ClientID %>").val();
            var code = $("#<%= hdnSelectedId.ClientID %>").val();
            var assetStatus = $("#<%= ddlStatus.ClientID %>").val();

            console.log(assetCode + " Assset type log " + assetType + " " + code + " " + assetStatus);

            if (assetType == 0) {
                var sucessMsg = GetErrorMessageDiv("Please Select Asset Type.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (assetCode == 0) {
                var sucessMsg = GetErrorMessageDiv("Please Select Asset Code.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (($("#txtAssignDate").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Select Assign Date.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (assetStatus == 3) {

                var assetOtherStatus = $("#<%= ddlOtherStatus.ClientID %>").val();

                if (assetOtherStatus == 0) {
                    var sucessMsg = GetErrorMessageDiv("Please Select Reason Status.", "MainContent_div_message_popup");
                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(sucessMsg);
                    hideStatusDiv("MainContent_div_message_popup");
                    return;
                }
            }

            if (assetType != 0 && assetCode != 0 && $("#txtAssignDate").val() != '') {

                if (onAddValidateDuplicates()) {

                    var userType = $("#<%= ddluserTypes.ClientID %>").val();
                    var userCode = document.getElementById('txtCusCode').value;
                    var Originator = $("#<%= OriginatorString.ClientID %>").val();

                    var remark = document.getElementById('txtRemark').value;
                    var cost = document.getElementById('txtCost').value;
                        cost = (parseFloat(cost)).toFixed(2);
                    var status = $("#<%= ddlStatus.ClientID %>").val();
                    var otherStatus = $("#<%= ddlOtherStatus.ClientID %>").val();
                    var assignDate = document.getElementById('txtAssignDate').value;
                    var returnDate = document.getElementById('txtReturnedDate').value;
                    var agreement = document.getElementById('txtAgreement').value;

                    var assignId = $("#<%= hdnSelectedAssetAssignId.ClientID %>").val();
                    
                    $("#assetgrid").data("kendoGrid").dataSource.insert(0, {
                        AssetTypeName: assetType,
                        AssetCode: assetCode,
                        RefundCost: cost,
                        Status: status,
                        OtherStatus: otherStatus,
                        AssignDate: assignDate,
                        ReturnDate: returnDate,
                        Agreement: agreement,
                        //AssignId: 0,
                        UserCode: userCode,
                        Remark: remark,
                        UserType: userType
                    });

                    ClearAssetDetail();
                }
            }
        }

        function LoadOtherStatus(ddlStatus) {
            var strStatus = document.getElementById('<%=ddlStatus.ClientID%>').value;

            //IF status = 3 only visible other status ddl and execute ajax
            if (strStatus == 3) {

                var params = "{'status':'" + strStatus + "'}";

                var tb = document.getElementById("trOther");
                tb.style.display = "table-row";

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/asset_management/asset_service.asmx/LoadOtherStatus",
                    data: params,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        var ddlUserCode = $("[id*=ddlOtherStatus]");
                        ddlUserCode.empty().append('<option selected="selected" value="0">- select -</option>');
                        $.each(r.d, function () {
                            ddlUserCode.append($("<option></option>").val(this['StatusId']).html(this['StatusName']));
                        });
                    },
                    error: function () {
                        //alert("An error has occurred during processing your request.");
                        var existsMsg = GetErrorMessageDiv("An error has occurred during processing your request.", "MainContent_div_message_popup");
                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        closepopupAsset();
                        hideStatusDiv("MainContent_div_message_popup");
                        return;
                    }
                });

            }
            else {
                var tb = document.getElementById("trOther");
                tb.style.display = "none";
            }
        }

        function onAddValidateDuplicates() {

            var currentID = $("#<%= ddlAssetCode.ClientID %>").val();

            var ligrid = $("#assetgrid").data("kendoGrid");
            var data = ligrid.dataSource.data();
            var count = $('#assetgrid').data("kendoGrid").dataSource.total();
            var status = true;
            if (count == 0) {
                status = true;
            }

            $.each(data, function (i, row) {
                console.log(i, +" " + row + " " + status);
               <%-- $("#<%= hdnSelectedAssetAssignId.ClientID %>").val(row.AssignId);--%>
                if ($.trim(row.AssetCode) == currentID) {
                    console.log(i, +" " + row.AssetCode + " " + row.AssignId);
                    
                    if (confirm("Do you want to update '" + row.AssetCode + "' ?")) {
                        ligrid.dataSource.remove(row);
                        status = true;
                    } else {
                        ClearAssetDetail();
                        status = false;
                    }

                    //status = false;
                }
                
                
            });
            return status;
        }

        function CreateNewDataSource() {

            var ligrid = $("#assetgrid").data("kendoGrid");
            var data = ligrid.dataSource.data();
            var count = $('#assetgrid').data("kendoGrid").dataSource.total();
            
            $.each(data, function (i, row) {
                if ($.trim(row.AssignDate) && typeof row.AssignDate === "string") {
                    row.AssignDate = kendo.toString(kendo.parseDate(row.AssignDate, 'yyyy-MM-dd'), 'yyyy-MM-dd');
                }
                if (row.ReturnDate && typeof row.ReturnDate === "string") {
                    row.ReturnDate = kendo.toString(kendo.parseDate(row.ReturnDate, 'yyyy-MM-dd'), 'yyyy-MM-dd');
                }
                console.log(i, +" " + row + " " + status);
                var userType = $("#<%= ddluserTypes.ClientID %>").val();
                var userCode = document.getElementById('txtCusCode').value;
                var Originator = $("#<%= OriginatorString.ClientID %>").val();

                var assetType = $.trim(row.AssetTypeName);
                var assetCode = $.trim(row.AssetCode);
                var cost = $.trim(row.RefundCost);
                var status = $.trim(row.Status);
                if (status == 3) {
                    var otherStatus = $.trim(row.OtherStatus);
                }
                else {
                    var otherStatus = 0;
                }

                var assignDate = $.trim(row.AssignDate);
                var returnDate = $.trim(row.ReturnDate);
                var agreement = $.trim(row.Agreement);
                var remark = $.trim(row.Remark);

                ligrid.dataSource.remove(row);

                $("#assetgrid").data("kendoGrid").dataSource.insert(row, {
                    AssetTypeName: assetType,
                    AssetCode: assetCode,
                    RefundCost: cost,
                    Status: status,
                    OtherStatus: otherStatus,
                    AssignDate: assignDate,
                    ReturnDate: returnDate,
                    Agreement: agreement,
                    //AssignId: 0,
                    UserCode: userCode,
                    Remark: remark,
                    UserType: userType

                });

                console.log(i, +" " + row + " " + status + "/" + assetType + "/" + assetCode + "/" + cost + "/" + status + "/" + assignDate);
                console.log(i, +" " + returnDate + " " + agreement + "/" + remark );


            });
        }


        function GetCustomerName() {
            var custCode = document.getElementById('txtCusCode').value;
            //x.value = x.value.toUpperCase();

            var params = "{'custCode':'" + custCode + "'}";

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/LoadCustomerName",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d == null) {
                        var sucessMsg = GetErrorMessageDiv("Outlet code does not exist.", "MainContent_div_message_popup");
                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(sucessMsg);
                        hideStatusDiv("MainContent_div_message_popup");
                        $("#txtCusCode").val("");
                        $("#txtCusName").val("");
                        return;
                    } else {
                        $("#txtCusName").val(result.d);
                        AssetManager.ReadItemList();
                        onAddValidateDuplicates();
                    }

                },
                error: function () {
                    //alert("An error has occurred during processing customer code.");
                    var existsMsg = GetErrorMessageDiv("An error has occurred during processing your request.", "MainContent_div_message_popup");
                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    hideStatusDiv("MainContent_div_message_popup");
                    return;
                }
            });
        }

        function ClearAssetDetail() {
            $("#<%= ddlAssetType.ClientID %>").val(0);
            $("#<%= ddlAssetCode.ClientID %>").empty();
            $("#txtCost").val("");
            $("#<%= ddlStatus.ClientID %>").val(1);
            $("#txtAssignDate").val("");
            $("#txtReturnedDate").val("");
            $("#txtAgreement").val("");
            $("#txtRemark").val("");
            $("#<%= hdnSelectedAssetAssignId.ClientID %>").val(0);
            $("#<%= hdnSelectedId.ClientID %>").val("");

            var tb = document.getElementById("trOther");
            tb.style.display = "none"; 

        }

        function ClearControls() {
            ClearAssetDetail();
            $("#<%= ddluserTypes.ClientID %>").val(0);
            $("#txtCusName").val("");
            $("#txtCusCode").val("");
            $("#<%= hdnSelectedAssetAssignId.ClientID %>").val("");
            $("#assetgrid").data("kendoGrid").dataSource.data([]);
           
        }

        function closepopupAsset() {
            var wnd = $("#AssetAssignmodalWindow").kendoWindow({
                title: "Asset Assign",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#AssetWindow").css("display", "none");
        }

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Assign Asset",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }


    </script>
</asp:Content>
