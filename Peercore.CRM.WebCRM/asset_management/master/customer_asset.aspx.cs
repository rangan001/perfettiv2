﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class asset_management_master_customer_asset : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
            {
                if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                {
                    if (Request.QueryString["type"] == "delete")
                    {
                        if (Request.QueryString["fm"].ToLower() == "deleteassignasset")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteAssignAsset();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                    }
                }
            }
            if (!IsPostBack)
            {

            }
            OriginatorString.Value = UserSession.Instance.UserName;
            LoadAssetTypeDropdown();
            LoadUserTypes();

            Master.SetBreadCrumb("Asset Assign (Outlet) ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void excelUpload_Click(object sender, EventArgs e)
    {
        CommonUtility commonUtility = new CommonUtility();
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;

        try
        {
            if (xmlUpload.HasFile)
            {
                string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads";
                string logPath = Server.MapPath(ConfigurationManager.AppSettings["LogPath"]);
                string logUrl = ConfigurationManager.AppSettings["LogURL"];

                string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);
                if (!isValidType)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUpload.SaveAs(FilePath);
                    AssetsClient assetClient = new AssetsClient();
                    string UserName = UserSession.Instance.UserName;

                    if (assetClient.UploadAssetAssignData(FilePath, UserName, logPath, true))
                    {
                        xmlUpload.Attributes.Clear();
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Uploaded.");

                        string pattern = "*.txt";
                        var dirInfo = new DirectoryInfo(logPath);
                        var file = (from f in dirInfo.GetFiles(pattern) orderby f.LastWriteTime descending select f).First();
                        string logFilePath = logUrl + file;

                        linkLog.Visible = true;
                        linkLog.NavigateUrl = logFilePath;

                        //using (System.IO.StreamReader Reader = new System.IO.StreamReader(logFilePath))
                        //{
                        //    string fileContent = Reader.ReadToEnd();
                        //    if (!string.IsNullOrEmpty(fileContent))
                        //    {
                        //        string cleanMessage = fileContent.Replace("\r\n", " | ");
                        //        Page page = HttpContext.Current.CurrentHandler as Page;
                        //        string script = string.Format("alert('{0}');", cleanMessage);
                        //        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
                        //        {
                        //            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "alert", script, true /* addScriptTags */);
                        //        }
                        //    }

                        //}
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }
                    xmlUpload.Dispose();
                }
            }
            else
            {
                Response.Write("<script>alert('Select file to Upload!');</script>");
            }
        }
        catch (Exception)
        {

            throw;
        }

    }

    private void LoadAssetTypeDropdown()
    {
        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 50;
        args.OrderBy = "  asset_type_name asc";

        AssetsClient assetClient = new AssetsClient();
        List<AssetTypeModel> data = new List<AssetTypeModel>();
        data = assetClient.GetAllAssetTypes(args);

        if (data.Count != 0)
        {
            ddlAssetType.Items.Clear();
            ddlAssetType.Items.Add(new ListItem("- Select -", "0"));
            foreach (AssetTypeModel item in data)
            {
                ddlAssetType.Items.Add(new ListItem(item.AssetTypeId.ToString() + " - " + item.AssetTypeName, item.AssetTypeName));
            }
            ddlAssetType.Enabled = true;
        }
        else
        {
            ddlAssetType.Items.Add(new ListItem("- Select -", "0"));
            ddlAssetType.Enabled = false;
        }
    }

    private void LoadUserTypes()
    {
        string[] user_types = ConfigurationManager.AppSettings["Asset_User_Types"].Split(';');

        ddluserTypes.DataSource = user_types;
        ddluserTypes.DataBind();
        ddluserTypes.Items.Insert(0, new ListItem("- Select -", "NA"));
    }

    private string DeleteAssignAsset()
    {
        try
        {
            int Id = 0;
            AssetsClient assetService = new AssetsClient();

            if (Request.QueryString["assignId"] != null && Request.QueryString["assignId"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["assignId"].ToString());
            }

            bool status = assetService.DeleteAssignAsset(Id, UserSession.Instance.UserName);

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

}
