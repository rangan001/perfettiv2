﻿<%@ Page Title="mSales - Monthly bill payment upload" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="payment_upload.aspx.cs" Inherits="asset_management_payments_payment_upload" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />


    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 600px">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>

                </div>
            </div>
            <%--  Contente--%>
            <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Rep Code 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtRepCode" maxlength="30"  />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Payment Year 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input id="yearpicker" class="textboxwidth" data-role="datepicker" data-start="decade" data-depth="decade" data-format="yyyy" />
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Payment Month 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input id="monthpicker" class="textboxwidth" data-role="datepicker" data-start="year" data-depth="year" data-format="MMMM" />
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Payment Date 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="date" id="datepicker" class="textboxwidth" data-role="datepicker"  />
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Mobile Bill No
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtBillNo" maxlength="30"  placeholder="dd/mm/yyyy"/>
                                        </span>
                                </td>
                            </tr>
                            <tr>
                                <tr>
                                <td colspan="2" class="textalignbottom">Bill Amount
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtBillAmount" maxlength="30"  />
                                        </span>
                                </td>
                            </tr>
                            <tr>
                                <tr>
                                <tr>
                                <td colspan="2" class="textalignbottom">Allowance
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtAllowance" maxlength="30"  />
                                        </span>
                                </td>
                            </tr>
                            <tr>
                                <tr>
                                <td colspan="2" class="textalignbottom">Remark
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth"  id="txtRemark" maxlength="30"  />
                                       </span>
                                </td>
                            </tr>
                            <tr>
                            <tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                    <asp:HiddenField ID="hdnSelectedId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
        </div>
    </div>

    <div class="toolbar_container" id="id_div_toolbar_container">
        <div class="toolbar_left" id="div_content">
            <div class="hoributton">
                <div style="display: none" id="id_div_buttonBar">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>

                <asp:Button ID="excelUpload" runat="server" ToolTip="Upload" Text="Upload" CssClass="savebtn" OnClick="excelUpload_Click" />
            </div>

        </div>

        <div class="toolbar_right" id="div3" style="display:none;">
            <div class="leadentry_title_bar">
                <div style="float: right; width: 65%;" id="div_backButton">
                    <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back"></div>
                    </asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="formleft" style="width: 100%">
            <div class="formtextdiv">
                Select Excel File
            </div>
            <div class="formdetaildiv_right" style="line-height: 26px;">
                :
                    <div id="div_upload" runat="server" style="background-color: #FFFFFF; float: right; overflow: hidden;">
                        <asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
                        <%--<asp:Button Text="Review" runat="server" ID="btnReview" Style="float: left;" Visible="false" />--%>
                        <%--<asp:Button Text="Upload" runat="server" ID="excelUpload" Style="float: left;" OnClick="excelUpload_Click" />--%>
                        <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
                            ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red" Style="float: left;"></asp:RegularExpressionValidator>
                    </div>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
            </div>
            <div class="formdetaildiv_right">
                <a href="../../docs/templates/MobileBillandAllowances.xlsx">Sample Upload File</a>
            </div>
        </div>


        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
        <div style="float: left; width: 100%; overflow: auto">
            <div id="gridPayments"></div>
        </div>
    </div>

    </div>

    <div id="PaymentWindow" style="display: none">
        <div id="div_Confirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <script type="text/javascript"> 
        var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {
            LoadAllPayments();

        });

        function LoadAllPayments() {

            var url = "javascript:EditPayments('#=PaymentId#','#=RepCode#','#=PayYear#','#=PayMonthName#','#=PaymentDate#','#=BillNo#','#=BillAmount#','#=Allowance#','#=Remark#');";

            var urlPayId = "<a href=\"" + url + "\">#=PaymentId#</a>";
            var urlRepCode = "<a href=\"" + url + "\">#=RepCode#</a>";
            var urlPayYear = "<a href=\"" + url + "\">#=PayYear#</a>";
            var urlpayMonth = "<a href=\"" + url + "\">#=PayMonthName#</a>";
            var urlbillNo = "<a href=\"" + url + "\">#=BillNo#</a>";

            $("#gridPayments").html("");
            $("#gridPayments").kendoGrid({
                columns: [
                    { template: urlPayId,field: "PaymentId", hidden: true },
                    { template: urlRepCode,field: "RepCode", title: "Rep Code", width: "100px" },
                    { template: urlPayYear,field: "PayYear", title: "Year", width: "100px" },
                    { field: "PayMonth", title: "Month", width: "100px",hidden: true },
                    { template: urlpayMonth,field: "PayMonthName", title: "Month", width: "100px" },
                    { template: "#= kendo.toString(kendo.parseDate(PaymentDate, 'dd/MM/yyyy'), 'dd/MM/yyyy') #", field: "PaymentDate", title: "Payment Date", width: "150px" },
                    { template: urlbillNo,field: "BillNo", title: "Bill No", width: "150px" },
                    { field: "BillAmount", title: "Bill Amount", width: "150px" },
                    { field: "Allowance", title: "Allowance", width: "150px" },
                    { field: "Remark", title: "Remark", width: "200px" },
                    {
                        template: '<a href="javascript:deletePayment(\'#=PaymentId#\', \'#=RepCode#\', \'#=PaymentDate#\');">Delete</a>',
                        field: "",
                        width: "60px"
                    }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                navigatable: true,
                editable: "popup",
                dataBound: onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    batch: true,
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            fields: {
                                PaymentId: { type: "number" },
                                RepCode: { type: "string" },
                                PayYear: { type: "number" },
                                PayMonth: { type: "number" },
                                PaymentDate: { type: "date", format: "{0:MM/dd/yyyy}" },
                                BillNo: { type: "number" },
                                BillAmount: { type: "number" },
                                Allowance: { type: "number" },
                                Remark: { type: "string" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/asset_management/payment_service.asmx/GetAllPayments",
                            contentType: "application/json; charset=utf-8",
                            data: { pgindex: take_grid },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    var entityGrid = $("#gridPayments").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }

            });
        }

        function EditPayments(payId, repCode, payYear, payMonth, payDate, billNo, billAmount, allowance, remark) {
            console.log(payDate);
            //var paydt = kendo.toString(kendo.parseDate(payDate, 'dd/MM/yyyy'), 'dd/MM/yyyy');
            var paydt = (kendo.toString(new Date(payDate), 'MM/dd/yyyy'));
            console.log( "***" + paydt);
            $("#hdnSelectedId").val(payId);
            $("#txtRepCode").val(repCode);
            $("#yearpicker").val(payYear);
            $("#monthpicker").val(payMonth);
            $("#datepicker").val(paydt);
            $("#txtBillNo").val(billNo);
            $("#txtBillAmount").val(billAmount);
            $("#txtAllowance").val(allowance);
            $("#txtRemark").val(remark);

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Edit Payments",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            $("#txtRepCode").focus();

            wnd.center().open();
        }

        function deletePayment(paymentId, repCode) {
            $("#PaymentWindow").show()
            deletePaymentWindow(paymentId, repCode);
        }

        $("#buttonSave").click(function () {
            savePayment();
        });

        $("#buttonClear").click(function () {
            ClearControls();
        });

        function deletePaymentWindow(paymentId, repCode) {

            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete payment assign to '" + repCode + "' ?";

            var wnd = $("#PaymentWindow").kendoWindow({
                title: "Delete Payment",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_Confirm_Message").text(message);
            wnd.center().open();

            $("#yes").click(function () {

                var root = 'asset_management/Payments/payment_upload.aspx?fm=deletePayment&type=delete&paymentid=' + paymentId;
                delete_payment(root);

            });

            $("#no").click(function () {
                closepopup();
            });

        }

        function delete_payment(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;

            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Payment was successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopup();
                        LoadAllPayments();
                        hideStatusDiv("MainContent_div_message");
                        $("#div_loader").hide();
                        return;
                    }
                    else {
                        var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        closepopup();
                        hideStatusDiv("MainContent_div_message_popup");
                        return;
                    }
                },
                error: function (msg) {
                    if (msg == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function closepopup() {
            var wnd = $("#PaymentWindow").kendoWindow({
                title: "Payments",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#PaymentWindow").css("display", "none");
        }

        function closePaymentpopup() {
            var wnd = $("#window").kendoWindow({
                title: "Payments",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        $("#datepicker").kendoDatePicker();

        $("#yearpicker").kendoDatePicker({
            start: "decade",
            depth: "decade",
            format: "yyyy"
        });

        $("#monthpicker").kendoDatePicker({
            start: "year",
            depth: "year",
            format: "MMMM"
        });

        function savePayment() {
            var paymentId = $("#hdnSelectedId").val();

            if (($("#txtRepCode").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter Rep Code.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (($("#yearpicker").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Select Year.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (($("#monthpicker").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Select Month.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (($("#datepicker").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Select Payment Date.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if ($("#txtRepCode").val() != '' && $("#yearpicker").val() != '' && $("#monthpicker").val() != '' && $("#datepicker").val() != '' ){
                var Originator = $("#<%= OriginatorString.ClientID %>").val(); 
                var repCode = $("#txtRepCode").val();
                var payYear = $("#yearpicker").val();
                var payMonth = $("#monthpicker").val();
                var payDate = $("#datepicker").val();
                var billNo = $("#txtBillNo").val();
                var billAmount = $("#txtBillAmount").val();
                var allowance = $("#txtAllowance").val();
                var remark = $("#txtRemark").val();
                var payId = $("#hdnSelectedId").val();

                console.log(Originator, " ", repCode, " ", payMonth, " ", payDate, " ", billNo, " ", billAmount, " ", allowance, " ", remark, " ", payId);
               
                var param = {
                    "payId": payId, "repCode": repCode, "payYear": payYear, "payMonth": payMonth, "payDate": payDate, "billNo": billNo,
                    "billAmount": billAmount, "allowance": allowance, "remark": remark, "Originator": Originator
                };

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/asset_management/payment_service.asmx/SavePayment",
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var sucessMsg = GetSuccesfullMessageDiv("Payment was successfully Saved.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopup();
                        LoadAllPayments();
                        ClearControls();
                        closePaymentpopup();
                        hideStatusDiv("MainContent_div_message");

                    },
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                            $("#div_loader").hide();
                        }
                    }
                });
            }

        }

        function ClearControls() {
            $("#hdnSelectedId").val("");
            $("#txtRepCode").val("");
            $("#yearpicker").val("");
            $("#monthpicker").val("");
            $("#datepicker").val("");
            $("#txtBillNo").val("");
            $("#txtBillAmount").val("");
            $("#txtAllowance").val("");
            $("#txtRemark").val("");
        }

    </script>

</asp:Content>
