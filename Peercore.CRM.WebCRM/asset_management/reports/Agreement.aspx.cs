﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dashboard_transaction_Agreement : System.Web.UI.Page
{
    private ReportDocument repDoc = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            string repType = ddlAgreement.SelectedValue;
            string userCode = ddlUserCode.SelectedValue;

            Master.SetBreadCrumb("Agreement", "#", "");
            btnPrintReport.Click += new EventHandler(btnPrintReport_Click);

            //if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
            //{
            //    if (this.repDoc != null)
            //    {
            //        this.repDoc.Close();
            //        this.repDoc.Dispose();
            //    }

            //    this.repDoc = new ReportDocument();
            //    repDoc = GetReportDocument();
            //    SavePDF(Server.MapPath(GenerateFileName("Agreement", Session.SessionID.ToString())) + ".pdf", GenerateFileName("Agreement", Session.SessionID.ToString()) + ".pdf", repDoc);

            //}
            //this.repDoc = new ReportDocument();

            if (!IsPostBack)
            {
                //LoadUserTypes();
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        if (!IsPostBack)
        {
            LoadReps();
        }

    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void btnPrintReport_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("Agreement", Session.SessionID.ToString())) + ".pdf", GenerateFileName("Agreement", Session.SessionID.ToString()) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();
        return fn;
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        string repType = ddlAgreement.SelectedValue;
        string userCode = "";

        if(ddlUserType.SelectedValue == "CUST")
        {
            userCode = txtCustomer.Text;
        }
        else
        {
            userCode = ddlUserCode.SelectedValue;
        }
        
        if (repType == "mobile_agreement")
        {
            string mobileCode = ddlMobile.SelectedValue;
            string simCode = ddlSIM.SelectedValue;

            rptDoc.Load(Server.MapPath("agreement_mobile.rpt"));

            rptDoc.SetParameterValue("@mobileCode", mobileCode);
            rptDoc.SetParameterValue("@simCode", simCode);
            rptDoc.SetParameterValue("@printerCode", "");

        }
        else if (repType == "printer_agreement")
        {
            string printerCode = ddlPrinter.SelectedValue;

            rptDoc.Load(Server.MapPath("agreement_printer.rpt"));

            rptDoc.SetParameterValue("@mobileCode", "");
            rptDoc.SetParameterValue("@simCode", "");
            rptDoc.SetParameterValue("@printerCode", printerCode);
        }

        rptDoc.SetParameterValue("@userCode", userCode);

        //rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }


    protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadReps();
    }

    protected void ddlUserCode_SelectedIndexChanged(object sender, EventArgs e)
    {

        string agreement = ddlAgreement.SelectedValue;
        string user = ddlUserCode.SelectedValue;
        LoadAssets(agreement, user);
    }

    protected void btnLoadUserDetails_Click(object sender, EventArgs e)
    {
        string agreement = ddlAgreement.SelectedValue;
        string user = txtCustomer.Text;

        AssetDAO dao = new AssetDAO();
        //Clear();
        string name = dao.LoadCustomerName(user);
        if(name == null)
        {
            lblcusName.Text = "Outlet Not Found.";
        }
        else
        {
            lblcusName.Text = name;
            LoadAssets(agreement, user);
        }
        LoadAssets(agreement, user);

    }

    public void LoadAssets(string agreement,string user)
    {
        List<AssetModel> list = new List<AssetModel>();
        AssetDAO dao = new AssetDAO();


        list = dao.GetAssertsForAgreement(user, "Phone");
        ddlMobile.DataSource = list;
        ddlMobile.DataTextField = "SerialNo";
        ddlMobile.DataValueField = "AssetCode";
        ddlMobile.DataBind();
        ddlMobile.Items.Insert(0, new ListItem(" - Select - ", ""));

        list = dao.GetAssertsForAgreement(user, "SIM");
        ddlSIM.DataSource = list;
        ddlSIM.DataTextField = "SerialNo";
        ddlSIM.DataValueField = "AssetCode";
        ddlSIM.DataBind();
        ddlSIM.Items.Insert(0, new ListItem(" - Select - ", ""));

        list = dao.GetAssertsForAgreement(user, "Printer");
        ddlPrinter.DataSource = list;
        ddlPrinter.DataTextField = "SerialNo";
        ddlPrinter.DataValueField = "AssetCode";
        ddlPrinter.DataBind();
        ddlPrinter.Items.Insert(0, new ListItem(" - Select - ", ""));
    }

    public void Clear()
    {
        try
        {
            ddlSIM.Items.Clear();
            ddlMobile.Items.Clear();
            ddlPrinter.Items.Clear();
            lblcusName.Text = "";
            txtCustomer.Text = "";
            ddlUserCode.Items.Clear();
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void LoadReps()
    {
        try
        {
            Clear();
            List<OriginatorModel> list = new List<OriginatorModel>();
            AssetDAO dao = new AssetDAO();
            list = dao.GetUserCodesByUserType(ddlUserType.SelectedValue);
            ddlUserCode.DataSource = list;
            ddlUserCode.DataTextField = "Name";
            ddlUserCode.DataValueField = "Originator";
            ddlUserCode.DataBind();
            ddlUserCode.Items.Insert(0, new ListItem(" - Select User - ", string.Empty));
            lblReps.Text = ddlUserType.SelectedItem.Text + " Name";
        }
        catch (Exception)
        {
            throw;
        }
    }
}