﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class business_sales_process_forms_loadendusersalesinfo : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnAllRepsEnduser_Click(object sender, EventArgs e)
    {
        try
        {
            SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = null;
            Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER] = salesInfoDetSrcCritaria = LoadSalesDetailsPage();

            if (salesInfoDetSrcCritaria.showAllReps)
            {
                salesInfoDetSrcCritaria.DetailType = "rep";
                salesInfoDetSrcCritaria.RepCode = string.Empty;
                salesInfoDetSrcCritaria.RepDescription = string.Empty;
                //salesInfoDetSrcCritaria.RepCodes.Clear();
            }
            List<SalesInfoDetailBackDTO> backList = (List<SalesInfoDetailBackDTO>)Session[CommonUtility.SALES_INFO_DETAIL_BACK];

            backList.RemoveAll(c => c.Type == "E");

            backList.Add(new SalesInfoDetailBackDTO()
            {
                Id = 1,
                Code = "",
                Description = "",
                DetailType = salesInfoDetSrcCritaria.DetailType,
                Type = "E"
            });


            Session[CommonUtility.SALES_INFO_DETAIL_BACK] = backList;

            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "LoadTitle('rep', 'n', '0', 'T', '', 'gridEndUserSales', 'GetBusinessSalesEndUser');SetSalesInfoCss('rep', 'enduser');", true);
        }
        catch (Exception ex)
        {
        }
    }

    private SalesInfoDetailSearchCriteriaDTO LoadSalesDetailsPage()
    {
        int iCostYear = 0;
        string sMonthAbbr = "";
        bool bShowAllReps = false;
        SalesClient salesClient = new SalesClient();
        //brSales oSales = new brSales();

        //string repCode = (string)new brOriginator().GetRepCode(GlobalValues.GetInstance().FilterByUserName == "" ? GlobalValues.GetInstance().UserName : GlobalValues.GetInstance().FilterByUserName);
        string repCode = "";// UserSession.Instance.RepCode;// " WHERE originator = '" + UserSession.Instance.RepCode + "'";
        //int costPeriod = Convert.ToInt32(oSales.GetCurrentCostPeriod(ref iCostYear));

        EndUserSalesDTO endUserSalesEntity = salesClient.GetCurrentCostPeriod();
        int costPeriod = endUserSalesEntity.Period;
        iCostYear = endUserSalesEntity.Year;


        sMonthAbbr = salesClient.GetMonthForCostPeriod(iCostYear, costPeriod);
        // Quinn - 16-Nov-2012
        if (UserSession.Instance.UserName.ToLower() == "cpereira")
            bShowAllReps = true;

        SalesInfoDetailSearchCriteriaDTO dto = new SalesInfoDetailSearchCriteriaDTO()
        {
            

            //BackeryOption = 0,
            Brand = string.Empty,
            //BusinessArea = string.Empty,
            //BusinessAreaList = new List<string>() { "ALL" },
            //CatalogCode = string.Empty,
            //CatalogDescripion = string.Empty,
            //CatalogType = "F",
            //CategoryGroup = string.Empty,
            //CategorySubGroup = string.Empty,
            cDisplayOption = 'D',
            CustomerCode = string.Empty,
            CustomerDescription = string.Empty,
            //CustomerGroup = string.Empty,
            //CustomerSubGroup = string.Empty,
            //DetailType = "market",
            DetailType = "customer",
            DisplayFullName = true,
            iCostPeriod = costPeriod,
            Market = string.Empty,
            Month = sMonthAbbr,
            //ParentCustomerCode = string.Empty,
            //parentCustomerDescription = string.Empty,
            RepCode = repCode == null ? string.Empty : repCode,
            //RepCodes = UserSession.Instance.ch,
            //ChildReps = UserSession.Instance.UserName == "cpereira" ? "" : UserSession.Instance.ChildReps,
            RepDescription = UserSession.Instance.OriginalUserName,
            //RepDescription = "",
            sLastFlag = "n",
            // sLcSumTable = "rep_history_summary",
            SortField = "6",
            //State = string.Empty,
            //State = repCode.Substring(0, 1),
            //SubParentCustomerCode = string.Empty,
            // SubParentCustomerDescription = string.Empty,
            showAllReps = bShowAllReps,
            RepType = UserSession.Instance.RepType
        };

        return dto;
        //ucSalesInfoDetail salesInfoView = ViewAdapter.Instance.GetSalesInfoDetail(dto, GlobalValues.GetInstance().UserName);
        //saleDetailsContent.Content = salesInfoView;
    }
}