﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class business_sales_transaction_businesssales_enquiry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = null;
                //if (Session[CommonUtility.SALES_INFO_DETAIL] != null)
                //{
                //    salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];
                //}
                //else
                //{
                //    Session[CommonUtility.SALES_INFO_DETAIL] = salesInfoDetSrcCritaria = LoadSalesDetailsPage();
                //}
                Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER] = Session[CommonUtility.SALES_INFO_DETAIL] = salesInfoDetSrcCritaria = LoadSalesDetailsPage();

                if (salesInfoDetSrcCritaria.showAllReps)
                {
                    salesInfoDetSrcCritaria.DetailType = "rep";
                    salesInfoDetSrcCritaria.RepCode = string.Empty;
                    salesInfoDetSrcCritaria.RepDescription = string.Empty;
                    //salesInfoDetSrcCritaria.RepCodes.Clear();
                }
                List<SalesInfoDetailBackDTO> backList = new List<SalesInfoDetailBackDTO>();
                backList.Add(new SalesInfoDetailBackDTO()
                {
                    Id = 1,
                    Code = "",
                    Description = "",
                    DetailType = salesInfoDetSrcCritaria.DetailType,
                    Type = "C"
                });

                backList.Add(new SalesInfoDetailBackDTO()
                {
                    Id = 1,
                    Code = "",
                    Description = "",
                    DetailType = salesInfoDetSrcCritaria.DetailType,
                    Type = "E"
                });

                Session[CommonUtility.SALES_INFO_DETAIL_BACK] = backList;

                //Comment by indunil
                //LoadTitle(salesInfoDetSrcCritaria);

                InitializeUIData(salesInfoDetSrcCritaria);
                LoadViewState(salesInfoDetSrcCritaria);
            }
        }
    }

    private void LoadTitle(SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria)
    {
        string sEnquiryTitle = "";
        switch (salesInfoDetSrcCritaria.DetailType)
        {
            case "bus area":
                sEnquiryTitle = "Business Area";
                break;
            case "market":
                sEnquiryTitle = "Market";
                break;
            case "brand":
                sEnquiryTitle = "Brand";
                break;
            case "cat_group":
                sEnquiryTitle = "Catalogue Group";
                break;
            case "cat_sub_group":
                sEnquiryTitle = "Catalogue Sub Group";
                break;
            case "product":
                sEnquiryTitle = "Product";
                break;
            case "customer":
                //sEnquiryTitle = "Customer";
                sEnquiryTitle = "Distributor";
                break;
            case "custgroup":
                sEnquiryTitle = "Customer Group";
                break;
            case "rep":
                sEnquiryTitle = "Sales Rep.";
                break;
            case "repgroup":
                sEnquiryTitle = "Sales Reps.";
                break;
            case "state":
                sEnquiryTitle = "State";
                break;
            default:
                break;
        }

        SalesClient salesClient = new SalesClient();
        int iCostYear = 0;
        if (salesInfoDetSrcCritaria.sLastFlag.Equals("n"))
        {
            iCostYear = salesClient.GetCurrentCostPeriod().Year;
        }
        else
        {
            iCostYear = salesClient.GetCurrentCostPeriod().Year - 1;
        }

        List<string> lstAllColumnHeaders = new List<string>();
        List<string> lstColumnHeaders = new List<string>();

        lstAllColumnHeaders = salesClient.LoadCostPeriods(iCostYear);


        int i = lstAllColumnHeaders.IndexOf(salesInfoDetSrcCritaria.Month) - 5;

        if (i < 0) i += 12;

        for (int x = 0; x < 6; x++)
        {
            lstColumnHeaders.Add(lstAllColumnHeaders[i]);
            //lstColumnHeaders.Add(lstAllColumnHeaders[i]);
            //lstColumnHeaders.Add(lstAllColumnHeaders[i]);

            i++;
            if (i > 11) i = 0;
        }

        lstColumnHeaders.Add("Total");
        //lstColumnHeaders.Add("Total");
        //lstColumnHeaders.Add("Total");
        lstColumnHeaders.Add(sEnquiryTitle);
        //lstColumnHeaders.Add("This YTD");
        //lstColumnHeaders.Add("This YTD");
        lstColumnHeaders.Add("This YTD");
        //lstColumnHeaders.Add("Last YTD");
        //lstColumnHeaders.Add("Last YTD");
        lstColumnHeaders.Add("Last YTD");
        //lstColumnHeaders.Add("%");
        //lstColumnHeaders.Add("%");
        lstColumnHeaders.Add("%");
        lstColumnHeaders.Add((iCostYear - 1).ToString());
        //lstColumnHeaders.Add((iCostYear - 1).ToString());
        //lstColumnHeaders.Add((iCostYear - 1).ToString());
        lstColumnHeaders.Add((iCostYear - 2).ToString());
        //lstColumnHeaders.Add((iCostYear - 2).ToString());
        //lstColumnHeaders.Add((iCostYear - 2).ToString());


    }

    private SalesInfoDetailSearchCriteriaDTO LoadSalesDetailsPage()
    {
        int iCostYear = 0;
        string sMonthAbbr = "";
        bool bShowAllReps = false;
        SalesClient salesClient = new SalesClient();
        //brSales oSales = new brSales();

        //string repCode = (string)new brOriginator().GetRepCode(GlobalValues.GetInstance().FilterByUserName == "" ? GlobalValues.GetInstance().UserName : GlobalValues.GetInstance().FilterByUserName);
        string repCode = UserSession.Instance.RepCode;// " WHERE originator = '" + UserSession.Instance.RepCode + "'";
        //int costPeriod = Convert.ToInt32(oSales.GetCurrentCostPeriod(ref iCostYear));

        EndUserSalesDTO endUserSalesEntity = salesClient.GetCurrentCostPeriod();
        int costPeriod = endUserSalesEntity.Period;
        
        
        sMonthAbbr = salesClient.GetMonthForCostPeriod(iCostYear, costPeriod);
        // Quinn - 16-Nov-2012
        if (UserSession.Instance.UserName == "cpereira")
            bShowAllReps = true;

        SalesInfoDetailSearchCriteriaDTO dto = new SalesInfoDetailSearchCriteriaDTO()
        {

            //BackeryOption = 0,
            Brand = string.Empty,
            //BusinessArea = string.Empty,
            //BusinessAreaList = new List<string>() { "ALL" },
            //CatalogCode = string.Empty,
            //CatalogDescripion = string.Empty,
            //CatalogType = "F",
            //CategoryGroup = string.Empty,
            //CategorySubGroup = string.Empty,
            cDisplayOption = 'D',
            CustomerCode = string.Empty,
            CustomerDescription = string.Empty,
            //CustomerGroup = string.Empty,
            //CustomerSubGroup = string.Empty,
            //DetailType = "market",
            DetailType = "customer",
            DisplayFullName = true,
            iCostPeriod = costPeriod,
            Market = string.Empty,
            Month = sMonthAbbr,
            //ParentCustomerCode = string.Empty,
            //parentCustomerDescription = string.Empty,
            RepCode = repCode == null ? string.Empty : repCode,
            //RepCodes = UserSession.Instance.ch,
            //ChildReps = UserSession.Instance.UserName == "cpereira"? "" : UserSession.Instance.ChildReps,
             RepDescription = UserSession.Instance.OriginalUserName,
            //RepDescription = "",
            sLastFlag = "n",
            //sLcSumTable = "rep_history_summary",
            SortField = "6",
            //State = string.Empty,
            //State = repCode.Substring(0, 1),
            //SubParentCustomerCode = string.Empty,
            //SubParentCustomerDescription = string.Empty,
            showAllReps = bShowAllReps,
            RepType = UserSession.Instance.RepType
        };

        return dto;
        //ucSalesInfoDetail salesInfoView = ViewAdapter.Instance.GetSalesInfoDetail(dto, GlobalValues.GetInstance().UserName);
        //saleDetailsContent.Content = salesInfoView;
    }

    private void SetTitle(string detailType)
    {
        string headerText = "";
        switch (detailType)
        {
            case "bus area":
                headerText = "Business Sales Enquiry - Business Area";
                break;
            case "market":
                headerText = "Business Sales Enquiry - Market";
                break;
            case "brand":
                headerText = "Business Sales Enquiry - Brand";
                break;
            case "cat_group":
                headerText = "Business Sales Enquiry - Product Group";
                break;
            case "cat_sub_group":
                headerText = "Business Sales Enquiry - Product Sub Group";
                break;
            case "product":
                headerText = "Business Sales Enquiry - Product";
                break;
            case "customer":
                headerText = "Business Sales Enquiry - Customer";
                break;
            case "custgroup":
                headerText = "Business Sales Enquiry - Customer Group";
                break;
            case "rep":
                headerText = "Business Sales Enquiry - Sales Rep.";
                break;
            case "repgroup":
                headerText = "Business Sales Enquiry - Sales Reps.";
                break;
            case "state":
                headerText = "Business Sales Enquiry - State";
                break;
            default:
                headerText = "Business Sales Enquiry";
                break;
        }

        Master.SetBreadCrumb(headerText, "#", "");
    }

    private void InitializeUIData(SalesInfoDetailSearchCriteriaDTO oOriginalSalesInfoDetSrcCritaria)
    {
        switch (oOriginalSalesInfoDetSrcCritaria.DetailType)
        {
            case "bus area":
                DropDownGroupBy.SelectedIndex = 0;
                break;
            case "market":
                DropDownGroupBy.SelectedIndex = 1;
                break;
            case "brand":
                DropDownGroupBy.SelectedIndex = 2;
                break;
            case "cat_group":
                DropDownGroupBy.SelectedIndex = 3;
                break;
            case "cat_sub_group":
                DropDownGroupBy.SelectedIndex = 4;
                break;
            case "product":
                DropDownGroupBy.SelectedIndex = 5;
                break;
            case "customer":
                DropDownGroupBy.SelectedIndex = 6;
                break;
            case "custgroup":
                DropDownGroupBy.SelectedIndex = 7;
                break;
            case "repgroup":
                DropDownGroupBy.SelectedIndex = 8;
                break;
            case "rep":
                DropDownGroupBy.SelectedIndex = 9;
                break;
            case "state":
                DropDownGroupBy.SelectedIndex = 10;
                break;
        }
    }
    /*
    private SalesInfoDetailSearchCriteriaDTO GetSalesInfoDetailSearchCriteria(SalesInfoDetailSearchCriteriaDTO oOrgSrcCriteria)
    {
        try
        {
            SalesInfoDetailSearchCriteriaDTO sidSrcCriteria = new SalesInfoDetailSearchCriteriaDTO();
            sidSrcCriteria.Market = oOrgSrcCriteria.Market;
            // Lv_market_desc 	= IFNULL(market_desc,'');                    
            sidSrcCriteria.BusinessArea = oOrgSrcCriteria.BusinessArea;
            // Lv_busarea_desc 	= IFNULL(busarea_desc,'');
            sidSrcCriteria.Brand = oOrgSrcCriteria.Brand;
            // Lv_brand_desc 	= IFNULL(brand_desc,'');
            sidSrcCriteria.CategoryGroup = oOrgSrcCriteria.CategoryGroup;
            // Lv_cat_group_desc  = IFNULL(cat_group_desc,'');
            sidSrcCriteria.CategorySubGroup = oOrgSrcCriteria.CategorySubGroup;
            // Lv_cat_sub_group_desc  = IFNULL(cat_sub_group_desc,'');
            sidSrcCriteria.CatalogCode = oOrgSrcCriteria.CatalogCode;
            sidSrcCriteria.CatalogDescripion = oOrgSrcCriteria.CatalogDescripion;
            sidSrcCriteria.CustomerCode = oOrgSrcCriteria.CustomerCode;
            sidSrcCriteria.CustomerDescription = oOrgSrcCriteria.CustomerDescription;
            sidSrcCriteria.RepCode = oOrgSrcCriteria.RepCode;
            sidSrcCriteria.RepDescription = oOrgSrcCriteria.RepDescription;
            // Lv_state_desc 	 = IFNULL(state,'');
            sidSrcCriteria.State = oOrgSrcCriteria.State;
            sidSrcCriteria.ParentCustomerCode = oOrgSrcCriteria.ParentCustomerCode;
            sidSrcCriteria.parentCustomerDescription = oOrgSrcCriteria.parentCustomerDescription;
            // Lv_detail_type   = SQUEEZE(sub_detail);
            // code_desc = m_tbl[].description;
            sidSrcCriteria.sLastFlag = oOrgSrcCriteria.sLastFlag;
            sidSrcCriteria.Month = oOrgSrcCriteria.Month;
            sidSrcCriteria.iCostPeriod = oOrgSrcCriteria.iCostPeriod;
            sidSrcCriteria.cDisplayOption = (DropDownDisplayOption.SelectedIndex == 0 ? 'D' : (DropDownDisplayOption.SelectedIndex == 1 ? 'T' : 'U'));
            sidSrcCriteria.SortField = oOrgSrcCriteria.SortField;
            sidSrcCriteria.CatalogType = oOrgSrcCriteria.CatalogType;
            sidSrcCriteria.sLcSumTable = oOrgSrcCriteria.sLcSumTable;
            sidSrcCriteria.BackeryOption = oOrgSrcCriteria.BackeryOption;
            sidSrcCriteria.DisplayFullName = oOrgSrcCriteria.DisplayFullName;

            sidSrcCriteria.BusinessAreaList = new List<string>();

            if (rabrand.Checked)
                sidSrcCriteria.DetailType = "brand";
            else if (rabusarea.Checked)
                sidSrcCriteria.DetailType = "bus area";
            else if (racatgroup.Checked)
                sidSrcCriteria.DetailType = "cat_group";
            else if (racustomer.Checked )
                sidSrcCriteria.DetailType = "customer";
            else if (ramarket.Checked)
                sidSrcCriteria.DetailType = "market";
            else if (raproduct.Checked)
                sidSrcCriteria.DetailType = "product";
            else if (rarep.Checked )
                sidSrcCriteria.DetailType = "rep";
            else if (rasubgroup.Checked)
                sidSrcCriteria.DetailType = "cat_sub_group";

            //DataRow dtSelectedRow = ((DataRowView)lstSalesTbl.SelectedItem).Row;

            string commenCode = "";
            string commenDescription = "";

            if (oOrgSrcCriteria.DetailType.Equals("bus area"))
            {
                sidSrcCriteria.BusinessArea = commenCode;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("market"))
            {
                sidSrcCriteria.Market = commenCode;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("brand"))
            {
                sidSrcCriteria.Brand = commenCode;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("cat_group"))
            {
                sidSrcCriteria.CategoryGroup = commenCode;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("cat_sub_group"))
            {
                sidSrcCriteria.CategorySubGroup = commenCode;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("product"))
            {
                sidSrcCriteria.CatalogCode = commenCode;
                sidSrcCriteria.CatalogDescripion = commenDescription;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("customer"))
            {
                sidSrcCriteria.CustomerCode = commenCode;
                sidSrcCriteria.CustomerDescription = commenDescription;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("custgroup"))
            {
                if (string.IsNullOrEmpty(commenDescription))
                {
                    sidSrcCriteria.ParentCustomerCode = "";
                    sidSrcCriteria.parentCustomerDescription = "Customers With No Group";
                }
                else
                {
                    sidSrcCriteria.ParentCustomerCode = commenCode;
                    sidSrcCriteria.parentCustomerDescription = commenDescription;
                }
            }
            else if (oOrgSrcCriteria.DetailType.Equals("repgroup"))
            {
                sidSrcCriteria.RepCode = commenCode;
                sidSrcCriteria.RepDescription = commenDescription;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("rep"))
            {
                sidSrcCriteria.RepCode = commenCode;
                sidSrcCriteria.RepDescription = commenDescription;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("state"))
            {
                sidSrcCriteria.State = commenCode;
            }

            return sidSrcCriteria;
        }
        catch (Exception)
        {
            throw;
        }
    }
    */
    private void LoadViewState(SalesInfoDetailSearchCriteriaDTO oOrgSrcCriteria)
    {
        try
        {
            SalesClient salesClient = new SalesClient(); 
            SalesInfoDetailViewStateDTO oSalesInfoDetailViewState = new SalesInfoDetailViewStateDTO();

            oSalesInfoDetailViewState = salesClient.GetSalesInfoDetailViewState(oOrgSrcCriteria, new ArgsDTO(),"grid");
            
            txtSelection.Text = oSalesInfoDetailViewState.CodeDescription;

            string sFlowContent = "";

            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sMonth))
                sFlowContent += "Month : " + oSalesInfoDetailViewState.sMonth + " | ";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sProductType))
                sFlowContent += "Product Type : " + oSalesInfoDetailViewState.sProductType + " | ";
            //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sState))
            //    sFlowContent += "State : " + oSalesInfoDetailViewState.sState + " | ";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sRep))
                sFlowContent += "Rep : " + oSalesInfoDetailViewState.sRep + " | ";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sCustomer))
                sFlowContent += "Customer : " + oSalesInfoDetailViewState.sCustomer + " | ";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sProduct))
                sFlowContent += "Product : " + oSalesInfoDetailViewState.sProduct + " | ";
            //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sBusArea))
            //    sFlowContent += "Bus Area : " + oSalesInfoDetailViewState.sBusArea + " | ";
            //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sCustomerGroup))
            //    sFlowContent += "Customer Group : " + oSalesInfoDetailViewState.sCustomerGroup + " | ";
            //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sSubParent))
            //    sFlowContent += "Sub Parent : " + oSalesInfoDetailViewState.sSubParent + " | ";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sBrand))
                sFlowContent += "Brand : " + oSalesInfoDetailViewState.sBrand + " | ";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sMarket))
                sFlowContent += "Market : " + oSalesInfoDetailViewState.sMarket + " | ";
            //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sCatalogueGroup))
            //    sFlowContent += "Product Group : " + oSalesInfoDetailViewState.sCatalogueGroup + " | ";
            //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sCatalogueSubGroup))
            //    sFlowContent += "Product Sub Group : " + oSalesInfoDetailViewState.sCatalogueSubGroup;

            lblCurrentFlow.InnerText = sFlowContent;


            //rabrand.Checked = (oSalesInfoDetailViewState.sSubDetailType == "brand");
            //rabusarea.Checked = (oSalesInfoDetailViewState.sSubDetailType == "bus area");
            //racatgroup.Checked = (oSalesInfoDetailViewState.sSubDetailType == "cat_group");
            //racustomer.Checked = (oSalesInfoDetailViewState.sSubDetailType == "customer");
            //ramarket.Checked = (oSalesInfoDetailViewState.sSubDetailType == "market");
            //raproduct.Checked = (oSalesInfoDetailViewState.sSubDetailType == "product");
            //rarep.Checked = (oSalesInfoDetailViewState.sSubDetailType == "rep");
            //rasubgroup.Checked = (oSalesInfoDetailViewState.sSubDetailType == "cat_sub_group");

            DropDownDisplayOption.SelectedIndex = oSalesInfoDetailViewState.DisplayOptionIndex;
            DropDownListSort.SelectedIndex = oSalesInfoDetailViewState.SortFieldOptionIndex;

            //if (oOriginalSalesInfoDetSrcCritaria.DisplayFullName)
            //{
            //    ((GridViewDataColumn)lstSalesTbl.Columns[22]).DataMemberBinding = new Binding("MD");
            //}
            //else
            //{
            //    ((GridViewDataColumn)lstSalesTbl.Columns[22]).DataMemberBinding = new Binding("MC");
            //}

            //SortDataTable();
            //LoadGridView();
            //SetGridViewVisibleColumns();

        }
        catch (Exception)
        {
            throw;
        }
    }


    [System.Web.Services.WebMethod(EnableSession = true)]
    public static List<string> LoadTitle(string detailType, string sastFlag, string month)
    {
        string sEnquiryTitle = "";
        switch (detailType)
        {
            case "bus area":
                sEnquiryTitle = "Business Area";
                break;
            case "market":
                sEnquiryTitle = "Market";
                break;
            case "brand":
                sEnquiryTitle = "Brand";
                break;
            case "cat_group":
                sEnquiryTitle = "Catalogue Group";
                break;
            case "cat_sub_group":
                sEnquiryTitle = "Catalogue Sub Group";
                break;
            case "product":
                sEnquiryTitle = "Product";
                break;
            case "customer":
                sEnquiryTitle = "Customer";
                //sEnquiryTitle = "Distributor";
                break;
            case "custgroup":
                sEnquiryTitle = "Customer Group";
                break;
            case "rep":
                sEnquiryTitle = "Sales Rep.";
                break;
            case "repgroup":
                sEnquiryTitle = "Sales Reps.";
                break;
            case "state":
                sEnquiryTitle = "State";
                break;
            default:
                break;
        }

        SalesClient salesClient = new SalesClient();
        int iCostYear = 0;
        if (sastFlag.Equals("n"))
        {
            iCostYear = salesClient.GetCurrentCostPeriod().Year;
        }
        else
        {
            iCostYear = salesClient.GetCurrentCostPeriod().Year - 1;
        }

        List<string> lstAllColumnHeaders = new List<string>();
        List<string> lstColumnHeaders = new List<string>();

        lstAllColumnHeaders = salesClient.LoadCostPeriods(iCostYear);
        
        //int i = lstAllColumnHeaders.IndexOf(month) - 5;
        int i = lstAllColumnHeaders.IndexOf(DateTime.Now.ToString("MMM").ToUpper()) - 2;

        if (i < 0) i += 12;

        for (int x = 0; x < 3; x++)
        {
            lstColumnHeaders.Add(lstAllColumnHeaders[i]);
            //lstColumnHeaders.Add(lstAllColumnHeaders[i]);
            //lstColumnHeaders.Add(lstAllColumnHeaders[i]);

            i++;
            if (i > 11) i = 0;
        }

        lstColumnHeaders.Add("Total");
        //lstColumnHeaders.Add("Total");
        //lstColumnHeaders.Add("Total");
        lstColumnHeaders.Add(sEnquiryTitle);
        //lstColumnHeaders.Add("This YTD");
        //lstColumnHeaders.Add("This YTD");
        lstColumnHeaders.Add("This YTD");
        //lstColumnHeaders.Add("Last YTD");
        //lstColumnHeaders.Add("Last YTD");
        lstColumnHeaders.Add("Last YTD");
        //lstColumnHeaders.Add("%");
        //lstColumnHeaders.Add("%");
        lstColumnHeaders.Add("%");
        lstColumnHeaders.Add((iCostYear - 1).ToString());
        //lstColumnHeaders.Add((iCostYear - 1).ToString());
        //lstColumnHeaders.Add((iCostYear - 1).ToString());
        lstColumnHeaders.Add((iCostYear - 2).ToString());
        //lstColumnHeaders.Add((iCostYear - 2).ToString());
        //lstColumnHeaders.Add((iCostYear - 2).ToString());

        return lstColumnHeaders;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = null;
        //if (Session[CommonUtility.SALES_INFO_DETAIL] != null)
        //{
        //    salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];

        //}
        //else
        //{
        //    salesInfoDetSrcCritaria = LoadSalesDetailsPage();
        //}

        //Session[CommonUtility.SALES_INFO_DETAIL] = salesInfoDetSrcCritaria = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria);

        //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "LoadTitle(\"" + salesInfoDetSrcCritaria.DetailType + "\", \"n\", \"0\", \"D\");", true);
    }
}