﻿using Peercore.CRM.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class business_sales_transaction_db_claims : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
        buttonbar.VisibleSave(true);
        buttonbar.EnableSave(true);

        buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
        buttonbar.VisibleDeactivate(true);
        buttonbar.EnableDeactivate(true);

        Session.Remove(CommonUtility.BREADCRUMB);

        Master.SetBreadCrumb("Distributer Claims ", "#", "");
    }

    protected void ButtonSave_Click(object sender)
    {
        //SaveRepDetails();
    }

    protected void ButtonClear_Click(object sender)
    {
        //SaveRepDetails();
    }
}