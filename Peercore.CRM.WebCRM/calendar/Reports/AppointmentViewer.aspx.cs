﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
//using Global.TransferComponents;
using Peercore.CRM.Shared;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.DataAccess.datasets;
using CRMServiceReference;
using System.Configuration;
using Peercore.CRM.DataAccess;
using Peercore.CRM.Entities;

public partial class calendar_Reports_AppointmentViewer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["start"] != "" && Request.QueryString["end"] != "")
        {
            ReportDocument rptDoc = new ReportDocument();
            // Just set the name of data table
           // string a = Server.MapPath("AppointmentReport.rpt");
            //AppointmentReport rptDoc = new AppointmentReport();
            rptDoc.Load(Server.MapPath("AppointmentReport.rpt"));
            ArgsEntity args = new ArgsEntity();
            args.SStartDate = Request.QueryString["start"];
            args.SEndDate = Request.QueryString["end"];
            args.Originator = UserSession.Instance.UserName;
            args.OriginatorId = UserSession.Instance.OriginatorId;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            AppointmentDataSet ds = AppointmentBR.Instance.GetReportData(args);

            rptDoc.SetDataSource(ds);

           rptDoc.SetParameterValue(0, UserSession.Instance.OriginatorCookers);

            //ReportTransferComponent component = new ReportTransferComponent(rptDoc, "Appointment", "Appointment");

            // Your .rpt file path will be below
            //rptDoc.SetDatabaseLogon("sumuduf", "vdbs@123", "AppointmentDetail", "ctrust");
            //set dataset to the report viewer.
            CrystalReportViewer1.ReportSource = rptDoc;
        }
    }
}