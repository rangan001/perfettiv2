﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="appointment_entry.aspx.cs" Inherits="calendar_transaction_appointment_entry" %>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true"></telerik:RadScriptManager>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>


    <script type="text/javascript">

        function pageLoad(sender, eventArgs) {
            $("#ctl00_MainContent_RadScheduler_Form_div_color").hide();
            $("#ctl00_MainContent_RadScheduler_Form_div_originator").hide();
            //$('#ctl00_MainContent_radActivityPlanner_Form_div_color_type').hide();

            hideStatusDiv("MainContent_div_message");
        }

    </script>

    <div class="divcontectmainforms">
        <div style="background-color: #FFF; padding: 10px; overflow: auto">
            <table border="0">
                <tr>
                    <td>
                        <input id="startDate" runat="server" value="" style="width:100px;" />                        
                    </td>
                    <td>
                        <input id="endDate" runat="server" value="" style="width:100px;" />
                    </td>
                    <td>
                        <asp:Button ID="btnPrint" runat="server" Text="PRINT" OnClick="btnPrint_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnOpenActivity" runat="server" Text="OPEN ACTIVITY" OnClick="btnOpenActivity_Click" />
                    </td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Time Range :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="radTimeRang" AutoPostBack="true" OnSelectedIndexChanged="radTimeRang_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Territory :"></asp:Label>            
                    </td>
                    <td>
                        <asp:DropDownList ID="radRepTerritory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="radRepTerritory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div id="activityColor" runat="server">
        </div>

          <div class="clearall"></div>
        <div id="div_message" runat="server" style="display:block;"></div>
          <div class="clearall"></div>

        <div>
           
                <telerik:RadAjaxPanel runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                    <telerik:RadScheduler runat="server" ID="RadScheduler" DataKeyField="ID" DataSubjectField="Subject"
                        DataStartField="Start" DataEndField="End" DataDescriptionField="Description"
                        SelectedView="MonthView" FirstDayOfWeek="Sunday" LastDayOfWeek="Saturday" Skin="CookersSkin"
                        EnableEmbeddedSkins="false" OverflowBehavior="Expand" Culture="en-AU"

                        OnClientAppointmentInserting = "OnClientAppointmentInserting"
                        OnClientAppointmentEditing = "OnClientAppointmentInserting"
                        OnClientRequestSuccess="OnClientRequestSuccess"
                        OnClientAppointmentDeleting="OnClientAppointmentDeleting"
                        OnClientAppointmentContextMenuItemClicked="OnClientAppointmentContextMenuItemClicked"
                        OnClientTimeSlotContextMenuItemClicked="OnClientTimeSlotContextMenuItemClicked"
                        OnClientFormCreated="OnClientFormCreated"
                        
                        OnClientAppointmentMoveEnd = "OnClientAppointmentEndEditing"
                        OnClientAppointmentMoveStart = "OnClientAppointmentStartEditing"
                        OnClientAppointmentMoving = "OnClientAppointmentEditing"

                        ShowFullTime="false"
                        TimelineView-NumberOfSlots="7" DayStartTime="07:00:00" DayEndTime="00:00:00"
                        StartInsertingInAdvancedForm="true" StartEditingInAdvancedForm="true" 
                        DisplayRecurrenceActionDialogOnMove="true" 
                        MonthView-HeaderDateFormat="MMMM yyyy" WeekView-HeaderDateFormat="d MMMM yyyy" TimelineView-HeaderDateFormat="d MMMM yyyy">
                        <AdvancedForm Modal="false" />
                        <WebServiceSettings Path="../../service/calendar/calendar_service.asmx" 
                            GetAppointmentsMethod="GetAppointments" 
                            InsertAppointmentMethod="InsertAppointment"  
                            ResourcePopulationMode="ServerSide" >
               
            </WebServiceSettings>
            <%--<TimelineView GroupBy="ActivityTypes" GroupingDirection="Horizontal" />--%>
            
                        <TimeSlotContextMenuSettings EnableDefault="true" />
                <AppointmentContextMenuSettings EnableDefault="true" />
                        <ResourceStyles>		
			</ResourceStyles>

                        <AppointmentContextMenus>
                            <telerik:RadSchedulerContextMenu runat="server" ID="SchedulerAppointmentContextMenu">
                                <Items>
                                    <telerik:RadMenuItem Text="New Appointment" Value="CommandAddAppointment" />
                                    <telerik:RadMenuItem Text="Send to Outlook" Value="CommandSendtoOutlook" />
                                    <telerik:RadMenuItem Text="Open Activity" Value="CommandOpenActivity" />
                                </Items>
                            </telerik:RadSchedulerContextMenu>
                        </AppointmentContextMenus>
                        <TimeSlotContextMenus>
                            <telerik:RadSchedulerContextMenu runat="server" ID="SchedulerAppointmentTimeSlot">
                                <Items>
                                    <telerik:RadMenuItem Text="New Appointment" Value="CommandAddAppointment">
                                    </telerik:RadMenuItem>
                                </Items>
                            </telerik:RadSchedulerContextMenu>
                        </TimeSlotContextMenus>
                    </telerik:RadScheduler>
                    </telerik:RadAjaxPanel>
               <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
    </div>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            $(document).ready(function () {
                
                $("#MainContent_startDate").kendoDatePicker({
                    // display month and year in the input
                    format: "dd-MMM-yyyy"
                });
                $("#MainContent_endDate").kendoDatePicker({
                    // display month and year in the input
                    format: "dd-MMM-yyyy"
                });

                
            });


            function OnClientAppointmentDeleting(sender, eventArgs) {

                eventArgs.set_cancel(true);
                //alert(eventArgs.get_appointment().get_id());
                DeleteAppointment(eventArgs.get_appointment().get_id());
            }

            function DeleteActivity(id) {
                if (confirm("Do you want to delete the related activity for this appointment?")) {
                    var root = "../process_forms/processmaster.aspx?fm=activity&type=Delete&id=" + id;
                    $.ajax({
                        url: root,
                        dataType: "html",
                        cache: false,
                        success: function (msg) {
                            if (msg != "") {
                                $("#MainContent_div_message").html(msg);
                                document.getElementById('MainContent_div_message').style.display = "block";
                                hideStatusDiv("MainContent_div_message");
                                var scheduler = $find('<%=RadScheduler.ClientID %>');
                                scheduler.rebind();
                            }
                        },
                        // error: function (XMLHttpRequest, textStatus, errorThrown) {
                        error: function (msg, textStatus) {
                            if (textStatus == "error") {
                                var msg = msg;
                            }
                        }
                    });
                }
                else {
                    //window.location = "../../activity_planner/transaction/activity_scheduler.aspx?flag=cancel";
                }
            }

            function DeleteAppointment(id) {

                
                var root = "../process_forms/processmaster.aspx?fm=appointment&type=Delete&id=" + id;
                $.ajax({
                    url: root,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        if (msg != "") {
                            if (msg == "1") {
                                DeleteActivity(id);
                                $("#MainContent_div_message").html(msg);
                                document.getElementById('MainContent_div_message').style.display = "block";
                                hideStatusDiv("MainContent_div_message");
                                var scheduler = $find('<%=RadScheduler.ClientID %>');
                                scheduler.rebind();
                            }
                            else {
                                $("#MainContent_div_message").html(msg);
                                document.getElementById('MainContent_div_message').style.display = "block";
                                hideStatusDiv("MainContent_div_message");
                                var scheduler = $find('<%=RadScheduler.ClientID %>');
                                scheduler.rebind();
                            }
                        }
                    },
                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                        }
                    }
                });

            }


            function OnClientAppointmentContextMenuItemClicked(sender, args) {
                switch (args.get_item().get_value()) {
                    case "CommandOpenActivity":
                        var ActivityId = args.get_appointment().get_resources().getResourcesByType("ActivityId").getResource(0).get_text();
                        var LeadId = args.get_appointment().get_resources().getResourcesByType("LeadId").getResource(0).get_text();
                        var CustCode = args.get_appointment().get_resources().getResourcesByType("CustCode").getResource(0).get_text();
                        if (ActivityId != '0') {
                            if( CustCode != '')
                                window.location = "../../activity_planner/transaction/activityentry.aspx?fm=cal&activityid=" + ActivityId + "&custid=" + CustCode;
                            else
                                window.location = "../../activity_planner/transaction/activityentry.aspx?fm=cal&activityid=" + ActivityId + "&leadid=" + LeadId;
                        }
                        break;
                }

            }

            function OnClientTimeSlotContextMenuItemClicked(sender, args) {
                
                switch (args.get_item().get_value()) {
                    case "SendToCalendar":
                        
                        //SendToCalendar(args.get_appointment().get_id(), args.get_appointment().get_start(), args.get_appointment().get_end());
                        break;
                    case "OpenContact":
                        OpenContact(args.get_appointment().get_id());
                        break;
                }

            }

            


            function OnClientFormCreated(sender, args) {
                try {
                    var $ = $telerik.$;
                    var text = args.get_appointment().get_resources().getResourcesByType("ActivityTypes").getResource(0).get_text();
                    //alert(text);
                    GetColorCode1(text);

                }
                catch (err) {
                }
            }

            function GetColorCode1(ActivityCode) {
                var root = "../../activity_planner/process_forms/processmaster.aspx?fm=activityplanner&type=activityType&ActivityCode=" + ActivityCode + "&title=" + $("div.rsAdvTitle").html();

                $.ajax({
                    url: root,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        //var EditWrapper = 
                        $("div.rsAdvTitle").html(msg);
                        //                 EditWrapper =  EditWrapper + msg ; //  "</div><div id='div_title' style='background-color:#92d050'>"+text+"</div>";
                        //                 $("div.rsAdvTitle").html(EditWrapper);
                    },
                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg; // "Sorry there was an error: ";
                            //$("#" + tagertdiv).html(msg);
                        }
                    }
                });
            }

            function OnClientRequestSuccess(sender, args) {
                if (index == 1) {
                    index = 0;
                    var test = "<div id='messagegreen'  class='message-green' rel='sucess'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
                    test = test + "<tr><td class='green-left'>Successfully Saved.</td><td class='green-right'><a class='close-green'><img src='"
                            + ROOT_PATH + "assets/images/icon_close_green.gif' alt='' /></a></td></tr></table></div>";

                    //                    $("#MainContent_div_message").show("fast");
                    $("#MainContent_div_message").html(test);
                    document.getElementById('MainContent_div_message').style.display = "block";
                    hideStatusDiv("MainContent_div_message");
                }
            }
            var index = 0;
            function OnClientAppointmentInserting(sender, args) {
                index = 1;
                menuShown = false;
                $('#ctl00_MainContent_RadScheduler_SchedulerAppointmentContextMenu_detached').css("display", "none");
                $('#ctl00_MainContent_RadScheduler_SchedulerAppointmentTimeSlot_detached').css("display", "none");
            }

            function OnClientAppointmentEditing(sender, eventArgs) {
                menuShown = false;
                $('#ctl00_MainContent_RadScheduler_SchedulerAppointmentContextMenu_detached').css("display", "none");
                $('#ctl00_MainContent_RadScheduler_SchedulerAppointmentTimeSlot_detached').css("display", "none");
            }

            function OnClientAppointmentStartEditing(sender, eventArgs) {
                menuShown = false;
                $('#ctl00_MainContent_RadScheduler_SchedulerAppointmentContextMenu_detached').css("display", "none");
                $('#ctl00_MainContent_RadScheduler_SchedulerAppointmentTimeSlot_detached').css("display", "none");
            }

            function OnClientAppointmentEndEditing(sender, eventArgs) {
                var isyes = confirm("Do you want to reschedule Appointment?");
                if (!isyes)
                    eventArgs.set_cancel(true);
            }
        </script>
        <script type="text/javascript">

            //rcbHovered rcbItem

            $("#ctl00_MainContent_RadScheduler_AdvancedEditForm_ResActivityTypes_Input").change(function () {
                SetActivityTypesColor();
            });
            $("#ctl00_MainContent_RadScheduler_AdvancedInsertForm_ResActivityTypes_Input").change(function () {
                SetActivityTypesColor();
            });

            function SetActivityTypesColor() {
                var scheduler = $find('<%=RadScheduler.ClientID %>');

//                var text = args.get_appointment().get_resources().getResourcesByType("ActivityTypes").getResource(0).get_text();
//                alert(text);

//                var count = scheduler.get_resources().getResourcesByType("ActivityTypes").get_count();
//                alter(count);
                for (var i = 0; i < 50; i++) {
                    var key = scheduler.get_resources().getResourcesByType("ActivityTypes").getResource(i).get_key();
                    var text = scheduler.get_resources().getResourcesByType("ActivityTypes").getResource(i).get_text();
                    if ($("li.rcbHovered").html() == text) {
                        GetColorCode1(key);
                        break;
                    }
                }
                //alert($("li.rcbHovered").html());
            }
        </script>


        <script type="text/javascript" language="javascript">

            var lastArgs = null;
            var lastContext = null;
            var longTouchID = 0;
            var menuShown = false;

            function longTouch() {
                if (menuShown) {
                    menuShown = false;
                    return;
                }
                longTouchID = 0;
                menuShown = true;
                var scheduler = $find("<%= RadScheduler.ClientID %>");
                var eventArgs = null;
                var target = null;
                if (lastContext.target.nodeName == 'TD') {
                    target = scheduler._activeModel.getTimeSlotFromDomElement(lastContext.target);
                    eventArgs = new Telerik.Web.UI.SchedulerTimeSlotContextMenuEventArgs(target.get_startTime(), target.get_isAllDay(), lastContext, target);
                    scheduler._raiseTimeSlotContextMenu(eventArgs);
                } else {
                    try{
                        target = scheduler.getAppointmentFromDomElement(lastContext.target);
                        eventArgs = new Telerik.Web.UI.SchedulerAppointmentContextMenuEventArgs(target, lastContext);
                        scheduler._raiseAppointmentContextMenu(eventArgs);
                     }
                    catch (err) {
                        target = scheduler._activeModel.getTimeSlotFromDomElement(lastContext.target);
                        eventArgs = new Telerik.Web.UI.SchedulerTimeSlotContextMenuEventArgs(target.get_startTime(), target.get_isAllDay(), lastContext, target);
                        scheduler._raiseTimeSlotContextMenu(eventArgs);
                    }
                }
            }

            function handleTouchStart(e) {
                if (menuShown) {
                    menuShown = false;
                    return;
                }
                lastContext = e;
                lastContext.target = e.originalTarget;
                lastContext.pageX = e.changedTouches[0].pageX;
                lastContext.pageY = e.changedTouches[0].pageY;
                lastContext.clientX = e.changedTouches[0].clientX;
                lastContext.clientY = e.changedTouches[0].clientY;
                longTouchID = setTimeout(longTouch, 1000);
            }

            function handleClick(e) {
                if (menuShown) {
                    menuShown = false;
                    document.body.removeEventListener('click', handleClick, true);
                    e.stopPropagation();
                    e.preventDefault();
                }
            }

            function handleTouchEnd(e) {
                if (longTouchID != 0)
                    clearTimeout(longTouchID);

                if (menuShown) {
                    document.body.addEventListener('click', handleClick, true);
                    e.preventDefault();
                }
            }

            function pageLoad() {
                if ($telerik.isMobileSafari || $telerik.isAndroid) {
                    var scrollArea = $telerik.$('.rsContent', $get("<%= RadScheduler.ClientID %>"))[0];
                    scrollArea.addEventListener('touchstart', handleTouchStart, false);
                    scrollArea.addEventListener('touchend', handleTouchEnd, false);
                }
            }
            $("#ctl00_MainContent_RadScheduler div.rsHeader").css('z-index', '999');
    </script>
    </telerik:RadCodeBlock>
</asp:Content>
