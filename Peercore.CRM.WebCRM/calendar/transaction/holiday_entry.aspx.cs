﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using CRMServiceReference;
using System.Drawing;
using System.Text;
using System.Web.Services;

public partial class calendar_transaction_holiday_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClearSessions();

        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
        buttonbar.onButtonNext = new usercontrols_buttonbar_navigation.ButtonNext(buttonNext_Click);
        if (!IsPostBack)
        {
            //k RadScheduler.TimeZoneID = TimeZoneInfo.Local.Id;
            //Remove the session before setting the breadcrumb.
            Loadcmbdata();

            HiddenFieldOriginator.Value = UserSession.Instance.UserName;
            Session.Remove(CommonUtility.BREADCRUMB);
            buttonbar.VisiblePrevious(false);
            Master.SetBreadCrumb("Holiday Entry ", ConfigUtil.ApplicationPath + "Default.aspx", "");
        }

        if (Request.QueryString["frm"] != null && Request.QueryString["frm"] != string.Empty && UserSession.Instance.OriginatorString.Equals("ASE"))
        {
            string from = Request.QueryString["frm"];
            if (from.Equals("itinerary"))
            {
                //Show a Button (Update Route Planner (display default hidden))
                div_btn_update_route_planner.Visible = true;
                
            }            
        }

    }

    #region - Private Methods -


    protected void buttonNext_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "call_cycle/transaction/route_entry.aspx");
    }

    protected void btn_update_route_planner_Click(object sender, EventArgs e)
    {
        CommonUtility commonUtility = new CommonUtility();
        //When it is clicked
        //GetAddedHolidays for the current Month
        ArgsDTO args = new ArgsDTO();
        CommonClient commonClient = new CommonClient();
        ActivityClient activityClient = new ActivityClient();
        List<CalendarHolidayDTO> holidayDataList = new List<CalendarHolidayDTO>();
        args.SToday = string.IsNullOrEmpty(args.SToday) ? "" : args.SToday;

        //HiddenFieldNavigatedDate.Value
        DateTime today = !String.IsNullOrEmpty(HiddenFieldNavigatedDate.Value) ? Convert.ToDateTime(HiddenFieldNavigatedDate.Value): DateTime.Today;
        string firstOfMonth = new DateTime(today.Year, today.Month, 1).ToString("MM/dd/yyyy");
        string lastOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("MM/dd/yyyy");

        args.SStartDate = firstOfMonth;//startdate.ToString("yyyy/MM/dd");
        args.SEndDate = lastOfMonth;//enddate.ToString("yyyy/MM/dd");;
        args.Originator = UserSession.Instance.UserName;

        holidayDataList = commonClient.GetAllHolidays(args);

        bool isSuccess = false;
        if (holidayDataList.Count > 0)
        {
            List<ActivityDTO> activityList = new List<ActivityDTO>();
            ActivityDTO activityDTO = null;
            foreach (CalendarHolidayDTO item in holidayDataList)
            {
                activityDTO = new ActivityDTO();
                activityDTO.Status = "R";//to indicate as rejected
                activityDTO.StartDate = new DateTime(item.Date.Year, item.Date.Month, item.Date.Day);
                activityDTO.EndDate = new DateTime(item.Date.Year, item.Date.Month, item.Date.Day);
                activityDTO.LastModifiedBy = UserSession.Instance.UserName;

                activityList.Add(activityDTO);
            }
            //call UpdateActivityStatusForTME in activityService            
            int actId = activityClient.UpdateActivityStatusForTME(activityList);
            if (actId > 0)
                isSuccess = true;

            if (isSuccess)
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Route Planner Successfully Updated.");
            }
            else
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occurred !");
            }
        }
        //
    }

    private void ClearSessions()
    {
        Session["InsertLogAndSendMail"] = "";
        Session["holidayChanged"] = "";
        Session["fromDate"] = "";
        Session["toDate"] = "";
        Session["ActId"] = "";
    }



    private void Load_ASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;

        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (aseList.Count > 0)
            aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });
        else
            aseList.Insert(0, new OriginatorDTO() { Name = "---No ASM---", UserName = "NO" });

        SetAses(aseList, "UserName");
    }

    private void LoadDistributors(string selectedASE)
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        if (selectedASE.Equals("ALL"))
        {
            //args.AdditionalParams = " dept_string = 'DIST' ";
        }
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND parent_originator = '" + selectedASE + "' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();

        distList = originatorClient.GetOriginatorsByCatergory(args);
        if (distList.Count > 0)
            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });
        else
            distList.Insert(0, new OriginatorDTO() { Name = "---NO Distributors---", UserName = "NO" });

        SetDistributors(distList, "UserName");
    }

    private void LoadReps(string selectedDistributor)
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        args.AdditionalParams = " dept_string = 'DR'  AND parent_originator = '" + selectedDistributor + "' ";
        args.StartIndex = 1;
        args.RowCount = 1000;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();

        repList = originatorClient.GetOriginatorsByCatergory(args);
        if (repList.Count > 0)
            repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });
        else
            repList.Insert(0, new OriginatorDTO() { Name = "----No Reps---", UserName = "NO" });

        SetReps(repList, "UserName");
    }

    public void Loadcmbdata()
    {

        if (UserSession.Instance.OriginatorString == "ADMIN")
        {
            Load_ASEs();
        }
        else if (UserSession.Instance.OriginatorString == "ASE")
        {
            cmbase.Items.Clear();
            span_cmbase.Visible = true;
            cmbase.Items.Add(new ListItem { Text = UserSession.Instance.Originator, Value = UserSession.Instance.UserName });
            cmbase.Enabled = false;
            span_cmbdis.Visible = true;

            //Load Distributors
            string selectedASE = UserSession.Instance.OriginalUserName;
            LoadDistributors(selectedASE);
        }
        else if (UserSession.Instance.OriginatorString == "DIST")
        {
            cmbase.Items.Clear();
            cmbdis.Items.Clear();
            span_cmbase.Visible = true;
            cmbase.Items.Add(new ListItem { Text = "ASE_Name" });
            cmbase.Enabled = false;
            span_cmbdis.Visible = true;
            cmbdis.Items.Add(new ListItem { Text = UserSession.Instance.Originator, Value = UserSession.Instance.UserName });
            cmbdis.Enabled = false;
            span_cmbrep.Visible = true;

            ////Load Reps
            string selectedDistributor = UserSession.Instance.OriginalUserName;
            LoadReps(selectedDistributor);
        }
        else if (UserSession.Instance.OriginatorString == "DR")
        {
            cmbase.Items.Clear();
            cmbdis.Items.Clear();
            cmbrep.Items.Clear();
            span_cmbase.Visible = true;
            cmbase.Items.Add(new ListItem { Text = "ASE_Name" });
            cmbase.Enabled = false;
            span_cmbdis.Visible = true;
            cmbdis.Items.Add(new ListItem { Text = "Distributor_Name" });
            cmbdis.Enabled = false;
            span_cmbrep.Visible = true;
            cmbrep.Items.Add(new ListItem { Text = UserSession.Instance.Originator, Value = UserSession.Instance.UserName });
            cmbrep.Enabled = false;
        }
    }

    protected void cmboase_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            handle_cmbase();
        }
        catch (Exception ex)
        {
        }
    }

    protected void cmbodis_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            handle_cmbdis();
        }
        catch (Exception ex)
        {
        }
    }

    protected void cmborep_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            handle_cmbrep();
        }
        catch (Exception ex)
        {
        }
    }

    private void handle_cmbase()
    {
        if (cmbase.SelectedItem.Value.Equals("ALL"))
        {
            span_cmbase.Visible = true;
            span_cmbdis.Visible = false;
            span_cmbrep.Visible = false;
        }
        else if (cmbase.SelectedItem.Value.Equals("NO"))
        {
            span_cmbase.Visible = true;
            span_cmbdis.Visible = false;
            span_cmbrep.Visible = false;
        }

        else
        {
            span_cmbase.Visible = true;
            span_cmbdis.Visible = true;
            span_cmbrep.Visible = false;

            //Load Distributors
            string selectedASE = GetAsesValue();
            LoadDistributors(selectedASE);

            //load the calendar details for ASE
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "loadcalender('" + selectedASE + "')", true);

        }

    }

    private void handle_cmbdis()
    {
        if (cmbdis.SelectedItem.Value.Equals("ALL"))
        {
            span_cmbase.Visible = true;
            span_cmbdis.Visible = true;
            span_cmbrep.Visible = false;
        }

        else if (cmbdis.SelectedItem.Value.Equals("NO"))
        {
            span_cmbase.Visible = true;
            span_cmbdis.Visible = true;
            span_cmbrep.Visible = false;
        }
        else
        {
            span_cmbase.Visible = true;
            span_cmbdis.Visible = true;
            span_cmbrep.Visible = true;

            ////Load Reps
            string selectedDistributor = GetDistributorsValue();
            LoadReps(selectedDistributor);

            //load the calendar details for Distributor
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "loadcalender('" + selectedDistributor + "')", true);

        }

    }

    public void handle_cmbrep()
    {

        if (cmbrep.SelectedItem.Value.Equals("ALL") || cmbrep.SelectedItem.Value.Equals("no"))
        {

        }

        else
        {
            //load the calendar details for Rep
            string selectedrep = GetRepValue();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "loadcalender('" + selectedrep + "')", true);
        }

    }

    public void SetAses(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        cmbase.DataSource = listOriginator;
        cmbase.DataTextField = "Name";
        cmbase.DataValueField = valueField;
        cmbase.DataBind();
    }

    public void SetDistributors(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        cmbdis.DataSource = listOriginator;
        cmbdis.DataTextField = "Name";
        cmbdis.DataValueField = valueField;
        cmbdis.DataBind();
    }

    public void SetReps(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        cmbrep.DataSource = listOriginator;
        cmbrep.DataTextField = "Name";
        cmbrep.DataValueField = valueField;
        cmbrep.DataBind();
    }

    public string GetAsesValue()
    {
        if (cmbase.SelectedItem != null)
            return cmbase.SelectedItem.Value;
        else
            return "";
    }

    public string GetDistributorsValue()
    {
        if (cmbdis.SelectedItem != null)
            return cmbdis.SelectedItem.Value;
        else
            return "";
    }

    public string GetRepValue()
    {
        if (cmbrep.SelectedItem != null)
            return cmbrep.SelectedItem.Value;
        else
            return "";
    }


    #endregion
}