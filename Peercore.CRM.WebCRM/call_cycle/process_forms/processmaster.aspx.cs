﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Configuration;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.ServiceModel.Description;

public partial class call_cycle_process_forms_processmaster : PageBase
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "Update")
                {
                    if (Request.QueryString["fm"] == "leadCustomer")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string saveHeaderText = UpdateHeaderText();
                        //Response.ContentType = "text/html";
                        ////Response.Write(saveHeaderText);
                        //Response.End();
                    }

                }
                else if (Request.QueryString["type"] == "Delete")
                {
                    if (Request.QueryString["fm"] == "CallCycle")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteDocument = DeleteCallCycle(int.Parse(Request.QueryString["id"]));
                        Response.ContentType = "text/html";
                        Response.Write(deleteDocument);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "templateCallCycle")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteDocument = DeleteCallCycleItem();
                        Response.ContentType = "text/html";
                        Response.Write(deleteDocument);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "routesequence")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteDocument = DeleteRouteSequenceItem();
                        Response.ContentType = "text/html";
                        Response.Write(deleteDocument);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "visitchecklistmasterentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteDocument = DeleteVisitCheckListMasterItem(Request.QueryString["id"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(deleteDocument);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "routemasterentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteDocument = DeleteRouteMaster(Request.QueryString["id"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(deleteDocument);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "templateentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadScheduleTemplate = LoadScheduleTemplate();
                        Response.ContentType = "text/html";
                        Response.Write(loadScheduleTemplate);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "scheduleentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadScheduleTemplate = LoadScheduleTemplate();
                        Response.ContentType = "text/html";
                        Response.Write(loadScheduleTemplate);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "LeadCustomerentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = AddLeadCustomer();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "checkLeadCustomer")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = CheckLeadCustomer();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "checkLeadCustHeader")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = CheckLeadCustHeader();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "EndUser")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = AddEndUser();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "checkEndUser")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = CheckEndUser();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "checkEndUserHeader")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = CheckEndUserHeader();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "LeadStage")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = SetLeadStage();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "callcycdlg")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = LoadCallCycleDialog();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "AddSelectedOutlets")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = AddSelectedOutlets();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "get")
                {
                    if (Request.QueryString["fm"] == "templateentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadTemplate = LoadTemplates2();
                        Response.ContentType = "text/html";
                        Response.Write(loadTemplate);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "templatesession")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadTemplate = LoadTemplatesBySession();
                        Response.ContentType = "text/html";
                        Response.Write(loadTemplate);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "routesequence")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadTemplate = LoadRouteTemplates(Request.QueryString["repcode"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(loadTemplate);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "change")
                {
                    if (Request.QueryString["fm"] == "templateentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string setupTemplate = ChangeSession2();
                        Response.ContentType = "text/html";
                        Response.Write(setupTemplate);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "routesequence")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string setupTemplate = ChangeRouteSession();
                        Response.ContentType = "text/html";
                        Response.Write(setupTemplate);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "changeroute")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string setupTemplate = ChangeRoute();
                        Response.ContentType = "text/html";
                        Response.Write(setupTemplate);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "insert")
                {
                    if (Request.QueryString["fm"] == "routeentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveRouteMaster();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "visitchecklistmasterentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveVisitCheckListMaster();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }
            }
        }
    }
    #endregion

    #region Methods
    private string LoadScheduleTemplate()
    {
        StringBuilder txt = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        ArgsDTO args = new ArgsDTO();

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

        args.ChildOriginators = args.ChildOriginators.Replace("originator", "created_by");
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 50;//ConfigUtil.MaxRowCount;
        StringBuilder whereStr = new StringBuilder();
        whereStr.Append(args.ChildOriginators);
        whereStr.Append(" AND del_flag != 'Y'");

        List<LookupDTO> lookupList = commonClient.GetLookups("GetCallCycles", whereStr.ToString(), null, args, false);

        txt.Append("<table border=\"0\" id=\"scheduleTable\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
        txt.Append("<tr class=\"detail-popsheader\">");
        txt.Append("<th></th>");
        txt.Append("<th>Schedule ID</th>");
        txt.Append("<th>Schedule</th>");
        txt.Append("</tr>");
        for (int i = 0; i < lookupList.Count; i++)
        {
            txt.Append("<tr>");
            txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\">  </td>");
            txt.Append("<td align=\"left\" class=\"tblborder\" id=\"f\"><a href=\"javascript:OpenCallCycles('" + lookupList[i].Code + "','" + lookupList[i].Description + "')\"> " + lookupList[i].Code + "</a></td>");
            txt.Append("<td align=\"left\" class=\"tblborder\" id=\"d\">" + lookupList[i].Description + "</td>");
            txt.Append("</tr>");
        }
        txt.Append("</table>");

        //txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");

        txt.Append("<script type='text/javascript'>");



        txt.Append("</script>");

        return txt.ToString();
    }


    private string LoadTemplates2()
    {
        StringBuilder txt = new StringBuilder();

        StringBuilder sb = new StringBuilder();
        sb.Append("<div id=\"div_change_route\" class=\"route_pointer\" style=\"display: none\">");
        sb.Append("<div class=\"title\">Route</div>");
        sb.Append("<div>");
        sb.Append("<select id=\"cmdRoute\" class=\"dropdownwidth\">");

        CallCycleClient callCycleClient = new CallCycleClient();
        foreach (var operationDescription in callCycleClient.Endpoint.Contract.Operations)
        {
            DataContractSerializerOperationBehavior dcsob =
                operationDescription.Behaviors.Find<DataContractSerializerOperationBehavior>();
            if (dcsob != null)
            {
                dcsob.MaxItemsInObjectGraph = 2147483647;
            }
        }

        ArgsDTO args = new ArgsDTO();
        string callcycycleid = string.Empty;

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.Originator = UserSession.Instance.UserName;

        args.ChildOriginators = args.ChildOriginators.Replace("originator", "C.created_by");
        if (!String.IsNullOrEmpty(UserSession.Instance.RepCode))
            args.ChildOriginators = "(" + args.ChildOriginators + " OR c.rep_code =\'" + UserSession.Instance.RepCode + "\')";
        args.StartIndex = ConfigUtil.StartIndex;
        //args.RowCount = ConfigUtil.MaxRowCount;
        args.RowCount = 600;
        args.RepType = UserSession.Instance.RepType;
        args.ShowCount = 0;
        args.OrderBy = "description,lead_name asc";


        if (Request.QueryString["callcycid"] != null && Request.QueryString["callcycid"] != string.Empty)
        {
            callcycycleid = Request.QueryString["callcycid"].ToString();
        }

        List<CallCycleContactDTO> callcycleContactDtoList = callCycleClient.GetCallCycleContacts(args, int.Parse(callcycycleid), false, args.RepType);
        if (callcycleContactDtoList != null)
        {
            if (callcycleContactDtoList.Count > 0)
                args.RepCode = callcycleContactDtoList[0].PrefixCode;
        }


        List<RouteDTO> drRoutes = null;

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            if (string.IsNullOrEmpty(args.RepCode))
            {
                args.RepType = UserSession.Instance.OriginatorString;
                drRoutes = callCycleClient.GetRoutesForTME(args);
            }
            else
            {
                args.RepCode = callCycleClient.GetRepCodeForCallCycleId(Convert.ToInt32(callcycycleid));
                drRoutes = callCycleClient.GetRoutesByRepCode(args);
            }
        }
        else
        {
            args.RepCode = callCycleClient.GetRepCodeForCallCycleId(Convert.ToInt32(callcycycleid));
            drRoutes = callCycleClient.GetRoutesByRepCode(args);
        }

        txt.Append("<div style=\"width:" + (240 * drRoutes.Count) + "px;\">");

        #region - Method 2-

        ScheduleStruct schedul_struct = new ScheduleStruct();
        schedul_struct.DayCallCycleList = new List<DayCallCycleDTO>();
        schedul_struct.CallCycleId = int.Parse(callcycycleid);
        schedul_struct.DRRoutes = drRoutes;
        if (callcycleContactDtoList != null)
            if (callcycleContactDtoList.Count > 0)
                schedul_struct.RepCode = callcycleContactDtoList[0].PrefixCode;

        for (int i = 1; i <= drRoutes.Count; i++)
        {
            DayCallCycleDTO dayCallCycleDTO = new global::DayCallCycleDTO();
            List<CallCycleContactDTO> routeOutletList = null;// callcycleContactDtoList.FindAll(f => f.WeekDayId == (i)).OrderBy(s => s.DayOrder).ToList();

            //if (i == 1)
            //{
            //    routeOutletList = callcycleContactDtoList.FindAll(f => f.WeekDayId == 0 || f.WeekDayId == 1).OrderBy(s => s.DayOrder).ToList();

            //}
            //else
            //{
            //    routeOutletList = callcycleContactDtoList.FindAll(f => f.WeekDayId == (i)).OrderBy(s => s.DayOrder).ToList();
            //}

            routeOutletList = callcycleContactDtoList.FindAll(f => f.RouteId == (drRoutes[i - 1].RouteMasterId)).OrderBy(s => s.DayOrder).ToList();
            routeOutletList.ForEach(c => c.WeekDayId = i);

            sb.Append("<option value=\"" + i + "\">" + drRoutes[i - 1].RouteName + "</option>");

            dayCallCycleDTO.WeekDayId = i;
            if (routeOutletList == null)
            {
                dayCallCycleDTO.DayList = new List<CallCycleContactDTO>();
            }
            else
            {
                dayCallCycleDTO.DayList = routeOutletList;
            }
            schedul_struct.DayCallCycleList.Add(dayCallCycleDTO);
            if (i >= 1)
                txt.Append(LoadSectionTwo(routeOutletList, schedul_struct, i, drRoutes));
        }

        Session[CommonUtility.SCHEDULE_LOG] = schedul_struct;
        txt.Append("</div>");
        sb.Append("</select>");
        sb.Append("<a id=\"a_change_route\" class=\"changeroute\" style=\"display: inline-block;\">Change route</a>");
        sb.Append("</div>");
        sb.Append("<div>");

        sb.Append("</div>");
        sb.Append("</div>");///href=\"javascript:SaveChangeRouteItem()\" 

        txt.Append(sb.ToString());
        #endregion

        txt.Append("<script type='text/javascript'>");
        txt.Append("$(document).ready(function () {");

        if (callcycleContactDtoList != null)
        {
            if (callcycleContactDtoList.Count != 0)
            {
                txt.Append("$('#MainContent_txtScheduleTemplate').val('" + callcycleContactDtoList[0].CallCycle.Description.ToString() + "');");
            }
        }
        txt.Append("var sourceid='';");

        if (UserSession.Instance.OriginatorString == "ADMIN")
        {
            txt.Append("$('ul.droptrue').sortable({");
            txt.Append("   connectWith: 'ul'");
            txt.Append("});");
        }

        txt.Append("$('ul.droptrue').bind('sortstop', function (event, ui) {");

        // 17122013 - Kavisha
        ////txt.Append("alert('Source '+event.currentTarget.id);");
        ////txt.Append("alert('Target '+ui.item[0].parentNode.id);");
        ////txt.Append("alert('Item '+ui.item[0].attributes[0].nodeValue);");
        //txt.Append("var sortindex = 0; for(var i=0;i<ui.item[0].parentNode.children.length ;i++){  if(ui.item[0].id == ui.item[0].parentNode.children[i].id){ sortindex =i; break;  } }");
        //txt.Append("      changeTemplate('" + callcycycleid + "',ui.item[0].parentNode.id,event.currentTarget.id,ui.item[0].id,sortindex);");
        txt.Append("var sortindex = 0;");
        txt.Append("for(var i=0;i<ui.item[0].parentNode.children.length ;i++){  ");
        txt.Append("        if(ui.item[0].id == ui.item[0].parentNode.children[i].id){ ");
        txt.Append("            sortindex =i; ");
        txt.Append("            break;  ");
        txt.Append("        }");
        txt.Append("    }");
        //txt.Append("   alert(event.currentTarget.id);");
        txt.Append("    changeTemplate('" + callcycycleid + "',ui.item[0].parentNode.id,event.currentTarget.id,ui.item[0].id,sortindex);");

        txt.Append("});");


        txt.Append("$(\"#a_change_route\").click(function () {");
        txt.Append("    var SDayId = $('#hdfSDayId').val();");
        txt.Append("    var SSortId = $('#hdfSSortId').val();");
        txt.Append("    var TDayId = $('#cmdRoute').val();");
        txt.Append("    if (SDayId != TDayId)");
        txt.Append("        ChangeRoute(SDayId, SSortId, TDayId);");
        txt.Append("    $('#div_change_route').css('display', 'none');");
        txt.Append("    $('#hdfSDayId').val('0');");
        txt.Append("    $('#hdfSSortId').val('0');");
        txt.Append("});");

        txt.Append("$(\".routeicon\").click(function (e) {");
        txt.Append("    var o = {");
        txt.Append("        left: e.pageX,");
        txt.Append("        top: e.pageY");
        txt.Append("    };");
        txt.Append("    $(\"#div_change_route\").show(200).offset(o);");

        txt.Append("});");


        txt.Append("});");

        //txt.Append("function drag(target, e) {");
        //txt.Append("      e.dataTransfer.setData('Text', target.id);");
        //txt.Append("      sourceid=target.parentNode.id;");
        //txt.Append("}");

        //txt.Append("function drop(target, e) {");
        //txt.Append("      var id = e.dataTransfer.getData('Text');");
        //txt.Append("      target.appendChild(document.getElementById(id));");
        //txt.Append("      changeTemplate('" + callcycycleid + "',target.attributes[0].value,sourceid,id);");
        //txt.Append("      e.preventDefault();");
        //txt.Append("}");
        txt.Append("</script>");

        return txt.ToString();
    }

    private string LoadRestructureTemplates1(ScheduleStruct scheduleStructList)
    {
        StringBuilder txt = new StringBuilder();

        StringBuilder sb = new StringBuilder();
        sb.Append("<div id=\"div_change_route\" class=\"route_pointer\" style=\"display: none\">");
        sb.Append("<div class=\"title\">Route</div>");
        sb.Append("<div>");
        sb.Append("<select id=\"cmdRoute\" class=\"dropdownwidth\">");

        int y = 1;
        foreach (var item in scheduleStructList.DRRoutes)
        {
            sb.Append("<option value=\"" + y + "\">" + item.RouteName + "</option>");
            y++;
        }
        sb.Append("</select>");
        sb.Append("<a id=\"a_change_route\" class=\"changeroute\" style=\"display: inline-block;\">Change route</a>");
        sb.Append("</div>");
        sb.Append("<div>");

        sb.Append("</div>");
        sb.Append("</div>");///href=\"javascript:SaveChangeRouteItem()\" 

        txt.Append("<div style=\" width:" + (240 * scheduleStructList.DayCallCycleList.Count) + "px;\">");

        txt.Append(sb.ToString());
        foreach (var item in scheduleStructList.DayCallCycleList)
        {
            int i = 1;

            //txt.Append("<header>Day 1 </header>");
            if (item.DayList != null)
            {
                if ((item.DayList != null && item.DayList.Count != 0) || scheduleStructList.DRRoutes.Count >= 1)
                {

                    txt.Append("<div class='wrap'><div class='headline'>" + scheduleStructList.DRRoutes[item.WeekDayId - 1].RouteName.ToString() + "</div>");
                    txt.Append("<ul class=\"droptrue daycycle\" id=\"day" + item.WeekDayId + "\" >");

                    item.DayList = item.DayList.OrderBy(c => c.DayOrder).ToList();
                    foreach (CallCycleContactDTO item1 in item.DayList)
                    {
                        txt.Append("<li class=\"ui-state-default\" id=\"day" + item.WeekDayId + "_" + i.ToString() + "\">");

                        if (UserSession.Instance.OriginatorString == "ADMIN")
                        {
                            txt.Append("<a href=\"javascript:DeleteCallCycleItem(" + scheduleStructList.CallCycleId + "," + item.WeekDayId + "," + i.ToString() + ")\"> <div class='removeicon'></div> </a>");
                            //txt.Append("<a href=\"javascript:DeleteCallCycleItem(" + scheduleStructList.CallCycleId + "," + item.WeekDayId + "," + i.ToString() + ") style=\"display:none\" \"> <div class='removeicon'></div> </a>");
                        }
                        else
                        {
                            txt.Append("<a> <div class='removeicon'></div> </a>");
                        }

                        txt.Append("<a id=\"a_route\" href=\"javascript:ChangeCallCycleItem(" + item.WeekDayId + "," + i.ToString() + ")\"><div class='routeicon' style=\"float: right;\"></div> </a>");

                        txt.Append("<div>" + item1.Contact.Name.ToString() + "</div>");



                        txt.Append("</li>");
                        i++;
                    }

                    txt.Append("</ul></div>");
                }
                else
                {

                }
            }

        }

        txt.Append("</div>");

        txt.Append("<script type='text/javascript'>");
        txt.Append("$(document).ready(function () {");
        txt.Append("var sourceid='';");

        txt.Append("$('ul.droptrue').sortable({");
        txt.Append("   connectWith: 'ul'");
        txt.Append("});");

        txt.Append("$('ul.droptrue').bind('sortstop', function (event, ui) {");
        txt.Append("var sortindex = 0; for(var i=0;i<ui.item[0].parentNode.children.length ;i++){     if(ui.item[0].id == ui.item[0].parentNode.children[i].id){ sortindex =i; break;  } }");
        //17122013 - Kavisha
        //  txt.Append("      changeTemplate('" + scheduleStructList.CallCycleId + "',ui.item[0].parentNode.id,event.currentTarget.id,ui.item[0].attributes[0].nodeValue,sortindex);");
        txt.Append("      changeTemplate('" + scheduleStructList.CallCycleId + "',ui.item[0].parentNode.id,event.currentTarget.id,ui.item[0].id,sortindex);");
        txt.Append("});");


        txt.Append("$(\"#a_change_route\").click(function (e) {");
        txt.Append("    var SDayId = $('#hdfSDayId').val();");
        txt.Append("    var SSortId = $('#hdfSSortId').val();");
        txt.Append("    var TDayId = $('#cmdRoute').val();");
        txt.Append("    if (SDayId != TDayId)");
        txt.Append("        ChangeRoute(SDayId, SSortId, TDayId);");
        txt.Append("    $('#div_change_route').css('display', 'none');");
        txt.Append("    $('#hdfSDayId').val('0');");
        txt.Append("    $('#hdfSSortId').val('0');");
        txt.Append("});");

        txt.Append("$(\".routeicon\").click(function (e) {");
        txt.Append("    var o = {");
        txt.Append("        left: e.pageX,");
        txt.Append("        top: e.pageY");
        txt.Append("    };");
        txt.Append("    $(\"#div_change_route\").show(200).offset(o);");

        txt.Append("});");


        txt.Append("});");
        txt.Append("</script>");

        return txt.ToString();
    }

    private string LoadRestructureRouteTemplate(ScheduleStruct scheduleStructList)
    {
        StringBuilder txt = new StringBuilder();



        #region First Section
        int i = 1;

        //txt.Append("<header>Day 1 </header>");
        if ((scheduleStructList.DRRoutes.Count != 0 && scheduleStructList.DRRoutes != null))
        {

            txt.Append("<div class='wrap'><div class='headline'>Route Sequence</div>");
            txt.Append("<ul class=\"droptrue\" id=\"day1\" >");

            scheduleStructList.DRRoutes = scheduleStructList.DRRoutes.OrderBy(c => c.routeSequence).ToList();
            foreach (RouteDTO item1 in scheduleStructList.DRRoutes)
            {
                txt.Append("<li class=\"ui-state-default\" id=\"day1_" + i.ToString() + "\">");

                if (UserSession.Instance.OriginatorString == "ADMIN")
                {
                    txt.Append("<a href=\"javascript:DeleteRouteSequecneItem('" + scheduleStructList.RepCode + "',1," + item1.routeSequence + "," + item1.RouteMasterId + ")\"><div class='removeicon'></div> </a>");
                }
                else
                {
                    txt.Append("<a><div class='removeicon'></div> </a>");
                }

                txt.Append("<div>" + item1.RouteName.ToString() + "</div>");
                txt.Append("</li>");
                i++;
            }
        }
        else
        {

        }

        txt.Append("</ul></div>");
        #endregion


        txt.Append("<script type='text/javascript'>");
        txt.Append("$(document).ready(function () {");
        txt.Append("var sourceid='';");

        txt.Append("$('ul.droptrue').sortable({");
        txt.Append("   connectWith: 'ul'");
        txt.Append("});");

        txt.Append("$('ul.droptrue').bind('sortstop', function (event, ui) {");
        txt.Append("var sortindex = 0; for(var i=0;i<ui.item[0].parentNode.children.length ;i++){     if(ui.item[0].id == ui.item[0].parentNode.children[i].id){ sortindex =i; break;  } }");
        //17122013 - Kavisha
        //  txt.Append("      changeTemplate('" + scheduleStructList.CallCycleId + "',ui.item[0].parentNode.id,event.currentTarget.id,ui.item[0].attributes[0].nodeValue,sortindex);");
        txt.Append("      changeRouteTemplate('" + scheduleStructList.CallCycleId + "',ui.item[0].parentNode.id,event.currentTarget.id,ui.item[0].id,sortindex);");
        txt.Append("});");

        txt.Append("});");
        txt.Append("</script>");

        return txt.ToString();
    }

    private string ChangeSession2()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<div>ff</div>");
        ScheduleStruct schedul_struct = new ScheduleStruct();

        if (Session[CommonUtility.SCHEDULE_LOG] != null)
        {
            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];
            schedul_struct.HasChanged = true;

            string targetlist = "";
            string sourcelist = "";
            string clickitemindex = "";
            int clickedindex = 0;
            int sortindex = 0;

            if (Request.QueryString["tgtday"] != null && Request.QueryString["tgtday"] != string.Empty)
            {
                targetlist = Request.QueryString["tgtday"].ToString();
            }

            if (Request.QueryString["srcid"] != null && Request.QueryString["srcid"] != string.Empty)
            {
                sourcelist = Request.QueryString["srcid"].ToString();
            }

            if (Request.QueryString["cliindex"] != null && Request.QueryString["cliindex"] != string.Empty)
            {
                clickitemindex = Request.QueryString["cliindex"].ToString();
            }

            if (Request.QueryString["sortindex"] != null && Request.QueryString["sortindex"] != string.Empty)
            {
                sortindex = int.Parse(Request.QueryString["sortindex"].ToString());
                sortindex++;
            }


            sourcelist = sourcelist.Replace("day", "");
            clickitemindex = clickitemindex.Replace(Request.QueryString["srcid"].ToString() + "_", "");
            targetlist = targetlist.Replace("day", "");

            clickedindex = int.Parse(clickitemindex);

            CallCycleContactDTO addItem = new CallCycleContactDTO();

            DayCallCycleDTO sourceDayCallCycle = schedul_struct.DayCallCycleList.FirstOrDefault(c => c.WeekDayId.ToString() == sourcelist);
            DayCallCycleDTO targetDayCallCycle = schedul_struct.DayCallCycleList.FirstOrDefault(c => c.WeekDayId.ToString() == targetlist);

            if (sourcelist == targetlist)
            {
                sourceDayCallCycle.DayList = sourceDayCallCycle.DayList.OrderBy(c => c.DayOrder).ToList();
                addItem = sourceDayCallCycle.DayList[clickedindex - 1];
                //addItem = sourceDayCallCycle.DayList.FirstOrDefault(c => c.DayOrder == clickedindex);
                sourceDayCallCycle.DayList = RemoveSchedulStructTolist(sourceDayCallCycle.DayList, clickedindex - 1);
            }
            else
            {
                addItem = sourceDayCallCycle.DayList[clickedindex - 1];
                //addItem = sourceDayCallCycle.DayList.FirstOrDefault(c => c.DayOrder == clickedindex);
            }

            targetDayCallCycle.DayList = OrderSchedulStructTolist(targetDayCallCycle.DayList);
            targetDayCallCycle.DayList = AddSchedulStructTolist(targetDayCallCycle.DayList, sortindex, addItem);

            if (sourcelist != targetlist)
            {
                sourceDayCallCycle.DayList = RemoveSchedulStructTolist(sourceDayCallCycle.DayList, clickedindex - 1);
            }

            Session[CommonUtility.SCHEDULE_LOG] = schedul_struct;
        }



        return LoadRestructureTemplates1(schedul_struct);
    }

    private string ChangeRoute()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<div> </div>");
        ScheduleStruct schedul_struct = new ScheduleStruct();

        if (Session[CommonUtility.SCHEDULE_LOG] != null)
        {
            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];
            schedul_struct.HasChanged = true;

            int SDayId = 0;
            int SSortId = 0;
            int TDayId = 0;
            if (Request.QueryString["SDayId"] != null && Request.QueryString["SDayId"] != string.Empty)
            {
                SDayId = int.Parse(Request.QueryString["SDayId"].ToString());
            }

            if (Request.QueryString["SSortId"] != null && Request.QueryString["SSortId"] != string.Empty)
            {
                SSortId = int.Parse(Request.QueryString["SSortId"].ToString());
            }

            if (Request.QueryString["TDayId"] != null && Request.QueryString["TDayId"] != string.Empty)
            {
                TDayId = int.Parse(Request.QueryString["TDayId"].ToString());
            }




            CallCycleContactDTO addItem = new CallCycleContactDTO();

            DayCallCycleDTO sourceDayCallCycle = schedul_struct.DayCallCycleList.FirstOrDefault(c => c.WeekDayId == SDayId);
            DayCallCycleDTO targetDayCallCycle = schedul_struct.DayCallCycleList.FirstOrDefault(c => c.WeekDayId == TDayId);

            addItem = sourceDayCallCycle.DayList[SSortId - 1];
            //addItem = sourceDayCallCycle.DayList.FirstOrDefault(c=>c.DayOrder == SSortId );

            targetDayCallCycle.DayList = OrderSchedulStructTolist(targetDayCallCycle.DayList);
            targetDayCallCycle.DayList = AddSchedulStructTolist(targetDayCallCycle.DayList, targetDayCallCycle.DayList.Count + 1, addItem);

            if (SDayId != TDayId)
            {
                sourceDayCallCycle.DayList = RemoveSchedulStructTolist(sourceDayCallCycle.DayList, SSortId - 1);
            }


            Session[CommonUtility.SCHEDULE_LOG] = schedul_struct;
        }



        return LoadRestructureTemplates1(schedul_struct);
    }



    private string ChangeRouteSession()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<div>ff</div>");
        ScheduleStruct schedul_struct = new ScheduleStruct();

        if (Session[CommonUtility.SCHEDULE_LOG] != null)
        {
            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];
            schedul_struct.HasChanged = true;

            string targetlist = "";
            string sourcelist = "";
            string clickitemindex = "";
            int clickedindex = 0;
            int sortindex = 0;

            if (Request.QueryString["tgtday"] != null && Request.QueryString["tgtday"] != string.Empty)
            {
                targetlist = Request.QueryString["tgtday"].ToString();
            }

            if (Request.QueryString["srcid"] != null && Request.QueryString["srcid"] != string.Empty)
            {
                sourcelist = Request.QueryString["srcid"].ToString();
            }

            if (Request.QueryString["cliindex"] != null && Request.QueryString["cliindex"] != string.Empty)
            {
                clickitemindex = Request.QueryString["cliindex"].ToString();
            }

            if (Request.QueryString["sortindex"] != null && Request.QueryString["sortindex"] != string.Empty)
            {
                sortindex = int.Parse(Request.QueryString["sortindex"].ToString());
                sortindex++;
            }


            sourcelist = sourcelist.Replace("day", "");
            clickitemindex = clickitemindex.Replace(Request.QueryString["srcid"].ToString() + "_", "");
            targetlist = targetlist.Replace("day", "");

            clickedindex = int.Parse(clickitemindex);

            RouteDTO addItem = new RouteDTO();
            //Add to list.
            switch (targetlist)
            {
                case "1":
                    if (sourcelist == "1")
                    {
                        schedul_struct.DRRoutes = schedul_struct.DRRoutes.OrderBy(c => c.routeSequence).ToList();
                        addItem = schedul_struct.DRRoutes[clickedindex - 1];
                        schedul_struct.DRRoutes = RemoveRouteStructTolist(schedul_struct.DRRoutes, clickedindex - 1);
                    }

                    schedul_struct.DRRoutes = OrderRouteStructTolist(schedul_struct.DRRoutes);
                    schedul_struct.DRRoutes = AddSchedulStructTolist(schedul_struct.DRRoutes, sortindex, addItem);

                    break;

            }

            //Deduct from List.
            switch (sourcelist)
            {
                case "1":
                    if (targetlist != "1")
                        schedul_struct.DRRoutes = RemoveRouteStructTolist(schedul_struct.DRRoutes, clickedindex - 1);
                    //schedul_struct.DayList_1.RemoveAt(clickedindex-1);
                    break;

            }

            Session[CommonUtility.SCHEDULE_LOG] = schedul_struct;
        }



        return LoadRestructureRouteTemplate(schedul_struct);
    }

    public List<CallCycleContactDTO> AddSchedulStructTolist(List<CallCycleContactDTO> dayList, int sortindex, CallCycleContactDTO addItem)
    {
        foreach (CallCycleContactDTO item in dayList)
        {
            if (item.DayOrder >= sortindex)
            {
                item.DayOrder++;
            }
        }

        addItem.DayOrder = sortindex;
        dayList.Add(addItem);
        dayList = dayList.OrderBy(c => c.DayOrder).ToList();

        return dayList;
    }

    public List<RouteDTO> AddSchedulStructTolist(List<RouteDTO> routeList, int sortindex, RouteDTO addItem)
    {
        foreach (RouteDTO item in routeList)
        {
            if (item.routeSequence >= sortindex)
            {
                item.routeSequence++;
            }
        }

        addItem.routeSequence = sortindex;
        routeList.Add(addItem);
        routeList = routeList.OrderBy(c => c.routeSequence).ToList();

        return routeList;
    }

    public List<CallCycleContactDTO> RemoveSchedulStructTolist(List<CallCycleContactDTO> dayList, int index)
    {
        //CallCycleContactDTO addItem = schedul_struct.DayList_1[clickedindex - 1];
        //int id = addItem.DayOrder;
        //dayList = dayList.OrderBy(c=>c.DayOrder).ToList();
        dayList.RemoveAt(index);
        int dayOrder = 1;
        foreach (CallCycleContactDTO item in dayList)
        {
            //if (item.DayOrder > dayOrder)
            //{
            item.DayOrder = dayOrder;
            dayOrder++;
            //}
        }
        return dayList;
    }

    public List<RouteDTO> RemoveRouteStructTolist(List<RouteDTO> routeList, int index)
    {
        routeList.RemoveAt(index);
        int routeOrder = 1;
        foreach (RouteDTO item in routeList)
        {
            item.routeSequence = routeOrder;
            routeOrder++;
        }
        return routeList;
    }

    public List<CallCycleContactDTO> OrderSchedulStructTolist(List<CallCycleContactDTO> dayList)
    {
        int dayOrder = 1;
        dayList = dayList.OrderBy(c => c.DayOrder).ToList();
        foreach (CallCycleContactDTO item in dayList)
        {
            item.DayOrder = dayOrder;
            dayOrder++;
        }
        return dayList;
    }

    public List<RouteDTO> OrderRouteStructTolist(List<RouteDTO> routeList)
    {
        int routeOrder = 1;
        routeList = routeList.OrderBy(c => c.routeSequence).ToList();
        foreach (RouteDTO item in routeList)
        {
            item.routeSequence = routeOrder;
            routeOrder++;
        }
        return routeList;
    }

    //public List<CallCycleContactDTO> RemoveSchedulStructTolist(List<CallCycleContactDTO> dayList, CallCycleContactDTO addItem)
    //{
    //    //CallCycleContactDTO addItem = schedul_struct.DayList_1[clickedindex - 1];
    //    int dayOrder = addItem.DayOrder;
    //    dayList.Remove(addItem);
    //    foreach (CallCycleContactDTO item in dayList)
    //    {
    //        if (item.DayOrder > dayOrder)
    //        {
    //            item.DayOrder--;
    //        }
    //    }
    //    return dayList;
    //}

    public string DeleteCallCycle(int id)
    {
        StringBuilder txt = new StringBuilder();
        try
        {
            CallCycleContactDTO callCycleContactDTO = new CallCycleContactDTO();
            callCycleContactDTO.Contact = new LeadCustomerDTO();
            callCycleContactDTO.Contact.SourceId = int.Parse(Request.QueryString["leadid"]);
            callCycleContactDTO.Contact.CustomerCode = Request.QueryString["custcode"];
            callCycleContactDTO.Contact.EndUserCode = Request.QueryString["enduser"];

            DateTime DueOn = DateTime.Today;
            if (!string.IsNullOrEmpty(Request.QueryString["DueOn"]))
                DueOn = DateTime.Parse(Request.QueryString["DueOn"]);

            if (DueOn < DateTime.Today && Request.QueryString["isnew"] != "true")
            {
                txt.Append("Cannot remove Contact/Customer from Past Call Cycles.");
            }
            else
            {
                CallCycleClient op = new CallCycleClient();
                bool success = false;
                if (Request.QueryString["isnew"] != "true")
                {
                    success = op.CallCycleContactDelete(callCycleContactDTO, id);
                }
                else
                {
                    success = true;
                }

                List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
                listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
                if (listCallCycleContact == null)
                    listCallCycleContact = new List<CallCycleContactDTO>();
                callCycleContactDTO = listCallCycleContact.FirstOrDefault(c => c.Contact.CustomerCode == callCycleContactDTO.Contact.CustomerCode
                        && c.Contact.SourceId == callCycleContactDTO.Contact.SourceId
                        && c.Contact.EndUserCode == callCycleContactDTO.Contact.EndUserCode);
                listCallCycleContact.Remove(callCycleContactDTO);
                Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
                if (success)
                {
                    txt.Append("true");
                }
                else
                {
                    txt.Append("");
                }
            }

        }
        catch (Exception ex)
        {
        }
        return txt.ToString();
    }

    public string CheckLeadCustomer()
    {
        string leadStage = Request.QueryString["LeadStage"];
        string custCode = Server.HtmlDecode(Request.QueryString["CustomerCode"]);
        string leadId = Request.QueryString["LeadId"];
        string isChecked = Request.QueryString["isChecked"];
        List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
        listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
        if (listCallCycleContact == null)
            listCallCycleContact = new List<CallCycleContactDTO>();

        CallCycleContactDTO callCycleContact = new CallCycleContactDTO();

        if (isChecked == "1")
        {
            if (AddLeadCustomer() == "true")
                return "true";
            else
                return "false";
        }
        else
        {
            if (leadStage == "Customer")
            {
                callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == custCode);
            }
            else
            {
                callCycleContact = listCallCycleContact.Find(c => c.Contact.SourceId == int.Parse(leadId));
            }

            listCallCycleContact.Remove(callCycleContact);

            if (callCycleContact.rowCount > 1)
            {
                callCycleContact.rowCount = callCycleContact.rowCount - 1;
                listCallCycleContact.Add(callCycleContact);
            }

            Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
        }

        if (listCallCycleContact.Count > 0)
            return "true";
        else
            return "false";
    }

    public string AddLeadCustomer()
    {
        try
        {
            string leadStage = Request.QueryString["LeadStage"];
            string custCode = Server.HtmlDecode(Request.QueryString["CustomerCode"]);
            string leadId = Request.QueryString["LeadId"];

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();
            LeadCustomerDTO combinedList = null;
            //LeadDTO leaddto = null;
            LeadCustomerClient oLead = new LeadCustomerClient();
            CustomerClient oCustomer = new CustomerClient();

            ArgsDTO args = new ArgsDTO();
            args.Originator = UserSession.Instance.UserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.CustomerCode = custCode;
            //args.LeadId = int.Parse(leadId);
            CallCycleContactDTO callCycleContact = new CallCycleContactDTO();
            CallCycleContactDTO contact = new CallCycleContactDTO();
            if (leadStage == "Customer")
            {
                args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                args.RowCount = 10;
                args.StartIndex = 1;

                args.AdditionalParams = " cust_code = '" + custCode + "' ";
                combinedList = oLead.GetCombinedCollection(args).FirstOrDefault();

                //combinedList = oCustomer.GetCustomerView(args);
            }
            else
            {
                args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                args.RowCount = 10;
                args.StartIndex = 1;

                args.AdditionalParams = " lead_id = " + leadId;
                combinedList = oLead.GetCombinedCollection(args).FirstOrDefault();
                //leaddto = oLead.GetLead(leadId);

            }

            contact.Contact = new LeadCustomerDTO();
            contact.LeadStage = new LeadStageDTO();
            contact.Contact.SourceId = combinedList.SourceId;
            contact.Contact.CustomerCode = combinedList.CustomerCode;
            contact.Contact.EndUserCode = "";
            contact.Contact.Name = combinedList.Name;
            contact.Contact.Address = combinedList.Address;
            contact.Contact.City = combinedList.City;
            contact.Contact.State = combinedList.State;
            contact.Contact.PostalCode = combinedList.PostalCode;
            contact.CreateActivity = true;
            contact.rowCount = 1;

            if (combinedList.CustomerCode != null && combinedList.CustomerCode != "")
            {
                contact.Contact.LeadCustomerType = LeadCustomerTypes.Customer;
                contact.LeadStage.StageName = "Customer";
                callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == contact.Contact.CustomerCode);
            }
            else
            {
                contact.Contact.LeadCustomerType = LeadCustomerTypes.Lead;
                contact.LeadStage.StageName = combinedList.LeadStage;
                callCycleContact = listCallCycleContact.Find(c => c.Contact.SourceId == contact.Contact.SourceId);
            }

            if (callCycleContact == null)
            {
                //Check Customer exists in any other schedule - if exists return true
                //if (IsCallCycleContactInAnyOtherSchedule(contact.Contact.CustomerCode))
                //{
                //return "true2"; //along with a link to relevent call cycle ID
                //}

                listCallCycleContact.Add(contact);
                Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;

            }
            else
            {
                contact.rowCount = callCycleContact.rowCount + 1;
                listCallCycleContact.Remove(callCycleContact);
                listCallCycleContact.Add(contact);
                Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;

                return "true1";
            }


            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }

    }

    public string AddLeadCustomerHeader()
    {
        try
        {
            string leadstring = Request.QueryString["leadstring"];
            string custstring = Server.HtmlDecode(Request.QueryString["custstring"]);
            string isChecked = Request.QueryString["isChecked"];

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();
            List<LeadCustomerDTO> combinedList = new List<LeadCustomerDTO>();
            List<LeadCustomerDTO> leadList = new List<LeadCustomerDTO>();
            //LeadDTO leaddto = null;
            LeadCustomerClient oLead = new LeadCustomerClient();
            CustomerClient oCustomer = new CustomerClient();

            ArgsDTO args = new ArgsDTO();
            //args.Originator = UserSession.Instance.UserName;
            //args.ChildOriginators = UserSession.Instance.ChildOriginators;
            //args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.CustomerCode = custCode;
            //args.LeadId = int.Parse(leadId);

            CallCycleContactDTO callCycleContact = new CallCycleContactDTO();
            CallCycleContactDTO contact = new CallCycleContactDTO();
            //if (leadStage == "Customer")
            //{
            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.RowCount = 100;
            args.StartIndex = 1;

            if (!string.IsNullOrEmpty(custstring))
            {
                args.AdditionalParams = " cust_code in(  '" + custstring.Replace(",", "','") + "' ) ";// +" or lead_id in ( " + leadstring + ")";
                combinedList = oLead.GetCombinedCollection(args);
            }
            if (!string.IsNullOrEmpty(leadstring))
            {
                args.AdditionalParams = " lead_id in(  " + leadstring + " ) ";
                leadList = oLead.GetCombinedCollection(args);
            }
            combinedList.AddRange(leadList);
            //combinedList = oCustomer.GetCustomerView(args);
            //}
            //else
            //{
            //    args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            //    args.RowCount = 10;
            //    args.StartIndex = 1;

            //    args.AdditionalParams = " lead_id in ( " + leadstring +")";
            //    combinedList = oLead.GetCombinedCollection(args).FirstOrDefault();
            //    //leaddto = oLead.GetLead(leadId);

            //}
            int count = 0;
            foreach (LeadCustomerDTO item in combinedList)
            {
                contact = new CallCycleContactDTO();
                contact.Contact = new LeadCustomerDTO();
                contact.LeadStage = new LeadStageDTO();
                contact.Contact.SourceId = item.SourceId;
                contact.Contact.CustomerCode = item.CustomerCode;
                contact.Contact.EndUserCode = "";
                contact.Contact.Name = item.Name;
                contact.Contact.Address = item.Address;
                contact.Contact.City = item.City;
                contact.Contact.State = item.State;
                contact.Contact.PostalCode = item.PostalCode;
                contact.CreateActivity = true;
                contact.rowCount = 1;

                if (item.CustomerCode != null && item.CustomerCode != "")
                {
                    contact.Contact.LeadCustomerType = LeadCustomerTypes.Customer;
                    contact.LeadStage.StageName = "Customer";
                    callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == contact.Contact.CustomerCode);
                }
                else
                {
                    contact.Contact.LeadCustomerType = LeadCustomerTypes.Lead;
                    contact.LeadStage.StageName = item.LeadStage;
                    callCycleContact = listCallCycleContact.Find(c => c.Contact.SourceId == contact.Contact.SourceId);
                }


                if (callCycleContact == null)
                {
                    listCallCycleContact.Add(contact);
                    Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
                }
                else
                {
                    contact.rowCount = callCycleContact.rowCount + 1;
                    listCallCycleContact.Remove(callCycleContact);
                    listCallCycleContact.Add(contact);
                    Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;

                    //return "true";
                }
                count++;
            }
            return count > 0 ? "true" : "false";
        }
        catch (Exception ex)
        {
            return "false";
        }

    }

    public string CheckLeadCustHeader()
    {
        string leadstring = Request.QueryString["leadstring"];
        string custstring = Server.HtmlDecode(Request.QueryString["custstring"]);
        string isChecked = Request.QueryString["isChecked"];


        List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
        listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
        if (listCallCycleContact == null)
            listCallCycleContact = new List<CallCycleContactDTO>();

        CallCycleContactDTO callCycleContact = new CallCycleContactDTO();

        if (isChecked == "1")
        {
            if (AddLeadCustomerHeader() == "true")
                return "true";
            else
                return "false";
        }
        else
        {
            string[] custList = custstring.Split(',');
            foreach (string custCode in custList)
            {
                callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == custCode);

                listCallCycleContact.Remove(callCycleContact);

                if (callCycleContact != null && callCycleContact.rowCount > 1)
                {
                    callCycleContact.rowCount = callCycleContact.rowCount - 1;
                    listCallCycleContact.Add(callCycleContact);
                }

            }

            string[] leadList = leadstring.Split(',');
            foreach (string leadId in leadList)
            {
                if (string.IsNullOrEmpty(leadId))
                    continue;

                callCycleContact = listCallCycleContact.Find(c => c.Contact.SourceId == int.Parse(leadId));

                listCallCycleContact.Remove(callCycleContact);

                if (callCycleContact != null && callCycleContact.rowCount > 1)
                {
                    callCycleContact.rowCount = callCycleContact.rowCount - 1;
                    listCallCycleContact.Add(callCycleContact);
                }

            }

            Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
        }

        if (listCallCycleContact.Count > 0)
            return "true";
        else
            return "false";
    }

    public string CheckEndUser()
    {
        string custCode = Server.HtmlDecode(Request.QueryString["CustomerCode"]);
        string endUserCode = Server.HtmlDecode(Request.QueryString["EndUserCode"]);
        string isChecked = Request.QueryString["isChecked"];

        List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
        listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
        if (listCallCycleContact == null)
            listCallCycleContact = new List<CallCycleContactDTO>();

        CallCycleContactDTO callCycleContact = new CallCycleContactDTO();

        callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == custCode && c.Contact.EndUserCode == endUserCode);

        if (isChecked == "1")
        {
            if (AddEndUser() == "true1")
                return "true";
            else
                return "false";
        }
        else
        {
            listCallCycleContact.Remove(callCycleContact);
            Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
        }

        if (listCallCycleContact.Count > 0)
            return "true";
        else
            return "false";
    }

    public string AddEndUser()
    {
        try
        {
            string custCode = Server.HtmlDecode(Request.QueryString["CustomerCode"]);
            string endUserCode = Server.HtmlDecode(Request.QueryString["EndUserCode"]);


            EndUserDTO oEndUser = null;
            EndUserClient oBrEndUser = new EndUserClient();

            ArgsDTO args = new ArgsDTO();
            args.Originator = UserSession.Instance.UserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.CustomerCode = custCode;
            args.EnduserCode = endUserCode;


            oEndUser = oBrEndUser.GetEndUser(args);

            CallCycleContactDTO contact = new CallCycleContactDTO();

            contact.Contact = new LeadCustomerDTO();
            contact.LeadStage = new LeadStageDTO();
            contact.Contact.EndUserCode = oEndUser.EndUserCode;
            contact.Contact.CustomerCode = oEndUser.CustomerCode;
            contact.Contact.Name = oEndUser.Name;
            contact.Contact.Address = oEndUser.Address1;
            contact.Contact.City = oEndUser.City;
            contact.Contact.State = oEndUser.State;
            contact.Contact.PostalCode = oEndUser.PostCode;
            contact.Contact.LeadCustomerType = LeadCustomerTypes.EndUser;
            contact.LeadStage.StageName = "End User";
            contact.CreateActivity = true;

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();
            CallCycleContactDTO callCycleContact = new CallCycleContactDTO();
            callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == contact.Contact.CustomerCode && c.Contact.EndUserCode == contact.Contact.EndUserCode);

            if (callCycleContact == null)
            {
                listCallCycleContact.Add(contact);
                Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
            }
            else
            {
                return "true1";
            }

            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }

    }

    public string CheckEndUserHeader()
    {
        string enduserstring = Server.HtmlDecode(Request.QueryString["enduserstring"]);
        //string endUserCode = Server.HtmlDecode(Request.QueryString["EndUserCode"]);
        string isChecked = Request.QueryString["isChecked"];

        List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
        listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
        if (listCallCycleContact == null)
            listCallCycleContact = new List<CallCycleContactDTO>();

        CallCycleContactDTO callCycleContact = new CallCycleContactDTO();

        //callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == custCode && c.Contact.EndUserCode == endUserCode);

        if (isChecked == "1")
        {
            if (AddEndUserHeader() == "true")
                return "true";
            else
                return "false";
        }
        else
        {
            string[] leadList = enduserstring.Split(',');
            foreach (string endUserCode in leadList)
            {
                callCycleContact = listCallCycleContact.Find(c => c.Contact.EndUserCode == endUserCode);//c => c.Contact.CustomerCode == custCode && c
                //callCycleContact = listCallCycleContact.Find(c => c.Contact.SourceId == int.Parse(leadId));
                listCallCycleContact.Remove(callCycleContact);
            }

            Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
        }

        if (listCallCycleContact.Count > 0)
            return "true";
        else
            return "false";
    }

    public string AddEndUserHeader()
    {
        try
        {
            string enduserstring = Server.HtmlDecode(Request.QueryString["enduserstring"]);

            //EndUserDTO oEndUser = null;
            List<EndUserDTO> oEndUserList = new List<EndUserDTO>();
            EndUserClient oBrEndUser = new EndUserClient();

            //ArgsDTO args = new ArgsDTO();
            //args.Originator = UserSession.Instance.UserName;
            //args.ChildOriginators = UserSession.Instance.ChildOriginators;
            //args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.CustomerCode = custCode;
            //args.EnduserCode = endUserCode;


            oEndUserList = oBrEndUser.GetEndUsersByCode(enduserstring);//
            int rowcount = 0;
            foreach (EndUserDTO oEndUser in oEndUserList)
            {
                CallCycleContactDTO contact = new CallCycleContactDTO();

                contact.Contact = new LeadCustomerDTO();
                contact.LeadStage = new LeadStageDTO();
                contact.Contact.EndUserCode = oEndUser.EndUserCode;
                contact.Contact.CustomerCode = oEndUser.CustomerCode;
                contact.Contact.Name = oEndUser.Name;
                contact.Contact.Address = oEndUser.Address1;
                contact.Contact.City = oEndUser.City;
                contact.Contact.State = oEndUser.State;
                contact.Contact.PostalCode = oEndUser.PostCode;
                contact.Contact.LeadCustomerType = LeadCustomerTypes.EndUser;
                contact.LeadStage.StageName = "End User";
                contact.CreateActivity = true;

                List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
                listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
                if (listCallCycleContact == null)
                    listCallCycleContact = new List<CallCycleContactDTO>();
                CallCycleContactDTO callCycleContact = new CallCycleContactDTO();
                callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == contact.Contact.CustomerCode && c.Contact.EndUserCode == contact.Contact.EndUserCode);

                if (callCycleContact == null)
                {
                    listCallCycleContact.Add(contact);
                    Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
                    rowcount++;
                }
            }
            return rowcount > 0 ? "true" : "false";
        }
        catch (Exception ex)
        {
            return "false";
        }

    }

    public string SetLeadStage()
    {
        try
        {
            if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
            {
                ArgsDTO args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                args.LeadStage = Request.QueryString["LeadStage"];
            }
            //LeadStage
            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }

    }

    public string LoadCallCycleDialog()
    {
        StringBuilder txt = new StringBuilder();
        txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");
        return txt.ToString();
    }


    public string DeleteCallCycleItem()
    {
        StringBuilder sb = new StringBuilder();
        ScheduleStruct schedul_struct = new ScheduleStruct();
        CallCycleClient callCycleClient = new CallCycleClient();
        if (Session[CommonUtility.SCHEDULE_LOG] != null)
        {
            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];

            //string clickitemindex = "";
            int clickedindex = 0;
            int dayOrder = 0;
            int callcycid = 0;

            if (Request.QueryString["cliindex"] != null && Request.QueryString["cliindex"] != string.Empty)
            {
                clickedindex = int.Parse(Request.QueryString["cliindex"].ToString());
            }
            if (Request.QueryString["dayorder"] != null && Request.QueryString["dayorder"] != string.Empty)
            {
                dayOrder = int.Parse(Request.QueryString["dayorder"].ToString());
            }
            if (Request.QueryString["callcycid"] != null && Request.QueryString["callcycid"] != string.Empty)
            {
                callcycid = int.Parse(Request.QueryString["callcycid"].ToString());
            }


            CallCycleContactDTO addItem = new CallCycleContactDTO();
            bool isSuccess = false;

            DayCallCycleDTO sourceDayCallCycle = schedul_struct.DayCallCycleList.FirstOrDefault(c => c.WeekDayId == clickedindex);

            sourceDayCallCycle.DayList = sourceDayCallCycle.DayList.OrderBy(c => c.DayOrder).ToList();
            addItem = sourceDayCallCycle.DayList.Find(c => c.DayOrder == dayOrder);
            isSuccess = callCycleClient.CallCycleContactDelete(addItem, callcycid);
            if (isSuccess)
            {
                sourceDayCallCycle.DayList.Remove(addItem);
                sourceDayCallCycle.DayList = OrderSchedulStructTolist(sourceDayCallCycle.DayList);
            }

            //Add to list.
            //switch (clickedindex)
            //{
            //    case 1:
            //        schedul_struct.DayList_1 = schedul_struct.DayList_1.OrderBy(c => c.DayOrder).ToList();
            //        addItem = schedul_struct.DayList_1.Find(c=>c.DayOrder == dayOrder);
            //        isSuccess = callCycleClient.CallCycleContactDelete(addItem, callcycid);
            //        if (isSuccess)
            //        {
            //            schedul_struct.DayList_1.Remove(addItem);
            //            schedul_struct.DayList_1 = OrderSchedulStructTolist(schedul_struct.DayList_1);
            //        }
            //        break;
            //    case 2:
            //        schedul_struct.DayList_2 = schedul_struct.DayList_2.OrderBy(c => c.DayOrder).ToList();
            //        addItem = schedul_struct.DayList_2.Find(c=>c.DayOrder == dayOrder);
            //        isSuccess = callCycleClient.CallCycleContactDelete(addItem, callcycid);
            //        if (isSuccess)
            //        {
            //            schedul_struct.DayList_2.Remove(addItem);
            //            schedul_struct.DayList_2 = OrderSchedulStructTolist(schedul_struct.DayList_2);
            //        }
            //        break;
            //    case 3:
            //        schedul_struct.DayList_3 = schedul_struct.DayList_3.OrderBy(c => c.DayOrder).ToList();
            //        addItem = schedul_struct.DayList_3.Find(c=>c.DayOrder == dayOrder);
            //        isSuccess = callCycleClient.CallCycleContactDelete(addItem, callcycid);
            //        if (isSuccess)
            //        {
            //            schedul_struct.DayList_3.Remove(addItem);
            //            schedul_struct.DayList_3 = OrderSchedulStructTolist(schedul_struct.DayList_3);
            //        }
            //        break;
            //    case 4:
            //        schedul_struct.DayList_4 = schedul_struct.DayList_4.OrderBy(c => c.DayOrder).ToList();
            //        addItem = schedul_struct.DayList_4.Find(c=>c.DayOrder == dayOrder);
            //        isSuccess = callCycleClient.CallCycleContactDelete(addItem, callcycid);
            //        if (isSuccess)
            //        {
            //            schedul_struct.DayList_4.Remove(addItem);
            //            schedul_struct.DayList_4 = OrderSchedulStructTolist(schedul_struct.DayList_4);
            //        }
            //        break;
            //    case 5:
            //        schedul_struct.DayList_5 = schedul_struct.DayList_5.OrderBy(c => c.DayOrder).ToList();
            //        addItem = schedul_struct.DayList_5.Find(c=>c.DayOrder == dayOrder);
            //        isSuccess = callCycleClient.CallCycleContactDelete(addItem, callcycid);
            //        if (isSuccess)
            //        {
            //            schedul_struct.DayList_5.Remove(addItem);
            //            schedul_struct.DayList_5 = OrderSchedulStructTolist(schedul_struct.DayList_5);
            //        }
            //        break;
            //}

        }
        Session[CommonUtility.SCHEDULE_LOG] = schedul_struct;
        //return LoadRestructureRouteTemplate(schedul_struct);
        return LoadRestructureTemplates1(schedul_struct);
    }

    public string DeleteRouteSequenceItem()
    {
        StringBuilder sb = new StringBuilder();
        ScheduleStruct schedul_struct = new ScheduleStruct();
        CallCycleClient callCycleClient = new CallCycleClient();
        if (Session[CommonUtility.SCHEDULE_LOG] != null)
        {
            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];

            //string clickitemindex = "";
            int clickedindex = 0;
            int dayOrder = 0;
            int callcycid = 0;
            int routeMasterId = 0;
            string repCode = "";

            if (Request.QueryString["cliindex"] != null && Request.QueryString["cliindex"] != string.Empty)
            {
                clickedindex = int.Parse(Request.QueryString["cliindex"].ToString());
            }
            if (Request.QueryString["dayorder"] != null && Request.QueryString["dayorder"] != string.Empty)
            {
                dayOrder = int.Parse(Request.QueryString["dayorder"].ToString());
            }
            if (Request.QueryString["callcycid"] != null && Request.QueryString["callcycid"] != string.Empty)
            {
                callcycid = int.Parse(Request.QueryString["callcycid"].ToString());
            }
            if (Request.QueryString["routeMasterId"] != null && Request.QueryString["routeMasterId"] != string.Empty)
            {
                routeMasterId = int.Parse(Request.QueryString["routeMasterId"].ToString());
            }
            if (Request.QueryString["repCode"] != null && Request.QueryString["repCode"] != string.Empty)
            {
                repCode = Request.QueryString["repCode"].ToString();
            }


            RouteDTO addItem = new RouteDTO();
            bool isSuccess = false;
            //Add to list.
            switch (clickedindex)
            {
                case 1:
                    schedul_struct.DRRoutes = schedul_struct.DRRoutes.OrderBy(c => c.routeSequence).ToList();
                    addItem = schedul_struct.DRRoutes.Find(c => c.routeSequence == dayOrder);
                    if (!string.IsNullOrEmpty(UserSession.Instance.OriginatorString))
                    {
                        addItem.OriginatorType = UserSession.Instance.OriginatorString;
                    }

                    RouteMasterDTO routeMasterDTO = new RouteMasterDTO();
                    routeMasterDTO.RouteMasterId = routeMasterId;
                    routeMasterDTO.RepCode = repCode;
                    routeMasterDTO.LastModifiedBy = UserSession.Instance.UserName;

                    isSuccess = callCycleClient.DeleteRouteMaster(routeMasterDTO);
                    if (isSuccess)
                    {
                        schedul_struct.DRRoutes.Remove(addItem);
                        schedul_struct.DRRoutes = OrderRouteStructTolist(schedul_struct.DRRoutes);
                    }
                    break;
            }

        }
        Session[CommonUtility.SCHEDULE_LOG] = schedul_struct;

        //return LoadRestructureTemplates1(schedul_struct);
        return LoadRestructureRouteTemplate(schedul_struct);
    }

    public string LoadTemplatesBySession()
    {
        StringBuilder sb = new StringBuilder();
        ScheduleStruct schedul_struct = new ScheduleStruct();
        CallCycleClient callCycleClient = new CallCycleClient();
        if (Session[CommonUtility.SCHEDULE_LOG] != null)
        {
            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];
        }
        return LoadRestructureTemplates1(schedul_struct);
    }


    public string LoadSectionTwo(List<CallCycleContactDTO> Day2List, ScheduleStruct schedul_struct, int rindex, List<RouteDTO> drRoute)
    {
        StringBuilder txt = new StringBuilder();

        int i = 1;
        //txt.Append("<div class='wrap'><div class='headline'>Day 2</div>");
        txt.Append("<div class='wrap'><div class='headline'>");

        if (drRoute.Count != 0)
            txt.Append(drRoute[(rindex - 1)].RouteName);
        else
            txt.Append("Undefined route");

        txt.Append("</div>");
        txt.Append("<ul class=\"droptrue daycycle\" id=\"day" + rindex.ToString() + "\">");
        //txt.Append("<header>Day 2 </header>");

        if (Day2List.Count != 0)
        {
            foreach (CallCycleContactDTO item2 in Day2List.OrderBy(c => c.DayOrder).ToList())
            {
                //txt.Append("<div class=\"subdiv\" id=\"div_day" + item2.ItemIndex + "\" draggable=\"true\" ondragstart=\"drag(this, event)\">");
                txt.Append("<li class=\"ui-state-default\" id=\"day" + rindex.ToString() + "_" + i.ToString() + "\">");
                //txt.Append("<div class='removeicon'></div>");

                if (UserSession.Instance.OriginatorString == "ADMIN")
                {
                    txt.Append("<a href=\"javascript:DeleteCallCycleItem(" + schedul_struct.CallCycleId + "," + rindex + "," + i.ToString() + ")\"><div class='removeicon'></div> </a>");
                }
                else
                {
                    txt.Append("<a> <div class='removeicon'></div> </a>");
                }

                txt.Append("<a id=\"a_route\" href=\"javascript:ChangeCallCycleItem(" + rindex + "," + i.ToString() + ")\"><div class='routeicon' style=\"float: right;\"></div> </a>");
                txt.Append("<div>" + item2.Contact.Name.ToString() + "</div>");
                txt.Append("</li>");
                i++;
            }
        }
        else
        {
        }

        txt.Append("</ul>");
        txt.Append("</div>");
        i++;

        return txt.ToString();
    }


    private string LoadRouteTemplates(string repcode)
    {
        StringBuilder txt = new StringBuilder();

        try
        {
            CallCycleClient callCycleClient = new CallCycleClient();
            ArgsDTO args = new ArgsDTO();

            if (UserSession.Instance != null)
                args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.Originator = UserSession.Instance.UserName;

            args.ChildOriginators = args.ChildOriginators.Replace("originator", "C.created_by");
            args.StartIndex = ConfigUtil.StartIndex;
            args.RowCount = ConfigUtil.MaxRowCount;
            args.RepType = UserSession.Instance.RepType;
            args.ShowCount = 0;
            args.OrderBy = "description,lead_name asc";
            args.RepCode = repcode;
            args.RepType = UserSession.Instance.OriginatorString;

            List<RouteDTO> drRoutes = null;
            //if (args.RepType == "ASE")
            //{
            //    if (UserSession.Instance.UserName == repcode)
            //    {
            //        drRoutes = callCycleClient.GetRoutesForTME(args);
            //    }
            //    else
            //    {
            //        drRoutes = callCycleClient.GetRoutesByRepCode(args);
            //    }
            //}
            //else
            //{
            drRoutes = callCycleClient.GetRoutesByRepCode(args);
            //}

            if (drRoutes != null)
            {
                if (drRoutes.Count != 0)
                {
                    foreach (RouteDTO routedto in drRoutes)
                    {
                        routedto.OriginatorType = UserSession.Instance.OriginatorString;
                    }

                    //Check the Saved Items.
                    ScheduleStruct schedul_struct = new ScheduleStruct();
                    schedul_struct.DRRoutes = drRoutes;
                    schedul_struct.RepCode = repcode;
                    schedul_struct.OriginatorType = UserSession.Instance.OriginatorString;

                    #region Setup Session

                    Session[CommonUtility.SCHEDULE_LOG] = schedul_struct;

                    #endregion

                    int i = 1;

                    #region First Section

                    //      txt.Append("<div class='wrap'><div class='headline'>Day 1</div>");  
                    txt.Append("<div class='wrap'><div class='headline'>");

                    txt.Append("Route Sequence");
                    txt.Append("</div>");
                    txt.Append("<ul class=\"droptrue\" id=\"day1\" >");

                    if (drRoutes.Count != 0)
                    {
                        foreach (RouteDTO item1 in drRoutes.OrderBy(c => c.routeSequence).ToList())
                        {
                            txt.Append("<li class=\"ui-state-default\" id=\"day1_" + i.ToString() + "\">");
                            //   txt.Append(item1.Contact.Name.ToString() + "</div>");
                            //   txt.Append("<div class='removeicon'></div>");

                            if (UserSession.Instance.OriginatorString == "ADMIN")
                            {
                                txt.Append("<a href=\"javascript:DeleteRouteSequecneItem('" + repcode + "',1," + item1.routeSequence + "," + item1.RouteMasterId + ")\"><div class='removeicon'></div> </a>");
                            }
                            else
                            {
                                txt.Append("<a><div class='removeicon'></div> </a>");
                            }

                            txt.Append("<div>" + item1.RouteName.ToString() + "</div>");
                            txt.Append("</li>");
                            i++;
                        }
                    }
                    else
                    {

                    }

                    txt.Append("</ul>");
                    txt.Append("</div>");
                    #endregion
                }
            }

            txt.Append("<script type='text/javascript'>");
            txt.Append("$(document).ready(function () {");

            //if (drRoutes != null)
            //{
            //    if (drRoutes.Count != 0)
            //    {
            //        txt.Append("$('#MainContent_txtScheduleTemplate').val('" + drRoutes[0]..Description.ToString() + "');");
            //    }
            //}
            txt.Append("var sourceid='';");

            txt.Append("$('ul.droptrue').sortable({");
            txt.Append("   connectWith: 'ul'");
            txt.Append("});");

            txt.Append("$('ul.droptrue').bind('sortstop', function (event, ui) {");

            //txt.Append("alert('Source '+event.currentTarget.id);");
            //txt.Append("alert('Target '+ui.item[0].parentNode.id);");
            //txt.Append("alert('Item '+ui.item[0].attributes[0].nodeValue);");
            txt.Append("var sortindex = 0; for(var i=0;i<ui.item[0].parentNode.children.length ;i++){  if(ui.item[0].id == ui.item[0].parentNode.children[i].id){ sortindex =i; break;  } }");
            txt.Append("      changeRouteTemplate('" + repcode + "',ui.item[0].parentNode.id,event.currentTarget.id,ui.item[0].id,sortindex);");
            txt.Append("var sortindex = 0;");
            txt.Append("for(var i=0;i<ui.item[0].parentNode.children.length ;i++){  ");
            txt.Append("        if(ui.item[0].id == ui.item[0].parentNode.children[i].id){ ");
            txt.Append("            sortindex =i; ");
            txt.Append("            break;  ");
            txt.Append("        }");
            txt.Append("    }");
            //txt.Append("   alert(event.currentTarget.id);");
            //   txt.Append("    changeRouteTemplate('" + repcode + "',ui.item[0].parentNode.id,event.currentTarget.id,ui.item[0].id,sortindex);");

            txt.Append("});");


            txt.Append("});");

            //txt.Append("function drag(target, e) {");
            //txt.Append("      e.dataTransfer.setData('Text', target.id);");
            //txt.Append("      sourceid=target.parentNode.id;");
            //txt.Append("}");

            //txt.Append("function drop(target, e) {");
            //txt.Append("      var id = e.dataTransfer.getData('Text');");
            //txt.Append("      target.appendChild(document.getElementById(id));");
            //txt.Append("      changeTemplate('" + callcycycleid + "',target.attributes[0].value,sourceid,id);");
            //txt.Append("      e.preventDefault();");
            //txt.Append("}");
            txt.Append("</script>");
        }
        catch { txt = new StringBuilder(); }

        return txt.ToString();
    }

    //    public string LoadRestructureTemplateSection1(List<CallCycleContactDTO> Day2List, ScheduleStruct schedul_struct , int rindex, List<RouteDTO> drRoute ){

    //    StringBuilder txt = new StringBuilder();
    //         txt.Append("<div class='wrap'><div class='headline'>Day 1</div>");       
    //        txt.Append("<ul class=\"droptrue\" id=\"day1\" >");
    //        //txt.Append("<header>Day 1 </header>");
    //        if (scheduleStructList.DayList_1.Count != 0)
    //        {
    //            scheduleStructList.DayList_1 = scheduleStructList.DayList_1.OrderBy(c => c.DayOrder).ToList();
    //            foreach (CallCycleContactDTO item1 in scheduleStructList.DayList_1)
    //            {
    //                txt.Append("<li class=\"ui-state-default\" id=\"day1_" + i.ToString() + "\">");
    //                txt.Append("<a href=\"javascript:DeleteCallCycleItem(" + scheduleStructList.CallCycleId + ",1," + item1.DayOrder + ")\"> <div class='removeicon'></div> </a>");

    //                txt.Append("<div>" + item1.Contact.Name.ToString() + "</div>");
    //                txt.Append("</li>");
    //                i++;
    //            }
    //        }
    //        else
    //        {

    //        }

    //        return txt.ToString();

    //}
    private string SaveRouteMaster()
    {
        StringBuilder sb = new StringBuilder();
        CallCycleClient callcycleClient = new CallCycleClient();
        string str = string.Empty;

        try
        {
            string selectedRouteId = Request.QueryString["selectedRouteId"];
            string routeName = Request.QueryString["routeName"];

            RouteMasterDTO routeMasterDTO = new RouteMasterDTO();
            //marketDTO.isNew = true;
            if (!string.IsNullOrEmpty(UserSession.Instance.OriginatorString) && (UserSession.Instance.OriginatorString == "ASE"))
            {
                //routeMasterDTO.RouteType = "ASE";
                routeMasterDTO.RouteType = "DR";
            }
            else
            {
                routeMasterDTO.RouteType = "DR";
            }

            routeMasterDTO.RouteMasterId = string.IsNullOrEmpty(selectedRouteId) ? 0 : Convert.ToInt32(selectedRouteId);
            routeMasterDTO.RouteName = routeName;

            if (IsRouteMasterExists(routeMasterDTO))
                return "exists";

            routeMasterDTO.Status = "A";
            routeMasterDTO.CreatedBy = UserSession.Instance.UserName;
            routeMasterDTO.LastModifiedBy = UserSession.Instance.UserName;

            bool status = callcycleClient.SaveRouteMaster(routeMasterDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private bool IsRouteMasterExists(RouteMasterDTO routeMasterDTO)
    {
        CallCycleClient callcycleClient = new CallCycleClient();
        try
        {
            return callcycleClient.IsRouteMasterExists(routeMasterDTO);
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsCallCycleContactInAnyOtherSchedule(string customerCode)
    {
        CallCycleClient callcycleClient = new CallCycleClient();
        try
        {
            return callcycleClient.IsCallCycleContactInAnyOtherSchedule(customerCode);
        }
        catch (Exception)
        {
            return true;
        }
    }

    public string AddSelectedOutlets()
    {
        try
        {
            //string leadStage = Request.QueryString["LeadStage"];
            //string customerCodes = Server.HtmlDecode(Request.QueryString["idstr"]);
            string customerCodes = Request.QueryString["idstr"];
            List<string> codes = new List<string>();
            codes = (customerCodes.Split(',')).ToList();
            customerCodes = "";
            foreach (string item in codes)
            {
                if (!String.IsNullOrEmpty(item))
                {
                    if (String.IsNullOrEmpty(customerCodes))
                        customerCodes = customerCodes + "'" + item + "'";
                    else
                        customerCodes = customerCodes + ",'" + item + "'";
                }
            }

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();
            List<LeadCustomerDTO> selectedCustomersList = new List<LeadCustomerDTO>();
            //LeadDTO leaddto = null;
            LeadCustomerClient oLead = new LeadCustomerClient();
            CustomerClient oCustomer = new CustomerClient();

            ArgsDTO args = new ArgsDTO();
            args.Originator = UserSession.Instance.UserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.CustomerCode = custCode;
            //args.LeadId = int.Parse(leadId);
            CallCycleContactDTO callCycleContact = new CallCycleContactDTO();
            CallCycleContactDTO contact = new CallCycleContactDTO();
            //if (leadStage == "Customer")
            //{
            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.RowCount = 10;
            args.StartIndex = 1;
            //customerCodes = customerCodes.Replace("'","''");
            args.AdditionalParams = " cust_code IN (" + customerCodes + ") ";
            selectedCustomersList = oLead.GetCombinedCollection(args);

            //combinedList = oCustomer.GetCustomerView(args);
            //}

            foreach (LeadCustomerDTO item in selectedCustomersList)
            {
                contact = new CallCycleContactDTO();
                contact.Contact = new LeadCustomerDTO();
                contact.LeadStage = new LeadStageDTO();
                contact.Contact.SourceId = item.SourceId;
                contact.Contact.CustomerCode = item.CustomerCode;
                contact.Contact.EndUserCode = "";
                contact.Contact.Name = item.Name;
                contact.Contact.Address = item.Address;
                contact.Contact.City = item.City;
                contact.Contact.State = item.State;
                contact.Contact.PostalCode = item.PostalCode;
                contact.CreateActivity = true;
                contact.rowCount = 1;

                if (item.CustomerCode != null && item.CustomerCode != "")
                {
                    contact.Contact.LeadCustomerType = LeadCustomerTypes.Customer;
                    contact.LeadStage.StageName = "Customer";
                    callCycleContact = listCallCycleContact.Find(c => c.Contact.CustomerCode == contact.Contact.CustomerCode);
                }
                else
                {
                    contact.Contact.LeadCustomerType = LeadCustomerTypes.Lead;
                    contact.LeadStage.StageName = item.LeadStage;
                    callCycleContact = listCallCycleContact.Find(c => c.Contact.SourceId == contact.Contact.SourceId);
                }

                if (callCycleContact == null)
                {
                    //Check Customer exists in any other schedule - if exists return true
                    //if (IsCallCycleContactInAnyOtherSchedule(contact.Contact.CustomerCode))
                    //{
                    //return "true2"; //along with a link to relevent call cycle ID
                    //}

                    listCallCycleContact.Add(contact);
                    Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;

                }
                else
                {
                    contact.rowCount = callCycleContact.rowCount + 1;
                    listCallCycleContact.Remove(callCycleContact);
                    listCallCycleContact.Add(contact);
                    Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;

                    return "true1";
                }
            }

            //Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }

    }

    private string DeleteVisitCheckListMasterItem(string id)
    {
        CallCycleClient callCycleClient = new CallCycleClient();
        VisitChecklistMasterDTO visitCheckListMasterDTO = new VisitChecklistMasterDTO();

        visitCheckListMasterDTO.VisitChecklistMasterId = int.Parse(id);
        visitCheckListMasterDTO.LastModifiedBy = UserSession.Instance.UserName;

        bool success = callCycleClient.DeleteVisitChecklistMaster(visitCheckListMasterDTO);

        StringBuilder txt = new StringBuilder();
        if (success)
        {
            txt.Append("true");
        }
        else
        {
            txt.Append("false");
        }
        return txt.ToString();
    }

    private string SaveVisitCheckListMaster()
    {
        StringBuilder sb = new StringBuilder();
        CallCycleClient callCycleClient = new CallCycleClient();
        string str = string.Empty;

        try
        {
            string selectedItemId = Request.QueryString["selectedItemId"];
            string name = Request.QueryString["name"];
            string description = Request.QueryString["description"];

            VisitChecklistMasterDTO visitCheckListMasterDTO = new VisitChecklistMasterDTO();

            DateTime today = DateTime.Today;

            visitCheckListMasterDTO.VisitChecklistMasterId = string.IsNullOrEmpty(selectedItemId) ? 0 : Convert.ToInt32(selectedItemId);
            visitCheckListMasterDTO.Name = name;
            visitCheckListMasterDTO.Description = description;
            visitCheckListMasterDTO.EffectiveStartDate = new DateTime(today.Year, today.Month, 1);
            visitCheckListMasterDTO.EffectiveEndDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
            visitCheckListMasterDTO.CreatedBy = UserSession.Instance.UserName;
            visitCheckListMasterDTO.LastModifiedBy = UserSession.Instance.UserName;

            bool status = callCycleClient.SaveAllVisitChecklistMaster(visitCheckListMasterDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private string DeleteRouteMaster(string id)
    {
        CallCycleClient callCycleClient = new CallCycleClient();
        RouteMasterDTO routeMasterDTO = new RouteMasterDTO();

        routeMasterDTO.RouteMasterId = int.Parse(id);
        routeMasterDTO.LastModifiedBy = UserSession.Instance.UserName;

        bool success = callCycleClient.DeleteRouteMaster(routeMasterDTO);

        StringBuilder txt = new StringBuilder();
        if (success)
        {
            txt.Append("true");
        }
        else
        {
            txt.Append("false");
        }
        return txt.ToString();
    }

    #endregion
}