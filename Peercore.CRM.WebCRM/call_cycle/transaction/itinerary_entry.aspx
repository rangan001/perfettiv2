﻿<%@ Page Title="mSales" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="itinerary_entry.aspx.cs" Inherits="call_cycle_transaction_itinerary_entry" %>

<%@ MasterType TypeName="SiteMaster" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField runat="server" ID="HiddenFieldItineraryDetails" />
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
                <div class="clearall">
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/activity_planner/transaction/activity_scheduler.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div class="formleft">
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Name</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListRep" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListRep_SelectedIndexChanged" 
                        CssClass="dropdown_small">
                    </asp:DropDownList>
                <%--<asp:TextBox ID="txtName" runat="server" CssClass="input-large1"></asp:TextBox>--%></div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Base Town</div>
            <div class="formdetaildiv_right">
                :
                <asp:TextBox ID="TxtBaseTown" runat="server" CssClass="input-large1"></asp:TextBox></div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Private</div>
            <div class="formdetaildiv_right">
                :
                <asp:TextBox ID="txtPrivate" runat="server" Width="100px" CssClass="input-large1 input_numeric_num"></asp:TextBox></div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Contigency</div>
            <div class="formdetaildiv_right">
                :
                <asp:TextBox ID="txtContigency" runat="server" Width="100px" CssClass="input-large1 input_numeric_num"></asp:TextBox></div>
        </div>
        <div class="formright">
            <div class="formtextdiv">
                Month</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="cmbMonth" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbMonth_SelectedIndexChanged">
                    <asp:ListItem Value="01" Text="January"></asp:ListItem>
                    <asp:ListItem Value="02" Text="February"></asp:ListItem>
                    <asp:ListItem Value="03" Text="March"></asp:ListItem>
                    <asp:ListItem Value="04" Text="April"></asp:ListItem>
                    <asp:ListItem Value="05" Text="May"></asp:ListItem>
                    <asp:ListItem Value="06" Text="June"></asp:ListItem>
                    <asp:ListItem Value="07" Text="July"></asp:ListItem>
                    <asp:ListItem Value="08" Text="August"></asp:ListItem>
                    <asp:ListItem Value="09" Text="September"></asp:ListItem>
                    <asp:ListItem Value="10" Text="October"></asp:ListItem>
                    <asp:ListItem Value="11" Text="November"></asp:ListItem>
                    <asp:ListItem Value="12" Text="December"></asp:ListItem>
                </asp:DropDownList>
                <%--<asp:TextBox ID="TxtMonth" runat="server"  CssClass="input-large1"></asp:TextBox>--%></div>
            <div class="clearall">
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Year</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListMonth_SelectedIndexChanged">
                </asp:DropDownList>
                <%--<asp:TextBox ID="TxtMonth" runat="server"  CssClass="input-large1"></asp:TextBox>--%></div>
            <div class="clearall">
            </div>
                        
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto; margin: 10px;">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="ItineraryGrid">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
            jQuery(function ($) {
                $('input.input_numeric_num').autoNumeric({ aSep: null, aDec: null, mDec: 0 });
            });

            loadTMEItinerary();
        });

        $("#MainContent_buttonbar_buttinSave").click(function () {
            itinerarySave();
        });
        $("#MainContent_buttonbar_buttonSaveAndExportExcel").click(function () {
            itinerarySave();
        });

        function LocalMilesEditor(container, options) {

            $('<input type="text" name="LocalMiles" data-text-field="LocalMiles" data-value-field="LocalMiles" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                        .kendoNumericTextBox({
                            format: "{0:n0}",
                            change: function changeTotal() {
                                settingItinerarygrid(options.model.uid, options.field);
                            }
                        });

        }

        function DirectMilesEditor(container, options) {
            $('<input type="text" name="DirectMiles" data-text-field="DirectMiles" data-value-field="DirectMiles" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                        .kendoNumericTextBox({
                            format: "{0:n0}",
                            change: function changeTotal() {
                                settingItinerarygrid(options.model.uid, options.field);
                            }
                        });

        }

        function settingItinerarygrid(uid, field) {
            var model = $("#ItineraryGrid").data("kendoGrid").dataSource.getByUid(uid);

            if (model != undefined) {
                model.fields.TotalMiles.editable = true;
                //if (model.LoadQty <= model.AllocQty) {
                var totalMiles = model.LocalMiles + model.DirectMiles;
                model.set("TotalMiles", totalMiles);
                //}
                //else {
                //    model.set("LoadQty", 0);
                //}
                model.fields.TotalMiles.editable = false;
            }
        }
    </script>
</asp:Content>
