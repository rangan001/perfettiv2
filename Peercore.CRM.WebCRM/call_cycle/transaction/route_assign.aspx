﻿<%@ Page Title="mSales - Route Assign" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="route_assign.aspx.cs" Inherits="call_cycle_transaction_route_assign" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField runat="server" ID="HiddenFieldRepCode" />
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenFieldOriginator" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteCode" />
    <asp:HiddenField ID="HiddenFieldExistClickOk" runat="server" />

    <div id="repmodalWindow" style="display: none">
        <div id="div_repconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div class="formleft">
            <div class="formtextdiv">
                Route Name 
            </div>
            <div class="formdetaildiv">
            
                <span class="wosub mand">
                    
                    <asp:TextBox ID="txtRouteName" CssClass="tb2" runat="server" Style="width: 200px;
                        text-transform: uppercase"></asp:TextBox>
                    <div runat="server" id="div_autocomplete_route">
                    </div>
                </span>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Sales Rep 
            </div>
            <div class="formdetaildiv">
                
                <span class="wosub mand">
                    <asp:TextBox ID="txtDRName" CssClass="tb" runat="server" Style="width: 200px; text-transform: uppercase"></asp:TextBox>
                    <div runat="server" id="div_autocomplete">
                    </div>
                </span>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Is Temparary Rep 
            </div>
            <div class="formdetaildiv">
                
                <span class="wosub mand">
                    <asp:CheckBox ID="chktempararychk" runat="server" />
                </span>
            </div>
            <div class="clearall">
            </div>
            <div id = "div_isTempSection">
                
                <div class="formtextdiv">
                    Temp DR Assign Date
                </div>
                <div class="formdetaildiv">
                    <span class="wosub mand">
                        <asp:TextBox ID="AssignDate" runat="server"></asp:TextBox>
                    </span>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="grid_container" id="div_mainAssignedRoutes">
            <div style="margin: 10px auto;">
                Assigned Routes
            </div>
            <div id="div_assignedRoutes">
            </div>
        </div>
    </div>
    <script type="text/javascript">        
        jQuery(function () {
            jQuery("#MainContent_txtDRName").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter DR name"
            });
        });

        jQuery(function () {
            jQuery("#MainContent_txtRouteName").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter Route"
            });
        });

        function setRepCode(repCode) {
            $("#MainContent_HiddenFieldRepCode").val(repCode);
        }

        function setRouteCode(routeCode) {
            $("#MainContent_HiddenFieldRouteCode").val(routeCode);
        }

        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
            $("#div_isTempSection").css("display", "none");

            $("#div_mainAssignedRoutes").css("display", "block");

            $("#MainContent_AssignDate").kendoDatePicker({
                value: new Date(),
                format: "dd-MMM-yyyy"
                // timeFormat: "HH:mm"
            });

            loadAssignedRoutes();
        });

        $('#MainContent_chktempararychk').change(function () {
            if ($(this).is(":checked")) {
                //var returnVal = confirm("Are you sure?");
                $("#div_isTempSection").css("display", "block");
                //$(this).attr("checked", returnVal);
            }
            else 
            {
                //var returnVal = confirm("Are you sure?");
                $("#div_isTempSection").css("display", "none");
                //$(this).attr("checked", returnVal);
            }
        });


    </script>
</asp:Content>
