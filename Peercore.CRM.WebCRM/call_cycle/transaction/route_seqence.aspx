﻿<%@ Page Title="mSales" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="route_seqence.aspx.cs" Inherits="call_cycle_transaction_route_seqence" %>

<%@ MasterType TypeName="SiteMaster" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/buttonbar_navigation.ascx" TagPrefix="ucl2" TagName="buttonbarNavi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField runat="server" ID="HiddenFieldRouteMasterID" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteName" />
    <asp:HiddenField runat="server" ID="HiddenFieldRepCode" />
    <meta name="”viewport”" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <script type="text/javascript" src="../../assets/scripts/jquery.ui.touch-punch.min.js"></script>

    <div id="confirmmodalWindow" style="display: none">
        <div id="div_Confirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yesConfirm" class="k-button">
            Yes</button>
        <button id="noConfirm" class="k-button">
            No</button>
    </div>

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div id="div_message" runat="server" style="display: none;">
            </div>
            <div class="clearall">
            </div>
            <div class="toolbar_left1" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                    <div class="clearall"></div>
                    <div>
                      
                    </div>
                </div>
            </div>
            <div class="toolbar_right2" id="div3">
                <div class="leadentry_title_bar">
                <ucl2:buttonbarNavi ID="buttonbarNavi" runat="server" />
                </div>
            </div>
            <div class="formleft">
                <div class="formtextdiv" id="formtextdiv" runat="server">
                    Sales Rep :
                </div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="txtDRName" runat="server" Style="text-transform: uppercase; display: none;"
                        CssClass="tb input-large1"></asp:TextBox>
                    <asp:DropDownList ID="ddlDRList" runat="server" ClientIDMode="Static" style="width: 200px;"></asp:DropDownList>
                    <div runat="server" id="div_autocomplete_DR">
                    </div>
                </div>
                <%--<div style="line-height:30px; margin-top:3px;">
                        <a href="#" id="id_schedule">
                        <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" />
                        </a>
                </div>--%>
                <div class="clearall">
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_schedule">
        </div>
        <div id="div_custgrid">
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            loadRouteTemplate($('#ddlDRList :selected').val());
        });

        hideStatusDiv("MainContent_div_message");
        $("#MainContent_buttonbarNavi_hlBack").css("display", "none");
//        jQuery(function () {
//            jQuery("#MainContent_txtDRName").validate({
//                expression: "if (VAL) return true; else return false;",
//                message: "Please Enter DR Name"
//            });
//        });

        $("MainContent_buttonbar_buttinSave").live("click", function () {
            jQuery("#MainContent_txtDRName").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter DR Name"
            });
        });

        $("#ddlDRList").change(function () {
            loadRouteTemplate($('#ddlDRList :selected').val());
        });

    </script>

</asp:Content>
