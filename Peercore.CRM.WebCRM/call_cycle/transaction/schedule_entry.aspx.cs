﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Text;


public partial class call_cycle_transaction_schedule_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
            buttonbar.onButtonActiveDeactive = new usercontrols_buttonbar.ButtonActiveDeactive(ButtonActiveDeactive_Click);

            buttonbar.onButtonRecur = new usercontrols_buttonbar.ButtonRecur(ButtonRecur_Click);
            buttonbar.onButtonTemplate = new usercontrols_buttonbar.ButtonTemplate(ButtonTemplate_Click);

            buttonbar.EnableSave(true);
            buttonbar.VisibleSave(true);
            buttonbar.EnableActiveDeactive(true);
            buttonbar.VisibleActiveDeactive(true);
            buttonbar.VisibleRecur(true);
            buttonbar.VisibleTemplate(true);
            buttonbar.VisibleDeactivate(true);
            buttonbar.VisibleActivityHistory(false);
            buttonbar.SetActiveDeactive(false);

            if (UserSession.Instance.OriginatorString == "ASE")
            {
                id_username.InnerText = "ASE";
                txtDRName.Text = UserSession.Instance.Originator;
                txtDRName.Enabled = false;
            }
            else
            {
                div_autocomplete.InnerHtml = SetAutocomplete_Temp();
            }

            SetBackLink();
            BindLeadCustomers();
            if (!IsPostBack)
            {
                Session[CommonUtility.CALL_CYCLE_CONTACTS] = null;
                if (Request.QueryString["callcycid"] != null)
                {


                    HiddenFieldCallCycleID.Value = Request.QueryString["callcycid"];
                    SetCallCycleState(int.Parse(HiddenFieldCallCycleID.Value));
                    LoadCallCycle(int.Parse(HiddenFieldCallCycleID.Value));
                    CallCyclegrid.Visible = false;

                }
                else
                {
                    HiddenFieldCallCycleID.Value = "0";
                }
            }

            Master.SetBreadCrumb("Route Schedule", "#", "");
        }
    }

    private void SetCallCycleState(int iCallCycleID)
    {
        bool isInActive = false;
        if (iCallCycleID == 0)
            isInActive = false;
        else
            isInActive = new CallCycleClient().GetCallCycleState(iCallCycleID);

        if (isInActive)
        {
            buttonbar.SetActiveDeactive(true);
        }
        else
        {
            buttonbar.SetActiveDeactive(false);
        }
    }

    public void LoadCallCycle(int iCallCycleID)
    {
        ArgsDTO argsDTO = new ArgsDTO();

        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        argsDTO.AdditionalParams = "";
        argsDTO.OrderBy = "CallCycleID ASC";

        argsDTO.StartIndex = 1;
        argsDTO.RowCount = 10;

        List<CallCycleDTO> listCallCycleContact = new List<CallCycleDTO>();
        CallCycleClient callCycleClient = new CallCycleClient();

        listCallCycleContact = callCycleClient.GetCallCycle(argsDTO, false, iCallCycleID, "");

        if (listCallCycleContact.Count > 0)
        {
            txtDescription.Text = listCallCycleContact[0].Comments;
            txtSchedule.Text = listCallCycleContact[0].Description;
            txtDRName.Text = listCallCycleContact[0].RepName;
            HiddenField_DueDate.Value = listCallCycleContact[0].DueOnString;
            tbCreated.InnerHtml = "Created By: " + listCallCycleContact[0].CreatedBy + ", " + listCallCycleContact[0].CreatedDateString;
        }
    }

    private void BindLeadCustomers()
    {
        if (CurrentUserSettings == null)
            CurrentUserSettings = new ArgsDTO();
        CurrentUserSettings.ChildOriginators = UserSession.Instance.ChildOriginators;
        CurrentUserSettings.Originator = UserSession.Instance.UserName;
        CurrentUserSettings.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        CurrentUserSettings.ActiveInactiveChecked = true;
        CurrentUserSettings.ReqSentIsChecked = false;
        CurrentUserSettings.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;
        CurrentUserSettings.LeadStage = "";
    }

    protected void ButtonSave_Click(object sender)
    {
        CommonUtility commonUtility = new CommonUtility();
        if (txtSchedule.Text.Trim() == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Call Cycle name is required.");
            txtSchedule.Focus();
            return;
        }
        SaveCallCycle();
    }

    private void SaveCallCycle()
    {
        try
        {
            bool iNoRecs = false;
            List<CallCycleContactDTO> oCustomers = null;
            oCustomers = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (oCustomers == null)
                oCustomers = new List<CallCycleContactDTO>();

            CallCycleDTO callCycle = new CallCycleDTO();
            List<CallCycleDTO> callcycleList = new List<CallCycleDTO>();

            CallCycleClient oCallCycle = new CallCycleClient();

            ArgsDTO argsDTO = new ArgsDTO();

            argsDTO.Originator = UserSession.Instance.UserName;
            argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
            argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            argsDTO.AdditionalParams = "";
            argsDTO.OrderBy = "CallCycleID DESC";

            argsDTO.StartIndex = 1;
            argsDTO.RowCount = 10;

            if (!string.IsNullOrEmpty(HiddenFieldRepCode.Value))
                callcycleList = oCallCycle.GetCallCyclesForRep(HiddenFieldRepCode.Value);

            if (callcycleList.Count != 0)
            {


                if ((callcycleList[0].CallCycleID != null) && (HiddenFieldExistClickOk.Value != "1"))
                {
                    string tempmsg = "";
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showroutetempexist('" + callcycleList[0].Description.Trim() + "','" + tempmsg + "');", true);
                    return;
                }
            }

            List<CallCycleDTO> lstCallCycle = oCallCycle.GetCallCycle(argsDTO, false, 0, txtSchedule.Text.Trim());
            if (lstCallCycle.Count > 0)
            {
                lstCallCycle = lstCallCycle.OrderByDescending(x => x.CallCycleID).ToList();
                HiddenFieldCallCycleID.Value = lstCallCycle[0].CallCycleID.ToString();
            }

            callCycle.CallCycleID = Convert.ToInt32(HiddenFieldCallCycleID.Value == "" ? "0" : HiddenFieldCallCycleID.Value);
            callCycle.Description = txtSchedule.Text;
            callCycle.DueOn = DateTime.Now; //dtpDueDate.Text != "" ? DateTime.Parse(dtpDueDate.Text) : DateTime.MinValue;
            callCycle.Comments = txtDescription.Text;
            callCycle.DelFlag = "N";
            callCycle.CCType = UserSession.Instance.RepType;
            callCycle.PrefixCode = HiddenFieldRepCode.Value;

            TimeSpan tsStartTime = new TimeSpan();// .(TimeSpan)rtpStartTime.SelectedTime;
            int callCycleId = 0;
            iNoRecs = oCallCycle.CallCycleSave(callCycle, oCustomers, UserSession.Instance.UserName, tsStartTime, out callCycleId);

            if (iNoRecs)
            {
                HiddenFieldCallCycleID.Value = callCycleId.ToString();
                //selectedCallCycleId = callCycle.CallCycleID;
                //txtCallCycleID.Text = callCycle.CallCycleID.ToString();

                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = new CommonUtility().GetSucessfullMessage("Successfully Saved.");

                //GlobalValues.GetInstance().ShowMessage("Successfully Saved.", GlobalValues.MessageImageType.Information);
                //GlobalValues.ResetPropertyState();
            }
        }
        catch (Exception oException)
        {
        }
    }

    protected void ButtonClear_Click(object sender)
    {
        Clear();
    }

    private void Clear()
    {
        div_message.Attributes.Add("style", "display:none");
        txtSchedule.Text = "";
        txtDescription.Text = "";
        //dtpDueDate.Text = DateTime.Today.ToString();
        //this.selectedCallCycleId = null;
        HiddenFieldCallCycleID.Value = "0";
        Session.Remove(CommonUtility.CALL_CYCLE_CONTACTS);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadLeadCustomer(0, 1);", true);
        //dgCust.ItemsSource = null;
        txtDRName.Text = "";

    }

    protected void ButtonActiveDeactive_Click(object sender)
    {
        try
        {
            CommonUtility commonUtility = new CommonUtility();
            if (string.IsNullOrEmpty(HiddenFieldCallCycleID.Value) || HiddenFieldCallCycleID.Value == "0")
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetErrorMessage("The call cycle is not saved!");

                return;
            }
            CallCycleClient oCallCycle = new CallCycleClient();

            bool isInActive = oCallCycle.GetCallCycleState(int.Parse(HiddenFieldCallCycleID.Value));

            if (oCallCycle.DeactivateCallCycle(int.Parse(HiddenFieldCallCycleID.Value), isInActive ? "N" : "Y"))
            {

                if (isInActive)
                {
                    buttonbar.SetActiveDeactive(false);
                    div_message.Attributes.Add("style", "display:block");
                    div_message.InnerHtml = commonUtility.GetSucessfullMessage("Call cycle was successfully activated");
                }
                else
                {
                    buttonbar.SetActiveDeactive(true);
                    div_message.Attributes.Add("style", "display:block");
                    div_message.InnerHtml = commonUtility.GetSucessfullMessage("Call cycle was successfully deactivated");
                }
                //SetCallCycleState(selectedCallCycleId.Value);
            }
        }
        catch (Exception oException)
        {

        }
    }


    protected void ButtonRecur_Click(object sender)
    {

    }

    protected void ButtonTemplate_Click(object sender)
    {
        Response.Redirect("~/call_cycle/transaction/template_entry.aspx?callcycid=" + HiddenFieldCallCycleID.Value);
    }

    private void SetBackLink()
    {
        if (Request.QueryString["callcycid"] != null && Request.QueryString["callcycid"] != string.Empty)
        {
            hlBack.NavigateUrl = "~/call_cycle/transaction/template_entry.aspx?callcycid=" + Request.QueryString["callcycid"];
        }
    }


    private string SetAutocomplete_Temp()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/call_cycle/call_cycle_service.asmx/GetRepsByOriginator\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Name,");
        sb.Append("                         desc: item.UserName");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("             select: function( event, ui ) {");
        sb.Append("                 var selectedObj = ui.item;");
        sb.Append("                 console.log(selectedObj);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustomerIndustry').val(selectedObj.industry);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustomerCode').val(selectedObj.label);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustCode').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_txtDRName').val(selectedObj.label);");
        sb.Append("                 setRepCode(selectedObj.desc);");
        sb.Append("                 if($('#MainContent_HiddenFieldCallCycleID').val() == '0' || $('#MainContent_HiddenFieldCallCycleID').val() == ''){");
        sb.Append("                     loadLeadCustomersForNewEntry(selectedObj.desc);");
        sb.Append("                 }");
        //sb.Append("                 setcuscode(selectedObj.label,selectedObj.desc,'0');");
        //k   sb.Append("                 loadproductlist(selectedObj.label);");
        //sb.Append("                 $('ul.ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all').css('display','none');");
        sb.Append("             }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }
}