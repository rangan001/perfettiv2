﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="territory_assign.aspx.cs" Inherits="call_cycle_transaction_territory_assign" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />

    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />

    <div id="repmodalWindow" style="display: none">
        <div id="div_repconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <div class="divcontectmainforms" style="color: black">
        <div class="toolbar_container" style="display: none;">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back"></div>

                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div class="clearall">
        </div>
        <div class="grid_container" id="div_mainAssignedRoutes">
            <div id="gridAllTerritoryReps">
            </div>
        </div>
    </div>
    <script type="text/javascript">

        var Territories = [];

        $(document).ready(function () {
            var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd-MMM-yyyy');
            var Originator = $("#<%= hfOriginator.ClientID %>").val();
            var OriginatorType = $("#<%= hfOriginatorType.ClientID %>").val();

            LoadTerritoriesByOriginator(OriginatorType, Originator);
            LoadAllSRTerritoryByOriginator(OriginatorType, Originator);
        });


    </script>
</asp:Content>