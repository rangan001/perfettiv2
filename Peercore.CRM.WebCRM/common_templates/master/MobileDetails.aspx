﻿<%@ Page Title="mSales - Mobile Security" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MobileDetails.aspx.cs" Inherits="common_templates_master_MobileDetails" %>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="width: 100%" id="Div2">
        <table border="0" style="margin-left: 25px; font-size: 20px; width: 35%; margin-top: 36px; float: left; padding: 10px;">
            <tbody>
                <tr id="trPin" runat="server" visible="false">
                    <td class="textalignbottom" style="font-size: 15px; border-bottom: 1px solid rgb(204, 204, 204); padding-bottom: 0px;">
                        Pin Number
                    </td>
                    <td colspan="3"  style="font-size: 20px; border-bottom: 1px solid rgb(204, 204, 204); padding-bottom: 0px;">
                        <asp:Label ID="lblPin" runat="server" Text=""></asp:Label>
                    </td>
                    <td colspan="3"  style="font-size: 15px; border-bottom: 1px solid rgb(204, 204, 204); padding-bottom: 0px;">
                        <asp:Button ID="btnReset" runat="server" Text="Reset" onclick="btnReset_Click" style="margin-bottom: 3px;"></asp:Button>
                    </td>
                </tr>

                <tr>
                    <td class="textalignbottom" style="font-size: 15px; border-bottom: 1px solid rgb(204, 204, 204); padding-bottom: 0px;">
                        Mobile Sync Details
                    </td>
                    <td colspan="3"  style="font-size: 20px; border-bottom: 1px solid rgb(204, 204, 204); padding-bottom: 0px;">
                        
                    </td>
                    <td colspan="3"  style="font-size: 15px; border-bottom: 1px solid rgb(204, 204, 204); padding-bottom: 0px;">
                       <asp:Button ID="btnNotifyIsSync" runat="server" Text="Get Sync Details" 
                            style="margin-bottom: 3px;" onclick="btnNotifyIsSync_Click"></asp:Button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <script type="text/javascript">

    </script>
</asp:Content>
