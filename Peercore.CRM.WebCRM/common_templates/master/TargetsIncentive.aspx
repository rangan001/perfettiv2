﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="TargetsIncentive.aspx.cs" Inherits="common_templates_master_TargetsIncentive" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    
        <table border="0" >
            <tbody>
                <tr>
                    <td class="textalignbottom">
                        Target breakdown Day(<span style="color: #f80000;">X</span>) 
                    </td>
                    <td colspan="3">
                        <span class="wosub mand">&nbsp;<input type="text" runat="server" class="txt14Days" id="txtTargetBreakDay"
                            style="" />
                            </span>
                    </td>
                </tr>
                <tr>
                    <td class="textalignbottom">
                        1 to <span style="color: #f80000;">X</span> Days
                    </td>
                    <td colspan="3">
                        <span class="wosub mand">&nbsp;<input type="text" runat="server" class="txt14Days" id="txtFirst14Days"
                            style="" />
                            <span>%</span></span>
                    </td>
                </tr>
                <tr>
                    <td class="textalignbottom">
                        <span style="color: #f80000;">X+1</span> to End of the Month
                    </td>
                    <td colspan="3">
                        <span class="wosub mand">&nbsp;<input type="text" runat="server" class="txt14Days" id="txtLast14Days" />
                            <span>%</span></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="textalignbottom">
                        <button class="k-button" id="buttonSave" type="button" onclick="return buttonSave_onclick()">
                            Save</button>
                        <button class="k-button" id="buttonClear">
                            Clear</button>
                        <asp:HiddenField ID="hdnSelectedFlavorId" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
            </tbody>
        </table>
    
    <script type="text/javascript">
        //Save button click

        $('.txt14Days').keypress(function (event) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && (event.which != 8 )) {
                event.preventDefault();
            }
        });

        $("#buttonSave").click(function () {

        });
        
        //Save button click
        $("#buttonClear").click(function () {
            $('#txtFirst14Days').val('');
            $('#txtLast14Days').val('');
            $('#txtFirst14Days').focus();
        });

        function buttonSave_onclick() {
            var valTargetBreakDay = $('#MainContent_txtTargetBreakDay').val();
            var valFirst14Days = $('#MainContent_txtFirst14Days').val();
            var valLast14Days = $('#MainContent_txtLast14Days').val();

            var param = { "TargetBreakDay": valTargetBreakDay, "First14Days": valFirst14Days, "Last14Days": valLast14Days };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/SaveTargetAndIncentiveNew",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    alert('Successfully Updated.');
                },
                error: function (response) {
                    alert('Error.');               
                }
            });
        }

    </script>
</asp:Content>
