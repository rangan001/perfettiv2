﻿<%@ Page Title="mSales - User Details" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="attendance_reasons.aspx.cs" Inherits="common_templates_master_user_details" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfReasonId" runat="server" />
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div id="windowReason" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Reason
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtReason" maxlength="150" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSaveReason" type="button">Save</button>
                                    <button class="k-button" id="buttonClearReason">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_reason" clientidmode="Static" runat="server" href="#">Add Reason</a>
                    </div>
                    <%--<div class="reloadlinkdiv" style="float: left" id="div_reload_1">
                        <a id="id_reload" clientidmode="Static" runat="server" href="#"></a>
                    </div>--%>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="gridReasons"></div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            loadAttendanceResonsGrid();
        });

        function loadAttendanceResonsGrid() {

            var take_grid = $("#MainContent_hfPageIndex").val();

            var url = "javascript:updateReason('#=id#','#=reason#','#=description#');";
            var urlReason = "<a href=\"" + url + "\">#=reason#</a>";

            $("#gridReasons").html("");
            $("#gridReasons").kendoGrid({
                height: 355,
                columns: [
                    { field: "id", title: "Id", width: "85px", hidden: true },
                    {
                        template: "# if (status == 'A' ) { # " + urlReason + " # } else { # <span class='span_button'>" + urlReason + "</span> #} #",
                        field: "", title: "Reason", width: "150px"
                    },
                    { field: "description", title: "Description", width: "250px", hidden: true },
                    { template: '<a href="javascript:DeleteAttendanceReason(\'#=id#\');">Delete</a>', field: "", width: "40px" },
                ],
                editable: false, // enable editing
                pageable: false,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            fields: {
                                id: { validation: { required: true } },
                                reason: { editable: false, nullable: true },
                                description: { editable: false, nullable: true },
                                status: { editable: false, nullable: true }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/getAttendanceReasons", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    $("#div_loader").hide();
                                    var entityGrid = $("#gridReasons").data("kendoGrid");

                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));

                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        function DeleteAttendanceReason(id) {

            param = { "id": id };
            debugger;
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/deleteAttendanceReason",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == true) {
                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        loadAttendanceResonsGrid();
                        hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });
        }

        //Load popup for Add new User
        $("#id_add_reason").click(function () {
            $("#windowReason").css("display", "block");
            $("#<%= hfReasonId.ClientID %>").val(0);
            $("#txtReason").val('');

            var wnd = $("#windowReason").kendoWindow({
                title: "Add/Edit Reason",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().open();
        });

        //Save button click
        $("#buttonSaveReason").click(function () {
            addNewReason();
        });

        //Button Clear Clicked in txtReason
        $("#buttonClearReason").click(function () {
            $("#txtReason").val('');
        });

        function addNewReason() {
            var id = $("#<%= hfReasonId.ClientID %>").val();
            var reason = document.getElementById("txtReason").value;
            var description = '';

            param = { "id": id, "reason": reason, "description": description };
            
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/addAttendanceReason",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == true) {
           
                        closepopup();
                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Saved.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        loadAttendanceResonsGrid();
                        hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (response) {
                                       
                }
            });
        }

        function closepopup() {
            var wnd = $("#windowReason").kendoWindow({
                title: "Add/Edit Reason",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#windowReason").css("display", "none");
        }

        function updateReason(id, reason, description) {
            $("#<%= hfReasonId.ClientID %>").val(id);
            $("#txtReason").val(reason);

            $("#windowReason").css("display", "block");
            var wnd = $("#windowReason").kendoWindow({
                title: "Add/Edit Reason",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().open();
        }

    </script>
</asp:Content>
