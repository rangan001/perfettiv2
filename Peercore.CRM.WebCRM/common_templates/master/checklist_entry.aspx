﻿<%@ Page Title="mSales - Check-List" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="checklist_entry.aspx.cs"
    Inherits="common_templates_master_checklist_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedImageName" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedImageFullPath" runat="server" />
    <asp:HiddenField ID="hfPageIndexRouteMasterGridRep" runat="server" />
    <asp:HiddenField ID="hfSelectedDistributorUsername" runat="server" />
    <asp:HiddenField ID="hfSelectedMarketId" runat="server" />
    <asp:HiddenField ID="hfSelectedVolume" runat="server" />
    <asp:HiddenField ID="hfSelectedOutletTypeId" runat="server" />
    <asp:HiddenField ID="hfSelectedTown" runat="server" />
    <asp:HiddenField ID="HiddenFieldSaveFailed" runat="server" />
    <asp:HiddenField ID="hfCheckListId" runat="server" />
    <div id="customermodalWindow" style="display: none">
        <div id="div_customerconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div style="display: none" id="id_div_buttonBar" class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
                <a id="id_add_checklist" class="sprite_button" style="float: left; margin-left: 0px;
                    margin-top: 0px;"><span class="plus icon"></span>New Check-List</a>
                <%--<a id="id_delete_checklist" class="sprite_button" style="float: left; margin-left: 5px; 
                    margin-top: 0px;"><span class="cross icon"></span>Delete</a>--%>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/common_templates/master/checklist_entry.aspx">
                        <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div class="clearall">
        </div>
        <div id="div_chkroutegrid" style="display: none">
        </div>
        <div id="div_custgrid" style="display: none;">
        </div>
        <div id="div_salesreps" style="display: none">
        </div>
        <div id="div_checklist" runat="server" style="display: block">
            <div style="min-width: 200px; overflow: auto">
                <div style="float: left; width: 98.5%; overflow: auto; margin: 0px 10px;">
                    <div id="divCheckList">
                    </div>
                </div>
            </div>
        </div>
        <div id="div_newEntry" runat="server" style="display: none">
            <%--layout new format--%>
            <div class="formleft" style="padding-top: 0;">
                <fieldset>
                    <legend>Basic Details</legend>
                    <div class="formtextdiv">
                        Task</div>
                    <div class="formdetaildiv_right">
                        <span class="wosub mand">:
                            <asp:TextBox ID="txtName" runat="server" CssClass="input-large1" MaxLength="150"></asp:TextBox>
                            <span style="color: Red">*</span>
                        </span>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Checklist Type</div>
                    <div class="formdetaildiv_right">
                        <span class="wosub mand">:
                            <asp:DropDownList ID="DropDownListChecklistTypes" runat="server" class="input-large1"
                                MaxLength="100">
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </div>
            <div class="formright" style="padding-top: 0;">
                <fieldset>
                    <legend>Checklist Image</legend>
                    <div class="formtextdiv">
                        Image
                    </div>
                    <div class="formdetaildiv">
                        <div id="div_preview" class="upload_image" runat="server">
                        </div>
                        <div class="clearall">
                        </div>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                    </div>
                    <div class="formdetaildiv" style="width: 71%">
                        <input class="uploadChecklistImage" name="checklistImage" id="checklistImage" type="file"
                            upload-id="02ebeebf-98aa-459b-b41f-49028fa37e9c" />
                        <%--<asp:Label ID="Label26" runat="server" Text="Supported Formats : JPG, GIF, TIFF, PNG, BMP"></asp:Label>--%>
                        <asp:Label ID="Label26" runat="server" Text="Supported Formats : JPG, JPEG "></asp:Label>
                        <asp:Label ID="lblImageName" runat="server" Text="*"></asp:Label>
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </div>
        </div>
        <div id="div_editEntry" runat="server" style="display: none">
            <%--layout new format--%>
            <div class="formleft" style="padding-top: 0;">
                <fieldset>
                    <legend>Basic Details</legend>
                    <div class="formtextdiv">
                        Task</div>
                    <div class="formdetaildiv_right">
                        <span class="wosub mand">:
                            <asp:TextBox ID="txtETask" runat="server" CssClass="input-large1" MaxLength="150"></asp:TextBox><span
                                style="color: Red">*</span></span>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Checklist Type</div>
                    <div class="formdetaildiv_right">
                        <span class="wosub mand">:
                            <asp:DropDownList ID="ddlEChecklistTypes" runat="server" class="input-large1"
                                MaxLength="100">
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </div>
            <div class="formright" style="padding-top: 0;">
                <fieldset>
                    <legend>Checklist Image</legend>
                    <div class="formtextdiv">
                        Image
                    </div>
                    <div class="formdetaildiv">
                        <div id="div_e_preview" class="upload_image" runat="server">
                        </div>
                        <div class="clearall">
                        </div>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                    </div>
                    <div class="formdetaildiv" style="width: 71%">
                        <input class="uploadChecklistImage" name="echecklistImage" id="echecklistImage" type="file"
                            upload-id="02ebeebf-98aa-459b-b41f-49028fa37e9c" />
                        <%--<asp:Label ID="Label26" runat="server" Text="Supported Formats : JPG, GIF, TIFF, PNG, BMP"></asp:Label>--%>
                        <asp:Label ID="Label1" runat="server" Text="Supported Formats : JPG, JPEG "></asp:Label>
                        <asp:Label ID="lblEImageName" runat="server" Text="*"></asp:Label>
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="clearall">
        </div>
    </div>


    <style>
        div.k-window 
        {
            position: fixed !important;
            height: auto  !important; }
            
        div.k-window-content 
        {
            height: 470px  !important; }
            
        .k-grid-content 
        {
            height: 356px  !important; }
            
        .k-window {
            background-color: #c1b984;
            box-shadow: 2px 2px 8px 1px rgb(112, 111, 173);
        }
    </style>


    <script type="text/javascript">
        $(document).ready(function () {

            hideStatusDiv("MainContent_div_message");
            loadCheckListGrid();
            InitializeChecklistImageUpload();
        });

        $("#id_add_checklist").click(function () {
            $('#MainContent_lblImageName').text("");
            $("#id_delete_checklist").hide();
            $("#id_add_checklist").hide();
            BtnAddCheckListHandler();
        });

        function BtnAddCheckListHandler() {
            $("#MainContent_div_newEntry").css("display", "block");
            $("#MainContent_div_checklist").css("display", "none");

            $("#id_add_checklist").css("display", "none");
            $("#id_delete_checklist").css("display", "none");

            $("#id_div_buttonBar").css("display", "block");
            $('#MainContent_hfCheckListId').val(0);
            $("#MainContent_DropDownListChecklistTypes").prop("disabled", false);
            //            $("#MainContent_txtName").val("");
            //            $("#MainContent_txtFgCode").val("");
            //            $("#MainContent_txtCode").val("");
            //            $("#MainContent_txtWeight").val("");
            //            $("#MainContent_cmbBrand").val("0");
            //            $("#MainContent_cmbCategory").val("0");
            //            $("#MainContent_cmbFlavor").val("0");
            //            $("#MainContent_cmbPacking").val("0");
            //            $("#MainContent_radiono").prop('checked', true);
            //            $("#MainContent_radioyes").prop('checked', false);

            //            $("#MainContent_radioPromoyes").prop('checked', true);
            //            $("#MainContent_radioPromono").prop('checked', false);

            //            $('#MainContent_HiddenFieldSelectedImageName').val();
            //            $('#MainContent_HiddenFieldSelectedImageFullPath').val();

            //            $("#MainContent_div_preview").css("display", "none");
            //            $("#MainContent_div_preview").html('');
        }

        function BtnEditCheckListHandler(CheckListId, chklstType, chklstName) {

            $('#MainContent_lblImageName').text("");
            $("#id_delete_checklist").hide();
            $("#id_add_checklist").hide();
            $("#MainContent_div_newEntry").css("display", "block");
            $("#MainContent_div_checklist").css("display", "none");

            $("#id_add_checklist").css("display", "none");
            $("#id_delete_checklist").css("display", "none");

            $("#id_div_buttonBar").css("display", "block");

            $('#MainContent_hfCheckListId').val(CheckListId);
            $('#MainContent_txtName').val(chklstName);

            $("#MainContent_DropDownListChecklistTypes option").filter(function () {
                //may want to use $.trim in here
                return $(this).text() == chklstType;
            }).prop('selected', true);

            $("#MainContent_DropDownListChecklistTypes").prop("disabled", true);

            LoadCheckListImage(CheckListId);
        }

        function LoadCheckListImage(CheckListId) {
            $("#div_loader").show();
            var param = { "CheckListId": CheckListId };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/SaveCheckListImageAndGetImageName",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != 'error') {
                        if (response.hasOwnProperty('d')) {
                            msg = response.d;
                        } else {
                            msg = response;
                        }
                        //debugger;
                        var json = JSON.parse(msg);

                        var imageName = json["ImageName"];

                        $("#MainContent_lblImageName").html(imageName);
                        //$('#MainContent_HiddenFieldSelectedImageName').val(imageName);
                        $("#MainContent_div_preview").css("display", "block");
                        $("#MainContent_div_preview").html('<img src="' + ROOT_PATH + 'docs/checklist_images/' + imageName + '">');
                        $("#div_loader").hide();
                    }
                    else {
                        $("#div_loader").hide();
                    }
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");
                    $("#div_loader").hide();
                }
            });
        }


        //        $('#<%=DropDownListChecklistTypes.ClientID%>').change(function (e) {
        //            var selectedOption = $('#<%=DropDownListChecklistTypes.ClientID%>').val();

        //            if (selectedOption == 1) {
        //                document.getElementById("repFieldSet").style.display = "none";
        //                document.getElementById("routeFieldSet").style.display = "none";
        //            }
        //            else if (selectedOption == 2) {
        //                
        //                // $('#routeFieldSet').style("display", "block");repFieldSet
        //                document.getElementById("repFieldSet").style.display = "none";
        //                document.getElementById("routeFieldSet").style.display = "block";
        //            }
        //            else if (selectedOption == 3) {
        //                
        //                document.getElementById("routeFieldSet").style.display = "none";
        //                document.getElementById("repFieldSet").style.display = "block";
        //            }
        //            else if (selectedOption == 4) {
        //                LoadCheckListSalesReps();
        //                document.getElementById("repFieldSet").style.display = "none";
        //                document.getElementById("routeFieldSet").style.display = "none";
        //            }
        //        });

        function CheckListOption(CheckListId, CheckListTypeId) {
            if (CheckListTypeId == 'Routes') {
                $("div#div_loader").show();
                $('#MainContent_hfCheckListId').val(CheckListId);
                loadRouteMasterGridForCheckList(CheckListId);
            }
            else if (CheckListTypeId == 'Outlets') {
                $("div#div_loader").show();
                var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=LeadStage&type=query&LeadStage=Customer";
                $.ajax({
                    url: root,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        $('#MainContent_hfCheckListId').val(CheckListId);
                        LoadCheckListCustomer(CheckListId);
                    },
                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                        }
                    }
                });
            }
//            else if (CheckListTypeId == 'Sales Reps') {
//                $("div#div_loader").show();
//                $('#MainContent_hfCheckListId').val(CheckListId);
//                loadSalesReps();
//            }

        }

        function InitializeChecklistImageUpload() {
            if ($('#MainContent_HiddenFieldSelectedImageName').val() == "") {
                $("#checklistImage").kendoUpload({
                    async: {
                        saveUrl: ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=checklistimage&type=insert",
                        autoUpload: true
                    },
                    upload: function onUpload(e) {
                        e.sender.options.async.saveUrl = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=checklistimage&type=insert";
                        var files = e.files;
                        // Check the extension of each file and abort the upload if it is not .jpg
                        $.each(files, function () {
                            //if (this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".gif" && this.extension.toLowerCase() != ".tif" && this.extension.toLowerCase() != ".tiff" && this.extension.toLowerCase() != ".png" && this.extension.toLowerCase() != ".bmp") {
                            if (this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".jpeg" && this.extension.toLowerCase() != ".png") {
                                e.preventDefault();
                                alert("Please Select an .JPG or .JPEG image!");
                            }
                        });
                    },
                    success: function onSuccess(e) {
                        //var ROOT_PATH = '/Peercore.CRM.WebCRM/'
                        //alert(filename);
                        var sessionId = $('#MainContent_hfSessionId').val();
                        //var filename = "CampaignImage_" + sessionId + "_" + e.files[0].name;
                        var filename = e.files[0].name;
                        //var fullFilename = e.files[0].rawFile.mozFullPath;
                        $('#MainContent_HiddenFieldSelectedImageName').val(filename);
                        $('#MainContent_HiddenFieldSelectedImageFullPath').val(ROOT_PATH + 'docs/product_images/' + filename);

                        $("#MainContent_lblImageName").html(filename);
                        $("#MainContent_div_preview").css("display", "block");
                        $("#MainContent_div_preview").html('<img src="' + ROOT_PATH + 'docs/checklist_images/' + filename + '">');
                        //$("#MainContent_divImagePreview").html('<img src="' + ROOT_PATH + 'docs/contactperson/' + filename + '">');
                    }
                });
            }
        }

        function IsGeneric(type) {
            var a = "Add Option";
            return a;
        }

        function loadCheckListGrid() {
            var take_grid = $("#MainContent_hfPageIndex").val();
            var url = "javascript:BtnEditCheckListHandler(\'#=ChecklistID#\',\'#=CheckListType#\',\'#=ChecklistName#\');";

            var s = "test";

            $("#div_loader").show();
            $("#divCheckList").html("");
            var urlType = "<a href=\"" + url + "\">#=CheckListType#</a>";
            var urlName = "<a href=\"" + url + "\">#=ChecklistName#</a>";
            $("#divCheckList").kendoGrid({
                height: 375,
                columns: [
                    { template: '<a href="javascript:DeleteCheckList(\'#=ChecklistID#\');">Delete</a>', field: "", width: "60px" },
                    { field: "ChecklistID", title: "CheckList Id", width: "85px", hidden: true },
                    { template: urlName, field: "ChecklistName", title: "Check-List Name", width: "300px" },
                    { field: "CheckListType", title: "Check-List Type", width: "300px" },
                    { template: '#if (CheckListType != \'Generic\') { #<a href="javascript:CheckListOption(\'#=ChecklistID#\',\'#=CheckListType#\');"> Add #=CheckListType#  </a> #}#', field: "", width: "150px" },
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                ChecklistID: { validation: { required: true} },
                                CheckListType: { editable: false, nullable: true },
                                ChecklistName: { editable: false, nullable: true }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllCheckListsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#divBrandGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        $(".check_row_outlet").live("click", function (e) {
            var chklst_id = $('#MainContent_hfCheckListId').val();
            var grid = $("#div_custgrid").data("kendoGrid");
            var checked = $(this).is(':checked');
            var col = $(this).closest('td');

            dataItem = grid.dataItem($(e.target).closest("tr"));

            $.ajax({
                url: ROOT_PATH + 'common_templates/process_forms/processmaster.aspx/UpdateCheckListCustomer',
                type: "POST",
                data: "{'CheckListId':'" + chklst_id + "', 'CustomerCode':'" + dataItem.CustomerCode + "', 'HasSelect':'" + checked + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //$("#Content").text(response.d);
                },
                failure: function (response) {
                    alert(response.d);
                }
            }).done(function (response) {
            });
        });

        $(".check_row_route").live("click", function (e) {
            var chklst_id = $('#MainContent_hfCheckListId').val();
            var grid = $("#div_chkroutegrid").data("kendoGrid");
            var checked = $(this).is(':checked');
            var col = $(this).closest('td');

            dataItem = grid.dataItem($(e.target).closest("tr"));
            $.ajax({
                url: ROOT_PATH + 'common_templates/process_forms/processmaster.aspx/UpdateCheckListRoute',
                type: "POST",
                data: "{'CheckListId':'" + chklst_id + "', 'RouteId':'" + dataItem.RouteMasterId + "', 'HasSelect':'" + checked + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //$("#Content").text(response.d);
                },
                failure: function (response) {
                    alert(response.d);
                }
            }).done(function (response) {
            });
        });

    </script>
</asp:Content>
