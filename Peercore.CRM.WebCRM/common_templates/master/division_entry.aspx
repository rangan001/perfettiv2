﻿<%@ Page Title="mSales - Division Entry" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="division_entry.aspx.cs" Inherits="common_templates_master_division_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:HiddenField ID="hfPageIndex" runat="server" />
<asp:HiddenField ID="OriginatorString" runat="server"/>
<asp:HiddenField ID="saveDeleteOption" runat="server"/>

<script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display:none; width:500px;">
            <div style="width:100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">
                                    Division Code
                                </td>
                                <td colspan="3">
                                <span class="wosub mand">
                                    &nbsp;<input type="text" class="textboxwidth" id="txtCode"/>
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Division Name
                                </td>
                                <td colspan="3">
                                <span class="wosub mand">
                                    &nbsp;<input type="text" class="textboxwidth" id="txtName"/>
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                    <asp:HiddenField ID="hdnSelectedDivisionId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_division" clientidmode="Static" runat="server" href="#">Add Division</a>
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
         <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
           <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="divDivisionGrid">
                </div>
            </div>
        </div>
    </div>

    <div id="popup">
        <div>
            <b>Enter Admin Password:</b></div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>

    <script type="text/javascript">
        var division_id;

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            loadDivisionGrid();
            $("#txtCode").focus();
        });


        //Load popup for Add new Brand
        $("#id_add_division").click(function () {
            $("#window").css("display", "block");
            $("#hdnSelectedDivisionId").val('');

            $("#txtCode").val('');
            $("#txtName").val('');
            $("#txtCode").prop('disabled', false);

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Division",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtCode").focus();
            wnd.center().open();
        });

        //Save button click
        $("#buttonSave").click(function () {
            //            DivisionEntrySaveDivision();
            showPopup(0, 1);
        });

        function showPopup(dID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            division_id = dID;
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    delete_selected_DeleteDivision(dID);
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    DivisionEntrySaveDivision();
            }
        }


        function confirmAdmin() {
            var password = document.getElementById("adminPass").value;
            var param = { "password": password };
            if (password == "") {
                var errorMsg = GetErrorMessageDiv("Please enter admin password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                                delete_selected_DeleteDivision(division_id);
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                                DivisionEntrySaveDivision();

                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin Password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            //                            showPopup();
                            $("#adminPass").val("");
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                        $("#adminPass").val("");
                    }

                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });

        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }


        $("#buttonClear").click(function () {
            $("#hdnSelectedDivisionId").val('');
            $("#txtCode").val('');
            $("#txtName").val('');
            $("#txtCode").prop('disabled', false);
        });

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Division",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

    </script>
</asp:Content>

