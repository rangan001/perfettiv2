﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using CRMServiceReference;
using Peercore.CRM.Shared;
using System.IO;
using System.Drawing;
using System.Web.Script.Serialization;
using System.Configuration;


public partial class common_templates_master_product_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //Set the Toolbar buttons
            buttonbar.VisibleSave(true);
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.EnableSave(true);
            

            if (!IsPostBack)
            {
                LoadOptions();
                hfSessionId.Value = Session.SessionID;
                //DropDownListNavigationType.SelectedIndex = 0;
                HiddenFieldSaveFailed.Value = "0";
                string caseDefaultValue = ConfigurationManager.AppSettings["CaseConfigDefault"];
                txtCaseConfiguration.Text = caseDefaultValue;
                string tonnageDefaultValue = ConfigurationManager.AppSettings["TonnageConfigdefault"];
                txtTonnageConfiguration.Text = tonnageDefaultValue;
                


                radiono.Checked = true;
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Product Entry ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.OriginatorString;
    }

    protected void ButtonSave_Click(object sender)
    {
        this.SaveProduct();
    }

    private bool IsProductCodeExists(ProductDTO productDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProductCodeExists(productDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsProductNameExists(ProductDTO productDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProductNameExists(productDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private void LoadBrandsToDropdown()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " name asc";

        CommonClient commonClient = new CommonClient();
        List<BrandDTO> brandList = new List<BrandDTO>();
        brandList = commonClient.GetAllBrandsDataAndCount(args);

        if (brandList.Count != 0)
        {
            cmbBrand.Items.Clear();
            cmbBrand.Items.Add(new ListItem("- Select -", "0"));
            foreach (BrandDTO item in brandList)
            {
                cmbBrand.Items.Add(new ListItem(item.BrandName, item.BrandId.ToString()));

            }
            //dropdownBankAccount.Items.Add(new ListItem("All", "All"));
            cmbBrand.Enabled = true;
           // cmbBrand.SelectedValue = cmbBrand.Items[0].ToString();
        }
        else 
        {
            cmbBrand.Items.Add(new ListItem("- Select -", "0"));
            cmbBrand.Enabled = false;
        }
    }

    private void LoadCategoriesToDropdown()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " id ";

        CommonClient commonClient = new CommonClient();
        List<ProductCategoryDTO> categoryList = new List<ProductCategoryDTO>();
        categoryList = commonClient.GetAllProductCategories(args);

        if (categoryList.Count != 0)
        {
            cmbCategory.Items.Clear();
            cmbCategory.Items.Add(new ListItem("- Select -", "0"));
            foreach (ProductCategoryDTO item in categoryList)
            {
                cmbCategory.Items.Add(new ListItem(item.Description, item.Id.ToString()));

            }
            //dropdownBankAccount.Items.Add(new ListItem("All", "All"));
            cmbCategory.Enabled = true;
            // cmbBrand.SelectedValue = cmbBrand.Items[0].ToString();
        }
        else
        {
            cmbCategory.Items.Add(new ListItem("- Select -", "0"));
            cmbCategory.Enabled = false;
        }
    }

    private void LoadProductPackingsToDropdown()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " packing_name asc";

        CommonClient commonClient = new CommonClient();
        List<ProductPackingDTO> packingsList = new List<ProductPackingDTO>();
        packingsList = commonClient.GetAllProductPackings(args);

        if (packingsList.Count != 0)
        {
            cmbPacking.Items.Clear();
            cmbPacking.Items.Add(new ListItem("- Select -", "0"));
            foreach (ProductPackingDTO item in packingsList)
            {
                cmbPacking.Items.Add(new ListItem(item.PackingName, item.PackingId.ToString()));
            }
            cmbPacking.Enabled = true;
        }
        else
        {
            cmbPacking.Items.Add(new ListItem("- Select -", "0"));
            cmbPacking.Enabled = false;
        }
    }

    private void LoadFlavorsToDropdown()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " flavor_name asc";

        CommonClient commonClient = new CommonClient();
        List<FlavorDTO> flavorsList = new List<FlavorDTO>();
        flavorsList = commonClient.GetAllFlavor();

        if (flavorsList.Count != 0)
        {
            cmbFlavor.Items.Clear();
            cmbFlavor.Items.Add(new ListItem("- Select -", "0"));
            foreach (FlavorDTO item in flavorsList)
            {
                cmbFlavor.Items.Add(new ListItem(item.FlavorName, item.FlavorId.ToString()));
            }
            cmbFlavor.Enabled = true;
        }
        else
        {
            cmbFlavor.Items.Add(new ListItem("- Select -", "0"));
            cmbFlavor.Enabled = false;
        }
    }

    private void LoadOptions()
    {
        LoadBrandsToDropdown();
        LoadCategoriesToDropdown();
        LoadProductPackingsToDropdown();
        LoadFlavorsToDropdown();
    }

    public void SaveProduct()
    {
        string jsonString = string.Empty;
        ProductDTO productDTO = new ProductDTO();
        CommonClient commonClient = new CommonClient();
        CommonUtility commonUtility = new CommonUtility();
        
        //string tonnagedefaultValue = 



        try
        {
            if (txtName.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please Enter Product Name.");
                HiddenFieldSaveFailed.Value = "1";
                return;
            }

            if (txtFgCode.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please Enter Product FG Code.");
                HiddenFieldSaveFailed.Value = "1";
                return;
            }

            if (txtCode.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please Enter Product Code.");
                HiddenFieldSaveFailed.Value = "1";
                return;
            }

            if (txtWeight.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please Enter Product Weight.");
                HiddenFieldSaveFailed.Value = "1";
                return;
            }

            if (txtCaseConfiguration.Text.Trim() == "")
            {
                txtCaseConfiguration.Text = "0";
                return;
            }
            if (txtTonnageConfiguration.Text.Trim() == "")
            {
                txtTonnageConfiguration.Text = "0";
                return;
            }


            string selectedImageName = !String.IsNullOrEmpty(HiddenFieldSelectedImageName.Value) ? HiddenFieldSelectedImageName.Value : null;
            int selectedProductId = !String.IsNullOrEmpty(HiddenFieldSelectedProductId.Value) ? Convert.ToInt32(HiddenFieldSelectedProductId.Value) : 0;

            if (commonClient.IsProductExsistInDiscountScheme(selectedProductId) >  0)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Cannot inactive this product. Because this product is exsisting in discount scheme.");
                HiddenFieldSaveFailed.Value = "1";
                return;
            }

            int brandId = Convert.ToInt32(cmbBrand.SelectedValue);
            int categoryId = Convert.ToInt32(cmbCategory.SelectedValue);
            int packingId = Convert.ToInt32(cmbPacking.SelectedValue);
            int flavourId = Convert.ToInt32(cmbFlavor.SelectedValue);
            

            float CaseConfigurationVal = float.Parse(txtCaseConfiguration.Text);

            float TonnageConfigurationVal = float.Parse(txtTonnageConfiguration.Text);

            string statusProduct = "A";
            if (ddlStatus.SelectedValue == "1")
            {
                statusProduct = "D";

                if (commonClient.IsProductExsistInDiscountScheme(selectedProductId) > 0)
                {
                    div_message.Attributes.Add("style", "display:block;padding:2px");
                    div_message.InnerHtml = commonUtility.GetErrorMessage("Cannot inactive this product. Because this product is exsisting in discount scheme.");
                    HiddenFieldSaveFailed.Value = "1";
                    return;
                }
            }

            string fgCode = txtFgCode.Text;
            string code = txtCode.Text;
            string name = txtName.Text;
            string weight = txtWeight.Text;

            bool isHighValue = false;
            if (radioyes.Checked)
                isHighValue = true;

            bool isPromoItem = false;
            if (radioPromoyes.Checked)
                isPromoItem = true;

            bool isMTItem = false;
            if (radioMTyes.Checked)
                isMTItem = true;

            bool isHalfQtyItem = false;
            if (radioHalfQtyItemYes.Checked)
                isHalfQtyItem = true;

            productDTO.ProductId = selectedProductId;
            productDTO.BrandId = brandId;
            productDTO.CategoryId = categoryId;
            productDTO.PackingId = packingId;
            productDTO.FlavourId = flavourId;
            productDTO.FgCode = fgCode;
            productDTO.Code = code;
            productDTO.Name = name;
            productDTO.Weight = Convert.ToDouble(weight);
            productDTO.IsHighValue = isHighValue;
            productDTO.IsPromoItem = isPromoItem;
            productDTO.IsMTItem = isMTItem;
            productDTO.IsHalfQtyItem = isHalfQtyItem;
            productDTO.CaseConfiguration = CaseConfigurationVal;
            productDTO.TonnageConfiguration = TonnageConfigurationVal;




            if (IsProductCodeExists(productDTO))
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Product Code Already Exists !");
                HiddenFieldSaveFailed.Value = "1";
                return;
            }
            if (IsProductNameExists(productDTO))
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Product Name Already Exists !");
                HiddenFieldSaveFailed.Value = "1";
                return;
            }

            //byte[] imageContent = null;
            //if (selectedImageName != null)
            //{
            //    string imagePath = ConfigUtil.ApplicationPath + "docs/product_images/" + selectedImageName;
            //    imageContent = System.IO.File.ReadAllBytes(Server.MapPath(imagePath));

            //    if(imageContent != null)
            //    {
            //        //  Rename Uploaded Image with the Product_Code.jpg
            //        try
            //        {
            //            string oldPath = ConfigUtil.ApplicationPath + "docs/product_images/" + selectedImageName;
            //            string newPath = ConfigUtil.ApplicationPath + "docs/product_images/" + productDTO.Code + ".jpg";

            //            //if new file exists, DElete it
            //            if(System.IO.File.Exists(Server.MapPath(newPath)))
            //                System.IO.File.Delete(Server.MapPath(newPath));

            //            System.IO.File.Move(Server.MapPath(oldPath), Server.MapPath(newPath));
            //        }
            //        catch (Exception ex)
            //        {
            //            div_message.Attributes.Add("style", "display:block");
            //            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            //            HiddenFieldSaveFailed.Value = "1";
            //            return;
            //        }
            //    }
            //}

            byte[] imageContent = null;
            if (selectedImageName != null)
            {
                string imagePath = System.AppDomain.CurrentDomain.BaseDirectory + "docs\\product_images\\" + selectedImageName;
                string imageOutPath = System.AppDomain.CurrentDomain.BaseDirectory + "docs\\product_images\\temp\\" + selectedImageName;
                // Load image.
                System.Drawing.Image image = System.Drawing.Image.FromFile(imagePath);

                // Compute thumbnail size.
                Size thumbnailSize = GetThumbnailSize(image);

                // Get thumbnail.
                System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                    thumbnailSize.Height, null, IntPtr.Zero);

                // Save thumbnail.
                thumbnail.Save(imageOutPath);

                //VaryQualityLevel(imagePath, selectedImageName);

                string imageReadPath = ConfigUtil.ApplicationPath + "docs/product_images/temp/" + selectedImageName;
                imageContent = System.IO.File.ReadAllBytes(Server.MapPath(imageReadPath));

                if (imageContent != null)
                {
                    //  Rename Uploaded Image with the Product_Code.jpg
                    try
                    {
                        string oldPath = ConfigUtil.ApplicationPath + "docs/product_images/temp/" + selectedImageName;
                        string newPath = ConfigUtil.ApplicationPath + "docs/product_images/temp/" + productDTO.Code + ".jpg";
                        string AllTempFolder = System.AppDomain.CurrentDomain.BaseDirectory + "docs/product_images/temp/";
                        string AllFolder = System.AppDomain.CurrentDomain.BaseDirectory + "docs/product_images/";

                        ////if new file exists, Delete it
                        //if (System.IO.File.Exists(Server.MapPath(oldPath)))
                        //    System.IO.File.Delete(Server.MapPath(oldPath));

                        ////if new file exists, Delete it
                        //if(System.IO.File.Exists(Server.MapPath(newPath)))
                        //    System.IO.File.Delete(Server.MapPath(newPath));

                        //System.IO.File.Move(Server.MapPath(oldPath), Server.MapPath(newPath));

                        //System.IO.File.Delete(Server.MapPath(AllTempFile));
                        System.IO.DirectoryInfo di = new DirectoryInfo(AllTempFolder);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }

                        //di = new DirectoryInfo(AllFolder);
                        //foreach (FileInfo file in di.GetFiles())
                        //{
                        //    file.Delete();
                        //}
                    }
                    catch (Exception ex)
                    {
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
                        HiddenFieldSaveFailed.Value = "1";
                        return;
                    }
                }
            }

            productDTO.ImageContent = imageContent;
            productDTO.Status = statusProduct;
            productDTO.CreatedBy = UserSession.Instance.UserName;
            productDTO.LastModifiedBy = UserSession.Instance.UserName;

            if (OriginatorString.Value == "ADMIN")
            {
                bool status = false;
                status = commonClient.SaveProduct(productDTO);

                if (status == true)
                {
                    div_message.Attributes.Add("style", "display:block");
                    div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                    HiddenFieldSelectedProductId.Value = "";
                    HiddenFieldSelectedImageName.Value = "";
                    HiddenFieldSelectedLinkOrFileName.Value = "";
                    HiddenFieldSaveFailed.Value = "0";
                }
                else
                {
                    div_message.Attributes.Add("style", "display:block");
                    div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
                    HiddenFieldSaveFailed.Value = "1";
                }
            }
            else
            {
                var json = new JavaScriptSerializer().Serialize(productDTO);
                Session["ProdDTO"] = json;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "showPopup", "showPopup()", true);
                HiddenFieldSaveFailed.Value = "1";
            }

            //bool status = false;
            //status = commonClient.SaveProduct(productDTO);

            //if (status == true)
            //{
            //    div_message.Attributes.Add("style", "display:block");
            //    div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
            //    HiddenFieldSelectedProductId.Value = "";
            //    HiddenFieldSelectedImageName.Value = "";
            //    HiddenFieldSelectedLinkOrFileName.Value = "";
            //    HiddenFieldSaveFailed.Value = "0";
            //}
            //else
            //{
            //    div_message.Attributes.Add("style", "display:block");
            //    div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            //    HiddenFieldSaveFailed.Value = "1";
            //}
        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            HiddenFieldSaveFailed.Value = "1";
            throw;
        }
    }

    private static Size GetThumbnailSize(System.Drawing.Image original)
    {
        // Maximum size of any dimension.
        const int maxPixels = 120;

        // Width and height.
        int originalWidth = original.Width;
        int originalHeight = original.Height;

        // Compute best factor to scale entire image based on larger dimension.
        double factor;
        if (originalWidth > originalHeight)
        {
            factor = (double)maxPixels / originalWidth;
        }
        else
        {
            factor = (double)maxPixels / originalHeight;
        }

        // Return thumbnail size.
        return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static string ASMSaveProduct(string ProductDTOObj)
    {
        CommonClient commonClient = new CommonClient();
        JavaScriptSerializer serializer = new JavaScriptSerializer();

        ProductDTO productDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ProductDTO>(ProductDTOObj);

        bool status = false;
        try
        {
            status = commonClient.SaveProduct(productDTO);
        }
        catch (Exception)
        {
        }
        return serializer.Serialize(status);

    }
}