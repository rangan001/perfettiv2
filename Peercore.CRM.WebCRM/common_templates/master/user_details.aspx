﻿<%@ Page Title="mSales - User Details" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="user_details.aspx.cs" Inherits="common_templates_master_user_details" %>

<%@ Register Src="~/usercontrols/salesinfo.ascx" TagPrefix="ucl1" TagName="salesinfo" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px;">

            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>

                            <tr>
                                <td class="textalignbottom">Originator
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtOriginatorValue" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Name
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtUserNameValue" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="textalignbottom">Designation
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtDesignationValue" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>


                            <tr>
                                <td class="textalignbottom">Password
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="password" class="textboxwidth input_numeric_price"
                                        id="txtPasswordValue" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="textalignbottom">UserType
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<asp:DropDownList Height="20px" ID="DropDownListUserTypes"
                                        CssClass="input-large1" runat="server">
                                    </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="textalignbottom">Mobile
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtMobile" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">
                                        Save</button>
                                    <button class="k-button" id="buttonClear">
                                        Clear</button>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <%--  for update--%>
        <div id="windowforUpdate" style="display: none; width: 500px;">

            <div style="width: 100%" id="Div2">
                <div id="div4">
                    <div id="div5" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>

                            <tr>
                                <td class="textalignbottom">Originator
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtOriginatorValueforUpdate" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Name
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtUserNameValueforUpdate" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="textalignbottom">Designation
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtDesignationValueforUpdate" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>


                            <tr>
                                <td class="textalignbottom">Password
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input class="textboxwidth input_numeric_price"
                                        id="txtPasswordValueforUpdate" type="password" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="textalignbottom">UserType
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<asp:DropDownList Height="20px" ID="DropDownListUserTypesforUpdate"
                                        CssClass="input-large1" runat="server">
                                    </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="textalignbottom">Mobile
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtMobileEdit" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="button1" type="button">
                                        Save</button>
                                    <button class="k-button" id="button2">
                                        Clear</button>
                                    <asp:HiddenField ID="userId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_user" clientidmode="Static" runat="server" href="#">Add New User</a>
                    </div>
                </div>
            </div>

            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>


        </div>

        <div class="clearall">
        </div>


        <div id="div_message" runat="server" style="display: none;"></div>
        <div class="clearall">
        </div>

        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="originator">
                </div>
            </div>
        </div>

    </div>



    <script type="text/javascript">

        $(document).ready(function () {
            loadOriginatorGrid();
        });

        function loadOriginatorGrid() {

            var take_grid = $("#MainContent_hfPageIndex").val();
            var url = "javascript:updateOriginatorDetails('#=UserId#','#=Originator#','#=UserName#','#=designation#','#=user_type#','#=Mobile#');";

            var urlOri = "<a href=\"" + url + "\">#=Originator#</a>";
            //                     $("#div_loader").show();
            $("#originator").html("");
            $("#originator").kendoGrid({
                height: 355,
                columns: [
                    { field: "UserId", title: "UserId", width: "85px", hidden: true },
                    { template: urlOri, field: "Originator", title: "Originator", width: "40px" },
                    { field: "UserName", title: "Name", width: "40px" },
                    { field: "designation", title: "Designation", width: "40px" },
                    { field: "type_des", title: "User Type", width: "40px" },
                    { field: "Mobile", title: "Mobile No", width: "40px" },
                    { template: '<a href="javascript:DeleteUser(\'#=UserId#\');">Delete</a>', field: "", width: "40px" },
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",

                        model: { // define the model of the data source. Required for validation and property types.
                            fields: {
                                UserId: { validation: { required: true } },
                                Originator: { editable: false, nullable: true },
                                UserName: { editable: false, nullable: true },
                                designation: { editable: false, nullable: true },
                                type_des: { editable: false, nullable: true },
                                user_type: { editable: false, nullable: true },
                                Mobile: { editable: false, nullable: true }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetOriginators", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    $("#div_loader").hide();
                                    var entityGrid = $("#originator").data("kendoGrid");

                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));

                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        function DeleteUser(UserId) {

            param = { "UserId": UserId };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/deleteUser",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.d == 'successful') {

                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        loadOriginatorGrid();
                        hideStatusDiv("MainContent_div_message");
                    }

                    else {
                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });
        }


        //Load popup for Add new User
        $("#id_add_user").click(function () {
            $("#window").css("display", "block");
            $("#txtOriginatorValue").val('');
            $("#txtUserNameValue").val('');
            $("#txtDesignationValue").val('');
            $("#txtPasswordValue").val('');
            $("#txtMobile").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit User",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().open();
        });

        //Save button click
        $("#buttonSave").click(function () {
            addNewUser();
        });

        //Button Clear Clicked in AddNewUser
        $("#buttonClear").click(function () {
            $("#txtOriginatorValue").val('');
            $("#txtUserNameValue").val('');
            $("#txtDesignationValue").val('');
            $("#txtPasswordValue").val('');
            $("#txtMobile").val('');
        });

        //Button Clear Clicked in updateUser
        $("#button2").click(function () {
            $("#txtOriginatorValueforUpdate").val('');
            $("#txtUserNameValueforUpdate").val('');
            $("#txtDesignationValueforUpdate").val('');
            $("#txtPasswordValueforUpdate").val('');
            $("#txtMobileEdit").val('');
        });

        function addNewUser() {
            var originator = document.getElementById("txtOriginatorValue").value;
            var userName = document.getElementById("txtUserNameValue").value;
            var designation = document.getElementById("txtDesignationValue").value;
            var password = document.getElementById("txtPasswordValue").value;
            var userType = $("select[id$=DropDownListUserTypes]").val();
            var mobile = document.getElementById("txtMobile").value;

            param = {
                "originator": originator,
                "userName": userName,
                "designation": designation,
                "password": password,
                "userType": userType,
                "mobile": mobile
            };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/addNewUser",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.d == 'successful') {
                        closepopup();
                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Saved.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        loadOriginatorGrid();
                        hideStatusDiv("MainContent_div_message");

                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });
        }

        function updateUser() {

            var originator = document.getElementById("txtOriginatorValueforUpdate").value;
            var userName = document.getElementById("txtUserNameValueforUpdate").value;
            var designation = document.getElementById("txtDesignationValueforUpdate").value;
            var password = document.getElementById("txtPasswordValueforUpdate").value;

            var userType = $("select[id$=DropDownListUserTypesforUpdate]").val();
            var userId = document.getElementById("userId").value;
            var mobile = document.getElementById("txtMobileEdit").value;

            param = {
                "originator": originator,
                "userName": userName,
                "designation": designation,
                "password": password,
                "userType": userType,
                "UserID": userId,
                "mobile": mobile
            };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/updateUser",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.d == 'successful') {

                        closeUpdatePopup();
                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Updated.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        loadOriginatorGrid();
                        hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });
        }

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit User",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        function closeUpdatePopup() {
            var wnd = $("#windowforUpdate").kendoWindow({
                title: "Add/Edit User",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#windowforUpdate").css("display", "none");
        }

        function updateOriginatorDetails(UserId, Originator, UserName, designation, type_des, mobile) {
            $("#userId").val(UserId);
            $("#txtOriginatorValueforUpdate").val(Originator);
            $("#txtUserNameValueforUpdate").val(UserName);
            $("#txtDesignationValueforUpdate").val(designation);
            $("#txtMobileEdit").val(mobile);
            $("#txtPasswordValueforUpdate").val('');
            $("#MainContent_DropDownListUserTypesforUpdate").val(type_des);

            $("#windowforUpdate").css("display", "block");
            var wnd = $("#windowforUpdate").kendoWindow({
                title: "Add/Edit User",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().open();
        }

        $("#button1").click(function () {
            updateUser();
        });

    </script>
</asp:Content>
