﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CRMServiceReference;

public partial class common_templates_master_user_details : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        if (!IsPostBack)
        {

        }

        Master.SetBreadCrumb("System Users ", "#", "");
        LoadOptions();
    }

    private void LoadOptions()
    {
        CommonClient commonClient = new CommonClient();
        List<OriginatorEntity> originatorTypeList = new List<OriginatorEntity>();
        originatorTypeList = commonClient.getAllOriginatorTypesForDropdown();
        if (originatorTypeList.Count != 0)
        {
            DropDownListUserTypes.Items.Clear();
            DropDownListUserTypes.Items.Add(new ListItem("- Select User Type -", "0"));
            foreach (OriginatorEntity item in originatorTypeList)
            {
                DropDownListUserTypes.Items.Add(new ListItem(item.type_des, item.user_type));
            }
            DropDownListUserTypes.Enabled = true;

            //for update

            DropDownListUserTypesforUpdate.Items.Clear();
            DropDownListUserTypesforUpdate.Items.Add(new ListItem("- Select User Type -", "0"));
            foreach (OriginatorEntity item in originatorTypeList)
            {
                DropDownListUserTypesforUpdate.Items.Add(new ListItem(item.type_des, item.user_type));
            }
            DropDownListUserTypesforUpdate.Enabled = true;

        }

    }
}
