﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Text;
using Peercore.CRM.Shared;
using System.IO;
using System.Threading.Tasks;

public partial class common_templates_process_forms_processmaster : System.Web.UI.Page
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "insert")
                {
                    if (Request.QueryString["fm"] == "brandentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveBrand();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "areaentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveArea();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "outlettypeentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveOutletType();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "productflavorentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveProductFlavor();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "productpackingmethod")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveProductPacking();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "productpriceentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveProductPrice();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "divisionentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveDivision();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "productcategoryentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveProductCategory();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }

                if (Request.QueryString["type"] == "insert")
                {
                    if (Request.QueryString["fm"] == "productentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveProduct();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "productimage")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = SaveProductImage();
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "customerimage")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = SaveCustomerImage();
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "checklistimage")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = SaveChecklistImage();
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                }

                if (Request.QueryString["type"] == "select")
                {
                    if (Request.QueryString["fm"] == "proddivi")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetDivisions();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }
                if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "addproductdivision")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = AddProductDivision();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "deleteproductdivision")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteProductDivision();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }

                else if (Request.QueryString["type"] == "delete")
                {
                    if (Request.QueryString["fm"].ToLower() == "deleteproductcategory")
                    {
                        // int marketid = Convert.ToInt32(Request.QueryString["marketid"].ToString());
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteOriginatorProductCategory();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "deleteproductflavour")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteProductFlavour();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "deletebrand")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteBrand();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "deletepackingmethod")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeletePacking();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "deleteoutlettype")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteOutletType();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "deletedivision")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteDivision();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "deletearea")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteArea();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["type"] == "delete")
                    {
                        if (Request.QueryString["fm"].ToLower() == "deleteproductinvoice")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteInvoice();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                        else if (Request.QueryString["fm"].ToLower() == "deleteproductorder")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteOrder();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                        else if (Request.QueryString["fm"] == "deletebulkinvoice")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteBulkInvoices(Request.QueryString["val"].ToString());
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                        else if (Request.QueryString["fm"] == "deletebulkorder")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteBulkOrders(Request.QueryString["val"].ToString());
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                    }
                    
                }

                
            }
        }
    }

    #endregion

    private string DeleteBulkInvoices(string custList)
    {
        var userName = UserSession.Instance.UserName;
        var userType = UserSession.Instance.Originator;

        CommonClient op = new CommonClient();
        bool success = op.DeleteBulkInvoices(userType, userName, custList);

        string txt = "";
        if (success)
        {
            txt = "1";
        }
        else
        {
            txt = "0";
        }
        return txt.ToString();
        //return txt;
    }


    private string DeleteBulkOrders(string custList)
    {
        var userName = UserSession.Instance.UserName;
        var userType = UserSession.Instance.Originator;

        CustomerClient op = new CustomerClient();
        bool success = op.DeleteBulkCust(userType, userName, custList);

        string txt = "";
        if (success)
        {
            txt = "1";
        }
        else
        {
            txt = "0";
        }
        return txt.ToString();
        //return "";
    }

    private string DeleteArea()
    {
        try
        {
            CommonClient commonService = new CommonClient();
            int areaId = 0;

            if (Request.QueryString["selectedAreaId"] != null && Request.QueryString["selectedAreaId"] != "")
            {
                areaId = Convert.ToInt32(Request.QueryString["selectedAreaId"].ToString());
            }

            bool status = commonService.DeleteArea(areaId);


            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private string DeleteDivision()
    {
        try
        {
            CommonClient commonService = new CommonClient();
            int divisionId = 0;

            if (Request.QueryString["selecteddivisionId"] != null && Request.QueryString["selecteddivisionId"] != "")
            {
                divisionId = Convert.ToInt32(Request.QueryString["selecteddivisionId"].ToString());
            }

            bool status = commonService.DeleteDivision(divisionId);


            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private string DeleteBrand()
    {
        try
        {
            CommonClient commonService = new CommonClient();
            int Id = 0;

            if (Request.QueryString["selectedBrandId"] != null && Request.QueryString["selectedBrandId"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["selectedBrandId"].ToString());
            }

            bool status = commonService.DeleteBrand(Id);


            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private string DeleteOriginatorProductCategory()
    {
        try
        {
            CommonClient commonService = new CommonClient();
            ProductCategoryDTO deletedproductcategory = new ProductCategoryDTO();
            //List<ProductCategoryDTO> productcategoryList = null;
            //productcategoryList = (Session[CommonUtility.ORIGINATOR_MARKETS] != null) ? (List<ProductCategoryDTO>)Session[CommonUtility.ORIGINATOR_MARKETS] : new List<ProductCategoryDTO>();
            int Id = 0;

            if (Request.QueryString["catid"] != null && Request.QueryString["catid"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["catid"].ToString());
            }

            deletedproductcategory.Id = Id;
            bool status = commonService.DeleteProductCategory(deletedproductcategory);

            //int index = productcategoryList.FindIndex(x => x.Id == Id);
            //productcategoryList.RemoveAt(index);
            //Session[CommonUtility.ORIGINATOR_MARKETS] = productcategoryList;
            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private string DeleteInvoice()
    {
        try
        {
            int Id = 0;
            CommonClient commonService = new CommonClient();
            InvoiceHeaderDTO deleteinvoice = new InvoiceHeaderDTO();
            
            if (Request.QueryString["invid"] != null && Request.QueryString["invid"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["invid"].ToString());
            }

            deleteinvoice.InvoiceId = Id;
            deleteinvoice.CreatedBy = UserSession.Instance.UserName;
            bool status = commonService.DeleteInvoice(deleteinvoice);

            //int index = productcategoryList.FindIndex(x => x.Id == Id);
            //productcategoryList.RemoveAt(index);
            //Session[CommonUtility.ORIGINATOR_MARKETS] = productcategoryList;
            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }
    
    private string DeleteOrder()
    {
        try
        {
            int Id = 0;
            OrderClient orderService = new OrderClient();

            if (Request.QueryString["orderid"] != null && Request.QueryString["orderid"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["orderid"].ToString());
            }

            bool status = orderService.DeleteOrder(UserSession.Instance.UserName, Id);

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    public string AddProductDivision()
    {
        string returnsave = string.Empty;
        try
        {
            int pid = string.IsNullOrEmpty(Request.QueryString["pid"]) ? 0 : int.Parse(Request.QueryString["pid"]);
            int did = string.IsNullOrEmpty(Request.QueryString["did"]) ? 0 : int.Parse(Request.QueryString["did"]);
            string pname = Request.QueryString["pname"];
            string dname = Request.QueryString["dname"];
            List<ProductDTO> poroductList = null;

            if (Session[CommonUtility.DIVISION_PRODUCTS] != null)
            {
                poroductList = (List<ProductDTO>)Session[CommonUtility.DIVISION_PRODUCTS];
            }
            else
            {
                poroductList = new List<ProductDTO>();
            }

            //ArgsDTO args = new ArgsDTO();
            //args.StartIndex = 1;
            //args.OrderBy = " name asc";
            //args.AdditionalParams = "";
            //args.RowCount = 1000;
            //CommonClient cmClient = new CommonClient();
            //poroductList = cmClient.GetAllProductsDataAndCount(args);

            if (!poroductList.Any(c => c.ProductId == pid && c.DivisionId == did))
            {
                poroductList.Add(new ProductDTO() { ProductId = pid, DivisionId = did, Name = pname, DivisionName = dname });
                CommonClient commonClient = new CommonClient();
                List<ProductDTO> productList = new List<ProductDTO>();
                ProductDTO productDTO = new ProductDTO();
                productDTO.ProductId = pid;
                productDTO.DivisionId = did;
                productDTO.CreatedBy = UserSession.Instance.UserName;
                productList.Add(productDTO);

                bool status = commonClient.SaveProductForDivision(did, productDTO);

                if (status)
                {
                    Session[CommonUtility.DIVISION_PRODUCTS] = null;
                }

                returnsave = (status == true) ? "true" : "false";
            }
        }
        catch (Exception ex)
        {
        }
        return returnsave;
    }
    public string DeleteProductDivision()
    {
        try
        {
            CommonClient commonClient = new CommonClient();
            int pid = string.IsNullOrEmpty(Request.QueryString["pid"]) ? 0 : int.Parse(Request.QueryString["pid"]);
            int did = string.IsNullOrEmpty(Request.QueryString["did"]) ? 0 : int.Parse(Request.QueryString["did"]);

            List<ProductDTO> poroductList = null;

            if (Session[CommonUtility.DIVISION_PRODUCTS] != null)
            {
                poroductList = (List<ProductDTO>)Session[CommonUtility.DIVISION_PRODUCTS];
            }
            else
            {
                poroductList = new List<ProductDTO>();
            }

            if (poroductList.Any(c => c.ProductId == pid))
            {
                poroductList.RemoveAll(c => c.ProductId == pid);
                bool success = commonClient.DeleteProductDevision(did, pid);

                if (success == true)
                    return "y";
            }
        }
        catch (Exception ex) { }

        return "n";
    }

    private string SaveProductFlavor()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedFlavorId = Request.QueryString["selectedFlavorId"];
            string flavorName = Request.QueryString["flavorName"];

            FlavorDTO flavorDTO = new FlavorDTO();
            flavorDTO.FlavorId = string.IsNullOrEmpty(selectedFlavorId) ? 0 : Convert.ToInt32(selectedFlavorId);
            flavorDTO.FlavorName = flavorName;

            if (IsProductFlavorNameExists(flavorDTO))
                return "nameExists";

            bool status = false;
            status = commonClient.SaveProductFlavor(flavorDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private string SaveProductPacking()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedPackingId = Request.QueryString["selectedPackingId"];
            string packingName = Request.QueryString["packingName"];

            ProductPackingDTO packingDTO = new ProductPackingDTO();
            packingDTO.PackingId = string.IsNullOrEmpty(selectedPackingId) ? 0 : Convert.ToInt32(selectedPackingId);
            packingDTO.PackingName = packingName;

            if (IsProductPackingNameExists(packingDTO))
                return "nameExists";

            bool status = false;
            status = commonClient.SaveProductPacking(packingDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private string SaveBrand()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedBrandId = Request.QueryString["selectedBrandId"];
            string brandCode = Server.HtmlDecode(Request.QueryString["brandCode"]);
            string brandName = Request.QueryString["brandName"];

            BrandDTO brandDTO = new BrandDTO();
            brandDTO.BrandId = string.IsNullOrEmpty(selectedBrandId) ? 0 : Convert.ToInt32(selectedBrandId);
            brandDTO.BrandCode = brandCode;
            brandDTO.BrandName = brandName;
            brandDTO.Status = "A";
            brandDTO.CreatedBy = UserSession.Instance.UserName;
            brandDTO.LastModifiedBy = UserSession.Instance.UserName;

            if (IsBrandCodeExists(brandDTO))
                return "codeExists";
            if (IsBrandNameExists(brandDTO))
                return "nameExists";

            bool status = false;
            status = commonClient.SaveBrand(brandDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private string SaveArea()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedAreaId = Request.QueryString["selectedAreaId"];
            //string brandCode = Server.HtmlDecode(Request.QueryString["brandCode"]);
            string areaName = Request.QueryString["areaName"];

            CityAreaDTO areaDTO = new CityAreaDTO();
            areaDTO.AreaID = string.IsNullOrEmpty(selectedAreaId) ? 0 : Convert.ToInt32(selectedAreaId);
            areaDTO.AreaCode = areaName;
            areaDTO.AreaName = areaName;
            //brandDTO.Status = "A";
            //brandDTO.CreatedBy = UserSession.Instance.UserName;
            //brandDTO.LastModifiedBy = UserSession.Instance.UserName;

            if (IsAreaNameExists(areaDTO))
                return "nameExists";

            bool status = false;
            status = commonClient.SaveArea(areaDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private string SaveOutletType()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedOutletTypeId = Request.QueryString["selectedOutletTypeId"];
            //string brandCode = Server.HtmlDecode(Request.QueryString["brandCode"]);
            string outlettypeName = Request.QueryString["outlettypeName"];

            OutletTypeDTO outlettypeDTO = new OutletTypeDTO();
            outlettypeDTO.Outlettype_id = string.IsNullOrEmpty(selectedOutletTypeId) ? 0 : Convert.ToInt32(selectedOutletTypeId);
            outlettypeDTO.Outlettype_name = outlettypeName;

            if (IsOutletTypeExists(outlettypeDTO))
                return "nameExists";

            bool status = false;
            status = commonClient.SaveOutletType(outlettypeDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private string SaveProductCategory()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedCategoryId = Request.QueryString["selectedCategoryId"];
            string categoryCode = Server.HtmlDecode(Request.QueryString["categoryCode"]);
            string categoryName = Request.QueryString["categoryName"];

            ProductCategoryDTO categoryDTO = new ProductCategoryDTO();
            categoryDTO.Id = string.IsNullOrEmpty(selectedCategoryId) ? 0 : Convert.ToInt32(selectedCategoryId);
            categoryDTO.Code = categoryCode;
            categoryDTO.Description = categoryName;
            //categoryDTO.Status = "A";
            //categoryDTO.CreatedBy = UserSession.Instance.UserName;
            //categoryDTO.LastModifiedBy = UserSession.Instance.UserName;

            if (IsProductCategoryCodeExists(categoryDTO))
                return "codeExists";
            if (IsProductCategoryNameExists(categoryDTO))
                return "nameExists";

            bool status = false;
            status = commonClient.SaveProductCategory(categoryDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }


    private string SaveDivision()
    {
        StringBuilder sb = new StringBuilder();
        DiscountSchemeClient discountClient = new DiscountSchemeClient();
        string str = string.Empty;

        try
        {
            string selectedDivisionId = Request.QueryString["selectedDiviId"];
            string divisionCode = Server.HtmlDecode(Request.QueryString["code"]);
            string divisionName = Request.QueryString["name"];

            DivisionDTO divisionDTO = new DivisionDTO();
            divisionDTO.DivisionId = string.IsNullOrEmpty(selectedDivisionId) ? 0 : Convert.ToInt32(selectedDivisionId);
            divisionDTO.DivisionCode = divisionCode;
            divisionDTO.DivisionName = divisionName;
            divisionDTO.IsActive = true;
            divisionDTO.CreatedBy = UserSession.Instance.UserName;
            divisionDTO.LastModifiedBy = UserSession.Instance.UserName;

            if (IsDivisionCodeExists(divisionDTO))
                return "codeExists";
            if (IsDivisionNameExists(divisionDTO))
                return "nameExists";

            bool status = false;
            int divisionId = 0;
            status = discountClient.SaveDivision(divisionDTO, out divisionId);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private string SaveProduct()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedProductId = Request.QueryString["selectedProdId"];
            string productCode = Server.HtmlDecode(Request.QueryString["code"]);
            string productName = Request.QueryString["name"];
            string productBrand = Request.QueryString["brand"];
            string productCategory = Request.QueryString["cat"];
            string productSKU = Request.QueryString["sku"];

            ProductDTO productDTO = new ProductDTO();
            productDTO.ProductId = string.IsNullOrEmpty(selectedProductId) ? 0 : Convert.ToInt32(selectedProductId);
            productDTO.Code = productCode;
            productDTO.Name = productName;
            productDTO.BrandId = string.IsNullOrEmpty(productBrand) ? 0 : Convert.ToInt32(productBrand);
            productDTO.CategoryId = string.IsNullOrEmpty(productCategory) ? 0 : Convert.ToInt32(productCategory);
            productDTO.Sku = string.IsNullOrEmpty(productSKU) ? 0 : Convert.ToInt32(productSKU);
            productDTO.Status = "A";
            productDTO.CreatedBy = UserSession.Instance.UserName;
            productDTO.LastModifiedBy = UserSession.Instance.UserName;

            if (IsProductCodeExists(productDTO))
                return "codeExists";
            if (IsProductNameExists(productDTO))
                return "nameExists";

            bool status = false;
            status = commonClient.SaveProduct(productDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }


    private bool IsBrandCodeExists(BrandDTO brandDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsBrandCodeExists(brandDTO); 
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsAreaNameExists(CityAreaDTO areaDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsAreaNameExists(areaDTO); 
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsOutletTypeExists(OutletTypeDTO outlettypeDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsOutletTypeExists(outlettypeDTO); 
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsProductFlavorNameExists(FlavorDTO flavorDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProductFlavorNameExists(flavorDTO); 
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsProductPackingNameExists(ProductPackingDTO packingDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProductPackingNameExists(packingDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsBrandNameExists(BrandDTO brandDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsBrandNameExists(brandDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsProductCategoryCodeExists(ProductCategoryDTO productcategoryDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProductCategoryCodeExists(productcategoryDTO);
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsProductCategoryNameExists(ProductCategoryDTO productcategoryDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProductCategoryNameExists(productcategoryDTO);
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsProductNameExists(ProductDTO productDTO) {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProductNameExists(productDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsProductCodeExists(ProductDTO productDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProductCodeExists(productDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private string SaveProductPrice()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedProductId = Request.QueryString["selectedProductId"];
            string selectedProductPriceId = Request.QueryString["selectedProductPriceId"];
            string retailPrice = Server.HtmlDecode(Request.QueryString["retailPrice"]);
            string consumerPrice = Request.QueryString["consumerPrice"];
            string distPrice = Request.QueryString["distPrice"];

            ProductDTO productDTO = new ProductDTO();
            productDTO.ProductPriceId = string.IsNullOrEmpty(selectedProductPriceId) ? 0 : Convert.ToInt32(selectedProductPriceId);
            productDTO.ProductId = string.IsNullOrEmpty(selectedProductId) ? 0 : Convert.ToInt32(selectedProductId);
            productDTO.RetailPrice = string.IsNullOrEmpty(retailPrice) ? 0 : Math.Round(Convert.ToDouble(retailPrice), 2);
            productDTO.OutletPrice = string.IsNullOrEmpty(consumerPrice) ? 0 : Math.Round(Convert.ToDouble(consumerPrice), 2);
            productDTO.DistributorPrice = string.IsNullOrEmpty(distPrice) ? 0 : Math.Round(Convert.ToDouble(distPrice), 2);

            productDTO.CreatedBy = UserSession.Instance.UserName;
            productDTO.LastModifiedBy = UserSession.Instance.UserName;

            bool status = false;
            status = commonClient.SaveProductPrice(productDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private bool IsDivisionCodeExists(DivisionDTO divisionDTO)
    {
        return false;
        //CommonClient commonClient = new CommonClient();
        //try
        //{
        //    return commonClient.IsBrandCodeExists(brandDTO); ;
        //}
        //catch (Exception)
        //{
        //    return true;
        //}
    }

    private bool IsDivisionNameExists(DivisionDTO divisionDTO)
    {
        return false;
        //CommonClient commonClient = new CommonClient();
        //try
        //{
        //    return commonClient.IsBrandNameExists(brandDTO); ;
        //}
        //catch (Exception)
        //{
        //    return true;
        //}
    }

    private string GetDivisions()
    {
        StringBuilder txt = new StringBuilder();
        DiscountSchemeClient schemeClient = new DiscountSchemeClient();
        ArgsDTO args = new ArgsDTO();

        //if (UserSession.Instance != null)
        //    args.ChildOriginators = UserSession.Instance.ChildOriginators;

        //if (Request.QueryString["catergory"] != null && Request.QueryString["catergory"] != "")
        //{
        //    args.AdditionalParams = " dept_string = '" + Request.QueryString["catergory"].ToUpper() + "'";
        //}

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;
        args.OrderBy = " division_name asc ";

        List<DivisionDTO> divisionList = schemeClient.GetAllDivisions(args);

        if (divisionList != null && divisionList.Count != 0)
        {
            txt.Append("<table border=\"0\" id=\"divisionTable\" width=\"270px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Division</th>");
            txt.Append("</tr>");

            for (int i = 0; i < divisionList.Count; i++)
            {
                txt.Append("<tr>");
                txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + divisionList[i].DivisionName + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + divisionList[i].DivisionId + "\" id=\"b\"/>   </td>");
                txt.Append("<td align=\"left\" class=\"tblborder\">" + divisionList[i].DivisionName + "</td>");
                txt.Append("</tr>");
            }

            txt.Append("</table>");

            txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");
        }
        else
        {
            txt.Append("<table border=\"0\" id=\"divisionTable\" width=\"270px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Id</th>");
            txt.Append("<th>Name</th>");
            txt.Append("</tr>");
            txt.Append("</table>");
        }

        return txt.ToString();
    }

    public string SaveProductImage()
    {
        //var isUpload = "0";
        string filename = "";
        try
        {
            //var db = new mypicksDBDataContext();
            HttpPostedFile postedFile = Request.Files["productImage"];
            string savepath = Server.MapPath("../../docs/product_images/");
            //string filename = GenerateFileName(postedFile.FileName, Session.SessionID);

            //filename = "CampaignImage_" + Session.SessionID + "_" + postedFile.FileName;
            filename = postedFile.FileName;
            if (!Directory.Exists(savepath))
                Directory.CreateDirectory(savepath);
            postedFile.SaveAs(savepath + filename);
            //isUpload = InsertAddDocument(postedFile.FileName);
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        return "1";
    }

    public string SaveCustomerImage()
    {
        //var isUpload = "0";
        string filename = "";
        try
        {
            //var db = new mypicksDBDataContext();
            HttpPostedFile postedFile = Request.Files["customerImage"];
            string savepath = Server.MapPath("../../docs/customer_images/");
            //string filename = GenerateFileName(postedFile.FileName, Session.SessionID);

            //filename = "CampaignImage_" + Session.SessionID + "_" + postedFile.FileName;
            filename = postedFile.FileName;
            if (!Directory.Exists(savepath))
                Directory.CreateDirectory(savepath);
            postedFile.SaveAs(savepath + filename);
            //isUpload = InsertAddDocument(postedFile.FileName);
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        return "1";
    }

    private string DeleteProductFlavour()
    {
        try
        {
            CommonClient commonService = new CommonClient();
            int Id = 0;

            if (Request.QueryString["selectedFlavorId"] != null && Request.QueryString["selectedFlavorId"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["selectedFlavorId"].ToString());
            }

            bool status = commonService.DeleteProductFlavor(Id);


            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private string DeleteOutletType()
    {
        try
        {
            CommonClient commonService = new CommonClient();
            int Id = 0;

            if (Request.QueryString["selectedOutletId"] != null && Request.QueryString["selectedOutletId"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["selectedOutletId"].ToString());
            }

            bool status = commonService.DeleteOutletType(Id);


            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private string DeletePacking()
    {
        try
        {
            CommonClient commonService = new CommonClient();
            int Id = 0;

            if (Request.QueryString["selectedPackingId"] != null && Request.QueryString["selectedPackingId"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["selectedPackingId"].ToString());
            }

            bool status = commonService.DeletePacking(Id);


            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    public string SaveChecklistImage()
    {
        //var isUpload = "0";
        string filename = "";
        try
        {
            //var db = new mypicksDBDataContext();
            HttpPostedFile postedFile = Request.Files["checklistImage"];
            string savepath = Server.MapPath("../../docs/checklist_images/");
            //string filename = GenerateFileName(postedFile.FileName, Session.SessionID);

            //filename = "CampaignImage_" + Session.SessionID + "_" + postedFile.FileName;
            filename = postedFile.FileName;
            if (!Directory.Exists(savepath))
                Directory.CreateDirectory(savepath);
            postedFile.SaveAs(savepath + filename);
            //isUpload = InsertAddDocument(postedFile.FileName);
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        return "1";
    }

    [System.Web.Services.WebMethod]
    public static string UpdateCheckListCustomer(int CheckListId, string CustomerCode, bool HasSelect)
    {
        try
        {
            CommonClient commonService = new CommonClient();
            bool status = commonService.UpdateCheckListCustomer(CheckListId, CustomerCode, HasSelect);

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    [System.Web.Services.WebMethod]
    public static string UpdateCheckListRoute(int CheckListId, int RouteId, bool HasSelect)
    {
        try
        {
            CommonClient commonService = new CommonClient();
            bool status = commonService.UpdateCheckListRoute(CheckListId, RouteId, HasSelect);

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }
}