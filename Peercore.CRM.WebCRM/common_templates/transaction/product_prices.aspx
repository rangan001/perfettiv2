﻿<%@ Page Title="mSales - Product Prices" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="product_prices.aspx.cs" Inherits="common_templates_transaction_product_prices" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server"/>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">
                                    Product
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<asp:DropDownList Height="20px" ID="DropDownListProducts"
                                        CssClass="input-large1" runat="server">
                                    </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <%--<tr>
                                <td class="textalignbottom">
                                    Code
                                </td>
                                <td colspan="3">
                                <span class="wosub mand">
                                    &nbsp;<input type="text" class="textboxwidth" id="txtCode"/>
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Name
                                </td>
                                <td colspan="3">
                                <span class="wosub mand">
                                    &nbsp;<input type="text" class="textboxwidth" id="txtName"/>
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="textalignbottom">
                                   Secondary Sales Price
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtRetailPrice" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td class="textalignbottom">
                                    Pack Consumer Price
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtConsumerPrice" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                     Primary Sales Price
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_price"
                                        id="txtDistributorPrice" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">
                                        Save</button>
                                    <button class="k-button" id="buttonClear">
                                        Clear</button>
                                    <asp:HiddenField ID="hdnSelectedProductPriceId" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hdnSelectedProductId" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hdnSelectedRetailPriceOld" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hdnSelectedConsumerPriceOld" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hdnSelectedDistributorPriceOld" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
    <div class="toolbar_container">
        <div class="toolbar_left" id="div_content">
            <div class="hoributton">
                <div class="addlinkdiv" style="float: left" id="div_address">
                    <a id="id_add_product_price" clientidmode="Static" runat="server" href="#">Add Price</a>
                </div>
            </div>
        </div>
        <div class="toolbar_right" id="div3">
            <div class="leadentry_title_bar">
                <div style="float: right; width: 65%;" align="right">
                    <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
    <div class="clearall">
    </div>
    <div id="div_message" runat="server" style="display: none;">
    </div>
    <div class="clearall">
    </div>
    <div style="min-width: 200px; overflow: auto">
        <div style="float: left; width: 100%; overflow: auto">
            <div id="divProductPriceGrid">
            </div>
        </div>
    </div>
    </div>
    <div id="popup">
        <div>
            <b>Enter Admin Password:</b></div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>

    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            loadProductPriceGrid();
            //$("#MainContent_DropDownListProducts").focus();

            jQuery(function ($) {
                $('input.input_numeric_price').autoNumeric({ aSep: null, aDec: '.', mDec: 2 });
            });
        });


        //Load popup for Add new Product Price
        $("#id_add_product_price").click(function () {
            $("#window").css("display", "block");
            $("#hdnSelectedProductPriceId").val('');
            $("#hdnSelectedProductId").val('');
            $("#hdnSelectedRetailPriceOld").val('');
            $("#hdnSelectedConsumerPriceOld").val('');
            $('#MainContent_DropDownListProducts option[value=0]').text('- Select Product -');
            $("#txtRetailPrice").val('');
            $("#txtConsumerPrice").val('');
            $("#txtDistributorPrice").val('');
            $("#MainContent_DropDownListProducts").prop('disabled', false);
            $("select#MainContent_DropDownListProducts").prop('selectedIndex', 0);

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Price",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#MainContent_DropDownListProducts").focus();
            wnd.center().open();
        });

        //Save button click
        $("#buttonSave").click(function () {
            //ProductPriceEntrySaveProductPrice();
            showPopup();
        });

        function showPopup() {
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                ProductPriceEntrySaveProductPrice();
            }
        }

        function confirmAdmin() {

            var password = document.getElementById("adminPass").value;

            if (password == '') {
                var errorMsg = GetErrorMessageDiv("Please Enter admin Password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            var param = { "password": password };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            ProductPriceEntrySaveProductPrice();
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin Password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            $("#adminPass").val("");
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                        $("#adminPass").val("");
                    }

                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });

        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        //Button Clear Clicked in Product Price
        $("#buttonClear").click(function () {
            $("#hdnSelectedProductPriceId").val('');
            $("#hdnSelectedProductId").val('');
            $("#hdnSelectedRetailPriceOld").val('');
            $("#hdnSelectedConsumerPriceOld").val('');
            $("#txtRetailPrice").val('');
            $("#txtConsumerPrice").val('');
            $("#txtDistributorPrice").val('');
            $("#MainContent_DropDownListProducts").prop('disabled', false);
            $('#MainContent_DropDownListProducts option[value=0]').text('- Select Product -');
            $("select#MainContent_DropDownListProducts").prop('selectedIndex', 0);

        });

        $("#MainContent_DropDownListProducts").change(function () {
            var end = this.value;
            $("#hdnSelectedProductId").val(end);
        });

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Price",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }
        
    </script>
</asp:Content>
