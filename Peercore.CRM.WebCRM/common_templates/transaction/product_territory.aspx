﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="product_territory.aspx.cs" Inherits="common_templates_transaction_product_territory" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .option-input {
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            appearance: none;
            position: relative;
            top: 6px;
            right: 0;
            bottom: 0;
            left: 0;
            height: 24px !important;
            width: 24px !important;
            transition: all 0.15s ease-out 0s;
            background: #cbd1d8;
            border: none;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            margin-right: 0.5rem;
            outline: none;
            position: relative;
            z-index: 1000;
        }

            .option-input:hover {
                background: #9faab7;
            }

            .option-input:checked {
                background: #40e0d0;
            }

                .option-input:checked::before {
                    height: 24px;
                    width: 24px;
                    position: absolute;
                    content: '✔';
                    display: inline-block;
                    font-size: 18px;
                    text-align: center;
                    line-height: 24px;
                }

                .option-input:checked::after {
                    -webkit-animation: click-wave 0.65s;
                    -moz-animation: click-wave 0.65s;
                    animation: click-wave 0.65s;
                    background: #40e0d0;
                    content: '';
                    display: block;
                    position: relative;
                    z-index: 100;
                }

        .k-grid td {
            padding-top: 0px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />
    <asp:HiddenField ID="deleteAssignOption" runat="server" />

    <div id="marketmodalWindow" style="display: none">
        <div id="div_marketconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <div id="confirmmodalWindow" style="display: none">
        <div id="div_Confirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yesConfirm" class="k-button">
            Yes</button>
        <button id="noConfirm" class="k-button">
            No</button>
    </div>

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>

        <div id="div_main" class="divcontectmainforms">
            <div id="div_message" runat="server" style="display: none">
            </div>
            <div id="window" style="width: 500px;">
                <div style="width: 100%" id="contactentry">
                    <div id="div_text">
                        <div id="div_message_popup" runat="server" style="display: none;">
                        </div>
                        <table border="0">
                            <tbody>
                                <%--<tr>
                                    <td colspan="2" class="textalignbottom" style="color: black">Territory
                                    </td>
                                    <td colspan="4">
                                        <span class="wosub mand">&nbsp;
                                            <input type="text" class="textboxwidth" id="txtTerritoryFilter" maxlength="30" placeholder="Select Territory" />
                                            <img id="img2" src="~/assets/images/box_downarrow.png" alt="popicon" runat="server" border="0" style="height: 15px; width: 15px" />
                                        </span>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td colspan="2" class="textalignbottom">Product
                                    </td>
                                    <td colspan="4">
                                        <span class="wosub mand">&nbsp;
                                            <asp:DropDownList Height="20px" ID="ddlProduct"
                                                CssClass="input-large1" runat="server" Style="height: 32px;">
                                            </asp:DropDownList>
                                            <%--<span style="color: Red">*</span>--%>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div style="min-width: 200px; overflow: auto">
                <div style="float: left; width: 100%; overflow: auto">
                    <div id="gridAllTerritories"></div>
                </div>
            </div>
        </div>

        <div id="popup">
            <div>
                <b>Please enter admin password:</b>
            </div>
            <input style="width: 100%;" id="adminPass" type="password" />
            <button type="button" onclick="confirmAdmin()">
                Confirm</button>
            <button type="button" onclick="exitAdmin()">
                Cancel</button>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            var Originator = $("#<%= hfOriginator.ClientID %>").val();
            var OriginatorType = $("#<%= hfOriginatorType.ClientID %>").val();

            //LoadTerritoriesforProductTerritory(OriginatorType, Originator);
            LoadTerritoriesforProducts(OriginatorType, Originator, 0);
        });

        $("#MainContent_ddlProduct").change(function () {
            var e = document.getElementById("MainContent_ddlProduct");
            var ProductId = e.options[e.selectedIndex].value;

            var Originator = $("#<%= hfOriginator.ClientID %>").val();
            var OriginatorType = $("#<%= hfOriginatorType.ClientID %>").val();

            LoadTerritoriesforProducts(OriginatorType, Originator, ProductId);
        });

    </script>
</asp:Content>

