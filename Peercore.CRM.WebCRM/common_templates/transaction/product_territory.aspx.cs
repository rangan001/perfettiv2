﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class common_templates_transaction_product_territory : System.Web.UI.Page
{
    CommonUtility commonUtility = new CommonUtility();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Remove the session before setting the breadcrumb.
        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("Product to Territories ", "#", "");

        if (!IsPostBack)
        {
            //Set the Toolbar buttons
            buttonbar.VisibleSave(true);
            //buttonbar.VisibleDeactivate(true);
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            //buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);

            buttonbar.EnableSave(false);
            //buttonbar.EnableDeactivate(true);

            LoadOptions();
        }

        hfOriginator.Value = UserSession.Instance.UserName;
        hfOriginatorType.Value = UserSession.Instance.OriginatorString;
    }

    protected void ButtonSave_Click(object sender)
    {
        this.SaveProducts();
    }

    //protected void ButtonClear_Click(object sender)
    //{
    //    //// Clear();
    //    //txtSelectedDivisionName.Text = string.Empty;
    //    div_message.Attributes.Add("style", "display:none;padding:8px");
    //}

    private void SaveProducts()
    {
        try
        {
            if (Session[CommonUtility.DIVISION_PRODUCTS] == null)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please assign Products for selected division.");
                return;
            }

            int divisionId = 0;
            //if (!string.IsNullOrEmpty(HiddenFieldSelectedDivisionId.Value))
            //    divisionId = Int32.Parse(HiddenFieldSelectedDivisionId.Value);

            CommonClient commonClient = new CommonClient();
            List<ProductDTO> productList = (List<ProductDTO>)Session[CommonUtility.DIVISION_PRODUCTS];
            bool status = commonClient.SaveProductDivisions(divisionId, productList);

            if (status)
            {
                div_message.Attributes.Add("style", "display:block;padding:8px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
                Session[CommonUtility.DIVISION_PRODUCTS] = null;
            }
        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured.");
        }
    }

    private void LoadOptions()
    {
        #region Territory

        ProductClient productClient = new ProductClient();
        ArgsModel args1 = new ArgsModel();
        args1.StartIndex = 1;
        args1.RowCount = 10000;
        args1.OrderBy = " name asc";

        List<ProductModel> objList = new List<ProductModel>();
        objList = productClient.GetAllProducts(args1);

        if (objList.Count != 0)
        {
            ddlProduct.Items.Clear();
            ddlProduct.Items.Add(new ListItem("- Select Territory -", "0"));
            foreach (ProductModel item in objList)
            {
                var strLength = (item.FgCode == null) ? 0 : item.FgCode.Length;
                string result = (item.FgCode == null) ? "" : item.FgCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlProduct.Items.Add(new ListItem(item.Name + "  |  " + result, item.ProductId.ToString()));
            }

            ddlProduct.Enabled = true;
        }

        #endregion
    }
}