﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using System.Text;
using CRMServiceReference;
using Peercore.CRM.Common;

public partial class dashboard_transaction_CallCycleContact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Dashboard", "#", "");
        Master.SetBreadCrumb("Call Cycle Analysis", "#", "");
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_analysisgraph.ButtonFilter(ButtonFilter_Click);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(2);
            LoadGrid();
        }
        
    }

    public void LoadGrid()
    {
        try
        {
            string originator = string.IsNullOrEmpty( UserSession.Instance.FilterByUserName) ?
                 UserSession.Instance.UserName : UserSession.Instance.FilterByUserName;

            ActivityClient c = new ActivityClient();

            

            //string userName = "";
            string fromDate = "";
            string toDate = "";

            //if (cmbReps.SelectedValue != null)
            //    userName = cmbReps.SelectedValue.ToString().Trim();
            fromDate = buttonbar1.GetFromDateText();
            toDate = buttonbar1.GetToDateText();

            ArgsDTO args = new ArgsDTO();           

            args.Originator = originator;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.SStartDate = fromDate;
            args.SEndDate = toDate;
            args.RepType = UserSession.Instance.RepType;
            args.StartIndex = 1;
            args.RowCount = 500;
            args.OrderBy = "";
            args.AdditionalParams = "";
            args.IsIncludeContact = false;

            Session[CommonUtility.GLOBAL_SETTING] = args;


            List<CallCycleContactDTO> callCycleContactList = c.GetAllCallCyclesCountById(args);
            AnalysisGraph(callCycleContactList);

            //args.LeadId = 372;
            //callCycleContactList = c.GetAllCallCycles(args);

            //CreateChart(vcStackGraph, callCycleContactList);
            //callCycleContactList = c.GetCallCycleContacts(0, originator, false, fromDate, toDate, GlobalValues.GetInstance().RepType);
            //if (callCycleContactList != null)
            //    dgCallCycle.ItemsSource = callCycleContactList;

            //lblCount.Content = "# Recs.: 0/" + callCycleContactList.Count.ToString();

        }
        catch (Exception oException)
        {
        }
    }

    public string SetSeriesStateData(List<CallCycleContactDTO>  callCycleContactList)
    {
        string series = "";
        int count = 0;
        foreach (CallCycleContactDTO lead in callCycleContactList)
        {
            count++;
            if (string.IsNullOrEmpty(series))
            {
                series = "{ name: '" + lead.CallCycle.Description + "', y: " + lead.CallCycle.NoOfLeadCustomers + ",rowcount: '" + count + "',des: '" + lead.CallCycle.CallCycleID + "' }";
            }
            else
            {
                series += ",{ name: '" + lead.CallCycle.Description + "', y: " + lead.CallCycle.NoOfLeadCustomers + ", rowcount: '" + count + "',des: '" + lead.CallCycle.CallCycleID + "' }";
            }
        }
        return series;
    }

    public void AnalysisGraph(List<CallCycleContactDTO> callCycleContactList)
    {
        string reportDate = "";

        reportDate = SetSeriesStateData(callCycleContactList);      

        StringBuilder sb = new StringBuilder();

        sb.Append("<div>");
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("$(function () {");
        sb.Append("    var chart;");
        sb.Append("    $(document).ready(function () {");
        sb.Append("        chart = new Highcharts.Chart({");
        sb.Append("            chart: {");
        sb.Append("                renderTo: 'MainContent_container',");
        sb.Append("                plotBackgroundColor: null,");
        sb.Append("                plotBorderWidth: null,");
        sb.Append("                plotShadow: false");
        sb.Append("            },");
        sb.Append("            title: {");
        sb.Append("                text: 'Call Cycle Analysis'");
        sb.Append("            },");
        sb.Append("            tooltip: {");
        sb.Append("                formatter: function () {");
        sb.Append("                    return '<b>' + this.point.name + ','+ this.point.y +'(' + Math.round(this.percentage*100)/100  + ' %)</b>';");
        sb.Append("                }");
        sb.Append("            },");

        sb.Append("            plotOptions: {");
        sb.Append("                pie: {");
        sb.Append("                    allowPointSelect: true,");
        sb.Append("                    cursor: 'pointer',");
        sb.Append("                    point: {");
        sb.Append("                        events: {");
        sb.Append("                            click: function (e) {");
        sb.Append("                                 GridCallCyclesGraph(this.des);");
        sb.Append("                            }");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    dataLabels: {");
        sb.Append("                        enabled: true,");
        //sb.Append("                        color: '#000000',");
        //sb.Append("                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',");
        sb.Append("                        connectorColor: '#000000',");
        sb.Append("                        formatter: function () {");
        sb.Append("                           if(this.point.name == ''){");
        sb.Append("                             return '<b>' + this.point.rowcount + ','+ this.point.y +'</b>';");
        sb.Append("                           }else {");
        sb.Append("                             return '<b>' + this.point.name + '</b>';");
        sb.Append("                           }");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    showInLegend: true");
        sb.Append("                }");
        sb.Append("            },");
        sb.Append("            series: [{");
        sb.Append("                type: 'pie',");
        sb.Append("                name: 'state',");
        sb.Append("                rowcount:'rowcount',");
        sb.Append("                des:'des',");
        sb.Append("                data: [");
        sb.Append(reportDate);
        sb.Append("        ]");
        sb.Append("            }]");
        sb.Append("        });");
        sb.Append("    });");

        sb.Append("});");
        sb.Append("</script>");
        sb.Append("</div>");

        container.InnerHtml = sb.ToString();
    }
    protected void ButtonFilter_Click(object sender)
    {
        LoadGrid();
    }

}