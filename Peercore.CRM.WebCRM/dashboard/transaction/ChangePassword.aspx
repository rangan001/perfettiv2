﻿<%@ Page Title="mSales - Change Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs"
    Inherits="dashboard_transaction_ChangePassword" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back">
                            </div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        <div>
            <div class="formleft" style="width: 100%">
                <div class="formtextdiv">
                    User Name</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="UserName" runat="server" CssClass="input-large1" Style="width: 300px;
                        left: 60px;" ReadOnly="true">
                    </asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Current Password</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="currentPassword" runat="server" MaxLength="8" TextMode="Password"
                        CssClass="input-large1" Style="width: 300px; left: 60px;" />
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    New Password</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="newPassword" runat="server" MaxLength="8" TextMode="Password" CssClass="input-large1"
                        Style="width: 300px; left: 60px;" />
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Confirm Password</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="confirmPassword" runat="server" MaxLength="8" TextMode="Password"
                        CssClass="input-large1" Style="width: 300px; left: 60px;" />
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    </div>
                <div class="formdetaildiv_right" style="text-align: right;">
                    <asp:Button ID="cancle" Text="Cancle" runat="server" Style="width: 50px; left: 65px;" OnClick="Cancle_Click" />
                    <asp:Button ID="changePassword" Text="Change Password" runat="server" Style="width: 120px; left: 65px;"
                        OnClick="changePassword_Click" />
                </div>
                <div class="clearall">
                </div>
            </div>
            <div class="clearall">
            </div>
        </div>
    </div>
    <script type="text/javascript">

        function MyFunc() {
            alert("Password successfully changed. You must restart mSales application.");
            window.location = "../../login.aspx";
        };

    </script>
</asp:Content>
