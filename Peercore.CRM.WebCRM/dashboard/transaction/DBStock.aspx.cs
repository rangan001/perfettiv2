﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;
using Peercore.CRM.Shared;
using CRMServiceReference;
using System.Data;
using System.ServiceModel;
using System.ServiceModel.Description;

public partial class dashboard_transaction_DBStock : System.Web.UI.Page
{
    #region Events

    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Distributor Stock", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributer_SelectionChanged);
        buttonbar1.onDropDownListQuantityType = new usercontrols_buttonbar_reports.DropDownListQuantityType(DropDownListQuantityType_SelectionChanged);


        if (!IsPostBack)
        {
            buttonbar1.SetVisible(13);
            LoadOptions();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            buttonbar1.HandleExportButtonEnable(false);
            buttonbar1.HandleFilterButtonEnable(false);

            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("DBStock", Session.SessionID.ToString())) + ".pdf", GenerateFileName("DBStock", Session.SessionID.ToString()) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {
            
        }
        finally
        {
            buttonbar1.HandleExportButtonEnable(true);
            buttonbar1.HandleFilterButtonEnable(true);
        }
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        LoadDistributors();
        LoadTerritory();
    }
    
    protected void DropDownListDistributer_SelectionChanged(object sender)
    {
        LoadTerritory();
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("DBStock", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("DBStock", Session.SessionID.ToString()) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    #endregion

    private void LoadOptions()
    {
        //Laod ASE's     
        LoadASEs();

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleASEDropdownVisible(false);
        }

        LoadDistributors();

        if (UserSession.Instance.OriginatorString == "DIST")
        {
            buttonbar1.SetAsesSelect("ALL");
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();

            buttonbar1.SetDistributorsSelect(UserSession.Instance.UserName);
            buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();

            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleDistributerDropdownEnable(false);
            buttonbar1.HandleTradeloadRepTypeDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleDistributerDropdownVisible(false);
            buttonbar1.HandleTradeloadRepTypeDropdownVisible(false);
        }

        LoadTerritory();
    }

    #region Load Drop Downs

    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
            if (aseList.Count > 0)
                aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string selectedASE = buttonbar1.GetAsesValue();

        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetDistributersByASMOriginator(args);
        if (distList.Count > 0)
            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    private void LoadTerritory()
    {
        ArgsModel args = new ArgsModel();
        args.OrderBy = "territory_name ASC";

        string selectedASE = buttonbar1.GetAsesValue();
        string selectedDis = buttonbar1.GetDistributorsValue();

        if (!selectedASE.Equals("ALL"))
        {
            if (selectedDis.Equals("ALL"))
            {
                args.AdditionalParams = " crm_mast_asm.originator = '" + selectedASE + "' ";
            }
            else
            {
                args.AdditionalParams = " crm_distributor.originator = '" + selectedDis + "' ";
            }
        }
        else
        {
            if (!selectedDis.Equals("ALL"))
            {
                args.AdditionalParams = " crm_distributor.originator = '" + selectedDis + "' ";
            }
        }

        args.StartIndex = 1;
        args.RowCount = 500;

        MasterClient masterClient = new MasterClient();
        List<TerritoryModel> terrList = new List<TerritoryModel>();
        terrList = masterClient.GetAllTerritoryByOriginator(args, 
            UserSession.Instance.OriginatorString, 
            UserSession.Instance.UserName);

        if (terrList.Count > 1)
            terrList.Insert(0, new TerritoryModel() { TerritoryName = "ALL", TerritoryId = 0 });

        buttonbar1.SetTerritories(terrList, "TerritoryId", "TerritoryName");
    }

    #endregion

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);

        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private DateTime? GetMonth()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetMonth());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportFor()
    {
        try
        {
            string reportfor = buttonbar1.GetShowReprtForValue();
            return reportfor;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = Convert.ToDateTime(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = Convert.ToDateTime(buttonbar1.GetToDateText());
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportSubName()
    {
        string selectedASE = buttonbar1.GetAsesValue();
        string selectedDIST = buttonbar1.GetDistributorsValue();
        string selectedREP = buttonbar1.GetRepsValue();

        //Edit By Irosh Fernando=========================
        string reportName_Ase = "ALL ASM, ";
        string reportName_Dis = "ALL Distributor, ";
        string reportName_Rep = "ALL Rep";

        if (!selectedASE.Equals("ALL") && !selectedASE.Equals(""))
        {
            reportName_Ase = "ASM - " + buttonbar1.GetAsesText() + ", ";
        }

        if (!selectedDIST.Equals("ALL") && !selectedDIST.Equals(""))
        {
            reportName_Dis = "Distributor - " + buttonbar1.GetDistributorsText() + ", ";
        }

        if (!selectedREP.Equals("ALL") && !selectedREP.Equals(""))
        {
            reportName_Rep = "Rep - " + buttonbar1.GetRepsText();
        }
        //===============================================

        return "For " + reportName_Ase + reportName_Dis + reportName_Rep;
    }

    private string GetDisString()
    {
        try
        {
            string reportfor = buttonbar1.GetDistributorsValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private string GetDBStockReportType()
    {
        try
        {
            string reportfor = buttonbar1.GetReportTypeQtyValue();
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (GetDBStockReportType() == "q")
            rptDoc.Load(Server.MapPath("../reports/DBStockQtyReport.rpt"));
        else if (GetDBStockReportType() == "v")
            rptDoc.Load(Server.MapPath("../reports/DBStockValueReport.rpt"));
        else
            rptDoc.Load(Server.MapPath("../reports/DBStockQtyReport.rpt"));

        //rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        //rptDoc.SetParameterValue("rptTitle2", "Month " + GetMonth().Value.ToString("MMM-yyyy"));

        rptDoc.SetParameterValue("@date", buttonbar1.GetFromDateText());
        rptDoc.SetParameterValue("@asm", buttonbar1.GetAsesValue());
        rptDoc.SetParameterValue("@distributor", buttonbar1.GetDistributorsValue());
        rptDoc.SetParameterValue("@territoryid", buttonbar1.GetTerritoryValue());
        string strQuantityType = buttonbar1.GetQuantityType();
        rptDoc.SetParameterValue("@QuantityType", strQuantityType); //Added by Rangan

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }
    protected void DropDownListQuantityType_SelectionChanged(object sender)
    {

    }

}