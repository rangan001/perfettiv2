﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EndUserEnquiry.aspx.cs" Inherits="dashboard_transaction_EndUserEnquiry" %>
<%@ Register Src="~/usercontrols/buttonbar_analysisgraph.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/toolbar_analysisgraph.ascx" TagPrefix="ucl" TagName="toolbar" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div id="new_window">
                <div id="div_custgrid">
                </div>
               
            </div>


<div class="divcontectmainforms">
        <%--<div class="homemsgboxtopic">
            <div style="float: left; width: 65%">
                Hot Operator Activity Report
            </div>
            <div style="float: right; width: 35%;" align="right">
            <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/dashboard/transaction/AnalysisGraph.aspx?rptid=3">
                        <img src="../../assets/images/back_button.png" alt="" border="0" />
            </asp:HyperLink>                
            </div>
        </div>--%>
        <div class="title_bar">
        <div style="width:90%; float:left;">
                    <ucl:toolbar ID="toolbar1" runat="server" />
        </div>
        <div style="width:10%; float:right;">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/dashboard/transaction/OpportunityAnalysis.aspx">
                        <div class="back"></div>
                        </asp:HyperLink>
        </div>    
     </div>
        <div class="clearall"></div>
        <div align="right">
            <div class="bg" align="left">
                <div id="div_message" class="savemsg" runat="server" style="display:none" ></div>        
                <div style="padding-left:10px;"><ucl:buttonbar ID="buttonbar1" runat="server" /></div>
            </div>
        </div>
        <%--<div class="clearall">&nbsp;</div> --%> 
        <div style="margin-top:25px;">
        
            <%--<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto" runat="server"></div>

<div class="clearall">&nbsp;</div>--%>
            <%--<div>
                <asp:CheckBox ID="chkProducts" runat="server" Text="Include Products with No Sales"/>
            </div>
            <div>
                <asp:CheckBox ID="chkActiveEndUsers" runat="server" Text="Active End Users Only" Checked="true"/>
            </div>--%>

            <div style="min-width: 200px; overflow: auto;">
            
            <div id="gridEndUserEnquiry">
            </div>
                
            </div>
    </div></div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#div_repgroup").css("display", "block");
            $("#div_costyear").css("display", "block");
            $("#div_costperiod").css("display", "inline-block");
            $("#div_distributor").css("display", "inline-block");
            $("#div_enduser").css("display", "block");
            $("#div_products").css("display", "inline-block");
            $("#div_activeendusers").css("display", "inline-block");
            $("#MainContent_toolbar1_a_enduser").css("background-color", "#ff6600");
            //GridLeadCustOppCount('');
            // GridEndUserEnquiry(10, 2012, 1, 1);

            var newcust_window = $("#new_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

            var onClose = function () {
                newcust_undo.show();
            }

            if (!newcust_window.data("kendoWindow")) {
                newcust_window.kendoWindow({
                    width: "500px",
                    height: "400px",
                    title: "Lookup",
                    close: onClose
                });
            }

            newcust_window.data("kendoWindow").close();

            $("#id_distributor").click(function () {
                $("div#div_loader").show();
                loaddistributorLookup('div_newcustgrid', 'newcust_window', 0);
                //$("div#div_loader").hide();
            });

            $("#id_endUser").click(function () {

               // $("div#div_loader").show();
              //  loadEndUserLookup(1, 0, 'F');
               // $("div#div_loader").show();
                $("div#div_loader").show();
                loadEndUserLookup('div_newcustgrid', 'newcust_window', $("#txtnewcustcode").val());
            });

        });

        


        
	</script>
       
</asp:Content>

