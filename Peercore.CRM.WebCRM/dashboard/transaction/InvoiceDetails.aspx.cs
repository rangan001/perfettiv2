﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net;
using System.Configuration;

public partial class dashboard_transaction_InvoiceDetails : System.Web.UI.Page
{
    private ReportDocument repDoc = null;

    #region Load Drop Downs

    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (!buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
            if (aseList.Count > 0)
                aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors(string selectedASE)
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND parent_originator = '" + selectedASE + "' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetOriginatorsByCatergory(args);

        if (!buttonbar1.GetShowReprtForValue().Equals("distributor"))//Dont add ALL to drop down if Distributor User selected
            if (distList.Count > 0)
                distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    private void LoadReps(string selectedDistributor)
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        if (selectedDistributor.Equals("ALL"))
        {
            args.AdditionalParams = " dept_string = 'DR' ";
            if (!buttonbar1.GetAsesValue().Equals("ALL"))
            {
                args.AdditionalParams = " dept_string = 'DR' AND " + (this.GetDistributorsString()).Replace("originator", "parent_originator");
            }
        }
        else
            args.AdditionalParams = " dept_string = 'DR'  AND parent_originator = '" + selectedDistributor + "' ";
        args.StartIndex = 1;
        args.RowCount = 1000;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetOriginatorsByCatergory(args);
        if (!buttonbar1.GetShowReprtForValue().Equals("rep"))//Dont add ALL to drop down if Rep User selected
            if (repList.Count > 0)
                repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetReps(repList, "UserName");
    }

    //private void LoadFilterByBrands()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.StartIndex = 1;
    //    args.RowCount = 1000;
    //    args.ROriginator = UserSession.Instance.OriginatorString;
    //    args.Originator = UserSession.Instance.OriginalUserName;
    //    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //    args.OrderBy = " name asc";
    //    CommonClient commonClient = new CommonClient();
    //    List<BrandDTO> brandList = new List<BrandDTO>();
    //    brandList = commonClient.GetAllBrandsDataAndCount(args);
    //    if (brandList.Count > 0)
    //        brandList.Insert(0, new BrandDTO() { BrandId = 0, BrandName = "ALL" });
    //    buttonbar1.SetBrands(brandList, "BrandId");
    //}

    //private void LoadFilterByPackingMethod()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.StartIndex = 1;
    //    args.RowCount = 1000;
    //    args.ROriginator = UserSession.Instance.OriginatorString;
    //    args.Originator = UserSession.Instance.OriginalUserName;
    //    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //    args.OrderBy = " packing_name asc";

    //    CommonClient commonClient = new CommonClient();
    //    List<ProductPackingDTO> packingsList = new List<ProductPackingDTO>();
    //    packingsList = commonClient.GetAllProductPackings(args);
    //    if (packingsList.Count > 0)
    //        packingsList.Insert(0, new ProductPackingDTO() { PackingId = 0, PackingName = "ALL" });
    //    buttonbar1.SetPackingMethod(packingsList, "PackingId");
    //}

    //private void LoadFilterByItemGroup()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.StartIndex = 1;
    //    args.RowCount = 1000;
    //    args.ROriginator = UserSession.Instance.OriginatorString;
    //    args.Originator = UserSession.Instance.OriginalUserName;
    //    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //    args.OrderBy = " id ";

    //    CommonClient commonClient = new CommonClient();
    //    List<ProductCategoryDTO> categoryList = new List<ProductCategoryDTO>();
    //    categoryList = commonClient.GetAllProductCategories(args);
    //    if (categoryList.Count > 0)
    //        categoryList.Insert(0, new ProductCategoryDTO() { Id = 0, Descrition = "ALL" });
    //    buttonbar1.SetItemGroup(categoryList, "Id");
    //}

    //private void LoadFilterByFlavor()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.StartIndex = 1;
    //    args.RowCount = 1000;
    //    args.ROriginator = UserSession.Instance.OriginatorString;
    //    args.Originator = UserSession.Instance.OriginalUserName;
    //    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //    args.OrderBy = " flavor_name asc";

    //    CommonClient commonClient = new CommonClient();
    //    List<FlavorDTO> flavorsList = new List<FlavorDTO>();
    //    flavorsList = commonClient.GetAllFlavor();
    //    if (flavorsList.Count > 0)
    //        flavorsList.Insert(0, new FlavorDTO() { FlavorId = 0, FlavorName = "ALL" });
    //    buttonbar1.SetFlavor(flavorsList, "FlavorId");
    //}

    #endregion

    private void LoadOptions()
    {
        //Laod ASE's     
        LoadASEs();

        //Load Distributors
        string selectedASE = buttonbar1.GetAsesValue();
        LoadDistributors(selectedASE);

        ////Load Reps
        string selectedDistributor = buttonbar1.GetDistributorsValue();
        LoadReps(selectedDistributor);

        //LoadFilterByBrands();
        //LoadFilterByFlavor();
        //LoadFilterByItemGroup();
        //LoadFilterByPackingMethod();
    }

    private string GetASEsString()
    {
        ListItemCollection aseList = buttonbar1.GetAllASEsInDropDown();
        string aseString = "";

        if (aseList != null)
        {
            if (aseList.Count > 0)
            {
                aseString = " originator IN (";
                foreach (ListItem item in aseList)
                {
                    aseString = aseString + "'" + item.Value + "',";
                }
                aseString = aseString.Replace("'ALL',", "");
                aseString = aseString.Remove(aseString.Length - 1, 1);//Removing Last Comma
                aseString = aseString + ") ";
            }
        }

        return aseString;
    }

    private string GetDistributorsString()
    {
        ListItemCollection distList = buttonbar1.GetAllDistributorsInDropDown();
        string distString = "";

        if (distList != null)
        {
            if (distList.Count > 0)
            {
                distString = " originator IN (";
                foreach (ListItem item in distList)
                {
                    distString = distString + "'" + item.Value + "',";
                }
                distString = distString.Replace("'ALL',", "");
                distString = distString.Remove(distString.Length - 1, 1);//Removing Last Comma
                distString = distString + ") ";
            }
        }

        return distString;
    }

    private string GetRepsString()
    {
        ListItemCollection repsList = buttonbar1.GetAllRepsInDropDown();
        string repString = "";

        if (repsList != null)
        {
            if (repsList.Count > 0)
            {
                repString = " originator IN (";
                foreach (ListItem item in repsList)
                {
                    repString = repString + "'" + item.Value + "',";
                }
                repString = repString.Replace("'ALL',", "");
                repString = repString.Remove(repString.Length - 1, 1);//Removing Last Comma
                repString = repString + ") ";
            }
            else
            {
                repString = " originator IN ( ' ' ) ";
            }
        }
        else
        {
            repString = " originator IN ( ' ' ) ";
        }

        return repString;
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = Convert.ToDateTime(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = Convert.ToDateTime(buttonbar1.GetToDateText());
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportSubName()
    {
        string reportName = "";

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))
        {
            reportName = "ASM : " + buttonbar1.GetAsesText();
        }
        else if (buttonbar1.GetShowReprtForValue().Equals("distributor"))
        {
            reportName = "Distributor : " + buttonbar1.GetDistributorsText();
        }
        else if (buttonbar1.GetShowReprtForValue().Equals("rep"))
        {
            reportName = "Rep : " + buttonbar1.GetRepsText();
        }

        return reportName;
    }

    #region report methods

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/tmp_" + sessionid.Trim() + "_" + ori_filename.Trim() + "_";
        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.Excel, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        rptDoc.Load(Server.MapPath("../reports/invoice_details_new.rpt"));
        rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        rptDoc.SetParameterValue("rptTitle2", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));

        rptDoc.SetParameterValue("@StartDate", GetFromDate());
        rptDoc.SetParameterValue("@EndDate", GetToDate());
        if (buttonbar1.GetShowReprtForValue().Equals("rep"))
            rptDoc.SetParameterValue("@RepCodeList", "ih.rep_code = '" + buttonbar1.GetRepsValue() + "' ");
        else
            rptDoc.SetParameterValue("@RepCodeList", GetRepsString().Replace("originator", "ih.rep_code"));
        rptDoc.SetParameterValue("@AdditionalParams", "");


        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        //crConnectionInfo.ServerName = @"ACTINIUM\PSLSQLDEV2012";
        //crConnectionInfo.DatabaseName = "perfetti_test";
        //crConnectionInfo.UserID = "sa";
        //crConnectionInfo.Password = "sun$hin3";

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }
        return rptDoc;
    }

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Item Wise Sales", "#", "");
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListReps = new usercontrols_buttonbar_reports.DropDownListReps(DropDownListReps_SelectionChanged);
        buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributors_SelectionChanged);
        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        buttonbar1.onDropDownListShowReportFor = new usercontrols_buttonbar_reports.DropDownListShowReportFor(DropDownListShowReportFor_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(3);
            LoadOptions();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("invoice_summary", Session.SessionID.ToString())) + ".pdf", GenerateFileName("invoice_summary", Session.SessionID.ToString()) + ".pdf", repDoc);
            

        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListReps_SelectionChanged(object sender)
    {

    }

    protected void DropDownListDistributors_SelectionChanged(object sender)
    {
        //Load Reps
        string selectedDistributor = buttonbar1.GetDistributorsValue();
        LoadReps(selectedDistributor);
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        //Load Distributors
        string selectedASE = buttonbar1.GetAsesValue();
        LoadDistributors(selectedASE);
        string selectedDistributor = buttonbar1.GetDistributorsValue();
        LoadReps(selectedDistributor);
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            ReportDocument rptDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("invoice_summary1", Session.SessionID.ToString())) + ".xls", GenerateFileName("invoice_summary1", Session.SessionID.ToString()) + ".xls", rptDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListShowReportFor_SelectionChanged(object sender)
    {
        //Laod ASE's     
        LoadASEs();

        //Load Distributors
        string selectedASE = buttonbar1.GetAsesValue();
        LoadDistributors(selectedASE);

        ////Load Reps
        string selectedDistributor = buttonbar1.GetDistributorsValue();
        LoadReps(selectedDistributor);

        buttonbar1.HandleShowReportForDropDownChangedInvoiceSummary();
    }

    #endregion
}