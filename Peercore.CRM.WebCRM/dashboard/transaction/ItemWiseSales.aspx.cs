﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CrystalDecisions.Shared;
using System.Net;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;
using CRMServiceReference;

public partial class dashboard_transaction_ItemWiseSales : PageBase
{
    private void LoadOptions()
    {
        LoadASEs();


        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetReportForSelect("rep");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }

        LoadDistributors();

        if (UserSession.Instance.OriginatorString == "DIST")
        {
            buttonbar1.SetReportForSelect("distributor");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect("ALL");
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();

            buttonbar1.SetDistributorsSelect(UserSession.Instance.UserName);
            buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();

            buttonbar1.SetTradeloadRepTypeSelect("p");

            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleDistributerDropdownEnable(false);
            buttonbar1.HandleTradeloadRepTypeDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleDistributerDropdownVisible(false);
            buttonbar1.HandleTradeloadRepTypeDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }

        LoadReps();

        LoadFilterByBrands();
        LoadFilterByFlavor();
        LoadFilterByItemGroup();
        LoadFilterByPackingMethod();
    }

    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        //if (buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
        if (aseList.Count > 1)
            aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string selectedASE = buttonbar1.GetAsesValue();

        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetDistributersByASMOriginator(args);
        if (distList.Count > 1)
            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    private void LoadReps()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string strASM = GetASEString();
        string strDIST = GetDisString();

        if (strDIST.Equals(""))
        {
            if (strASM.Equals(""))
                args.AdditionalParams = " originator.dept_string = 'DR' ";
            else
                args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_mast_asm.originator = '" + strASM + "' ";
        }
        else
        {
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_distributor.originator = '" + strDIST + "' ";
        }

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetAllSRByOriginator(args);
        if (repList.Count > 1)
            repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetReps(repList, "UserName");
    }

    private string GetASEString()
    {
        try
        {
            string reportfor = buttonbar1.GetAsesValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private string GetDisString()
    {
        try
        {
            string reportfor = buttonbar1.GetDistributorsValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = DateTime.Parse(buttonbar1.GetToDateText()).AddDays(1);
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private void LoadFilterByBrands()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " name asc";
        CommonClient commonClient = new CommonClient();
        List<BrandDTO> brandList = new List<BrandDTO>();
        brandList = commonClient.GetAllBrandsDataAndCount(args);
        if (brandList.Count > 0)
            brandList.Insert(0, new BrandDTO() { BrandId = 0, BrandName = "ALL" });
        buttonbar1.SetBrands(brandList, "BrandId");
    }

    private void LoadFilterByPackingMethod()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " packing_name asc";

        CommonClient commonClient = new CommonClient();
        List<ProductPackingDTO> packingsList = new List<ProductPackingDTO>();
        packingsList = commonClient.GetAllProductPackings(args);
        if (packingsList.Count > 0)
            packingsList.Insert(0, new ProductPackingDTO() { PackingId = 0, PackingName = "ALL" });
        buttonbar1.SetPackingMethod(packingsList, "PackingId");
    }

    private void LoadFilterByItemGroup()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " id ";

        CommonClient commonClient = new CommonClient();
        List<ProductCategoryDTO> categoryList = new List<ProductCategoryDTO>();
        categoryList = commonClient.GetAllProductCategories(args);
        if (categoryList.Count > 0)
            categoryList.Insert(0, new ProductCategoryDTO() { Id = 0, Description = "ALL" });
        buttonbar1.SetItemGroup(categoryList, "Id");
    }

    private void LoadFilterByFlavor()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " flavor_name asc";

        CommonClient commonClient = new CommonClient();
        List<FlavorDTO> flavorsList = new List<FlavorDTO>();
        flavorsList = commonClient.GetAllFlavor();
        if (flavorsList.Count > 0)
            flavorsList.Insert(0, new FlavorDTO() { FlavorId = 0, FlavorName = "ALL" });
        buttonbar1.SetFlavor(flavorsList, "FlavorId");
    }

    private string GetReportSubName()
    {
        string selectedASE = buttonbar1.GetAsesValue();
        string selectedDIST = buttonbar1.GetDistributorsValue();
        string selectedREP = buttonbar1.GetRepsValue();

        //string reportName = "";

        //if (buttonbar1.GetShowReprtForValue().Equals("ase"))
        //{
        //    reportName = "For ALL ASE";
        //}
        //else if (buttonbar1.GetShowReprtForValue().Equals("distributor"))
        //{
        //    reportName = "For ALL Distributors";
        //    if (!selectedASE.Equals("ALL") && !selectedASE.Equals(""))
        //    {
        //        reportName = "For All Distributors of ASE - " + buttonbar1.GetAsesText();
        //    }
        //}
        //else if (buttonbar1.GetShowReprtForValue().Equals("rep"))
        //{
        //    reportName = "For ALL Reps of ASE";
        //    if (!selectedASE.Equals("ALL") && !selectedASE.Equals(""))
        //    {
        //        reportName = "For " + buttonbar1.GetRepsText() + " Reps of ASE - " + buttonbar1.GetAsesText();

        //        if (!selectedDIST.Equals("ALL") && !selectedDIST.Equals(""))
        //        {
        //            reportName = "For All Reps of Distriutor - " + buttonbar1.GetDistributorsText();
        //        }
        //    }
        //    else
        //    {
        //        reportName = "For " + buttonbar1.GetRepsText() + " Reps of ASE - " + buttonbar1.GetAsesText();
        //    }
        //}

        //return reportName;


        //Edit By Irosh Fernando=========================
        string reportName_Ase = "ALL ASM, ";
        string reportName_Dis = "ALL Distributor, ";
        string reportName_Rep = "ALL Rep";

        if (!selectedASE.Equals("ALL") && !selectedASE.Equals(""))
        {
            reportName_Ase = "ASM - " + buttonbar1.GetAsesText() + ", ";
        }

        if (!selectedDIST.Equals("ALL") && !selectedDIST.Equals(""))
        {
            reportName_Dis = "Distributor - " + GetDistributorById(buttonbar1.GetDistributorsValue()).OriginatorId + " / " + buttonbar1.GetDistributorsText() + ", ";
        }

        if (!selectedREP.Equals("ALL") && !selectedREP.Equals(""))
        {
            reportName_Rep = "Rep - " + buttonbar1.GetRepsText();
        }
        //===============================================

        return "For " + reportName_Ase + reportName_Dis + reportName_Rep;
    }

    private OriginatorDTO GetDistributorById(string Id)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        OriginatorDTO distList = new OriginatorDTO();
        distList = originatorClient.GetOriginator(Id);

        return distList;
    }

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();
        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private string FilterByReport()
    {
        string reportFilter = "";

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))
        {
            if (buttonbar1.GetAsesValue().Equals("ALL"))
            {
                reportFilter = "";
            }
            else
            {
                reportFilter = "z.ase_id = '" + buttonbar1.GetAsesValue() + "'";
            }
        }

        if (buttonbar1.GetShowReprtForValue().Equals("distributor"))
        {
            if (buttonbar1.GetAsesValue().Equals("ALL"))
            {
                reportFilter = "";
            }
            else
            {
                if (buttonbar1.GetDistributorsValue().Equals("ALL") || buttonbar1.GetDistributorsValue().Equals(""))
                {
                    reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
                }
                else
                {
                    reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "' AND dis_id = '" + buttonbar1.GetDistributorsValue() + "'";
                }
            }

        }

        if (buttonbar1.GetShowReprtForValue().Equals("rep"))
        {
            if (buttonbar1.GetAsesValue().Equals("ALL"))
            {
                reportFilter = "";
            }
            else
            {
                if (buttonbar1.GetDistributorsValue().Equals("ALL") || buttonbar1.GetDistributorsValue().Equals(""))
                {
                    reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
                }
                else
                {
                    if (buttonbar1.GetRepsValue().Equals("ALL") || buttonbar1.GetRepsValue().Equals(""))
                    {
                        if (buttonbar1.GetDistributorsValue().Equals("ALL") || buttonbar1.GetDistributorsValue().Equals(""))
                        {
                            reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
                        }
                        else
                        {
                            reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "' AND dis_id = '" + buttonbar1.GetDistributorsValue() + "'";
                        }
                    }
                    else
                    {
                        reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "' AND dis_id = '" + buttonbar1.GetDistributorsValue() + "' AND rep_id = '" + buttonbar1.GetRepsValue() + "'";
                    }
                }
            }

        }

        return reportFilter;
    }

    private ReportDocument GetReportDocument()
    {
        string strQuantityType = buttonbar1.GetQuantityType();
        ReportDocument rptDoc = new ReportDocument();

        if (buttonbar1.GetReportTypeValue() == "v")
            rptDoc.Load(Server.MapPath("../reports/item_wise_sales_value.rpt"));

        if (buttonbar1.GetReportTypeValue() == "f")
        {
            rptDoc.Load(Server.MapPath("../reports/item_wise_sales.rpt"));
            rptDoc.SetParameterValue("@QuantityType", strQuantityType);
        }

        else if (buttonbar1.GetReportTypeValue() == "q")
        { 
            rptDoc.Load(Server.MapPath("../reports/item_wise_sales.rpt"));
            rptDoc.SetParameterValue("@QuantityType", strQuantityType);

        }

        string title2 = "From " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " To " + GetToDate().Value.AddDays(-1).ToString("dd-MMM-yyyy");
        title2 = title2 + " By " + buttonbar1.GetReportTypeText();

        rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        rptDoc.SetParameterValue("rptTitle2", title2);
        rptDoc.SetParameterValue("group_name", buttonbar1.GetGroupByText());
        rptDoc.SetParameterValue("@StartDate", GetFromDate());
        rptDoc.SetParameterValue("@EndDate", GetToDate());
        rptDoc.SetParameterValue("@BrandId", Int32.Parse(buttonbar1.GetFilterByBrandValue()));
        rptDoc.SetParameterValue("@PackingId", Int32.Parse(buttonbar1.GetFilterByPackingMethodValue()));
        rptDoc.SetParameterValue("@FlavorId", Int32.Parse(buttonbar1.GetFilterByFlavorValue()));
        rptDoc.SetParameterValue("@CategoryId", Int32.Parse(buttonbar1.GetFilterByItemGroupValue()));
        rptDoc.SetParameterValue("@Hvp", buttonbar1.GetFilterByHVPValue());
        rptDoc.SetParameterValue("@GroupCol", buttonbar1.GetGroupByValue());

        string strAsm = buttonbar1.GetAsesValue();
        string strDis = buttonbar1.GetDistributorsValue();
        string strRep = buttonbar1.GetRepsValue();

        if ((strAsm == "ALL") && (strDis == "ALL") && (strRep == "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "");
        }
        else if ((strAsm == "ALL") && (strDis == "ALL") && (strRep != "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.rep_code = '" + strRep + "'");
        }
        else if ((strAsm == "ALL") && (strDis != "ALL") && (strRep != "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.rep_code = '" + strRep + "'");
        }
        else if ((strAsm != "ALL") && (strDis != "ALL") && (strRep != "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.rep_code = '" + strRep + "'");
        }
        else if ((strAsm == "ALL") && (strDis != "ALL") && (strRep == "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.dis_id = '" + strDis + "'");
        }
        else if ((strAsm != "ALL") && (strDis != "ALL") && (strRep == "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.dis_id = '" + strDis + "'");
        }
        else if ((strAsm != "ALL") && (strDis == "ALL") && (strRep == "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.ase_id = '" + strAsm + "'");
        }

        rptDoc.SetParameterValue("@DbCodeList", "");
        rptDoc.SetParameterValue("@ReportFor", buttonbar1.GetShowReprtForValue());
        rptDoc.SetParameterValue("@SelectedASE", buttonbar1.GetAsesValue());
        rptDoc.SetParameterValue("@ReportType", buttonbar1.GetReportTypeValue());
        rptDoc.SetParameterValue("@AdditionalParams", FilterByReport());
         //Added by Rangan


        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }


    private string GetQuantitytype() //Added by Rangan
    {
        string quantitytype = "";
        return quantitytype;
    }

    private ReportDocument GetReportDocument_Excel()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (buttonbar1.GetReportTypeValue() == "v")
            rptDoc.Load(Server.MapPath("../reports/item_wise_sales_value_excel.rpt"));
        else
            rptDoc.Load(Server.MapPath("../reports/item_wise_sales_excel.rpt"));


        string title2 = "From " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " To " + GetToDate().Value.AddDays(-1).ToString("dd-MMM-yyyy");
        title2 = title2 + " By " + buttonbar1.GetReportTypeText();

        rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        rptDoc.SetParameterValue("rptTitle2", title2);
        rptDoc.SetParameterValue("group_name", buttonbar1.GetGroupByText());

        rptDoc.SetParameterValue("@StartDate", GetFromDate());
        rptDoc.SetParameterValue("@EndDate", GetToDate());
        rptDoc.SetParameterValue("@BrandId", Int32.Parse(buttonbar1.GetFilterByBrandValue()));
        rptDoc.SetParameterValue("@PackingId", Int32.Parse(buttonbar1.GetFilterByPackingMethodValue()));
        rptDoc.SetParameterValue("@FlavorId", Int32.Parse(buttonbar1.GetFilterByFlavorValue()));
        rptDoc.SetParameterValue("@CategoryId", Int32.Parse(buttonbar1.GetFilterByItemGroupValue()));
        rptDoc.SetParameterValue("@Hvp", buttonbar1.GetFilterByHVPValue());
        rptDoc.SetParameterValue("@GroupCol", buttonbar1.GetGroupByValue());

        string strAsm = buttonbar1.GetAsesValue();
        string strDis = buttonbar1.GetDistributorsValue();
        string strRep = buttonbar1.GetRepsValue();

        if ((strAsm == "ALL") && (strDis == "ALL") && (strRep == "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "");
        }
        else if ((strAsm == "ALL") && (strDis == "ALL") && (strRep != "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.rep_code = '" + strRep + "'");
        }
        else if ((strAsm == "ALL") && (strDis != "ALL") && (strRep != "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.rep_code = '" + strRep + "'");
        }
        else if ((strAsm != "ALL") && (strDis != "ALL") && (strRep != "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.rep_code = '" + strRep + "'");
        }
        else if ((strAsm == "ALL") && (strDis != "ALL") && (strRep == "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.dis_id = '" + strDis + "'");
        }
        else if ((strAsm != "ALL") && (strDis != "ALL") && (strRep == "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.dis_id = '" + strDis + "'");
        }
        else if ((strAsm != "ALL") && (strDis == "ALL") && (strRep == "ALL"))
        {
            rptDoc.SetParameterValue("@RepCodeList", "ih.ase_id = '" + strAsm + "'");
        }

        rptDoc.SetParameterValue("@DbCodeList", "");
        rptDoc.SetParameterValue("@ReportFor", buttonbar1.GetShowReprtForValue());
        rptDoc.SetParameterValue("@SelectedASE", buttonbar1.GetAsesValue());
        rptDoc.SetParameterValue("@ReportType", buttonbar1.GetReportTypeValue());
        rptDoc.SetParameterValue("@AdditionalParams", FilterByReport());


        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }



    #region Events

    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Item Wise Sales", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListReps = new usercontrols_buttonbar_reports.DropDownListReps(DropDownListReps_SelectionChanged);
        buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributors_SelectionChanged);
        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        buttonbar1.onDropDownListShowReportFor = new usercontrols_buttonbar_reports.DropDownListShowReportFor(DropDownListShowReportFor_SelectionChanged);
        buttonbar1.onDropDownListQuantityType = new usercontrols_buttonbar_reports.DropDownListQuantityType(DropDownListQuantityType_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(2);
            LoadOptions();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("ItemWiseSales_", Session.SessionID.ToString())) + ".pdf", GenerateFileName("ItemWiseSales_", Session.SessionID.ToString()) + ".pdf", repDoc);

        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListReps_SelectionChanged(object sender)
    {

    }

    protected void DropDownListDistributors_SelectionChanged(object sender)
    {
        LoadReps();

        buttonbar1.HandleShowReportForDropDownChanged();
        buttonbar1.EnableDistributorDropDown(true);

        buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();
        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.HandleASEDropdownEnable(false);
        }
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        LoadDistributors();
        LoadReps();

        buttonbar1.HandleShowReportForDropDownChanged();
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument_Excel();
            SaveExcel(Server.MapPath(GenerateFileName("ItemWiseSales", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("ItemWiseSales", Session.SessionID.ToString()) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListShowReportFor_SelectionChanged(object sender)
    {
        //Laod ASE's     
        LoadASEs();
        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);
        }

        if (!buttonbar1.GetShowReprtForValue().Equals("ase"))
        {
            LoadDistributors();
            LoadReps();
        }

        buttonbar1.HandleShowReportForDropDownChanged();
        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.HandleASEDropdownEnable(false);
        }
    }


    protected void DropDownListQuantityType_SelectionChanged(object sender)
    {

    }

    #endregion
}