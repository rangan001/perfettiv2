﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.DataAccess.datasets;
using CRMServiceReference;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net;
using System.Configuration;

public partial class dashboard_transaction_MonthlyRepSalesValuesReport : System.Web.UI.Page
{
    #region Load Drop Downs

    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        //if (buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
        //    if (aseList.Count > 0)
        //        aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string selectedASE = buttonbar1.GetAsesValue();

        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetDistributersByASMOriginator(args);
        //if (distList.Count > 0)
        //    distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    private void LoadReps()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string strASM = GetASEString();
        string strDIST = GetDisString();

        if (strDIST.Equals(""))
        {
            if (strASM.Equals(""))
                args.AdditionalParams = " originator.dept_string = 'DR' ";
            else
                args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_mast_asm.originator = '" + strASM + "' ";
        }
        else
        {
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_distributor.originator = '" + strDIST + "' ";
        }

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetAllSRByOriginator(args);
        //if (repList.Count > 0)
        //    repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetReps(repList, "UserName");
    }

    private OriginatorDTO GetDistributorById(string Id)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        OriginatorDTO distList = new OriginatorDTO();
        distList = originatorClient.GetOriginator(Id);

        return distList;
    }

    #endregion

    private void LoadOptions()
    {
        //Laod ASE's     
        LoadASEs();
        
        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetReportForSelect("rep");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }

        LoadDistributors();

        if (UserSession.Instance.OriginatorString == "DIST")
        {
            buttonbar1.SetReportForSelect("distributor");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect("ALL");
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();

            buttonbar1.SetDistributorsSelect(UserSession.Instance.UserName);
            buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();

            buttonbar1.SetTradeloadRepTypeSelect("p");

            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleDistributerDropdownEnable(false);
            buttonbar1.HandleTradeloadRepTypeDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleDistributerDropdownVisible(false);
            buttonbar1.HandleTradeloadRepTypeDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }

        LoadReps();
    }

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private DateTime? GetMonth()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetMonth());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportFor()
    {
        try
        {
            string reportfor = buttonbar1.GetShowReprtForValue();
            return reportfor;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = Convert.ToDateTime(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = Convert.ToDateTime(buttonbar1.GetToDateText());
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportSubName()
    {
        string selectedASE = buttonbar1.GetAsesValue();
        string selectedDIST = buttonbar1.GetDistributorsValue();
        string selectedREP = buttonbar1.GetRepsValue();

        return selectedREP;
    }

    private string GetASEString()
    {
        try
        {
            string reportfor = buttonbar1.GetAsesValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private string GetDisString()
    {
        try
        {
            string reportfor = buttonbar1.GetDistributorsValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }   
    
    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        rptDoc.Load(Server.MapPath("../reports/monthly_rep_sales_values_report.rpt"));
        rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        rptDoc.SetParameterValue("rptTitle2", GetMonth().Value.ToString("MMM-yyyy"));
        //rptDoc.SetParameterValue("rptMonth", GetMonth().Value.ToString("MM"));

        rptDoc.SetParameterValue("@Month", GetMonth());
        //rptDoc.SetParameterValue("@EndDate", GetToDate());
        //rptDoc.SetParameterValue("@BrandId", Int32.Parse(buttonbar1.GetFilterByBrandValue()));
        //rptDoc.SetParameterValue("@PackingId", Int32.Parse(buttonbar1.GetFilterByPackingMethodValue()));
        //rptDoc.SetParameterValue("@FlavorId", Int32.Parse(buttonbar1.GetFilterByFlavorValue()));
        //rptDoc.SetParameterValue("@CategoryId", Int32.Parse(buttonbar1.GetFilterByItemGroupValue()));
        //rptDoc.SetParameterValue("@ReportFor", GetReportFor());
        //rptDoc.SetParameterValue("@AseVal", "");
        //rptDoc.SetParameterValue("@monthNum", "09");
        rptDoc.SetParameterValue("@RepVal", buttonbar1.GetRepsValue());


        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }
        return rptDoc;
    }

    #region Events

    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Rep / Outlet / Month wise Sales Report", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListReps = new usercontrols_buttonbar_reports.DropDownListReps(DropDownListReps_SelectionChanged);
        buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributors_SelectionChanged);
        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(11);
            LoadOptions();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("MonthlyRepSalesValues", DateTime.Now.ToString("yymmddhhmm"))) + ".pdf", GenerateFileName("MonthlyRepSalesValues", DateTime.Now.ToString("yymmddhhmm")) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListReps_SelectionChanged(object sender)
    {

    }

    protected void DropDownListDistributors_SelectionChanged(object sender)
    {
        LoadReps();

        buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();
        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.HandleASEDropdownEnable(false);
        }
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        LoadDistributors();
        LoadReps();
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("MonthlyRepSalesValues", DateTime.Now.ToString("yymmddhhmm"))) + ".xlsx", GenerateFileName("MonthlyRepSalesValues", DateTime.Now.ToString("yymmddhhmm")) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    #endregion
}