﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="OpportunityAnalysis.aspx.cs" Inherits="dashboard_transaction_OpportunityAnalysis" %>
<%@ Register Src="~/usercontrols/buttonbar_analysisgraph.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/toolbar_analysisgraph.ascx" TagPrefix="ucl" TagName="toolbar" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="divcontectmainforms">
    <div class="title_bar">
        <div style="width:90%; float:left;">
                    <ucl:toolbar ID="toolbar1" runat="server" />
        </div>
        <div style="width:10%; float:right;">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/dashboard/transaction/CallCycleContact.aspx">
                        <div class="back"></div>
                        </asp:HyperLink>
        </div>    
     </div>
        <%--<div class="homemsgboxtopic">
            <div style="float: left; width: 65%">
                Hot Operator Activity Report
            </div>
            <div style="float: right; width: 35%;" align="right">
            <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/dashboard/transaction/AnalysisGraph.aspx?rptid=3">
                        <img src="../../assets/images/back_button.png" alt="" border="0" />
            </asp:HyperLink>                
            </div>
        </div>--%>
        <div class="clearall"></div>
        <div align="right">
            <div class="bg" align="left">
                <div id="div_message" class="savemsg" runat="server" style="display:none" ></div>        
                <div style="padding-left:10px;"><ucl:buttonbar ID="buttonbar1" runat="server" /></div>
            </div>
        </div>
        <div class="clearall">&nbsp;</div>  
        <div style="margin-top:25px;">
        
            <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto" runat="server"></div>

<div class="clearall">&nbsp;</div>

            <div style="min-width: 200px; overflow: auto;">
            <div id="gridLeadCustOppCount">
            </div>
            <div id="gridLeadCustOpp">
            </div>
                
            </div>
    </div></div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#div_repgroup").css("display", "block");
            $("#div_state").css("display", "block");
            $("#div_market").css("display", "block");
            $("#div_FromDate").css("display", "block");
            $("#div_ToDate").css("display", "block");
            $("#MainContent_toolbar1_a_opportunity").css("background-color", "#ff6600");
            
            GridLeadCustOppCount('');
            GridLeadCustOpp('');
        });

        
	</script>
       
</asp:Content>

