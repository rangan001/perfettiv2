﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CRMServiceReference;
using CrystalDecisions.Shared;
using System.Configuration;

public partial class dashboard_transaction_OtherReports : System.Web.UI.Page
{
    private ReportDocument repDoc = null;

    //#region Load Drop Downs

    private void LoadSFAInvoiceSummaryReportProducts()
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<ProductDTO> data = new List<ProductDTO>();

        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 0;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.AdditionalParams = "";
        args.StartIndex = 1;
        args.OrderBy = " code asc";

        data = commonClient.GetAllProductsDataAndCountWithOutImage(args);

        buttonbar1.SetSFAInvoiceSummaryReportProducts(data);
    }

    //private void LoadASEs()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.OrderBy = "name ASC";
    //    args.AdditionalParams = " dept_string = 'ASE' ";
    //    args.StartIndex = 1;
    //    args.RowCount = 500;
    //    OriginatorClient originatorClient = new OriginatorClient();
    //    List<OriginatorDTO> aseList = new List<OriginatorDTO>();
    //    aseList = originatorClient.GetOriginatorsByCatergory(args);

    //    if (buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
    //        if (aseList.Count > 0)
    //            aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

    //    buttonbar1.SetAses(aseList, "UserName");
    //}

    //private void LoadDistributors(string selectedASE)
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.OrderBy = "name ASC";
    //    if (selectedASE.Equals("ALL"))
    //        args.AdditionalParams = " dept_string = 'DIST' ";
    //    else
    //        args.AdditionalParams = " dept_string = 'DIST'  AND parent_originator = '" + selectedASE + "' ";
    //    args.StartIndex = 1;
    //    args.RowCount = 500;
    //    OriginatorClient originatorClient = new OriginatorClient();
    //    List<OriginatorDTO> distList = new List<OriginatorDTO>();

    //    if (buttonbar1.GetShowReprtForValue().Equals("rep"))//Dont add ALL to drop down if Distributor User selected
    //    {
    //        distList = originatorClient.GetOriginatorsByCatergory(args);
    //    }
    //    else//Dont add ALL to drop down if Distributor User selected
    //    {
    //        distList = originatorClient.GetOriginatorsByCatergory(args);
    //        if (distList.Count > 0)
    //            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });
    //    }

    //    buttonbar1.SetDistributors(distList, "UserName");
    //}

    private void LoadSFAInvoiceSummaryReportSalesReps()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string strASM = "";
        string strDIST = "";

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            strASM = UserSession.Instance.UserName;
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_mast_asm.originator = '" + strASM + "' ";
        }
        else if (UserSession.Instance.OriginatorString == "DIST")
        {
            strDIST = UserSession.Instance.UserName;
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_distributor.originator = '" + strDIST + "' ";
        }
        else 
        {
            strDIST = UserSession.Instance.UserName;
            args.AdditionalParams = " originator.dept_string = 'DR'";
        }

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetAllSRByOriginator(args);
        if (repList.Count > 1)
            repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetSFAInvoiceSummaryReportSalesRep(repList);
    }

    ////private void LoadFilterByBrands()
    ////{
    ////    ArgsDTO args = new ArgsDTO();
    ////    args.StartIndex = 1;
    ////    args.RowCount = 1000;
    ////    args.ROriginator = UserSession.Instance.OriginatorString;
    ////    args.Originator = UserSession.Instance.OriginalUserName;
    ////    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    ////    args.OrderBy = " name asc";
    ////    CommonClient commonClient = new CommonClient();
    ////    List<BrandDTO> brandList = new List<BrandDTO>();
    ////    brandList = commonClient.GetAllBrandsDataAndCount(args);
    ////    if (brandList.Count > 0)
    ////        brandList.Insert(0, new BrandDTO() { BrandId = 0, BrandName = "ALL" });
    ////    buttonbar1.SetBrands(brandList, "BrandId");
    ////}

    ////private void LoadFilterByPackingMethod()
    ////{
    ////    ArgsDTO args = new ArgsDTO();
    ////    args.StartIndex = 1;
    ////    args.RowCount = 1000;
    ////    args.ROriginator = UserSession.Instance.OriginatorString;
    ////    args.Originator = UserSession.Instance.OriginalUserName;
    ////    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    ////    args.OrderBy = " packing_name asc";

    ////    CommonClient commonClient = new CommonClient();
    ////    List<ProductPackingDTO> packingsList = new List<ProductPackingDTO>();
    ////    packingsList = commonClient.GetAllProductPackings(args);
    ////    if (packingsList.Count > 0)
    ////        packingsList.Insert(0, new ProductPackingDTO() { PackingId = 0, PackingName = "ALL" });
    ////    buttonbar1.SetPackingMethod(packingsList, "PackingId");
    ////}

    ////private void LoadFilterByItemGroup()
    ////{
    ////    ArgsDTO args = new ArgsDTO();
    ////    args.StartIndex = 1;
    ////    args.RowCount = 1000;
    ////    args.ROriginator = UserSession.Instance.OriginatorString;
    ////    args.Originator = UserSession.Instance.OriginalUserName;
    ////    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    ////    args.OrderBy = " id ";

    ////    CommonClient commonClient = new CommonClient();
    ////    List<ProductCategoryDTO> categoryList = new List<ProductCategoryDTO>();
    ////    categoryList = commonClient.GetAllProductCategories(args);
    ////    if (categoryList.Count > 0)
    ////        categoryList.Insert(0, new ProductCategoryDTO() { Id = 0, Descrition = "ALL" });
    ////    buttonbar1.SetItemGroup(categoryList, "Id");
    ////}

    ////private void LoadFilterByFlavor()
    ////{
    ////    ArgsDTO args = new ArgsDTO();
    ////    args.StartIndex = 1;
    ////    args.RowCount = 1000;
    ////    args.ROriginator = UserSession.Instance.OriginatorString;
    ////    args.Originator = UserSession.Instance.OriginalUserName;
    ////    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    ////    args.OrderBy = " flavor_name asc";

    ////    CommonClient commonClient = new CommonClient();
    ////    List<FlavorDTO> flavorsList = new List<FlavorDTO>();
    ////    flavorsList = commonClient.GetAllFlavor();
    ////    if (flavorsList.Count > 0)
    ////        flavorsList.Insert(0, new FlavorDTO() { FlavorId = 0, FlavorName = "ALL" });
    ////    buttonbar1.SetFlavor(flavorsList, "FlavorId");
    ////}

    //#endregion

    //private void LoadOptions()
    //{
    //    ////Laod ASE's     
    //    //LoadASEs();

    //    ////Load Distributors
    //    //string selectedASE = buttonbar1.GetAsesValue();
    //    //LoadDistributors(selectedASE);

    //    //////Load Reps
    //    //string selectedDistributor = buttonbar1.GetDistributorsValue();
    //    //LoadReps(selectedDistributor);
    //}

    //private string GetASEsString()
    //{
    //    ListItemCollection aseList = buttonbar1.GetAllASEsInDropDown();
    //    string aseString = "";

    //    if (aseList != null)
    //    {
    //        if (aseList.Count > 0)
    //        {
    //            aseString = " originator IN (";
    //            foreach (ListItem item in aseList)
    //            {
    //                aseString = aseString + "'" + item.Value + "',";
    //            }
    //            aseString = aseString.Replace("'ALL',", "");
    //            aseString = aseString.Remove(aseString.Length - 1, 1);//Removing Last Comma
    //            aseString = aseString + ") ";
    //        }
    //    }

    //    return aseString;
    //}

    //private string GetDistributorsString()
    //{
    //    ListItemCollection distList = buttonbar1.GetAllDistributorsInDropDown();
    //    string distString = "";

    //    if (distList != null)
    //    {
    //        if (distList.Count > 0)
    //        {
    //            distString = " originator IN (";
    //            foreach (ListItem item in distList)
    //            {
    //                distString = distString + "'" + item.Value + "',";
    //            }
    //            distString = distString.Replace("'ALL',", "' ',");
    //            distString = distString.Remove(distString.Length - 1, 1);//Removing Last Comma
    //            distString = distString + ") ";
    //        }
    //    }

    //    return distString;
    //}

    //private string GetRepsString()
    //{
    //    ListItemCollection repsList = buttonbar1.GetAllRepsInDropDown();
    //    string repString = "";

    //    if (repsList != null)
    //    {
    //        if (repsList.Count > 0)
    //        {
    //            repString = " originator IN (";
    //            foreach (ListItem item in repsList)
    //            {
    //                repString = repString + "'" + item.Value + "',";
    //            }
    //            repString = repString.Replace("'ALL',", "'',");
    //            repString = repString.Remove(repString.Length - 1, 1);//Removing Last Comma
    //            repString = repString + ") ";
    //        }
    //        else
    //        {
    //            repString = " originator IN ( ' ' ) ";
    //        }
    //    }
    //    else
    //    {
    //        repString = " originator IN ( ' ' ) ";
    //    }

    //    return repString;
    //}

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = Convert.ToDateTime(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = Convert.ToDateTime(buttonbar1.GetToDateText());
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
     
    private string GetReportName()
    {
        try
        {
            string repName = buttonbar1.GetOtherReportName();
            return repName;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetHvpValue()
    {
        try
        {
            string hvpValue = buttonbar1.GetFilterByHVPValue();
            return hvpValue;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    //private string GetReportSubName_Ase()
    //{
    //    string reportName = "";
    //    reportName = "ASE : " + buttonbar1.GetAsesText();

    //    return reportName;
    //}

    //private string GetReportSubName_Dis()
    //{
    //    string reportName = "";

    //    string strDisValue = buttonbar1.GetDistributorsValue();

    //    if (GetDistributorById(strDisValue) == null)
    //    {
    //        reportName = "Distributor : " + buttonbar1.GetDistributorsText();
    //    }
    //    else
    //    {
    //        reportName = "Distributor : " + buttonbar1.GetDistributorsText();
    //    }

    //    return reportName;
    //}

    //private string GetReportSubName_DisCode()
    //{
    //    string reportName = "";

    //    string strDisValue = buttonbar1.GetDistributorsValue();

    //    if (GetDistributorById(strDisValue) == null)
    //    {
    //        reportName = "";
    //    }
    //    else
    //    {
    //        reportName = "DB Code : " + GetDistributorById(strDisValue).OriginatorId;
    //    }

    //    return reportName;
    //}

    //private OriginatorDTO GetDistributorById(string Id)
    //{
    //    OriginatorClient originatorClient = new OriginatorClient();
    //    OriginatorDTO distList = new OriginatorDTO();
    //    distList = originatorClient.GetOriginator(Id);

    //    return distList;

    //    //if (!buttonbar1.GetShowReprtForValue().Equals("distributor"))//Dont add ALL to drop down if Distributor User selected
    //    //    if (distList.Count > 0)
    //    //        distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

    //    //buttonbar1.SetDistributors(distList, "UserName");
    //}

    //private string GetReportSubName_Rep()
    //{
    //    string reportName = "";
    //    reportName = "Rep : " + buttonbar1.GetRepsText();

    //    return reportName;
    //}

    #region report methods

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        try
        {
            rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
            string FilePath = Server.MapPath(generatefilename);
            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
        }
        catch (Exception)
        {
            throw;
        }
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private string GetSales_Rep()
    {
        try
        {
            string prod_code = buttonbar1.GetSFASalesRep();
            return prod_code;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetProd_code1()
    {
        try
        {
            string prod_code = buttonbar1.GetSFAProduct1();
            return prod_code;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetProd_code2()
    {
        try
        {
            string prod_code = buttonbar1.GetSFAProduct2();
            return prod_code;
        }
        catch
        {
            return null;
        }
    }

    private string GetInvoiceAmount()
    {
        try
        {
            string invAmt = (buttonbar1.GetSFAInvoiceAmount() == "") ? "0" : buttonbar1.GetSFAInvoiceAmount();
            return invAmt;
        }
        catch
        {
            return null;
        }
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();
        try
        {
            if (GetReportName() == "sfa_working_hours")
            {
                rptDoc.Load(Server.MapPath("../reports/sfa_working_hours.rpt"));
                rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
                rptDoc.SetParameterValue("@start_date", GetFromDate());
                rptDoc.SetParameterValue("@end_date", GetToDate());
                rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
            }
            else if (GetReportName() == "sfa_invoice_summary")
            {
                rptDoc.Load(Server.MapPath("../reports/sfa_invoice_summary.rpt"));
                //rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
                rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
                rptDoc.SetParameterValue("@rep_code", GetSales_Rep());
                rptDoc.SetParameterValue("@start_date", GetFromDate());
                rptDoc.SetParameterValue("@end_date", GetToDate());
                rptDoc.SetParameterValue("@prod_code1", GetProd_code1());
                rptDoc.SetParameterValue("@prod_code2", GetProd_code2());
                rptDoc.SetParameterValue("@prod_amt", GetInvoiceAmount());
                rptDoc.SetParameterValue("@prod_code1_heading", GetProd_code1());
                rptDoc.SetParameterValue("@prod_code2_heading", GetProd_code2());
                rptDoc.SetParameterValue("@prod_invamt_heading", GetInvoiceAmount());
                //rptDoc.SetParameterValue("@hvp_value", GetHvpValue());
            }
            else if (GetReportName() == "sfa_order_summary")
            {
                rptDoc.Load(Server.MapPath("../reports/sfa_order_summary.rpt"));
                //rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
                rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
                rptDoc.SetParameterValue("@rep_code", GetSales_Rep());
                rptDoc.SetParameterValue("@start_date", GetFromDate());
                rptDoc.SetParameterValue("@end_date", GetToDate());
                rptDoc.SetParameterValue("@prod_code1", GetProd_code1());
                rptDoc.SetParameterValue("@prod_code2", GetProd_code2());
                rptDoc.SetParameterValue("@prod_amt", GetInvoiceAmount());
                rptDoc.SetParameterValue("@prod_code1_heading", GetProd_code1());
                rptDoc.SetParameterValue("@prod_code2_heading", GetProd_code2());
                rptDoc.SetParameterValue("@prod_invamt_heading", GetInvoiceAmount());
                //rptDoc.SetParameterValue("@hvp_value", GetHvpValue());
            }
            else if (GetReportName() == "daily_absent_reps")
            {
                rptDoc.Load(Server.MapPath("../reports/daily_absent_reps.rpt"));
                //rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
                rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
                rptDoc.SetParameterValue("@StartDate", GetFromDate());
                rptDoc.SetParameterValue("@EndDate", GetToDate());
            }
            else if (GetReportName() == "asm_visits")
            {
                rptDoc.Load(Server.MapPath("../reports/asm_visits.rpt"));
                //rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
                rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
                rptDoc.SetParameterValue("@StartDate", GetFromDate());
                rptDoc.SetParameterValue("@EndDate", GetToDate());
            }

            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;

            string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

            crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
            crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
            crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
            crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

            CrTables = rptDoc.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }
        }
        catch (Exception ex)
        {
            throw;
        }

        return rptDoc;
    }

    //private ReportDocument GetReportDocument_Excel()
    //{
    //    ReportDocument rptDoc = new ReportDocument();
    //    try
    //    {
    //        if (GetReportName() == "sfa_working_hours")
    //        {
    //            rptDoc.Load(Server.MapPath("../reports/sfa_working_hours.rpt"));
    //            rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
    //            rptDoc.SetParameterValue("@start_date", GetFromDate());
    //            rptDoc.SetParameterValue("@end_date", GetToDate());
    //            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
    //        }
    //        else if (GetReportName() == "sfa_invoice_summary")
    //        {
    //            rptDoc.Load(Server.MapPath("../reports/sfa_invoice_summary_excel.rpt"));
    //            //rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
    //            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
    //            rptDoc.SetParameterValue("@rep_code", GetSales_Rep());
    //            rptDoc.SetParameterValue("@start_date", GetFromDate());
    //            rptDoc.SetParameterValue("@end_date", GetToDate());
    //            rptDoc.SetParameterValue("@prod_code1", GetProd_code1());
    //            rptDoc.SetParameterValue("@prod_code2", GetProd_code2());
    //            rptDoc.SetParameterValue("@prod_amt", GetInvoiceAmount());
    //            rptDoc.SetParameterValue("@prod_code1_heading", GetProd_code1());
    //            rptDoc.SetParameterValue("@prod_code2_heading", GetProd_code2());
    //            rptDoc.SetParameterValue("@prod_invamt_heading", GetInvoiceAmount());
    //        }
    //        else if (GetReportName() == "sfa_order_summary")
    //        {
    //            rptDoc.Load(Server.MapPath("../reports/sfa_order_summary.rpt"));
    //            //rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
    //            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
    //            rptDoc.SetParameterValue("@rep_code", GetSales_Rep());
    //            rptDoc.SetParameterValue("@start_date", GetFromDate());
    //            rptDoc.SetParameterValue("@end_date", GetToDate());
    //            rptDoc.SetParameterValue("@prod_code1", GetProd_code1());
    //            rptDoc.SetParameterValue("@prod_code2", GetProd_code2());
    //            rptDoc.SetParameterValue("@prod_amt", GetInvoiceAmount());
    //            rptDoc.SetParameterValue("@prod_code1_heading", GetProd_code1());
    //            rptDoc.SetParameterValue("@prod_code2_heading", GetProd_code2());
    //            rptDoc.SetParameterValue("@prod_invamt_heading", GetInvoiceAmount());
    //        }
    //        else if (GetReportName() == "daily_absent_reps")
    //        {
    //            rptDoc.Load(Server.MapPath("../reports/daily_absent_reps.rpt"));
    //            //rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
    //            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
    //            rptDoc.SetParameterValue("@StartDate", GetFromDate());
    //            rptDoc.SetParameterValue("@EndDate", GetToDate());
    //        }
    //        else if (GetReportName() == "asm_visits")
    //        {
    //            rptDoc.Load(Server.MapPath("../reports/asm_visits.rpt"));
    //            //rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
    //            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
    //            rptDoc.SetParameterValue("@StartDate", GetFromDate());
    //            rptDoc.SetParameterValue("@EndDate", GetToDate());
    //        }

    //        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
    //        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
    //        ConnectionInfo crConnectionInfo = new ConnectionInfo();
    //        Tables CrTables;

    //        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

    //        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
    //        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
    //        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
    //        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

    //        CrTables = rptDoc.Database.Tables;
    //        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
    //        {
    //            crtableLogoninfo = CrTable.LogOnInfo;
    //            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
    //            CrTable.ApplyLogOnInfo(crtableLogoninfo);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw;
    //    }

    //    return rptDoc;
    //}

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Other Reports", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListOtherReports = new usercontrols_buttonbar_reports.DropDownListOtherReports(DropDownListShowReportFor_SelectionChanged);

        if (!IsPostBack)
        {
            LoadSFAInvoiceSummaryReportProducts();
            LoadSFAInvoiceSummaryReportSalesReps();
            buttonbar1.SetVisible(9);
        }
        else
        {
            string control_id = getPostBackControlName();

            //Call the method, except drodown change event
            if (!(control_id.Equals("cmbShowReportFor") || control_id.Equals("cmbDistributors") || control_id.Equals("cmbAse")))
                ButtonFilter_Click(this);
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            if (GetReportName() == "sfa_working_hours")
            {
                SavePDF(Server.MapPath(GenerateFileName("SFA_Working_Hour", Session.SessionID.ToString()) + ".pdf"), GenerateFileName("SFA_Working_Hour", Session.SessionID.ToString()) + ".pdf", repDoc);
            }
            else if (GetReportName() == "sfa_invoice_summary")
            {
                SavePDF(Server.MapPath(GenerateFileName("SFA_Invoice_Summary", Session.SessionID.ToString()) + ".pdf"), GenerateFileName("SFA_Invoice_Summary", Session.SessionID.ToString()) + ".pdf", repDoc);
            }
            else if (GetReportName() == "sfa_order_summary")
            {
                SavePDF(Server.MapPath(GenerateFileName("SFA_Order_Summary", Session.SessionID.ToString()) + ".pdf"), GenerateFileName("SFA_Order_Summary", Session.SessionID.ToString()) + ".pdf", repDoc);
            }
            else if (GetReportName() == "daily_absent_reps")
            {
                SavePDF(Server.MapPath(GenerateFileName("DailyAbsentReps", Session.SessionID.ToString()) + ".pdf"), GenerateFileName("DailyAbsentReps", Session.SessionID.ToString()) + ".pdf", repDoc);
            }
            else if (GetReportName() == "asm_visits")
            {
                SavePDF(Server.MapPath(GenerateFileName("ASMVisits", Session.SessionID.ToString()) + ".pdf"), GenerateFileName("ASMVisits", Session.SessionID.ToString()) + ".pdf", repDoc);
            }
        }
        catch (Exception ex)
        {
            
        }
    }

    //protected void DropDownListReps_SelectionChanged(object sender)
    //{

    //}

    //protected void DropDownListDistributors_SelectionChanged(object sender)
    //{
    //    //Load Reps
    //    string selectedDistributor = buttonbar1.GetDistributorsValue();
    //    LoadReps(selectedDistributor);
    //}

    //protected void DropDownListAse_SelectionChanged(object sender)
    //{
    //    //Load Distributors
    //    string selectedASE = buttonbar1.GetAsesValue();
    //    LoadDistributors(selectedASE);
    //    string selectedDistributor = buttonbar1.GetDistributorsValue();
    //    LoadReps(selectedDistributor);

    //    buttonbar1.HandleShowReportForDropDownChanged();
    //    //buttonbar1.EnableDistributorDropDown(true);
    //}

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            
            if (GetReportName() == "sfa_working_hours")
            {
                SaveExcel(Server.MapPath(GenerateFileName("SFA_Working_Hour", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("SFA_Working_Hour", Session.SessionID.ToString()) + ".xlsx", repDoc);
            }
            else if (GetReportName() == "sfa_invoice_summary")
            {
                SaveExcel(Server.MapPath(GenerateFileName("SFA_Invoice_Summary", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("SFA_Invoice_Summary", Session.SessionID.ToString()) + ".xlsx", repDoc);
            }
            else if (GetReportName() == "sfa_order_summary")
            {
                SaveExcel(Server.MapPath(GenerateFileName("SFA_Order_Summary", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("SFA_Order_Summary", Session.SessionID.ToString()) + ".xlsx", repDoc);
            }
            else if (GetReportName() == "daily_absent_reps")
            {
                SaveExcel(Server.MapPath(GenerateFileName("DailyAbsentReps", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("DailyAbsentReps", Session.SessionID.ToString()) + ".xlsx", repDoc);
            }
            else if (GetReportName() == "asm_visits")
            {
                SaveExcel(Server.MapPath(GenerateFileName("ASMVisits", Session.SessionID.ToString()) + ".xlsx"), GenerateFileName("ASMVisits", Session.SessionID.ToString()) + ".xlsx", repDoc);
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListShowReportFor_SelectionChanged(object sender)
    {
        ////Laod ASE's     
        //LoadASEs();

        ////Load Distributors
        //string selectedASE = buttonbar1.GetAsesValue();
        //LoadDistributors(selectedASE);

        //////Load Reps
        //string selectedDistributor = buttonbar1.GetDistributorsValue();
        //LoadReps(selectedDistributor);

        //buttonbar1.HandleShowReportForDropDownChanged();
    }

    private string getPostBackControlName()
    {
        Control control = null;
        //first we will check the "__EVENTTARGET" because if post back made by       the controls
        //which used "_doPostBack" function also available in Request.Form collection.
        string ctrlname = Page.Request.Params["__EVENTTARGET"];
        if (ctrlname != null && ctrlname != String.Empty)
        {
            control = Page.FindControl(ctrlname);
        }
        // if __EVENTTARGET is null, the control is a button type and we need to
        // iterate over the form collection to find it
        else
        {
            string ctrlStr = String.Empty;
            Control c = null;
            foreach (string ctl in Page.Request.Form)
            {
                //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                //mouse x and y coordinates
                if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                {
                    ctrlStr = ctl.Substring(0, ctl.Length - 2);
                    c = Page.FindControl(ctrlStr);
                }
                else
                {
                    c = Page.FindControl(ctl);
                }
                if (c is System.Web.UI.WebControls.Button ||
                         c is System.Web.UI.WebControls.ImageButton)
                {
                    control = c;
                    break;
                }
            }
        }
        return control.ID;

    }

    #endregion
}