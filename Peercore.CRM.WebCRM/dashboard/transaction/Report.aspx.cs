﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.DataAccess.datasets;
using CRMServiceReference;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net;
using System.Configuration;

public partial class dashboard_transaction_Report : System.Web.UI.Page
{
    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private string GetReport()
    {
        try
        {
            string report = buttonbar1.GetShowReportValue();
            return report;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetIdleDays()
    {
        try
        {
            string report = buttonbar1.GetShowIdleValue();
            return report;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = Convert.ToDateTime(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    
    private string GetDivision()
    {
        try
        {
            string returnVal = buttonbar1.GetDivisionValue();
            return returnVal;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetTerritory()
    {
        try
        {
            string returnVal = buttonbar1.GetTerritoryValue();
            return returnVal;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = Convert.ToDateTime(buttonbar1.GetToDateText());
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportSubName()
    {
        string reportName_Ase = "Rep working hours ";


        return "For " + reportName_Ase;
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();


        //if (GetReport() == "hours")
        //{
        //    rptDoc.Load(Server.MapPath("../reports/Rep_working_hours.rpt"));
        //    rptDoc.SetParameterValue("@start_date", GetFromDate());
        //    rptDoc.SetParameterValue("@end_date", GetToDate());
        //}
        if (GetReport() == "Idle")
        {
            rptDoc.Load(Server.MapPath("../reports/Idle_customers.rpt"));
            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
            rptDoc.SetParameterValue("@start_date", GetFromDate());
            rptDoc.SetParameterValue("@territory", GetTerritory());
        }
        //else if (GetReport() == "cancle")
        //{
        //    rptDoc.Load(Server.MapPath("../reports/cancle_invoices.rpt"));
        //    rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
        //    rptDoc.SetParameterValue("@start_date", GetFromDate());
        //    rptDoc.SetParameterValue("@end_date", GetToDate());
        //}
        else if (GetReport() == "idl_pro")
        {
            rptDoc.Load(Server.MapPath("../reports/idle_productlist.rpt"));
            rptDoc.SetParameterValue("@start_date", GetFromDate());
            rptDoc.SetParameterValue("@territory", GetTerritory());
        }



        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }
        return rptDoc;
    }

    #region Events

    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        //I1.Attributes["src"] = "http://localhost:56186/";

        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Other Report", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListShowReport = new usercontrols_buttonbar_reports.DropDownListShowReport(DropDownListReport_SelectionChanged);
        //buttonbar1.onDropDownListReps = new usercontrols_buttonbar_reports.DropDownListReps(DropDownListReps_SelectionChanged);
        //buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetDivisions();
            buttonbar1.SetVisible(10);
            LoadOptions();
        }
    }

    private void LoadOptions()
    {
        LoadTerritory();
    }

    private void LoadTerritory()
    {
        ArgsModel args = new ArgsModel();
        args.OrderBy = "territory_name ASC";

        string selectedASE = "ALL";
        string selectedDis = "ALL";

        if (!selectedASE.Equals("ALL"))
        {
            if (selectedDis.Equals("ALL"))
            {
                args.AdditionalParams = " crm_mast_asm.originator = '" + selectedASE + "' ";
            }
            else
            {
                args.AdditionalParams = " crm_distributor.originator = '" + selectedDis + "' ";
            }
        }

        args.StartIndex = 1;
        args.RowCount = 500;

        MasterClient masterClient = new MasterClient();
        List<TerritoryModel> terrList = new List<TerritoryModel>();
        terrList = masterClient.GetAllTerritoryByOriginator(args,
            UserSession.Instance.OriginatorString,
            UserSession.Instance.UserName);

        if (terrList.Count > 0)
            terrList.Insert(0, new TerritoryModel() { TerritoryName = "ALL", TerritoryId = 0 });

        buttonbar1.SetTerritories(terrList, "TerritoryId", "TerritoryName");
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            ReportDocument rptDoc = GetReportDocument();

            if (GetReport() == "Idle")
            {
                SavePDF(Server.MapPath(GenerateFileName("CancelInvoice", Session.SessionID.ToString())) + ".pdf", GenerateFileName("CancelInvoice", Session.SessionID.ToString()) + ".pdf", rptDoc);
            }
            else if (GetReport() == "idl_pro")
            {
                SavePDF(Server.MapPath(GenerateFileName("IdleProducts", Session.SessionID.ToString())) + ".pdf", GenerateFileName("IdleProducts", Session.SessionID.ToString()) + ".pdf", rptDoc);
            }
            
        }
        catch (Exception ex)
        {
        }
    }

    //protected void DropDownListReps_SelectionChanged(object sender)
    //{
    //}

    protected void DropDownListReport_SelectionChanged(object sender)
    {
        //LoadOptions();
        buttonbar1.HandleShowReportDropDownChangedReportDetails();

    }

    //protected void DropDownListAse_SelectionChanged(object sender)
    //{
    //    //Load Distributors
    //    string selectedASE = buttonbar1.GetAsesValue();
    //   // LoadDistributors(selectedASE);
    //}

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            ReportDocument rptDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("CancelInvoice", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("CancelInvoice", Session.SessionID.ToString()) + ".xlsx", rptDoc);
        }
        catch (Exception ex)
        {
        }
    }

    #endregion
}