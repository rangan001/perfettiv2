﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Peercore.CRM.Shared;
using CRMServiceReference;
using CrystalDecisions.Shared;
using System.Configuration;

public partial class dashboard_transaction_RepsItemSalesBrand : System.Web.UI.Page
{
    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Reps Item Sales - Brands", "#", "");
        this.repDoc = new ReportDocument();

        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListDivisions = new usercontrols_buttonbar_reports.DropDownListDivisions(DropDownListDivisions_SelectionChanged);
        buttonbar1.onDropDownListRepSalesItemsBrand = new usercontrols_buttonbar_reports.DropDownListRepSalesItemsBrand(DropDownListRepSalesItemsReportType_SelectionChanged);
        //buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetDivisions();
            buttonbar1.SetVisible(16);
            //LoadOptions();
            LoadProducts(-1);
            LoadFilterByBrands();
        }
    }

    protected void DropDownListDivisions_SelectionChanged(object sender)
    {
        //Load Reps
        string selectedDivision = buttonbar1.GetDivisionValue();
        LoadProducts(int.Parse(selectedDivision));
    }

    protected void DropDownListRepSalesItemsReportType_SelectionChanged(object sender)
    {
        //if (GetReportType() == "ic")
        //{
        //    buttonbar1.SetVisible(15);
        //}
        //else
        //{
        //    buttonbar1.SetVisible(16);
        //}
    }

    private void LoadProducts(int strDivisionId)
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        //if (strDivisionId.Equals(-1))
        args.AdditionalParams = " ";
        //else
        //    args.AdditionalParams = " parent_originator = '" + strDivisionId + "' ";
        args.StartIndex = 1;
        args.RowCount = 1000;

        CommonClient commonClient = new CommonClient();
        List<ProductDTO> prodList = new List<ProductDTO>();

        prodList = commonClient.GetProductsByDivisionId(strDivisionId, args);
        if (prodList.Count > 0)
            prodList.Insert(0, new ProductDTO() { Name = "ALL", ProductId = -1 });

        buttonbar1.SetProducts(prodList, "ProductId");
    }

    private void LoadFilterByBrands()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " name asc";
        CommonClient commonClient = new CommonClient();
        List<BrandDTO> brandList = new List<BrandDTO>();
        brandList = commonClient.GetAllBrandsDataAndCount(args);
        if (brandList.Count > 0)
            brandList.Insert(0, new BrandDTO() { BrandId = 0, BrandName = "ALL" });
        buttonbar1.SetBrands(brandList, "BrandId");
    }


    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("RepsItemSalesBrand", DateTime.Now.ToString("yymmddhhmm"))) + ".pdf", GenerateFileName("RepsItemSalesBrand", DateTime.Now.ToString("yymmddhhmm")) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("RepsItemSalesBrand", DateTime.Now.ToString("yymmddhhmm"))) + ".xlsx", GenerateFileName("RepsItemSalesBrand", DateTime.Now.ToString("yymmddhhmm")) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (GetReportType() == "q")
            rptDoc.Load(Server.MapPath("../reports/reps_item_sales_report_brand.rpt"));
        else if (GetReportType() == "v")
            rptDoc.Load(Server.MapPath("../reports/reps_item_sales_report_value_brand.rpt"));
        else if (GetReportType() == "ic")
            rptDoc.Load(Server.MapPath("../reports/reps_item_sales_report_invoicecount_brand.rpt"));

        //rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        //rptDoc.SetParameterValue("rptTitle2", GetMonth().Value.ToString("MMM-yyyy"));
        //rptDoc.SetParameterValue("rptMonth", GetMonth().Value.ToString("MM"));
        rptDoc.SetParameterValue("@Originator", UserSession.Instance.UserName);
        rptDoc.SetParameterValue("@StartDate", GetFromDate());
        rptDoc.SetParameterValue("@ToDate", GetToDate());
        //rptDoc.SetParameterValue("@BrandId", Int32.Parse(buttonbar1.GetFilterByBrandValue()));
        //rptDoc.SetParameterValue("@PackingId", Int32.Parse(buttonbar1.GetFilterByPackingMethodValue()));
        //rptDoc.SetParameterValue("@FlavorId", Int32.Parse(buttonbar1.GetFilterByFlavorValue()));
        //rptDoc.SetParameterValue("@CategoryId", Int32.Parse(buttonbar1.GetFilterByItemGroupValue()));
        rptDoc.SetParameterValue("@ReportType", GetReportType());
        //rptDoc.SetParameterValue("@AseVal", "");
        //rptDoc.SetParameterValue("@monthNum", "09");
        rptDoc.SetParameterValue("@DivisionId", GetDivision());
        //rptDoc.SetParameterValue("@ProductId", GetProduct());
        rptDoc.SetParameterValue("@BrandId", GetBrand());

        //This is for PAge Setup for Print
        //PrintOptions boPrintOptions = rptDoc.PrintOptions;
        //// The paper size used for this report is  '14x7'.
        //System.Drawing.Printing.PrinterSettings oPrinterSettings = new System.Drawing.Printing.PrinterSettings();
        //oPrinterSettings.PrinterName = "     ";
        //foreach (System.Drawing.Printing.PaperSize oPaperSize in oPrinterSettings.PaperSizes)
        //{
        //    if ("14x7" == oPaperSize.PaperName)
        //    {
        //        boPrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)oPaperSize.RawKind;
        //        break;
        //    }
        //}

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }

    private string GetReportType()
    {
        try
        {
            string reportfor = buttonbar1.GetReportTypeRepsItemSalesBrandValue();
            return reportfor;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetDivision()
    {
        try
        {
            string val = buttonbar1.GetDivisionValue();
            return val;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetProduct()
    {
        try
        {
            string val = buttonbar1.GetProductValue();
            return val;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetBrand()
    {
        try
        {
            string val = buttonbar1.GetFilterByBrandValue();
            return val;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetToDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + sessionid.Trim() + "_";

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }
}