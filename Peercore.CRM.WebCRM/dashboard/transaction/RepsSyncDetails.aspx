﻿<%@ Page Title="mSales - Reports" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="RepsSyncDetails.aspx.cs" Inherits="dashboard_transaction_RepsSyncDetails" %>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server"/>
    <asp:HiddenField ID="OriginatorTypeString" runat="server"/>

    <div class="divcontectmainforms">
        <div class="title_bar">
            <div style="width: 90%; float: left;">
            </div>
            <div style="width: 10%; float: right;">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back"></div>
                </asp:HyperLink>
            </div>
        </div>
        <div>
            <input type="button" id="btnPushNotify" value="Send Notification to Sync" class="push_notify" />
            <div class="clearall">
            </div>
            <div class="grid_container">
                <div id="grid">
                </div>
            </div>
        </div>
        <asp:Button ID="btnViewSyncReport" runat="server" Text="View Sync Details"
            Style="margin-bottom: 3px; margin-left: 10px;" OnClick="btnViewSyncReport_Click"></asp:Button>
        <%--<asp:Button ID="btnViewNotifyIsSync" runat="server" Text="View Sync Details"
            Style="margin-bottom: 3px; margin-left: 10px;"
            OnClick="btnViewNotifyIsSync_Click"></asp:Button>
        <asp:Button ID="btnExportNotifyIsSync" runat="server" Text="Export Sync Details"
            Style="margin-bottom: 3px; margin-left: 10px;" 
            OnClick="btnExportNotifyIsSync_Click"></asp:Button>--%>
        <div style="margin-top: 25px;">
            <div style="min-width: 500px; height: 500px;" id="target">
                <embed src="" width="100%" height="100%" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#MainContent_toolbar1_a_report").css("background-color", "#ff6600");

            LoadSyncData(false);

        });

        $("#btnPushNotify").click(function () {
            debugger;
            var originatorType = $("#MainContent_OriginatorTypeString").val();
            var originator = $("#MainContent_OriginatorString").val();
            var url = "https://localhost:44362/api/sync";

            var items = $("#grid").data("kendoGrid").dataSource.data();

            var SyncDevices = '';
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.IsRepSync == true) {
                    SyncDevices = SyncDevices + item.RepCode + '-' + item.EmeiNo + ',';
                }
                //SyncDevices.push({
                //    repcode: item.RepCode,
                //    imei: item.EmeiNo
                //});
            }

            var root = ROOT_PATH + "service/master/master.asmx/UpdateRepsSyncData";
            debugger;

            $.ajax({
                type: "POST",
                url: root,
                data: '{ apiUri: "' + url + '", ' +
                    ' OriginatorType: "' + originatorType + '", ' +
                    ' Originator: "' + originator + '", ' +
                    ' DeviceIMEI: "' + SyncDevices + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        LoadSyncData(true);
                        RefreshGrid();
                    }
                    else {
                        alert('Sync notification is not working.')
                    }
                },
                failure: OnFail
            });
            
        });

        var interval = null

        function RefreshGrid() {
            interval = setInterval(function () {
                var grid = $("#grid").data("kendoGrid");
                grid.dataSource.read(); 
                grid.refresh(); 
            }, 5000);
        }

        function loadpdf(name) {
            $("#target embed").attr("src", name);
        }

        function LoadSyncData(IsClear) {
            var originatorType = $("#MainContent_OriginatorTypeString").val();
            var originator = $("#MainContent_OriginatorString").val();

            var take_grid = $("#MainContent_hfPageIndex").val();
            $("#grid").html("");
            $("#grid").kendoGrid({
                // height: 400,
                columns: [
                    {
                        field: "IsRepSync",
                        title: "",
                        template: "<input type=\"checkbox\" #= IsRepSync ? checked='checked' : '' # class=\"check_row_srsync\" id=\"#=EmeiNo#\" />",
                        headerTemplate: '<input type=\"checkbox\" class=\"checkAll_row_srsync\" />',
                        width: "70px",
                        filterable: false,
                        sortable: false
                    },
                    { field: "RepId", title: "RepId", width: "85px", hidden: true },
                    { field: "RepCode", title: "SR Code", width: "95px" },
                    { field: "RepName", title: "SR Name", width: "160px" },
                    { field: "EmeiNo", title: "IMEI", width: "130px" },
                    { field: "TerritoryName", title: "Territory", width: "98px" },
                    {
                        field: "DeliveryStatus",
                        title: "Delivery Status",
                        template: "<input type=\"checkbox\" #= DeliveryStatus ? checked='checked' : '' # />",
                        width: "110px"
                    },
                    {
                        field: "IsSynced",
                        title: "Sync Status",
                        template: "<input type=\"checkbox\" #= IsSynced ? checked='checked' : '' # />",
                        width: "110px"
                    }
                ],
                editable: false, // enable editing
                pageable: false,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: true,
                //dataBound: LeadCustomerGrid_onDataBound,
                //toolbar:
                //    [
                //        { name: "all", text: "Select all" },
                //        { name: "none", text: "Deselect all" }
                //    ],
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 500,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                IsRepSync: { validation: { required: true } },
                                RepId: { validation: { required: true } },
                                RepCode: { validation: { required: true } },
                                RepName: { editable: true, nullable: true },
                                DeviceToken: { validation: { required: true } },
                                EmeiNo: { validation: { required: true } },
                                AsmName: { validation: { required: true } },
                                TerritoryCode: { validation: { required: true } },
                                TerritoryName: { validation: { required: true } },
                                DeliveryStatus: { validation: { required: true } },
                                IsSynced: { validation: { required: true } },
                                rowCount: { type: "number", validation: { required: true } }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/master/master.asmx/GetAllSalesRepSyncData", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { OriginatorType: originatorType, Originator: originator, IsClear: IsClear, pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    var entityGrid = $("#grid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    },
                    aggregate: [{ field: "rowCount", aggregate: "max" }]
                }
            });

            var grid = $("#grid").data("kendoGrid");
            grid.bind("dataBinding", grid_dataBinding);
            grid.dataSource.fetch();
        }

        function grid_dataBinding(e) {
            console.log("dataBinding");
        }

        //var grid = $("#grid");

        //function selectDeselect(e) {
        //    debugger;
        //    var kendoGrid = e.data.grid.data("kendoGrid"),
        //        items = [];
        //    // if we modify the data in the loop, then it blocks on the current element...
        //    // so I store the items and the modify them in another loop
        //    e.data.grid.find("tr[role='row']").each(function () {
        //        items.push(e.data.grid.data("kendoGrid").dataItem(this));
        //    });
        //    for (var i = 0; i < items.length; i++)
        //        items[i].set("IsRepSync", e.data.setTo ? 1 : 0);
        //}

        //function selectDeselectAlt(e) {
        //    // alternatively, to avoid the limitations of dataItem(), I retrieve the data from the data source
        //    e.data.grid.find("tr[role='row']").each(function () {
        //        var uid = $(this).attr("data-uid"),
        //            item = e.data.dataSource.getByUid(uid);
        //        item.set("IsRepSync", e.data.setTo ? 1 : 0);
        //    });
        //}

        //grid.on("click", ".k-grid-all", { grid: grid, dataSource: sharedDataSource, setTo: true }, selectDeselect);
        //grid.on("click", ".k-grid-none", { grid: grid, dataSource: sharedDataSource, setTo: false }, selectDeselectAlt);

        var SyncDevices = [];

        $('.check_row_srsync').live('click', function (e) {
            clearInterval(interval);

            var checked = $(this).is(':checked');
            var grid = $('#grid').data().kendoGrid;
            var dataItem = grid.dataItem($(this).closest('tr'));
            dataItem.set("IsRepSync", checked);
        });

        $(".checkAll_row_srsync").live('click', function (e) {
            clearInterval(interval);

            var cb = $(this);
            var checked = cb.is(':checked');

            var grid = $('#grid').data('kendoGrid');
            var rows = grid.tbody[0].rows;

            for (var rowIndex in rows) {
                if (rowIndex === "length") {
                    break;
                }

                var dataItem = grid.dataItem(rows[rowIndex]);
                dataItem.set("IsRepSync", checked);
            }
        });

        //function GetSelectedDevices() {
        //    debugger;
        //    var callCycleContactDTOList = [];
        //    //var idstr = '';
        //    var items = $("#grid").data("kendoGrid").dataSource.data();
        //    for (var i = 0; i < items.length; i++) {
        //        var item = items[i];
        //        if (i == (items.length - 1) && (item != undefined && item != null && item != '' && item.IsRepSync == true)) {
        //            //idstr += item.ProductId;
        //            callCycleContactDTOList.push(item.DeviceToken);
        //        }
        //        else if (item != undefined && item != null && item != '' && item.IsRepSync == true) {
        //            //idstr += item.ProductId + ",";
        //            callCycleContactDTOList.push(item.DeviceToken);
        //        }
        //    }

        //    alert(JSON.stringify(callCycleContactDTOList));

        //    //debugger; 
        //    return callCycleContactDTOList;
        //}

    </script>
</asp:Content>
