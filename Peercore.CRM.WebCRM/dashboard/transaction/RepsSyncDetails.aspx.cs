﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.IO;

public partial class dashboard_transaction_RepsSyncDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.UserName;
        OriginatorTypeString.Value = UserSession.Instance.OriginatorString;

        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("Reps Sync Details ", "#", "");
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        rptDoc.Load(Server.MapPath("../reports/cryRepsSyncDetails.rpt"));

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }
    protected void btnViewNotifyIsSync_Click(object sender, EventArgs e)
    {
        try
        {
            ReportDocument rptDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("RepSyncDetails", Session.SessionID.ToString())) + ".pdf", GenerateFileName("RepSyncDetails", Session.SessionID.ToString()) + ".pdf", rptDoc);
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnExportNotifyIsSync_Click(object sender, EventArgs e)
    {
        try
        {
            ReportDocument rptDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("RepSyncDetails", Session.SessionID.ToString())) + ".xls", GenerateFileName("RepSyncDetails", Session.SessionID.ToString()) + ".xls", rptDoc);
        }
        catch (Exception ex)
        {
        }
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.Excel, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        FileInfo file = new FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    protected void btnViewSyncReport_Click(object sender, EventArgs e)
    {
        try
        {
            ReportDocument rptDoc = GetReportDocumentNew();
            SavePDF(Server.MapPath(GenerateFileName("SRSyncReport", Session.SessionID.ToString())) + ".pdf", GenerateFileName("SRSyncReport", Session.SessionID.ToString()) + ".pdf", rptDoc);
        }
        catch (Exception ex)
        {
        }
    }

    private ReportDocument GetReportDocumentNew()
    {
        ReportDocument rptDoc = new ReportDocument();

        rptDoc.Load(Server.MapPath("../reports/cryRepsSyncDetailsNew.rpt"));

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }
}