﻿using CRMServiceReference;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.UI;

public partial class dashboard_transaction_UploadSummary : System.Web.UI.Page
{
    private ReportDocument repDoc = null;

    #region Load Drop Downs

    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
            if (aseList.Count > 0)
                aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string selectedASE = buttonbar1.GetAsesValue();

        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetDistributersByASMOriginator(args);
        if (distList.Count > 0)
            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Upload Summary", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        //buttonbar1.onDropDownListReps = new usercontrols_buttonbar_reports.DropDownListReps(DropDownListReps_SelectionChanged);
        //buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributors_SelectionChanged);
        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        //buttonbar1.onDropDownListShowReportFor = new usercontrols_buttonbar_reports.DropDownListShowReportFor(DropDownListShowReportFor_SelectionChanged);

        //buttonbar1.o = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        buttonbar1.onDropDownListQuantityType = new usercontrols_buttonbar_reports.DropDownListQuantityType(DropDownListQuantityType_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(14);
            LoadOptions();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument_Excel();
            SaveExcel(Server.MapPath(GenerateFileName("UploadSummary", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("UploadSummary", Session.SessionID.ToString()) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("UploadSummary", Session.SessionID.ToString())) + ".pdf", GenerateFileName("UploadSummary", Session.SessionID.ToString()) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();
        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        FileInfo file = new FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (buttonbar1.GetReportTypeUploadSummary() == "su")
        {
            rptDoc.Load(Server.MapPath("../reports/UploadStockSummary.rpt"));
        }
        else
        {
            rptDoc.Load(Server.MapPath("../reports/UploadTargetSummary.rpt"));
        }

        rptDoc.SetParameterValue("@StartDate", GetFromDate());
        rptDoc.SetParameterValue("@EndDate", GetToDate());
        rptDoc.SetParameterValue("@ASE", buttonbar1.GetAsesValue());
        rptDoc.SetParameterValue("@Dist", buttonbar1.GetDistributorsValue());
        string strQuantityType = buttonbar1.GetQuantityType();
        rptDoc.SetParameterValue("@QuantityType", strQuantityType); //Added by Rangan

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }

    private ReportDocument GetReportDocument_Excel()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (buttonbar1.GetReportTypeUploadSummary() == "su")
            rptDoc.Load(Server.MapPath("../reports/UploadStockSummary.rpt"));
        else
            rptDoc.Load(Server.MapPath("../reports/UploadTargetSummary.rpt"));

        rptDoc.SetParameterValue("@StartDate", GetFromDate());
        rptDoc.SetParameterValue("@EndDate", GetToDate());
        rptDoc.SetParameterValue("@ASE", buttonbar1.GetAsesValue());
        rptDoc.SetParameterValue("@Dist", buttonbar1.GetDistributorsValue());

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = DateTime.Parse(buttonbar1.GetToDateText()).AddDays(1);
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private void LoadOptions()
    {
        //Laod ASE's     
        LoadASEs();

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleASEDropdownVisible(false);
        }

        LoadDistributors();

        if (UserSession.Instance.OriginatorString == "DIST")
        {
            buttonbar1.SetAsesSelect("ALL");
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();

            buttonbar1.SetDistributorsSelect(UserSession.Instance.UserName);
            buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();

            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleDistributerDropdownEnable(false);
            buttonbar1.HandleTradeloadRepTypeDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleDistributerDropdownVisible(false);
            buttonbar1.HandleTradeloadRepTypeDropdownVisible(false);
        }
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        LoadDistributors();
    }



    protected void DropDownListQuantityType_SelectionChanged(object sender)
    {

    }
}