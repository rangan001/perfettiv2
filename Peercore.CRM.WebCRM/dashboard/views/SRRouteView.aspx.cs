﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dashboard_views_SRRouteView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        hfOriginator.Value = UserSession.Instance.UserName;
        hfOriginatorType.Value = UserSession.Instance.OriginatorString;

        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("Reps Geo-Location Map ", "#", "");

        if (!IsPostBack)
        {
            LoadSalesReps();
        }
    }

    protected void LoadSalesReps()
    {
        if (UserSession.Instance.OriginatorString.Equals("ASE") || UserSession.Instance.OriginatorString.Equals("ADMIN") || UserSession.Instance.OriginatorString.Equals("SLC"))
        {
            List<KeyValuePair<string, string>> repLines = new List<KeyValuePair<string, string>>();
            repLines = GetRepsForDropdown();
            foreach (KeyValuePair<string, string> ValuePair in repLines)
            {
                ddlRepList.Items.Add(new ListItem(ValuePair.Value.ToString(), ValuePair.Key.ToString()));
            }
            ddlRepList.SelectedIndex = 0;
            if (repLines.Count > 0)
            {
                ArgsDTO args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                args.RepCode = ddlRepList.SelectedValue;
            }
        }
        else
        {
            ddlRepList.Items.Add(new ListItem(UserSession.Instance.Originator, UserSession.Instance.UserName));
        }
    }

    private List<KeyValuePair<string, string>> GetRepsForDropdown()
    {
        List<KeyValuePair<string, string>> repLines = new List<KeyValuePair<string, string>>();
        List<OriginatorDTO> originatorList = new List<OriginatorDTO>();
        OriginatorClient originatorClient = new OriginatorClient();

        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.Originator = UserSession.Instance.UserName;
        args.OriginatorType = UserSession.Instance.OriginatorString;
        args.AdditionalParams = " dept_string = 'DR'";
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 10000;// ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        originatorList = originatorClient.GetRepsByOriginator_For_Route_Assign(args);

        if (originatorList != null)
        {
            foreach (OriginatorDTO reps in originatorList)
            {
                repLines.Add(new KeyValuePair<string, string>(reps.RepCode, reps.Name));
            }
        }

        return repLines;
    }
}