﻿<%@ Page Title="mSales - Discount Promotions" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="discount_scheme_entry.aspx.cs" Inherits="discount_scheme_transaction_discount_scheme_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="HiddenFieldSchemeHeaderId" runat="server" />
    <asp:HiddenField ID="HiddenFieldIsActive" runat="server" Value="1" />
    <div id="schememodalWindow" style="display: none">
        <div id="div_schemeconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/default.aspx">
                            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
            <div>
                <a class="search_toggle" id="btnOpenSearchPanel" clientidmode="Static" runat="server" href="#">Scheme Search</a>
            </div>

        <div>
            <div class="addlinkdiv" style="float: left; margin-bottom: 10px; display: none">
                <a id="btnAddNewScheme" clientidmode="Static" runat="server" href="#">Add Scheme</a>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_schemeSearchPanel" style="display: none">
            <%--layout new format--%>
            <div class="formleft">
                <div class="formtextdiv">
                    Start Date</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    End Date</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                    <%--<input type="button" id="btn_filter" value="Search" class="searchbtn1" />--%>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    &nbsp;</div>
                <div class="formdetaildiv_right">
                    <asp:RadioButton ID="rdbActive" GroupName="grpActiveInactive" runat="server" Text=" Active "
                        CssClass="radibutton_width" />
                    <asp:RadioButton ID="rdbInactive" GroupName="grpActiveInactive" runat="server" Text=" Inactive "
                        CssClass="radibutton_width" />
                </div>
                <div class="clearall">
                </div>
                 <div class="formtextdiv">
                    &nbsp;</div>
                    <div class="formdetaildiv_right">
                     <input type="button" id="btn_filter" value="Search" class="searchbtn1" />
                    </div>
            </div>
        </div>
        <div class="clearall">
        </div>

        <div id="div_schemeEntryPanel" runat="server" style="display: none">
            <%--layout new format--%>
            <div class="formleft">
                <div class="formtextdiv">
                    Scheme name</div>
                <div class="formdetaildiv_right">
                    : <span class="wosub mand">
                        <asp:TextBox ID="txtSchemeName" runat="server" CssClass="input-large1" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="Label2" runat="server" Text="*" Font-Bold="True" Font-Size="Large"
                            ForeColor="#FF3300"></asp:Label>
                    </span>
                </div>
                <%--<div class="clearall">
                </div>
                <div class="formtextdiv">
                    Territory</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:DropDownList ID="DropDownListDivision" runat="server" CssClass="input-large1">
                    </asp:DropDownList>
                </div>--%>
                <div class="clearall">
                </div>
            </div>
            <div class="formright">
                <div class="formtextdiv">
                    Start Date</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtStartDateEntry" runat="server"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    End Date</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtEndDateEntry" runat="server"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Active</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:CheckBox ID="chkActive" runat="server"></asp:CheckBox>
                </div>
                <div class="clearall">
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>

        <div class="grid_container">
            <div id="divSchemeHeaderGrid">
            </div>
        </div>
        
    </div>
    <a id="a_show_pdf" target="_blank" style="display:none"></a>

    <script type="text/javascript">
        // hideStatusDiv("MainContent_div_message");
        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
            //$("#div_schemeEntryForm").css("display", "none");

            var fromDate = $("#MainContent_txtStartDate").val();
            var toDate = $("#MainContent_txtEndDate").val();
            var isInactive = parseInt($("#MainContent_HiddenFieldIsActive").val());
            loadDiscountSchemeHeaders(fromDate, toDate, isInactive);

            $("#MainContent_txtStartDate").kendoDatePicker({
                //value: new Date(),
                format: "dd-MMM-yyyy"
                //change: startDate_onChange
            });

            $("#MainContent_txtEndDate").kendoDatePicker({
                //value: new Date(),
                format: "dd-MMM-yyyy"
                //change: startDate_onChange
            });

            $("#MainContent_txtStartDateEntry").kendoDatePicker({
                //value: new Date(),
                format: "dd-MMM-yyyy"
                //change: startDate_onChange
            });

            $("#MainContent_txtEndDateEntry").kendoDatePicker({
                //value: new Date(),
                format: "dd-MMM-yyyy"
                //change: startDate_onChange
            });
        });

        $("#btn_filter").click(function () {

            var fromDate = $("#MainContent_txtStartDate").val();
            var toDate = $("#MainContent_txtEndDate").val();
            var isInactive = parseInt($("#MainContent_HiddenFieldIsActive").val());
            loadDiscountSchemeHeaders(fromDate, toDate, isInactive);
        });

        $("#MainContent_buttonbar_buttinSave").click(function () {
            jQuery(function () {
                jQuery("#MainContent_txtSchemeName").validate({
                    expression: "if (VAL) return true; else return false;",
                    message: "Please Enter Name"
                });
            });
        });

        $("#btnOpenSearchPanel").click(function () {
            if (($("#div_schemeSearchPanel").css('display') == 'none')) {
                $("#div_schemeSearchPanel").css("display", "block");
            }
            else {
                $("#div_schemeSearchPanel").css("display", "none");
            }
        });

        $("#btnAddNewScheme").click(function () {
            if (($("#MainContent_div_schemeEntryPanel").css('display') == 'none')) {
                //$("#div_schemeSearchPanel").css("display", "none");
                $("#MainContent_HiddenFieldSchemeHeaderId").val(0);
                $("#MainContent_txtSchemeName").val('');
                //$("select#MainContent_DropDownListDivision").prop('selectedIndex', 0);
                $("#MainContent_txtStartDateEntry").val((new Date()).format("dd-MMM-yyyy"));
                $("#MainContent_txtEndDateEntry").val((new Date()).format("dd-MMM-yyyy"));
                $("#MainContent_div_schemeEntryPanel").css("display", "block");
                $("#MainContent_txtSchemeName").focus();
            }
            else {
                if ($("#MainContent_txtSchemeName").val() == '') {
                    $("#MainContent_div_schemeEntryPanel").css("display", "none");
                }
                else {
                    //$("#div_schemeSearchPanel").css("display", "none");
                    $("#MainContent_HiddenFieldSchemeHeaderId").val(0);
                    $("#MainContent_txtSchemeName").val('');
                    //$("select#MainContent_DropDownListDivision").prop('selectedIndex', 0);
                    $("#MainContent_txtStartDateEntry").val((new Date()).format("dd-MMM-yyyy"));
                    $("#MainContent_txtEndDateEntry").val((new Date()).format("dd-MMM-yyyy"));
                    $("#MainContent_txtSchemeName").focus();
                }
            }
        });

        $('#MainContent_rdbActive').change(function () {
            if ($(this).is(":checked")) {
                $("#MainContent_HiddenFieldIsActive").val('1');
            }
        });

        $('#MainContent_rdbInactive').change(function () {
            if ($(this).is(":checked")) {
                $("#MainContent_HiddenFieldIsActive").val('0');
            }
        });

        function showActiveInactiveConfirmation(schemeHeaderId, schemeName, isActive) {
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "";
            if (isActive == "true") {
                message = "Are you sure, You want to save selected Discount Scheme As Inactive ?";
                isActive = "false";
            } else {
                message = "Are you sure, You want to save selected Discount Scheme As Active ?";
                isActive = "true";
            }
            var wnd = $("#schememodalWindow").kendoWindow({
                title: "Change Active/Inactive",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_schemeconfirm_message").text(message);
            wnd.center().open();

            $("#yes").click(function () {
                UpdateSchemeActiveInactive(schemeHeaderId, isActive);
                wnd.close();            
            });

            $("#no").click(function () {
                wnd.close();
                var fromDate = $("#MainContent_txtStartDate").val();
                var toDate = $("#MainContent_txtEndDate").val();
                var isInactive = parseInt($("#MainContent_HiddenFieldIsActive").val());
                loadDiscountSchemeHeaders(fromDate, toDate, isInactive);
            });
        }

        function UpdateSchemeActiveInactive(schemeHeaderId, isActive) {
            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=CheckSchemeActiveInactive&type=Update&schemeHeaderId=" + schemeHeaderId + "&isActive=" + isActive;
            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Saved.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        hideStatusDiv("MainContent_div_message");

                        var fromDate = $("#MainContent_txtStartDate").val();
                        var toDate = $("#MainContent_txtEndDate").val();
                        var isInactive = parseInt($("#MainContent_HiddenFieldIsActive").val());
                        loadDiscountSchemeHeaders(fromDate, toDate, isInactive);
                    } else {
                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });
            //alert(schemeHeaderId.toString());
        }

        /*Load Scheme Header Grid in Discount Scheme Entry Page*/
        function loadDiscountSchemeHeaders(fromDate, toDate, isActive) {
            var take_grid = $("#MainContent_hfPageIndex").val();
            var addOptionsUrl = ROOT_PATH + "discount_scheme/transaction/discount_option_entry.aspx?id=#=SchemeHeaderId#&type=a";
            var editOptionsUrl = ROOT_PATH + "discount_scheme/transaction/discount_option_entry.aspx?id=#=SchemeHeaderId#&type=e";
            var url = "javascript:updateSchemeHeader('#=SchemeHeaderId#','#=SchemeName#','#=IsActive#','#=StartDate#','#=EndDate#','#=DivisionId#','#=DivisionName#');";
            var activeInactiveUrl = "javascript:showActiveInactiveConfirmation('#=SchemeHeaderId#','#=SchemeName#','#=IsActive#');";
            
            var urlName = "<a href=\"" + url + "\">#=SchemeName#</a>";
            var activeInactive = "<a href=\"" + activeInactiveUrl + "\">Activate</a>";
            if (isActive == 1) {
                activeInactive = "<a href=\"" + activeInactiveUrl + "\">Deactivate</a>";
            }
            
            $("#div_loader").show();
            $("#divSchemeHeaderGrid").html("");
            $("#divSchemeHeaderGrid").kendoGrid({
                height: 300,
                columns: [
                        { field: "SchemeHeaderId", title: "SchemeHeaderId", width: "85px", hidden: true },
                        { template: "<a href=\"" + addOptionsUrl + "\">Add Options</a>", field: "", width: "75px", hidden: false },
                        //{ template: "<a href=\"" + editOptionsUrl + "\">Edit Options</a>", field: "", width: "75px", hidden: false },
                        { template: urlName, field: "SchemeName", title: "Scheme Name", width: "250px" },
                        { field: "IsActive", title: "IsActive", width: "85px", hidden: true },
                        { field: "StartDate", title: "Start Date", width: "100px", format: DATE_FORMAT },
                        { field: "EndDate", title: "End Date", width: "100px", format: DATE_FORMAT },
                        //{ field: "DivisionId", title: "DivisionId", width: "85px", hidden: true },
                        //{ field: "DivisionName", title: "Division", width: "250px" },
                        { template: '<a href="javascript:PrintDiscountScheme(#=SchemeHeaderId#)" >Print</a>', width: "75px" },
                        { template: activeInactive, field: "", width: "75px", hidden: false }
                    ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                SchemeHeaderId: { validation: { required: true} },
                                SchemeName: { editable: false, nullable: true },
                                IsActive: { editable: false, nullable: true },
                                StartDate: { type: "date", editable: false, nullable: true },
                                EndDate: { type: "date", editable: false, nullable: true },
                                //DivisionId: { editable: false, nullable: true },
                                //DivisionName: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetSchemeHeaderDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { fromDate: fromDate, toDate: toDate, isInactive: isActive, pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#divSchemeHeaderGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        /*Update Brand in Brand Entry Page*/
        function updateSchemeHeader(SchemeHeaderId, SchemeName, IsActive, StartDate, EndDate, DivisionId, DivisionName) {
            //$("#div_schemeSearchPanel").css("display", "none");
            $("#MainContent_HiddenFieldSchemeHeaderId").val(SchemeHeaderId);
            $("#MainContent_txtSchemeName").val(SchemeName);
            //$("#MainContent_DropDownListDivision").val(parseInt(DivisionId));
            //$("select#MainContent_DropDownListDivision").prop('selectedValue', 2);
            if (IsActive == "true") {
                $("#MainContent_chkActive").prop('checked', true);
            } else {
                $("#MainContent_chkActive").prop('checked', false);
            }
            $("#MainContent_txtStartDateEntry").val((new Date(StartDate)).format("dd-MMM-yyyy"));
            $("#MainContent_txtEndDateEntry").val((new Date(EndDate)).format("dd-MMM-yyyy"));
            $("#MainContent_div_schemeEntryPanel").css("display", "block");
            $("#MainContent_txtSchemeName").focus();
        }

    </script>
</asp:Content>
