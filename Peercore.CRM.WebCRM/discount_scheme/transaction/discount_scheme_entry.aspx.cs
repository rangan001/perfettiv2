﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Entities;
//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;
using Telerik.Web.UI;
using System.Drawing;
using System.Data;

public partial class discount_scheme_transaction_discount_scheme_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.EnableSave(true);
            buttonbar.VisibleSave(true);

            buttonbar.onButtonAddNew = new usercontrols_buttonbar.ButtonAddNew(ButtonAddNew_Click);
            buttonbar.VisibleAddNew(true);
            
            if (!IsPostBack)
            {
                
            }
            
            LoadDivisions();

            DateTime today = DateTime.Today;
            txtStartDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
            txtEndDate.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");

            rdbActive.Checked = true;

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Discount Scheme ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void LoadDivisions()
    {
        ////ArgsDTO args = new ArgsDTO();
        ////DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        ////args.OrderBy = " division_name asc ";
        ////args.AdditionalParams = "";
        ////args.StartIndex = 1;
        ////args.RowCount = 100;

        ////List<DivisionDTO> divisionList = discountSchemeClient.GetAllDivisions(args);

        ////DropDownListDivision.DataSource = divisionList;
        ////DropDownListDivision.DataTextField = "DivisionName";
        ////DropDownListDivision.DataValueField = "DivisionId";
        ////DropDownListDivision.DataBind();

        //MasterClient masterlient = new MasterClient();
        //ArgsModel args1 = new ArgsModel();
        //args1.StartIndex = 1;
        //args1.RowCount = 10000;
        //args1.OrderBy = " area_id asc";

        //List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        //objTrrList = masterlient.GetAllTerritoryByOriginator(args1, UserSession.Instance.OriginatorString,
        //                                                                        UserSession.Instance.UserName);

        //if (objTrrList.Count != 0)
        //{
        //    DropDownListDivision.Items.Clear();
        //    DropDownListDivision.Items.Add(new ListItem("- Select Territory -", "0"));
        //    foreach (TerritoryModel item in objTrrList)
        //    {
        //        var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
        //        string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

        //        DropDownListDivision.Items.Add(new ListItem(result + "  |  " + item.TerritoryName, item.TerritoryId.ToString()));
        //    }

        //    DropDownListDivision.Enabled = true;
        //}

    }

    protected void ButtonSave_Click(object sender)
    {
        SaveSchemeHeader();
    }

    protected void ButtonAddNew_Click(object sender)
    {
        div_schemeEntryPanel.Attributes.Add("style", "display:block");
        HiddenFieldSchemeHeaderId.Value = "0";
        txtSchemeName.Text = "";
        //DropDownListDivision.SelectedIndex = 0;
        DateTime today = DateTime.Today;
        chkActive.Checked = true;
        txtStartDateEntry.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
        txtEndDateEntry.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");
        txtSchemeName.Focus();        
    }

    public void SaveSchemeHeader()
    {
        CommonUtility commonUtility = new CommonUtility();
        SchemeHeaderDTO schemeHeaderDTO = new SchemeHeaderDTO();
        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        try
        {
            if (txtSchemeName.Text.Trim() != "")
            {
                schemeHeaderDTO.SchemeHeaderId = Convert.ToInt32(HiddenFieldSchemeHeaderId.Value) > 0 ? Convert.ToInt32(HiddenFieldSchemeHeaderId.Value) : 0;
                schemeHeaderDTO.SchemeName = txtSchemeName.Text;
                schemeHeaderDTO.DivisionId = 0;//Convert.ToInt32(DropDownListDivision.SelectedValue);
                //schemeHeaderDTO.DivisionName = DropDownListDivision.SelectedItem.Text;
                schemeHeaderDTO.StartDate = Convert.ToDateTime(txtStartDateEntry.Text);
                schemeHeaderDTO.EndDate = Convert.ToDateTime(txtEndDateEntry.Text);

                schemeHeaderDTO.IsActive = chkActive.Checked;

                schemeHeaderDTO.CreatedBy = UserSession.Instance.UserName;
                schemeHeaderDTO.LastModifiedBy = UserSession.Instance.UserName;

                bool isSuccess = false;

                if (IsSchemeHeaderNameExists(schemeHeaderDTO))
                {
                    div_message.Attributes.Add("style", "display:block;padding:2px");
                    div_message.InnerHtml = commonUtility.GetErrorMessage("Scheme Name already exists.");
                    div_schemeEntryPanel.Attributes.Add("style", "display:block");
                    txtSchemeName.Attributes.Add("onfocus", "this.select();");
                    txtSchemeName.Focus();
                    return;
                }

                int schemeHeaderId;
                CommonClient cClient = new CommonClient();
                if (schemeHeaderDTO.SchemeHeaderId > 0)
                {
                    isSuccess = discountSchemeClient.UpdateSchemeHeader(schemeHeaderDTO);
                    schemeHeaderId = schemeHeaderDTO.SchemeHeaderId;

                    try
                    {
                        if (isSuccess && schemeHeaderId > 0)
                        {
                            cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            TransactionTypeModules.Update,
                            TransactionModules.DiscountScheme,
                            UserSession.Instance.OriginalUserName + " update discount scheme " + schemeHeaderDTO.SchemeName);
                        }
                    }
                    catch { }
                }
                else
                {
                    isSuccess = discountSchemeClient.InsertSchemeHeader(schemeHeaderDTO, out schemeHeaderId);

                    try
                    {
                        if (isSuccess && schemeHeaderId > 0)
                        {
                            cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            TransactionTypeModules.Update,
                            TransactionModules.DiscountScheme,
                            UserSession.Instance.OriginalUserName + " update discount scheme " + schemeHeaderDTO.SchemeName);
                        }
                    }
                    catch { }
                }

                if (isSuccess && schemeHeaderId > 0)
                {
                    div_message.Attributes.Add("style", "display:block;padding:4px");
                    div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
                }
                else
                {
                    div_message.Attributes.Add("style", "display:block;padding:4px");
                    div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
                }
            }
            else
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Scheme Name cannot be empty.");
                return;
            }
        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private bool IsSchemeHeaderNameExists(SchemeHeaderDTO schemeHeaderDTO)
    {
        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        try
        {
            return discountSchemeClient.IsSchemeHeaderNameExists(schemeHeaderDTO);
        }
        catch (Exception)
        {
            return true;
        }
    }
}