﻿<%@ Page Title="mSales - Discount Promotions" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="option_level_entry.aspx.cs" Inherits="discount_scheme_transaction_option_level_entry" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
 <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <%--<ucl:buttonbar ID="buttonbar" runat="server" />--%>
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=Customer">
            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        
        <div>
            <div>
            <%--<a id="a_add_scheme">Add Scheme</a> --%>
            <button id="a_add_scheme" class="k-button">
            Add</button>
            </div>
            <div class="formleft">
                <div class="formtextdiv">
                    Scheme Details</div>
                <div class="formdetaildiv_right">
                    :
                    <span class="wosub mand">
                    <asp:TextBox ID="txtSchemeDetails" runat="server" ClientIDMode="Static"></asp:TextBox>
                    <span style="color: Red">*</span>
                    </span>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Scheme Type</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:DropDownList ID="DropDownListSchemeType" runat="server" CssClass="input-large1">
                        
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>
                <div id="div_Percentage" style="display: none">
                    <div class="formtextdiv">
                        Discount Percentage</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtPercentage" runat="server" ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                    </div>
                </div>
                <div class="clearall">
                </div>

                <div id="div_value">
                    <div class="formtextdiv">
                        Discount Value</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtDiscountValue" runat="server" ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                    </div>
                </div>
                <div class="clearall">
                </div>
                
            </div>
            <div class="formright">
                
                <div class="formtextdiv">
                    Is Retailer</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:CheckBox ID="chkIsRetailer" runat="server"  ClientIDMode="Static"/>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Detail Type</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:DropDownList ID="DropDownListDetailType" runat="server" CssClass="input-large1">
                        
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>

           </div>
           <div class="clearall">
                </div>
           <div id="div_free_product" style="display: none">
                <a id="a_product">Add Free Product</a> 
                <div class="clearall">
                </div>
                
                <div id="dis_product"></div>
           </div>
        </div>
        <div>
            <div id="div_option_level">
                <a id="a_option_level">Add Product Option</a> 
                <div class="clearall">
                </div>

                <div id="grid_option_level"></div>
           </div>
        </div>

        <div>
            <div id="div_combination"  style="display: none">
                <a id="a_combination">Add Combination</a> 
                <div class="clearall">
                </div>
                <div id="grid_combination"></div>
           </div>
           <asp:HiddenField ID="HiddenField_combination_indexId" runat="server" ClientIDMode="Static" Value ="0"/>
        </div>

    </div>
    <div id="div_free_product_form" style="display: none">
        <asp:HiddenField ID="HiddenField_ProductId" runat="server" />
        <div>
            <div class="formleft">
                <div class="formtextdiv">
                    Product</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtProduct" runat="server" ReadOnly="true" ></asp:TextBox>
                    <div style="line-height:30px; margin-top:3px;">
                <a href="#" id="id_schedule">
                        <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Quantity</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtQty" runat="server" CssClass="validate-numbers"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
            </div>
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Add</button>
        <button id="no" class="k-button">
            No</button>

            
    </div>

    <div id="div_option_level_form" style="display: none">
        <asp:HiddenField ID="HiddenField_BrandId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HiddenField_PackingTypeId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HiddenField_CatlogId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HiddenField_ItemGroupId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HiddenField_IndexId" runat="server" ClientIDMode="Static" Value ="0"/>
        <div id="div_typebill">
            <div class="formleft">
                <div class="formtextdiv">
                    Is Bill</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:CheckBox ID="chkIsBill" runat="server"  ClientIDMode="Static"/>
                </div>
                <div class="clearall">
                </div>
            </div>
        </div>
        <div class="clearall"></div>
        <div id="div_bill" style="display: none">
            <div class="formleft">
                <div class="formtextdiv">
                    Operator</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:DropDownList ID="DropDownListOperator" runat="server" ClientIDMode="Static">
                            <asp:ListItem Value="1" Text="greater than"></asp:ListItem>
                            <asp:ListItem Value="2" Text="greater than or equal"></asp:ListItem>
                            <asp:ListItem Value="3" Text="less than"></asp:ListItem>
                            <asp:ListItem Value="4" Text="less than or equal"></asp:ListItem>
                            <asp:ListItem Value="5" Text="equal"></asp:ListItem>
                            <asp:ListItem Value="6" Text="between"></asp:ListItem>
                        </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Min Value</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtMinBValue" runat="server"  ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                 </div>
                 <div class="clearall">
                </div>
            </div>
            <div class="formright" id="div_between" style="display: none">
                <div class="formtextdiv">
                   </div>
                <div class="formdetaildiv_right">
                
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Max Value</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtMaxBValue" runat="server"  ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
           </div>
        
        </div>
        <div class="clearall"></div>
        <div id="div_qty_limit">
            <div>
                <div class="formleft">
                    <div class="formtextdiv">
                        Brand</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtBrand" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_brand">
                            <img id="img3" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Packing Type</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtPackingType" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_PackingType">
                            <img id="img5" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>
                    </div>
                    <div class="clearall">
                    </div>

                    <div class="formtextdiv">
                        HVP</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:CheckBox ID="chbIsHVP" runat="server"  ClientIDMode="Static"/>
                    </div>
                    <div class="clearall">
                    </div>

                    <div id="div_min_val">

                    <div class="formtextdiv">
                       Min Quantity</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtMinQuantity" runat="server" ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>

                    <div class="formtextdiv">
                        Min Value</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtMinValue" runat="server" ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>
                  </div>
                </div>
                <div class="formright">
                    <div class="formtextdiv">
                        Item Group</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtItemGroup" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_ItemGroup">
                            <img id="img4" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>
                    </div>
                    <div class="clearall">
                    </div>

                    <div class="formtextdiv">
                        Catlog</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtCatlog" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_Catlog">
                            <img id="img6" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>
                    </div>
                    <div class="clearall">
                    </div>

                    <div id="div_isexcept">
                    <div class="formtextdiv">
                        Is Except</div>
                    <div class="formdetaildiv_right">
                        <asp:CheckBox ID="chkIsExcept" runat="server"  ClientIDMode="Static"/>
                    </div>
                    <div class="clearall">
                    </div>
                    </div>


                    <div id="div_max_val">
                    <div class="formtextdiv">
                        Max Quantity</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtMaxQuantity" runat="server"  ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>

                    <div class="formtextdiv">
                        Max Value</div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtMaxValue" runat="server"  ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>
                   </div>
               </div>           
            </div>
        </div>     
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="btnOptionAdd" class="k-button">
            Add</button>
        <button id="btnCombinationAdd" class="k-button">
            Add</button>
        <button id="btnOptionNo" class="k-button">
            No</button>
        
            

            
    </div>

    <div id= "program_window">
        <div id="Product_subwindow"></div>
    </div>
    <script type="text/javascript">
        jQuery(function ($) {
            $('input.validate-numbers').autoNumeric({ mDec: 0 });
            jQuery("#txtSchemeDetails").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter Scheme Details"
            });
        });
        


        $("#a_combination").click(function () {
            AddCombination();
        });
        function RemoveCombinationFromSession(indexId) {

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=removecombination&type=query&cid=" + indexId + "&oid=" + $("#HiddenField_IndexId").val();

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        var grid = $("#grid_combination").data("kendoGrid");
                        grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }
        //        function EditOptionLevel(ProductId, Qty, Product) {
        function EditCombination(id, brand, packt, cat, itemg, brandid, packtid, catid, itemgid, ishvp, isexc) {

            $("#HiddenField_combination_indexId").val(id);
            $("#txtBrand").val(brand);
            $("#txtPackingType").val(packt);
            $("#txtCatlog").val(cat);
            $("#txtItemGroup").val(itemg);
            $("#HiddenField_BrandId").val(brandid);
            $("#HiddenField_PackingTypeId").val(packtid);
            $("#HiddenField_CatlogId").val(catid);
            $("#HiddenField_ItemGroupId").val(itemgid);

            $("#div_qty_limit").css("display", "block");
            $("#div_bill").css("display", "none");
            $("#div_typebill").css("display", "none");
            $("#div_between").css("display", "none");
            $('#chkIsBill').prop('checked', false);

            $('#chbIsHVP').prop('checked', ishvp);
            $('#chkIsExcept').prop('checked', isexc); 
            
            $("#btnOptionAdd").css("display", "none");
            $("#btnCombinationAdd").css("display", "inline-block");
            $("#div_min_val").css("display", "none");
            $("#div_max_val").css("display", "none");
            $("#div_isexcept").css("display", "block");
            
            wnd.center().open();
        }

        function AddCombination() {
            $("#HiddenField_combination_indexId").val("0");
            $("#txtBrand").val("Any");
            $("#txtPackingType").val("Any");
            $("#txtCatlog").val("Any");
            $("#txtItemGroup").val("Any");
            $("#HiddenField_BrandId").val("0");
            $("#HiddenField_PackingTypeId").val("0");
            $("#HiddenField_CatlogId").val("0");
            $("#HiddenField_ItemGroupId").val("0");
            $("#txtMinQuantity").val("0");
            $("#txtMaxQuantity").val("0");
            $("#txtMinValue").val("0");
            $("#txtMaxValue").val("0");
            $("#DropDownListOperator").val(0);

            $("#div_qty_limit").css("display", "block");
            $("#div_bill").css("display", "none");
            $("#div_typebill").css("display", "none");
            $("#div_between").css("display", "none");

            $("#div_min_val").css("display", "none");
            $("#div_max_val").css("display", "none");
            $("#div_isexcept").css("display", "block");

            $('#chkIsBill').prop('checked', false);
            $('#chbIsHVP').prop('checked', false);
            $('#chkIsExcept').prop('checked', false); 

            $("#btnOptionAdd").css("display", "none");
            $("#btnCombinationAdd").css("display", "inline-block");

            wnd.center().open();
        }
       
       

        function AddCombinationToSession() {
            var ishvp = 0;
            var isexc = 0;

            if ($('#chbIsHVP').is(":checked")) {
                ishvp = 1;
            }
            if ($('#chkIsExcept').is(":checked")) {
                isexc = 1;
            }

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=addcombination&type=query&oid=" + $("#HiddenField_IndexId").val()
                         + "&cid=" + $("#HiddenField_combination_indexId").val()
                         + "&brand=" + $("#txtBrand").val()
                         + "&packt=" + $("#txtPackingType").val()
                         + "&cat=" + $("#txtCatlog").val()
                         + "&itemg=" + $("#txtItemGroup").val()
                         + "&brandid=" + $("#HiddenField_BrandId").val()
                         + "&packtid=" + $("#HiddenField_PackingTypeId").val()
                         + "&catid=" + $("#HiddenField_CatlogId").val()
                         + "&itemgid=" + $("#HiddenField_ItemGroupId").val()
                         + "&isexc=" + isexc
                         + "&ishvp=" + ishvp;


            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        var grid = $("#grid_combination").data("kendoGrid");
                        grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }

        function GetCombinationList(indexId) {

            // var take_grid = $("#MainContent_hfPageIndex").val();
            $("#grid_combination").html("");
            $("#grid_combination").kendoGrid({
                // height: 400,
                columns: [
                { template: '<a href="javascript:RemoveCombinationFromSession(#=IndexId#)">Delete</a>',
                    width: "50px"
                },
                { template: '<a href="javascript:EditCombination(#=IndexId#,\'#=BrandName#\',\'#=ProductPackingName#\',\'#=CatlogName#\',\'#=ProductCatagoryName#\',#=BrandId#,#=ProductPackingId#,#=CatlogId#,#=ProductCategoryId#,#=IsHvp#,#=IsExceptProduct#)">Edit</a>',
                    width: "50px"
                },
            {field: "ProductCatagoryName", title: "Item Group", width: "112px", hidden: false },
            { field: "BrandName", title: "Brand", width: "120px", hidden: false },
            { field: "CatlogName", title: "Product", width: "130px", hidden: false },
            { field: "ProductPackingName", title: "packing type", width: "100px", hidden: false },
            { field: "IsHvp", title: "HVP", width: "100px", hidden: false }
        ],
                editable: false, // enable editing
                pageable: false,
                sortable: false,
                filterable: false,
                selectable: "single",
                columnMenu: false,
                //dataBound: LeadCustomerGrid_onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetCombinationList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { indexId: indexId },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }//,
                    //aggregate: [{ field: "rowCount", aggregate: "max"}]
                }
            });
        }



    </script>

    <script type="text/javascript">

        $("#a_add_scheme").click(function () {
            //AddSchemeToSession();
        });

        function AddSchemeToSession() {

            var isret = 0;
            var val = 0;
            var per = 0;

            if ($("#txtSchemeDetails").val().Length == 0) {
                return;
            }

            if ($('#chkIsRetailer').is(":checked")) {
                isret = 1;
            } 

            if ($("#MainContent_DropDownListDetailType").val() == 1) {
                per = 0;
                val = $("#txtDiscountValue").val();
            }
            else if ($("#MainContent_DropDownListDetailType").val() == 2) {
                per = $("#txtPercentage").val();
                val = 0;
            } 

            MainContent_DropDownListDetailType

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=addschemedetail&type=query&id=" + $("#HiddenField_IndexId").val()
                         + "&stype=" + $("#MainContent_DropDownListSchemeType").val()
                         + "&per=" + per
                         + "&val=" + val
                         + "&dtype=" + $("#MainContent_DropDownListDetailType").val()
                         + "&isret=" + isret
                         + "&stext=" + $("#txtSchemeDetails").val();

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        alter("add");
                        //var grid = $("#grid_option_level").data("kendoGrid");
                        //grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }

        $("#DropDownListOperator").change(function () {
            
            if ($(this).val() == 6) {
                $("#div_between").css("display", "block");
            } else {
                $("#div_between").css("display", "none");
            }
        });
        $("#MainContent_DropDownListSchemeType").change(function () {
            
            if ($(this).val() == 1) {
                $("#div_typebill").css("display", "block");
            } else {
                $("#div_typebill").css("display", "none");
            }
        });
        
        $("#chkIsBill").change(function () {
            if (this.checked) {
                $("#div_qty_limit").css("display", "none");
                $("#div_bill").css("display", "block");
            } else {
                $("#div_qty_limit").css("display", "block");
                $("#div_bill").css("display", "none");
            }
        });

        $("#a_option_level").click(function () {
            AddOptionLevel();
        });
        function RemoveOptionLevelFromSession(indexId) {

            $("#div_combination").css("display", "none");
            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=removeoptionlevel&type=query&id=" + indexId;

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        var grid = $("#grid_option_level").data("kendoGrid");
                        grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }
//        function EditOptionLevel(ProductId, Qty, Product) {
        function EditOptionLevel(id, brand, packt, cat, itemg, brandid, packtid, catid, itemgid, maxq, minq, maxv, minv, opeid, isbill, ishvp) {
            $("#div_combination").css("display", "none");
            $("#HiddenField_IndexId").val(id);
            $("#txtBrand").val(brand);
            $("#txtPackingType").val(packt);
            $("#txtCatlog").val(cat);
            $("#txtItemGroup").val(itemg);
            $("#HiddenField_BrandId").val(brandid);
            $("#HiddenField_PackingTypeId").val(packtid);
            $("#HiddenField_CatlogId").val(catid);
            $("#HiddenField_ItemGroupId").val(itemgid);
            $("#txtMinQuantity").val(minq);
            $("#txtMaxQuantity").val(maxq);
            $("#txtMinValue").val(minv);
            $("#txtMaxValue").val(maxv);
            
            $('#chkIsBill').prop('checked', isbill);
            if (isbill) {
                $("#div_qty_limit").css("display", "none");
                $("#div_bill").css("display", "block");
                $("#txtMinBValue").val(minv);
                $("#txtMaxBValue").val(maxv);
                $("#txtMinValue").val("0");
                $("#txtMaxValue").val("0");
                $("#DropDownListOperator").val(opeid);
                if (opeid == 6) {
                    $("#div_between").css("display", "block");
                } else {
                    $("#div_between").css("display", "none");
                }
            } else {
                $("#div_qty_limit").css("display", "block");
                $("#div_bill").css("display", "none");
                $("#txtMinValue").val(minv);
                $("#txtMaxValue").val(maxv);
                $("#txtMinBValue").val("0");
                $("#txtMaxBValue").val("0");
                $("#DropDownListOperator").val(0);
            }
            $('#chbIsHVP').prop('checked', ishvp);

            $("#div_min_val").css("display", "block");
            $("#div_max_val").css("display", "block");
            $("#div_isexcept").css("display", "none");

            $("#btnOptionAdd").css("display", "inline-block");
            $("#btnCombinationAdd").css("display", "none");

            wnd.center().open();
        }

        function AddOptionLevel() {
            $("#div_combination").css("display", "none");
            $("#HiddenField_IndexId").val("0");
            $("#txtBrand").val("Any");
            $("#txtPackingType").val("Any");
            $("#txtCatlog").val("Any");
            $("#txtItemGroup").val("Any");
            $("#HiddenField_BrandId").val("0");
            $("#HiddenField_PackingTypeId").val("0");
            $("#HiddenField_CatlogId").val("0");
            $("#HiddenField_ItemGroupId").val("0");
            $("#txtMinQuantity").val("0");
            $("#txtMaxQuantity").val("0");
            $("#txtMinValue").val("0");
            $("#txtMaxValue").val("0");
            $("#DropDownListOperator").val(0);

            $("#div_qty_limit").css("display", "block");
            $("#div_bill").css("display", "none");
            $('#chkIsBill').prop('checked', false);
            $('#chbIsHVP').prop('checked', false);

            $("#div_min_val").css("display", "block");
            $("#div_max_val").css("display", "block");
            $("#div_isexcept").css("display", "none");

            $("#btnOptionAdd").css("display", "inline-block");
            $("#btnCombinationAdd").css("display", "none");

            wnd.center().open();
        }
        var wnd = null;
        function LoadOptionLevelPopUp() {

            $("#btnOptionNo").css("display", "inline-block");
            $("#btnOptionAdd").css("display", "inline-block");
            $("#btnCombinationAdd").css("display", "none");

            wnd = $("#div_option_level_form").kendoWindow({
                title: "Add Option Level",
                modal: true,
                visible: false,
                resizable: false,
                width: 700
            }).data("kendoWindow");

            //$("#div_confirm_message").text(message);
            //wnd.center().open();
            wnd.close();
            $("#btnOptionAdd").click(function () {
                AddOptionLevelToSession();
                wnd.close();
            });

            $("#btnOptionNo").click(function () {
                wnd.close();
            });

            $("#btnCombinationAdd").click(function () {
                AddCombinationToSession();
                wnd.close();
            });
            
        }

        function AddOptionLevelToSession() {
            var ishvp = 0;
            var isbill = 0;
            var opeid = 0;
            var maxb = 0;
            var minb = 0;

            if ($('#chkIsBill').is(":checked")) {
                isbill = 1;
                opeid = $("#DropDownListOperator").val();
                minb = $("#txtMinBValue").val();
                if (opeid == 6) {
                    maxb = $("#txtMaxBValue").val();
                }
            } else {
                minb = $("#txtMinValue").val();
                maxb = $("#txtMaxValue").val();
                opeid = 0;
                isbill = 0;
            }
            

            if ($('#chbIsHVP').is(":checked")) {
                ishvp = 1;
            }
            

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=addoptionlevel&type=query&id=" + $("#HiddenField_IndexId").val()
                         + "&brand=" + $("#txtBrand").val()
                         + "&packt=" + $("#txtPackingType").val()
                         + "&cat=" + $("#txtCatlog").val()
                         + "&itemg=" + $("#txtItemGroup").val()
                         + "&brandid=" + $("#HiddenField_BrandId").val()
                         + "&packtid=" + $("#HiddenField_PackingTypeId").val()
                         + "&catid=" + $("#HiddenField_CatlogId").val()
                         + "&itemgid=" + $("#HiddenField_ItemGroupId").val()
                         + "&minq=" + $("#txtMinQuantity").val()
                         + "&maxq=" + $("#txtMaxQuantity").val()
                         + "&minv=" + minb //$("#txtMinValue").val()
                         + "&maxv=" + maxb //$("#txtMaxValue").val()
                         + "&ishvp=" + ishvp
                         + "&isbill=" + isbill
                         + "&opeid=" + opeid
                         //+ "&maxb=" + maxb
                         //+ "&minb=" + minb;
            
            
            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        var grid = $("#grid_option_level").data("kendoGrid");
                        grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }

        function OptionLevel_onChange(arg) {
            var selectedItem = this.dataItem(this.select());
            $("#HiddenField_IndexId").val(selectedItem.IndexId);
            $("#div_combination").css("display", "block");
            GetCombinationList(selectedItem.IndexId);
        }

        function GetOptionLevelList() {

            // var take_grid = $("#MainContent_hfPageIndex").val();
            $("#grid_option_level").html("");
            $("#grid_option_level").kendoGrid({
                // height: 400,
                columns: [
                { template: '<a href="javascript:RemoveOptionLevelFromSession(#=IndexId#)">Delete</a>',
                    width: "50px"
                },
                { template: '<a href="javascript:EditOptionLevel(#=IndexId#,\'#=BrandName#\',\'#=ProductPackingName#\',\'#=CatlogName#\',\'#=ProductCatagoryName#\',#=BrandId#,#=ProductPackingId#,#=CatlogId#,#=ProductCategoryId#,#=MaxQuantity#,#=MinQuantity#,#=MaxValue#,#=MinValue#,#=OperatorId#,#=IsBill#,#=IsHvp#)">Edit</a>',
                    width: "50px"
                },
//            { template: '<a href="javascript:EditFreeProduct(#=ProductId#,#=Qty#,\'#=ProductName#\')">#=ProductName#</a>',
//                field: "ProductName", width: "200px", title: "SKU"
//            },
                //{field: "ProductName", title: "SKU", width: "85px", hidden: false },
            { field: "ProductCatagoryName", title: "Item Group", width: "112px", hidden: false },
            { field: "BrandName", title: "Brand", width: "120px", hidden: false },
            { field: "CatlogName", title: "Product", width: "130px", hidden: false },
            { field: "ProductPackingName", title: "packing type", width: "100px", hidden: false },
            { field: "IsHvp", title: "HVP", width: "100px", hidden: false },
            { field: "MinQuantity", title: "MinQuantity", width: "100px", hidden: false },
            { field: "MaxQuantity", title: "MaxQuantity", width: "100px", hidden: false },
            { field: "MinValue", title: "MinValue", width: "100px", hidden: false },
            { field: "MaxValue", title: "MaxValue", width: "100px", hidden: false } 
        ],
                editable: false, // enable editing
                pageable: false,
                sortable: false,
                filterable: false,
                selectable: "single",
                columnMenu: false,
                change: OptionLevel_onChange,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetOptionLevelList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer', pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }//,
                    //aggregate: [{ field: "rowCount", aggregate: "max"}]
                }
            });
        }

        jQuery(function ($) {
            GetOptionLevelList();
            LoadOptionLevelPopUp();

            //            hideStatusDiv("MainContent_div_message");

            //            jQuery(function ($) {
            //                $('input.input_numeric_tp').autoNumeric({ aSep: null, aDec: null, mDec: 0 });
            //            }); meric({ aSep: '.', aDec: ',' });
            //loadEntryPageTabs("customer");

        });


        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
</script>
<script type="text/javascript">

    $("#MainContent_DropDownListDetailType").change(function () {
        if ($(this).val() == 1) {
            $('#div_Percentage').css('display', 'none');
            $('#div_value').css('display', 'block');
            $('#div_free_product').css('display', 'none');
        }
        else if ($(this).val() == 2) {
            $('#div_Percentage').css('display', 'block');
            $('#div_value').css('display', 'none');
            $('#div_free_product').css('display', 'none');
        }
        else if ($(this).val() == 3) {
            GetFreeProductList();
            $('#div_Percentage').css('display', 'none');
            $('#div_value').css('display', 'none');
            $('#div_free_product').css('display', 'block');            
        }
    });
    

    function GetFreeProductList() {

           // var take_grid = $("#MainContent_hfPageIndex").val();
        $("#dis_product").html("");
        $("#dis_product").kendoGrid({
                // height: 400,
            columns: [
                { template: '<a href="javascript:RemoveToSession(#=ProductId#,\'#=ProductName#\')">Delete</a>',
                     width: "50px"
                },
            { template: '<a href="javascript:EditFreeProduct(#=ProductId#,#=Qty#,\'#=ProductName#\')">#=ProductName#</a>',
                field: "ProductName", width: "200px", title: "SKU"
            },
            //{field: "ProductName", title: "SKU", width: "85px", hidden: false },
            { field: "Qty", title: "Qty", width: "120px", hidden: false },
            { field: "ProductCatagoryName", title: "Item Group", width: "105px", hidden: true },
            { field: "BrandName", title: "Brand", width: "60px", hidden: true },
            { field: "CatlogName", title: "Product", width: "130px", hidden: true },
            { field: "ProductPackingName", title: "packing type", width: "98px", hidden: true },
            { field: "ItemTypeName", title: "HVP", width: "250px", hidden: true }
        ],
                editable: false, // enable editing
                pageable: false,
                sortable: false,
                filterable: false,
                selectable: "single",
                columnMenu: false,
                //dataBound: LeadCustomerGrid_onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
//                                Name: { validation: { required: true} },
//                                LeadStage: { validation: { required: true} },
//                                CustomerCode: { editable: true, nullable: true },
//                                State: { validation: { required: true} },
//                                City: { validation: { required: true} },
//                                SourceId: { validation: { required: true} },
//                                Address: { validation: { required: true} },
//                                rowCount: { type: "number", validation: { required: true} }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetFreeProductList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer', pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
//                                if (textStatus == "success") {
//                                    var entityGrid = $("#grid").data("kendoGrid");
//                                    if ((take_grid != '') && (gridindex == '0')) {
//                                        entityGrid.dataSource.page((take_grid - 1));
//                                        gridindex = '1';
//                                    }
//                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }//,
                    //aggregate: [{ field: "rowCount", aggregate: "max"}]
                }
            });
        }

        $("#a_product").click(function () {
            AddFreeProduct();
        });

        function EditFreeProduct(ProductId, Qty, Product) {
            $("#MainContent_HiddenField_ProductId").val(ProductId);
            $("#MainContent_txtQty").val(Qty);
            $("#MainContent_txtProduct").val(Product);
            LoadPopUp();
        }

        function AddFreeProduct() {
            $("#MainContent_HiddenField_ProductId").val(0);
            $("#MainContent_txtQty").val(0);
            $("#MainContent_txtProduct").val('');
            LoadPopUp();
        }
        function LoadPopUp() {
            
            //$("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var wnd = $("#div_free_product_form").kendoWindow({
                title: "Add Free Product",
                modal: true,
                visible: false,
                resizable: false,
                width: 300
            }).data("kendoWindow");

            //$("#div_confirm_message").text(message);
            wnd.center().open();

            $("#yes").click(function () {
                AddToSession();
                wnd.close();
            });

            $("#no").click(function () {
                wnd.close();
            });
        }

        $("#id_schedule").click(function () {
            loadFreeProductList();
        });

        
        function loadFreeProductList() {
            $("#Product_subwindow").html("");
            $("#Product_subwindow").kendoGrid({
                height: 300,
                columns: [
                    { field: "ProductId", title: "ProductId", hidden: false },
                    { template: '<a href="javascript:OpenSelectFreeProduct(#=ProductId#,\'#=Name#\')">#=Name#</a>',
                    field: "Program Name", width: "130px"
                    }

                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                //filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                ProgramId: { validation: { required: true} },
                                ProgramName: { validation: { required: true} },
                                Status: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllProductsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    //$("#program_window").data("kendoWindow").open();

                                    var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "500px",
                                            height: "400px",
                                            title: "Lookup",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();

                                    $("div#div_loader").hide();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }


        function OpenSelectFreeProduct(productId, name) {
            $("#MainContent_HiddenField_ProductId").val(productId);
            $("#MainContent_txtProduct").val(name);
            $("#program_window").data("kendoWindow").close();           
        }
        function AddToSession() {

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=addfreeproduct&type=query&id=" + $("#MainContent_HiddenField_ProductId").val()
                         + "&qty=" + $("#MainContent_txtQty").val() + "&name=" +$("#MainContent_txtProduct").val();

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        var grid = $("#dis_product").data("kendoGrid");
                        grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }

        function RemoveToSession(ProductId) {

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=removefreeproduct&type=query&id=" + ProductId;

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        var grid = $("#dis_product").data("kendoGrid");
                        grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }


        function OpenSelectBrand(brandId, name) {
            $("#HiddenField_BrandId").val(brandId);
            $("#txtBrand").val(name);
            $("#program_window").data("kendoWindow").close();
        }
        $("#a_brand").click(function () {
            loadBrandList();
        });
        function loadBrandList() {
            $("#Product_subwindow").html("");
            $("#Product_subwindow").kendoGrid({
                height: 300,
                columns: [
                    { field: "BrandId", title: "BrandId", width: "130px", hidden: false },
                    { template: '<a href="javascript:OpenSelectBrand(#=BrandId#,\'#=BrandName#\')">#=BrandName#</a>',
                        field: "Brand Name"
                    }

                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                //filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                ProgramId: { validation: { required: true} },
                                ProgramName: { validation: { required: true} },
                                Status: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllBrandsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                
                                if (textStatus == "success") {
                                    //$("#program_window").data("kendoWindow").open();

                                    var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "500px",
                                            height: "400px",
                                            title: "Lookup",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();

                                    $("div#div_loader").hide();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }



        function OpenSelectPackingType(PackingTypeId, name) {
            $("#HiddenField_PackingTypeId").val(PackingTypeId);
            $("#txtPackingType").val(name);
            $("#program_window").data("kendoWindow").close();
        }
        $("#a_PackingType").click(function () {
            loadPackingTypeList();
        });
        function loadPackingTypeList() {
            $("#Product_subwindow").html("");
            $("#Product_subwindow").kendoGrid({
                height: 300,
                columns: [
                    { field: "Code", title: "Packing Type Id", width: "130px", hidden: false },
                    { template: '<a href="javascript:OpenSelectPackingType(#=Code#,\'#=Description#\')">#=Description#</a>',
                        field: "Packing Type"
                    }

                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                //filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                ProgramId: { validation: { required: true} },
                                ProgramName: { validation: { required: true} },
                                Status: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetAllPackingTypeDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            //data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {

                                if (textStatus == "success") {
                                    //$("#program_window").data("kendoWindow").open();

                                    var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "500px",
                                            height: "400px",
                                            title: "Lookup",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();

                                    $("div#div_loader").hide();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }



        function OpenSelectCatlog(CatlogId, name) {
            $("#HiddenField_CatlogId").val(CatlogId);
            $("#txtCatlog").val(name);
            $("#program_window").data("kendoWindow").close();
        }
        $("#a_Catlog").click(function () {
            loadCatlogList();
        });
        function loadCatlogList() {
            $("#Product_subwindow").html("");
            $("#Product_subwindow").kendoGrid({
                height: 300,
                columns: [
                    { field: "CatlogId", title: "Catlog Id", width: "130px", hidden: false },
                    { template: '<a href="javascript:OpenSelectCatlog(#=CatlogId#,\'#=Description#\')">#=Description#</a>',
                        field: "Catlog"
                    }

                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                //filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                ProgramId: { validation: { required: true} },
                                ProgramName: { validation: { required: true} },
                                Status: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetCatalogDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            //data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {

                                if (textStatus == "success") {
                                    //$("#program_window").data("kendoWindow").open();

                                    var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "500px",
                                            height: "400px",
                                            title: "Lookup",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();

                                    $("div#div_loader").hide();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }




        function OpenSelectProductCategory(PackingTypeId, name) {
            $("#HiddenField_ItemGroupId").val(PackingTypeId);
            $("#txtItemGroup").val(name);
            $("#program_window").data("kendoWindow").close();
        }
        $("#a_ItemGroup").click(function () {
            loadProductCategoryList();
        });
        function loadProductCategoryList() {
            $("#Product_subwindow").html("");
            $("#Product_subwindow").kendoGrid({
                height: 300,
                columns: [
                    { field: "Code", title: "Item Group id", width: "130px", hidden: false },
                    { template: '<a href="javascript:OpenSelectProductCategory(#=Code#,\'#=Description#\')">#=Description#</a>',
                        field: "Item Group"
                    }

                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                //filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                ProgramId: { validation: { required: true} },
                                ProgramName: { validation: { required: true} },
                                Status: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetAllProductCategoryDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            //data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {

                                if (textStatus == "success") {
                                    //$("#program_window").data("kendoWindow").open();

                                    var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "500px",
                                            height: "400px",
                                            title: "Lookup",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();

                                    $("div#div_loader").hide();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }


    </script>
</asp:Content>

