﻿<%@ Page Title="mSales - Incentive Plan" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="incentive_plan.aspx.cs" Inherits="incentive_plan_master_incentive_plan" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfPageIndex2" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedHeaderId" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedDetailId" runat="server" />
    <asp:HiddenField ID="HiddenFieldWeeklyGrid" runat="server" />
    <asp:HiddenField ID="HiddenFieldPartialGrid" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenFieldWeeklyUID" ClientIDMode="Static"  />
    <asp:HiddenField runat="server" ID="HiddenFieldPartialUID" ClientIDMode="Static"  />
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 400px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">
                                    Header
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" style="width: 209px" class="textboxwidth" id="txtHeader" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Cum. Target (%)
                                </td>
                                <td>
                                    <span class="wosub mand">&nbsp;<input type="text" style="width: 50px" class="textboxwidth input_numeric_target"
                                        id="txtCumTarget" readonly="readonly" />
                                        <span style="color: Red">*</span></span>
                                </td>
                                <td class="textalignbottom">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Incentive (%)
                                </td>
                                <td>
                                    <span class="wosub mand">&nbsp;<input type="text" style="width: 50px" class="textboxwidth input_numeric_target"
                                        id="txtWkIncentive" readonly="readonly" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <%--<tr>
                                <td class="textalignbottom">
                                    Incentive (%)
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" style="width: 50px" class="textboxwidth input_numeric_target"
                                        id="txtWkIncentive" readonly="readonly" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>--%>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonAddWeekly" type="button">
                                        OK</button>
                                    <button class="k-button" id="buttonClear" style="display: none">
                                        Clear</button>
                                    <asp:HiddenField ID="hdnSelectedSecondarySalesWeeklyId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="window2" style="display: none; width: 300px;">
            <div style="width: 100%" id="contactentry2">
                <div id="div_text2">
                    <div id="div_message_popup2" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">
                                    Achievement (%)
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" style="width: 100px" class="textboxwidth input_numeric_target"
                                        id="txtAchievement" readonly="readonly" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Incentive (%)
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" style="width: 100px" class="textboxwidth input_numeric_target"
                                        id="txtIncentive" readonly="readonly" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonAddPartial" type="button">
                                        OK</button>
                                    <button class="k-button" id="buttonClear2" style="display: none">
                                        Clear</button>
                                    <asp:HiddenField ID="hdnSelectedSecondarySalesPartialId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="toolbar_container">
           <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
           <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div class="formleft">
            <div class="formtextdiv">
                Plan Name</div>
            <div class="formdetaildiv_right">
                
                <asp:TextBox ID="txtPlanName" runat="server" CssClass="input-large1"></asp:TextBox>
                <a href="#" id="id_select_plan" runat="server">
                                <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Description</div>
            <div class="formdetaildiv_right">
                
                <asp:TextBox ID="txtDescription" runat="server" CssClass="input-large1" TextMode="MultiLine"
                    Height="50px"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
        </div>
        <div class="formright">
            <div class="formtextdiv">
                Effective Start Date</div>
            <div class="formdetaildiv_right" style="margin-top: -10px;">
                <div class="datepicker" id="div_Date">
                    <asp:TextBox ID="dtpDate" runat="server" ClientIDMode="Static" Style="width: 120px;"></asp:TextBox>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Effective End Date</div>
            <div class="formdetaildiv_right" style="margin-top: -10px;">
                <div class="datepicker" id="div_Date_to">
                    <asp:TextBox ID="dtpDateTo" runat="server" ClientIDMode="Static" Style="width: 120px;"></asp:TextBox>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Division</div>
            <div class="formdetaildiv_right">                
                <asp:DropDownList ID="DropDownListDivision" runat="server" CssClass="input-large1"
                    Width="150px">
                </asp:DropDownList>
            </div>
            <div class="clearall">
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="formleft">
            <div style="min-width: 200px; overflow: auto; padding: 10px">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left; display: block" id="div_weekly_add">
                        <a id="id_add_weekly_record" clientidmode="Static" runat="server" href="#">Add Record</a>
                    </div>
                </div>
                <div style="float: left; width: 100%; overflow: auto;">
                    <div id="divWeeklyTargetIncentiveGrid">
                    </div>
                </div>
            </div>
        </div>

        <div class="formright">
            <div style="min-width: 200px; overflow: auto; padding: 10px">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left; display: block" id="div_partial_add">
                        <a id="id_add_partial_record" clientidmode="Static" runat="server" href="#">Add Record</a>
                    </div>
                </div>
                <div style="float: left; width: 100%; overflow: auto;">
                    <div id="divPartialAchievementIncentiveGrid">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
    </div>
    <div id="modalWindowPlansLookup">
        <div id="div_plans_subwindow">
        </div>
    </div>
    <script src="../../assets/scripts/moment.js" type="text/javascript"></script>
    <script src="../../assets/scripts/moment-with-locales.js" type="text/javascript"></script>
    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        $("#MainContent_id_select_plan").click(function () {
            loadIncentivePlansLookup();
        });

        $(document).ready(function () {
            jQuery(function ($) {
                $('input.input_numeric_target').autoNumeric({ aSep: null, aDec: '.', mDec: 2 });
            });

            jQuery(function ($) {
                $('input.input_numeric_incentive').autoNumeric({ aSep: null, aDec: '.', mDec: 2 });
            });

            $("#dtpDate").kendoDatePicker({
                //display month and year in the input
                //value: new Date(),
                format: "dd-MMM-yyyy"
            });
            $("#dtpDateTo").kendoDatePicker({
                // display month and year in the input
                //value: new Date(),
                format: "dd-MMM-yyyy"
            });

            loadWeeklyTargetIncentiveGrid();
            loadPartialAchievementIncentiveGrid();
        });

        //Load popup for Add new Weekly Record
        $("#id_add_weekly_record").click(function () {
            $("#hdnSelectedSecondarySalesWeeklyId").val('0');
            $("#HiddenFieldWeeklyUID").val('');
            
            updateWeeklyEntry('0', '0', '', '', '');
        });

        //Load popup for Add new Partial Record
        $("#id_add_partial_record").click(function () {
            $("#hdnSelectedSecondarySalesPartialId").val('0');
            $("#HiddenFieldPartialUID").val('');

            updatePartialEntry('0', '0', '', '');
        });

        $("#buttonAddWeekly").click(function () {
            //$("#hdnSelectedSecondarySalesWeeklyId").val('0');
            AddUpdateWeeklyEntry();
        });

        $("#buttonAddPartial").click(function () {
            //$("#hdnSelectedSecondarySalesPartialId").val('0');
            AddUpdatePartialEntry();
        });

        function closepopupWeekly (){
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Weekly Targets",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        function closepopupPartial (){
            var wnd = $("#window2").kendoWindow({
                title: "Add/Edit Achievements",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window2").css("display", "none");
        }

        /*Load Weekly Target Grid*/
        function loadWeeklyTargetIncentiveGrid() {
            var take_grid = $("#MainContent_hfPageIndex").val();
            var url = "javascript:updateWeeklyEntry('#=SecondarySalesWeeklyId#','#=IncentivePlanDetailsId#','#=Header#','#=CumTarPerc#','#=IncPerc#');";
            var selectedHeaderId = $("#MainContent_HiddenFieldSelectedHeaderId").val();
            var urlHeader = "<a href=\"" + url + "\">#=Header#</a>";
            var urlTar = "<a href=\"" + url + "\">#=CumTarPerc#%</a>";
            var urlInc = "<a href=\"" + url + "\">#=IncPerc#%</a>";

            var deleteUrl = "javascript:DeleteWeeklyRecord('#=SecondarySalesWeeklyId#');";
            //            var urlCode = "<a href=\"" + url + "\">#=BrandCode#</a>";
            $("#div_loader").show();
            $("#divWeeklyTargetIncentiveGrid").html("");
            $("#divWeeklyTargetIncentiveGrid").kendoGrid({
                height: 200,
                columns: [
            { template: "<a href=\"" + deleteUrl + "\">Delete</a>", width: "50px" },
            { field: "SecondarySalesWeeklyId", title: "SecondarySalesWeeklyId", width: "85px", hidden: true },
            { field: "IncentivePlanDetailsId", title: "IncentivePlanDetailsId", width: "85px", hidden: true },
            { template: urlHeader, field: "Header", title: "Header", width: "200px", hidden: false },
            { template: urlTar, field: "CumTarPerc", title: "Cumulative Target (%)", width: "120px", hidden: false, format: "{0:n2}" },
            { template: urlInc, field: "IncPerc", title: "Incentive (%)", width: "120px", hidden: false, format: "{0:n2}" },
            { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "LastModifiedDate", width: "150px", title: "LastModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true }
        ],
                editable: false, // enable editing
                pageable: false,
                sortable: false,
                filterable: false,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 100,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                SecondarySalesWeeklyId: { validation: { required: true} },
                                IncentivePlanDetailsId: { validation: { required: true} },
                                Header: { editable: false, nullable: true },
                                CumTarPerc: { type: "number", editable: false, nullable: true },
                                IncPerc: { type: "number", editable: false, nullable: true },
                                CreatedDate: { type: "date", validation: { required: false} },
                                LastModifiedDate: { type: "date", validation: { required: false} }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetWeeklyTargetIncentives", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { selectedHeaderId: selectedHeaderId, pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#divWeeklyTargetIncentiveGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        /*Load Partial Target Grid*/
        function loadPartialAchievementIncentiveGrid() {
            var take_grid = $("#MainContent_hfPageIndex2").val();
            var url = "javascript:updatePartialEntry('#=SecondarySalesPartialId#','#=IncentivePlanDetailsId#','#=AchievementPerc#','#=IncentivePerc#');";
            var selectedHeaderId = $("#MainContent_HiddenFieldSelectedHeaderId").val();
            var urlAch = "<a href=\"" + url + "\">#=AchievementPerc#%</a>";
            var urlInc = "<a href=\"" + url + "\">#=IncentivePerc#%</a>";
            //            var urlCode = "<a href=\"" + url + "\">#=BrandCode#</a>";
            var deleteUrl = "javascript:DeletePartialRecord('#=SecondarySalesPartialId#');";

            $("#div_loader").show();
            $("#divPartialAchievementIncentiveGrid").html("");
            $("#divPartialAchievementIncentiveGrid").kendoGrid({
                height: 200,
                columns: [
            { template: "<a href=\"" + deleteUrl + "\">Delete</a>", width: "50px" },
            { field: "SecondarySalesPartialId", title: "SecondarySalesWeeklyId", width: "85px", hidden: true },
            { field: "IncentivePlanDetailsId", title: "IncentivePlanDetailsId", width: "85px", hidden: true },
            { template: urlAch, field: "AchievementPerc", title: "Achievement (%)", width: "120px", hidden: false, format: "{0:n2}" },
            { template: urlInc, field: "IncentivePerc", title: "Incentive (%)", width: "120px", hidden: false, format: "{0:n2}" },
            { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "LastModifiedDate", width: "150px", title: "LastModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true }
        ],
                editable: false, // enable editing
                pageable: false,
                sortable: false,
                filterable: false,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                SecondarySalesPartialId: { validation: { required: true} },
                                IncentivePlanDetailsId: { validation: { required: true} },
                                AchievementPerc: { type: "number", editable: false, nullable: true },
                                IncentivePerc: { type: "number", editable: false, nullable: true },
                                CreatedDate: { type: "date", validation: { required: false} },
                                LastModifiedDate: { type: "date", validation: { required: false} }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetPartialTargetIncentives", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { selectedHeaderId: selectedHeaderId, pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#divPartialAchievementIncentiveGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }
        
        /*load Poppup Weekly Entry */
        function updateWeeklyEntry(SecondarySalesWeeklyId, IncentivePlanDetailsId, Header, CumTarPerc, IncPerc) {
            $("#hdnSelectedSecondarySalesWeeklyId").val(SecondarySalesWeeklyId);
            $("#txtHeader").val(Header);
            $("#txtCumTarget").val(CumTarPerc);
            $("#txtWkIncentive").val(IncPerc);

            //Set selected record UID
            $("#HiddenFieldWeeklyUID").val("");
            var grid = $("#divWeeklyTargetIncentiveGrid").data("kendoGrid");
            var row = grid.select();
            if (row[0] != undefined) {
                $("#HiddenFieldWeeklyUID").val(row[0].dataset.uid);
            }

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Weekly Targets",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtHeader").focus();

            wnd.center().open();
        }

        /*Load poppup Partial Target */
        function updatePartialEntry(SecondarySalesPartialsId, IncentivePlanDetailsId, AchievementPerc, IncentivePerc) {
            //debugger;
            $("#hdnSelectedSecondarySalesPartialId").val(SecondarySalesPartialsId);
            $("#hdnSelectedSecondarySalesPartialId").val(IncentivePlanDetailsId);
            $("#txtAchievement").val(AchievementPerc);
            $("#txtIncentive").val(IncentivePerc);

            //Set selected record UID
            $("#HiddenFieldPartialUID").val("");
            var grid = $("#divPartialAchievementIncentiveGrid").data("kendoGrid");
            var row = grid.select();

            if (row[0] != undefined) {
                $("#HiddenFieldPartialUID").val(row[0].dataset.uid);
            }

            $("#window2").css("display", "block");
            var wnd = $("#window2").kendoWindow({
                title: "Add/Edit Achievements",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtCumTarget").focus();

            wnd.center().open();
        }
        
        /*Add/Update Weekly Entry */
        function AddUpdateWeeklyEntry() {
            //debugger;
            var secondarySalesWeeklyId = $("#hdnSelectedSecondarySalesWeeklyId").val();
            var header = $("#txtHeader").val();
            var cumTarget = $("#txtCumTarget").val();
            var incPerc = $("#txtWkIncentive").val();

            if (header.trim() == "") {
                var infoMsg = GetInformationMessageDiv("Header can not be empty!", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(infoMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (cumTarget.trim() == "") {
                var infoMsg = GetInformationMessageDiv("Cum. Target Percentage can not be empty!", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(infoMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (incPerc.trim() == "") {
                var infoMsg = GetInformationMessageDiv("Incentive Percentage can not be empty!", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(infoMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }   
                 
            var weekly_uid = $("#HiddenFieldWeeklyUID").val();
            var weekly_grid = $("#divWeeklyTargetIncentiveGrid").data("kendoGrid");
            var weekly_model = $("#divWeeklyTargetIncentiveGrid").data("kendoGrid").dataSource.getByUid(weekly_uid);

            if (weekly_model != undefined) {
                //Update record in the grid (#divWeeklyTargetIncentiveGrid) with the changes

                //Header
                weekly_model.fields.Header.editable = true;
                weekly_model.set("Header", header);
                weekly_model.fields.Header.editable = false;

                //parseFloat
                weekly_model.fields.CumTarPerc.editable = true;
                weekly_model.set("CumTarPerc", parseFloat(cumTarget));
                weekly_model.fields.CumTarPerc.editable = false;

                //parseFloat
                weekly_model.fields.IncPerc.editable = true;
                weekly_model.set("IncPerc", parseFloat(incPerc));
                weekly_model.fields.IncPerc.editable = false;
            }
            else
            {
                //Add new record to the grid (#divWeeklyTargetIncentiveGrid) with the values
                var weekly_grid = $('#divWeeklyTargetIncentiveGrid').data('kendoGrid');
                var weekly_dataSource = weekly_grid.dataSource;
                weekly_dataSource.add({ SecondarySalesWeeklyId: 0,
                    IncentivePlanDetailsId: 0,
                    Header: header,
                    CumTarPerc: parseFloat(cumTarget),
                    IncPerc: parseFloat(incPerc),
                    CreatedDate: new Date(),
                    LastModifiedDate: new Date()
                });
            }

            weekly_grid.refresh();

            closepopupWeekly();
        }

        /*Add/Update Partial Entry */
        function AddUpdatePartialEntry() {
            var secondarySalesPartialId = $("#hdnSelectedSecondarySalesPartialId").val();
            var achievement = $("#txtAchievement").val();
            var incentive = $("#txtIncentive").val();

            if(achievement.trim() == "")
            {
                var infoMsg = GetInformationMessageDiv("Achievement Percentage can not be empty!", "MainContent_div_message_popup2");
                $('#MainContent_div_message_popup2').css('display', 'block');
                $("#MainContent_div_message_popup2").html(infoMsg);
                hideStatusDiv("MainContent_div_message_popup2");
                return;
            }

            if(incentive.trim() == "")
            {
                var infoMsg = GetInformationMessageDiv("Incentive Percentage can not be empty!", "MainContent_div_message_popup2");
                $('#MainContent_div_message_popup2').css('display', 'block');
                $("#MainContent_div_message_popup2").html(infoMsg);
                hideStatusDiv("MainContent_div_message_popup2");
                return;
            }

            var partial_uid = $("#HiddenFieldPartialUID").val();
            var partial_grid = $("#divPartialAchievementIncentiveGrid").data("kendoGrid");
            var partial_model = $("#divPartialAchievementIncentiveGrid").data("kendoGrid").dataSource.getByUid(partial_uid);

            if (partial_model != undefined) {
                //Update record in the grid (#divPartialAchievementIncentiveGrid) with the changes

                //AchievementPerc
                partial_model.fields.AchievementPerc.editable = true;
                partial_model.set("AchievementPerc", parseFloat(achievement));
                partial_model.fields.AchievementPerc.editable = false;

                //IncentivePerc
                partial_model.fields.IncentivePerc.editable = true;
                partial_model.set("IncentivePerc", parseFloat(incentive));
                partial_model.fields.IncentivePerc.editable = false;
            }
            else
            {
                //Add new record to the grid (#divPartialAchievementIncentiveGrid) with the values
                var partial_grid = $('#divPartialAchievementIncentiveGrid').data('kendoGrid');
                var partial_dataSource = partial_grid.dataSource;
                partial_dataSource.add({SecondarySalesPartialId: 0, 
                                        IncentivePlanDetailsId: 0,
                                        AchievementPerc: parseFloat(achievement),
                                        IncentivePerc: parseFloat(incentive),
                                        CreatedDate: new Date(),
                                        LastModifiedDate: new Date()
                                    });
            }

            partial_grid.refresh();

            closepopupPartial();
        }

        /*Load Incentive Plans Lookup*/
        function loadIncentivePlansLookup() {
            $("#div_loader").show();
            $("#div_plans_subwindow").html("");
            $("#div_plans_subwindow").kendoGrid({
                height: 380,
                columns: [
                    { field: "IncentivePlanHeaderId", title: "IncentivePlanHeaderId", hidden: true },
                    { template: '<a href="javascript:OpenSelectedPlan(\'#=IncentivePlanHeaderId#\',\'#=PlanName#\',\'#=Description#\',\'#=StartDate#\',\'#=EndDate#\',\'#=DivisionId#\')">#=PlanName#</a>',
                        field: "Plan", width: "250px"
                    },
                    { field: "Description", title: "Description", hidden: true, width: "250px" },
                    { field: "DivisionId", title: "DivisionId", hidden: true, width: "100px" },
                    { field: "StartDate", width: "150px", title: "Start Date", format: DATE_FORMAT, hidden: false },
                    { field: "EndDate", width: "150px", title: "End Date", format: DATE_FORMAT, hidden: false }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 500,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                IncentivePlanHeaderId: { validation: { required: true} },
                                PlanName: { validation: { required: true} },
                                Description: { validation: { required: true} },
                                DivisionId: { validation: { required: true} },
                                StartDate: { type: "date", validation: { required: false} },
                                EndDate: { type: "date", validation: { required: false} }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllIncentivePlansForLookup", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: {pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                $("#div_loader").hide();
                                if (textStatus == "success") {
                                    var newcust_window = $("#modalWindowPlansLookup"),
                                        newcust_undo = $("#undo").bind("click", function () {
                                            newcust_window.data("kendoWindow").open();
                                            newcust_undo.hide();
                                        });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "650px",
                                            height: "420px",
                                            title: "Incenitve Plans",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        $("#MainContent_buttonbar_buttinSave").click(function () {
            entityGrid = $("#divWeeklyTargetIncentiveGrid").data("kendoGrid");
            $("#MainContent_HiddenFieldWeeklyGrid").val(JSON.stringify(entityGrid.dataSource.view()));

            entityGrid = $("#divPartialAchievementIncentiveGrid").data("kendoGrid");
            $("#MainContent_HiddenFieldPartialGrid").val(JSON.stringify(entityGrid.dataSource.view()));
        });

        function OpenSelectedPlan(incentivePlanHeaderId, planName, description, startDate, endDate, divisionId) {
            //debugger;
            $("#MainContent_HiddenFieldSelectedHeaderId").val(incentivePlanHeaderId);
            $("#MainContent_txtPlanName").val(planName);
            $("#MainContent_txtDescription").val(description);

            var sDate = new Date(startDate);
            var eDate = new Date(endDate);

            $("#dtpDate").val(moment(sDate).format('DD-MMM-YYYY'));
            $("#dtpDateTo").val(moment(eDate).format('DD-MMM-YYYY'));

            $("#MainContent_DropDownListDivision").val(divisionId);

            loadWeeklyTargetIncentiveGrid();
            loadPartialAchievementIncentiveGrid();

            $("#modalWindowPlansLookup").data("kendoWindow").close();
        }

        function DeleteWeeklyRecord(weeklyId) {
            if (confirm("Are you sure you want to delete this Record?")) {
                //newly added record (remove from the data source)
                if (weeklyId == '0') {
                    //Set selected record UID
                    var grid = $("#divWeeklyTargetIncentiveGrid").data("kendoGrid");
                    var row = grid.select();
                    $("#HiddenFieldWeeklyUID").val(row[0].dataset.uid);
                    var uid = $("#HiddenFieldWeeklyUID").val();
                    var model = $("#divWeeklyTargetIncentiveGrid").data("kendoGrid").dataSource.getByUid(uid);
                    $('#divWeeklyTargetIncentiveGrid').data("kendoGrid").dataSource.remove(model);

                    var sucessMsg = GetSuccesfullMessageDiv("Record was successfully Deleted.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    $("div#div_loader").hide();
                    grid.refresh();
                    hideStatusDiv("MainContent_div_message");
                    return;
                }
                
                $("div#div_loader").show();
                var param = { 'weeklyId': weeklyId };

                var url = ROOT_PATH + "service/lead_customer/common.asmx/DeactivateSecondarySalesWeeklyRecord";

                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.hasOwnProperty('d')) {
                            msg = response.d;
                        } else {
                            msg = response;
                        }
                        if (msg == "true") {
                            var sucessMsg = GetSuccesfullMessageDiv("Record was successfully Deleted.", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(sucessMsg);
                            $("div#div_loader").hide();
                            loadWeeklyTargetIncentiveGrid();
                            hideStatusDiv("MainContent_div_message");
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            $("div#div_loader").hide();
                            hideStatusDiv("MainContent_div_message");
                        }
                    },
                    error: function (response) {
                        var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        $("div#div_loader").hide();
                        hideStatusDiv("MainContent_div_message");
                    }
                });
            }
        }

        function DeletePartialRecord(partialId) {
            if (confirm("Are you sure you want to delete this Record?")) {
                //newly added record (remove from the data source)
                if (weeklyId == '0') {
                    //Set selected record UID
                    var grid = $("#divPartialAchievementIncentiveGrid").data("kendoGrid");
                    var row = grid.select();
                    $("#HiddenFieldPartialUID").val(row[0].dataset.uid);
                    var uid = $("#HiddenFieldPartialUID").val();
                    var model = $("#divPartialAchievementIncentiveGrid").data("kendoGrid").dataSource.getByUid(uid);
                    $('#divPartialAchievementIncentiveGrid').data("kendoGrid").dataSource.remove(model);

                    var sucessMsg = GetSuccesfullMessageDiv("Record was successfully Deleted.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    $("div#div_loader").hide();
                    grid.refresh();
                    hideStatusDiv("MainContent_div_message");
                    return;
                }

                $("div#div_loader").show();
                var param = { 'partialId': partialId };

                var url = ROOT_PATH + "service/lead_customer/common.asmx/DeactivateSecondarySalesPartialRecord";

                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.hasOwnProperty('d')) {
                            msg = response.d;
                        } else {
                            msg = response;
                        }
                        if (msg == "true") {
                            var sucessMsg = GetSuccesfullMessageDiv("Record was successfully Deleted.", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(sucessMsg);
                            $("div#div_loader").hide();
                            loadPartialAchievementIncentiveGrid();
                            hideStatusDiv("MainContent_div_message");
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            $("div#div_loader").hide();
                            hideStatusDiv("MainContent_div_message");
                        }
                    },
                    error: function (response) {
                        var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        $("div#div_loader").hide();
                        hideStatusDiv("MainContent_div_message");
                    }
                });
            }
        }

    </script>
</asp:Content>
