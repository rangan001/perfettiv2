﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using CRMServiceReference;
using System.Web.Script.Serialization;

public partial class incentive_plan_master_incentive_plan : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //Set the Toolbar buttons
            buttonbar.VisibleSave(true);
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.EnableSave(true);

            buttonbar.VisibleDeactivate(true);
            buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
            buttonbar.EnableDeactivate(true);

            if (!IsPostBack)
            {
                DateTime today = DateTime.Today;
                dtpDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
                dtpDateTo.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");

                LoadOptions();

                HiddenFieldSelectedHeaderId.Value = "0";
                HiddenFieldSelectedDetailId.Value = "0";

                Session["SELECTED_DEATIL_ID"] = null;

                HiddenFieldWeeklyGrid.Value = "";
                HiddenFieldPartialGrid.Value = "";
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Incentive Plan ", "#", "");
            
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    public void LoadOptions()
    {
        ArgsDTO args = new ArgsDTO();

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;
        args.OrderBy = " division_name asc ";

        DiscountSchemeClient schemeClient = new DiscountSchemeClient();

        List<DivisionDTO> divisionList = (schemeClient.GetAllDivisions(args)).OrderBy(a => a.DivisionId).ToList();

        DropDownListDivision.DataSource = divisionList;
        DropDownListDivision.DataTextField = "DivisionName";
        DropDownListDivision.DataValueField = "DivisionId";
        DropDownListDivision.DataBind();
    }

    protected void ButtonSave_Click(object sender)
    {
        this.SaveIncentivePlan();
    }

    protected void ButtonClear_Click(object sender)
    {
        HiddenFieldSelectedHeaderId.Value = "0";
        HiddenFieldSelectedDetailId.Value = "0";
        txtPlanName.Text = "";
        txtDescription.Text = "";
        DateTime today = DateTime.Today;
        dtpDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
        dtpDateTo.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");

        HiddenFieldWeeklyGrid.Value = "";
        HiddenFieldPartialGrid.Value = "";

        Session["SELECTED_DEATIL_ID"] = null;
        
        LoadOptions();
    }

    public void SaveIncentivePlan()
    {
        CommonUtility commonUtility = new CommonUtility();
        try
        {
            bool isSucc = false;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = "";

            List<IncentiveSecondarySalesWeeklyDTO> weeklyList = new List<IncentiveSecondarySalesWeeklyDTO>();
            List<IncentiveSecondarySalesPartialDTO> partialList = new List<IncentiveSecondarySalesPartialDTO>();

            List<IncentivePlanDetailsDTO> detailList = new List<IncentivePlanDetailsDTO>();
            IncentivePlanDetailsDTO detail = new IncentivePlanDetailsDTO();

            List<IncentivePlanHeaderDTO> headerList = new List<IncentivePlanHeaderDTO>();
            IncentivePlanHeaderDTO header = new IncentivePlanHeaderDTO();

            List<PaymentSettlementDTO> ChequePaymentsList = new List<PaymentSettlementDTO>();
            List<PaymentSettlementDTO> UpdatedChequePaymentsList = new List<PaymentSettlementDTO>();

            if (!string.IsNullOrEmpty(HiddenFieldWeeklyGrid.Value))
            {
                jsonString = @"" + HiddenFieldWeeklyGrid.Value.Replace("\"ExtensionData\":{},", "");
                weeklyList = serializer.Deserialize<List<IncentiveSecondarySalesWeeklyDTO>>(jsonString);
            }

            if (!string.IsNullOrEmpty(HiddenFieldPartialGrid.Value))
            {
                jsonString = @"" + HiddenFieldPartialGrid.Value.Replace("\"ExtensionData\":{},", "");
                partialList = serializer.Deserialize<List<IncentiveSecondarySalesPartialDTO>>(jsonString);
            }

            //Validation
            if (String.IsNullOrEmpty(txtPlanName.Text.Trim()))
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Plan Name can not be Empty !");
                return;
            }

            //Setting Header
            header.IncentivePlanHeaderId = String.IsNullOrEmpty(HiddenFieldSelectedHeaderId.Value) ? 0 : Convert.ToInt32(HiddenFieldSelectedHeaderId.Value);
            header.PlanName = txtPlanName.Text;
            header.Description = txtDescription.Text;
            header.StartDate = Convert.ToDateTime(dtpDate.Text);
            header.EndDate = Convert.ToDateTime(dtpDateTo.Text);

            //Validation
            if (!(header.StartDate < header.EndDate))
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetErrorMessage("End date should be a date after Start Date !");
                return;
            }

            header.DivisionId = Convert.ToInt32(DropDownListDivision.SelectedValue);
            header.CreatedBy = UserSession.Instance.UserName;
            header.LastModifiedBy = UserSession.Instance.UserName;

            detail.IncentivePlanDetailsId = Session["SELECTED_DEATIL_ID"] != null ? Convert.ToInt32(Session["SELECTED_DEATIL_ID"]) : 0;
            detail.IncentivePlanHeaderId = header.IncentivePlanHeaderId;

            detail.IncentiveSecondarySalesWeeklyList = weeklyList;
            detail.IncentiveSecondarySalesPartialList = partialList;

            detailList.Add(detail);

            header.IncentivePlanDetailsList = detailList;

            headerList.Add(header);

            CommonClient commonClient = new CommonClient();

            if (headerList.Count != 0)
            {
                isSucc = commonClient.SaveIncentivePlans(headerList);
            }

            if (isSucc)
            {
                div_message.Attributes.Add("style", "display:block;padding:4px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
            }
            else
            {
                div_message.Attributes.Add("style", "display:block;padding:4px");
                div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            }

        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
        //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadTemplate('" + schedul_struct.CallCycleId.ToString() + "');", true);
        //ScriptManager.RegisterStartupScript(this, GetType(), "hideloader2", "hideloader();", true);
    }
}