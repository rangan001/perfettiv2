﻿<%@ Page Title="mSales | Customer Category" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CustomerCategory.aspx.cs" Inherits="lead_customer_master_CustomerCategory" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .toolbar_container {
            border-top: none;
            border-bottom: 0px solid #ebedf2;
            margin-bottom: 40px;
            padding: 0px 0px 7px 0px;
            height: auto;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />
    <asp:HiddenField ID="hfOriginatorString" runat="server" />
    <asp:HiddenField ID="saveDeleteOption" runat="server" />

    <asp:HiddenField ID="hdnSelectedCategoryId" runat="server" ClientIDMode="Static" />

    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Category Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtCategoryName" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="toolbar_container">
            <div class="addlinkdiv" style="float: left" id="div_address">
                <a id="id_add_category" clientidmode="Static" runat="server" href="#">Add Category</a>
            </div>
            <div class="toolbar_left" id="div_content" style="display: none;">
                <div class="hoributton">
                </div>
            </div>
            <div class="toolbar_right" id="div3" style="display: none;">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back">

            </div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="gridAllCategory"></div>

            </div>
        </div>
    </div>

    <script type="text/javascript">

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            LoadAllCategory();
            $("#txtCategoryName").focus();
        });

        function LoadAllCategory() {
            var take_grid = $("#MainContent_hfPageIndex").val();

            var url = "javascript:EditCategory('#=CategoryId#','#=CategoryName#');";

            var urlCategoryName = "<a href=\"" + url + "\">#=CategoryName#</a>";

            $("#div_loader").show();
            $("#gridAllCategory").html("");
            $("#gridAllCategory").kendoGrid({
                height: 375,
                columns: [
                    { field: "CategoryId", title: "Category Id", width: "85px", hidden: true },
                    { template: urlCategoryName, field: "CategoryName", title: "Category", width: "105px" },
                    { template: '<a href="javascript:DeleteCustomerCategory(\'#=CategoryId#\', \'#=CategoryName#\');">Delete</a>', field: "", width: "70px" }
                ],
                editable: false,
                pageable: false,
                sortable: false,
                filterable: false,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { //Define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                CategoryId: { validation: { required: true } },
                                CategoryName: { editable: false, nullable: true },
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllCustomerCategory", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#gridAllCategory").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models });
                            }
                            else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        function DeleteCustomerCategory(categoryId, categoryName) {
            debugger;
            var r = confirm("Do you want to delete this Category?");
            if (r == true) {
                DeleteCustomerCategoryByCategoryId(categoryId, categoryName);
            }
        }

        function DeleteCustomerCategoryByCategoryId(categoryId, categoryName) {
            var root = ROOT_PATH + "service/lead_customer/common.asmx/DeleteCustomerCategoryByCategoryId";

            $.ajax({
                type: "POST",
                url: root,
                data: '{ categoryId: "' + categoryId + '", categoryName: "' + categoryName + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == true) {
                        var sucessMsg = GetSuccesfullMessageDiv("Category was successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        LoadAllCategory();
                        hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });
        }

        //Load popup for Add new Brand
        $("#id_add_category").click(function () {
            $("#window").css("display", "block");
            $("#hdnSelectedCategoryId").val('0');
            $("#txtCategoryName").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Category",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtCategoryName").focus();
            wnd.center().open();
        });

        //Save button click
        $("#buttonSave").click(function () {
            SaveCustomerCategory();
        });

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        $("#buttonClear").click(function () {
            $("#hdnSelectedCategoryId").val('0');
            $("#txtCategoryName").val('');
        });

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Category",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        /*Update Region Entry Page*/
        function EditCategory(CategoryId, CategoryName) {
            $("#hdnSelectedCategoryId").val(CategoryId);
            $("#txtCategoryName").val(CategoryName);

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Category",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            $("#txtCategoryName").focus();

            wnd.center().open();
        }

        function SaveCustomerCategory() {

            var selectedCategoryId = $("#hdnSelectedCategoryId").val();

            if (selectedCategoryId == '') {
                selectedCategoryId = '0';
            }

            if (($("#txtCategoryName").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please enter a Category Name.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }
            else {
                //var brandCode = $("#txtCode").val();
                var txtCategoryName = $("#txtCategoryName").val();

                var url = ROOT_PATH + "service/lead_customer/common.asmx/InsertCustomerCategory"

                var data = '{ categoryId: "' + selectedCategoryId + '", ' +
                    ' categoryName: "' + txtCategoryName + '" }';

                console.log(data);

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    contentType: "application/json;",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == true) {
                            var sucessMsg = GetSuccesfullMessageDiv("Category was successfully Saved.", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(sucessMsg);
                            closepopup();
                            LoadAllCategory();
                            hideStatusDiv("MainContent_div_message");
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            closepopup();
                            hideStatusDiv("MainContent_div_message");
                        }
                    },
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                        }
                    }
                });
            }
        }

    </script>
</asp:Content>

