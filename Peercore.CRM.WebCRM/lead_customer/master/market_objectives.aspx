﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="market_objectives.aspx.cs" Inherits="lead_customer_master_market_objectives" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenFieldMarketID" />
    <asp:HiddenField runat="server" ID="HiddenFieldMarketName" />
    <asp:HiddenField runat="server" ID="HiddenFieldSuccessfullySaved" />
    <asp:HiddenField ID="HiddenFieldMarketObjectives" runat="server" />

    <%--    <div class="divcontectmainforms">--%>
<%--    <div id="routeTargetmodalWindow" style="display: none">
        <div id="div_routeTargetConfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>--%>

    <div id="markets_window" style="display: none">
        <div id="markets_subwindow">
        </div>
    </div>

    <div class="divcontectmainforms" style="min-height: 600px;">
        <div id="window" style="display:none; width:500px;">
            <div style="width:100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">
                                    Code
                                </td>
                                <td colspan="3">
                                <span class="wosub mand">
                                    &nbsp;<input type="text" class="textboxwidth" id="txtCode"/>
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Description
                                </td>
                                <td colspan="3">
                                <span class="wosub mand">
                                    &nbsp;<input type="text" class="textboxwidth" id="txtDescription"/>
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button" >Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                    <asp:HiddenField ID="hdnSelectedObjectiveId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
<%--                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_market_objective" clientidmode="Static" runat="server" href="#">Add Objective</a>                     
                    </div>--%>
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div id="div_message" runat="server" style="display: none;">
            </div>
            <div id="div_info" runat="server" style="display: none">
            </div>

            <div class="formleft">
                <div class="formtextdiv">
                    Market</div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtMarketName" runat="server" Width="192px"></asp:TextBox>
                </div>
                <div style="line-height:30px; margin-top:3px;">
                <a href="#" id="id_markets"> <%--id_schedule--%>
                        <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>
                <div class="clearall">
                </div>
            </div>

            <%--<div class="grid_container">
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Market
                </div>
                <div class="formdetaildiv">
                    <span class="wosub mand">
                        <asp:TextBox ID="txtMarketName" CssClass="tb" runat="server" Style="width: 200px;
                            text-transform: uppercase"></asp:TextBox>
                    </span>
                </div>
                <div class="clearall">
                </div>
            </div>--%>
            <div class="clearall">
            </div>
            <div class="grid_container" id="div_mainMarketObjectives">
                <div id="div_gridTitleMarketObjectives">
                    <%-- Objectives--%>
                    <asp:Label ID="lblObjectives" runat="server" Text="Objectives" CssClass="grid_title" Style="margin-left: 10px;"></asp:Label>
                                         <div id="div_addMarketObjButton" class="addlinkdiv" style="float: right; margin-bottom:10px;">
                        <a id="id_add_market_objective" clientidmode="Static" runat="server" href="#">Add Objective</a>
                    </div>  
                </div>
                <div class="clearall"></div>                              
                <div id="div_marketObjectives">
                </div>
            </div>
            <div class="clearall">
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $("#div_gridTitleMarketObjectives").css('display', 'none');
        $("#div_marketObjectives").css('display', 'none');

        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");

            if ($('#MainContent_HiddenFieldMarketID').val() != '') {
                $("#div_gridTitleMarketObjectives").css('display', 'block');
                $("#div_marketObjectives").css('display', 'block');
                loadMarketObjectives($('#MainContent_HiddenFieldMarketID').val());
            }

            $("#id_markets").click(function () {
                  GetAllMarketsList();
            });

            var window = $("#markets_window"),
                        undo = $("#undo")
                                .bind("click", function () {
                                    window.data("kendoWindow").open();
                                    undo.hide();
                                });

            var onClose = function () {
                undo.show();
            }

            if (!window.data("kendoWindow")) {
                window.kendoWindow({
                    width: "550px",
                    height: "390px",
                    title: "Market",
                    close: onClose
                });
            }
        
        });

        $("#MainContent_buttonbar_buttinSave").click(function () {
            entityGrid1 = $("#div_marketObjectives").data("kendoGrid");
            $("#MainContent_HiddenFieldMarketObjectives").val(JSON.stringify(entityGrid1.dataSource.view()));
        });

        //Load popup for Add new Objective
        $("#id_add_market_objective").click(function () {
            AddMarketObjectives();
        });

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Objective",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        $("#buttonClear").click(function () {
            $("#hdnSelectedObjectiveId").val('');
            $("#txtCode").val('');
            $("#txtDescription").val('');
        });

        //Save button click
        $("#buttonSave").click(function () {
            MarketObjectiveSave();
        });
        
    </script>
</asp:Content>

