﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using CRMServiceReference;
using System.Web.Script.Serialization;

public partial class lead_customer_master_market_objectives : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            //buttonbar.VisibleSave(true);

            if (!IsPostBack)
            {

            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Market Objectives ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    //protected void ButtonSave_Click(object sender)
    //{
    //    //SaveMarketObjectives();
    //}
}