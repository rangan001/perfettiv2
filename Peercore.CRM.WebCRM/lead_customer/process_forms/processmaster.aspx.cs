﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Configuration;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;

public partial class lead_customer_process_forms_processmaster : System.Web.UI.Page
{
    //ArgsDTO args;
    public const string CLIENT_COLUMN_SETTING = "CLIENT_COLUMN_SETTING";
    public const string CLEAR_FLAG = "CLEAR_FLAG";

    #region Events
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "get")
                {
                    if (Request.QueryString["fm"] == "leadentry" || Request.QueryString["fm"] == "customerentry")
                    {
                        //Load document for lead entry form.
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocument = GetDocumentContent();
                        Response.ContentType = "text/html";
                        Response.Write(getdocument);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "customeremail" || Request.QueryString["fm"] == "leademail")
                    {
                        //Load email for customerEntry,leadentry form.
                        Response.Expires = -1;
                        Response.Clear();
                        string getmailattachment = GetEmailAttachment();
                        Response.ContentType = "text/html";
                        Response.Write(getmailattachment);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "customeremailcontent")
                    {
                        //Load email for customerEntry form.
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetEmailContent();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "contactpersonaddress")
                    {
                        //Load email for customerEntry form.
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetMaillingAddress();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }

                }
                else if (Request.QueryString["type"] == "Update")
                {
                    if (Request.QueryString["fm"] == "leadCustomer")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string saveHeaderText = UpdateHeaderText();
                        Response.ContentType = "text/html";
                        //Response.Write(saveHeaderText);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "custenduser")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string transferenduser = TransferEndUser("");
                        Response.ContentType = "text/html";
                        Response.Write(transferenduser);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "transferenduser")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string transferenduser = SaveTransferEndUser();
                        Response.ContentType = "text/html";
                        Response.Write(transferenduser);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "Delete")
                {
                    if (Request.QueryString["fm"] == "document")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteDocument = DeleteLeadDocument(Request.QueryString["id"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(deleteDocument);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "Opportunity")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteLeadstage = DeleteOpportunity(Request.QueryString["id"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(deleteLeadstage);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "UnassignedRoutefromRep")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteLeadstage = UnassignedRoutefromRep(Request.QueryString["id"].ToString(), Request.QueryString["repcode"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(deleteLeadstage);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "deletecustomer")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteCustomerByCusCode();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "deleteroutecustomer")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteRouteCustomerByCusCode();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "deleteaddcustomerroute")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteAddCustomerRoute();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "deletebulkcust")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteBulkCust(Request.QueryString["val"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "custenduser")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = LoadNewCustomer();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "insert")
                {
                    if (Request.QueryString["fm"] == "contactdetails")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = InsertContactDetailsForm("");
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }

                    else if (Request.QueryString["fm"] == "bankdetails")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = InsertBankDetailsForm("");
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "addcontactdetails")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = InsertContactDetails();
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "addbankdetails")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = InsertBankDetails();
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "leaddocument" || Request.QueryString["fm"] == "custdocument" || Request.QueryString["fm"] == "enduerdocument")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = SaveCustomerDocument();
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "contactpersonimagesave")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = UploadProfilePic();
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "marketobjectives")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string Oppportunity = SaveMarketObjective();
                        Response.ContentType = "text/html";
                        Response.Write(Oppportunity);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "lookup")
                {

                }
                else if (Request.QueryString["type"] == "select")
                {
                    if (Request.QueryString["fm"].ToLower() == "distributorentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetUsersByCatergory();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "repentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetRepsList();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "allrepentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetAllRepsList();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"].ToLower() == "alldistributorentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetAllDistributorList();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "report")
                {
                    if (Request.QueryString["fm"] == "endusersales")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getleadstage = SetEnduserSalesData(Request.QueryString["month"].ToString(), Request.QueryString["year"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(getleadstage);
                        Response.End();
                    }

                    else if (Request.QueryString["fm"] == "debtorssales")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getleadstage = GetDebtorsSales(Request.QueryString["id"].ToString(), Request.QueryString["tdiv"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(getleadstage);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "customersales")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getleadstage = GetCustomerSales(Request.QueryString["id"].ToString(), Request.QueryString["tdiv"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(getleadstage);
                        Response.End();
                    }
                    else if (Request.QueryString["fm"] == "customerenduval")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        ArgsDTO args = new ArgsDTO();
                        EndUserClient endUserClient = new EndUserClient();
                        args.CustomerCode = Request.QueryString["custid"].ToString();
                        args.EnduserCode = Request.QueryString["id"].ToString();
                        string enduserid = "";
                        if (!string.IsNullOrEmpty(args.EnduserCode))
                        {
                            EndUserDTO endUserDTO = ValidateEndUserForThisCustomer(args);
                            List<EndUserDTO> listEndUser = ValidateEndUserForOtherCustomers(args);

                            
                            if (!string.IsNullOrEmpty(endUserDTO.Name))
                            {
                                enduserid = "This End User already exists as " + endUserDTO.Name + "for the current distributor.";
                            }
                            if (listEndUser != null && listEndUser.Count > 0)
                            {
                                if (enduserid != "")
                                    enduserid += Environment.NewLine;

                                enduserid += "End user code entered has been saved against distributor(s)..." + Environment.NewLine;
                                //foreach (EndUser endUser in listEndUser)
                                //    msg += "as '" + endUser.Name + "' for '" + endUser.CustomerName + "'" + Environment.NewLine;

                                for (int i = 0; i < listEndUser.Count; i++)
                                {
                                    enduserid += " as '" + listEndUser[i].Name + "' for '" + listEndUser[i].CustomerName + "'" + Environment.NewLine;

                                }

                            }


                        }
                        Response.ContentType = "text/html";
                        Response.Write(enduserid);
                        Response.End();
                    } 
                    else if (Request.QueryString["fm"] == "othercustomerenduval")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        ArgsDTO args = new ArgsDTO();
                        EndUserClient endUserClient = new EndUserClient();
                        args.CustomerCode = Request.QueryString["custid"].ToString();
                        args.EnduserCode = Request.QueryString["id"].ToString();
                        List<EndUserDTO> listEndUser = ValidateEndUserForOtherCustomers(args);
                        string msg = "";
                        if (listEndUser != null && listEndUser.Count > 0)
                        {
                            if (msg != "")
                                msg += Environment.NewLine;

                            msg += "End user code entered has been saved against distributor(s)..." + Environment.NewLine;
                            //foreach (EndUser endUser in listEndUser)
                            //    msg += "as '" + endUser.Name + "' for '" + endUser.CustomerName + "'" + Environment.NewLine;

                            for (int i = 0; i < listEndUser.Count; i++)
                            {
                                msg += " as '" + listEndUser[i].Name + "' for '" + listEndUser[i].CustomerName + "'" + Environment.NewLine;

                            }

                        }
                        Response.ContentType = "text/html";
                        Response.Write(msg);
                        Response.End();

                    }
                    else if (Request.QueryString["type"] == "saveXML")
                    {
                        saveXMLFile();
                    }
                    else if (Request.QueryString["type"] == "LoadState")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string txtLoadState = LoadState(Request.QueryString["leadstage"]);
                        Response.ContentType = "text/html";
                        Response.Write(txtLoadState);
                        Response.End();
                    }
                }
            }
        }
    }
    #endregion

    #region Methods

    private string DeleteCustomerByCusCode()
    {
        try
        {
            string cusCode = string.Empty;
            CommonClient commService = new CommonClient();
            CustomerDTO delCustomer = new CustomerDTO();

            if (Request.QueryString["cusno"] != null && Request.QueryString["cusno"] != "")
            {
                cusCode = Request.QueryString["cusno"].ToString();
            }

            delCustomer.CustomerCode = cusCode;
            delCustomer.CreatedBy = UserSession.Instance.UserName;
            bool status = commService.DeleteCustomerByCusCode(delCustomer);

            //int index = productcategoryList.FindIndex(x => x.Id == Id);
            //productcategoryList.RemoveAt(index);
            //Session[CommonUtility.ORIGINATOR_MARKETS] = productcategoryList;
            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private string DeleteRouteCustomerByCusCode()
    {
        try
        {
            string cusCode = string.Empty;
            int routeid = 0;
            CommonClient commService = new CommonClient();
            CustomerDTO delCustomer = new CustomerDTO();

            if (Request.QueryString["cusno"] != null && Request.QueryString["cusno"] != "")
            {
                cusCode = Request.QueryString["cusno"].ToString();
            }

            if (Request.QueryString["routeid"] != null && Request.QueryString["routeid"] != "")
            {
                routeid = int.Parse(Request.QueryString["routeid"].ToString());
            }

            delCustomer.CustomerCode = cusCode;
            delCustomer.RouteAssignId = routeid;
            delCustomer.CreatedBy = UserSession.Instance.UserName;
            bool status = commService.DeleteCustomerByCusCode(delCustomer);

            //int index = productcategoryList.FindIndex(x => x.Id == Id);
            //productcategoryList.RemoveAt(index);
            //Session[CommonUtility.ORIGINATOR_MARKETS] = productcategoryList;
            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private string DeleteAddCustomerRoute()
    {
        try
        {
            string cusCode = string.Empty;
            int routeid = 0;
            CommonClient commService = new CommonClient();
            CustomerDTO delCustomer = new CustomerDTO();

            if (Request.QueryString["cusno"] != null && Request.QueryString["cusno"] != "")
            {
                cusCode = Request.QueryString["cusno"].ToString();
            }

            delCustomer.CustomerCode = cusCode;
            delCustomer.RouteAssignId = routeid;
            delCustomer.CreatedBy = UserSession.Instance.UserName;
            bool status = commService.DeleteAddCustomerRoute(delCustomer);

            //int index = productcategoryList.FindIndex(x => x.Id == Id);
            //productcategoryList.RemoveAt(index);
            //Session[CommonUtility.ORIGINATOR_MARKETS] = productcategoryList;
            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

    private List<EndUserDTO> ValidateEndUserForOtherCustomers(ArgsDTO args)
    {
        EndUserClient endUserClient = new EndUserClient();
        if (!string.IsNullOrWhiteSpace(args.CustomerCode) && !string.IsNullOrWhiteSpace(args.EnduserCode))
        {
            //ArgsDTO args = new ArgsDTO();
            //args.CustomerCode = HiddenFieldCustomerCode.Value;
            //args.EnduserCode = HiddenEndUserCode.Value;
            return endUserClient.IsEndUserExistForOtherCustomers(args);
        }

        return null;
    }

    private EndUserDTO ValidateEndUserForThisCustomer(ArgsDTO args)
    {
        EndUserClient endUserClient = new EndUserClient();
        if (!string.IsNullOrWhiteSpace(args.CustomerCode) && !string.IsNullOrWhiteSpace(args.EnduserCode))
        {

            return endUserClient.IsEndUserExistForCustomer(args);
        }

        return null;
    }

    private string UpdateHeaderText()
    {
        StringBuilder txt = new StringBuilder();

        string sFileName = ConfigUtil.ApplicationPath + "docs/" + "CustomerState";
        List<ColumnSettingDTO> Settings = new List<ColumnSettingDTO>();
        CommonUtility commonUtility = new CommonUtility();
        bool save = false;

        try
        {
            string headerSelectedName = Request.QueryString["selected"];
            string[] splitSelectedName = headerSelectedName.Split(new Char[] { ',' });

            string headerNotSelectedName = Request.QueryString["notselected"];
            string[] splitNotSelectedName = headerNotSelectedName.Split(new Char[] { ',' });

            if (splitNotSelectedName.Length > 0 || splitNotSelectedName.Length > 0)
            {
                for (int i = 0; i < splitNotSelectedName.Length; i++)
                {
                    if (splitNotSelectedName[i] != "")
                    {
                        ColumnSettingDTO setting = new ColumnSettingDTO();
                        setting.propertyName = splitNotSelectedName[i].ToString();
                        setting.uniqueName = splitNotSelectedName[i].ToString();
                        setting.header = splitNotSelectedName[i].ToString();
                        setting.visible = false;
                        Settings.Add(setting);
                    }
                }

                for (int i = 0; i < splitSelectedName.Length; i++)
                {
                    if (splitSelectedName[i] != "")
                    {
                        ColumnSettingDTO setting = new ColumnSettingDTO();
                        setting.propertyName = splitSelectedName[i].ToString();
                        setting.uniqueName = splitSelectedName[i].ToString();
                        setting.header = splitSelectedName[i].ToString();
                        setting.visible = true;
                        Settings.Add(setting);
                    }
                }
            }

            if (sFileName == "")
                Save();
            else
            {
                save = Save(sFileName, Settings);
                if (save == true)
                {
                    txt.Append(commonUtility.GetSucessfullMessage("Information Successfully Saved."));
                }
                else
                {
                    txt.Append(commonUtility.GetErrorMessage("Information Not Successfully Saved."));
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        return txt.ToString();
    }

    public void Save()
    {
        FileStream stream = new FileStream(Server.MapPath("GridSettings.xml"), FileMode.Create);
    }

    public bool Save(string sFileName, List<ColumnSettingDTO> Settings)
    {
        CommonClient serviceCommon = new CommonClient();
        ColumnSettingDTO ColumnSettingEntity = new ColumnSettingDTO();
        List<ColumnSettingDTO> lstColumn = new List<ColumnSettingDTO>();
        bool save = false;

        const string SORT_ORDER = "SORT_ORDER";
        const string SORT_NAME = "SORT_NAME";

        try
        {
            ColumnSettingEntity.originator = UserSession.Instance.UserName;
            ColumnSettingEntity.gridName = Request.QueryString["leadstage"].ToLower() == "enduser" ? "Enduser" : "LeadAndCustomer";
            ColumnSettingEntity.userControl = "LeadAndCustomerUserControl";
            if (Session[SORT_ORDER] != null && Session[SORT_NAME] != null)
            {
                ColumnSettingEntity.sortName = Convert.ToString(Session[SORT_NAME]);
                ColumnSettingEntity.sortType = Convert.ToString(Session[SORT_ORDER]);
            }

            foreach (ColumnSettingDTO column in Settings)
            {
                lstColumn.Add(column);
            }
            ColumnSettingEntity.lstColumnSetting = lstColumn.ToList();

            save = serviceCommon.InserColumnSetting(ColumnSettingEntity);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return save;
    }

    private void saveXMLFile()
    {
        try
        {

            const string SORT_ORDER = "SORT_ORDER";
            const string SORT_NAME = "SORT_NAME";

            const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
            const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";

            List<ColumnSettingDTO> Setting = new List<ColumnSettingDTO>();

            string headerSelectedName = Request.QueryString["selected"];
            string[] splitSelectedName = headerSelectedName.Split(new Char[] { ',' });

            string headerNotSelectedName = Request.QueryString["notselected"];
            string[] splitNotSelectedName = headerNotSelectedName.Split(new Char[] { ',' });

            if (splitNotSelectedName.Length > 0 || splitNotSelectedName.Length > 0)
            {
                for (int i = 0; i < splitNotSelectedName.Length; i++)
                {
                    if (splitNotSelectedName[i] != "")
                    {
                        ColumnSettingDTO objsetting = new ColumnSettingDTO();
                        objsetting.propertyName = splitNotSelectedName[i].ToString();
                        objsetting.uniqueName = splitNotSelectedName[i].ToString();
                        objsetting.header = splitNotSelectedName[i].ToString();
                        if (Session[SORT_ORDER] != null && Session[SORT_NAME] != null)
                        {
                            objsetting.sortName = Convert.ToString(Session[SORT_NAME]);
                            objsetting.sortType = Convert.ToString(Session[SORT_ORDER]);
                        }
                        else if (Session[CLIENT_SORT_ORDER] != null && Session[CLIENT_SORT_NAME] != null)
                        {
                            objsetting.sortName = Convert.ToString(Session[CLIENT_SORT_NAME]);
                            objsetting.sortType = Convert.ToString(Session[CLIENT_SORT_ORDER]);
                        }
                        else
                        {
                            objsetting.sortName = "Name";
                            objsetting.sortType = "A";
                        }
                        objsetting.visible = false;
                        Setting.Add(objsetting);
                    }
                }

                for (int i = 0; i < splitSelectedName.Length; i++)
                {
                    if (splitSelectedName[i] != "")
                    {
                        ColumnSettingDTO objsetting = new ColumnSettingDTO();
                        objsetting.propertyName = splitSelectedName[i].ToString();
                        objsetting.uniqueName = splitSelectedName[i].ToString();
                        objsetting.header = splitSelectedName[i].ToString();
                        if (Session[SORT_ORDER] != null && Session[SORT_NAME] != null)
                        {
                            objsetting.sortName = Convert.ToString(Session[SORT_NAME]);
                            objsetting.sortType = Convert.ToString(Session[SORT_ORDER]);
                        }
                        else if (Session[CLIENT_SORT_ORDER] != null && Session[CLIENT_SORT_NAME] != null)
                        {
                            objsetting.sortName = Convert.ToString(Session[CLIENT_SORT_NAME]);
                            objsetting.sortType = Convert.ToString(Session[CLIENT_SORT_ORDER]);
                        }
                        else
                        {
                            objsetting.sortName = "Name";
                            objsetting.sortType = "A";
                        }
                        objsetting.visible = true;
                        Setting.Add(objsetting);
                    }
                }
            }
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AddHeader("Content-Disposition", "attachment; filename = DefaulTableSettings.xml");
            Response.ContentType = "text/xml";
            XmlSerializer x = new XmlSerializer(typeof(List<ColumnSettingDTO>));
            x.Serialize(Response.OutputStream, Setting);
            Response.End();
        }
        catch (Exception)
        {
            throw;
        }
    }

    public string LoadState(string leadstage)
    {
        StringBuilder selected = new StringBuilder();
        StringBuilder notSelected = new StringBuilder();

        List<ColumnSettingDTO> config1 = Session[CLIENT_COLUMN_SETTING] as List<ColumnSettingDTO>;
        List<ColumnSettingDTO> config = new List<ColumnSettingDTO>();

        CommonClient serviceCommon = new CommonClient();
        ColumnSettingDTO ColumnSettingDTO = new ColumnSettingDTO();

        try
        {
            if (Convert.ToBoolean(Session[CLEAR_FLAG]) == true)
            {
                notSelected.Append("");
            }
            else
            {
                if (config1 != null)
                {
                    foreach (ColumnSettingDTO objc in config1)
                    {
                        if (objc.visible == true)
                        {
                            selected.Append(objc.propertyName + ",");
                        }
                        else
                        {
                            notSelected.Append(objc.propertyName + ",");
                        }
                    }
                }
                else
                {
                    ColumnSettingDTO = serviceCommon.GetColumnSetting(UserSession.Instance.UserName,leadstage.ToLower() == "enduser" ?"Enduser":"LeadAndCustomer");
                    if (ColumnSettingDTO.lstColumnSetting != null)
                    {
                        config = ColumnSettingDTO.lstColumnSetting.ToList();
                    }
                    if (config.Count > 0 || config != null)
                    {
                        foreach (ColumnSettingDTO objc in config)
                        {
                            if (objc.visible == true)
                            {
                                selected.Append(objc.propertyName + ",");
                            }
                            else
                            {
                                notSelected.Append(objc.propertyName + ",");
                            }
                        }
                    }
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        Session[CLIENT_COLUMN_SETTING] = null;
        Session[CLEAR_FLAG] = null;
        return notSelected.ToString();
    }

    private string SetEnduserSalesData(string month, string year)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            ArgsDTO argsDTO = (ArgsDTO)Session[CommonUtility.GLOBAL_SETTING];

            argsDTO.SMonth = int.Parse(month);
            argsDTO.SYear = int.Parse(year);
            Session[CommonUtility.GLOBAL_SETTING] = argsDTO;

        }
        catch (Exception ex)
        {
            //sb.Append("Error");
        }
        return sb.ToString();
    }
    private string GetDebtorsSales(string customerCode, string tagertdiv)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            List<EndUserSalesDTO> debtorsBarometerList = null;
            
                if (!string.IsNullOrWhiteSpace(customerCode))
                    debtorsBarometerList = new SalesClient().GetDebtorsDetails(customerCode);

                //if (debtorsBarometerList != null)
                //{
                //    if (debtorsBarometerList.Count!=0)
                        sb.Append(RenderDebtorsSales(customerCode, tagertdiv, debtorsBarometerList));
                //}
        }
        catch (Exception ex)
        {
            //sb.Append("Error");
        }
        return sb.ToString();
    }

    private string GetCustomerSales(string customerCode, string tagertdiv)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            EndUserSalesDTO salesGraphData = null;

            string date = DateTime.Today.ToUniversalTime().ToString(ConfigUtil.DatePatternForSql);
            salesGraphData = new SalesClient().GetSalesData(customerCode, date);

            if (salesGraphData != null)
                sb.Append(RenderSalesGraphs(tagertdiv, salesGraphData));
        }
        catch (Exception ex)
        {
            //sb.Append("Error");
        }
        return sb.ToString();
    }

    private string RenderDebtorsSales(string customerCode, string tagertdiv, List<EndUserSalesDTO> endUserSalesDTO)
    {
        StringBuilder txt = new StringBuilder();

        #region Setting Data.
        decimal dCur = 0, d30 = 0, d60 = 0, d90 = 0, dOver = 0;
        DateTime Ld_datenow = DateTime.Today;
        DateTime Ld_as_at_date = DateTime.Today;
        DateTime Ld_datethen = DateTime.Today;
        DateTime Ld_doc_date = DateTime.Today;
        TimeSpan Ld_datediff;
        decimal dTotal = 0;
        int Li_TransType = 0;
        string Lv_Status = "";
        decimal Lv_UnallocDiscount = 0;

        foreach (EndUserSalesDTO item in endUserSalesDTO)
        {
            /* ADD DISC IF RCT IS UNALLOCATED */
            if (Li_TransType == 0 && Lv_Status == "")
                item.DueAmount += Lv_UnallocDiscount;

            // FOR LEDGER TYPE - 'MON'
            Ld_datenow = new DateTime(Ld_as_at_date.Year, Ld_as_at_date.Month, 1);

            if (item.GracePeriod != 0)
            {
                //if due_date_rad = 1 THEN 
                Ld_datethen = new DateTime(Ld_doc_date.Year, item.Documentdate.Month, 1);
                Ld_datediff = Ld_datenow - Ld_datethen;

            }
            else
            {
                Ld_datethen = new DateTime(Ld_doc_date.Year, item.Documentdate.Month, 1);
                Ld_datediff = Ld_datenow - Ld_datethen;
            }

            if (Ld_datediff.Days <= 0)
                dCur += item.DueAmount;
            else if (Ld_datediff.Days > 20 && Ld_datediff.Days < 40)
                d30 += item.DueAmount;
            else if (Ld_datediff.Days > 50 && Ld_datediff.Days < 70)
                d60 += item.DueAmount;
            else if (Ld_datediff.Days > 80 && Ld_datediff.Days < 100)
                d90 += item.DueAmount;
            else
                dOver += item.DueAmount;

            dTotal += item.DueAmount;
        }
        
        #endregion

        txt.Append("<script language=\"javascript\" type=\"text/javascript\">");
        txt.Append(" var chart;");
        txt.Append(" $(document).ready(function () {");

        txt.Append(" chart = new Highcharts.Chart({");
        txt.Append(" chart: {");
        txt.Append(" renderTo: '" + tagertdiv + "',");
        txt.Append(" type: 'column'");
        txt.Append(" },");
        txt.Append(" title: {");
        txt.Append(" text: ''");
        txt.Append(" },");
        txt.Append(" xAxis: {");

        txt.Append(" title: {");
        txt.Append(" text: 'Days'");
        txt.Append(" },");

        txt.Append(" categories: ['CUR', '30', '60', '90', 'OVER']");
        txt.Append(" },");
        txt.Append(" yAxis: {");

        txt.Append(" min: 10,");
        txt.Append(" title: {");
        txt.Append(" text: 'Outstanding$'");
        txt.Append(" },");
        txt.Append(" stackLabels: {");
        txt.Append(" enabled: true,");
        txt.Append(" style: {");
        txt.Append(" fontWeight: 'bold',");
        txt.Append(" color: 'gray',");
        txt.Append(" backgroundColor: 'red'");
        txt.Append(" }");

        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" legend: {");
        txt.Append(" enabled: false");
        txt.Append(" },");
        txt.Append(" tooltip: {");
        txt.Append(" formatter: function () {");
        txt.Append(" return '' +");
        txt.Append(" this.x + ': ' + Highcharts.numberFormat(this.y, 2);");
        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" plotOptions: {");
        txt.Append(" column: {");
        txt.Append(" pointPadding: 0.2,");
        txt.Append(" borderWidth: 0");
        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" series: [{");
        txt.Append(" name: 'Salesqq',");
        txt.Append(" data: [{ y: " + (dCur != null ? dCur : 0) + ", color: '#4E9258' }, { y: " + (d30 != null ? d30 : 0)
            + ", color: '#4E9258' }, { y: " + (d60 != null ? d60 : 0) + ", color: '#FF6600' }");
        txt.Append(" , { y: " + (d90 != null ? d90 : 0) + ", color: '#FF6600' }, { y: " + (dOver != null ? dOver : 0) + ", color: '#FF6600'}],");
        txt.Append(" dataLabels: {");
        txt.Append(" enabled: false, rotation: -90, color: '#FFFFFF', align: 'right', x: -3, y: 10,");
        txt.Append(" formatter: function () {");
        txt.Append(" return this.y;");
        txt.Append(" },");
        txt.Append(" style: {");
        txt.Append(" fontSize: '13px',");
        txt.Append(" fontFamily: 'Verdana, sans-serif'");
        txt.Append(" }");
        txt.Append(" }");
        txt.Append(" }]");
        txt.Append(" });");
        txt.Append(" });");
        txt.Append(" ");
        txt.Append(" ");

        txt.Append("</script>");

        txt.Append("<script language=\"javascript\" type=\"text/javascript\">");
        txt.Append("$(document).ready(function(){");
        txt.Append("$(\"#lbDebtorsTotal\").html(\"Total($): " + dTotal.ToString() + "\");");
        txt.Append("});");
        txt.Append("</script>");
        return txt.ToString();
    }

    private string RenderSalesGraphs(string tagertdiv, EndUserSalesDTO endUserSalesDTO)
    {
        string istonnechecked = "t";
        double totalsale = 0;
        string tonetext = "(Tonnes)";

        if (Request.QueryString["istonne"] != null && Request.QueryString["istonne"] != string.Empty)
            istonnechecked = Request.QueryString["istonne"].ToString();

        if (istonnechecked == "t")
        {
            totalsale = endUserSalesDTO.Tonnes1 + endUserSalesDTO.Tonnes2 + endUserSalesDTO.Tonnes3;
        }
        else if (istonnechecked == "r")
        {
            totalsale = Convert.ToDouble(endUserSalesDTO.Dollar1) + Convert.ToDouble(endUserSalesDTO.Dollar2) + Convert.ToDouble(endUserSalesDTO.Dollar3);
            tonetext = "(Dollar)";
        }
        
        StringBuilder txt = new StringBuilder();
        txt.Append("<script language=\"javascript\" type=\"text/javascript\">");
        txt.Append(" var chart;");
        txt.Append(" $(document).ready(function () {");

        txt.Append(" chart = new Highcharts.Chart({");
        txt.Append(" chart: {");
        txt.Append(" renderTo: 'monthSales_container',");
        txt.Append(" type: 'column'");
        txt.Append(" },");
        txt.Append(" title: {");
        txt.Append(" text: ''");
        txt.Append(" },");
        txt.Append(" xAxis: {");
        txt.Append(" categories: ['" + endUserSalesDTO.XLabel_FirstMonth + "', '" + endUserSalesDTO.XLabel_SecondMonth + "', '" + endUserSalesDTO.XLabel_ThirdMonth + "']");
        txt.Append(" },");
        txt.Append(" yAxis: {");

        txt.Append(" min: 0,");
        txt.Append(" title: {");
        txt.Append(" text: 'Sales'");
        txt.Append(" },");
        txt.Append(" stackLabels: {");
        txt.Append(" enabled: true,");
        txt.Append(" style: {");
        txt.Append(" fontWeight: 'bold',");
        txt.Append(" color: 'gray',");
        txt.Append(" backgroundColor: 'red'");
        txt.Append(" }");

        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" legend: {");
        txt.Append(" enabled: false");
        txt.Append(" },");
        txt.Append(" tooltip: {");
        txt.Append(" formatter: function () {");
        txt.Append(" return '' +");
        txt.Append(" this.x + ': ' + Highcharts.numberFormat(this.y, 2);");
        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" plotOptions: {");
        txt.Append(" column: {");
        txt.Append(" pointPadding: 0.2,");
        txt.Append(" borderWidth: 0");
        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" series: [{");
        txt.Append(" name: 'Salesqq',");
        txt.Append(" data: [{ y: " + (istonnechecked == "t" ? endUserSalesDTO.Tonnes1.ToString() : endUserSalesDTO.Dollar1.ToString()) + ", color: '#003399' }, { y: " + (istonnechecked == "t" ? endUserSalesDTO.Tonnes2.ToString() : endUserSalesDTO.Dollar2.ToString())
            + ", color: '#FFFF66' }, { y: " + (istonnechecked == "t" ? endUserSalesDTO.Tonnes3.ToString() : endUserSalesDTO.Dollar3.ToString()) + ", color: '#FF0000' }");
        txt.Append("  ],");
        txt.Append(" dataLabels: {");
        txt.Append(" enabled: false, rotation: -90, color: '#FFFFFF', align: 'right', x: -3, y: 10,");
        txt.Append(" formatter: function () {");
        txt.Append(" return this.y;");
        txt.Append(" },");
        txt.Append(" style: {");
        txt.Append(" fontSize: '13px',");
        txt.Append(" fontFamily: 'Verdana, sans-serif'");
        txt.Append(" }");
        txt.Append(" }");
        txt.Append(" }]");
        txt.Append(" });");
        txt.Append(" });");
        txt.Append(" ");
        txt.Append(" ");
        txt.Append("</script>");

        txt.Append("<script language=\"javascript\" type=\"text/javascript\">");
        txt.Append("$(document).ready(function(){");
        txt.Append("$(\"#lbSalesSummary\").html(\"Avg. Wk. Sales: " + string.Format("{0:0.00}", totalsale / 13) + "\");");
        txt.Append("$(\"#lbTotSales\").html(\"" + tonetext + "\");");
        
        txt.Append("});");
        txt.Append("</script>");

        return txt.ToString();
    }

    private string InsertContactDetailsForm(string errormessage, bool hasError = false)
    {//OriginatorLookup
        StringBuilder txt = new StringBuilder();

        string type = string.IsNullOrEmpty(Request.QueryString["typ"]) ? "" : Request.QueryString["typ"];
        string address1 = string.IsNullOrEmpty(Request.QueryString["add1"]) ? "" : Request.QueryString["add1"];
        string address2 = string.IsNullOrEmpty(Request.QueryString["add2"]) ? "" : Request.QueryString["add2"];
        string City = string.IsNullOrEmpty(Request.QueryString["cty"]) ? "" : Request.QueryString["cty"];
        string State = string.IsNullOrEmpty(Request.QueryString["sta"]) ? "" : Request.QueryString["sta"];
        string Postcode = string.IsNullOrEmpty(Request.QueryString["pstcde"]) ? "" : Request.QueryString["pstcde"];
        string Country = string.IsNullOrEmpty(Request.QueryString["cntry"]) ? "" : Request.QueryString["cntry"];
        string Name = string.IsNullOrEmpty(Request.QueryString["name"]) ? "" : Server.UrlDecode(Request.QueryString["name"]);
        string AddContact = string.IsNullOrEmpty(Request.QueryString["adcntac"]) ? "" : Request.QueryString["adcntac"];
        string AddTelephone = string.IsNullOrEmpty(Request.QueryString["adtele"]) ? "" : Request.QueryString["adtele"];
        string AddFax = string.IsNullOrEmpty(Request.QueryString["adfax"]) ? "" : Request.QueryString["adfax"];

        string CustomerCode = string.IsNullOrEmpty(Request.QueryString["custid"]) ? "" : Server.UrlDecode(Request.QueryString["custid"]);
        string LeadId = string.IsNullOrEmpty(Request.QueryString["leadid"]) ? "" : Request.QueryString["leadid"];
        string AssigneeNo = string.IsNullOrEmpty(Request.QueryString["assinto"]) ? "0" : Request.QueryString["assinto"];
        string LeadAddresID = string.IsNullOrEmpty(Request.QueryString["leadadid"]) ? "0" : Request.QueryString["leadadid"];

        string ms = string.IsNullOrEmpty(Request.QueryString["ms"]) ? "0" : Request.QueryString["ms"];


        txt.Append("<div id=\"contactentry\" width=\"380px\">");

        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
            txt.Append("<div id=\"div_text\" rel=\"0\">");
        else
            txt.Append("<div id=\"div_text\">");

        txt.Append("<table border=\"0\">");
        //
        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
        {
            CommonUtility utility = new CommonUtility();
            txt.Append("<tr rel=\"0\"><td colspan=\"5\">" + utility.GetErrorMessage(errormessage) + "</td></tr>");
        }

        //txt.Append("<tr><td class=\"textalignbottom\">Type</td><td colspan=\"3\">");

        //if (type.Equals("Postal"))
        //{
        //    if (!string.IsNullOrWhiteSpace(CustomerCode))
        //    {
        //        //This is only for customer address.
        //        txt.Append("&nbsp;<select id=\"cmbAddressType\" name=\"D1\" disabled=\"disabled\">");
        //        txt.Append("<option>Street Address</option>");
        //        txt.Append("<option selected=\"selected\">Postal Address</option>");
        //    }
        //    else
        //    {
        //        txt.Append("&nbsp;<select id=\"cmbAddressType\" name=\"D1\">");
        //        txt.Append("<option>Street Address</option>");
        //        txt.Append("<option selected=\"selected\">Postal Address</option>");
        //    }
        //}
        //else if (type.Equals("Street"))
        //{
        //    if (!string.IsNullOrWhiteSpace(CustomerCode))
        //    {
        //        //This is only for customer address.
        //        txt.Append("&nbsp;<select id=\"cmbAddressType\" name=\"D1\" disabled=\"disabled\">");
        //        txt.Append("<option  selected=\"selected\">Street Address</option>");
        //        txt.Append("<option>Postal Address</option>");
        //    }
        //    else
        //    {
        //        txt.Append("&nbsp;<select id=\"cmbAddressType\" name=\"D1\">");
        //        txt.Append("<option  selected=\"selected\">Street Address</option>");
        //        txt.Append("<option>Postal Address</option>");
        //    }
        //}
        //else
        //{
        //    txt.Append("&nbsp;<select id=\"cmbAddressType\" name=\"D1\">");
        //    txt.Append("<option selected=\"selected\">Street Address</option>");
        //    txt.Append("<option>Postal Address</option>");
        //}
        //txt.Append("</select></td></tr>");

        txt.Append("<tr><td class=\"textalignbottom\">Address 1</td><td colspan=\"3\">&nbsp;<input id=\"txtAddress1\" type=\"text\" class=\"textboxwidth\" Value=\"" + address1 + "\"/></td> " + (ms == "1" ? "Please Enter Address1." : "") + " </tr>");
        txt.Append("<tr><td class=\"textalignbottom\">Address 2</td><td colspan=\"3\">&nbsp;<input id=\"txtAddress2\" type=\"text\" class=\"textboxwidth\" Value=\"" + address2 + "\"/></td></tr>");
        txt.Append("<tr><td class=\"textalignbottom\">City</td><td colspan=\"3\">&nbsp;<input id=\"txtCity\" type=\"text\" class=\"textboxwidth\" Value=\"" + City + "\"/></td></tr>");
        txt.Append("<tr><td class=\"textalignbottom\">State</td><td>&nbsp;<select id=\"txtState\" name=\"D1\" class=\"dropdownwidth\">");

        if (string.IsNullOrWhiteSpace(State))
            txt.Append("<option selected=\"selected\"></option>");
        else
            txt.Append("<option></option>");
        CommonClient commonClient = new CommonClient();
        List<StateDTO> stateList = commonClient.GetAllStates();
        foreach (StateDTO item in stateList)
        {
            if (item.StateName.Equals(State.Trim()))
                txt.Append("<option selected=\"selected\">" + item.StateName + "</option>");
            else
                txt.Append("<option>" + item.StateName + "</option>");

        }
        txt.Append("</select></td><td class=\"textalignbottom\">Post Code</td><td>&nbsp;<input id=\"txtPostcode\" maxlength=\"4\" type=\"text\" Value=\"" + Postcode + "\" class=\"smltextwidth\"/></td></tr>");

        txt.Append("<tr><td class=\"textalignbottom\">Country</td><td colspan=\"3\">&nbsp;<select id=\"cmbCountry\" name=\"D1\" class=\"cntydropwidth\">");

        if (string.IsNullOrWhiteSpace(Country))
            txt.Append("<option selected=\"selected\"></option>");
        else
            txt.Append("<option></option>");
        List<CountryDTO> countryList = commonClient.GetCountry();
        foreach (CountryDTO item in countryList)
        {
            if (item.CountryName.Equals(Country))
                txt.Append("<option selected=\"selected\">" + item.CountryName + "</option>");
            else
                txt.Append("<option>" + item.CountryName + "</option>");
        }
        txt.Append("</select></td></tr>");

        txt.Append("<tr><td class=\"textalignbottom\">Name</td><td colspan=\"3\">&nbsp;<input id=\"txtName\" type=\"text\" class=\"textboxwidth\" Value=\"" + Name + "\"/></td></tr>");
        txt.Append("<tr><td class=\"textalignbottom\">Contact</td><td colspan=\"3\">&nbsp;<input id=\"txtAddContact\" class=\"textboxwidth\" type=\"text\" Value=\"" + AddContact + "\"/></td></tr>");
        txt.Append("<tr><td class=\"textalignbottom\">Telephone</td><td>&nbsp;<input id=\"txtAddTelephone\" onkeypress=\"return isNumber(event)\" type=\"text\" class=\"smltextwidth\" Value=\"" + AddTelephone + "\"/></td><td class=\"textalignbottom\">Fax</td><td>&nbsp;<input id=\"txtAddFax\" onkeypress=\"return isNumber(event)\" type=\"text\" class=\"smltextwidth\" Value=\"" + AddFax + "\"/></td></tr>");
        txt.Append("<tr><td class=\"textalignbottom\" colspan =\"4\"> <button id=\"jqi_state0_buttonSave\" class=\"k-button\">Save</button> <button id=\"jqi_state0_buttonClear\" class=\"k-button\">Clear</button> </td></tr>");

        txt.Append("</table>");

        txt.Append("<script type='text/javascript'>");
        txt.Append("$('#jqi_state0_buttonClear').click(function () {");

        txt.Append("$('#txtAddress1').val('');");
        txt.Append("$('#txtAddress2').val('');");
        txt.Append("$('#txtCity').val('');");
        txt.Append("$('#txtState').val('');");

        txt.Append("$('#txtPostcode').val('');");
        txt.Append("$('#cmbCountry').val('');");
        txt.Append("$('#txtName').val('');");
        txt.Append("$('#txtAddContact').val('');");
        txt.Append("$('#txtAddTelephone').val('');");
        txt.Append("$('#txtAddFax').val('');");

        txt.Append("});");

        txt.Append("$('#jqi_state0_buttonSave').click(function () {");
        // txt.Append("window.location = 'processmaster.aspx?fm=addcontactdetails&type=insert&querytype=repgroup&typ='+document.getElementById('cmbAddressType').value;");
        txt.Append("var asset_type_name1 = 'lead_customer/process_forms/processmaster.aspx?fm=addcontactdetails&type=insert';");
        txt.Append("var asset_type_name2 = 'lead_customer/process_forms/processmaster.aspx?fm=contactdetails&type=insert';");
        txt.Append("var asset_type_name = '';");

        txt.Append("asset_type_name=asset_type_name +'&typ=';");
        txt.Append("asset_type_name=asset_type_name +'&add1='+escape($('#txtAddress1').val());");
        txt.Append("asset_type_name=asset_type_name +'&add2='+escape($('#txtAddress2').val());");
        txt.Append("asset_type_name=asset_type_name +'&cty='+escape($('#txtCity').val());");
        txt.Append("asset_type_name=asset_type_name +'&sta='+escape($('#txtState').val());");

        txt.Append("asset_type_name=asset_type_name +'&pstcde='+escape($('#txtPostcode').val());");
        txt.Append("asset_type_name=asset_type_name +'&cntry='+escape($('#cmbCountry').val());");
        txt.Append("asset_type_name=asset_type_name +'&name='+escape($('#txtName').val());");
        txt.Append("asset_type_name=asset_type_name +'&adcntac='+escape($('#txtAddContact').val());");
        txt.Append("asset_type_name=asset_type_name +'&adtele='+escape($('#txtAddTelephone').val());");
        txt.Append("asset_type_name=asset_type_name +'&adfax='+escape($('#txtAddFax').val());");

        if (!string.IsNullOrWhiteSpace(CustomerCode))
        {
            txt.Append("asset_type_name=asset_type_name +'&custid=" +Server.UrlEncode(CustomerCode) + "';");
            txt.Append("asset_type_name=asset_type_name +'&assinto=" + AssigneeNo + "';");
        }
        else if (!string.IsNullOrWhiteSpace(LeadId))
        {
            txt.Append("asset_type_name=asset_type_name +'&leadid=" + LeadId + "';");
            txt.Append("asset_type_name=asset_type_name +'&leadadid=" + LeadAddresID + "';");
        }

        txt.Append("if($('#txtAddress1').val() != ''){");
        txt.Append("var newurl=asset_type_name1 + asset_type_name;");
        //txt.Append("alert('Add '+escape(newurl));");
        txt.Append("insertAddress(newurl);}");
        txt.Append("else { ");
        txt.Append("asset_type_name=asset_type_name +'&ms=1';");
        txt.Append("var newurl2=asset_type_name2 + asset_type_name;");
        //txt.Append("alert(escape(newurl2));");
        txt.Append("addAddress(newurl2,'');}");
        txt.Append("});");


        txt.Append("$('div#jqi').css('width', '380px');");
        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
            txt.Append("$('div#jqi').css('height', '455px');");
        else
            txt.Append("$('div#jqi').css('height', '410px');");

        txt.Append("$('div#jqi').css('left', '80%');");
        txt.Append("</script>");
        txt.Append("</div>");
        txt.Append("</div>");
        //txt.Append("<script type='text/javascript'>");
        //txt.Append("$('#div_promt').html('" + txt.ToString().Trim() + "');");
        //txt.Append("</script>");

        return txt.ToString();
    }

    private string InsertBankDetailsForm(string errormessage, bool hasError = false)
    {//OriginatorLookup
        StringBuilder txt = new StringBuilder();
        string type = string.IsNullOrEmpty(Request.QueryString["typ"]) ? "" : Request.QueryString["typ"];
        string accountNo = string.IsNullOrEmpty(Request.QueryString["accno"]) ? "" : Request.QueryString["accno"];
        string bank = string.IsNullOrEmpty(Request.QueryString["bnk"]) ? "" : Request.QueryString["bnk"];
        string branch = string.IsNullOrEmpty(Request.QueryString["brch"]) ? "" : Request.QueryString["brch"];
        string DistributorCode = string.IsNullOrEmpty(Request.QueryString["distid"]) ? "" : Server.UrlDecode(Request.QueryString["distid"]);

        txt.Append("<div id=\"contactentry\" width=\"380px\">");

        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
            txt.Append("<div id=\"div_text\" rel=\"0\">");
        else
            txt.Append("<div id=\"div_text\">");

        txt.Append("<table border=\"0\">");
        
        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
        {
            CommonUtility utility = new CommonUtility();
            txt.Append("<tr rel=\"0\"><td colspan=\"5\">" + utility.GetErrorMessage(errormessage) + "</td></tr>");
        }

        txt.Append("<tr><td class=\"textalignbottom\">Account No</td><td colspan=\"3\">&nbsp;<input id=\"txtAccountNo\" type=\"text\" class=\"textboxwidth\" Value=\"" + accountNo + "\"/></td>  </tr>");
        txt.Append("<tr><td class=\"textalignbottom\">Bank</td><td colspan=\"3\">&nbsp;<input id=\"txtBank\" type=\"text\" class=\"textboxwidth\" Value=\"" + bank + "\"/></td></tr>");
        txt.Append("<tr><td class=\"textalignbottom\">Branch</td><td colspan=\"3\">&nbsp;<input id=\"txtBranch\" type=\"text\" class=\"textboxwidth\" Value=\"" + branch + "\"/></td></tr>");
        txt.Append("<tr><td class=\"textalignbottom\" colspan =\"4\"> <button id=\"jqi_state0_buttonSave\" class=\"k-button\">Save</button> <button id=\"jqi_state0_buttonClear\" class=\"k-button\">Clear</button> </td></tr>");

        txt.Append("</table>");

        txt.Append("<script type='text/javascript'>");
        txt.Append("$('#jqi_state0_buttonClear').click(function () {");

        txt.Append("$('#txtAccountNo').val('');");
        txt.Append("$('#txtBank').val('');");
        txt.Append("$('#txtBranch').val('');");

        txt.Append("});");

        txt.Append("$('#jqi_state0_buttonSave').click(function () {");
        // txt.Append("window.location = 'processmaster.aspx?fm=addcontactdetails&type=insert&querytype=repgroup&typ='+document.getElementById('cmbAddressType').value;");
        txt.Append("var asset_type_name1 = 'lead_customer/process_forms/processmaster.aspx?fm=addbankdetails&type=insert';");
        txt.Append("var asset_type_name2 = 'lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert';");
        txt.Append("var asset_type_name = '';");

        txt.Append("asset_type_name=asset_type_name +'&accno='+escape($('#txtAccountNo').val());");
        txt.Append("asset_type_name=asset_type_name +'&bnk='+escape($('#txtBank').val());");
        txt.Append("asset_type_name=asset_type_name +'&brch='+escape($('#txtBranch').val());");
        txt.Append("asset_type_name=asset_type_name +'&typ=DIST';");

        if (!string.IsNullOrWhiteSpace(DistributorCode))
        {
            txt.Append("asset_type_name=asset_type_name +'&distid=" + Server.UrlEncode(DistributorCode) + "';");
       //     txt.Append("asset_type_name=asset_type_name +'&assinto=" + AssigneeNo + "';");
        }


        txt.Append("if($('#txtAccountNo').val() != ''){");
        txt.Append("var newurl=asset_type_name1 + asset_type_name;");
        //txt.Append("alert('Add '+escape(newurl));");
        txt.Append("insertBankDetails(newurl);}");
        txt.Append("else { ");
      //k  txt.Append("asset_type_name=asset_type_name +'&ms=1';");
        txt.Append("var newurl2=asset_type_name2 + asset_type_name;");
        //txt.Append("alert(escape(newurl2));");
        txt.Append("addBankAccount(newurl2,'');}");
        txt.Append("});");


        txt.Append("$('div#jqi').css('width', '380px');");
        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
            txt.Append("$('div#jqi').css('height', '455px');");
        else
            txt.Append("$('div#jqi').css('height', '410px');");

        txt.Append("$('div#jqi').css('left', '80%');");
        txt.Append("</script>");
        txt.Append("</div>");
        txt.Append("</div>");
        //txt.Append("<script type='text/javascript'>");
        //txt.Append("$('#div_promt').html('" + txt.ToString().Trim() + "');");
        //txt.Append("</script>");

        return txt.ToString();
    }

    private string InsertContactDetails()
    {
        StringBuilder txt = new StringBuilder();
        bool success = false;
        bool hasWarning = false;

        string type = Request.QueryString["typ"];
        string address1 = Request.QueryString["add1"];
        string address2 = Request.QueryString["add2"];
        string City = Request.QueryString["cty"];
        string State = Request.QueryString["sta"];
        string Postcode = Request.QueryString["pstcde"];
        string Country = Request.QueryString["cntry"];
        string Name = Request.QueryString["name"];
        string AddContact = Request.QueryString["adcntac"];
        string AddTelephone = Request.QueryString["adtele"];
        string AddFax = Request.QueryString["adfax"];
        string CustomerCode = Server.UrlDecode(Request.QueryString["custid"]);
        string LeadId = Request.QueryString["leadid"];
        string AssigneeNo = Request.QueryString["assinto"];
        string LeadAddresID = Request.QueryString["leadadid"];

        try
        {
            CustomerClient oAddress = new CustomerClient();
            LeadClient leadClient = new LeadClient();
            LeadAddressDTO customerAddress = new LeadAddressDTO();

            if (!string.IsNullOrWhiteSpace(CustomerCode))
                customerAddress.CustCode = CustomerCode;
            else if (!string.IsNullOrWhiteSpace(LeadId))
                customerAddress.LeadID = int.Parse(LeadId);

            if (!string.IsNullOrWhiteSpace(AssigneeNo))
                customerAddress.AssigneeNo = int.Parse(AssigneeNo);

            if (!string.IsNullOrWhiteSpace(LeadAddresID))
                customerAddress.LeadAddressID = int.Parse(LeadAddresID);

            if (string.IsNullOrWhiteSpace(address1))
            {
                //GlobalValues.GetInstance().ShowMessage("Address 1 cannot be empty.", GlobalValues.MessageImageType.Information);
                return "0";
            }

            customerAddress.Address1 = address1;
            customerAddress.Address2 = address2;
            customerAddress.City = City;
            customerAddress.State = State;
            customerAddress.PostCode = Postcode;
            customerAddress.Country = Country;
            customerAddress.Name = Name;
            customerAddress.CreatedDate = DateTime.Now.ToUniversalTime();

            //Check the Street and Postal Address.
            //if (!string.IsNullOrWhiteSpace(LeadId))
            //{
            //    int streetAddressesCount = 0;
            //    int postalAddressesCount = 0;

            //    List<LeadAddressDTO> leadAddressList = leadClient.GetLeadAddreses(int.Parse(LeadId));

            //    if (LeadAddresID == null || LeadAddresID == "0")
            //    {
            //        foreach (LeadAddressDTO item in leadAddressList)
            //        {
            //            if (item.AddressType.Equals(ConfigUtil.StreetAddressType))
            //                streetAddressesCount++;
            //            else if (item.AddressType.Equals(ConfigUtil.PostalAddressType))
            //                postalAddressesCount++;
            //        }

            //        if (streetAddressesCount >= 1 && type == "Street Address")
            //        {
            //            // txt.Append("Maximum number of Street addresses are added");
            //            txt.Append(InsertContactDetailsForm("Maximum number of Street addresses are added", true));
            //            hasWarning = true;
            //            return txt.ToString();
            //        }

            //        if (postalAddressesCount >= 1 && type == "Postal Address")
            //        {
            //            //txt.Append("Maximum number of Postal addresses are added");
            //            txt.Append(InsertContactDetailsForm("Maximum number of Postal addresses are added", true));
            //            hasWarning = true;
            //            return txt.ToString();
            //        }
            //    }
            //}

            ////if (cmbAreaCode.SelectedItem == null)
            ////{
            ////    GlobalValues.GetInstance().ShowMessage("Please select an 'Area Code'.", GlobalValues.MessageImageType.Information);
            ////    return;
            ////}

            if (!hasWarning)
            {
                customerAddress.Telephone = AddTelephone;
                customerAddress.Fax = AddFax;
                customerAddress.Contact = AddContact;
                customerAddress.CreatedBy = UserSession.Instance.UserName;


                //if (type.Equals("Street Address"))
                //{
                //    customerAddress.AssignedTo = "1";
                //    customerAddress.AddressType = ConfigUtil.StreetAddressType;
                //}
                //else
                //{
                    customerAddress.AssignedTo = "C";
                    customerAddress.AddressType = "D";
                //    customerAddress.AddressType = ConfigUtil.PostalAddressType;
                //}

                if (!string.IsNullOrWhiteSpace(CustomerCode))
                {
                    if (oAddress.SaveCustomerAddress(customerAddress))
                    {
                        return "1";
                    }
                }
                else if (!string.IsNullOrWhiteSpace(LeadId))
                {
                    int leadAddressId = 0;
                    if (leadClient.SaveLeadAddreses(customerAddress, ref leadAddressId))
                    {
                        return "1";
                    }
                    else
                    {
                        txt.Append(InsertContactDetailsForm("Maximum number of " + type + " are added", true));
                        hasWarning = true;
                        return txt.ToString();
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
        return txt.ToString();
    }

    private string InsertBankDetails()
    {
        StringBuilder txt = new StringBuilder();
        bool success = false;
        bool hasWarning = false;

        string Type = Request.QueryString["typ"];//"DIST";//Request.QueryString["typ"];
        string AccountNo = Request.QueryString["accno"];
        string Bank = Request.QueryString["bnk"];
        string Branch = Request.QueryString["brch"];
        string Distributor = Request.QueryString["distid"];


        try
        {
            CustomerClient oAddress = new CustomerClient();
            DistributorClient oDistributor = new DistributorClient();
            LeadClient leadClient = new LeadClient();
            LeadAddressDTO customerAddress = new LeadAddressDTO();
            BankAccountDTO bankAccount = new BankAccountDTO();

            bankAccount.SourceTypeText = Type;
            bankAccount.AccountNumber = AccountNo;
            bankAccount.Bank = Bank;
            bankAccount.Branch = Branch;
            bankAccount.CreatedBy = Distributor;

            //k

            //if (!string.IsNullOrWhiteSpace(CustomerCode))
            //    customerAddress.CustCode = CustomerCode;
            //else if (!string.IsNullOrWhiteSpace(LeadId))
            //    customerAddress.LeadID = int.Parse(LeadId);

            //if (!string.IsNullOrWhiteSpace(AssigneeNo))
            //    customerAddress.AssigneeNo = int.Parse(AssigneeNo);

            //if (!string.IsNullOrWhiteSpace(LeadAddresID))
            //    customerAddress.LeadAddressID = int.Parse(LeadAddresID);

            //if (string.IsNullOrWhiteSpace(address1))
            //{
            //    //GlobalValues.GetInstance().ShowMessage("Address 1 cannot be empty.", GlobalValues.MessageImageType.Information);
            //    return "0";
            //}

            //k
            //customerAddress.Address1 = address1;
            //customerAddress.Address2 = address2;
            //customerAddress.City = City;
            //customerAddress.State = State;
            //customerAddress.PostCode = Postcode;
            //customerAddress.Country = Country;
            //customerAddress.Name = Name;
            //customerAddress.CreatedDate = DateTime.Now.ToUniversalTime();

            //Check the Street and Postal Address.
            //if (!string.IsNullOrWhiteSpace(LeadId))
            //{
            //    int streetAddressesCount = 0;
            //    int postalAddressesCount = 0;

            //    List<LeadAddressDTO> leadAddressList = leadClient.GetLeadAddreses(int.Parse(LeadId));

            //    if (LeadAddresID == null || LeadAddresID == "0")
            //    {
            //        foreach (LeadAddressDTO item in leadAddressList)
            //        {
            //            if (item.AddressType.Equals(ConfigUtil.StreetAddressType))
            //                streetAddressesCount++;
            //            else if (item.AddressType.Equals(ConfigUtil.PostalAddressType))
            //                postalAddressesCount++;
            //        }

            //        if (streetAddressesCount >= 1 && type == "Street Address")
            //        {
            //            // txt.Append("Maximum number of Street addresses are added");
            //            txt.Append(InsertContactDetailsForm("Maximum number of Street addresses are added", true));
            //            hasWarning = true;
            //            return txt.ToString();
            //        }

            //        if (postalAddressesCount >= 1 && type == "Postal Address")
            //        {
            //            //txt.Append("Maximum number of Postal addresses are added");
            //            txt.Append(InsertContactDetailsForm("Maximum number of Postal addresses are added", true));
            //            hasWarning = true;
            //            return txt.ToString();
            //        }
            //    }
            //}

            ////if (cmbAreaCode.SelectedItem == null)
            ////{
            ////    GlobalValues.GetInstance().ShowMessage("Please select an 'Area Code'.", GlobalValues.MessageImageType.Information);
            ////    return;
            ////}

            //if (!hasWarning)
            //{
            //    customerAddress.Telephone = AddTelephone;
            //    customerAddress.Fax = AddFax;
            //    customerAddress.Contact = AddContact;
            //    customerAddress.CreatedBy = UserSession.Instance.UserName;


            //    //if (type.Equals("Street Address"))
            //    //{
            //    //    customerAddress.AssignedTo = "1";
            //    //    customerAddress.AddressType = ConfigUtil.StreetAddressType;
            //    //}
            //    //else
            //    //{
            //    customerAddress.AssignedTo = "C";
            //    customerAddress.AddressType = "D";
            //    //    customerAddress.AddressType = ConfigUtil.PostalAddressType;
            //    //}

            //    if (!string.IsNullOrWhiteSpace(CustomerCode))
            //    {
            //        if (oAddress.SaveCustomerAddress(customerAddress))
            //        {
            //            return "1";
            //        }
            //    }
            //    else if (!string.IsNullOrWhiteSpace(LeadId))
            //    {
            //        int leadAddressId = 0;
            //        if (leadClient.SaveLeadAddreses(customerAddress, ref leadAddressId))
            //        {
            //            return "1";
            //        }
            //        else
            //        {
            //            txt.Append(InsertContactDetailsForm("Maximum number of " + type + " are added", true));
            //            hasWarning = true;
            //            return txt.ToString();
            //        }
            //    }
            //}


            if (oDistributor.SaveBankAccounts(bankAccount))
            {
                return "1";
            }
            else {
                return "Error Occured";
            }

            
        }
        catch (Exception ex)
        {
        }
        return txt.ToString();
    }

    private string GetEmailAttachment()
    {
        StringBuilder sdoc = new StringBuilder();

        if (Request.QueryString["emid"] != null && Request.QueryString["emid"] != string.Empty)
        {
            CommonClient commonClient = new CommonClient();

            Directory.CreateDirectory(Request.PhysicalApplicationPath + "\\docs\\");

            string path = Request.PhysicalApplicationPath + "docs\\";

            EmailAndAttachmentsDTO emailAndAttachments = commonClient.GetEmailbyId(int.Parse(Request.QueryString["emid"].Trim()), path, Session.SessionID.ToString());
            List<EmailAttachmentDTO> attachmentList = emailAndAttachments.Attachments;// commonClient.GetEmailAttachment(int.Parse(Request.QueryString["emid"].Trim()), path,Session.SessionID.ToString());

            //sdoc.Append(emailAndAttachments.Email.FileName);
            sdoc.Append("<div id=\"div_mainemailattach\">");
            sdoc.Append("<div id=\"div_emailattach\" class=\"emailattach\" rel=\"" + emailAndAttachments.Email.FileName + "\">");
            sdoc.Append("<ul>");
            foreach (EmailAttachmentDTO item in attachmentList)
            {
                sdoc.Append("<li>");

                string extension = item.FileName.Trim().Contains(".") ? item.FileName.Trim().Substring(item.FileName.Trim().LastIndexOf(".")) : string.Empty;

                if (!string.IsNullOrWhiteSpace(extension))
                {
                    string[] tempfilename = item.FilePath.Trim().Split('\\');
                    string newtempfilename = tempfilename[(tempfilename.Length - 1)].ToString();

                    if ((extension.Equals(".jpg")) || (extension.Equals(".gif")) || (extension.Equals(".png")) || (extension.Equals(".txt")))
                    {
                        if (tempfilename.Length > 0)
                        {
                            sdoc.Append("<a href=" + "javascript:loadMailContent('" + newtempfilename.Trim() + "');>");
                            sdoc.Append(item.FileName.Trim());
                            sdoc.Append("</a>");
                        }
                    }
                    else
                    {
                        string ori_path = ConfigUtil.ApplicationPath + "docs/" + newtempfilename.Trim();
                        sdoc.Append("<a href=\"" + ori_path + "\">");
                        sdoc.Append(item.FileName.Trim());
                        sdoc.Append("</a>");
                    }

                }
                sdoc.Append("</li>");
            }

            sdoc.Append("</ul>");
            sdoc.Append("</div>");
            sdoc.Append("</div>");

        }
        return sdoc.ToString();
    }

    private string GetEmailContent()
    {
        StringBuilder sdoc = new StringBuilder();

        if (Request.QueryString["ne"] != null && Request.QueryString["ne"] != string.Empty)
        {
            string ext = Request.QueryString["ne"].Trim().Contains(".") ? Request.QueryString["ne"].Substring(Request.QueryString["ne"].LastIndexOf(".")) : string.Empty;
            string path = Request.PhysicalApplicationPath + "\\docs\\" + Request.QueryString["ne"];

            sdoc.Append("<div class=\"well\">");
            sdoc.Append("<div id=\"cp3\" data-color-format=\"hex\" class=\"input-append color\">");
            if (ext.Equals(".txt"))
            {
                StreamReader fp = null;

                try
                {
                    fp = File.OpenText(path);
                    sdoc.Append("<div  style=\"height:343px;width:531px;overflow:auto;\">");
                    sdoc.Append(Server.HtmlEncode(fp.ReadToEnd()));
                    sdoc.Append("</div>");
                    fp.Close();
                }
                catch (Exception err)
                {
                    sdoc.Append("File Read Failed. Reason is as follows ");
                }
            }
            else if ((ext.Equals(".jpg")) || (ext.Equals(".gif")) || (ext.Equals(".png")))
            {
                sdoc.Append("<div style=\"height:343px;width:531px;overflow:auto;\">");
                sdoc.Append("<img src=\"" + ConfigUtil.ApplicationPath + "docs/" + Request.QueryString["ne"].Trim() + "\" />");
                sdoc.Append("</div>");
            }
            sdoc.Append("</div>");
            sdoc.Append("</div>");

            sdoc.Append("<script type='text/javascript'>");
            //sdoc.Append("$('div#jqi').css('width', '560px');");
            //sdoc.Append("$('div#jqi').css('height', '400px');");
            //sdoc.Append("$('div#jqi').css('left', '28');");
            sdoc.Append("</script>");
        }
        return sdoc.ToString();
    }

    public string ProcessRequest()
    {
        var isUpload = "0";
        try
        {
            //var db = new mypicksDBDataContext();
            HttpPostedFile postedFile = Request.Files["files"];
            string savepath = Server.MapPath("../../docs/upload/");
            string filename = postedFile.FileName;
            if (!Directory.Exists(savepath))
                Directory.CreateDirectory(savepath);
            postedFile.SaveAs(savepath + filename);
            //isUpload = InsertAddDocument(postedFile.FileName);
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        return isUpload;
    }

    private string InsertAddDocument(string documentName, string filePath, string customerCode,string leadId,string endUserId = "")
    //private string InsertAddDocument(string filename)
    {

        bool iNoRecs = false;
        //string strpath = oFileDialog.FileName;
        //LeadClient oDocument = new LeadClient();

        //DocumentDTO document = new DocumentDTO();

        ////document.DocumentName = System.IO.Path.GetFileNameWithoutExtension(filename);
        //document.DocumentName = filename;

        //if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != "")
        //{
        //    document.LeadID = int.Parse(Request.QueryString["leadid"]);
        //}
        //else if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != "")
        //{
        //    document.CustCode = Request.QueryString["custid"];
        //}

        //document.Path = System.IO.Path.GetFileName(filename);

        ////document.Content = GlobalValues.GetInstance().ConvertFile(oFileDialog.FileName);
        //document.AttachedDate = DateTime.Now;
        //document.AttachedBy = UserSession.Instance.UserName;
        //document.LastModifiedBy = UserSession.Instance.UserName;
        //document.LastModifiedDate = DateTime.Now;

        //try
        //{
        //    iNoRecs = oDocument.SaveDocument(document);

        //}
        //catch (Exception oException)
        //{
        //}
        string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs";

        LeadClient leadClient = new LeadClient();
        DocumentDTO document = new DocumentDTO();

        document.DocumentName = documentName;
        document.Path = filePath;

        if (!string.IsNullOrWhiteSpace(customerCode))
            document.CustCode = HttpContext.Current.Server.UrlDecode(customerCode);
        if (!string.IsNullOrWhiteSpace(leadId))
            document.LeadID = int.Parse(leadId);
        if (!string.IsNullOrWhiteSpace(endUserId))
            document.EnduserCode = endUserId;

        document.AttachedDate = DateTime.Now;
        document.AttachedBy = UserSession.Instance.UserName;
        document.FileLocation = Path.Combine(path, filePath);
        document.LastModifiedBy = UserSession.Instance.UserName;
        document.LastModifiedDate = DateTime.Now;
        //newid = 0;
        try
        {
            iNoRecs = leadClient.SaveDocument(document);
        }
        catch (Exception oException)
        {
        }
        return iNoRecs ? "1" : "0";
    }

    private string GetDocumentContent()
    {
        StringBuilder sdoc = new StringBuilder();

        if (Request.QueryString["docname"] != null && Request.QueryString["docname"] != string.Empty)
        {
            LeadClient documentClient = new LeadClient();

            Directory.CreateDirectory(Request.PhysicalApplicationPath + "\\docs\\");

            string path = Request.PhysicalApplicationPath + "\\docs\\";

            string filename = Request.QueryString["docname"];
            string ext = Request.QueryString["ext"];

            //DocumentDTO docDTO = documentClient.GetDocument(int.Parse(Request.QueryString["docid"].Trim()));

            //string leadcustcode = string.Empty;
            //if ((string.IsNullOrWhiteSpace(docDTO.CustCode)) && (docDTO.LeadID != 0))
            //    leadcustcode = docDTO.LeadID.ToString();
            //else
            //    leadcustcode = docDTO.CustCode.Trim();

            //string new_tempFileName = GenerateFileName(leadcustcode, docDTO.DocumentName.Trim()) + (docDTO.Path.Contains(".") ?
            //                               docDTO.Path.Substring(docDTO.Path.LastIndexOf(".")) : string.Empty);

            string new_path = path + filename;// new_tempFileName;
            ext = System.IO.Path.GetExtension(ext).Replace(".", "");

            if ((ext.Equals("jpg")) || (ext.Equals("gif")) || (ext.Equals("png")) || (ext.Equals("txt")))
            {
                if (filename != null)
                {
                    sdoc.Append("<div class=\"well\">");
                    sdoc.Append("<div id=\"cp3\" data-color-format=\"hex\" class=\"input-append color\">");
                    if (ext.Equals("txt"))
                    {
                        StreamReader fp = null;

                        try
                        {
                            fp = File.OpenText(new_path);
                            sdoc.Append("<div  style=\"height:590px;width:790px;overflow:auto;\">");
                            sdoc.Append(Server.HtmlEncode(fp.ReadToEnd()));
                            sdoc.Append("</div>");
                            fp.Close();
                        }
                        catch (Exception err)
                        {
                            sdoc.Append("File Read Failed. Reason is as follows ");
                        }
                    }
                    else if ((ext.Equals("jpg")) || (ext.Equals("gif")) || (ext.Equals("png")) || (ext.Equals("jpg")))
                    {
                        sdoc.Append("<div style=\"height:590px;width:790px;overflow:auto;\">");
                        sdoc.Append("<img src=\"" + ConfigUtil.ApplicationPath + "docs/" + filename + "\" />");
                        sdoc.Append("</div>");
                    }
                    sdoc.Append("</div>");
                    sdoc.Append("</div>");

                    sdoc.Append("<script type='text/javascript'>");
                    sdoc.Append("$('div#jqi').css('width', '560px');");
                    sdoc.Append("$('div#jqi').css('height', '400px');");
                    sdoc.Append("$('div#jqi').css('left', '28');");
                    sdoc.Append("</script>");
                }
            }
            else
            {
                string filepath = Request.PhysicalApplicationPath + "\\docs\\" + filename;
                /*FileInfo file;
                file = new FileInfo(filepath);
                if (file.Exists)
                {
                    Response.ClearContent();
                    //Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name)
                    Response.AddHeader("Content-Disposition", "inline; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = ReturnExtension(file.Extension.ToLower());
                    Response.TransmitFile(file.FullName);
                    Response.End();
                }*/

                sdoc.Append(filepath);
            }
        }

        return sdoc.ToString();
    }


    private string DeleteOpportunity(string OpportunityId)
    {
        OpportunityClient op = new OpportunityClient();
        bool success = op.DeleteOpportunity(int.Parse(OpportunityId));

        StringBuilder txt = new StringBuilder();
        if (success)
        {
            txt.Append("true");
        }
        else
        {
            txt.Append("false");
        }
        return txt.ToString();

    }

    private string UnassignedRoutefromRep(string RouteId, string RepCode)
    {
        LeadCustomerClient op = new LeadCustomerClient();
        bool success = op.UnassignedRoutefromRep(int.Parse(RouteId), RepCode);

        StringBuilder txt = new StringBuilder();
        if (success)
        {
            txt.Append("true");
        }
        else
        {
            txt.Append("false");
        }
        return txt.ToString();

    } 
    
    private string DeleteBulkCust(string custList)
    {
        var userName = UserSession.Instance.UserName;
        var userType = UserSession.Instance.Originator;

        CustomerClient op = new CustomerClient();
        bool success = op.DeleteBulkCust(userType, userName, custList);

        StringBuilder txt = new StringBuilder();
        if (success)
        {
            txt.Append("true");

            try
            {
                CommonClient cClient = new CommonClient();
                cClient.CreateTransactionLog(userName,
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    "BulkDelete",
                    "Customer",
                    userName + " use bulk delete to delete customer list " + custList.Substring(1, 100));
            }
            catch { }
        }
        else
        {
            txt.Append("false");
        }
        //return txt.ToString();
        return "";
    }

    private string DeleteLeadDocument(string leaddocumentId)
    {
        bool success = false;
        StringBuilder txt = new StringBuilder();
        LeadClient docuement = new LeadClient();
        success = docuement.DeleteLeadDocument(int.Parse(leaddocumentId));
        if (success)
        {
            txt.Append("1");
        }
        else
        {
            txt.Append("0");
        }
        return txt.ToString();

    }

    public string UploadProfilePic()
    {
        var isUpload = "0";
        try
        {
            //var db = new mypicksDBDataContext();
            HttpPostedFile postedFile = Request.Files["file_upload"];
            string savepath = Server.MapPath("../../docs/contactperson/");
            string filename = GenerateFileName(postedFile.FileName,Session.SessionID);
            if (!Directory.Exists(savepath))
                Directory.CreateDirectory(savepath);
            postedFile.SaveAs(savepath + filename);
            //isUpload = InsertAddDocument(postedFile.FileName);
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        return isUpload;
    }

    private string GenerateFileName(string ori_imagename, string sessionid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";

        fn += "contactperson_" + sessionid + "_" + ori_imagename.Trim();  //to generate a unique name
        return fn;
    }

    public string SaveCustomerDocument()
    {
        var isUpload = "0";
        try
        {
            HttpPostedFile postedFile = Request.Files["files"];
           // string savepath = Server.MapPath("../../docs/upload/");

            string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            string filename = postedFile.FileName;

            string newFileNameWithoutExt = Path.GetFileNameWithoutExtension(filename);
            string extension = Path.GetExtension(filename);
            string tempFileName = "";
            string leadcustid = string.Empty;
            string eduid = string.Empty;

            if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != "")
            {
                leadcustid = Request.QueryString["leadid"];
            }
            else if (Request.QueryString["eduid"] != null && Request.QueryString["eduid"] != "")
            {
                leadcustid = Request.QueryString["custid"];
                eduid = Request.QueryString["eduid"];
            }
            else if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != "")
            {
                leadcustid = Request.QueryString["custid"];
            }

            if (string.IsNullOrWhiteSpace(leadcustid))
                return "-1";

            tempFileName = GenerateFileName(newFileNameWithoutExt, Session.SessionID, Server.HtmlEncode(leadcustid)) + extension;
            filename = Path.Combine(path, tempFileName);

            postedFile.SaveAs(filename);
            if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != "")
            {
                isUpload = InsertAddDocument(newFileNameWithoutExt, tempFileName, null, leadcustid);
            }
            else if (Request.QueryString["eduid"] != null && Request.QueryString["eduid"] != "")
            {
                isUpload = InsertAddDocument(newFileNameWithoutExt, tempFileName, leadcustid, null, eduid);
            }
            else if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != "")
            {
                isUpload = InsertAddDocument(newFileNameWithoutExt, tempFileName, leadcustid, null);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        return isUpload;
    }

    private string GenerateFileName(string ori_imagename, string sessionid, string leadcustid)
    {
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "tmp_" + sessionid.Trim() + "_" + ori_imagename.Trim() + "_";
        fn += leadcustid.Trim();   //to generate a unique name
        return fn;
    }

    private string TransferEndUser(string errormessage, bool hasError = false)
    {
        StringBuilder txt = new StringBuilder();

        string custcode = string.IsNullOrEmpty(Request.QueryString["custid"]) ? "" : Request.QueryString["custid"];
        string endusercode = string.IsNullOrEmpty(Request.QueryString["eduid"]) ? "" : Request.QueryString["eduid"];
        
        string ms = string.IsNullOrEmpty(Request.QueryString["ms"]) ? "0" : Request.QueryString["ms"];

        txt.Append("<div id=\"contactentry\" width=\"380px\">");

        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
            txt.Append("<div id=\"div_text\" rel=\"0\">");
        else
            txt.Append("<div id=\"div_text\">");

        txt.Append("<table border=\"0\">");
        //
        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
        {
            CommonUtility utility = new CommonUtility();
            txt.Append("<tr rel=\"0\"><td colspan=\"5\">" + utility.GetErrorMessage(errormessage) + "</td></tr>");
        }

        txt.Append("<tr><td style=\"width: 134px;\" class=\"textalignbottom\">End User Code</td><td colspan=\"2\" style=\"width: 155px;\" >" + endusercode + "</td> </tr>");
        txt.Append("<tr><td style=\"width: 134px;\" class=\"textalignbottom\">New Cust Code</td><td style=\"width: 155px;\">&nbsp;<input id=\"txtnewcustcode\" type=\"text\" Value=\"" + "" + "\"/></td>");
        txt.Append("<td style=\"width: 130px;\"><a href=\"#\" id=\"id_custlookup\"><img id=\"Img1\" src=\""+ConfigUtil.ApplicationPath+"assets/images/popicon.png\" alt=\"popicon\" border=\"0\" /></a></td>");
        txt.Append("</tr>");

       // txt.Append("<tr><td class=\"textalignbottom\" colspan =\"4\"> <div id = \"jqi_state0_buttonSave\" >Save</div> </td></tr>");
        txt.Append("<tr><td class=\"textalignbottom\" colspan =\"4\"> <button id=\"jqi_state0_buttonSave\" class=\"k-button\">Save</button> </td></tr>");

        txt.Append("</table>");

        txt.Append("<script type='text/javascript'>");

        txt.Append("$('#id_custlookup').click(function () {");
        txt.Append("loadnewcustomer('div_newcustgrid','newcust_window');");
        txt.Append("});");

        txt.Append("$('#jqi_state0_buttonSave').click(function () {");
        txt.Append("var asset_type_name1 = 'lead_customer/process_forms/processmaster.aspx?fm=transferenduser&type=Update';");
        
        txt.Append("var asset_type_name = '';");
        txt.Append("asset_type_name=asset_type_name +'&n_custid='+escape($('#txtnewcustcode').val());");
        
        txt.Append("asset_type_name=asset_type_name +'&custid=" + custcode + "';");
        txt.Append("asset_type_name=asset_type_name +'&eduid=" + endusercode + "';");

        txt.Append("if($('#txtnewcustcode').val() != ''){");
        txt.Append("var newurl=asset_type_name1 + asset_type_name;");
      //  txt.Append("alert('Add '+$('#txtnewcustcode').val());");
        txt.Append("savetransfer(newurl);");
        txt.Append("}");
        txt.Append("else { ");
        txt.Append("asset_type_name=asset_type_name +'&ms=1';");
        txt.Append("var newurl2=asset_type_name2 + asset_type_name;");
        //txt.Append("alert(escape(newurl2));");
        //txt.Append("addAddress(newurl2,'');}");
        txt.Append("}});");


        txt.Append("$('div#jqi').css('width', '380px');");
        if (hasError && (!string.IsNullOrWhiteSpace(errormessage)))
            txt.Append("$('div#jqi').css('height', '455px');");
        else
            txt.Append("$('div#jqi').css('height', '410px');");

        txt.Append("$('div#jqi').css('left', '80%');");
        txt.Append("</script>");
        txt.Append("</div>");
        txt.Append("</div>");
        //txt.Append("<script type='text/javascript'>");
        //txt.Append("$('#div_promt').html('" + txt.ToString().Trim() + "');");
        //txt.Append("</script>");

        return txt.ToString();
    }

    private string SaveTransferEndUser(bool hasError = false)
    {
        StringBuilder txt = new StringBuilder();

        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            string custcode = string.IsNullOrEmpty(Request.QueryString["custid"]) ? "" : Server.HtmlDecode(Request.QueryString["custid"]);
            string endusercode = string.IsNullOrEmpty(Request.QueryString["eduid"]) ? "" : Request.QueryString["eduid"];
            string newcust_code = string.IsNullOrEmpty(Request.QueryString["n_custid"]) ? "" : Request.QueryString["n_custid"];

            string ms = string.IsNullOrEmpty(Request.QueryString["ms"]) ? "0" : Request.QueryString["ms"];

            EndUserClient endUserClient = new EndUserClient();
            EndUserDTO endUser = new EndUserDTO();
            bool iNoRecs = false;
            CommonUtility commonUtility = new CommonUtility();

            try
            {
                endUser.EndUserCode = endusercode;
                endUser.CustomerCode = newcust_code;

                ArgsDTO args = new ArgsDTO();
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                args.CustomerCode = custcode;
                args.Originator = UserSession.Instance.Originator;

                iNoRecs = endUserClient.Transfer(endUser, args);

                if (iNoRecs)
                {
                    txt.Append(commonUtility.GetSucessfullMessage("Successfully Transferred."));
                }
                else
                {
                    txt.Append(commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE));
                }
            }
            catch (Exception oException)
            {
                txt.Append(commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE));
            }
        }
        return txt.ToString();
    }

    private string LoadNewCustomer()
    {
        StringBuilder newcust = new StringBuilder();
        //Setting Args
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;

        StringBuilder txt = new StringBuilder();
        string pageType = "1";
        if (Request.QueryString["pg"] != null)
        {
            pageType = Request.QueryString["pg"];
        }

        CommonClient commonClient = new CommonClient();
        List<object> para = new List<object>();
        int currentIndex = 0;
        para.Add("");
        para.Add(UserSession.Instance.DefaultDepartmentId);

        txt.Append("<script type='text/javascript'> ");
        string where = "";
        if (Request.QueryString["s1"] != null)
        {
            where = "( UPPER(a.cust_code) like '%" + Request.QueryString["s1"].ToUpper() + "%')";
        }
        if (Request.QueryString["s2"] != null)
        {
            where += (string.IsNullOrEmpty(where) ? "" : " AND ") + "( UPPER(a.name) like '%" + Request.QueryString["s2"].ToUpper() + "%')";
        }
        where = (string.IsNullOrEmpty(where) ? "" : " WHERE ") + where;
        para.Add(where);

        txt.Append(" </script>");

        string order = "a.name asc";
        if (Request.QueryString["order"] != null)
        {
            order = Request.QueryString["order"];
        }

        para.Add(order);

        if (Request.QueryString["si"] != null)
        {
            args.StartIndex = int.Parse(Request.QueryString["si"].ToString());
            currentIndex = int.Parse(Request.QueryString["si"].ToString());
        }

        if (Request.QueryString["rc"] != null)
            args.RowCount = int.Parse(Request.QueryString["rc"].ToString());

        List<LookupDTO> lookupList = commonClient.GetLookups("CustomerLookup", "", para, args, true);

        int rowcount = lookupList.Count == 0 ? 0 : lookupList[0].rowCount;
        txt.Append("<table border=\"0\" id=\"customerTable\" width=\"400px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");

        CommonUtility commonUtility = new CommonUtility();
        int lastPageNumber = commonUtility.PageCount(rowcount, ConfigUtil.RowCount);
        string com_url = (pageType.Equals("0") ? "getAllPrevcustcodeList" : "getAllPrevcustcode") +
            "('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=prevcustcode&si=0&rc=10&pg=" + pageType;

        string new_url = com_url + "&order=upper( a.cust_code ) "
            + (order.Contains("asc") && order.Contains("a.cust_code") ? " desc " : " asc") + "&s1='+$('#txtcustcode').val()+'&s2='+$('#txtname').val(),'');";

        txt.Append("<tr class=\"detail-popsheader\">");
        txt.Append("<td></td>");
        txt.Append("<td><a title='Next' href=\"javascript:void(0);\" onclick=\"" + new_url.Trim() + "\">Customer&nbsp;Code&nbsp;</a></td>");

        new_url = com_url + "&order=upper( a.name ) "
            + (order.Contains("asc") && order.Contains("a.name") ? " desc " : " asc") + "&s1='+$('#txtcustcode').val()+'&s2='+$('#txtname').val(),'');";

        txt.Append("<td><a title='Next' href=\"javascript:void(0);\" onclick=\"" + new_url.Trim() + "\">&nbsp;Name</a></td>");
        txt.Append("<tr>");

        txt.Append("<tr>");
        txt.Append("<td></td>");
        new_url = com_url + "&order=" + order + "&s1='+$('#txtcustcode').val()+'&s2='+$('#txtname').val(),'');";

        txt.Append("<td style=\"width:120px\"><div class=\"ui-widget\"> <input id=\"txtcustcode\" class=\"rgFilterBox\" type=\"text\" style=\"width: 60px\" />&nbsp;<input id=\"btncustcode\" type=\"button\" class=\"filterbtntest\" onclick=\"" + new_url.Trim() + "\"/> </div></td>");

        txt.Append("<td><div class=\"ui-widget\"> <input id=\"txtname\" class=\"rgFilterBox\" type=\"text\" style=\"width: 100px\" />&nbsp;<input id=\"btnname\" type=\"button\"  class=\"filterbtntest\" onclick=\"" + new_url.Trim() + "\"/> </div></td>");
        txt.Append("<tr>");

        if (lookupList.Count != 0)
        {
            for (int i = 0; i < lookupList.Count; i++)
            {
                txt.Append("<tr>");
                txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"id_prevcustocde\" value=" + lookupList[i].Code + " id=\"id_prevcustocde\"/>  </td>");
                txt.Append("<td class=\"tblborder\" align=\"left\">" + lookupList[i].Code + "</td>");
                txt.Append("<td class=\"tblborder\" align=\"left\">" + lookupList[i].Description + "</td>");
                txt.Append("</tr>");
            }
            txt.Append("</table>");

            txt.Append("<br />");

            txt.Append("<table border=\"0\" id=\"customerTable\" width=\"300px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr>");

            txt.Append(CreatePaginator(currentIndex, rowcount, pageType, "&order=" + order + "&s1='+$('#txtcustcode').val()+'&s2='+$('#txtname').val() + '"));

            txt.Append("</tr>");
            txt.Append("</table>");
        }
        return txt.ToString();
    }

    private string CreatePaginator(int currentPageNumber, int totrowcount, string pageType, string para)
    {
        StringBuilder sb = new StringBuilder();
        CommonUtility commonUtility = new CommonUtility();

        //#region Navigation
        //int rowCount = ConfigUtil.RowCount;
        //int lastPageNumber = commonUtility.PageCount(totrowcount, rowCount);

        //string new_url = "";


        //sb.Append("<div class=\"paging\">");
        //sb.Append("<ul>");

        //if (lastPageNumber > 1)
        //{
        //    new_url = (pageType.Equals("0") ? "getAllPrevcustcodeList" : "getAllPrevcustcode") + "('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=prevcustcode&si=" + (currentPageNumber - 1) + "&rc=" + rowCount + "&pg=" + pageType + para + "','');";

        //    if (currentPageNumber == 0)
        //        sb.Append("<li><a title='Previous' class=\"back_link\">«</a></li>");
        //    else
        //        sb.Append("<li><a title='Previous' class=\"back_link\" href=\"javascript:void(0);\" onclick=\"" + new_url.Trim() + "\">«</a></li>");
        //}

        //for (int i = GetInitialValue(currentPageNumber, lastPageNumber); i < NavigationCount(currentPageNumber, lastPageNumber, currentPageNumber); i++)
        //{
        //    new_url = (pageType.Equals("0") ? "getAllPrevcustcodeList" : "getAllPrevcustcode") + "('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=prevcustcode&si=" + (i - 1) + "&rc=" + rowCount + "&pg=" + pageType + para + "','');";

        //    if (i == (currentPageNumber + 1))
        //        sb.Append("<li><a class=\"current\" style=\"background-color:#FFFFFF\">" + i.ToString() + "</a></li>");
        //    else
        //        sb.Append("<li><a href=\"javascript:void(0);\"  onclick=\"" + new_url.Trim() + "\" >" + i.ToString() + "</a></li>");
        //}

        //if (lastPageNumber > 1)
        //{
        //    new_url = (pageType.Equals("0") ? "getAllPrevcustcodeList" : "getAllPrevcustcode") + "('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=prevcustcode&si=" + (currentPageNumber + 1) + "&rc=" + rowCount + "&pg=" + pageType + para + "','');";

        //    if ((currentPageNumber + 1) == lastPageNumber)
        //        sb.Append("<li><a title='Next' class=\"forw_link\">»</a></li>");
        //    else
        //        sb.Append("<li><a title='Next' class=\"forw_link\" href=\"javascript:void(0);\" onclick=\"" + new_url.Trim() + "\">»</a></li>");
        //}

        //sb.Append("</ul>");
        //sb.Append("<div class=\"clear\"></div>");
        //sb.Append("</div>");
        //#endregion

        return sb.ToString();
    }

    private int GetInitialValue(int startIndex, int lastPageNumber)
    {
        if ((lastPageNumber - 1) > startIndex)
        {
            //Set the initial value.(eg : 0+5)
            int index = startIndex + 6;

            //Maximum page number shold be 10, and lastpagenumber should ne greather than 10, otherwise show the whole 10 pages
            if (index >= 11 && lastPageNumber > 10)
            {
                int balaneIndex = index - 10;
                return balaneIndex + 1;
            }
            else
                return 1;
        }
        else
        {
            //When last page number less than 10.
            if (lastPageNumber < 10)
                return 1;
            else
                return startIndex - 4;
        }
    }

    private string GetMaillingAddress()
    {
        StringBuilder d = new StringBuilder();
        LeadAddressDTO la = new LeadAddressDTO();
        la.Address1 = "NEW Address1";
        la.Address2 = "Address2";
        la.City = "City";

        //string jsonResult = JavaScriptConvert.SerializeObject(customer);
        string json = new JavaScriptSerializer().Serialize(la);
        return json;
        
    }

    private string SaveMarketObjective()
    {
        StringBuilder sb = new StringBuilder();
        LeadCustomerClient leadCustomerClient = new LeadCustomerClient();
        string str = string.Empty;

        try
        {
            string selectedObjectiveId = Request.QueryString["selectedObjectiveId"];
            int marketId = Convert.ToInt32(Request.QueryString["marketId"]);
            string code = Request.QueryString["code"];
            string description = Request.QueryString["description"];

            MarketObjectiveDTO marketObjectiveDTO = new MarketObjectiveDTO();
            MarketDTO marketDTO = new MarketDTO();
            ObjectiveDTO objectiveDTO = new ObjectiveDTO();

            objectiveDTO.ObjectiveId = string.IsNullOrEmpty(selectedObjectiveId) ? 0 : Convert.ToInt32(selectedObjectiveId);
            objectiveDTO.Code = code;
            objectiveDTO.Description = description;
            objectiveDTO.Type = "NORM";
            objectiveDTO.CreatedBy = UserSession.Instance.UserName;
            objectiveDTO.LastModifiedBy = UserSession.Instance.UserName;

            if (IsObjectiveCodeExists(objectiveDTO))
                return "codeExists";

            marketDTO.MarketId = marketId;

            marketObjectiveDTO.Market = marketDTO;
            marketObjectiveDTO.Objective = objectiveDTO;
            marketObjectiveDTO.CreatedBy = UserSession.Instance.UserName;
            marketObjectiveDTO.LastModifiedBy = UserSession.Instance.UserName;

            bool status = leadCustomerClient.SaveMarketObjective(marketObjectiveDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private bool IsObjectiveCodeExists(ObjectiveDTO objectiveDTO)
    {
        LeadCustomerClient leadCustomerClient = new LeadCustomerClient();
        try
        {
            return leadCustomerClient.IsObjectiveCodeExists(objectiveDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private string GetUsersByCatergory()
    {
        StringBuilder txt = new StringBuilder();
        OriginatorClient originatorClient = new OriginatorClient();
        ArgsDTO args = new ArgsDTO();

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

        args.AdditionalParams = " dept_string = '" + Request.QueryString["deptString"].ToUpper() + "'";

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 1000;// ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        List<OriginatorDTO> originatorList = originatorClient.GetOriginatorsByCatergory(args);

        if (originatorList != null && originatorList.Count != 0)
        {
            txt.Append("<div style=\"height: 300px; max-width:350px; overflow-y: scroll;\">");
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"260px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("</tr>");

            for (int i = 0; i < originatorList.Count; i++)
            {
                txt.Append("<tr>");
                if (i == 0)
                    txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" checked name=\"group1\" value=\"" + originatorList[i].Name + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].UserName + "\" id=\"b\"/>   </td>");
                else
                    txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + originatorList[i].Name + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].UserName + "\" id=\"b\"/>   </td>");
                txt.Append("<td align=\"left\" class=\"tblborder\">" + originatorList[i].Name + "</td>");
                txt.Append("</tr>");
            }

            txt.Append("</table>");
            txt.Append("</div>");
            txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");
            txt.Append("<button id=\"jqi_state0_buttonCancel\" class=\"k-button\">Cancel</button>");
        }
        else
        {
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"270px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("<th>Name</th>");
            txt.Append("</tr>");
            txt.Append("</table>");
        }

        return txt.ToString();
    }

    private string GetAllDistributorList()
    {
        StringBuilder txt = new StringBuilder();
        OriginatorClient originatorClient = new OriginatorClient();
        ArgsDTO args = new ArgsDTO();

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

        args.AdditionalParams = " dept_string = '" + Request.QueryString["deptString"].ToUpper() + "'";

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 10000;// ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        List<OriginatorDTO> originatorList = originatorClient.GetAllDistributorList(args);

        if (originatorList != null && originatorList.Count != 0)
        {
            txt.Append("<div style=\"height: 300px; max-width:700px; overflow-y: scroll;\">");
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"860px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>DB Code</th><th>DB Name</th>");
            txt.Append("</tr>");

            for (int i = 0; i < originatorList.Count; i++)
            {
                txt.Append("<tr>");
                if (i == 0)
                {
                    txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" checked name=\"group1\" value=\"" + originatorList[i].Name + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].UserName + "\" id=\"b\"/>   </td>");
                }
                else
                {
                    txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + originatorList[i].Name + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].UserName + "\" id=\"b\"/>   </td>");
                }
                txt.Append("<td align=\"left\" class=\"tblborder\">" + originatorList[i].UserName + "</td>");
                txt.Append("<td align=\"left\" class=\"tblborder\">" + originatorList[i].Name + "</td>");
                txt.Append("</tr>");
            }

            txt.Append("</table>");
            txt.Append("</div>");
            txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");
            txt.Append("<button id=\"jqi_state0_buttonCancel\" class=\"k-button\">Cancel</button>");
        }
        else
        {
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"700px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("<th>Name</th>");
            txt.Append("</tr>");
            txt.Append("</table>");
        }

        return txt.ToString();
    }

    private string GetRepsList()
    {
        StringBuilder txt = new StringBuilder();
        OriginatorClient originatorClient = new OriginatorClient();
        ArgsDTO args = new ArgsDTO();

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

        args.AdditionalParams = " dept_string = 'DR'";

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 1000;// ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        List<OriginatorDTO> originatorList = originatorClient.GetOriginatorsByCatergory(args);

        if (originatorList != null && originatorList.Count != 0)
        {
            txt.Append("<div style=\"height: 300px; max-width:350px; overflow-y: scroll;\">");
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"260px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("</tr>");

            for (int i = 0; i < originatorList.Count; i++)
            {
                txt.Append("<tr>");
                if (i == 0)
                    txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" checked name=\"group1\" value=\"" + originatorList[i].Name + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].UserName + "\" id=\"b\"/>   </td>");
                else
                    txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + originatorList[i].Name + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].UserName + "\" id=\"b\"/>   </td>");
                txt.Append("<td align=\"left\" class=\"tblborder\">" + originatorList[i].Name + "</td>");
                txt.Append("</tr>");
            }

            txt.Append("</table>");
            txt.Append("</div>");
            txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");
            txt.Append("<button id=\"jqi_state0_buttonCancel\" class=\"k-button\">Cancel</button>");
        }
        else
        {
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"270px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("<th>Name</th>");
            txt.Append("</tr>");
            txt.Append("</table>");
        }

        return txt.ToString();
    }

    private string GetAllRepsList()
    {
        StringBuilder txt = new StringBuilder();
        MasterClient masterClient = new MasterClient();
        ArgsDTO args = new ArgsDTO();

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

        args.AdditionalParams = " dept_string = 'DR'";

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 1000;// ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        List<SRModel> originatorList = new List<SRModel>();
        originatorList = masterClient.GetAllSRDetails();

        if (originatorList != null && originatorList.Count != 0)
        {
            txt.Append("<div style=\"height: 300px; max-width:700px; overflow-y: scroll;\">");
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"860px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>SR Name</th><th>SR Code</th><th>Territory</th>");
            txt.Append("</tr>");

            for (int i = 0; i < originatorList.Count; i++)
            {
                txt.Append("<tr>");
                if (i == 0)
                {
                    txt.Append("<td style=\"padding-left:5px; width: 40px;\" class=\"tblborder\"> <input type=\"radio\" checked name=\"group1\" value=\"" + originatorList[i].RepName + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].Originator + "\" id=\"b\"/>   </td>");
                    
                }
                else
                {
                    txt.Append("<td style=\"padding-left:5px; width: 40px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + originatorList[i].RepName + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].Originator + "\" id=\"b\"/>   </td>");
                    
                }

                if (originatorList[i].Status == "A")
                {
                    txt.Append("<td align=\"left\" class=\"tblborder\" style=\" width: 220px;\">" + originatorList[i].RepName + "</td>");
                }
                else
                {
                    txt.Append("<td align=\"left\" class=\"tblborder \" style=\" width: 220px;\"><div class=\"search-table-inactive-text\">" + originatorList[i].RepName + "</div></td>");
                }

                txt.Append("<td align=\"left\" class=\"tblborder \" style=\" width: 100px;\">" + originatorList[i].RepCode + "</td>");
                txt.Append("<td align=\"left\" class=\"tblborder \" style=\" width: 200px;\">" + originatorList[i].TerritoryName + "</td>");
                txt.Append("</tr>");
            }

            txt.Append("</table>");
            txt.Append("</div>");
            txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");
            txt.Append("<button id=\"jqi_state0_buttonCancel\" class=\"k-button\">Cancel</button>");
        }
        else
        {
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"700px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("<th>Name</th>");
            txt.Append("</tr>");
            txt.Append("</table>");
        }

        return txt.ToString();
    }

    #endregion
}