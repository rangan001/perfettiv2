﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="DropDownTest2.aspx.cs" Inherits="lead_customer_transaction_DropDownTest2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div>
        <input id="cmbMailingAddresses2" style="width: 200px;" />
    </div>

 <script type="text/javascript">
     $(document).ready(function () {
         $("#cmbMailingAddresses2").kendoDropDownList({
             autoBind: false,
             dataTextField: "Address1",
             dataValueField: "Address1",
          //   headerTemplate: '<div class="dropdown-header">' +
          //                      '<span class="k-widget k-header">Photo</span>' +
          //                      '<span class="k-widget k-header">Contact info</span>' +
          //                  '</div>',
             valueTemplate: '<span>#:data.Address1#</span><span>#:data.Address2#</span>',
             template: '<span class="k-state-default">#:data.Address1#</span>' +' '+ '<span>#:data.Address2#</span>' + '<p></p>' +
                                  '<span class="k-state-default">#: data.City #</span>' + ' ' + '<span>#: data.State #</span>' + ' ' + '<span>#: data.PostCode #</span>' + '<p></p>' +
                                  '<span class="k-state-default">#:data.Country#</span>',
             dataSource: {
                 transport: {
                     read: {
                         url: ROOT_PATH + "service/lead_customer/common.asmx/GetAddressDropdownData",
                         contentType: "application/json; charset=utf-8",
                         type: "POST",
                         data: { custCode: '5WADANV0' }
                     },
                     parameterMap: function (data, operation) {
                         //data = $.extend( data);
                         
                         return JSON.stringify(data);

                     }
                 },
                 schema: {
                     data: "d"
                 }
             },

             optionLabel: "-Please Select-"
         });
     });
    </script>
</asp:Content>

