﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="contact_person_entry.aspx.cs" Inherits="lead_customer_transaction_contact_person_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <script type="text/javascript" src="../../assets/scripts/jquery.maskedinput.js"></script>
    <style type="text/css">
    .k-dropdown-wrap{ width:200px;}
    </style>
    <asp:HiddenField ID="HiddenField_CustCode" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_LeadId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_Address1" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_Address2" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_City" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_State" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_PostCode" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_Country" runat="server" ClientIDMode="Static" />
    <%--    <asp:HiddenField ID="HiddenField_OtherAddress1" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_OtherAddress2" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_OtherCity" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_OtherState" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_OtherPostCod" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HiddenField_OtherCountry" runat="server" ClientIDMode="Static" />--%>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/leadentry.aspx">
                            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="hoributton">
            <div class="clearall">
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div class="form">
            <div class="formleft_extended">
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label27" runat="server" Text="Type"></asp:Label></div>
                <div class="formdetaildiv">
                    <asp:DropDownList ID="cmbType" runat="server" CssClass="input-large">
                        <asp:ListItem Value="P" Text="Personal"></asp:ListItem>
                        <asp:ListItem Value="D" Text="Department"></asp:ListItem>
                        <asp:ListItem Value="C" Text="CofA"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label>
                </div>
                <div class="formdetaildiv">
                    <span class="wosub mand">
                        <asp:DropDownList ID="cmbTitle" runat="server" Width="80px">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtFirstName" runat="server" Width="80px" MaxLength="20"></asp:TextBox>
                        <b style="color: #FF0000; font-size: large">*</b> </span>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label2" runat="server" Text="Last Name"></asp:Label>
                </div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="20" CssClass="input-large"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label3" runat="server" Text="Phone"></asp:Label>
                </div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtPhone" runat="server" MaxLength="10" CssClass="input-large-phone"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label4" runat="server" Text="Mobile"></asp:Label>
                </div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtMobile" runat="server" MaxLength="14" CssClass="input-large-mobile"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label5" runat="server" Text="Fax"></asp:Label>
                </div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtFaxNumber" runat="server" MaxLength="10" CssClass="input-large-phone"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label6" runat="server" Text="Email"></asp:Label>
                </div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input-large" data-email-msg="Email format is not valid"
                        type="email" class="k-textbox"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label7" runat="server" Text="Comments"></asp:Label>
                </div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" onbeforepaste="doBeforePaste(this);"
                        onkeypress="doKeypress(this);" onpaste="doPaste(this);" Height="50px" onfocus="ClearText('MainContent_txtDescription');"
                        MaxLength="500" CssClass="input-large"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
            </div>
            <div class="formright_extended">
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label8" runat="server" Text="Organisation"></asp:Label>
                </div>
                <div class="formdetaildiv_extended">
                    <asp:TextBox ID="txtContact" runat="server" ReadOnly="true" CssClass="input-xlarge"></asp:TextBox>
                    <a href="#" id="A1">
                        <img id="img1" src="~/assets/images/popiconbw.png" alt="popicon" runat="server" border="0" /></a>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label9" runat="server" Text="Customer"></asp:Label>
                </div>
                <div class="formdetaildiv_extended">
                    <asp:TextBox ID="txtCustomer" runat="server" ReadOnly="true" CssClass="input-xlarge"></asp:TextBox>
                    <a href="#" id="A2">
                        <img id="img2" src="~/assets/images/popiconbw.png" alt="popicon" runat="server" border="0" /></a>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label28" runat="server" Text="End User"></asp:Label>
                </div>
                <div class="formdetaildiv_extended">
                    <asp:TextBox ID="txtEnduser" runat="server" ReadOnly="true" CssClass="input-xlarge"></asp:TextBox>
                    <a href="#" id="A3">
                        <img id="img3" src="~/assets/images/popiconbw.png" alt="popicon" runat="server" border="0" /></a>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label10" runat="server" Text="Position"></asp:Label>
                </div>
                <div class="formdetaildiv_extended">
                    <asp:TextBox ID="txtPosition" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label11" runat="server" Text="Reports To"></asp:Label>
                </div>
                <div class="formdetaildiv_extended">
                    <asp:TextBox ID="txtReportsTo" runat="server" CssClass="input-xlarge" MaxLength="40"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:Label ID="Label12" runat="server" Text="Special Interest"></asp:Label>
                </div>
                <div class="formdetaildiv_extended">
                    <asp:TextBox ID="txtSpecialInterest" runat="server" TextMode="MultiLine" Height="50px"
                        onfocus="ClearText('MainContent_txtSpecialInterest');" MaxLength="500" CssClass="input-xlarge"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    <asp:CheckBox ID="chkKeyContact" runat="server" />
                    Key Contact
                </div>
                <%--        <div class="squaredOne">
	<input type="checkbox" value="None" id="squaredOne" name="check" />
	<label for="squaredOne"></label>
</div>--%>
                <div class="formdetaildiv">
                </div>
            </div>
            <div class="clearall">
            </div>
            <div runat="server" id="tbCreated" class="specialevents" align="right">
            </div>
            <br />
            <div runat="server" id="tbModified" class="specialevents" align="right">
            </div>
            <asp:Label ID="txtOriginator" runat="server"></asp:Label>
            <div id="forecast">
                <div id="tabstrip">
                    <ul>
                        <li class="k-state-active">Address </li>
                        <li>Image </li>
                    </ul>
                    <div>
                        <div class="formleft">
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label13" runat="server" Text="Mailing Address"></asp:Label>
                            </div>
                            <div class="formdetaildiv">
                                <asp:DropDownList ID="cmbMailingAddresses" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cmbMailingAddresses_SelectedIndexChanged"
                                    CssClass="input-large1-select" Visible="false">
                                </asp:DropDownList>
                                <input id="cmbMailingAddresses2" style="width: 300px;" />
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label15" runat="server" Text="Street"></asp:Label>
                            </div>
                            <div class="formdetaildiv">
                                <asp:TextBox ID="txtMailingStreet" runat="server" TextMode="MultiLine" Height="50px"
                                    CssClass="input-large1-textarea"></asp:TextBox>
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label17" runat="server" Text="City"></asp:Label>
                            </div>
                            <div class="formdetaildiv">
                                <asp:TextBox ID="txtMailingCity" runat="server" CssClass="input-large1-textbox"></asp:TextBox>
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label19" runat="server" Text="State"></asp:Label>
                            </div>
                            <div class="formdetaildiv">
                                <asp:DropDownList ID="cmbMailingState" runat="server" CssClass="input-large1-select">
                                </asp:DropDownList>
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label21" runat="server" Text="Postcode"></asp:Label>
                            </div>
                            <div class="formdetaildiv">
                                <asp:TextBox ID="txtMailingPostcode" runat="server" CssClass="input-large1-textbox"></asp:TextBox>
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label23" runat="server" Text="Country"></asp:Label>
                            </div>
                            <div class="formdetaildiv">
                                <asp:DropDownList ID="cmbMailingCountry" runat="server" CssClass="input-large1-select">
                                </asp:DropDownList>
                            </div>
                            <div class="clearall">
                            </div>
                        </div>
                        <div class="formright">
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label14" runat="server" Text="Other Address"></asp:Label>
                            </div>
                            <div class="formdetaildiv">
                                <asp:DropDownList ID="cmbOtherAddress" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cmbOtherAddress_SelectedIndexChanged"
                                    CssClass="input-large1-select" Visible="false">
                                </asp:DropDownList>
                                <input id="cmbOtherAddress2" style="width: 300px;" />
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label16" runat="server" Text="Street"></asp:Label>
                            </div>
                            <div class="formdetaildiv_right">
                                <asp:TextBox ID="txtOtherStreet" runat="server" TextMode="MultiLine" Height="50px"
                                    CssClass="input-large1-textarea"></asp:TextBox>
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label18" runat="server" Text="City"></asp:Label>
                            </div>
                            <div class="formdetaildiv_right">
                                <asp:TextBox ID="txtOtherCity" runat="server" CssClass="input-large1-textbox"></asp:TextBox>
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label20" runat="server" Text="State"></asp:Label>
                            </div>
                            <div class="formdetaildiv_right">
                                <asp:DropDownList ID="cmbOtherState" runat="server" CssClass="input-large1-select">
                                </asp:DropDownList>
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label22" runat="server" Text="Postcode"></asp:Label>
                            </div>
                            <div class="formdetaildiv_right">
                                <asp:TextBox ID="txtOtherPostcode" runat="server" CssClass="input-large1-textbox"></asp:TextBox>
                            </div>
                            <div class="clearall">
                            </div>
                            <div class="formtextdiv_extended">
                                <asp:Label ID="Label24" runat="server" Text="Country"></asp:Label>
                            </div>
                            <div class="formdetaildiv_right">
                                <asp:DropDownList ID="cmbOtherCountry" runat="server" CssClass="input-large1-select">
                                </asp:DropDownList>
                            </div>
                            <div class="clearall">
                            </div>
                        </div>
                        <div class="clearall">
                        </div>
                    </div>
                    <div>
                        <div style="padding-left: 10px; padding-right: 10px;">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label25" runat="server" Text="Select Image"></asp:Label>
                                        <%--<input type="file" name="file_upload" id="file_upload" />--%>
                                    </td>
                                    <td>
                                        <input class="upload" name="file_upload" id="file_upload" type="file" upload-id="02ebeebf-98aa-459b-b41f-49028fa37e9c" />
                                    </td>
                                    <td>
                                        <span style="display: inline-block; float: left;">
                                            <asp:Label ID="Label26" runat="server" Text="Supported Formats : JPG, GIF, TIFF, PNG, BMP"></asp:Label></span>
                                    </td>
                                    <td rowspan="2">
                                        <div id="div_preview" style="overflow: auto;" runat="server">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="txtContactID" runat="server" />
            <asp:HiddenField ID="hfImageName" runat="server" />
            <asp:HiddenField ID="txtImageId" runat="server" />
            <asp:HiddenField ID="hfSessionId" runat="server" />
        </div>
    </div>
    <script type="text/javascript">

        hideStatusDiv("MainContent_div_message");

        jQuery("#MainContent_txtFirstName").validate({
            expression: "if (VAL) return true; else return false;",
            message: "Please Enter FirstName."
        });

        jQuery(function ($) {
            $('input.input_numeric').autoNumeric({ aSep: null, aDec: null, mDec: 0 });

        });

        //        jQuery(function ($) {
        //            $('input.input-large-phone').mask("9?9999999999999");

        //        });

        //        jQuery(function ($) {
        //            $('input.input_fax').mask("9?999999999");

        //        });

        jQuery(function ($) {
            $.mask.definitions['~'] = '[+0123456789]';
            $('input.input-large-mobile').mask("~9?999999999999");
        });

        jQuery(function ($) {
            $('input.input-large-phone').mask("9?999999999");
        });

        $(document).ready(function () {
            LoadContactPersonEntry();
        });

        $("#MainContent_txtDescription").blur(function () {
            var text = $("#MainContent_txtDescription").val();
            if (text == "" || text == null || text == undefined) {
                $("#MainContent_txtDescription").val('(Max 500 Characters)');
            } else if (text.indexOf('(Max 500 Characters)') == 0) {
                $("#MainContent_txtDescription").val();
            } else {
                $("#MainContent_txtDescription").val(text);
            }
        });

        $("#MainContent_txtSpecialInterest").blur(function () {
            var text = $("#MainContent_txtSpecialInterest").val();
            if (text == "" || text == null || text == undefined) {
                $("#MainContent_txtSpecialInterest").val('(Max 500 Characters)');
            } else if (text.indexOf('(Max 500 Characters)') == 0) {
                $("#MainContent_txtSpecialInterest").val();
            } else {
                $("#MainContent_txtSpecialInterest").val(text);
            }
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {

            $("#cmbMailingAddresses2").kendoDropDownList({
                autoBind: false,
                dataTextField: "Address1",
                select: onSelect,
                dataValueField: "Address1",
                //   headerTemplate: '<div class="dropdown-header">' +
                //                      '<span class="k-widget k-header">Photo</span>' +
                //                      '<span class="k-widget k-header">Contact info</span>' +
                //                  '</div>',
                valueTemplate: '<span>#:data.Address1#</span><span>#:data.Address2#</span>',
                template: '<div class="select-div-box"><span><h6>#:data.Address1#</h6></span>' + '\n' + '<span><h6>#:data.Address2#</h6></span>' + '<p></p>' +
                                  '<span>#: data.City #</span>' + ' ' + '<span>#: data.State #</span>' + ' ' + '<span>#: data.PostCode #</span>' + '<p></p>' +
                                  '<span>#:data.Country#</span> </div>',
                dataSource: {
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAddressDropdownData",
                            contentType: "application/json; charset=utf-8",
                            type: "POST",
                            data: { custCode: $('#HiddenField_CustCode').val(), leadId: $('#HiddenField_LeadId').val() }
                        },
                        parameterMap: function (data, operation) {
                            //data = $.extend( data);
                            return JSON.stringify(data);
                        }
                    },
                    schema: {
                        data: "d"
                    }
                }

                //optionLabel: "-Select Address-"
            });
  


            $("#cmbOtherAddress2").kendoDropDownList({
                autoBind: false,
                dataTextField: "Address1",
                select: onSelectOtherAddress,
                dataValueField: "Address1",
                //   headerTemplate: '<div class="dropdown-header">' +
                //                      '<span class="k-widget k-header">Photo</span>' +
                //                      '<span class="k-widget k-header">Contact info</span>' +
                //                  '</div>',
                valueTemplate: '<span>#:data.Address1#</span><span>#:data.Address2#</span>',
                template: '<div class="select-div-box"><span><h6>#:data.Address1#</h6></span>' + '\n' + '<span><h6>#:data.Address2#</h6></span>' + '<p></p>' +
                                  '<span>#: data.City #</span>' + ' ' + '<span>#: data.State #</span>' + ' ' + '<span>#: data.PostCode #</span>' + '<p></p>' +
                                  '<span>#:data.Country#</span> </div>',
                dataSource: {
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAddressDropdownData",
                            contentType: "application/json; charset=utf-8",
                            type: "POST",
                            data: { custCode: $('#HiddenField_CustCode').val(), leadId: $('#HiddenField_LeadId').val() }
                        },
                        parameterMap: function (data, operation) {
                            //data = $.extend( data);
                            return JSON.stringify(data);
                        }
                    },
                    schema: {
                        data: "d"
                    }
                },

              //  optionLabel: "-Select Address-"
            });
        });

      function onSelectOtherAddress(e) {

                var dataItem = this.dataItem(e.item.index());
                $("#HiddenField_Address1").val(dataItem.Address1);
                $("#HiddenField_Address2").val(dataItem.Address2);
                $("#HiddenField_City").val(dataItem.City);
                $("#HiddenField_State").val(dataItem.State);
                $("#HiddenField_PostCode").val(dataItem.PostCode);
                $("#HiddenField_Country").val(dataItem.Country);

                if (dataItem.Address1 != null || dataItem.Address1 != undefined || dataItem.Address2 != null || dataItem.Address2 != undefined) {
                    $("#MainContent_txtOtherStreet").val(dataItem.Address1 + ' ' + dataItem.Address2);
                }
                else if (dataItem.Address1 != null || dataItem.Address1 != undefined) {
                    $("#MainContent_txtOtherStreet").val(dataItem.Address1);
                }
                else if (dataItem.Address2 != null || dataItem.Address2.val() != undefined) {
                    $("#MainContent_txtOtherStreet").val(dataItem.Address2);
                }

                if (dataItem.City != null || dataItem.City != undefined) {
                    $("#MainContent_txtOtherCity").val(dataItem.City);
                }

                if (dataItem.State != null || dataItem.State != undefined) {
                    $("#MainContent_cmbOtherState").val(dataItem.State);
                }

                if (dataItem.PostCode != null || dataItem.PostCode != undefined) {
                    $("#MainContent_txtOtherPostcode").val(dataItem.PostCode);
                }

                if (dataItem.Country != null || dataItem.Country != undefined) {
                    $("#MainContent_cmbOtherCountry").val(dataItem.Country);
                }
                 }

            function onSelect(e) {

                var dataItem = this.dataItem(e.item.index());
                $("#HiddenField_Address1").val(dataItem.Address1);
                $("#HiddenField_Address2").val(dataItem.Address2);
                $("#HiddenField_City").val(dataItem.City);
                $("#HiddenField_State").val(dataItem.State);
                $("#HiddenField_PostCode").val(dataItem.PostCode);
                $("#HiddenField_Country").val(dataItem.Country);

                if (dataItem.Address1 != null || dataItem.Address1 != undefined || dataItem.Address2 != null || dataItem.Address2 != undefined) {
                    $("#MainContent_txtMailingStreet").val(dataItem.Address1 + ' ' + dataItem.Address2);
                }
                else if (dataItem.Address1 != null || dataItem.Address1 != undefined) {
                    $("#MainContent_txtMailingStreet").val(dataItem.Address1);
                }
                else if (dataItem.Address2 != null || dataItem.Address2.val() != undefined) {
                    $("#MainContent_txtMailingStreet").val(dataItem.Address2);
                }

                if (dataItem.City != null || dataItem.City != undefined) {
                    $("#MainContent_txtMailingCity").val(dataItem.City);
                }

                if (dataItem.State != null || dataItem.State != undefined) {
                    $("#MainContent_cmbMailingState").val(dataItem.State);
                }

                if (dataItem.PostCode != null || dataItem.PostCode != undefined) {
                    $("#MainContent_txtMailingPostcode").val(dataItem.PostCode);
                }

                if (dataItem.Country != null || dataItem.Country != undefined) {
                    $("#MainContent_cmbMailingCountry").val(dataItem.Country);
                }
                 }

                //            var root = "../process_forms/processmaster.aspx?fm=contactpersonaddress&type=get";

                //            $.ajax({
                //                url: root,
                //                dataType: "json",
                //                cache: false,
                //                success: function (msg) {
                //                    if (msg != "") {

                //                        //var txt = msg.split(",");
                //                        var customeraddress = eval(msg);
                //                        if (customeraddress != null && customeraddress != undefined) {
                //                            $("#MainContent_txtMailingStreet").val(customeraddress.Address1);
                //                        }


                //                    }
                //                    else {

                //                    }
                //                },
                //                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                //                error: function (msg, textStatus) {
                //                    if (textStatus == "error") {
                //                        var msg = msg; // "Sorry there was an error: ";
                //                        //$("#" + tagertdiv).html(msg);
                //                    }
                //                }
                //            });


           
       
    </script>
</asp:Content>
