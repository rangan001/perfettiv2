﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;

public partial class lead_customer_transaction_contact_person_entry : PageBase
{
    bool have = false;
    CommonUtility commonUtility = new CommonUtility();

    #region Properties
    public string ContactPersonId
    {
        get
        {
            if (ViewState[CommonUtility.CONTACTPERSON_ID] != null && ViewState[CommonUtility.CONTACTPERSON_ID] != string.Empty)
            {
                return ViewState[CommonUtility.CONTACTPERSON_ID].ToString();
            }
            return string.Empty;
        }
        set { ViewState[CommonUtility.CONTACTPERSON_ID] = value; }
    }
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        txtDescription.Attributes.Add("maxLength", txtDescription.MaxLength.ToString());
        buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
        buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
        buttonbar.VisibleSaveandNew(false);
        buttonbar.VisibleOpenContact(false);
        buttonbar.VisibleSave(true);
        buttonbar.VisibleDeactivate(true);

        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                Master.SetBreadCrumb("Contact Person Entry ", "#", "");
                hfSessionId.Value = Session.SessionID;
                string customerCode = "";

                txtDescription.Text = "(Max 500 Characters)";
                txtSpecialInterest.Text = "(Max 500 Characters)";

                LoadOptions();

                if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty && int.Parse(Request.QueryString["leadid"]) != 0)
                {
                    ViewState[CommonUtility.LEAD_ID] = Request.QueryString["leadid"];
                    ViewState[CommonUtility.ENDUSER_CODE] = null;
                    ViewState[CommonUtility.CUSTOMER_CODE] = null;

                    HiddenField_LeadId.Value = Request.QueryString["leadid"];

                    if (Request.QueryString["personid"] != null && Request.QueryString["personid"] != string.Empty && int.Parse(Request.QueryString["personid"]) != 0)
                    {
                        ContactPersonId = Request.QueryString["personid"];
                        OpenContactPerson();
                    }

                    LoadLeadAddressGrid();
                    LoadRelatedData();

                    //hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&ref=per#tabs";
                }
                
                else if((Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
                    && ((Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"]))))
                {
                    //End User Section.
                    ViewState[CommonUtility.ENDUSER_CODE] = Request.QueryString["eduid"];
                    ViewState[CommonUtility.CUSTOMER_CODE] = Request.QueryString["custid"];
                    ViewState[CommonUtility.LEAD_ID] = null;

                    HiddenField_CustCode.Value = Server.HtmlDecode(Request.QueryString["custid"]);
                    

                    if (Request.QueryString["personid"] != null && Request.QueryString["personid"] != string.Empty && int.Parse(Request.QueryString["personid"]) != 0)
                    {
                        ContactPersonId = Request.QueryString["personid"];
                        OpenContactPerson();
                    }

                    //if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty && Request.QueryString["eduid"] != null && Request.QueryString["eduid"] != string.Empty) 
                    //{
                    //    ContactPersonId = Request.QueryString["personid"];
                    //    OpenContactPerson();
                    //}

                    
                    HideAddressCmbBox();
                    LoadRelatedData();
                }
                else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
                {
                    ViewState[CommonUtility.CUSTOMER_CODE] = Request.QueryString["custid"];
                    customerCode = Request.QueryString["custid"];
                    
                    ViewState[CommonUtility.LEAD_ID] = null;
                    ViewState[CommonUtility.ENDUSER_CODE] = null;

                    HiddenField_CustCode.Value = Server.HtmlDecode(Request.QueryString["custid"]);

                    // Edit Contact
                    //if (Request.QueryString["personid"] != null && Request.QueryString["personid"] != string.Empty && int.Parse(Request.QueryString["personid"]) != 0)
                    //{
                    if (Request.QueryString["Origin"] != null && !string.IsNullOrEmpty(Request.QueryString["Origin"]) &&
                        Request.QueryString["ContactType"] != null && !string.IsNullOrEmpty(Request.QueryString["ContactType"]))
                    {
                        //ContactPersonId = Request.QueryString["personid"];
                        ViewState["Origin"] = Request.QueryString["Origin"];
                        ViewState["ContactType"] = Request.QueryString["ContactType"];
                        OpenContactPerson();
                    }

                    LoadLeadAddressGrid();
                    LoadRelatedData();

                    //hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&ref=per#tabs";
                }
                SetBackLink();
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //byte[] rt = ConvertFile(FileUpload1.FileName);
    }

    protected void cmbMailingAddresses_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbMailingAddresses.SelectedItem != null)
        {
            cmbMailingAddresses.SelectedValue = "";

            CustomerClient oCustomer = new CustomerClient();
            //ViewState[CommonUtility.LEAD_ID] 
            List<LeadAddressDTO> leadAddressList = new List<LeadAddressDTO>();
            if (ViewState[CommonUtility.CUSTOMER_CODE] != null)
                leadAddressList = oCustomer.GetCustomerAddresses(ViewState[CommonUtility.CUSTOMER_CODE].ToString(),new ArgsDTO());
            else
                leadAddressList = new LeadClient().GetLeadAddreses(int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()));

            LeadAddressDTO address = leadAddressList.Where(p => (p.Address1 + " " + p.Address2 + " " + p.City + " " + p.State + " " + p.Country) == cmbMailingAddresses.Text).FirstOrDefault();

            txtMailingCity.Text = HttpUtility.HtmlDecode(address.City);
            if (cmbMailingCountry.Items.Contains(new ListItem(address.Country.Trim())))
                cmbMailingCountry.SelectedValue = address.Country.Trim();
            else
                cmbMailingCountry.Items.Add(new ListItem(address.Country.Trim()));

            txtMailingPostcode.Text = HttpUtility.HtmlDecode(address.PostCode);
            if (cmbMailingState.Items.Contains(new ListItem(address.State.Trim())))
                cmbMailingState.SelectedValue = address.State.Trim();
            else
                cmbMailingState.Items.Add(new ListItem(address.State.Trim()));

            txtMailingStreet.Text = HttpUtility.HtmlDecode(address.Address1) + "\n" + HttpUtility.HtmlDecode(address.Address2);
        }
    }

    protected void cmbOtherAddress_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbOtherAddress.SelectedItem != null)
        {
            CustomerClient oCustomer = new CustomerClient();

            //List<LeadAddressDTO> leadAddressList = oCustomer.GetCustomerAddresses(ViewState[CommonUtility.CUSTOMER_CODE].ToString());
            List<LeadAddressDTO> leadAddressList = new List<LeadAddressDTO>();
            if (ViewState[CommonUtility.CUSTOMER_CODE] != null)
                leadAddressList = oCustomer.GetCustomerAddresses(ViewState[CommonUtility.CUSTOMER_CODE].ToString(),new ArgsDTO());
            else
                leadAddressList = new LeadClient().GetLeadAddreses(int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()));
            LeadAddressDTO address = leadAddressList.Where(p => p.Address1 == cmbOtherAddress.Text).FirstOrDefault();

            txtOtherCity.Text = HttpUtility.HtmlDecode(address.City);
            cmbOtherCountry.Text = address.Country.Trim();
            txtOtherPostcode.Text = HttpUtility.HtmlDecode(address.PostCode);
            cmbOtherState.Text = address.State.Trim();
            txtOtherStreet.Text = HttpUtility.HtmlDecode(address.Address1) + "\n" + HttpUtility.HtmlDecode(address.Address2);
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        SaveContact();
    }

    protected void ButtonClear_Click(object sender)
    {
        ClearContact();
    }
    #endregion

    #region Methods

    private void SetBackLink()
    {
        //bool isfrmHome = false;

        if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty && int.Parse(Request.QueryString["leadid"]) != 0)
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
                hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&fm=" + Request.QueryString["fm"] + "&ty=" + Request.QueryString["ty"] + "&ref=per#tabs";
            else
                hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&fm=lead&ref=per#tabs";


        }
        else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
        {
            if (Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"]))
            {
                if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
                    hlBack.NavigateUrl = "~/lead_customer/transaction/enduser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&fm=" + Request.QueryString["fm"] + "&ref=per#tabs";
                else
                    hlBack.NavigateUrl = "~/lead_customer/transaction/enduser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&fm=endUser&ref=per#tabs";
            }
            else
            {
                if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)

                    hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Server.UrlEncode(Request.QueryString["custid"]) + "&fm=" + Request.QueryString["fm"] + "&ref=per#tabs";
                else
                {
                    hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Server.UrlEncode(Request.QueryString["custid"]) + "&fm=lead" + "&ty=customer" + "&ref=per#tabs";
                }
            }
        }

        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] == "search")
        {
            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/search.aspx";
        }
    }
    private void LoadOptions()
    {
        try
        {
            // Load the First Name
            DataTable dtFirstName = new DataTable();
            dtFirstName.Columns.Add("FirstNameCode", Type.GetType("System.String"));
            dtFirstName.Columns.Add("FirstName", Type.GetType("System.String"));

            List<string> sArrFirstName = new List<string>();
            sArrFirstName.Add("None");
            sArrFirstName.Add("Mr");
            sArrFirstName.Add("Ms");
            sArrFirstName.Add("Mrs");
            sArrFirstName.Add("Dr");
            sArrFirstName.Add("Prof");

            cmbTitle.DataSource = sArrFirstName;
            cmbTitle.DataBind();

            cmbTitle.SelectedValue = "None";

            CommonClient commonClient = new CommonClient();
            List<CountryDTO> countryList = commonClient.GetCountry();
            countryList.Insert(0, new CountryDTO() { CountryName = "", CountryId = 0 });

            foreach (CountryDTO item in countryList)
            {
                cmbMailingCountry.Items.Add(item.CountryName);
                cmbOtherCountry.Items.Add(item.CountryName);
            }

            List<StateDTO> stateList = commonClient.GetAllStates();
            stateList.Insert(0, new StateDTO() { StateName = "", StateID = 0 });

            foreach (StateDTO item in stateList)
            {
                cmbMailingState.Items.Add(item.StateName);
                cmbOtherState.Items.Add(item.StateName);
            }

        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    public void OpenContactPerson()
    {
        CustomerClient oContact = new CustomerClient();
        EndUserClient eContact = new EndUserClient();
        
        //IngresDataReader idrContact = null;

        try
        {
            ContactPersonDTO contactPerson = null;

            if ((ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty) && (ViewState[CommonUtility.ENDUSER_CODE] != null && ViewState[CommonUtility.ENDUSER_CODE] != string.Empty))
            {
                contactPerson = eContact.GetEndUserContactPerson(Convert.ToInt32(ContactPersonId));
            }

            else if (ViewState[CommonUtility.CUSTOMER_CODE] == null || ViewState[CommonUtility.CUSTOMER_CODE]==string.Empty)
            {
                contactPerson = oContact.GetContactPerson(Convert.ToInt32(ContactPersonId));
            }

            else if (ViewState[CommonUtility.CUSTOMER_CODE] != null || ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty)
            {
                contactPerson = oContact.GetCustomerContact(ViewState[CommonUtility.CUSTOMER_CODE].ToString(), ViewState["ContactType"].ToString(),ViewState["Origin"].ToString());  
                        
            }
            else
            {
                contactPerson = null;// oContact.GetCustomerContact(Convert.ToInt32(ContactPersonId), ViewState[CommonUtility.CUSTOMER_CODE].ToString());
                if (contactPerson.CreatedBy != UserSession.Instance.UserName)
                {
                    buttonbar.EnableSave(false);
                    buttonbar.EnableDeactivate(false);
                   // btnSave.IsEnabled = false;
                    //btnClear.IsEnabled = false;
                }
            }

            if (contactPerson != null)
            {
                ContactPersonId = contactPerson.ContactPersonID.ToString();
                ViewState[CommonUtility.LEAD_ID] = contactPerson.LeadID;
                txtEmail.Text = contactPerson.EmailAddress;
                cmbTitle.SelectedValue = contactPerson.Title != "" ? contactPerson.Title : "None";
                txtFirstName.Text = HttpUtility.HtmlDecode(contactPerson.FirstName);
                txtLastName.Text = HttpUtility.HtmlDecode(contactPerson.LastName);
                txtMailingCity.Text = HttpUtility.HtmlDecode(contactPerson.MailingCity);
                cmbMailingCountry.Text = contactPerson.MailingCountry;
                txtMailingPostcode.Text = HttpUtility.HtmlDecode(contactPerson.MailingPostcode);
                cmbMailingState.Text = contactPerson.MailingState;
                txtMailingStreet.Text = HttpUtility.HtmlDecode(contactPerson.MailingAddress);
                txtMobile.Text = HttpUtility.HtmlDecode(contactPerson.Mobile.Trim());
               // txtOriginator.Text = contactPerson.Originator;
                txtOtherCity.Text = HttpUtility.HtmlDecode(contactPerson.OtherCity);
                cmbOtherCountry.Text = contactPerson.OtherCountry;
                txtOtherPostcode.Text = HttpUtility.HtmlDecode(contactPerson.OtherPostCode);
                cmbOtherState.Text = contactPerson.OtherState;
                txtOtherStreet.Text = HttpUtility.HtmlDecode(contactPerson.OtherAddress);
                txtPhone.Text = HttpUtility.HtmlDecode(contactPerson.Telephone);
                txtPosition.Text = HttpUtility.HtmlDecode(contactPerson.Position);
                txtReportsTo.Text = HttpUtility.HtmlDecode(contactPerson.ReportsTo);
                txtDescription.Text = HttpUtility.HtmlDecode(contactPerson.Description);
                txtFaxNumber.Text = HttpUtility.HtmlDecode(contactPerson.Fax);
                txtSpecialInterest.Text = HttpUtility.HtmlDecode(contactPerson.SpecialInterests);
                if (contactPerson.KeyContact == "Y")
                    chkKeyContact.Checked = true;
/*
                //LoadImage(contactPerson.ImagePath);
                LoadImage();
                // CREATED BY / DATE and LAST MODIFIED BY / DATE
                if (!string.IsNullOrEmpty(contactPerson.ImagePath))
                    existingImagePath = contactPerson.ImagePath.Replace(@"\\", @"\");
*/
                tbCreated.InnerHtml = "Created By: " + contactPerson.CreatedBy + ", " +
                    contactPerson.CreatedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");

                tbModified.InnerHtml = "Last Modified By: " + contactPerson.LastModifiedBy + ", " +
                    contactPerson.LastModifiedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");

                if (contactPerson.LeadName != null)
                    txtContact.Text = contactPerson.LeadName;

                if (string.IsNullOrWhiteSpace(contactPerson.FirstName))
                {
                    txtFirstName.Text = contactPerson.Contact;
                }

                if (contactPerson.ContactType == "P")
                {
                    cmbType.SelectedValue = "P";
                    //cmbType.SelectedItem.Text = "Personal";
                }
                else if (contactPerson.ContactType == "D" && contactPerson.Origin == UserSession.Instance.ClientType)
                {
                    cmbType.SelectedValue = "D";
                    //cmbType.SelectedItem.Text = "Department";
                }
                else
                {
                    cmbType.SelectedValue = "C";
                    //cmbType.SelectedItem.Text = "CofA";
                }

                //Load the image data
                if ((ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty) && (ViewState[CommonUtility.ENDUSER_CODE] != null && ViewState[CommonUtility.ENDUSER_CODE] != string.Empty))
                LoadEndUserImage(eContact);
                else
                LoadImage(oContact);
            }
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void LoadImage(CustomerClient oContact)
    {
        try
        {
            string path = HttpContext.Current.Request.PhysicalApplicationPath + "docs";

            LeadClient leadClient = new LeadClient();
            ContactPersonImageDTO image = new ContactPersonImageDTO();
            if (ViewState[CommonUtility.CUSTOMER_CODE] == null || ViewState[CommonUtility.CUSTOMER_CODE]==string.Empty)
                image = leadClient.GetLeadContactPersonImage(int.Parse(ContactPersonId), int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()), path, Session.SessionID.ToString());
            else// if (!string.IsNullOrWhiteSpace(this.CustomerCode))
                image = oContact.GetCustomerContactPersonImage(int.Parse(ContactPersonId), ViewState[CommonUtility.CUSTOMER_CODE].ToString(), path, Session.SessionID.ToString());
            div_preview.InnerHtml = " <img src='../../docs/contactperson/"+ image.FileName + "'>";
            txtImageId.Value = image.ContactPersonImageId.ToString();
        }
        catch (Exception ex)
        {
            txtImageId.Value = "0";

            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void LoadEndUserImage(EndUserClient eContact)
    {
        
        try
        {
            string path = HttpContext.Current.Request.PhysicalApplicationPath + "docs";

            ContactPersonImageDTO image = new ContactPersonImageDTO();

            image = eContact.GetEndUserContactPersonImage(int.Parse(ContactPersonId), ViewState[CommonUtility.ENDUSER_CODE].ToString(), path, Session.SessionID.ToString());
            div_preview.InnerHtml = " <img src='../../docs/contactperson/" + image.FileName + "'>";
            txtImageId.Value = image.ContactPersonImageId.ToString();
        }
        catch (Exception ex)
        {
            txtImageId.Value = "0";

            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    public void LoadLeadAddressGrid()
    {
        LeadClient leadAddress = new LeadClient();
        CustomerClient oCustomer = new CustomerClient();
        try
        {
            if (ViewState[CommonUtility.LEAD_ID] != null && int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()) > 0)
            {
                List<LeadAddressDTO> addressList = leadAddress.GetLeadAddreses(int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()));
                LoadAddress(addressList);
            }
            else if (ViewState[CommonUtility.CUSTOMER_CODE] != null && !string.IsNullOrWhiteSpace(ViewState[CommonUtility.CUSTOMER_CODE].ToString()))
            {
                List<LeadAddressDTO> addressList = oCustomer.GetCustomerAddresses(ViewState[CommonUtility.CUSTOMER_CODE].ToString(),new ArgsDTO());
                LoadAddress(addressList);
            }
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void LoadAddress(List<LeadAddressDTO> addressList)
    {
        cmbMailingAddresses.Items.Add("");
        cmbOtherAddress.Items.Add("");
        foreach (LeadAddressDTO item in addressList)
        {
            cmbMailingAddresses.Items.Add(item.Address1+" "+item.Address2+" "+item.City+" "+item.State+" "+item.Country);
            cmbOtherAddress.Items.Add(item.Address1 + " " + item.Address2 + " " + item.City + " " + item.State + " " + item.Country);
        }
    }

    private void LoadRelatedData()
    {
        if (ViewState[CommonUtility.LEAD_ID] != null && int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()) != 0)
        {
            GetLeadDetails(ViewState[CommonUtility.LEAD_ID].ToString());
            //btnCustomer.IsEnabled = false;
            //btnContact.IsEnabled = false;
            //hasParentContact = true;
        }

        else if (ViewState[CommonUtility.ENDUSER_CODE] != null && !string.IsNullOrWhiteSpace(ViewState[CommonUtility.CUSTOMER_CODE].ToString()) && ViewState[CommonUtility.CUSTOMER_CODE] != null && !string.IsNullOrWhiteSpace(ViewState[CommonUtility.CUSTOMER_CODE].ToString()))
        {
            GetEndUserDetails(ViewState[CommonUtility.ENDUSER_CODE].ToString(), ViewState[CommonUtility.CUSTOMER_CODE].ToString());
        }


        else if (ViewState[CommonUtility.CUSTOMER_CODE] != null && !string.IsNullOrWhiteSpace(ViewState[CommonUtility.CUSTOMER_CODE].ToString()))
        {
            GetCustomerDetails(ViewState[CommonUtility.CUSTOMER_CODE].ToString());
            //btnContact.IsEnabled = false;
            //btnCustomer.IsEnabled = false;
            //hasParentContact = true;
        }

        

    }

    private void HideAddressCmbBox() {
        cmbMailingAddresses.Visible = false;
        cmbOtherAddress.Visible = false;
    }


    private void GetEndUserDetails(string enduserCode,string customerCode) {
        EndUserClient oEndUser = new EndUserClient();

        try {
            ArgsDTO args = new ArgsDTO();
            args.EnduserCode = Server.UrlDecode(enduserCode);
            args.CustomerCode = Server.UrlDecode(customerCode);
            EndUserDTO enduser = oEndUser.GetEndUser(args);

            if (enduser != null)
            {

                txtEnduser.Text = enduser.Name;
                txtCustomer.Text = enduser.CustomerName;
            }
        }
        catch(Exception oException){
        }
    }

    private void GetLeadDetails(string leadId)
    {
        LeadClient oLead = new LeadClient();

        try
        {
            LeadDTO lead = oLead.GetLead(leadId);

            if (lead != null)
            {

                txtContact.Text = lead.LeadName;
            }
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml =commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void GetCustomerDetails(string customerCode)
    {
        CustomerClient oCustomer = new CustomerClient();

        try
        {

            CustomerDTO customer = oCustomer.GetCustomer(customerCode);

            if (customer != null)
            {

                txtCustomer.Text = customer.Name;
            }
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void SaveContact()
    {
        CommonUtility commonUtility = new CommonUtility();
        if (string.IsNullOrWhiteSpace(txtContact.Text) && string.IsNullOrWhiteSpace(txtCustomer.Text) && string.IsNullOrWhiteSpace(txtEnduser.Text))
        {            
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Organisation/Customer/End User cannot be empty.");
            return;
        }

        if (string.IsNullOrWhiteSpace(txtEnduser.Text))
        {
            #region Validation - Maximum number of Contacts

            CommonClient oBrContactPerson = new CommonClient();
            CustomerClient customerClient = new CustomerClient();
            LeadClient leadClient = new LeadClient();
            List<ContactPersonDTO> lContact = new List<ContactPersonDTO>();

            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.Originator = UserSession.Instance.Originator;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ClientType = UserSession.Instance.ClientType;
            args.CustomerCode = Request.QueryString["custid"];

            args.StartIndex = 1;
            args.RowCount = 30;
            args.AdditionalParams = "";
            args.OrderBy = "first_name ASC";

            //string vv = ViewState[CommonUtility.CUSTOMER_CODE].ToString();
            //string vv1 = ViewState[CommonUtility.LEAD_ID].ToString();

            if ((ViewState["ContactType"] != null && !string.IsNullOrEmpty(ViewState["ContactType"].ToString())))
            {
                lContact = null;
            }
            else if ( ViewState[CommonUtility.LEAD_ID] != null && !string.IsNullOrWhiteSpace(ViewState[CommonUtility.LEAD_ID].ToString()))
            {
                args.LeadId = Convert.ToInt32(int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()));
                lContact = leadClient.GetContactsByOrigin(args, true);
            }
            else if (ViewState[CommonUtility.CUSTOMER_CODE] != null && !string.IsNullOrWhiteSpace(ViewState[CommonUtility.CUSTOMER_CODE].ToString()))
                lContact = customerClient.GetCustomerContacts(args.CustomerCode, args, UserSession.Instance.ClientType, true);
            
            int personalCount = 0;
            int departmentCount = 0;
            int coaCount = 0;

            if (lContact != null)
            {

                //Commented by kavisha
                //if (!string.IsNullOrEmpty(ViewState[CommonUtility.LEAD_ID].ToString()))
                //    lContact = (from n in lContact
                //                where n.ContactPersonID != Convert.ToInt32(int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()))
                //                select n).ToList();
                if (ViewState[CommonUtility.LEAD_ID] != null && int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()) != 0)
                    lContact = (from n in lContact
                                where n.ContactPersonID != Convert.ToInt32(int.Parse(ViewState[CommonUtility.LEAD_ID].ToString()))
                                select n).ToList();



                foreach (ContactPersonDTO item in lContact)
                {
                    if (item.ContactPersonID.ToString() == ContactPersonId && item.ContactType == cmbType.SelectedItem.Value )
                    {
                            if ((item.Origin == UserSession.Instance.ClientType && cmbType.SelectedItem.Text == "Department") ||
                                 (item.Origin == "COA" && cmbType.SelectedItem.Text == "CofA") || item.ContactType == "P")
                            {
                                personalCount = 0;
                                coaCount = 0;
                                departmentCount = 0;
                                break;
                            }
                        
                    }
                        if (item.ContactType.Equals("P"))
                            personalCount++;
                        else if (item.ContactType.Equals("D") && item.Origin.Equals("COA"))
                            coaCount++;
                        else if (item.ContactType.Equals("D") && !item.Origin.Equals("COA"))
                            departmentCount++;
                    
                }

                //if (string.IsNullOrEmpty(Request.QueryString["personid"]))
                //{

                    if (personalCount >= 1 && cmbType.SelectedItem.Text == "Personal")
                    {
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetErrorMessage("Maximum number of 'Personal' contacts are added");
                        return;
                    }

                    if (coaCount >= 1 && cmbType.SelectedItem.Text == "CofA")
                    {
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetErrorMessage("Maximum number of 'CofA' contacts are added");
                        return;
                    }

                    if (departmentCount >= 1 && cmbType.SelectedItem.Text == "Department")
                    {
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetErrorMessage("Maximum number of 'Department' contacts are added");
                        return;
                    }
                //}
            }

            #endregion
        }



        if (txtFirstName.Text.Trim() == "")
        {
            div_info.Attributes.Add("style", "display:block");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = "Please Enter FirstName.";
            txtFirstName.Focus();
            return;
        }

        if (string.IsNullOrWhiteSpace(txtContact.Text) && string.IsNullOrWhiteSpace(txtCustomer.Text))
        {
            div_info.Attributes.Add("style", "display:block");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = "Contact/Customer cannot be empty.";

           // btnContact_Click(this, new RoutedEventArgs());

            return;
        }

        if (!string.IsNullOrEmpty(txtEmail.Text))
        {
            /*
            if (!clsGlobal.ValidateExpression(txtEmail.Text, GlobalConstant.ValidEmailExpression))
            {
                div_info.Visible = true;
                div_info.InnerHtml = "Email address is invalid.";
                txtEmail.Focus();
                return;
            */
        }

        bool iNoRecs ;

        ContactPersonDTO contact = new ContactPersonDTO();

        contact.ContactPersonID = ((!string.IsNullOrWhiteSpace(ContactPersonId)) ? Convert.ToInt32(ContactPersonId) : 0);

        if (ViewState[CommonUtility.LEAD_ID] != null && ViewState[CommonUtility.LEAD_ID] != string.Empty)
            contact.LeadID = int.Parse(ViewState[CommonUtility.LEAD_ID].ToString());

        contact.EmailAddress = HttpUtility.HtmlEncode(txtEmail.Text);
        contact.Title = cmbTitle.SelectedValue.ToString() == "None" ? "" : cmbTitle.SelectedValue.ToString();
        contact.FirstName = HttpUtility.HtmlEncode(txtFirstName.Text);
        contact.LastName = HttpUtility.HtmlEncode(txtLastName.Text);
        contact.MailingCity = HttpUtility.HtmlEncode(txtMailingCity.Text);
        contact.MailingCountry = cmbMailingCountry.Text;
        contact.MailingPostcode = HttpUtility.HtmlEncode(txtMailingPostcode.Text);
        contact.MailingState = cmbMailingState.Text;
        contact.MailingAddress = HttpUtility.HtmlEncode(txtMailingStreet.Text);
        contact.Mobile = HttpUtility.HtmlEncode(txtMobile.Text.Trim());
        contact.Originator = HttpUtility.HtmlEncode(txtOriginator.Text);
        contact.OtherCity = HttpUtility.HtmlEncode(txtOtherCity.Text);
        contact.OtherCountry = cmbOtherCountry.Text;
        contact.OtherPostCode = HttpUtility.HtmlEncode(txtOtherPostcode.Text);
        contact.OtherState = cmbOtherState.Text;
        contact.OtherAddress = HttpUtility.HtmlEncode(txtOtherStreet.Text);
        contact.Telephone = HttpUtility.HtmlEncode(txtPhone.Text.ToString().Trim());
        contact.Position = HttpUtility.HtmlEncode(txtPosition.Text);
        contact.ReportsTo = HttpUtility.HtmlEncode(txtReportsTo.Text);

        if (txtDescription.Text.Contains("(Max 500 Characters)"))
        {
            contact.Description = "";
        }
        else
        {
            contact.Description = HttpUtility.HtmlEncode(txtDescription.Text);
        }

        contact.Fax = HttpUtility.HtmlEncode(txtFaxNumber.Text.ToString().Trim());
        if (txtSpecialInterest.Text.Contains("(Max 500 Characters)"))
        {
            contact.SpecialInterests = "";
        }
        else
        {
            contact.SpecialInterests = HttpUtility.HtmlEncode(txtSpecialInterest.Text);
        }
        contact.KeyContact = chkKeyContact.Checked ? "Y" : "N";
        contact.KeyContactChecked = chkKeyContact.Checked;
        contact.CreatedBy = UserSession.Instance.UserName;
        contact.CreatedDate = DateTime.Now.ToUniversalTime();
        contact.LastModifiedBy = UserSession.Instance.UserName;
        contact.LastModifiedDate = DateTime.Now.ToUniversalTime();
        
        if (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty && ViewState[CommonUtility.ENDUSER_CODE] != null && ViewState[CommonUtility.ENDUSER_CODE] != string.Empty)
        {
            contact.CustCode = ViewState[CommonUtility.CUSTOMER_CODE].ToString();
            contact.EndUserCode = ViewState[CommonUtility.ENDUSER_CODE].ToString();
        }
        else if(ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE]!=string.Empty) 
        {contact.CustCode = ViewState[CommonUtility.CUSTOMER_CODE].ToString();}
        contact.Contact = contact.Title + " " + contact.FirstName + " " + contact.LastName;

        //contact.Origin = UserSession.Instance.UserName;
        //contact.ContactType = "P";
        if (cmbType.SelectedItem.Text == "Personal")
        {
            contact.ContactType = "P";
            contact.Origin = UserSession.Instance.UserName;
        }
        else if (cmbType.SelectedItem.Text == "Department")
        {
            contact.ContactType = "D";
            contact.Origin = UserSession.Instance.ClientType;
        }
        else
        {
            contact.ContactType = "D";
            contact.Origin = "COA";
        }

        contact.ImagePath = "";
        contact.Note = "";

        try
        {
            //if (!string.IsNullOrWhiteSpace(txtImagePath.Text))
            //{
            //    if (!Directory.Exists(Global.clsGlobal.GetInstance().ContactImagePath))
            //    {
            //        GlobalValues.GetInstance().ShowMessage("The specified path is not found : " + Global.clsGlobal.GetInstance().ContactImagePath,
            //            GlobalValues.MessageImageType.Warning);
            //        return;
            //    }
            //    else
            //        contact.ImagePath = SaveImagePath(txtImagePath.Text);
            //}

            // This is included in the Save method - Quinn
            //if (chkKeyContact.IsChecked == true)
            //    toggleKeyContacts(contact.LeadID, contact.ContactPersonID);



            CustomerClient customerClient = new CustomerClient();
            LeadClient leadClient = new LeadClient();
            EndUserClient endUserClient = new EndUserClient();

            int contactID = 0;

            if ((ViewState[CommonUtility.ENDUSER_CODE] != null && ViewState[CommonUtility.ENDUSER_CODE] != string.Empty)
                && (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty))
            {
                iNoRecs = endUserClient.SaveEndUserContact(contact , ref contactID);
            }
            //else if (ViewState[CommonUtility.CUSTOMER_CODE] == null || ViewState[CommonUtility.CUSTOMER_CODE] == string.Empty)
            //{
            //    iNoRecs = customerClient.SaveContactPerson(out contactID, contact);
            //}
            else if (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty)
            {
                iNoRecs = leadClient.SaveCustomerContact(contact, out contactID);
            }
            else
               
            {
                iNoRecs = customerClient.SaveContactPerson(contact, out contactID);
                
            }

            ContactPersonId = contactID.ToString();

            if (iNoRecs)
            {

                if (!string.IsNullOrEmpty(hfImageName.Value))
                {
                    //string path = HttpContext.Current.Request.PhysicalApplicationPath + "docs\\tmp_contactperson_" + Session.SessionID + "_";
                    //if (!Directory.Exists(path))
                    //    Directory.CreateDirectory(path);

                    var file = hfImageName.Value;
                    //string filename = path + file;

                    ContactPersonImageDTO image = new ContactPersonImageDTO();
                    image.FileName = file;

                    image.FilePath = "";// filename;
                    image.isDirty = true;
                    image.ContactPersonImageId = int.Parse(string.IsNullOrEmpty(txtImageId.Value) ? "0" : txtImageId.Value);
                    
                    CommonClient commonClient = new CommonClient();
                 
                    bool imageRecs = false;
                    int cpImageId = image.ContactPersonImageId;

                    if (image != null && image.isDirty)
                    {

                        if (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty && ViewState[CommonUtility.ENDUSER_CODE] != null && ViewState[CommonUtility.ENDUSER_CODE] != string.Empty)
                        {
                            image.ContactPersonId = contactID;
                            image.CustomerCode = contact.CustCode;
                            image.EndUserCode = contact.EndUserCode;
                            try
                            {
                                imageRecs = endUserClient.SaveEndUserContactPersonImage(image, ref cpImageId);
                            }
                            catch (Exception ex)
                            {
                            }
                            LoadEndUserImage(new EndUserClient());
                        }
                        
                        else if (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty)
                        {
                            image.ContactNo = contactID;
                            image.CustomerCode = contact.CustCode;

                            try
                            {
                                imageRecs = commonClient.SaveContactPersonImage(image);
                            }
                            catch (Exception ex)
                            {
                            }
                            LoadImage(new CustomerClient());
                        }
                        else
                        {
                            image.LeadID = contact.LeadID;
                            image.ContactPersonId = contactID;

                            try
                            {
                                imageRecs = commonClient.SaveContactPersonImage(image);
                            }
                            catch (Exception ex)
                            {
                            }
                            LoadImage(new CustomerClient());
                        }

                        
                     
                        if (!imageRecs)
                        {
                            div_info.Attributes.Add("style", "display:block");
                            div_message.Attributes.Add("style", "display:none");
                            div_info.InnerHtml = commonUtility.GetErrorMessage("Error in saving image");
                            return;
                        }
                    }
                   
                }

                div_info.Attributes.Add("style", "display:none");
                div_message.Attributes.Add("style", "display:block");

                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
            }
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

  
    private void toggleKeyContacts(int leadID, int contactPersonID)
    {
        /*
        brContactPerson oContact = new brContactPerson();
        try
        {
            oContact.toggleKeyContacts(leadID, contactPersonID);
        }
        catch (Exception)
        {

            throw;
        }
         * */
    }

    public byte[] ConvertFile(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

        //Create a byte array of file stream length
        byte[] ImageData = new byte[fs.Length];

        //Read block of bytes from stream into the byte array
        fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

        //Close the File Stream
        fs.Close();
        return ImageData; //return the byte data
    }

    private void ClearContact()
    {
        ContactPersonId = "0";

        txtEmail.Text = string.Empty;
        cmbTitle.SelectedValue = "None" ;
        txtFirstName.Text = string.Empty;
        txtLastName.Text = string.Empty;
        txtMailingCity.Text = string.Empty;
        cmbMailingCountry.SelectedIndex = 0;
        txtMailingPostcode.Text = string.Empty;

        cmbMailingState.SelectedIndex = 0;
        txtMailingStreet.Text = string.Empty;
        txtMobile.Text = string.Empty;
        txtOriginator.Text = string.Empty;
        txtOtherCity.Text = string.Empty;
        cmbOtherCountry.SelectedIndex = 0;
        txtOtherPostcode.Text = string.Empty;
        cmbOtherState.SelectedIndex = 0;
        txtOtherStreet.Text = string.Empty;
        txtPhone.Text = string.Empty;

        txtPosition.Text = string.Empty;
        txtReportsTo.Text = string.Empty; ;
        txtDescription.Text = string.Empty;
        txtFaxNumber.Text = string.Empty;
        txtSpecialInterest.Text = string.Empty;
        chkKeyContact.Checked = false;

        hfImageName.Value = string.Empty;
        txtImageId.Value = string.Empty;
        tbCreated.InnerHtml = "";
        tbModified.InnerHtml = "";
        
    }
    #endregion
}