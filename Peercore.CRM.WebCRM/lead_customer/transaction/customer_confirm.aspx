﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="customer_confirm.aspx.cs" Inherits="lead_customer_transaction_customer_confirm" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="CustomermodalWindow" style="display: none">
        <div id="div_CustomerConfirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server"/>

    <div runat="server" id="div_message" style="display: none">
    </div>
    <div class="clearall">
    </div>
    <div runat="server" id="header" class="homemsgboxtopic" style="display: none;">
        CONTACTS
    </div>
    <div class="clear">
    </div>
    
    <div class="formtextdiv input-mini">
        <span>ASM</span>
        <div>
            <asp:DropDownList ID="ddlAsmList" runat="server" ClientIDMode="Static" Style="width: 230px;">
            </asp:DropDownList>
        </div>
    </div>

    <div class="formtextdiv input-mini">
        <span>Territory</span>
        <div>
            <asp:DropDownList ID="ddlTerritoryList" runat="server" ClientIDMode="Static" Style="width: 230px;">
            </asp:DropDownList>
        </div>
    </div>

    <div class="formtextdiv input-mini">
        <span></span>
        <div style="width: 245px">
            <a class="searchbtn sprite_button" id="a_search" style="margin-left: 10px; margin-top: 19px;">Search</a>
        </div>
    </div>

    <div class="clear">
    </div>

    <div id="grid" style="margin-top: 40px">
    </div>

     <div id="popup">
        <div>
            <b>Please enter admin password:</b></div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>


  <%--  <div id="dialog" title="Basic dialog">
        <input type="password" size="25" />
    </div>--%>

    <script type="text/javascript">

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            var asm = $("#ddlAsmList").val();
            var territory = $("#ddlTerritoryList").val();

            CustomersforConfirm(false, OriginatorString, asm, territory);
        });

        $("#a_search").click(function () {
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            var asm = $("#ddlAsmList").val();
            var territory = $("#ddlTerritoryList").val();

            CustomersforConfirm(false, OriginatorString, asm, territory);
        });

        /*Delete market in Product Category Assign Page*/
        function AcceptCustomer(Originator, CustCode) {
            $("#div_loader").show();

            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            var asm = $("#ddlAsmList").val();
            var territory = $("#ddlTerritoryList").val();

            var root = ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/AcceptCustomer?Originator=" + OriginatorString + "&CustCode=" + CustCode;

            $.ajax({
                type: "POST",
                url: root,
                data: '{ Originator : "' + OriginatorString + '", CustCode : "' + CustCode + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function OnSuccess(response) {
                    $("#div_loader").hide();
                    CustomersforConfirm(false, OriginatorString, asm, territory);
                },
                failure: OnFail
            });
        }

        /*Delete market in Product Category Assign Page*/
        function RejectCustomer(Originator, CustCode) {
            $("#div_loader").show();

            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            var asm = $("#ddlAsmList").val();
            var territory = $("#ddlTerritoryList").val();

            var root = ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/RejectCustomer?Originator=" + OriginatorString + "&CustCode=" + CustCode;

            $.ajax({
                type: "POST",
                url: root,
                data: '{ Originator : "' + OriginatorString + '", CustCode : "' + CustCode + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function OnSuccess(response) {
                    $("#div_loader").hide();
                    CustomersforConfirm(false, OriginatorString, asm, territory);
                },
                failure: OnFail
            });
        }
       

    </script>
</asp:Content>

