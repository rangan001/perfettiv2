﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class lead_customer_transaction_customer_confirm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                LoadOption();

                if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
                {
                    //Remove the session before setting the breadcrumb.
                    Session.Remove(CommonUtility.BREADCRUMB);

                    string headertext = "mSales Outlets Confirmation";

                    Master.SetBreadCrumb(headertext, "#", "");
                }
            }

            //id_delete_customers.Visible = false;
            //if (UserSession.Instance.OriginatorString == "ADMIN")
            //{
            //    id_delete_customers.Visible = true;
            //}
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.UserName;
    }

    public void LoadOption()
    {
        #region ASM
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;

        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (aseList.Count > 0)
            aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        SetAses(aseList, "UserName");

        #endregion

        #region Territory

        MasterClient masterlient = new MasterClient();
        ArgsModel args1 = new ArgsModel();
        args1.StartIndex = 1;
        args1.RowCount = 10000;
        args1.OrderBy = " territory_name asc";

        List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        objTrrList = masterlient.GetAllTerritoryByOriginator(args1, UserSession.Instance.OriginatorString,
                                                                                UserSession.Instance.UserName);

        if (objTrrList.Count != 0)
        {
            ddlTerritoryList.Items.Clear();
            ddlTerritoryList.Items.Add(new ListItem("- Select Territory -", "0"));
            foreach (TerritoryModel item in objTrrList)
            {
                var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
                string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlTerritoryList.Items.Add(new ListItem(item.TerritoryName + "  |  " + result, item.TerritoryId.ToString()));
            }

            ddlTerritoryList.Enabled = true;
        }

        #endregion
    }

    public void SetAses(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        ddlAsmList.DataSource = listOriginator;
        ddlAsmList.DataTextField = "Name";
        ddlAsmList.DataValueField = valueField;
        ddlAsmList.DataBind();
    }
}