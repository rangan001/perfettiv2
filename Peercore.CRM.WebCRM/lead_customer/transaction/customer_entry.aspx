﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="customer_entry.aspx.cs" Inherits="lead_customer_transaction_customer_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/entrypage_tabs.ascx" TagPrefix="ucl2" TagName="tabentry" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <asp:HiddenField ID="HiddenFieldCustAddress1" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustAddress2" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustCity" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustState" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustPostcode" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactPhone" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactMobile" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactFax" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactWebSite" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactEmail" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactPrefMethod" runat="server" />
    <asp:HiddenField ID="HiddenCustomerCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldFormType" runat="server" />
    <asp:HiddenField ID="HiddenFieldSaveClickOk" runat="server" />
    <asp:HiddenField ID="HiddenFieldTabIndex" runat="server" Value="1" />
    <asp:HiddenField ID="HiddenFieldRepCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldAssignedRouteName" runat="server" />
    <asp:HiddenField ID="HiddenFieldRouteAssignId" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenFieldRouteMasterId" runat="server" />
    <asp:HiddenField ID="HiddenFieldMarketId" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenFieldMarketName" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedImageName" runat="server" />
    <div id="customermodalWindow" style="display: none">
        <div id="div_customerconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes
        </button>
        <button id="no" class="k-button">
            No
        </button>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=Customer">
                            <div class="back">
                            </div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        <div>
            <%--layout new format--%>
            <div class="formleft" style="width:35%">
                <div class="formtextdiv">
                    Outlet No</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtCode" runat="server" CssClass="input-large1" MaxLength="12"></asp:TextBox>
                    <%--<a id="link_searchcode" runat="server">
                    <img id="Img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>--%>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Name</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtName" runat="server" CssClass="input-large1"></asp:TextBox></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Address</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="TxtAddress1" runat="server" CssClass="input-large1"></asp:TextBox></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Telephone</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="TxtTelephone" runat="server" CssClass="input-large1" MaxLength="10" onkeypress="javascript:return isNumber(event)"></asp:TextBox></div>
                <div class="clearall">
                </div>
                
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                Outlet MT Code</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtOutletUserCode" runat="server" CssClass="input-large1"></asp:TextBox></div>

                    <div class="clearall">
                </div>
                <%--<div class="formtextdiv">
                Province</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListProvince" runat="server" CssClass="input-large1">
                </asp:DropDownList></div>
                <div class="clearall">
            </div>
            <div class="formtextdiv">
                Geo Location</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListGeoLocation" runat="server" CssClass="input-large1">
                </asp:DropDownList></div>
                <div class="clearall">
            </div>
            <div class="formtextdiv">
                Status</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListStatus" runat="server" CssClass="input-large1">
                </asp:DropDownList></div>
                <div class="clearall">
            </div>
            <div class="formtextdiv">
                Products Handled</div>
            <div class="formdetaildiv_right">
                :
            <asp:DropDownList ID="DropDownListProHandled" runat="server" CssClass="input-large1">
                </asp:DropDownList></div>
                <div class="clearall">
            </div>--%>
                <%--<div class="formtextdiv">
                Company</div>
            <div class="formdetaildiv_right">
                :
                <asp:TextBox ID="txtCompany" runat="server" CssClass="input-large1"></asp:TextBox></div>

                <div class="clearall">
            </div>

            <div class="formtextdiv">
                Market</div>
            <div class="formdetaildiv">
                :
                <asp:DropDownList ID="dropdownMarket" runat="server">
                </asp:DropDownList>
            </div>

            <div class="clearall">
            </div>

            <div class="formtextdiv">
                Sub Market</div>
            <div class="formdetaildiv">
                :
                <asp:DropDownList ID="dropdownSubMarket" runat="server" Width="108px">
                </asp:DropDownList>
            </div>

            <div class="clearall">
            </div>

            <div class="formtextdiv">
                Potential Opportunity</div>
            <div class="formdetaildiv">
                :
                <asp:DropDownList ID="dropdownPotentialOpportunity" runat="server" Width="108px">
                </asp:DropDownList>
                <asp:TextBox ID="txtPotentialOpportunity" runat="server" Width="40px"  CssClass="input_numeric" style="text-align:right"></asp:TextBox>
             </div>

                <div class="clearall">
            </div>

              <div class="formtextdiv">
                Food Service Rep</div>
            <div class="formdetaildiv">
                :<div id="divfoodrep" runat="server" style="display:inline-block; margin:5px 0px 5px 5px;" ></div>
                </div>

                <div class="clearall">
            </div>--%>
            </div>
            <div class="formright" style="width:35%">
                <%--            <div class="names">
                <asp:Label ID="tbStage" runat="server" Text=""  CssClass="namehighliter"></asp:Label></div>--%>
                <%--                <div class="formtextdiv">
                    Rating</div>
                <div class="formdetaildiv">
                    <telerik:RadRating ID="rRating" runat="server" Width="100px" ItemCount="5" Value="0"
                        SelectionMode="Continuous" Precision="Half" Orientation="Horizontal" />
                </div>--%>
                <div class="formtextdiv" style="display:none;">
                    Periphery</div>
                <div class="formdetaildiv_right" style="display:none;">
                    :
                    <asp:DropDownList ID="DropDownListPeriphery" runat="server" CssClass="input-large1">
                        <asp:ListItem Value="-1" Text="Please Select"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Direct Distributor"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Non-Direct Distributor"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Outlet Type</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:DropDownList ID="DropDownListOutletType" runat="server" CssClass="input-large1">
                        <%--<asp:ListItem Value="-1" Text="Please Select"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Hotel"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Stores"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Book Shop"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Pastry Shop"></asp:ListItem>
                        <asp:ListItem Value="5" Text="Grocery"></asp:ListItem>
                        <asp:ListItem Value="6" Text="Pharmacy"></asp:ListItem>
                        <asp:ListItem Value="7" Text="Communication"></asp:ListItem>
                        <asp:ListItem Value="8" Text="Super Market"></asp:ListItem>--%>
                    </asp:DropDownList>
                </div>

                <div class="clearall">
                </div>

                <div class="formtextdiv">
                    Longitude</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtLongitude" runat="server" CssClass="input-large1" MaxLength="10" onkeypress="javascript:return isNumber(event)" ></asp:TextBox></div>
                <div class="clearall">
                </div>

                <div class="formtextdiv">
                    Latitude</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtLatitude" runat="server" CssClass="input-large1" MaxLength="10" onkeypress="javascript:return isNumber(event)"></asp:TextBox></div>
                <div class="clearall">
                </div>

                <div class="formtextdiv">
                    Assigned Route
                </div>
                <div class="formdetaildiv_right">
                    : <span class="wosub mand">
                        <asp:TextBox ID="txtDRName" CssClass="tb input-large1" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                        <div runat="server" id="div_autocomplete">
                        </div>
                    </span>
                </div>

                <div class="clearall">
                </div>
                <div class="formtextdiv" style="display:none;">
                    Volume</div>
                <div class="formdetaildiv_right" style="display:none;">
                    :
                    <asp:DropDownList ID="DropDownListVolume" runat="server" CssClass="input-large1">
                        <asp:ListItem Value="-1" Text="Please Select"></asp:ListItem>
                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                        <asp:ListItem Value="2" Text="2"></asp:ListItem>
                        <asp:ListItem Value="3" Text="3"></asp:ListItem>
                        <asp:ListItem Value="4" Text="4"></asp:ListItem>
                        <asp:ListItem Value="5" Text="5"></asp:ListItem>
                        <asp:ListItem Value="6" Text="6"></asp:ListItem>
                        <asp:ListItem Value="7" Text="7"></asp:ListItem>
                        <asp:ListItem Value="8" Text="8"></asp:ListItem>
                        <asp:ListItem Value="9" Text="9"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>

                <div class="formtextdiv" style="display:block;">
                    Category</div>
                <div class="formdetaildiv_right" style="display:block;">
                    :
                    <asp:DropDownList ID="DropDownListCategory" runat="server" CssClass="input-large1">
                        <%--<asp:ListItem Value="-1" Text="Please Select"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Modern"></asp:ListItem>
                        <asp:ListItem Value="2" Text="General"></asp:ListItem>
                        <asp:ListItem Value="3" Text="HoReCa"></asp:ListItem>--%>
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>

                <div class="formtextdiv" style="display: none">
                    Market</div>
                <div class="formdetaildiv_right" style="display: none">
                    :
                    <asp:DropDownList ID="DropDownListMarkets" runat="server" CssClass="input-large1">
                    </asp:DropDownList>
                </div>
                <div class="formdetaildiv_right" style="display: none">
                    :
                    <asp:TextBox ID="txtMarketName" runat="server" CssClass="input-large1" Enabled="false"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <%--     <div class="formtextdiv">
               Canvassing Frequency</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListCanvFreq" runat="server" CssClass="input-large1">
                </asp:DropDownList></div>

                <div class="clearall"></div>

                
            <div class="formtextdiv">
               ASU 30 Share (%)</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListAsu" runat="server" CssClass="input-large1">
                </asp:DropDownList></div>

                <div class="clearall"></div>

                                         <div class="formtextdiv">
               Authority Assesment</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListAssessment" runat="server" CssClass="input-large1"> 
                </asp:DropDownList></div>

                <div class="clearall"></div>--%>
                <div class="formtextdiv" style="display: none">
                    Is In TLP</div>
                <div class="formdetaildiv" style="display: none">
                    :
                    <asp:RadioButton ID="radioyes" Text=" Yes" runat="server" GroupName="istlp" />&nbsp;&nbsp;
                    <asp:RadioButton ID="radiono" runat="server" Text=" No" GroupName="istlp" />
                </div>
                <div class="clearall">
                </div>

                <div class="formtextdiv" style="">
                    Retail / Wholesale</div>
                <div class="formdetaildiv_right" style="">
                    :
                    <asp:DropDownList ID="DropDownIsRetail" runat="server" CssClass="input-large1">
                        <asp:ListItem Value="1" Text="Retail"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Wholesale"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>


                <%--<div class="formtextdiv">
                    Assigned DR
                </div>
                <div class="formdetaildiv_right">
                    :
                    <span class="wosub mand">
                        <asp:TextBox ID="txtDRName" CssClass="tb input-large1" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                        <div runat="server" id="div_autocomplete">
                        </div>
                    </span>
                </div>
                <div class="clearall">
                </div>--%>
            </div>

            <div class="formright_image" style="float:left; width:25%;">
                <%--<fieldset>
                    <legend>Customer Image</legend>
                    <div class="formdetaildiv">
                        <div id="div_preview" class="upload_image" runat="server">
                        </div>
                    </div>
                </fieldset>--%>
                <fieldset>
                    <legend>Outlet&nbsp; Image</legend>
                    <div class="formdetaildiv" style="width:100%">
                        <div id="div_preview" class="upload_image" runat="server">
                        </div>
                        <div class="clearall">
                        </div>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv" style="display: none;">
                    </div>
                    <div class="formdetaildiv" style="width: 71%">
                        <input class="uploadCustomerImage" name="customerImage" id="customerImage" type="file"
                            upload-id="02ebeebf-98aa-459b-b41f-49028fa37e9c" />
                        <asp:Label ID="Label26" runat="server" Text="Supported Formats : JPG, JPEG" style="color:gray; font-size: 11px;"></asp:Label>
                        <asp:Label ID="lblImageName" runat="server" Text="*" style="color: red"></asp:Label>
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </div>

            <div class="clearall">
            </div>
            <div class="specialevents">
                <asp:Label ID="tbDuration" runat="server" Text=""></asp:Label></div>
            <div class="clearall">
            </div>
            <div class="specialevents">
                <asp:Label ID="tbWeeksSinceLastCalled" runat="server" Text=""></asp:Label></div>
            <div class="specialevents">
                <span id="tbCreated" runat="server"></span>
            </div>
            <div class="specialevents">
                <span id="tbModified" runat="server"></span>
            </div>
        </div>
    </div>
    <div class="divcontectmainforms" style="display: none;">
        <a name="tabs"></a>
        <ucl2:tabentry ID="tabentry1" runat="server" />
    </div>
    <asp:HiddenField ID="hfWebsite" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfPreferredMethod" runat="server"></asp:HiddenField>
    <script type="text/javascript">
        // hideStatusDiv("MainContent_div_message");
        jQuery(function ($) {
            hideStatusDiv("MainContent_div_message");
            if ($('#MainContent_HiddenFieldMarketName').val() != '' && $('#MainContent_HiddenFieldMarketName').val() != null) {
                $('#MainContent_txtMarketName').val($('#MainContent_HiddenFieldMarketName').val());
            }
            else {
                $('#MainContent_txtMarketName').val(' - ');
            }

            jQuery(function ($) {
                $('input.input_numeric_tp').autoNumeric({ aSep: null, aDec: null, mDec: 0 });
            });
            // $('input.input_numeric').autoNumeric();
            // $('input.input_numeric').autoNumeric({ aSep: '.', aDec: ',' });
            //loadEntryPageTabs("customer");
            var items = [
                ["Sales History", "usercontrols/saleshistory.aspx"],
                // ["Contact Details", "usercontrols/customer_contactdetails.aspx"],
                ["Address", "usercontrols/customer_address.aspx"],
                ["Contact Person", "usercontrols/customer_contactperson.aspx"],
                ["End User", "usercontrols/customer_enduser.aspx?ty=" + $("#MainContent_HiddenFieldFormType").val() + ""],
                ["Activity", "usercontrols/customer_activity.aspx"],
                ["Document", "usercontrols/customer_documents.aspx"],
                ["Opportunities", "usercontrols/customer_oppotunity.aspx"],
                ["Emails", "usercontrols/customer_email.aspx"],
                ["Promotions", "usercontrols/customer_promotions.aspx"]

            ];
            loadEntryPageTabs("customer", items, $("#MainContent_HiddenFieldTabIndex").val());

            InitializeCustomerImageUpload();
            var custId = getParameterByName('custid');
            LoadCustomerImage(custId);
        });

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function LoadCustomerImage(CustomerCode) {
            $("#div_loader").show();
            var param = { "CustomerCode": CustomerCode };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/SaveCustomerImageAndGetImageName",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                    } else {
                        msg = response;
                    }
                    //debugger;
                    var json = JSON.parse(msg);

                    var imageName = json["ImageName"];

                    if (imageName != null) {
                        $("#MainContent_lblImageName").html(imageName);
                        //$('#MainContent_HiddenFieldSelectedImageName').val(imageName);
                        $("#MainContent_div_preview").css("display", "block");
                        $("#MainContent_div_preview").html('<img src="' + ROOT_PATH + 'docs/customer_images/' + imageName + '"  style="width:100%">');
                    }
                    $("#div_loader").hide();
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");
                    $("#div_loader").hide();
                }
            });
        }

        //        $("#id_prevcustcode").click(function () {
        //            prevcustcode('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=prevcustcode&si=0&rc=10&pg=0');
        //        });

        //        $("#id_bdmgroup").click(function () {
        //            repgroup('lead_customer/process_forms/processmaster.aspx?fm=customerentry&type=query&querytype=repgroup');
        //        });

        //        $("#MainContent_Contact Detail_userControl_id_address").click(function () {
        //            addAddress('lead_customer/process_forms/processmaster.aspx?fm=contactdetails&type=insert&querytype=repgroup','');
        //        });
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        //        $("#MainContent_buttonbar_buttinSave").click(function () { 
        //        });

        function InitializeCustomerImageUpload() {
            $("#customerImage").kendoUpload({
                async: {
                    saveUrl: ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=customerimage&type=insert",
                    autoUpload: true
                },
                multiple: false,
                showFileList: false,
                localization: {
                    select: 'Select File',
                    remove: '',
                    cancel: ''
                },
                upload: function onUpload(e) {
                    e.sender.options.async.saveUrl = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=customerimage&type=insert";
                    var files = e.files;
                    // Check the extension of each file and abort the upload if it is not .jpg
                    $.each(files, function () {
                        //if (this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".gif" && this.extension.toLowerCase() != ".tif" && this.extension.toLowerCase() != ".tiff" && this.extension.toLowerCase() != ".png" && this.extension.toLowerCase() != ".bmp") {
                        if (this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".jpeg" && this.extension.toLowerCase() != ".png") {
                            e.preventDefault();
                            alert("Please Select an .JPG or .JPEG image!");
                        }
                    });
                },
                success: function onSuccess(e) {
                    //var ROOT_PATH = '/Peercore.CRM.WebCRM/'
                    //alert(filename);
                    var sessionId = $('#MainContent_hfSessionId').val();
                    //var filename = "CampaignImage_" + sessionId + "_" + e.files[0].name;
                    var filename = e.files[0].name;
                    //var fullFilename = e.files[0].rawFile.mozFullPath;
                    $('#MainContent_HiddenFieldSelectedImageName').val(filename);
                    $('#MainContent_HiddenFieldSelectedImageFullPath').val(ROOT_PATH + 'docs/customer_images/' + filename);

                    $("#MainContent_lblImageName").html(filename);
                    $("#MainContent_div_preview").css("display", "block");
                    $("#MainContent_div_preview").html('<img src="' + ROOT_PATH + 'docs/customer_images/' + filename + '">');
                    //$("#MainContent_divImagePreview").html('<img src="' + ROOT_PATH + 'docs/contactperson/' + filename + '">');
                }

            });
        }

        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;

            return true;
        }
    </script>
</asp:Content>
