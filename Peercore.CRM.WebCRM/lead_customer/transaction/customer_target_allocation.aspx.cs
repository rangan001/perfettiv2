﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Text;
using CRMServiceReference;
using System.Web.Script.Serialization;

public partial class lead_customer_transaction_customer_target_allocation :  PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.VisibleSave(true);

            if (!IsPostBack)
            {
                div_autocomplete.InnerHtml = SetAutocomplete_Route();;
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Customer Targets ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        CustomerClient customerClient = new CustomerClient();
        CommonClient commonClient = new CommonClient();
        CommonUtility commonUtility = new CommonUtility();

        try
        {
            if (txtRouteName.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Route Name cannot be empty.");
                return;
            }

            int selectedRouteTargetId = Convert.ToInt32(HiddenFieldSelectedRouteTargetId.Value);
            int routeMasterId = Convert.ToInt32(HiddenFieldRouteMasterID.Value);
            double selectedRouteTarget = Convert.ToDouble(HiddenFieldSelectedRouteTarget.Value);

            bool isSucc = false;

            DateTime today = DateTime.Today;
            DateTime firstOfMonth = new DateTime(today.Year, today.Month, 1);
            DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = "";

            List<CustomerDTO> customerTargetsList = new List<CustomerDTO>();
            //List<PaymentSettlementDTO> UpdatedChequePaymentsList = new List<PaymentSettlementDTO>();

            if (!string.IsNullOrEmpty(HiddenFieldCustomerTargets.Value))
            {
                jsonString = @"" + HiddenFieldCustomerTargets.Value.Replace("\"ExtensionData\":{},", "");
                customerTargetsList = serializer.Deserialize<List<CustomerDTO>>(jsonString);
            }

            if (HiddenFieldSelectedRouteTarget.Value.Trim() == "0" && (customerTargetsList == null || customerTargetsList.Count == 0))
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetInfoMessage("Route Target is not set and there are no customers added to the route.");
                return;
            }

            if (HiddenFieldSelectedRouteTarget.Value.Trim() != "0" && (customerTargetsList == null || customerTargetsList.Count == 0))
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetInfoMessage("There are no customers added to the route.");
                return;
            }

            double calculatedRouteTarget = 0;

            if (customerTargetsList != null && customerTargetsList.Count != 0)
            {
                foreach (CustomerDTO item in customerTargetsList)
                {
                    if (item.TargetId == 0 && item.StickTarget > 0)//new insert entry
                    {
                        item.EffStartDate = firstOfMonth;
                        item.EffEndDate = endOfMonth;
                        item.CreatedBy = UserSession.Instance.UserName;
                    }
                    else if (item.TargetId > 0)//update entry
                    {
                        item.ModifiedBy = UserSession.Instance.UserName;
                    }

                    //calculating routeTarget
                    calculatedRouteTarget = calculatedRouteTarget + item.StickTarget;
                }
            }
            calculatedRouteTarget = Math.Round(calculatedRouteTarget, 3);
            selectedRouteTarget = Math.Round(selectedRouteTarget, 3);

            if (calculatedRouteTarget > selectedRouteTarget && HiddenFieldOverwriteClickOk.Value != "1")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showRouteTargetConfirmMg('"
                                                    + txtRouteName.Text.Trim() + "','" 
                                                    + selectedRouteTarget.ToString() + "','"
                                                    + calculatedRouteTarget.ToString() + "');", true);
                return;
            }

            RouteMasterDTO routeTarget = new RouteMasterDTO();
            routeTarget.TargetId = selectedRouteTargetId;
            if (calculatedRouteTarget > selectedRouteTarget && HiddenFieldOverwriteClickOk.Value == "1")
            {
                routeTarget.Target = calculatedRouteTarget;
            }
            else
            {
                routeTarget.Target = selectedRouteTarget;
            }

            //checking for routeTarget, Update or Insert
            if (routeTarget.TargetId == 0)
            {
                routeTarget.RouteMasterId = routeMasterId;
                routeTarget.EffStartDate = firstOfMonth;
                routeTarget.EffEndDate = endOfMonth;
                routeTarget.CreatedBy = UserSession.Instance.UserName;
            }
            else
            {
                routeTarget.LastModifiedBy = UserSession.Instance.UserName;
            }
            
            List<RouteMasterDTO> routeTargets = new List<RouteMasterDTO>();
            routeTargets.Add(routeTarget);
            TargetAllocationDTO targets = new TargetAllocationDTO();
            targets.RouteTargetList = routeTargets;

            isSucc = commonClient.SaveTargetAllocation(targets);

            isSucc = customerClient.SaveCustomerTargets(customerTargetsList);

            if (isSucc == true)
            {
                // div_info.Attributes.Add("style", "display:none");
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                HiddenFieldSuccessfullySaved.Value = "1";
                HiddenFieldOverwriteClickOk.Value = "0";
                //txtDRName.Text = "";
                //txtRouteName.Text = "";
            }
            else
            {
                div_message.Attributes.Add("style", "display:none");
                div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
                HiddenFieldOverwriteClickOk.Value = "0";
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    private string SetAutocomplete_Route()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/call_cycle/call_cycle_service.asmx/GetRouteNames\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.RouteName,");
        sb.Append("                         desc: item.RouteMasterId");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");

        sb.Append("                  var selectedObj = ui.item;");

        sb.Append("                 $('#MainContent_txtRouteName').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_HiddenFieldRouteMasterID').val(selectedObj.desc);");
        sb.Append("                 $('#div_RouteTarget').css('display', 'block'); ");
        sb.Append("                 loadSelectedRouteTarget(selectedObj.desc);");
        sb.Append("                 $('#div_gridTitleCustomerTargets').css('display', 'block'); ");
        sb.Append("                 loadCustomerTargets(selectedObj.desc);");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }
}