﻿<%@ Page Title="mSales - ASM / Distributor Entry" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="distributor_entry.aspx.cs" Inherits="lead_customer_transaction_distributor_entry" %>

<%@ MasterType TypeName="SiteMaster" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/entrypage_tabs.ascx" TagPrefix="ucl2" TagName="tabentry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .search-table-inactive-text {
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            appearance: none;
            position: relative;
            top: 6px;
            right: 0;
            bottom: 0;
            left: 0;
            transition: all 0.15s ease-out 0s;
            background: #9a4343;
            border: none;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            margin-right: 0.5rem;
            outline: none;
            position: relative;
            z-index: 1000;
        }

        .option-input {
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            appearance: none;
            position: relative;
            top: 6px;
            right: 0;
            bottom: 0;
            left: 0;
            height: 24px !important;
            width: 24px !important;
            transition: all 0.15s ease-out 0s;
            background: #cbd1d8;
            border: none;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            margin-right: 0.5rem;
            outline: none;
            position: relative;
            z-index: 1000;
        }

            .option-input:hover {
                background: #9faab7;
            }

            .option-input:checked {
                background: #40e0d0;
            }

                .option-input:checked::before {
                    height: 24px;
                    width: 24px;
                    position: absolute;
                    content: '✔';
                    display: inline-block;
                    font-size: 18px;
                    text-align: center;
                    line-height: 24px;
                }

                .option-input:checked::after {
                    -webkit-animation: click-wave 0.65s;
                    -moz-animation: click-wave 0.65s;
                    animation: click-wave 0.65s;
                    background: #40e0d0;
                    content: '';
                    display: block;
                    position: relative;
                    z-index: 100;
                }

        #divEMEINos .k-grid-content {
            height: 70px !important;
        }

        .k-grid-content {
            background: #fff;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src="../../assets/scripts/jquery.maskedinput.js"></script>
    <asp:HiddenField ID="HiddenFieldCustAddress1" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustAddress2" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustCity" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustState" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustPostcode" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactPhone" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactMobile" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactFax" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactWebSite" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactEmail" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactPrefMethod" runat="server" />
    <asp:HiddenField ID="HiddenCustomerCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldFormType" runat="server" />
    <asp:HiddenField ID="HiddenFieldSaveClickOk" runat="server" />
    <asp:HiddenField ID="HiddenFieldTabIndex" runat="server" Value="1" />
    <asp:HiddenField ID="HiddenFieldIsDistributorSaved" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedDistId" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedOriginatorId" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedOriginator" runat="server" />
    <asp:HiddenField ID="HiddenFieldLoadUserType" runat="server" />
    
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />

    <div id="customermodalWindow" style="display: none">
        <div id="div_customerconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div class="divcontectmainforms" style="color:black;">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=Customer">
                        <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="modalWindowOriginators" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        <div>
            <div>
                <fieldset>
                    <legend>Search</legend>
                    <div class="formtextdiv">
                        Distributer :
                    </div>
                    <div class="formdetaildiv_right">
                        <a href="#" id="id_select_user" runat="server">
                            <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                    </div>
                </fieldset>
            </div>
            <div>
                <fieldset>
                    <legend>Distributer Details</legend>
                    <div class="formleft">
                        <div class="formtextdiv">
                            DB Code
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                        <asp:TextBox ID="txtDBCode" runat="server" CssClass="input-large1" MaxLength="15"></asp:TextBox><span
                            style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>
                        <div class="formtextdiv">
                            DB Name
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                        <asp:TextBox ID="txtName" runat="server" CssClass="input-large1" MaxLength="150"></asp:TextBox><span
                            style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>
                        <div class="formtextdiv">
                            Display Name
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                        <asp:TextBox ID="txtDisplayName" runat="server" CssClass="input-large1" MaxLength="150"></asp:TextBox><span
                            style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>
                        <div class="formtextdiv">
                            Status
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                            <asp:DropDownList ID="ddlASEStatus" runat="server" CssClass="input-large1" MaxLength="100">
                                <asp:ListItem Text="Active" Value="A"></asp:ListItem>
                                <asp:ListItem Text="Inactive" Value="D"></asp:ListItem>
                            </asp:DropDownList>
                            </span>
                        </div>

                    </div>
                    <div class="formright">
                        <div class="formtextdiv">
                            User Name
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                        <asp:TextBox ID="TxtUserName" runat="server" CssClass="input-large1" MaxLength="24"></asp:TextBox><span
                            style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>
                        <div class="formtextdiv">
                            NIC
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                        <asp:TextBox ID="txtNIC" runat="server" CssClass="input-large1"
                            MaxLength="16"></asp:TextBox><span style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>
                        <div class="formtextdiv">
                            Password
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                        <asp:TextBox ID="TxtPassword" runat="server" CssClass="input-large1" TextMode="Password"
                            MaxLength="16"></asp:TextBox><span style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>
                        <div class="formtextdiv">
                            Verify Password
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                        <asp:TextBox ID="TxtVerifyPassword" runat="server" CssClass="input-large1" TextMode="Password"
                            MaxLength="16"></asp:TextBox><span style="color: Red">*</span></span>
                        </div>
                    </div>
                </fieldset>
            </div>
            <%--<div>
                <fieldset>
                    <div class="formleft">
                        <div class="formtextdiv">
                            Territory
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                            <asp:DropDownList ID="ddlTerritory" runat="server" CssClass="input-large1" MaxLength="100">
                            </asp:DropDownList>
                            </span>
                        </div>
                        <div class="formtextdiv">
                            <button id="btnAddDistributorTerritory" type="button">Add Territory</button>
                        </div>
                    </div>
                    <legend>Territory Details</legend>
                    <div id="gridDistributerTerritory" style="float: left;"></div>
                </fieldset>
            </div>--%>
            <div>
                <fieldset>
                    <legend>Contact Details</legend>
                    <div class="formleft">
                        <div class="formtextdiv">
                            Address
                        </div>
                        <div class="formdetaildiv_right">
                            :
                            <asp:TextBox ID="TxtAddress1" runat="server" CssClass="input-large1" MaxLength="250"></asp:TextBox>
                        </div>
                        <div class="clearall">
                        </div>
                        <div class="formtextdiv">
                            Email
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                        <asp:TextBox ID="TextBoxEmail" runat="server" CssClass="input-large1" MaxLength="100"></asp:TextBox></span>
                        </div>

                    </div>
                    <div class="formright">
                        <div class="formtextdiv">
                            Mobile
                        </div>
                        <div class="formdetaildiv_right">
                            :
                    <asp:TextBox ID="txtMobile" runat="server" MaxLength="10" CssClass="input-large1-mobile" onkeypress="javascript:return isNumber(event)"></asp:TextBox>
                        </div>
                        <div class="clearall">
                        </div>
                        <div class="formtextdiv">
                            Telephone
                        </div>
                        <div class="formdetaildiv_right">
                            :
                            <asp:TextBox ID="TxtTelephone" runat="server" CssClass="input-large1-phone" MaxLength="10"
                        onkeypress="javascript:return isNumber(event)"></asp:TextBox>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div>
                <fieldset>
                    <legend>IMEI Details</legend>
                    <div class="formleft">
                        <div class="formtextdiv" id="Div4" runat="server">
                            Is IMEI User
                        </div>
                        <div class="formdetaildiv_right" id="Div3" runat="server">
                            :
                            <asp:CheckBox ID="chkIsNotEmeiUser" runat="server" RepeatDirection="Horizontal" CellSpacing="5"></asp:CheckBox>
                        </div>
                    </div>
                    <div class="formright">
                        <div class="formtextdiv">
                            IMEI No
                        </div>
                        <div class="formdetaildiv_right">
                            :
                            <div id="divEMEINos" style="height: 70px; float: right; margin-bottom: 10px; margin-left: 5px; width: 350px;">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

    </div>
    <div class="divcontectmainforms" style="display: none;">
        <a name="tabs"></a>
        <ucl2:tabentry ID="tabentry1" runat="server" />
    </div>
    <script type="text/javascript">
        
        hideStatusDiv("MainContent_div_message");

        function DisableUserNameField() {
            $("#MainContent_TxtUserName").prop('disabled', true);
        }

        function EnableUserNameField() {
            $("#MainContent_TxtUserName").prop('disabled', false);
        }

        var Territories = [];

        $(document).ready(function () {
            var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd-MMM-yyyy');
            var Originator = $("#<%= hfOriginator.ClientID %>").val();
            var OriginatorType = $("#<%= hfOriginatorType.ClientID %>").val();

            LoadTerritoriesByOriginator(OriginatorType, Originator);

            getEMEINumbers(0);

            $("#divEMEINos .k-grid-header").css('display', 'none');
            $("#divEMEINos .k-grid-content").css('height', '70px');
        });

        $("#MainContent_id_select_user").click(function () {
            get_users_lookup('lead_customer/process_forms/processmaster.aspx?fm=alldistributorentry&type=select&deptString=' + $("#MainContent_HiddenFieldLoadUserType").val());
        });

        $("#btnAddDistributorTerritory").click(function () {
            var strTrrritory = $('#MainContent_ddlTerritory').val();
            var strDistributor = $('#MainContent_HiddenFieldSelectedDistId').val();
            var strOriginator = $('#MainContent_hfOriginator').val();

            if (strTrrritory == '') {
                return;
            }
            if (strDistributor == '') {
                return;
            }

            var param = {
                "territory_id": strTrrritory,
                "distributor_id": strDistributor,
                "created_by": strOriginator
            };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/distributor_Service.asmx/SaveDistributorTerritory",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    LoadAllTerritoryByDistributer(strDistributor);
                },
                error: function (response) {
        
                }
            });
        });

        /*Load User Lookup in Distributor Entry Page*/
        function get_users_lookup(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    $("#div_loader").hide();
                    if (msg != "") {
                        var wnd = $("#modalWindowOriginators").kendoWindow({
                            title: "Distributors",
                            modal: true,
                            visible: false,
                            resizable: false,
                            width: 700
                        }).data("kendoWindow");

                        $("#modalWindowOriginators").html(msg);
                        wnd.center().open();

                        $("#jqi_state0_buttonOk").click(function () {
                            //debugger;
                            var originatorName = '';
                            var originatorId = '';
                            $('#originatorTable tr').each(function () {
                                if ($(this).children(':eq(0)').find('#a').is(':checked')) {
                                    originatorName = $(this).children(':eq(0)').find('#a').val();
                                    originatorId = $(this).children(':eq(0)').find('#b').val();
                                }
                            });

                            $('#MainContent_txtSelectedOriginatorName').val(originatorName);
                            $('#MainContent_HiddenFieldSelectedOriginator').val(originatorId);
                            wnd.close();
                            DisableUserNameField();
                            GetDistributorDetails(originatorId);
                        });

                        $("#jqi_state0_buttonCancel").click(function () {
                            closeUsersPopup();
                        });
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                        //$("#" + tagertdiv).html(msg);
                    }
                }
            });
        }

        function closeUsersPopup() {
            var wnd = $("#modalWindowOriginators").kendoWindow({
                title: "Users",
                modal: true,
                visible: false,
                resizable: false,
                width: 700
            }).data("kendoWindow");
            wnd.center().close();
            //$("#modalWindowOriginators").css("display", "none");
        }

        function GetDistributorDetails(originator) {
            $("#div_loader").show();

            var param = { "originator": originator };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/distributor_Service.asmx/GetDistributorDetails",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                    }
                    else {
                        msg = response;
                    }

                    var json = JSON.parse(msg);

                    var distId = json["DistributorId"];
                    var code = json["Code"];
                    var name = json["Name"];
                    var username = json["Originator"];
                    var password = json["Password"];
                    var address = json["Adderess"];
                    var telephone = json["Telephone"];
                    var mobile = json["Mobile"];
                    var email = json["Email"];
                    var holidays = json["HolidaysList"];
                    var parentOrginator = json["ParentOriginator"];
                    var areaId = json["AreaId"];
                    var status = json["Status"];
                    var displayname = json["DisplayName"];

                    $('#MainContent_HiddenFieldSelectedDistId').val(distId);
                    $('#MainContent_HiddenFieldSelectedOriginator').val(username);
                    $('#MainContent_txtDBCode').val(code);
                    $('#MainContent_txtName').val(name);
                    $('#MainContent_txtDisplayName').val(displayname);
                    $('#MainContent_TxtUserName').val(username);
                    $('#MainContent_TxtPassword').val(password);
                    $('#MainContent_TxtVerifyPassword').val(password);
                    $('#MainContent_TxtAddress1').val(address);
                    $('#MainContent_TxtTelephone').val(telephone);
                    $('#MainContent_txtMobile').val(mobile);
                    $('#MainContent_TextBoxEmail').val(email);
                    $('#MainContent_ddlASE').val(parentOrginator);
                    $('#MainContent_hdnASE').val(parentOrginator);
                    $('#MainContent_ddlArea').val(areaId);
                    $('#MainContent_hdnArea').val(areaId);
                    $('#MainContent_ddlASEStatus').val(status);

                    getEMEINumbers(originator);
                    $("#divEMEINos .k-grid-header").css('display', 'none');
                    $("#divEMEINos .k-grid-content").css('height', '70px');

                    //Unckeck All items
                    $("[id*=chklstHolidays] input:checkbox").prop('checked', false); // To uncheck all 

                    var isnotemeiuser = json["IsNotEMEIUser"];
                    //Unckeck All items
                    if (isnotemeiuser == true)
                        $('#MainContent_chkIsNotEmeiUser').prop('checked', true);
                    else
                        $('#MainContent_chkIsNotEmeiUser').prop('checked', false);

                    ////Check Items by value
                    //var selValue = new Array()
                    //selValue = holidays;
                    //var $ctrls = $("[id*=chklstHolidays]");
                    //for (var i = 0; i < selValue.length; i++) {
                    //    $ctrls.find('input:checkbox[value=' + selValue[i] + ']').prop('checked', true);
                    //}

                    //LoadAllTerritoryByDistributer(distId);

                    $("#div_loader").hide();

                    //enabletabs();

                    //if ($("#MainContent_HiddenFieldLoadUserType").val() == "ASE")
                    //    $("#id_address").attr("href", "javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&typ=TME&distid=" + username + "','')");
                    //else if ($("#MainContent_HiddenFieldLoadUserType").val() == "DIST")
                    //    $("#id_address").attr("href", "javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&typ=DIST&distid=" + username + "','')");
                    //else if ($("#MainContent_HiddenFieldLoadUserType").val() == "DR")
                    //    $("#id_address").attr("href", "javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&typ=DR&distid=" + username + "','')");

                    $("#MainContent_HiddenFieldIsDistributorSaved").val("1");
                    //EnableUserNameField();
                    //loadDistributorBankGrid(username, 'DistBankGrid');
                    

                    $("#MainContent_buttonbar_buttinSave").removeClass('dissavebtn').addClass('savebtn');
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");
                }
            });
        }

        function DeleteEMEIFromOriginator(originator, emei_no) {
            var r = confirm("Do you want to delete this EMEI no?");
            if (r == true) {
                var param = { 'originator': originator, 'emei_no': emei_no };

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/lead_customer/lead_contacts_Service.asmx/DeleteEMEIFromOriginator",
                    //data: JSON.stringify(param),
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        getEMEINumbers(originator);
                        $("#divEMEINos .k-grid-header").css('display', 'none');
                        $("#divEMEINos .k-grid-content").css('height', '70px');
                    },
                    error: function (response) {
                    }
                });
            }
        }

        /*Load User Lookup in Distributor Entry Page*/
        function getEMEINumbers(originator) {
            $("#div_loader").show();
            var deptString = "DIST";
            $("#divEMEINos").html("");
            $("#divEMEINos").kendoGrid({
                height: 70,
                columns: [
                    { field: "originator", title: "originator", hidden: true },
                    {
                        template: '<a href="javascript:OpenEMEIForEdit(\'#=originator#\',\'#=emei_no#\')">#=emei_no#</a>',
                        field: "emei_no", width: "200px"
                    },
                    {
                        template: '<a href="javascript:DeleteEMEIFromOriginator(\'#=originator#\',\'#=emei_no#\')">Delete</a>',
                        width: "70px"
                    }
                ],
                editable: false, // enable editing
                pageable: false,
                sortable: true,
                filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 2,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                UserName: { validation: { required: true } },
                                Name: { validation: { required: true } }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllEMEIByOriginator", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { originator: originator, deptString: deptString, pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                $("#div_loader").hide();
                                if (textStatus == "success") {

                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }
    </script>
</asp:Content>
