﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class lead_customer_transaction_distributor_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ////if (UserSession.Instance.OriginatorString.Equals("ADMIN"))
        ////{
        ////    LoadASEs();
        ////}
        ////else if (UserSession.Instance.OriginatorString.Equals("ASE"))
        ////{
        ////    //ddlASE.Text = UserSession.Instance.Originator;

        ////    List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        ////    aseList.Insert(0, new OriginatorDTO() { Name = UserSession.Instance.Originator, UserName = "" });
        ////    ddlASE.DataSource = aseList;
        ////    ddlASE.DataTextField = "Name";
        ////    ddlASE.DataValueField = "UserName";
        ////    ddlASE.DataBind();
        ////    ddlASE.Enabled = false;

        ////    hdnASE.Value = UserSession.Instance.UserName;
        ////}
        //////LoadASEs();
        //LoadAREAs();

        buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
        buttonbar.VisibleSave(true);
        buttonbar.EnableSave(true);

        //buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
        //buttonbar.VisibleDeactivate(true);
        //buttonbar.EnableDeactivate(true);

        DisableTabs();
        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("Distributor Entry ", "#", "");

        //if (Request.QueryString["type"].ToString() == "ASE")
        //{
        //    LableType.Text = "ASM";
        //    dh.Style.Add("display", "none");
        //    dh2.Style.Add("display", "none");
        //    Master.SetBreadCrumb("ASM Entry ", "#", "");
        //    HiddenFieldLoadUserType.Value = "ASE";

        //    dis_ase.Style.Add("display", "none");
        //    dh3.Style.Add("display", "none");
        //    dh4.Style.Add("display", "none");

        //    if (UserSession.Instance.OriginatorString.Equals("ASE"))
        //    {
        //        //id_select_user.Visible = false;
        //        //HiddenFieldSelectedOriginator.Value = UserSession.Instance.UserName;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "abc", "DisableUserNameField();", true);
        //        //TxtUserName.Enabled = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "abcd", "GetDistributorDetails('" + UserSession.Instance.UserName.Trim() + "');", true);
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "abcde", "SetVisibleFalseClearButton();", true);
        //    }
        //}
        //else if (Request.QueryString["type"].ToString() == "DIST")
        //{
       // LableType.Text = "Distributor";
        Master.SetBreadCrumb("Distributor Entry ", "#", "");
        HiddenFieldLoadUserType.Value = "DIST";
        //}
        //else if (Request.QueryString["type"].ToString() == "DR")
        //{
        //    LableType.Text = "Distributor Representative";
        //    Master.SetBreadCrumb("Distributor Representative Entry ", "#", "");
        //    HiddenFieldLoadUserType.Value = "DR";
        //}

        ////if (!IsPostBack)
        ////{
        ////    buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
        ////    buttonbar.VisibleSave(true);
        ////    buttonbar.EnableSave(true);

        ////    buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
        ////    buttonbar.VisibleDeactivate(true);
        ////    buttonbar.EnableDeactivate(true);

        ////    HiddenFieldIsDistributorSaved.Value = "0";
        ////    Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>();
        ////}

        if (!IsPostBack)
        {
            //if (UserSession.Instance.OriginatorString.Equals("ADMIN"))
            //{
            //    LoadASEs();
            //}
            //else if (UserSession.Instance.OriginatorString.Equals("ASE"))
            //{
            //    //ddlASE.Text = UserSession.Instance.Originator;

            //    List<OriginatorDTO> aseList = new List<OriginatorDTO>();
            //    aseList.Insert(0, new OriginatorDTO() { Name = UserSession.Instance.Originator, UserName = "" });
            //    ddlASE.DataSource = aseList;
            //    ddlASE.DataTextField = "Name";
            //    ddlASE.DataValueField = "UserName";
            //    ddlASE.DataBind();
            //    ddlASE.Enabled = false;

            //    hdnASE.Value = UserSession.Instance.UserName;
            //}

            LoadOptions();

            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.VisibleSave(true);
            buttonbar.EnableSave(true);

            //buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
            //buttonbar.VisibleDeactivate(true);
            //buttonbar.EnableDeactivate(true);

            HiddenFieldIsDistributorSaved.Value = "0";
            Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>();
        }

        hfOriginator.Value = UserSession.Instance.UserName;
        hfOriginatorType.Value = UserSession.Instance.OriginatorString;
    }

    private void LoadOptions()
    {
        #region Territory

        //MasterClient masterlient = new MasterClient();
        //ArgsModel args1 = new ArgsModel();
        //args1.StartIndex = 1;
        //args1.RowCount = 10000;
        //args1.OrderBy = " area_id asc";

        //List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        //objTrrList = masterlient.GetAllTerritoryByOriginator(args1, UserSession.Instance.OriginatorString,
        //                                                                        UserSession.Instance.UserName);

        //if (objTrrList.Count != 0)
        //{
        //    ddlTerritory.Items.Clear();
        //    ddlTerritory.Items.Add(new ListItem("- Select Territory -", "0"));
        //    foreach (TerritoryModel item in objTrrList)
        //    {
        //        var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
        //        string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

        //        ddlTerritory.Items.Add(new ListItem(result + "  |  " + item.TerritoryName, item.TerritoryId.ToString()));
        //    }

        //    ddlTerritory.Enabled = true;
        //}

        #endregion
    }

    //private void LoadASEs()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.OrderBy = "name ASC";
    //    args.AdditionalParams = " dept_string = 'ASE' ";
    //    args.StartIndex = 1;
    //    args.RowCount = 500;
    //    OriginatorClient originatorClient = new OriginatorClient();
    //    List<OriginatorDTO> aseList = new List<OriginatorDTO>();
    //    aseList = originatorClient.GetOriginatorsByCatergory(args);

    //    if (aseList.Count > 0)
    //        aseList.Insert(0, new OriginatorDTO() { Name = "", UserName = "" });

    //    ddlASE.DataSource = aseList;
    //    ddlASE.DataTextField = "Name";
    //    ddlASE.DataValueField = "UserName";
    //    ddlASE.DataBind();
    //}

    //private void LoadAREAs()
    //{
    //    //ArgsDTO args = new ArgsDTO();
    //    //args.OrderBy = "name ASC";
    //    //args.AdditionalParams = " dept_string = 'ASE' ";
    //    //args.StartIndex = 1;
    //    //args.RowCount = 500;

    //    ArgsDTO args = new ArgsDTO();
    //    args.OrderBy = "area_id ASC";
    //    //args.AdditionalParams = " dept_string = 'ASE' ";
    //    args.StartIndex = 1;
    //    args.RowCount = 500;
    //    CommonClient commonClient = new CommonClient();
    //    List<CityAreaDTO> areaList = new List<CityAreaDTO>();
    //    areaList = commonClient.GetAllCityAreas(args);

    //    //CommonClient commonClient = new CommonClient();
    //    //List<CityAreaDTO> areaList = new List<CityAreaDTO>();
    //    //areaList = commonClient.GetAllCityAreas();

    //    if (areaList.Count > 0)
    //        areaList.Insert(0, new CityAreaDTO() { AreaName = "", AreaID = 0 });

    //    ddlArea.DataSource = areaList;
    //    ddlArea.DataTextField = "AreaCode";
    //    ddlArea.DataValueField = "AreaID";
    //    ddlArea.DataBind();
    //}

    protected void ButtonSave_Click(object sender)
    {
        SaveDistributorDetails();
    }
    


    public void SaveDistributorDetails()
    {
        DistributerModel distributor = new DistributerModel();
        DistributorClient distributorClient = new DistributorClient();
        CommonUtility commonUtility = new CommonUtility();

        distributor.DistributorId = String.IsNullOrEmpty(HiddenFieldSelectedDistId.Value) ? 0 : Convert.ToInt32(HiddenFieldSelectedDistId.Value);
        distributor.Name = txtName.Text;
        distributor.DisplayName = txtDisplayName.Text;
        distributor.DeptString = Request.QueryString["type"].ToString();

        //if (distributor.DeptString == "DIST")
        //{
            if (distributor.DistributorId == 0)
            {
                distributor.Originator = TxtUserName.Text;
                distributor.Code = TxtUserName.Text;
            }
            else
            {
                distributor.Originator = HiddenFieldSelectedOriginator.Value;
                distributor.Code = HiddenFieldSelectedOriginator.Value;
            }
        //}
        //else
        //{
        //    if (distributor.DistributorId == 0)
        //    {
        //        distributor.Originator = TxtUserName.Text;
        //        distributor.Code = TxtUserName.Text;
        //    }
        //    else
        //    {
        //        distributor.Originator = HiddenFieldSelectedOriginator.Value;
        //        distributor.Code = HiddenFieldSelectedOriginator.Value;
        //    }
        //    //distributor.Originator = HiddenFieldSelectedOriginator.Value;
        //    //distributor.Code = HiddenFieldSelectedOriginator.Value;
        //}

        distributor.Password = TxtPassword.Text;
        distributor.Adderess = TxtAddress1.Text;
        //distributor.Telephone = TxtTelephone.Text;
        distributor.Mobile = txtMobile.Text;
        distributor.Email = TextBoxEmail.Text;
        //distributor.Holiday = DropDownListHoliday.SelectedValue;
        distributor.CreatedBy = UserSession.Instance.UserName;
        distributor.LastModifiedBy = UserSession.Instance.UserName;
        //distributor.AreaId = int.Parse((hdnArea.Value == "") ? "0" : hdnArea.Value);
        distributor.Status = ddlASEStatus.SelectedValue;
        distributor.IsNotEMEIUser = chkIsNotEmeiUser.Checked;
        distributor.NIC = txtNIC.Text;

        //if (distributor.DeptString == "ASE")
        //{
        //    distributor.ParentOriginator = hdnASE.Value;//UserSession.Instance.UserName;
        //    distributor.LastModifiedBy = UserSession.Instance.UserName;
        //}
        //else
        //{
        //distributor.ParentOriginator = hdnASE.Value;//UserSession.Instance.UserName;
        distributor.CreatedBy = UserSession.Instance.UserName;
            distributor.LastModifiedBy = UserSession.Instance.UserName;
        //}

        ////Setyting Holidays
        //List<string> holidayList = new List<string>();
        //string hday = "";
        //foreach (ListItem item in chklstHolidays.Items)
        //{
        //    if (item.Selected)
        //    {
        //        hday = item.Value;
        //        holidayList.Add(hday);
        //    }
        //}

        //distributor.HolidaysList = holidayList;

        if (distributor.Name == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please enter distributor name");
            return;
        }
        else if (distributor.Originator == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please enter user name");
            return;
        }
        else if (distributor.Password == "")
        {
            //div_message.Attributes.Add("style", "display:block");
            //div_message.InnerHtml = commonUtility.GetErrorMessage("Please enter a password");
            //return;
        }
        else if (distributor.Password != TxtVerifyPassword.Text)
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please confirm password");
            return;
        }
        else if (IsUserNameExists(distributor) && distributor.DistributorId == 0)
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Username Exists!");
            return;
        }
        //else if ((distributor.ParentOriginator == null || distributor.ParentOriginator == "") && Request.QueryString["type"].ToString() == "DIST")
        //{
        //    div_message.Attributes.Add("style", "display:block");
        //    div_message.InnerHtml = commonUtility.GetErrorMessage("Please Select an ASM");
        //    return;
        //}
        //else if (Request.QueryString["type"].ToString() == "ASE")
        //{
        //    distributor.ParentOriginator = UserSession.Instance.UserName;
        //}
        else
        {
            bool iNoRecs = distributorClient.SaveDistributor(distributor);

            if (iNoRecs)
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Completed.");
                HiddenFieldIsDistributorSaved.Value = "1";
                HiddenFieldSelectedDistId.Value = "0";
                HiddenFieldSelectedOriginatorId.Value = "0";
                HiddenFieldSelectedOriginator.Value = "";
                buttonbar.EnableSave(true);
                EnableTabs();
                Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>(distributor.Originator, Request.QueryString["type"].ToString());
                txtDBCode.Text = "";
                txtName.Text = "";
                txtNIC.Text = "";
                txtDisplayName.Text = "";
                TxtUserName.Text = "";
                TxtPassword.Text = "";
                TxtVerifyPassword.Text = "";
                TxtAddress1.Text = "";
                //TxtTelephone.Text = "";
                txtMobile.Text = "";
                TextBoxEmail.Text = "";
                //DropDownListHoliday.SelectedIndex = 0;
                //ddlASE.SelectedIndex = 0;
                //ddlArea.SelectedIndex = 0;
            }
            else
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured.");
            }
        }
    }

    //protected void ButtonClear_Click(object sender)
    //{
    //    HiddenFieldSelectedDistId.Value = "0";
    //    HiddenFieldSelectedOriginatorId.Value = "0";
    //    HiddenFieldSelectedOriginator.Value = "";

    //    txtName.Text = "";
    //    TxtUserName.Text = "";
    //    TxtPassword.Text = "";
    //    TxtVerifyPassword.Text = "";
    //    TxtAddress1.Text = "";
    //    TxtTelephone.Text = "";
    //    txtMobile.Text = "";
    //    TextBoxEmail.Text = "";
    //    DropDownListHoliday.SelectedIndex = 0;

    //    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "EnableUserNameField();", true);
    //    HiddenFieldIsDistributorSaved.Value = "0";

    //    buttonbar.EnableSave(true);
    //    Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>();
    //    div_message.Attributes.Add("style", "display:none");
    //}


    private void DisableTabs()
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "disabletabs();", true);
    }

    private void EnableTabs()
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "enabletabs();", true);
    }

    private bool IsUserNameExists(DistributerModel distributorDTO)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        bool exists = false;
        try
        {
            OriginatorDTO ori = originatorClient.GetOriginator(distributorDTO.Originator);
            if (ori != null)
            {
                if (ori.OriginatorId != null)
                    if (ori.OriginatorId > 0)
                        exists = true;
            }
            else
                exists = false;
            return exists;
        }
        catch (Exception)
        {
            return true;
        }
    }
}