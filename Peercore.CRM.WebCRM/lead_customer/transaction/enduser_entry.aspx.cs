﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using CRMServiceReference;
using System.IO;
using System.ServiceModel;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class lead_customer_transaction_enduser_entry : PageBase
{
    #region Constant
    CommonUtility commonUtility = new CommonUtility();
    #endregion

    #region Properties
    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }
    #endregion Properties

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //Remove Other Session.
            ClearSession();

            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
            buttonbar.onButtonActiveDeactive = new usercontrols_buttonbar.ButtonActiveDeactive(ButtonActiveDeactive_Click);
            buttonbar.onButtonActivityHistory = new usercontrols_buttonbar.ButtonActivityHistory(ButtonActivityHistory_Click);

            buttonbar.VisibleSave(true);
            buttonbar.VisibleDeactivate(true);
            buttonbar.VisibleActiveDeactive(true);
            buttonbar.VisibleActivityHistory(true);
            //buttonbar.SetActiveDeactive(true);

            if (!IsPostBack)
            {

                SetBackLink();
                if ((Request.QueryString["eduid"] != null && Request.QueryString["eduid"] != string.Empty)
                        && (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty))
                {
                    //Edit


                    string enduser_code = Server.HtmlDecode(Request.QueryString["eduid"].ToString());
                    string customer_code = Server.HtmlDecode(Request.QueryString["custid"].ToString());

                    EndUserEntry = new KeyValuePair<string, string>(enduser_code, "enduser_code");

                    HiddenEndUserCode.Value = Server.UrlEncode(enduser_code);
                    HiddenFieldCustomerCode.Value = Server.UrlEncode(customer_code);

                    GetEndUser(enduser_code, customer_code);
                    //GetEndUser(enduser_code, customer_code);

                  //  id_Dist.Visible = false;
                   id_dist_disable.Visible = false;
                }


                else
                {
                    //New

                    DisableTabs();
                    ClearFields();
                    GetCustomer(Request.QueryString["custid"]);
                    buttonbar.SetActiveDeactive(false);
                    buttonbar.EnableActiveDeactive(false);
                    Session.Remove(CommonUtility.ENDUSER_DATA);
                }

                HiddenFieldTabIndex.Value = "1";

                if (Request.QueryString["fm"] == "opp")
                {
                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunity.aspx";//?cht=pipe&fm=lead&pipid=1";
                }
                if (Request.QueryString["ref"] != null && Request.QueryString["ref"] != string.Empty)
                {
                    if (Request.QueryString["ref"] == "con")
                    {
                        HiddenFieldTabIndex.Value = "1";
                    }

                    if (Request.QueryString["ref"] == "act")
                    {
                        HiddenFieldTabIndex.Value = "2";
                    }
                    if (Request.QueryString["ref"] == "doc")
                    {
                        HiddenFieldTabIndex.Value = "3";
                    }
                    if (Request.QueryString["ref"] == "opp")
                    {
                        HiddenFieldTabIndex.Value = "4";
                    }
                    if (Request.QueryString["ref"] == "end")
                    {
                        HiddenFieldTabIndex.Value = "5";
                    }
                    if (Request.QueryString["ref"] == "pri")
                    {
                        HiddenFieldTabIndex.Value = "5";
                    }
                    if (Request.QueryString["ref"] == "sal")
                    {
                        HiddenFieldTabIndex.Value = "6";
                    }
                }

            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Distributor End User Entry ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        if (Page.IsValid)
        {
            SaveEndUser();
        }
    }

    protected void ButtonClear_Click(object sender)
    {
        ClearFields();
    }

    protected void ButtonActiveDeactive_Click(object sender)
    {
        try
        {
            EndUserClient endUserClient = new EndUserClient();
            ArgsDTO args = new ArgsDTO();
            if ((!string.IsNullOrWhiteSpace(HiddenEndUserCode.Value)) && (!string.IsNullOrWhiteSpace(HiddenFieldCustomerCode.Value)))
            {
                args.CustomerCode = HiddenFieldCustomerCode.Value;
                args.EnduserCode = HiddenEndUserCode.Value;

                EndUserDTO enduser = endUserClient.GetEndUser(args);

                int noOfRecsEffected = endUserClient.SetActiveStatus(args, !enduser.IsActive);

                if (enduser.IsActive)
                {
                    buttonbar.SetActiveDeactive(true);
                    buttonbar.EnableSave(false);
                    buttonbar.EnableDeactivate(false);
                    DisableTabs();
                }
                else
                {
                    buttonbar.SetActiveDeactive(false);
                    buttonbar.EnableSave(true);
                    buttonbar.EnableDeactivate(true);
                    EnableTabs();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ButtonActivityHistory_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "activity_planner/transaction/activity_history.aspx?fm=acthis&custid=" + HiddenFieldCustomerCode.Value +
        "&eduid=" + HiddenEndUserCode.Value);
    }
    #endregion

    #region Methods
    //Remove Other Session.
    private void ClearSession()
    {
        Session[CommonUtility.LEAD_DATA] = null;
        Session[CommonUtility.CUSTOMER_DATA] = null;
    }

    private void SetBackLink()
    {

        if ((Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"])) && (Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"])))
        {
            if (Request.QueryString["ty"] == "customer")
            {
                hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&fm=lead" + "&ty=customer" + "&ref=end#tabs";
            }
            else
            {
                if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
                    hlBack.NavigateUrl = "~/lead_customer/transaction/EndUser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&fm=" + Request.QueryString["fm"] + "&ref=end#tabs";
                else if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
                    hlBack.NavigateUrl = "~/lead_customer/transaction/EndUser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&ty=" + Request.QueryString["ty"] + "&ref=end#tabs";
                else
                    hlBack.NavigateUrl = "~/lead_customer/transaction/EndUser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&ref=end#tabs";
            }
        }

        else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
                hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&fm=" + Request.QueryString["fm"] + "&ref=end#tabs";
            else
                hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&fm=lead" + "&ty=customer" + "&ref=end#tabs";
        }

        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        {
            if (Request.QueryString["fm"] == "endUser")
                hlBack.NavigateUrl = "~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=End User" + "&fm=" + Request.QueryString["fm"];
        }

    }

    //private void SetBackLink()
    //{
    //    if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
    //    {
    //        if (Request.QueryString["fm"] == "home")
    //        {
    //            hlBack.NavigateUrl = "~/default.aspx?fm=home";
    //        }
    //        else if (Request.QueryString["fm"] == "activity")
    //        {
    //            hlBack.NavigateUrl = "~/activity_planner/transaction/activity_scheduler.aspx";
    //        }
    //        else if (Request.QueryString["fm"] == "act")
    //        {
    //            hlBack.NavigateUrl = "~/calendar/transaction/appointment_entry.aspx";
    //        }
    //        else if (Request.QueryString["fm"] == "dsbd")
    //        {
    //            if (Request.QueryString["rptid"] != null && Request.QueryString["rptid"] != string.Empty)
    //            {
    //                if (Request.QueryString["rptid"].ToString() == "1" || Request.QueryString["rptid"].ToString() == "2")
    //                {
    //                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx?rptid=" + Request.QueryString["rptid"].ToString()
    //                        + "&month=" + Request.QueryString["month"].ToString() + "&fromdate=" + Request.QueryString["fromdate"].ToString() + "&todate="
    //                        + Request.QueryString["todate"].ToString() + "&leadstage=" + Request.QueryString["leadstage"].ToString(); ;
    //                }
    //                else
    //                {
    //                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx?rptid=" + Request.QueryString["rptid"].ToString();
    //                }
    //            }
    //            else
    //                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx";
    //        }
    //        else if (Request.QueryString["fm"] == "dsbd1")
    //        {
    //            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/BdmWideSalesAnalysis.aspx";
    //        }
    //        else if (Request.QueryString["fm"] == "dsbd2")
    //        {
    //            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/BdmWideServiceCallAnalysis.aspx";
    //        }
    //        else if (Request.QueryString["fm"] == "dsbd3")
    //        {
    //            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/CheckListAnalysis.aspx";
    //        }
    //        else if (Request.QueryString["fm"] == "opp")
    //        {
    //            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunity.aspx?cht=pipe&fm=lead";
    //        }
    //        else if (Request.QueryString["fm"] == "lead")
    //        {
    //            if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
    //            {
    //                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/lead_contacts.aspx?ty=" + Request.QueryString["ty"] + "&fm=lead";
    //            }
    //        }
    //        else if (Request.QueryString["fm"] == "search")
    //        {
    //            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/search.aspx";
    //        }
    //    }
    //}
    private void GetCustomer(string customercode)
    {
        try
        {
            HiddenFieldCustomerCode.Value = customercode;
            CustomerClient customerClient = new CustomerClient();
            CustomerDTO customerDTO = customerClient.GetCustomer(customercode);
            if (!string.IsNullOrWhiteSpace(customerDTO.Name))
                TextBoxDist.Text = customerDTO.Name;

            txtAssignedTo.Text = UserSession.Instance.RepCode;
        }
        catch (Exception ex)
        {
        }
    }
    private void GetEndUser(string endusercode, string customercode)
    {
        EndUserClient endUserClient = new EndUserClient();
        ArgsDTO arg = new ArgsDTO();

        try
        {
            arg.CustomerCode = Server.UrlDecode(customercode);
            arg.EnduserCode = Server.UrlDecode(endusercode);

            EndUserDTO enduser = endUserClient.GetEndUser(arg);

            SettingSession(endusercode);

            if (enduser == null)
                return;

            #region Disable Controls
            //DisableControls();
            #endregion

            if (!string.IsNullOrWhiteSpace(enduser.CustomerName))
                TextBoxDist.Text = enduser.CustomerName;
            else
                TextBoxDist.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.EndUserCode))
                txtCode.Text = enduser.EndUserCode;
            else
                txtCode.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.Name))
                txtCustomerName.Text = enduser.Name;
            else
                txtCustomerName.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.Contact))
                txtContact.Text = enduser.Contact;
            else
                txtContact.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.Telephone))
                mtxtPhone.Text = enduser.Telephone;
            else
                mtxtPhone.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.EmailAddress))
                txtEmail.Text = enduser.EmailAddress;
            else
                txtEmail.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.PrimaryRepName))
                div_fsrep.InnerHtml = "<strong> :" + enduser.PrimaryRepName + "</strong>";
            else
                div_fsrep.InnerHtml = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.Address2))
                txtAddress2.Text = enduser.Address2;
            else
                txtAddress2.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.Address1))
                txtAddress1.Text = enduser.Address1;
            else
                txtAddress1.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.City))
                txtCustCity.Text = enduser.City;
            else
                txtCustCity.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.State))
                txtCustState.Text = enduser.State;
            else
                txtCustState.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.PostCode))
                txtCustPostcode.Text = enduser.PostCode;
            else
                txtCustPostcode.Text = string.Empty;

            if (!string.IsNullOrWhiteSpace(enduser.RepCode))
                txtAssignedTo.Text = enduser.RepCode;
            else
                txtAssignedTo.Text = string.Empty;

            if (enduser.Rating != null)
                rRating.Value = enduser.Rating;

            if (!string.IsNullOrWhiteSpace(enduser.SecondaryRepName))
                div_BakeryRep.InnerHtml = "<strong> :" + enduser.SecondaryRepName + "</strong>";
            else
                div_BakeryRep.InnerHtml = string.Empty;

            if (enduser.PrimaryDistributor == "Y")
                chkprimarychk.Checked = true;
            else
                chkprimarychk.Checked = false;

            if (enduser.EndUserType == "B")
                radiobakery.Checked = true;
            else if (enduser.EndUserType == "F")
                radiofood.Checked = true;

            if (!enduser.IsActive)
            {
                buttonbar.EnableSave(false);
                buttonbar.EnableDeactivate(false);
                buttonbar.SetActiveDeactive(true);
                DisableTabs();
            }
            else
            {
                buttonbar.EnableSave(true);
                buttonbar.EnableDeactivate(true);
                buttonbar.SetActiveDeactive(false);
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            div_info.Attributes.Add("style", "display:block");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void ClearFields()
    {
        div_message.InnerHtml = "";
        div_message.Attributes.Add("style", "display:none;padding:2px");

        div_info.Attributes.Add("style", "display:none");
        div_info.InnerHtml = "";

        HiddenFieldExistClickOk.Value = "";
        HiddenFieldOtherExistClickOk.Value = "";

        TextBoxDist.Text = string.Empty;
        txtCode.Text = string.Empty;
        txtCode.ReadOnly = false;
        txtCustomerName.Text = string.Empty;
        txtContact.Text = string.Empty;
        mtxtPhone.Text = string.Empty;
        txtEmail.Text = string.Empty;
        div_fsrep.InnerHtml = "";
        txtAddress1.Text = string.Empty;
        txtAddress2.Text = string.Empty;
        txtCustCity.Text = string.Empty;
        txtCustPostcode.Text = string.Empty;
        txtCustState.Text = string.Empty;
        chkprimarychk.Checked = false;
        rRating.Value = 0;
        div_BakeryRep.InnerHtml = "";

        buttonbar.EnableActiveDeactive(false);

        if (UserSession.Instance.RepType == "B")
            radiobakery.Checked = true;
        else
            radiofood.Checked = true;

        id_Dist.Visible = true;
        id_dist_disable.Visible = false;

        EndUserEntry = new KeyValuePair<string, string>();

        DisableTabs();
    }

    private void SettingSession(string endusercode)
    {
        KeyValuePair<string, string> enduser_data = new KeyValuePair<string, string>(Server.UrlEncode(endusercode), "EndUser");

        //Setting session value.
        EndUserEntry = enduser_data;
    }

    private void SaveEndUser()
    {
        List<string> errorlist = new List<string>();
        EndUserClient endUserClient = new EndUserClient();
        try
        {
            if (VlidateView(ref errorlist))
            {
                //HiddenEndUserCode.Value = txtCode.Text;
                EndUserDTO endUserDTO = ValidateEndUserForThisCustomer();

                if ((endUserDTO.Name != null) && ((HiddenFieldExistClickOk.Value != "1")))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showenduserexistmsg('" + endUserDTO.Name.Trim() + "');", true);
                    return;
                }
                else
                {
                    List<EndUserDTO> listEndUser = ValidateEndUserForOtherCustomers();

                    if ((listEndUser != null && listEndUser.Count > 0) && ((HiddenFieldExistClickOk.Value != "1") &&
                            (HiddenFieldOtherExistClickOk.Value != "1")))
                    {
                        StringBuilder sMsg = new StringBuilder();
                        sMsg.Append("End user code entered has been saved against distributor(s)....");
                        foreach (EndUserDTO endUser in listEndUser)
                            sMsg.Append(" as '" + endUser.Name + "' for '" + endUser.CustomerName + "'" + System.Environment.NewLine);

                        sMsg.Append("Do you want to continue saving?");
                        string a = HttpUtility.JavaScriptStringEncode(sMsg.ToString());
                        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showenduserexistmsg('" + a + "');", true);
                        return;
                    }
                    else {
                        HiddenFieldExistClickOk.Value = "1";
                    }
                }

                #region End User Save
                if (HiddenFieldExistClickOk.Value == "1" || endUserDTO == null)
                {
                    EndUserDTO enduser = new EndUserDTO();

                    enduser.Address1 = txtAddress1.Text;
                    enduser.Address2 = txtAddress2.Text;
                    enduser.Contact = txtContact.Text;
                    enduser.EmailAddress = txtEmail.Text;
                    enduser.EndUserCode = txtCode.Text.Trim();
                    enduser.CustomerCode = HiddenFieldCustomerCode.Value;
                    enduser.Name = txtCustomerName.Text;
                    enduser.City = txtCustCity.Text;
                    enduser.PostCode = txtCustPostcode.Text;
                    enduser.State = txtCustState.Text;
                    enduser.Telephone = mtxtPhone.Text == null ? string.Empty : mtxtPhone.Text.Trim();
                    enduser.RepCode = txtAssignedTo.Text;

                    enduser.PrimaryDistributor = (chkprimarychk.Checked == true ? "Y" : "N");
                    enduser.Rating = rRating.Value != null ? Convert.ToInt32(rRating.Value) : 0;
                    enduser.EndUserType = (Convert.ToBoolean(radiobakery.Checked) ? "B" :
                        (Convert.ToBoolean(radiofood.Checked) ? "F" : ""));

                    string endusercode = string.Empty;
                    bool noOfRecs = endUserClient.EndUserSave(enduser);


                    if (noOfRecs)
                    {
                        GetEndUser(enduser.EndUserCode, enduser.CustomerCode);
                        buttonbar.EnableActiveDeactive(true);
                        HiddenEndUserCode.Value = enduser.EndUserCode;
                        HiddenFieldExistClickOk.Value = "";
                        HiddenFieldOtherExistClickOk.Value = "";
                        div_message.Attributes.Add("style", "display:block;padding:2px");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");

                        div_info.Attributes.Add("style", "display:none");
                        div_info.InnerHtml = "";
                    }
                }
                #endregion
            }
            else
            {


                HiddenFieldExistClickOk.Value = "";
                HiddenFieldOtherExistClickOk.Value = "";
                div_info.Attributes.Add("style", "display:block;padding:2px");
                div_message.Attributes.Add("style", "display:none");
                div_info.InnerHtml = commonUtility.GetLengthyErrorMessage(errorlist[0].ToString());
            }
        }
        catch (Exception)
        {
            //Error Occured.
            //commonUtility.ErrorMaintain(ex);
            HiddenFieldExistClickOk.Value = "";
            HiddenFieldOtherExistClickOk.Value = "";
            div_info.Attributes.Add("style", "display:block;padding:2px");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private bool VlidateView(ref List<string> errorMsges)
    {
        EndUserClient enduserClient = new EndUserClient();
        List<EndUserDTO> enduserList ;
        ArgsDTO args = new ArgsDTO();

        bool isValid = true;
        if (string.IsNullOrWhiteSpace(HiddenFieldCustomerCode.Value))
        {
            errorMsges.Add("Customer cannot be empty.");
            isValid = false;
            //return false;
        }
        else if (string.IsNullOrWhiteSpace(txtCode.Text))
        {
            errorMsges.Add("End user code cannot be empty.");
            isValid = false;
            //return false;
        }
        else if (txtCode.Text != "")
        {
            StringBuilder error = new StringBuilder();
            error.Append("Invalid Enduser Code!" + Environment.NewLine + "Enduser Code should be of 8 characters long. " +
                            "Second last character should be in 'TVSWNNQ' and last character should be a number " +
                            "OR last two characters should be 'NZ'.");

            if (txtCode.Text.Trim().Length != 8)
            {
                errorMsges.Add(error.ToString());
                isValid = false;
                //return false;
            }
            else if (txtCode.Text.Trim().Substring(6, 2) != "NZ")
            {
             if (!"TVSWNNQ".Contains(txtCode.Text.Trim().Substring(6, 1).ToUpper()))
           //     if (!"TVSWNNQ".Contains(txtCode.Text.Trim().Substring(0, 7).ToUpper()))
                {
                    errorMsges.Add(error.ToString());
                    isValid = false;
                    // return false;
                }
                else if (!"0123456789".Contains(txtCode.Text.Trim().Substring(7, 1)))
                {
                    errorMsges.Add(error.ToString());
                    isValid = false;
                    //return false;
                }
            }
        }
        else if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
        {
            errorMsges.Add("Name cannot be empty.");
            isValid = false;
            //return false;
        }
        else if (string.IsNullOrWhiteSpace(txtAssignedTo.Text))
        {
            errorMsges.Add("Assigned To cannot be empty.");
            isValid = false;
            //return false;
        }

        //string param = "de.enduser_code = '" + txtCode.Text + "'";
        //enduserList = new List<EndUserDTO>(enduserClient.GetEndUsersByCode(param));

        //if (enduserList.Count > 0)
        //{
        //    // isValid = false;


        //  //  ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showenduserexist();", true);
        //    isValid = false;
        //}

        HiddenEndUserCode.Value = txtCode.Text.Trim();

        return isValid;
    }

    private EndUserDTO ValidateEndUserForThisCustomer()
    {
        EndUserClient endUserClient = new EndUserClient();
        if (!string.IsNullOrWhiteSpace(HiddenFieldCustomerCode.Value) && !string.IsNullOrWhiteSpace(HiddenEndUserCode.Value))
        {
            ArgsDTO args = new ArgsDTO();
            args.CustomerCode = Server.UrlDecode(HiddenFieldCustomerCode.Value);
            args.EnduserCode = Server.UrlDecode(HiddenEndUserCode.Value.ToUpper());
            return endUserClient.IsEndUserExistForCustomer(args);
        }

        return null;
    }

    private List<EndUserDTO> ValidateEndUserForOtherCustomers()
    {
        EndUserClient endUserClient = new EndUserClient();
        if (!string.IsNullOrWhiteSpace(HiddenFieldCustomerCode.Value) && !string.IsNullOrWhiteSpace(HiddenEndUserCode.Value))
        {
            ArgsDTO args = new ArgsDTO();
            args.CustomerCode = HiddenFieldCustomerCode.Value;
            args.EnduserCode = HiddenEndUserCode.Value.ToUpper();
            return endUserClient.IsEndUserExistForOtherCustomers(args);
        }

        return null;
    }

    private void DisableTabs()
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "disabletabs();", true);
    }

    private void EnableTabs()
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "enabletabs();", true);
    }
    #endregion
}