﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="leadentry.aspx.cs" Inherits="lead_customer_transaction_leadentry" %>

<%@ Register Src="~/usercontrols/entrypage_tabs.ascx" TagName="pagetabs" TagPrefix="ucl1" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="HiddenAssignTo" runat="server" />
    <asp:HiddenField ID="HiddenFieldBDMGroup" runat="server" />
    <asp:HiddenField ID="HiddenFieldLeadId" runat="server" />
    <asp:HiddenField ID="HiddenFieldUsertype" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactPhone" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactMobile" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactFax" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactWebSite" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactEmail" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactPrefMethod" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustAddress" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustCity" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustPostcode" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustState" runat="server" />
    <asp:HiddenField ID="HiddenFieldIsContinueWithSave" runat="server" />
    <asp:HiddenField ID="HiddenFieldHasClickedCheckList" runat="server" />
    <asp:HiddenField ID="HiddenFieldConvertCustCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldConversionSent" runat="server" />
    <asp:HiddenField ID="HiddenFieldCheckYesClicked" runat="server" />
    <asp:HiddenField ID="HiddenFieldDisableAddContactDetails" runat="server" />
    <asp:HiddenField ID="HiddenFieldDisableAddContactPerson" runat="server" />
    <asp:HiddenField ID="HiddenFieldIsDeactivate" runat="server" />
    <asp:HiddenField ID="HiddenFieldDeactivateoppotunity" runat="server" />
    <asp:HiddenField ID="HiddenFieldMaxOpportunities" runat="server" />
    <asp:HiddenField ID="HiddenFieldLeadStage" runat="server" />
    <asp:HiddenField ID="HiddenFieldTabIndex" runat="server" Value="1" />
    <asp:HiddenField ID="HiddenFieldConvertCount" runat="server" />
    <script type="text/javascript">
        //         $("a#id_custcode").fancybox({
        //             'titlePosition': 'inside',
        //             'transitionIn': 'none',
        //             'transitionOut': 'none'
        //         });
    </script>
    <div id="div_promt" style="display: none">
    </div>
    <div id="modalWindow" style="display: none">
        <div id="div_duplicaterecord" runat="server" style="display: block">
        </div>
        <div id="div_confirm_message" class="msg">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
        <button id="save" class="k-button">
            Save</button>
    </div>
    <div style="display: none">
        <div id="div_prevcustcode" runat="server">
        </div>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="buttonbar">
                    <asp:Button ID="buttonSave" runat="server" OnClick="buttonSave_Click" CssClass="savebtn" Text="Save" />
                    <asp:Button ID="buttonClear" runat="server" OnClick="buttonClear_Click" CssClass="clearbtn" Text="Clear" />
                    <asp:Button ID="buttonActiveDeactive" runat="server" OnClick="buttonActiveDeactive_Click"
                        CssClass="deactivebtn" />
                    <asp:Button ID="buttonConvert" runat="server" OnClick="buttonConvert_Click" CssClass="convertprosbtn" />
                    <asp:Button ID="buttonActivityHistory"  ToolTip="Activity History"  runat="server" Text="Activity History"
                        OnClick="buttonActivityHistory_Click" CssClass="activity_history"/>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx?ty=Lead">
                        <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <%-- <div id="div1" class="savemsg" runat="server" style="display:none" ></div>--%>
        <div class="clearall">
        </div>
        <div class="clearall">
        </div>
        <div class="right">
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div class="formleft">
            <div class="formtextdiv">
                Name
            </div>
            <div class="formdetaildiv_right"><span class="wosub mand">
                <asp:TextBox ID="txtName" CssClass="tb input-large1" runat="server" Style="text-transform: uppercase;" >
                </asp:TextBox><span style="color: Red">*</span></span>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Company</div>
            <div class="formdetaildiv_right">
                <asp:TextBox ID="txtCompany" runat="server" CssClass="input-large1" Style="text-transform: uppercase"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Market</div>
            <div class="formdetaildivlead">
                <asp:DropDownList ID="dropdownMarket" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Sub Market</div>
            <div class="formdetaildivlead">
                <asp:DropDownList ID="dropdownChannel" runat="server" Width="108px">
                </asp:DropDownList>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Potential Opportunity</div>
            <div class="formdetaildivlead">
                <asp:DropDownList ID="dropdownWeek" runat="server" AppendDataBoundItems="true" Width="108px">
                </asp:DropDownList>
                <asp:TextBox ID="TextBoxLiters" CssClass="input_numeric" runat="server" Style="text-align: right;
                    width: 60px"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Annual Revenue $</div>
            <div class="formdetaildivlead">
                <asp:TextBox ID="TextBoxAnnualRevenue" CssClass="input_numeric" runat="server" Style="text-align: right;
                    width: 100px"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                No. of Employees</div>
            <div class="formdetaildivlead">
                <asp:TextBox ID="TextBoxEmployees" CssClass="input_numeric" runat="server" Style="text-align: right;
                    width: 100px"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Description</div>
            <div class="formdetaildivlead">
                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Columns="28"
                    Rows="3"></asp:TextBox>
            </div>
        </div>
        <div class="formright">
            <div class="formtextdiv">
                Rating</div>
            <div class="formdetaildivlead" style="margin-bottom:15px;">
                <telerik:RadRating ID="rRating" runat="server" Width="110px" ItemCount="5" Value="0"
                    SelectionMode="Continuous" Precision="Half" Orientation="Horizontal" />
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Business Potential</div>
            <div class="formdetaildivlead">
                <asp:TextBox ID="txtBusinessPotential" runat="server"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Referred By</div>
            <div class="formdetaildivlead">
                <asp:TextBox ID="txtRefBy" runat="server"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Source</div>
            <div class="formdetaildivlead">
                <asp:DropDownList ID="DropDownListSource" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Assigned To</div>
            <div class="formdetaildivlead">
                <asp:TextBox ID="txtAssignto" runat="server" ReadOnly="true"></asp:TextBox>
                <a href="#" id="id_assign">
                    <img id="Img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                <%--<a href="#" id="id_assign">
                    </a>--%>
            </div>
            <div class="clearall">
            </div>
            <%--<div class="formtextdiv">
                BDM Group</div>
            <div class="formdetaildivlead">
               
                    <asp:TextBox ID="div_bdm_group" runat="server" ReadOnly="true"></asp:TextBox>
               <a href="#" id="id_bdmgroup">
                    <img id="Img2" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
            </div>
                        

            <div class="clearall"></div>  --%>
            <div id="div_additionalText" runat="server" class="specialevents" align="right">
            </div>
            <div id="div_created" runat="server" class="specialevents" align="right">
            </div>
            <div id="div_modifiedby" runat="server" class="specialevents" align="right">
            </div>
            <div class="names">
                <asp:Label ID="tbStage" runat="server" Text="" CssClass="namehighliter"></asp:Label></div>
            <div class="clearall">
            </div>
        </div>
        <div class="clearall">
        </div>
        <div>
            <span style="color: Red">* Mandatory Fields</span></div>
        <%--New Form Layout--%>
    </div>
    <div>
        &nbsp;</div> <%--<div id="window"></div>--%>
    <a name="tabs"></a>
    <div class="divcontectmainforms">
        <ucl1:pagetabs ID="pagetabs1" runat="server" />
        <script type="text/javascript">
            hideStatusDiv("MainContent_div_message");
            $("#id_assign").click(function () {
                originator('activity_planner/process_forms/processmaster.aspx?fm=activityentry&type=query&querytype=originator');
                //originator('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=originator');
            });

            $("#MainContent_txtName").keyup(function () {
                var leadid = $("input[id=MainContent_HiddenFieldLeadId]").val();

                if (leadid == null || leadid == '') {
                    $("#MainContent_txtCompany").val($("#MainContent_txtName").val());
                }
            });

            $("#MainContent_txtName").blur(function () {
                var leadid = $("input[id=MainContent_HiddenFieldLeadId]").val();

                if (leadid == null || leadid == '') {
                    $("#MainContent_txtCompany").val($("#MainContent_txtName").val());
                }
            });

            $("#MainContent_buttonSave").click(function () {
                if ($("#txtEmail").val() != null && $("#txtEmail").val().length > 0) {

                    jQuery("#txtEmail").validate({
                        expression: "if (isFill(SelfID))" +
                                " {" +
	                        "        if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/))" +
		                    "            return true; " +
	                        "        else " +
		                        "           return false; " +
                                "   } else {return true; }",
                        message: "Please enter valid Email id."
                    });
                } else {
                    jQuery("#txtEmail").validate({
                        onsubmit: true
                    });
                }
            });


            $(document).ready(function () {


                var leadid = $("input[id=MainContent_HiddenFieldLeadId]").val();
                var leadtype = $().val();
                var items = [
                            ["Contact Detail", "usercontrols/contactdetails.aspx?leadid=" + leadid ],
                            ["Addresses", "usercontrols/contactaddress.aspx?leadid=" + leadid],
                            ["Contact Person", "usercontrols/contactperson.aspx?leadid=" + leadid],
                            ["Activity", "usercontrols/activity.aspx?leadid=" + leadid],
                            ["Documents", "usercontrols/document.aspx?leadid=" + leadid],
                            ["Opportunities", "usercontrols/customer_oppotunity.aspx?leadid=" + leadid],
                            ["Emails", "usercontrols/customer_email.aspx?leadid=" + leadid]
                        ];

                loadEntryPageTabs("Lead", items, $("#MainContent_HiddenFieldTabIndex").val());


                //                $("#MainContent_TextBoxLiters").kendoNumericTextBox({
                //                    format: "#.00"
                //                });

                //                $("#MainContent_TextBoxAnnualRevenue").kendoNumericTextBox({
                //                    format: "#.00"
                //                });

                //                $("#MainContent_TextBoxEmployees").kendoNumericTextBox({
                //                    format: "#"
                //                });
                //$("#MainContent_txtName").kendoValidator().data("kendoValidator");


            });


            jQuery(function () {

                jQuery("#MainContent_txtName").validate({
                    expression: "if (VAL) return true; else return false;",
                    message: "Please Enter Name."
                });


            });

            jQuery(function ($) {
                $('input.input_numeric').autoNumeric();
                $('input.input_numeric').autoNumeric({ aSep: '.', aDec: ',' });
            });

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
    </div>
    <div runat="server" id="div_autocomplete">
    </div>
    <div id="Div1" style="display: none">
        <div id="div2" style="height: 300px; overflow: scroll;">
        </div>
        <button id="buttonOk" class="k-button">
            Ok</button>
    </div>
</asp:Content>
