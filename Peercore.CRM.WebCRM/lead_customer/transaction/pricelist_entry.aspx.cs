﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using CRMServiceReference;
using System.IO;
using System.ServiceModel;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System.Web.Script.Serialization;

public partial class lead_customer_transaction_pricelist_entry : PageBase
{
    #region Properties
    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }
    #endregion Properties

    protected void Page_Load(object sender, EventArgs e)
    {
        if (UserSession.Instance.UserName != null)
        {
            if (!IsPostBack)
            {
                if ((Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"])) && (Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"])))
                {
                    HiddenFieldEndUser_EndUsercode.Value = Request.QueryString["eduid"];
                    HiddenFieldEndUser_CustCode.Value = Request.QueryString["custid"];
                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/enduser_entry.aspx?eduid=" + HiddenFieldEndUser_EndUsercode.Value +
                        "&custid=" + HiddenFieldEndUser_CustCode.Value + "&fm=endUser&ref=pri#tabs";
                }

                else if (Request.QueryString["eduid"] != null && Request.QueryString["eduid"] != string.Empty)
                {
                    HiddenFieldEndUser_EndUsercode.Value = Request.QueryString["eduid"];
                    SettingSession(Request.QueryString["eduid"]);
                }
                else if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty)
                {
                    HiddenFieldEndUser_CustCode.Value = Request.QueryString["custid"];
                }
                else
                {
                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/enduser_entry.aspx?eduid=" + HiddenFieldEndUser_EndUsercode.Value +
                       "&custid=" + HiddenFieldEndUser_CustCode.Value + "&ref=pri#tabs";
                }

                if (Request.QueryString["effecdate"] != null && Request.QueryString["effecdate"] != string.Empty)
                {
                    HiddenField_Effecdate.Value = Request.QueryString["effecdate"];
                }
                else
                {
                    HiddenField_Effecdate.Value = DateTime.Today.ToString("dd-MMM-yyyy");
                }

                
                
                //lead_customer/transaction/enduser_entry.aspx?custid=SUNSUNV1&eduid=ASTBELV1&ref=act#tabs
                LoadData();
                //LoadEnduserPrice();
                if (Request.QueryString["ty"] == "edit")
                {
                    HiddenField_Mode.Value = Request.QueryString["ty"];
                }
                else
                {
                    HiddenField_Mode.Value = "new";
                    HiddenField_Effecdate.Value = DateTime.Today.ToString("dd-MMM-yyyy");
                }
                Master.SetBreadCrumb("Pricelist Entry ", "#", "");

                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadpage();", true);
            }
        }

    }
    private void LoadData(){
        CustomerClient customerClient = new CustomerClient();
        txtCustomerName.Text =customerClient.GetCustomer(HiddenFieldEndUser_CustCode.Value).Name;
        txtCustomerName.ToolTip = txtCustomerName.Text;

        EndUserClient endUserClient =new EndUserClient();
        ArgsDTO args =new ArgsDTO();
        args.CustomerCode = HiddenFieldEndUser_CustCode.Value;
        args.EnduserCode = HiddenFieldEndUser_EndUsercode.Value;
        txtEnduser.Text = endUserClient.GetEndUser(args).Name;
            txtEnduser.ToolTip = txtEnduser.Text;

        div_Originator.InnerText = UserSession.Instance.UserName;
    }

    protected void buttonSave_Click(object sender, EventArgs e)
    {
        try
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = "";
            EndUserDTO collection = null;
            EndUserClient oBrEndUser = new EndUserClient();

            if (!string.IsNullOrEmpty(HiddenField_PriceList.Value))
            {
                //jsonString = @"{""CategoryList"":" + HiddenField_PriceList.Value.Replace("\"ExtensionData\":{},", "").Replace("\"EndUserName\":null,", "").Replace("\"CustomerName\":null,", "").Replace("\"CategoryList\":null,", "") + "}";
                jsonString = @"{""CategoryList"":" + HiddenField_PriceList.Value.Replace("\" \":null,", "").Replace("\"ExtensionData\":{},", "").Replace("null", "\" \"").Replace("{\"__type\":\"CRMServiceReference.EndUserPriceDTO\",", "") + "}";
                //jsonString = jsonString.Replace("\"EffectiveDate\":null,", "").Replace("\"EffectiveDate\":{},", "");
                //jsonString = jsonString.Replace("\"LastUpdated\":null,", "").Replace("\"LastUpdated\":{},", "");
                collection = serializer.Deserialize<EndUserDTO>(jsonString);
                //jobCard.JobCardTankList = collection.JobCardTankList;
            }

            EndUserClient endUserClient = new EndUserClient();
            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(HiddenFieldEndUser_CustCode.Value);
            args.EnduserCode = HiddenFieldEndUser_EndUsercode.Value;
            args.ManagerMode = true;
            args.CustomerType = "EndUser";
            args.StartIndex = 1;
            args.RowCount = 1000;
            args.OrderBy = "catlog_code ASC";
            #endregion


            string effectivedate = dpEffectiveDate.Text;
            // string 
            if (string.IsNullOrEmpty(effectivedate))
                effectivedate = DateTime.Now.ToString("dd-MMM-yyyy");


            if (HiddenField_Mode.Value == "edit")
            {
                List<EndUserPriceDTO> lEndUserPrice = endUserClient.GetEndUserPricing(args, effectivedate);


                //        brEndUser oBrEndUser = new brEndUser();

                // if (!isNew)
                //{//
                //List<EndUserPriceDTO> lEndUserPrice = oBrEndUser.GetEndUserPricing(this.custCode, this.endUserCode, this.effectiveDate);

                List<EndUserPriceDTO> lGridList = collection.CategoryList.Where(s => s.Version > 0).ToList();// ((List<EndUserPrice>)rgvEnduserPrice.ItemsSource).Where(s => s.Version > 0).ToList();
                int iGridCount = collection.CategoryList.Where(s => s.Version > 0).Count(); // ((List<EndUserPrice>)rgvEnduserPrice.ItemsSource).Where(s => s.Version > 0).Count();

                // Check the Count of Product Lines
                if (lEndUserPrice.Count != lGridList.Count)
                {
                    div_message.Attributes.Add("style", "display:block;padding:2px");
                    div_message.InnerHtml = new CommonUtility().GetErrorMessage("Data displayed is not up-to-date. Please refresh data and re-save.");
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadpage();", true);
                    return;
                }

                // Check the versions
                foreach (EndUserPriceDTO price in lGridList)
                {
                    EndUserPriceDTO oldPrice = lEndUserPrice.Find(s => s.CatlogCode == price.CatlogCode);

                    if (oldPrice != null && oldPrice.Version != price.Version)
                    {
                        div_message.Attributes.Add("style", "display:block;padding:2px");
                        div_message.InnerHtml = new CommonUtility().GetErrorMessage("Data displayed is not up-to-date. Please refresh data and re-save.");
                        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadpage();", true);
                        return;
                    }
                }
                //}
            }
            DateTime effectiveDate = !string.IsNullOrEmpty(dpEffectiveDate.Text) ? DateTime.Parse(dpEffectiveDate.Text) : DateTime.Today;

            List<EndUserPriceDTO> listProducts = collection.CategoryList;
            //KeyValuePair<bool, string> saveState = new KeyValuePair<bool, string>();
            //if (HiddenField_Mode.Value == "edit")
            //    args.Status = lEndUserPrice[0].Version.ToString();
            //else
            //    args.Status = (lEndUserPrice[0].Version + 1).ToString();
            if (listProducts != null)
            {
                if (listProducts.Count != 0)
                {
                    args.Status = (listProducts[0].Version + 1).ToString();
                }
            }

            bool saveState = oBrEndUser.SavePriceList(args, effectiveDate, listProducts);

            if (saveState)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = new CommonUtility().GetSucessfullMessage("Successfully Saved !");
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadpage();", true);
            }
        }
        catch (Exception ex)
        {
        }
    }       

    private void SettingSession(string endusercode)
    {
        KeyValuePair<string, string> enduser_data = new KeyValuePair<string, string>(Server.UrlEncode(endusercode), "EndUser");

        //Setting session value.
        EndUserEntry = enduser_data;
    }
}