﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using System.Web.Script.Serialization;
using System.Text;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System.Data;
using System.Data.OleDb;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

public partial class loading_stocks_transaction_loading_stocks : PageBase
{
    CommonUtility commonUtility = new CommonUtility();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.onButtonFileUpload = new usercontrols_buttonbar.ButtonFileUpload(ButtonFileUpload_Click);

            buttonbar.VisibleSave(true);
            buttonbar.VisibleFileUpload(true);

            HiddenFieldOriginator.Value = UserSession.Instance.UserName;

            hfOriginator.Value = UserSession.Instance.UserName;
            hfOriginatorType.Value = UserSession.Instance.OriginatorString;

            if (!IsPostBack)
            {
                //if (UserSession.Instance.OriginatorString == "ADMIN")
                //{
                //    lstDistributorList = LoadDistributors("ALL");
                //}
                //else
                //{
                //    lstDistributorList = LoadDistributors(UserSession.Instance.UserName);
                //}

                LoadOptions();
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Stock Maintanance ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void LoadOptions()
    {
        #region Territory

        MasterClient masterlient = new MasterClient();
        ArgsModel args1 = new ArgsModel();
        args1.StartIndex = 1;
        args1.RowCount = 10000;
        args1.OrderBy = " area_id asc";

        List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        objTrrList = masterlient.GetAllTerritoryByOriginator(args1, UserSession.Instance.OriginatorString,
                                                                                UserSession.Instance.UserName);

        if (objTrrList.Count != 0)
        {
            //ddlTerritory.Items.Clear();
            //ddlTerritory.Items.Add(new ListItem("- Select Territory -", "0"));
            foreach (TerritoryModel item in objTrrList)
            {
                var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
                string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                //ddlTerritory.Items.Add(new ListItem(result + "  |  " + item.TerritoryName, item.TerritoryId.ToString()));
            }

            //ddlTerritory.Enabled = true;
        }

        #endregion
    }

    protected void ButtonFileUpload_Click(object sender)
    {
        ProductStockModel loadStockDTO = new ProductStockModel();
        StockClient stockClient = new StockClient();
        string errorDesc = "";

        try
        {
            if (Upload())
            {
                if (this.currFileExtension == ".xlsx" || this.currFileExtension == ".xls")
                {
                    loadStockDTO.AllocDate = Convert.ToDateTime(dtpUploadDate.Text);
                    loadStockDTO.CreatedBy = UserSession.Instance.UserName;
                    loadStockDTO.LastModifiedBy = UserSession.Instance.UserName;

                    DataTable dtStock = ReadExcelToTable(currFilePath, out errorDesc);  //Read Excel File (.XLS and .XLSX Format)
                    if (errorDesc != "Success")
                    {
                        div_message.Attributes.Add("style", "display:block;padding:2px");
                        div_message.InnerHtml = commonUtility.GetErrorMessage("File error: " + errorDesc);
                        return;
                    }

                    List<ProductModel> ListProduct = new List<ProductModel>();
                    foreach (DataRow row in dtStock.Rows)
                    {
                        ProductModel item = new ProductModel();
                        item.CustomerCode = row[0].ToString();
                        item.Code = row[1].ToString();
                        int qty = 0;
                        int.TryParse(row[2].ToString(), out qty);
                        item.Qty = qty;
                        ListProduct.Add(item);
                    }

                    bool isSave = stockClient.UploadStockToBulkTerritory(ListProduct, loadStockDTO);

                    if (isSave)
                    {
                        div_message.Attributes.Add("style", "display:block; padding:4px");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
                    }
                }
                else
                {
                    div_message.Attributes.Add("style", "display:block;padding:2px");
                    div_message.InnerHtml = commonUtility.GetErrorMessage("Please select stock upload file.");
                    return;
                }
            }
            else
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Cannot upload excel file.");
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        string jsonString = string.Empty;
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        ProductStockModel stockEntity = new ProductStockModel();
        StockClient stockClient = new StockClient();

        try
        {
            //if (ddlTerritory.SelectedValue.Trim() != "")
            ////if (txtDistPermanent.Text.Trim() != "")
            //{
            if (!string.IsNullOrEmpty(HiddenFieldStockGrid.Value.Trim()))
            {
                jsonString = @"{""LstLoadStockDetail"":" + HiddenFieldStockGrid.Value.Replace("\"ExtensionData\":{},", "") + "}";
                stockEntity = serializer.Deserialize<ProductStockModel>(jsonString);
                //stockEntity.LstLoadStockDetail = collection.LstLoadStockDetail;

                //loadStockDTO.RouteId = Convert.ToInt32(HiddenFieldRouteMasterID.Value);
                //loadStockDTO.RepCode = HiddenFieldRepCode.Value;
                //loadStockDTO.RouteAssignID = Convert.ToInt32(HiddenFieldAssignedRouteId.Value);
                //loadStockDTO.DistributorId = String.IsNullOrEmpty(HiddenFieldDistributorID.Value)?0:Convert.ToInt32(HiddenFieldDistributorID.Value);
                //loadStockDTO.DistributorId = String.IsNullOrEmpty(ddlTerritory.SelectedValue) ? 0 : Convert.ToInt32(ddlTerritory.SelectedValue);

                stockEntity.AllocDate = Convert.ToDateTime(dtpCloseDate.Text);
                stockEntity.StockAllocType = String.IsNullOrEmpty(ddlStockAllocateType.SelectedValue) ? "" : ddlStockAllocateType.SelectedValue;
                stockEntity.CreatedBy = UserSession.Instance.UserName;
                stockEntity.LastModifiedBy = UserSession.Instance.UserName;
                stockEntity.TerritoryId = Convert.ToInt32(hfTerritoryId.Value);

                //HiddenFieldDistributorID.Value = ddlTerritory.SelectedValue;
            }

            bool isSave = stockClient.SaveLoadStock(stockEntity);

            if (isSave)
            {
                div_message.Attributes.Add("style", "display:block;padding:4px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved!");
                HiddenFieldGridDataChanged.Value = "0";
            }
            else
            {
                div_message.Attributes.Add("style", "display:block;padding:4px");
                div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            }
            //}
            //else
            //{
            //    div_message.Attributes.Add("style", "display:block;padding:2px");
            //    div_message.InnerHtml = commonUtility.GetErrorMessage("Please select Distributor!");
            //    return;
            //}
        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    #region "Read Stock from Excel File"

    string currFilePath = string.Empty; //File Full Path
    string currFileExtension = string.Empty;  //File Extension

    private bool Upload()
    {
        bool ret = false;

        try
        {
            HttpPostedFile file = this.fileSelect.PostedFile;

            string fileName = file.FileName;
            if (!string.IsNullOrEmpty(fileName))
            {
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads\\stocks"; //ConfigurationManager.AppSettings["FolderPath"];

                fileName = System.IO.Path.GetFileName(fileName); //Get File Name (not including path)
                this.currFileExtension = System.IO.Path.GetExtension(fileName);   //Get File Extension
                this.currFilePath = FolderPath + "\\" + fileName; //Get File Path after Uploading and Record to Former Declared Global Variable
                file.SaveAs(this.currFilePath);  //Upload

                ret = true;
            }
        }
        catch { ret = false; }

        return ret;
    }

    private DataTable ReadExcelToTable(string path, out string errorDesc)
    {
        DataTable dt = new DataTable();

        try
        {
            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(path, false))
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                }

                foreach (Row row in rows) //this will also include your header row...
                {
                    DataRow tempRow = dt.NewRow();
                    try
                    {
                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            try
                            {
                                tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                            }
                            catch { }
                        }

                        if (tempRow[0].ToString() != "")
                            dt.Rows.Add(tempRow);
                    }
                    catch { }
                }
            }

            dt.Rows.RemoveAt(0);
            dt.AcceptChanges();

            errorDesc = "Success";
        }
        catch(Exception ex) {
            errorDesc = ex.Message;
            dt = new DataTable(); 
        }

        return dt;
    }

    public static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        string value = cell.CellValue.InnerXml;

        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }

    #endregion
}