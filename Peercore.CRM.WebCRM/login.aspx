﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>mSales v 2.0.1</title>

    <link href="assets/stylesheets/core_theme1.css" rel="stylesheet" type="text/css" />
    <link href="assets/stylesheets/custom-style.css" rel="stylesheet" />

    <script type="text/javascript" src="assets/scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="assets/scripts/common.js"></script>

</head>
<body class="login-page">

    <%--<div class="head">
        <div class="header-inner">
            <div class="logo">
                <img runat="server" src="~/assets/images/logo.png" alt="Tobacco Logo" />
            </div>
        </div>
    </div>--%>
    <!--header-->


    <div id="about" class="banner">
        <div class="banner-inner">

            <%-- <div class="about">
                <h1>
                    Introducing the all new application
                </h1>
                <p>
                    The new Web CRM can run on your PC, Laptop, iPad, Windows and Android tablets. Lead
                    generation, profiling the Customers, managing the end user data, formulate opportunities,
                    plan your call cycles, record your activities and schedule your calendar all within
                    the new CRM.
                </p>
                <div class="macair">
                    <img runat="server" src="~/assets/images/air.png" alt="" /></div>
                <div class="mini">
                    <img runat="server" src="~/assets/images/mini.png" alt="" /></div>
            </div>--%>

            <div class="login-logo">
                <img runat="server" src="~/assets/images/perfetti-logo.png" alt="" />
            </div>

            <div class="login">
                <form id="form1" runat="server">
                    <div runat="server" name="login-form" class="login-form" id="divloginform">
                        <h1 runat="server" class="top" id="headinglable">Log In to Your Account</h1>
                        <%--<div id="divError" align="center" style="width: 70%; padding-left: 20px;"></div>--%>
                        <div class="content" align="center">
                            <asp:Label ID="divError" runat="server" CssClass="error" Style="color: red;"></asp:Label>

                        </div>
                        <div runat="server" class="content" id="divLoginContent">
                            <asp:TextBox ID="username" runat="server" type="text" class="input username" placeholder="Enter username"></asp:TextBox>
                            <asp:TextBox ID="password" runat="server" type="password" class="input password" placeholder="Enter password"></asp:TextBox>
                        </div>
                        <div runat="server" class="login_footer">
                            <asp:Button ID="btnSubmit" runat="server" class="btn btn-default" Text="Login" OnClick="btnLogin_Click" />
                        </div>
                    </div>
                    <div runat="server" name="otpverify-form" class="login-form" id="divotpverifyform" visible="false">
                        <h1 runat="server" class="top" id="h1">Verify your OTP</h1>
                        <%--<div id="divError" align="center" style="width: 70%; padding-left: 20px;"></div>--%>
                        <div class="content" align="center">
                            <asp:Label ID="divOtpError" runat="server" CssClass="error" Style="color: red;"></asp:Label>

                            <div runat="server" class="content login_footer" id="divOtp" style="padding-top:0px !important">
                                <asp:TextBox ID="txtOtpNumber" runat="server" type="text" class="input username" placeholder="Enter OTP"></asp:TextBox>
                            </div>
                            <div runat="server" class="login_footer">
                                <asp:Button ID="btnResendOtp" runat="server" class="btn btn-default" Text="Resend OTP" OnClick="btnResendOtp_Click" />
                                <asp:Button ID="btnVerify" runat="server" class="btn btn-default" Text="Verify" OnClick="btnVerify_Click" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <!--banner-inner-->
    </div>
    <!--banner-->


    <div id="screenshots" class="screenshots"></div>

    <asp:Panel ID="div_loader" runat="server" class="loadingdiv" Visible="false">
        <div class="windows8">
            <div class="wBall" id="wBall_1">
                <div class="wInnerBall">
                </div>
            </div>
            <div class="wBall" id="wBall_2">
                <div class="wInnerBall">
                </div>
            </div>
            <div class="wBall" id="wBall_3">
                <div class="wInnerBall">
                </div>
            </div>
            <div class="wBall" id="wBall_4">
                <div class="wInnerBall">
                </div>
            </div>
            <div class="wBall" id="wBall_5">
                <div class="wInnerBall">
                </div>
            </div>
        </div>
    </asp:Panel>
    <%--<div class="loadingdiv" align="center" id="div_loader" runat="server">
        

        
    </div>--%>

    <%-- <footer class="footer">
        <div class="copyright">© Perfetti 2020</div> 
    </footer>--%>
</body>


<script type="text/javascript">
    document.getElementById('username').focus();

    //function signinsubmit() {
    //    var u = document.getElementById('username').value;
    //    var p = document.getElementById('password').value;

    //    UserLogin(u, p, 'divError');
    //}

    function divHide() {
        $("#divError").fadeOut("slow");
    }
</script>

</html>
