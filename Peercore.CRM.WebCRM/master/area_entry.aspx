﻿<%@ Page Title="mSales - Area" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="area_entry.aspx.cs" Inherits="common_templates_master_area_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <%--<script type="text/javascript" src=""></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="saveDeleteOption" runat="server" />

    <asp:HiddenField ID="hdnSelectedAreaId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedRegionId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedTerritoryId" runat="server" ClientIDMode="Static" />

    <asp:HiddenField ID="hdnSelectedAreaCode" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedAreaName" runat="server" ClientIDMode="Static" />

    <div id="div_main" class="divcontectmainforms">
        <div id="windowArea" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Area Code
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtAreaCode" maxlength="30" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Area Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtAreaName" maxlength="100" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Region
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList ID="ddlRegion"
                                            CssClass="input-large1" runat="server">
                                        </asp:DropDownList>
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSaveArea" type="button">Save</button>
                                    <button class="k-button" id="buttonClearArea">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="windowTerritory" style="display: none; width: 500px;">
            <div style="width: 100%" id="TerritoryEntry">
                <div id="div_text_area">
                    <div id="div1" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Territory Code
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtTerritoryCode" maxlength="30" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Territory Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtTerritoryName" maxlength="100" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Area
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList ID="ddlArea"
                                            CssClass="input-large1" runat="server">
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSaveTerritory" type="button">Save</button>
                                    <button class="k-button" id="buttonClearTerritory">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_area" clientidmode="Static" runat="server" href="#">Add Area</a>
                    </div>
                    <%--<div class="reloadlinkdiv" style="float: left" id="div_reload_1">
                        <a id="id_reload" clientidmode="Static" runat="server" href="#"></a>
                    </div>--%>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="gridAllAreas"></div>

                <script type="text/x-kendo-template" id="templateArea">
                    <div class="tabstrip">
                        <ul>
                            <li class="k-state-active">
                                Territory
                            </li>
                            <li>
                                ASM Info
                            </li>
                        </ul>
                        <div>
                            <div class="area-territory"></div>
                        </div>
                        <div>
                            <div class='area-asm'>
                                <%--<ul>
                                   <li><label>RSM Code:</label>#= RsmCode #</li>
                                    <li><label>RSM Name:</label>#= RsmName #</li>
                                   <li><label>User Name:</label>#= RsmUserName #</li>
                                    <li><label>Email:</label>#= RsmEmail #</li>
                                </ul>--%>
                            </div>
                        </div>
                    </div>
                </script>
            </div>
        </div>
    </div>

    <div id="popup">
        <div>
            <b>Enter Administrator Password:</b>
        </div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>


    <script type="text/javascript">

        var area_id;

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            LoadAllAreas();
            $("#txtareacode").focus();
        });

        //Load popup for Add new Brand
        $("#id_add_area").click(function () {
            $("#windowArea").css("display", "block");
            ClearControlsArea();

            var wnd = $("#windowArea").kendoWindow({
                title: "Add/Edit Area",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtName").focus();
            wnd.center().open();
        });

        //Load popup for Add new Brand
        $("#id_reload").click(function () {
            LoadAllAreas();
        });

        //#region Admin Confirmation

        function confirmAdmin() {
            var password = document.getElementById("adminPass").value;
            var param = { "password": password };
            if (password == "") {
                var errorMsg = GetErrorMessageDiv("Please enter admin password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                                delete_selected_area(area_id);
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                                AreaEntrySave();
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            $("#adminPass").val("");
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                        $("#adminPass").val("");
                    }

                },
                error: function (response) {
                }
            });
        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        //#endregion

        //Save button click
        $("#buttonSaveArea").click(function () {
            showPopupArea(0, 1);
        });

        function showPopupArea(aID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            area_id = aID;
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    delete_selected_area(aID);
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    SaveArea();
            }
        }

        $("#buttonClearArea").click(function () {
            ClearControls();
        });

        function ClearControlsArea() {
            $("#hdnSelectedAreaId").val('');
            $("#txtAreaCode").val('');
            $("#txtAreaName").val('');
            $("#hdnSelectedAreaId").val('');
            $("#hdnSelectedAreaCode").val('');
            $("#hdnSelectedAreaName").val('');
            $("#hdnSelectedTerritoryId").val('');
        }

        //Save button click
        $("#buttonSaveTerritory").click(function () {
            showPopupTerritory(0, 1);
        });

        function showPopupTerritory(aID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            area_id = aID;
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    DeleteTerritory(aID);
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    SaveAreaTerritory();
            }
        }

        $("#buttonClearTerritory").click(function () {
            ClearControlsTerritory();
        });

        function ClearControlsTerritory() {
            $("#hdnSelectedCompId").val('1');
            $("#hdnSelectedRegionId").val('');
            $("#hdnSelectedAreaId").val('');
            $("#txtAreaCode").val('');
            $("#txtAreaName").val('');
        }

        function closepopupArea() {
            var wnd = $("#windowArea").kendoWindow({
                title: "Add/Edit Area",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#windowArea").css("display", "none");
        }

        function closepopupTerritory() {
            var wnd = $("#windowTerritory").kendoWindow({
                title: "Add/Edit Territory",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#windowTerritory").css("display", "none");
        }

    </script>
</asp:Content>

