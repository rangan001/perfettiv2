﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="asm_entry.aspx.cs" Inherits="master_asm_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        .option-input {
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            appearance: none;
            position: relative;
            top: 6px;
            right: 0;
            bottom: 0;
            left: 0;
            height: 24px !important;
            width: 24px !important;
            transition: all 0.15s ease-out 0s;
            background: #cbd1d8;
            border: none;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            margin-right: 0.5rem;
            outline: none;
            position: relative;
            z-index: 1000;
        }

            .option-input:hover {
                background: #9faab7;
            }

            .option-input:checked {
                background: #40e0d0;
            }

                .option-input:checked::before {
                    height: 24px;
                    width: 24px;
                    position: absolute;
                    content: '✔';
                    display: inline-block;
                    font-size: 18px;
                    text-align: center;
                    line-height: 24px;
                }

                .option-input:checked::after {
                    -webkit-animation: click-wave 0.65s;
                    -moz-animation: click-wave 0.65s;
                    animation: click-wave 0.65s;
                    background: #40e0d0;
                    content: '';
                    display: block;
                    position: relative;
                    z-index: 100;
                }

        .k-grid td {
            padding-top: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="saveDeleteOption" runat="server" />

    <asp:HiddenField ID="hdnSelectedAsmId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedAreaId" runat="server" ClientIDMode="Static" />

    <div id="div_main" class="divcontectmainforms">
        <div id="windowASM" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Code
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtasmcode" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtasmname" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">User Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtasmusername" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Area
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList Height="20px" ID="ddlArea"
                                            CssClass="input-large1" runat="server">
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Mobile
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtasmtel1" /></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Email
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtasmemail" /></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_asm" clientidmode="Static" runat="server" href="#">Add ASM</a>
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back">

            </div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="gridAllASM"></div>
                <script type="text/x-kendo-template" id="templateASM">
                    <div class="tabstrip">
                        <ul>
                            <li class="k-state-active">
                                SR Info
                            </li>
                            <li>
                                Territories Info
                            </li>
                            <li>
                                IMEI Info
                            </li>
                            <li>
                                Change Password
                            </li>
                        </ul>
                        <div>
                            <div class="asm-sr"></div>
                        </div>
                        <div>
                            <div class="asm-territory"></div>
                        </div>
                        <div>
                            <table border="0">
                                <tbody>
                                    <tr>
                                        <td colspan="1">
                                            Is IMEI User : 
                                            <input type="checkbox" id="chkIsIMEIUser" 
                                                    style="margin:0px; width: 150px;" 
                                                    class="option-input"
                                                    #= IsNotIMEIuser ? '' : checked='checked' #
                                                    onclick = "clickIsImei(#=AsmId#)"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="asm-imei"></div>
                        </div>
                        <div>
                            <div class='asm-cp'>
                                <table border="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="1">New Password
                                            </td>
                                            <td colspan="4">
                                                <input type="password" class="k-input k-textbox" id="txtAsmNewPassword_#=AsmId#" data-bind="value:AsmPW" style="margin:0px; width: 150px;" />
                                                <span style="color: Red">*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1">Confirm Password
                                            </td>
                                            <td colspan="4">
                                                <input type="password" class="k-input k-textbox" id="txtAsmConfirmPassword_#=AsmId#" data-bind="value:AsmCPW" style="margin:0px; width: 150px;" />
                                                <span style="color: Red">*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1">
                                            </td>
                                            <td colspan="4" class="textalignbottom">
                                                <button class="k-button" 
                                                        id="buttonAsmUpdatePW"
                                                        type="button"
                                                        onclick = "UpdateAsmPassword(#=AsmId#)">Update</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </script>
            </div>
        </div>
        <div id="windowSR" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentrysr">
                <div id="div_textsr">
                    <div id="div1" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Code
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtsrcode" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtsrname" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr">
                                <td colspan="2" class="textalignbottom">User Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtsrusername" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Territory
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList Height="20px" ID="ddlTerritory"
                                            CssClass="input-large1" runat="server">
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr">
                                <td colspan="2" class="textalignbottom">Mobile
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtsrtel1" /></span>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td colspan="2" class="textalignbottom">Email
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtsremail" /></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSaveSR" type="button">Update</button>
                                    <%--<button class="k-button" id="buttonClearSR">Clear</button>--%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="popup">
        <div>
            <b>Enter Administrator Password:</b>
        </div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>

    <script type="text/javascript">

        var area_id;

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            LoadAllASM();
            //$("#txtrsmcode").focus();
        });

        //Load popup for Add new Brand
        $("#id_add_asm").click(function () {
            $("#windowASM").css("display", "block");
            ClearAsmControls();

            var wnd = $("#windowASM").kendoWindow({
                title: "Add/Edit Asm",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtasmname").focus();
            wnd.center().open();
        });

        //Save button click
        $("#buttonSave").click(function () {
            showPopup(0, 1);
        });

        //Update SR button click
        $("#buttonSaveSR").click(function () {
            showPopupSR(0, 1);
        });

        function showPopup(aID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            area_id = aID;
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    DeleteRSM(aID);
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    SaveAsm();
            }
        }

        function showPopupSR(aID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            area_id = aID;
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    DeleteTerritoryfromSR(aID);
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    SaveSR();
            }
        }

        function confirmAdmin() {
            var password = document.getElementById("adminPass").value;
            var param = { "password": password };
            if (password == "") {
                var errorMsg = GetErrorMessageDiv("Please enter admin password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                                DeleteASM(area_id);
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                                SaveAsm();
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            $("#adminPass").val("");
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                        $("#adminPass").val("");
                    }

                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });

        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        $("#buttonClear").click(function () {
            $("#hdnSelectedAreaId").val('');
            $("#txtName").val('');
        });

        function clickIsImei(asmId) {
            var checked = false;
            var chkPassport = document.getElementById("chkIsIMEIUser");
            if (chkPassport.checked) {
                checked = true;
            } else {
                checked = false;
            }

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/master/master.asmx/UpdateAsmIsIMEI",
                data: '{ asm_id: "' + asmId + '", ' +
                    ' is_imei: "' + checked + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                },
                error: function (response) {                   
                }
            });
        }

        function UpdateAsmPassword(asmId)
        {
            var valPW = $('#txtAsmNewPassword_' + asmId).val();
            var valCPW = $('#txtAsmConfirmPassword_' + asmId).val();

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/master/master.asmx/UpdateAsmPassword",
                data: '{ asm_id: "' + asmId + '", ' +
                    ' password: "' + valPW + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var sucessMsg = GetSuccesfullMessageDiv("Password successfully updated.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    LoadAllASM();
                    hideStatusDiv("MainContent_div_message");
                },
                error: function (response) {                   
                }
            });
        }

        function closepopup() {
            var wnd = $("#windowASM").kendoWindow({
                title: "Add/Edit Asm",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#windowASM").css("display", "none");
        }

        function ClearAsmControls() {
            $("#hdnSelectedAsmId").val('');
            $("#hdnSelectedAreaId").val('');
            $("#txtasmcode").val('');
            $("#txtasmname").val('');
            $("#txtasmusername").val('');
            $("#MainContent_ddlArea").val(0);
            $("#txtasmtel1").val('');
            $("#txtasmemail").val('');
        }

    </script>
</asp:Content>

