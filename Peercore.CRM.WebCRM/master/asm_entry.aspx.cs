﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class master_asm_entry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
            }

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("ASM Entry ", "#", "");
            LoadOptions();
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.OriginatorString;
        hfOriginator.Value = UserSession.Instance.UserName;
    }

    private void LoadOptions()
    {
        MasterClient masterlient = new MasterClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 10000;
        args.OrderBy = " area_id asc";

        List<AreaModel> objList = new List<AreaModel>();
        objList = masterlient.GetAllAreaNew(args);

        if (objList.Count != 0)
        {
            ddlArea.Items.Clear();
            ddlArea.Items.Add(new ListItem("- Select Area -", "0"));
            foreach (AreaModel item in objList)
            {
                var strLength = (item.AreaCode == null) ? 0 : item.AreaCode.Length;
                string result = (item.AreaCode == null) ? "" : item.AreaCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlArea.Items.Add(new ListItem(result + "  |  " + item.AreaName, item.AreaId.ToString()));
            }

            ddlArea.Enabled = true;
           
        }
        
        List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        objTrrList = masterlient.GetAllTerritoryByOriginator(args, UserSession.Instance.OriginatorString, 
                                                                                UserSession.Instance.UserName);

        if (objTrrList.Count != 0)
        {
            ddlTerritory.Items.Clear();
            ddlTerritory.Items.Add(new ListItem("- Select Territory -", "0"));
            foreach (TerritoryModel item in objTrrList)
            {
                var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
                string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlTerritory.Items.Add(new ListItem(result + "  |  " + item.TerritoryName, item.TerritoryId.ToString()));
            }

            ddlTerritory.Enabled = true;
           
        }
    }
}