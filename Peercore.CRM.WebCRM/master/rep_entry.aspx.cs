﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System.ServiceModel;

public partial class lead_customer_transaction_rep_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
        buttonbar.VisibleSave(true);
        buttonbar.EnableSave(true);

        buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
        buttonbar.VisibleDeactivate(true);
        buttonbar.EnableDeactivate(true);

        Session.Remove(CommonUtility.BREADCRUMB);

        Master.SetBreadCrumb("SR Entry ", "#", "");

        if (!IsPostBack)
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.VisibleSave(true);
            buttonbar.EnableSave(true);

            buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
            buttonbar.VisibleDeactivate(true);
            buttonbar.EnableDeactivate(true);

            HiddenFieldIsDistributorSaved.Value = "0";
            Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>();

            txtJoinedDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");

            LoadOptions();

            DropDownListGroup.SelectedIndex = 0;
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        SaveRepDetails();
    }

    protected void ClearPage()
    {
        HiddenFieldSelectedRepId.Value = "0";
        HiddenFieldSelectedOriginatorId.Value = "0";
        HiddenFieldSelectedOriginator.Value = "";

        txtName.Text = "";
        TxtUserName.Text = "";
        txtNIC.Text = "";
        TxtPassword.Text = "";
        TxtVerifyPassword.Text = "";
        TxtAddress1.Text = "";
        txtMobile.Text = "";
        TextBoxEmail.Text = "";

        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "EnableUserNameField();", true);
        HiddenFieldIsDistributorSaved.Value = "0";

        buttonbar.EnableSave(true);
        Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>();

        txtJoinedDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");

        DropDownListGroup.SelectedIndex = 0;
        ddlTerritory.SelectedIndex = 0;

        txtRepCode.Text = "";
        HiddenFieldSelectedDistributorUserName.Value = "";

        foreach (ListItem item in chklstHolidays.Items)
        {
            if (item.Selected)
            {
                item.Selected = false;
            }
        }
    }

    public void SaveRepDetails()
    {
        SRModel srEntity = new SRModel();
        MasterClient masterClient = new MasterClient();
        CommonUtility commonUtility = new CommonUtility();

        srEntity.RepId = Convert.ToInt32((HiddenFieldSelectedRepId.Value == "") ? "0" : HiddenFieldSelectedRepId.Value);
        srEntity.RepName = txtName.Text;
        srEntity.RepCode = txtRepCode.Text;
        srEntity.DeptString = "DR";
        srEntity.Originator = TxtUserName.Text;

        if (srEntity.RepName == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please enter rep name");
            return;
        }

        if (srEntity.RepCode == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please enter rep code");
            return;
        }

        if (srEntity.Originator == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please enter user name");
            return;
        }

        //if (srEntity.RepId == 0)
        //{
        if (TxtPassword.Text == "")
        {
            TxtPassword.Text = "pass";
            TxtVerifyPassword.Text = "pass";
            //div_message.Attributes.Add("style", "display:block");
            //div_message.InnerHtml = commonUtility.GetErrorMessage("Please enter a password");
            //return;
        }
        //}

        if (TxtPassword.Text != TxtVerifyPassword.Text)
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please confirm password");
            return;
        }

        //if (IsUserNameExists(distributor))
        //{
        //    div_message.Attributes.Add("style", "display:block");
        //    div_message.InnerHtml = commonUtility.GetErrorMessage("Username Exists !");
        //    return;
        //}

        if (ddlTerritory.SelectedValue == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please select a Territory");
            return;
        }

        string RepCode;
        if (IsTerritoryAssigned(ddlTerritory.SelectedValue, out RepCode))
        {
            //div_message.Attributes.Add("style", "display:block");
            ////div_message.InnerHtml = commonUtility.GetErrorMessage("Selected Territory Already Assigned. ");
            //div_message.InnerHtml = commonUtility.GetQuestionMessage("Selected Territory Already Assigned. ");

            string message = "Selected Territory was already assigned to Rep " + RepCode + ". Do you want to change it?";
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("return confirm('");
            sb.Append(message);
            sb.Append("');");
            ClientScript.RegisterOnSubmitStatement(this.GetType(), "alert", sb.ToString());
        }


        srEntity.Password = TxtPassword.Text;
        srEntity.Address1 = TxtAddress1.Text;
        srEntity.Mobile = txtMobile.Text;
        srEntity.Email = TextBoxEmail.Text;
        srEntity.NIC = txtNIC.Text;
        srEntity.Status = ddlRepStatus.SelectedValue;
        srEntity.IsNotEMEIUser = chkIsNotEmeiUser.Checked;
        srEntity.CreatedBy = UserSession.Instance.UserName;
        srEntity.LastModifiedBy = UserSession.Instance.UserName;
        srEntity.TerritoryId = Convert.ToInt32(ddlTerritory.SelectedValue);

        List<string> holidayList = new List<string>();
        string hday = "";
        foreach (ListItem item in chklstHolidays.Items)
        {
            if (item.Selected)
            {
                hday = item.Value;
                holidayList.Add(hday);
            }
        }

        srEntity.HolidaysList = holidayList;
        srEntity.GroupCode = DropDownListGroup.SelectedValue;
        srEntity.JoinedDate = Convert.ToDateTime(txtJoinedDate.Text);

        bool iNoRecs = false;
        iNoRecs = masterClient.SaveSR(srEntity);

        if (iNoRecs)
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
            HiddenFieldIsDistributorSaved.Value = "1";
            buttonbar.EnableSave(true);
            ClearPage();
        }
        else
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured.");
        }
    }

    protected void ButtonClear_Click(object sender)
    {
        HiddenFieldSelectedRepId.Value = "0";
        HiddenFieldSelectedOriginatorId.Value = "0";
        HiddenFieldSelectedOriginator.Value = "";

        txtName.Text = "";
        txtNIC.Text = "";
        TxtUserName.Text = "";
        TxtPassword.Text = "";
        TxtVerifyPassword.Text = "";
        TxtAddress1.Text = "";
        txtMobile.Text = "";
        TextBoxEmail.Text = "";

        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "EnableUserNameField();", true);
        HiddenFieldIsDistributorSaved.Value = "0";

        buttonbar.EnableSave(true);
        Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>();
        div_message.Attributes.Add("style", "display:none");

        txtJoinedDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");

        DropDownListGroup.SelectedIndex = 0;
        ddlTerritory.SelectedIndex = 0;

        txtRepCode.Text = "";
        HiddenFieldSelectedDistributorUserName.Value = "";
    }

    private void DisableTabs()
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "disabletabs();", true);
    }

    private void EnableTabs()
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "enabletabs();", true);
    }

    private bool IsUserNameExists(DistributorDTO distributorDTO)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        bool exists = false;
        try
        {
            OriginatorDTO ori = originatorClient.GetOriginator(distributorDTO.Originator);

            if (ori != null)
            {
                if (ori.OriginatorId != null)
                    if (ori.OriginatorId > 0)
                        exists = true;
            }
            else
            {
                exists = false;
            }

            return exists;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private bool IsRepCodeExists(DistributorDTO distributorDTO)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        DistributorClient distributorClient = new DistributorClient();
        bool exists = false;
        try
        {
            string ori = originatorClient.GetUserNameByRepCode(distributorDTO.Code);
            if (ori != null && !ori.Equals(""))
            {
                exists = true;
            }
            else
                exists = false;
            return exists;
        }
        catch (Exception)
        {
            return true;
        }
    }
    
    private bool IsTerritoryAssigned(string territoryId, out string RepCode)
    {
        MasterClient masterClient = new MasterClient();
        RepCode = "";

        bool exists = false;
        try
        {
            bool ori = masterClient.IsTerritoryAssigned(territoryId, out RepCode);
            if (ori)
            {
                exists = true;
            }
            else
                exists = false;
            return exists;
        }
        catch (Exception)
        {
            return true;
        }
    }

    private void LoadOptions()
    {
        #region Group

        CommonClient oLookupTable = new CommonClient();
        CommonUtility commonUtility = new CommonUtility();

        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.SStartDate = DateTime.Today.ToString(ConfigUtil.DateTimePatternForSql);
        args.SEndDate = DateTime.Today.AddDays(1).ToString(ConfigUtil.DateTimePatternForSql);

        List<LookupTableDTO> groupList = new List<LookupTableDTO>();
        try
        {
            oLookupTable = new CommonClient();
            groupList = oLookupTable.GetLookupTables("RGRP", args);
            LookupTableDTO select = new LookupTableDTO();
            select.TableCode = "0";
            select.TableDescription = "- Select Group -";
            groupList.Insert(0, select);
            DropDownListGroup.DataSource = groupList;
            DropDownListGroup.DataTextField = "TableDescription";
            DropDownListGroup.DataValueField = "TableCode";
            DropDownListGroup.DataBind();

            foreach (LookupTableDTO item in groupList)
            {
                if (item.DefaultValue == "Y")
                {
                    DropDownListGroup.SelectedValue = item.TableCode; break;
                }
            }
        }
        catch (Exception x)
        {
            div_info.Attributes.Add("style", "display:block");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
        finally
        {
            if (oLookupTable.State != CommunicationState.Closed)
                oLookupTable.Close();
        }


        #endregion

        #region Territory

        MasterClient masterlient = new MasterClient();
        ArgsModel args1 = new ArgsModel();
        args1.StartIndex = 1;
        args1.RowCount = 10000;
        args1.OrderBy = " territory_name asc";

        List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        objTrrList = masterlient.GetAllTerritoryByOriginator(args1, UserSession.Instance.OriginatorString,
                                                                                UserSession.Instance.UserName);

        if (objTrrList.Count != 0)
        {
            ddlTerritory.Items.Clear();
            ddlTerritory.Items.Add(new ListItem("- Select Territory -", "0"));
            foreach (TerritoryModel item in objTrrList)
            {
                var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
                string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlTerritory.Items.Add(new ListItem(item.TerritoryName + "  |  " + result, item.TerritoryId.ToString()));
            }

            ddlTerritory.Enabled = true;
        }

        #endregion
    }
}