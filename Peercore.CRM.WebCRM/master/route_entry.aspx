﻿<%@ Page Title="mSales - Route Entry" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="route_entry.aspx.cs" Inherits="call_cycle_transaction_route_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/buttonbar_navigation.ascx" TagPrefix="ucl2" TagName="buttonbarNavi" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />
    <asp:HiddenField ID="saveDeleteOption" runat="server" />

    <asp:HiddenField ID="hdnSelectedTerritoryId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedRouteId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedRouteCode" runat="server" ClientIDMode="Static" />

    <div id="repmodalWindow" style="display: none">
        <div id="div_repconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <div id="confirmmodalWindow" style="display: none">
        <div id="div_Confirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yesConfirm" class="k-button">
            Yes</button>
        <button id="noConfirm" class="k-button">
            No</button>
    </div>

    <div id="div_main" class="divcontectmainforms">
        <div id="windowRoute" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr style="display:none;">
                                <td class="textalignbottom">Route Code
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtRouteCode" maxlength="12" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Route Name
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtRouteName" maxlength="50"  />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Territory
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="ddlTerritory"
                                        CssClass="input-large1" runat="server">
                                    </asp:DropDownList>
                                    <span style="color: Red">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">
                                        Save</button>
                                    <button class="k-button" id="buttonClear">
                                        Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="windowOutlet" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry1">
                <div id="div_text">
                    <div id="div1" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">Outlet Code
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtOutletCode" disabled="disabled" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Outlet Name
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtOutletName" disabled="disabled" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">Route
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList Height= "16px" ID="ddlRoute"
                                        CssClass="input-large1" runat="server">
                                    </asp:DropDownList>
                                    <span style="color: Red">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSaveOutlet" type="button">
                                        Save</button>
                                    <button class="k-button" id="buttonClearOutlet">
                                        Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="toolbar_container" id="id_div_toolbar_container">
            <div class="toolbar_left1" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_route" clientidmode="Static" runat="server" href="#">Add Route</a>
                    </div>
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                    <div class="clearall">
                    </div>
                </div>
            </div>
            <div class="toolbar_right2" id="div3">
                <div class="leadentry_title_bar">
                    <ucl2:buttonbarNavi ID="buttonbarNavi" runat="server" />
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div id="div_routes" runat="server">
            <%--div_routes_rep--%>
            <div style="min-width: 200px; overflow: auto">
                <div style="float: left; width: 98.5%; overflow: auto; margin: 0px 10px;">
                    <div id="gridAllRoutes">
                    </div>
                    <script type="text/x-kendo-template" id="templateRoute">
                        <div class="tabstrip">
                            <ul>
                                <li class="k-state-active">
                                    Outlets
                                </li>
                            </ul>
                            <div>
                                <div class="route-outlet"></div>
                            </div>
                        </div>
                    </script>
                </div>
            </div>
        </div>
    </div>

    <div id="popup">
        <div>
            <b>Enter Administrator Password:</b>
        </div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>

    <script type="text/javascript">

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            LoadAllRoutes();

            $("#txtRouteCode").focus();
        });

        //Load popup for Add new Brand
        $("#id_add_route").click(function () {
            $("#windowRoute").css("display", "block");
            ClearControlsRoute();

            var wnd = $("#windowRoute").kendoWindow({
                title: "Add/Edit Route",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtRouteCode").focus();
            wnd.center().open();
        });

        //#region Admin Confirmation

        function confirmAdmin() {
            var password = document.getElementById("adminPass").value;
            var param = { "password": password };
            if (password == "") {
                var errorMsg = GetErrorMessageDiv("Please enter admin password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                                DeleteRoute(route_id);
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                                SaveRoute();
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            $("#adminPass").val("");
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                        $("#adminPass").val("");
                    }
                },
                error: function (response) {
                }
            });
        }

        function closepopupRoute() {
            var wnd = $("#windowRoute").kendoWindow({
                title: "Add/Edit Route",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#windowRoute").css("display", "none");
        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        //#endregion

        //Save button click
        $("#buttonSave").click(function () {
            debugger;
            showPopupArea(0, 1);
        });

        function showPopupArea(aID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            route_id = aID;

            $("#adminPass").val("");
            var OriginatorString = $("#<%= hfOriginatorType.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    DeleteRoute(route_id);
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    SaveRoute();
            }
        }

        $("#buttonClearArea").click(function () {
            ClearControls();
        });

        function ClearControlsRoute() {
            $("#hdnSelectedRouteId").val('');
            $("#txtRouteCode").val('1');
            $("#txtRouteName").val('');
            $("#hdnSelectedTerritoryId").val('');
            $("#MainContent_ddlTerritory").val('-1');
        }

		//Save button click
		$("#buttonSaveOutlet").click(function () {
			UpdateOutletInRouteMaster();
        });

		

	</script>
</asp:Content>
