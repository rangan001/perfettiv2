﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="rsm_entry.aspx.cs" Inherits="common_templates_master_rsm_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfRSMOriginator" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="saveDeleteOption" runat="server" />

    <asp:HiddenField ID="hdnSelectedRsmId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedRegionId" runat="server" ClientIDMode="Static" />

    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Code
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtrsmcode" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtrsmname" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">User Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtrsmusername" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <%--<tr>
                                <td colspan="2" class="textalignbottom">Password
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="password" class="textboxwidth" id="txtrsmpassword" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Confirm Password
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="password" class="textboxwidth" id="txtrsmconfirmpassword" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>--%>
                            <tr>
                                <td colspan="2" class="textalignbottom">Region
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList ID="ddlRegion"
                                            CssClass="input-large1" runat="server">
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Mobile
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtrsmtel1" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Email
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtrsmemail" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_rsm" clientidmode="Static" runat="server" href="#">Add RSM</a>
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back">

            </div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="gridAllRSM"></div>
                <script type="text/x-kendo-template" id="templateRSM">
                    <div class="tabstrip">
                        <ul>
                            <li class="k-state-active">
                                Area Info
                            </li>
                            <li>
                                Change Password
                            </li>
                        </ul>
                        <div>
                            <div class="rsm-area"></div>
                        </div>
                        <div>
                            <div class='rsm-cp'>
                                <table border="0">
                                    <tbody>
                                        <tr>
                                            <td colspan="1">New Password
                                            </td>
                                            <td colspan="4">
                                                <input type="text" id="txtRsmNewPassword" style="margin:0px; width: 150px;" />
                                                <span style="color: Red">*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1">Confirm Password
                                            </td>
                                            <td colspan="4">
                                                <input type="text" id="txtRsmConfirmPassword" style="margin:0px; width: 150px;" />
                                                <span style="color: Red">*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1">
                                            </td>
                                            <td colspan="4" class="textalignbottom">
                                                <button class="k-button" id="buttonRsmUpdatePW" type="button">Update</button>
                                                <button class="k-button" id="buttonRsmCancelPW">Cancel</button>
                                            </td>
                                        </tr>
                                    </tbody>
                            </div>
                        </div>
                    </div>
                </script>
            </div>
        </div>
    </div>

    <div id="popup">
        <div>
            <b>Enter Administrator Password:</b>
        </div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>


    <script type="text/javascript">

        var area_id;

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            LoadAllRSM();
            $("#txtrsmcode").focus();
        });

        //Load popup for Add new Brand
        $("#id_add_rsm").click(function () {
            $("#window").css("display", "block");
            $("#hdnSelectedRsmId").val('');
            $("#hdnSelectedRegionId").val('');
            $("#txtrsmname").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit RSM",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtrsmname").focus();
            wnd.center().open();
        });

        //Save button click
        $("#buttonSave").click(function () {
            showPopup(0, 1);
        });

        function showPopup(aID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            area_id = aID;
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    DeleteRSM(aID);
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    SaveRSM();
            }
        }

        function confirmAdmin() {
            var password = document.getElementById("adminPass").value;
            var param = { "password": password };
            if (password == "") {
                var errorMsg = GetErrorMessageDiv("Please enter admin password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                                delete_selected_area(area_id);
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                                SaveRSM();
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            $("#adminPass").val("");
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                        $("#adminPass").val("");
                    }

                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });

        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        $("#buttonClear").click(function () {
            $("#hdnSelectedAreaId").val('');
            $("#txtName").val('');
        });

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit RSM",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

    </script>
</asp:Content>

