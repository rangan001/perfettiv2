﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.CRM.Common;
using System.Text;

public partial class pipelines_stage_transaction_opportunity : PageBase
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
            {
                if (Request.QueryString["pipid"] != null)
                {
                    HiddenFieldPipelineStageId.Value = Request.QueryString["pipid"];
                }
                else
                {
                    HiddenFieldPipelineStageId.Value = "0";
                }
                string header = string.Empty;
                if (Request.QueryString["pipheader"] != null)
                {
                   // div_header.InnerText = "OPPORTUNITIES - " + Request.QueryString["pipheader"].ToUpper();
                    header = "Opportunities - " + Request.QueryString["pipheader"].ToUpper();
                }
                else
                {
                    header = "Opportunities";
                   // div_header.InnerText = "OPPORTUNITIES";
                }


                if (BackButtonForAll != null)
                {
                    if (Request.Url != null && Request.UrlReferrer != null && Request.Url.AbsolutePath != Request.UrlReferrer.AbsolutePath)
                    {
                        if ((Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty && Request.QueryString["fm"] == "lead") ||
                             Request.UrlReferrer.AbsolutePath.Contains("search.aspx"))
                        {
                            List<string> back = (List<string>)BackButtonForAll;
                            BackButtonForAll = null;
                            if (back.Count == 1)
                            {
                                hfPageIndex.Value = (int.Parse(back[0]) + 1).ToString();
                            }
                        }
                    }
                }

                //Remove the session before setting the breadcrumb.
                Session.Remove(CommonUtility.BREADCRUMB);
                Master.SetBreadCrumb(header, ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunity.aspx", "");
                OpportunityAnalysis();

                
                if (Request.QueryString["cht"] != null)
                {
                    if (Request.QueryString["cht"] != string.Empty)
                    {
                        //Master.SetCommonHeader(Request.QueryString["cht"].ToString());
                    }
                }
                else
                {
                    //Master.SetCommonHeader("Pipeline & Opportunities");
                }


            }
            else
            {
                Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
            }
        }
    }
    #endregion

    public void OpportunityAnalysis()
    {
        CommonClient common = new CommonClient();

        pipelines_stage_service pipelinesStageService = new pipelines_stage_service();

        DataSourceResult dataSourceResult = pipelinesStageService.GetOppDataAndCount(HiddenFieldPipelineStageId.Value, "1", "", 1, 0, null, null, hfPageIndex.Value);
        List<ClientOpportunityDTO> lstClientOpportunity = (List<ClientOpportunityDTO>)dataSourceResult.Data;
       string reportDate = SetSeriesOpportunityData(lstClientOpportunity);
        StringBuilder sb = new StringBuilder();

        sb.Append("<div>");
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("$(function () {");
        sb.Append("    var chart;");
        sb.Append("    $(document).ready(function () {");
        sb.Append("        chart = new Highcharts.Chart({");
        sb.Append("            chart: {");
        sb.Append("                renderTo: 'MainContent_container',");
        sb.Append("                plotBackgroundColor: null,");
        sb.Append("                plotBorderWidth: null,");
        sb.Append("                plotShadow: false");
        sb.Append("            },");
        sb.Append("            title: {");
        sb.Append("                text: 'Opportunity Analysis'");
        sb.Append("            },");
        sb.Append("            tooltip: {");
        sb.Append("                formatter: function () {");
        sb.Append("                    return '<b>' + this.point.rowcount + ','+ this.point.y +'(' + Math.round(this.percentage*100)/100  + ' %)</b>';");
        sb.Append("                }");
        sb.Append("            },");

        sb.Append("            plotOptions: {");
        sb.Append("                pie: {");
        sb.Append("                    allowPointSelect: true,");
        sb.Append("                    cursor: 'pointer',");
        sb.Append("                    point: {");
        sb.Append("                        events: {");
        sb.Append("                            click: function (e) {");
        sb.Append("                                 loadOpportunityGrid($('#MainContent_HiddenFieldPipelineStageId').val(),this.name); ");
        sb.Append("                            }");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    dataLabels: {");
        sb.Append("                        enabled: true,");
        sb.Append("                        color: '#000000',");
        //sb.Append("                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',");
        sb.Append("                        connectorColor: '#000000',");
        sb.Append("                        formatter: function () {");
        sb.Append("                           if(this.point.des == ''){");
        sb.Append("                             return '<b>' + this.point.rowcount + ','+ this.point.y +'</b>';");
        sb.Append("                           }else {");
        sb.Append("                             return '<b>' + this.point.des + '</b>';");
        sb.Append("                           }");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    showInLegend: true");
        sb.Append("                }");
        sb.Append("            },");
        sb.Append("            series: [{");
        sb.Append("                type: 'pie',");
        sb.Append("                name: 'state',");
        sb.Append("                rowcount:'rowcount',");
        sb.Append("                des:'des',");
        sb.Append("                data: [");
        sb.Append(reportDate);
        sb.Append("        ]");
        sb.Append("            }]");
        sb.Append("        });");
        sb.Append("    });");

        sb.Append("});");
        sb.Append("</script>");
        sb.Append("</div>");

        container.InnerHtml = sb.ToString();
    }

    public string SetSeriesOpportunityData(List<ClientOpportunityDTO> leadCustomerList)
    {
        string series = "";
        int count = 0;
        foreach (ClientOpportunityDTO lead in leadCustomerList)
        {
            count++;
            if (string.IsNullOrEmpty(series))
            {
                series = "{ name: '" + lead.OpportunityOwner + "', y: " + lead.Count + ", rowcount: '" + count + "',des: '" + lead.OpportunityOwner + "',color : '" + GetColor(lead.OpportunityOwner) + "' }";
            }
            else
            {
                series += ",{ name: '" + lead.OpportunityOwner + "', y: " + lead.Count + ", rowcount: '" + count + "',des: '" + lead.OpportunityOwner + "',color : '" + GetColor(lead.OpportunityOwner) + "' }";
            }
        }
        return series;
    }

    public string GetColor(string name)
    {
        string color = "";
        switch (name)
        {
            case "Customer" :
                color = "#e38928";
                break;
            case "Lead":
                color = "#2e5b94";
                break;
            case "End User":
                color = "#892a27";
                break;
        }
        return color;
    }
}