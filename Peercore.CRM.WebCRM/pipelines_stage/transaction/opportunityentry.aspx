﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="opportunityentry.aspx.cs" Inherits="pipelines_stage_transaction_opportunityentry" %>
    <%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
        
    </telerik:RadScriptManager>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall"></div>
         
        <div class="clearall"></div>
        <div id="div_message" runat="server" style="display:none"></div>
        <%--<div class="clearall">&nbsp;</div>--%>
        <div id="div_info" runat="server" style="display:none"></div>
        <div>
        <div class="formleft">
        <div class="formtextdiv_extended">Opportunity</div>
        <div class="formdetaildiv_right">
        
        <span class="wosub mand">
        <asp:TextBox ID="txtName" runat="server" CssClass="input-large"></asp:TextBox>
        <asp:Label ID="tbContact" runat="server" Text="*" Font-Bold="True" Font-Size="Large" ForeColor="#FF3300"></asp:Label>
        </span> 
        
       
        </div>
        <div class="clearall"></div>
        <div class="formtextdiv_extended">Organisation</div>
        <div class="formdetaildiv_right">                    
        <asp:TextBox ID="txtContact" runat="server" ReadOnly="true" CssClass="input-large"></asp:TextBox>
                    <%--<input type="text" id="txtContact" name="txtContact" runat="server" readonly="readonly"/>--%>
                   <%-- <asp:Button ID="Button1" runat="server" Text="?" />--%>
                   <a href="#" id="id_Contact"><img id="imgContact" src="~/assets/images/popiconbw.png" alt="popicon" runat="server" border="0" /></a>
       </div>
        <div class="clearall"></div>

        <div class="formtextdiv_extended">Customer</div>
         <div class="formdetaildiv_right">                    
         <asp:TextBox ID="txtCustomer" runat="server" ReadOnly="true" CssClass="input-large"></asp:TextBox>
         <a href="#" id="id_Customer"><img id="imgCustomer" src="~/assets/images/popiconbw.png" alt="popicon" runat="server" border="0" /></a></div>
        <div class="clearall"></div>

        <div class="formtextdiv_extended">End User</div>
        <div class="formdetaildiv_right">
        <asp:TextBox ID="txtEndUser" runat="server" ReadOnly="true" CssClass="input-large"></asp:TextBox>
        <a href="#" id="A1"><img id="img2" src="~/assets/images/popiconbw.png" alt="popicon" runat="server" border="0" /></a>
        </div>
        <div class="clearall"></div>

        <div class="formtextdiv_extended">Assigned To</div>
        <div class="formdetaildiv_right">
        <asp:TextBox ID="txtAssignto" runat="server" CssClass="input-large"></asp:TextBox>
                    <a href="#" id="id_assignedto"><img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                    <%--<asp:TextBox ID="txtRepGroup" runat="server" Width="100px"></asp:TextBox> 
                    <a href="#" id="id_repgroup" ><img id="img2" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>--%>
                    <%--<a href="#" id="A3" onclick="Repgroup()" >BDM Group</a>--%>
        </div>
        <div class="clearall"></div>

        <div class="formtextdiv_extended">Description</div>
        <div class="formdetaildiv_right"><asp:TextBox ID="txtDescription" runat="server" MaxLength="500" Height="55px"  CssClass="input-large" onbeforepaste="doBeforePaste(this);" onkeypress="doKeypress(this);" onpaste="doPaste(this);" TextMode="MultiLine" onfocus="ClearText('MainContent_txtDescription');"></asp:TextBox>
        </div>
         <div class="clearall"></div>
<div>
                       <asp:Label ID="tbCreated" runat="server" Text="" CssClass="specialevents"></asp:Label>
                    <br /> 
                    <asp:Label ID="tbModified" runat="server" Text="" CssClass="specialevents"></asp:Label>  
</div>

    </div>
     <div class="formright">
     <div class="formtextdiv_extended">Close Date</div>
     <div class="formdetaildiv_right">                    
     <asp:TextBox ID="dtpCloseDate" runat="server"></asp:TextBox>
                    <%--<telerik:RadDatePicker ID="dtpCloseDate" runat="server" Height="20px">
                    </telerik:RadDatePicker>--%>
                    <%-- <telerik:RadDateTimePicker ID="closedateTimePicker" runat="server">
                    </telerik:RadDateTimePicker>--%>
     </div>
         <div class="clearall"></div>

     <div class="formtextdiv_extended">Stage</div>
     <div class="formdetaildiv_right">                    
     <asp:DropDownList ID="cmbStage" runat="server" CssClass="input-large">
                    </asp:DropDownList>
                    <%--<telerik:RadComboBox ID="cmbStage" runat="server">
                    </telerik:RadComboBox>--%>
     </div> 
     <div class="clearall"></div>

     <div class="formtextdiv_extended">Probability</div>
     <div class="formdetaildiv_right">
      <telerik:RadSlider runat="server" ID="radSlider" CssClass="input-large"
                        Height="40px" TrackPosition="TopLeft" ItemType="Item" 
                        EnableServerSideRendering="True" DbValue="0">
                        <Items>
                            <telerik:RadSliderItem Text="0" Value="0" ToolTip="0"
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="10" Value="10" ToolTip="10" 
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="20" Value="20" ToolTip="20" 
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="30" Value="30" ToolTip="30" 
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="40" Value="40" ToolTip="40"
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="50" Value="50" ToolTip="50" 
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="60" Value="60" ToolTip="60"
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="70" Value="70" ToolTip="70"
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="80" Value="80" ToolTip="80"
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="90" Value="90" ToolTip="90"
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                            <telerik:RadSliderItem Text="100" Value="100" ToolTip="100" 
                                runat="server" Height="26px" Width="28px"></telerik:RadSliderItem>
                        </Items>
                    </telerik:RadSlider>
     </div>
     <div class="clearall"></div>

     <div class="formtextdiv_extended"><asp:Label ID="lblAmount" runat="server" Text="Amount $"></asp:Label></div>
     <div class="formdetaildiv_right">
        <span class="wosub mand">
            <asp:TextBox ID="mtxtAmount" runat="server" Text="0" CssClass="input-large-validate-numbers" style="text-align:right"></asp:TextBox>
            <asp:Label ID="txtbkAmount" runat="server" Text="*" Font-Bold="True" Font-Size="Large" ForeColor="#FF3300"></asp:Label>
        </span>
     </div>

     <div class="clearall"></div>
     <div class="formtextdiv_extended"> <asp:Label ID="lblUnitsAbbriviation" runat="server" Text="Units" ></asp:Label></div>
     <div class="formdetaildiv_right">                    
         <span class="wosub mand">
            <asp:TextBox ID="mtxtUnits" runat="server" Text="0" CssClass="input-large-validate-numbers" style="text-align:right"></asp:TextBox>
            <asp:Label ID="txtbkUnits" runat="server" Text="*" Font-Bold="True" Font-Size="Large" ForeColor="#FF3300"></asp:Label>
         </span>
     </div>
     <div class="clearall"></div>
     <div class="formtextdiv_extended">Tonnes </div>
     <div class="formdetaildiv_right"><asp:TextBox ID="txtTonnes" runat="server" CssClass="input-large-validate-numbers" Text="0" style="text-align:right"></asp:TextBox></div>


     </div></div>
     <div class="clearall"></div>
     <div style="margin-bottom:20px;">
<asp:Label ID="lblProduct" runat="server" Text="PRODUCTS" CssClass="specialevents"></asp:Label>
<asp:Label ID="Label1" runat="server" Text="*" Font-Bold="True" Font-Size="Large" ForeColor="#FF3300"></asp:Label>
<div class="addnewdiv" style="float:right; margin-right:10px;"><a id="id_addproduct" href="#" >ADD PRODUCT</a> </div>
</div>

<div style="margin:10px;">
                <a name="grid"></a>
                    <div id="loadProductGrid"></div>
</div>


        </div>
        <div class="clearall"></div>
        <br />
        <br />
        <asp:HiddenField runat="server" ID="AmmountHiddenField" />
        <asp:HiddenField runat="server" ID="UnitHiddenField"/>
        <asp:HiddenField runat="server" ID="txtOpportunityID" />
        <asp:HiddenField runat="server" ID="RepGroupID"/>

        <asp:HiddenField runat="server" ID="HiddenFieldFlag"/>

        <asp:HiddenField ID="HiddenFieldCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldDesc" runat="server" />
    <asp:HiddenField ID="HiddenFieldPrice" runat="server" />
    <asp:HiddenField ID="HiddenFieldConversion" runat="server" />
            <asp:HiddenField ID="HiddenFieldRepType" runat="server" />
    <%--</div>--%>
    <div id="modalWindow" style="display:none;height:450px;">
    <div id="div_confirm_message" ></div>
    <%--<button id="buttonOk" class="k-button">Ok</button>--%>
</div>
    <script type="text/javascript">

        jQuery(function () {
            jQuery("#MainContent_txtName").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter Opportunity"
            });
        });

        jQuery(function () {
            
            jQuery("#MainContent_mtxtAmount").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter Amount"
            });
        });
        jQuery(function ($) {
            $('input.input-large-validate-numbers').autoNumeric();
            $('input.input-large-validate-numbers').autoNumeric({ aSep: ',', aDec: '.' });
        });

        hideStatusDiv("MainContent_div_message");
        $("#id_repgroup").click(function () {
            repgroup('pipelines_stage/process_forms/processmaster.aspx?fm=oppportunityentry&type=query&querytype=repgroup');
        });

        $("#buttonOpenContactEntry").click(function () {
            window.location = "../../lead_customer/transaction/leadentry.aspx";
        });


        $("#id_addproduct").click(function () {
            if ($("#MainContent_HiddenFieldCode").val() != '') {
                $("#MainContent_div_message").html(GetErrorMessage("A product is already added. Delete the existing product to add a new product."));
                document.getElementById('MainContent_div_message').style.display = "block";
            } else {
                //catalog('pipelines_stage/process_forms/processmaster.aspx?fm=oppportunityentry&type=query&querytype=product');
                var wnd = $("#modalWindow").kendoWindow({
                    title: "Lookup",
                    modal: true,
                    visible: false,
                    resizable: false,
                    width: 550
                }).data("kendoWindow");

                LookupAllCatalog("div_confirm_message", $("#MainContent_HiddenFieldRepType").val());

                loadProductGrid();
            }
        });



        $("#MainContent_txtDescription").blur(function () {
            var text = $("#MainContent_txtDescription").val();
            if (text == "" || text == null || text == undefined) {
                $("#MainContent_txtDescription").val('(Max 500 Characters)');
            } else if (text.indexOf('(Max 500 Characters)') == 0) {
                $("#MainContent_txtDescription").val();
            } else {
                $("#MainContent_txtDescription").val(text);
            }
        });

        $(document).ready(function () {
            opportunityentrypageLoad();

            $("#MainContent_dtpCloseDate").kendoDatePicker({
                // display month and year in the input
                format: "dd-MMM-yyyy"
            });

        });

        $("#MainContent_mtxtUnits").blur(function () {
            SetTonnesAndAmount();
        });

        function SetTonnesAndAmount() {
            
            var units = $("#MainContent_mtxtUnits").val();
            var code = $("#MainContent_HiddenFieldCode").val();
            var reptype = $("#MainContent_HiddenFieldRepType").val();
            var Price = $("#MainContent_HiddenFieldPrice").val();
            var Conversion = $("#MainContent_HiddenFieldConversion").val();

            if (units != '' && code != '' && reptype == 'B') {

                $("#MainContent_txtTonnes").val((units * Conversion).toFixed(2));
                //alert((units * Conversion).toFixed(2));
                $("#MainContent_mtxtAmount").val(units * Price);
            }
        }
    </script>
    <script type="text/javascript">

        
    </script>
</asp:Content>
