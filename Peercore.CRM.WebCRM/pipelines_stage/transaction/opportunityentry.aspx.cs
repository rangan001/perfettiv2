﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Shared;
//using Peercore.CRM.Entities;
using Peercore.CRM.Common;
using CRMServiceReference;
using Telerik.Web.UI;
using System.Text;

public partial class pipelines_stage_transaction_opportunityentry : System.Web.UI.Page
{
    #region Constant
    private const string OPPORTUNITY_ID = "OPPORTUNITY_ID";
    private const string CATALOGLIST = "CATALOGLIST";
    #endregion Constant

    CommonUtility commonUtility = new CommonUtility();

    #region Methods

    private void GetCustomerDetails()
    {
        CustomerClient customerClient = new CustomerClient();

        try
        {
            CustomerDTO customer = customerClient.GetCustomer(ViewState["custid"].ToString());

            if (customer != null)
            {
                txtCustomer.Text = customer.Name;
            }
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void GetEndUserDetails() {
        EndUserClient oEndUser = new EndUserClient();
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.EnduserCode = ViewState["eduid"].ToString();
            args.CustomerCode = ViewState["custid"].ToString();
            EndUserDTO enduser = oEndUser.GetEndUser(args);

            if (enduser != null)
            {

                txtEndUser.Text = enduser.Name;
                txtCustomer.Text = enduser.CustomerName;
            }
        }
        catch (Exception oException)
        {
        }
    }

    private void LoadOptions()
    {
        PipelineStageClient pipelineStageClient = new PipelineStageClient();
        ArgsDTO args = new ArgsDTO();
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        try
        {
            List<PipelineStageDTO> pipelineStageList = pipelineStageClient.GetPipelineStage(args);
            cmbStage.DataSource = pipelineStageList;
            cmbStage.DataTextField = "PipelineStageName";
            cmbStage.DataValueField = "PipelineStageID";
            cmbStage.DataBind();
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void GetLeadDetails()
    {
        LeadClient leadClient = new LeadClient();
        try
        {
            if (int.Parse(ViewState["leadid"].ToString()) != 0)
            {
                LeadDTO lead = leadClient.GetLead(ViewState["leadid"].ToString());

                if (lead != null)
                {
                    txtContact.Text = lead.LeadName;
                }
            }
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    public void OpenOpportunity()
    {
        OpportunityClient opportunityClient = new OpportunityClient();
        try
        {
            CustomerOpportunitiesDTO opportunities = opportunityClient.GetOpportunity(Convert.ToInt32(txtOpportunityID.Value));

            if (opportunities != null)
            {
                txtOpportunityID.Value = opportunities.OpportunityID.ToString();
                //txtLeadID.Text = opportunities.LeadID.ToString();
                ViewState["leadid"] = opportunities.LeadID;

                txtAssignto.Text = opportunities.Originator;
                txtName.Text = opportunities.Name;
                dtpCloseDate.Text = opportunities.CloseDate.ToString("dd-MMM-yyyy");
                cmbStage.SelectedValue = opportunities.Stage.ToString();
                radSlider.Value = Decimal.Parse((opportunities.Probability / 10).ToString());
                mtxtAmount.Text = opportunities.Amount.ToString();
                txtContact.Text = opportunities.LeadName;
                txtDescription.Text = opportunities.Description;
                //txtCustCode.Text = opportunities.CustCode;
                mtxtUnits.Text = String.Format("{0:0.00}", opportunities.Units) ;
                if (ViewState["custid"] != null && !string.IsNullOrEmpty(ViewState["custid"].ToString()))
                    GetCustomerDetails();
                RepGroupID.Value = opportunities.RepGroupID.ToString();
                //txtRepGroup.Text = opportunities.RepGroupName;
                txtTonnes.Text = opportunities.Tonnes.ToString();
                HiddenFieldConversion.Value = (opportunities.Tonnes / opportunities.Units).ToString();

                // CREATED BY / DATE and LAST MODIFIED BY / DATE
                string createdBy = opportunities.CreatedBy;
                string createdDate = opportunities.CreatedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");

                tbCreated.Text = "Created By: " + createdBy + ", " + createdDate;

                string lastModifiedBy = opportunities.LastModifiedBy;
                string lastModifiedDate = opportunities.LastModifiedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");

                tbModified.Text = "Last Modified By: " + lastModifiedBy + ", " + lastModifiedDate;

                // Get the Products of this Opportunity

                int opportunityId = opportunities.OpportunityID;

                List<CRMServiceReference.CatalogDTO> catalogList = opportunityClient.GetOppProducts(opportunities.OpportunityID,new ArgsDTO());
                if (catalogList == null)
                    catalogList = new List<CRMServiceReference.CatalogDTO>();
                else
                {
                    HiddenFieldCode.Value = catalogList[0].CatlogCode;
                    HiddenFieldDesc.Value = catalogList[0].Description;
                    HiddenFieldPrice.Value = catalogList[0].Price.ToString();
                    
                }
                Session[CATALOGLIST] = catalogList;
            }
        }
        catch (Exception oException)
        {
            //commonUtility.ErrorMaintain(oException);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void load()
    {
        //lblRepGroup.Text = UserSession.Instance.RepGroups + " Group";
        //if (UserSession.Instance.IsAmountVisible == true)
        //    txtbkUnits.Visible = false;
        //else
        //    txtbkAmount.Visible = false;

        //lblAmount.Visible = false;
        //mtxtAmount.Visible = false;
       // txtbkAmount.Visible = false;
        //txtbkUnits.Visible = false;
        //txtName.Focus();

        //if (ViewState["custid"] != null && ViewState["custid"].ToString() != "")
        //{
        //    tbContact.Visible = false;
        //}


        HiddenFieldRepType.Value = UserSession.Instance.RepType;
        if (UserSession.Instance.RepType == "B")
            txtbkAmount.Visible = false;
        else
            txtbkUnits.Visible = false;

        //lblUnitsAbbriviation.Content = clsGlobal.GetInstance().UnitsAbbriviation.Trim();
        txtName.Focus();

        if (ViewState["custid"] != null && ViewState["custid"].ToString() != "")
        {
            tbContact.Visible = false;
        }
    }

    private void SaveOpportunity()
    {
        
        if (txtName.Text.Trim() == "")
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Name cannot be empty.");
            return;
        }

        if (string.IsNullOrWhiteSpace(ViewState["custid"].ToString()) && string.IsNullOrWhiteSpace(ViewState["eduid"].ToString()) && string.IsNullOrWhiteSpace(ViewState["leadid"].ToString()))
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Organisation/Customer/End User cannot be empty.");
            return;
        }

        if (string.IsNullOrEmpty(HiddenFieldCode.Value))
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("A product must be selected.");
            return;
        }

        //if (products.Count > 1)
        //{
        //    GlobalValues.GetInstance().ShowMessage("Only one product can be selected for an opportunity.",
        //        GlobalValues.MessageImageType.Information);
        //    return;
        //}

        //if (products.Count == 0)
        //{
        //    GlobalValues.GetInstance().ShowMessage("A product must be selected.", GlobalValues.MessageImageType.Information);
        //    return;
        //}

        if (UserSession.Instance.RepType == "B")
        {
            if (string.IsNullOrEmpty(mtxtUnits.Text.Trim()))
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Units cannot be empty.");
                mtxtUnits.Focus();
                return;
            }
            else if (Convert.ToDouble(mtxtUnits.Text.Trim()) == 0)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Units cannot be empty.");
                mtxtUnits.Focus();
                return;
            }

            if (string.IsNullOrEmpty(HiddenFieldPrice.Value) || Double.Parse(HiddenFieldPrice.Value) == 0)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Product Price cannot be empty.");
                return;
            }

            if (string.IsNullOrEmpty(txtTonnes.Text))
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Tonnes cannot be empty.");
                txtTonnes.Focus();
                return;
            }
            else if (Convert.ToDouble(txtTonnes.Text) == 0)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Tonnes cannot be empty.");
                txtTonnes.Focus();
                return;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(mtxtAmount.Text) || Convert.ToDouble(mtxtAmount.Text.Trim()) == 0)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Amount cannot be empty.");
                mtxtAmount.Focus();
                return;
            }
        }


        int iNoRecs;

        OpportunityClient oOpportunity = new OpportunityClient();
        OpportunityDTO opportunity = new OpportunityDTO();

        opportunity.OpportunityID = !string.IsNullOrWhiteSpace(txtOpportunityID.Value) ? Convert.ToInt32(txtOpportunityID.Value) : 0;
        opportunity.LeadID = ViewState["leadid"] != null ? int.Parse(ViewState["leadid"].ToString()) : 0;
        opportunity.Originator = (txtAssignto.Text == "" ? UserSession.Instance.UserName : txtAssignto.Text);
        opportunity.Name = txtName.Text;
        opportunity.CloseDate = !string.IsNullOrEmpty(dtpCloseDate.Text) ? DateTime.Parse(dtpCloseDate.Text): DateTime.Today;
        opportunity.Stage = Convert.ToInt32(cmbStage.SelectedValue);
        opportunity.Probability = Convert.ToDouble(radSlider.Value) * 10;
        if (!string.IsNullOrEmpty(mtxtAmount.Text))
            opportunity.Amount = mtxtAmount.Text.ToString().Trim() != "" ? Convert.ToDouble(mtxtAmount.Text.ToString().Trim()) : 0;
        //opportunity.Units = !string.IsNullOrWhiteSpace(txtUnits.Text) ? Convert.ToDouble(txtUnits.Text) : 0;
        if (!string.IsNullOrEmpty(mtxtUnits.Text))
            opportunity.Units = mtxtUnits.Text.ToString().Trim() != "" ? Convert.ToDouble(mtxtUnits.Text.ToString().Trim()) : 0;

        if (txtDescription.Text.IndexOf("(Max 500 Characters)") != 0)
            opportunity.Description = txtDescription.Text;

        opportunity.CreatedBy = UserSession.Instance.UserName;
        opportunity.CreatedDate = DateTime.Now.ToUniversalTime();
        opportunity.LastModifiedBy = UserSession.Instance.UserName;
        opportunity.LastModifiedDate = DateTime.Now.ToUniversalTime();
        //opportunity.CustCode = txtCustCode.Text;
        if (ViewState["custid"] != null)
            opportunity.CustCode = this.ViewState["custid"].ToString();
        opportunity.RepGroupID = !string.IsNullOrEmpty( RepGroupID.Value) ? int.Parse(RepGroupID.Value.ToString()) : 0;
        opportunity.Tonnes = !string.IsNullOrEmpty(txtTonnes.Text) ? Convert.ToDouble(txtTonnes.Text.Trim()) : 0;
        if (ViewState["eduid"] != null)
            opportunity.EndUserCode = this.ViewState["eduid"].ToString();

        try
        {
            
            List<CatalogDTO> list = new List<CatalogDTO>();//(List<CatalogDTO>)HttpContext.Current.Session[CATALOGLIST];
            if (!string.IsNullOrEmpty(HiddenFieldCode.Value))
                list.Add(new CatalogDTO() { CatlogCode = HiddenFieldCode.Value, Description = HiddenFieldDesc.Value, Price = double.Parse(HiddenFieldPrice.Value) });
            int opportunityId = 0;
            bool isSave = oOpportunity.SaveOpportunity(opportunity, list, out opportunityId);
            txtOpportunityID.Value = opportunityId.ToString();
            ViewState["oppid"] = opportunityId.ToString();
            if (isSave)
            {
                div_message.Attributes.Add("style", "display:block;padding:8px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
                //rgvProducts.DataSource = list;
                //rgvProducts.DataBind();
                Session[CATALOGLIST] = list;
            }
        }
        catch (Exception oException)
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured.");
        }
    }

    private void Clear()
    {
        dtpCloseDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        cmbStage.SelectedIndex = 0;

        tbCreated.Text = string.Empty;
        tbModified.Text = string.Empty;
        radSlider.Value = 0;
        mtxtAmount.Text = string.Empty;
        mtxtUnits.Text = string.Empty;
        ViewState.Remove("RepGroupID");
        //rgvProducts.DataSource = null;
        txtName.Text = string.Empty;

        txtAssignto.Text = UserSession.Instance.UserName;
    }

    #endregion Methods

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        txtDescription.Attributes.Add("maxLength", txtDescription.MaxLength.ToString());
        buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
        buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
        buttonbar.onButtonOpenContactEntry = new usercontrols_buttonbar.ButtonOpenContactEntry(ButtonOpenContactEntry_Click);
        buttonbar.VisibleSave(true);
        buttonbar.VisibleDeactivate(true);
        buttonbar.VisibleOpenContact(true);

        buttonbar.VisibleSaveandNew(false);

        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                PageLoad();
                
                LoadOptions();
                load();
                SetBackLink();
            }
            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Opportunity Entry ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void PageLoad()
    {
        Session.Remove(CATALOGLIST);
        int iOpportunityID = 0;
        int iLeadID = 0;
        string customerCode = "";

        txtAssignto.Text = UserSession.Instance.UserName;
        dtpCloseDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");

        if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty && int.Parse(Request.QueryString["leadid"]) != 0)
        {
            ViewState["leadid"] = Request.QueryString["leadid"];
            iLeadID = int.Parse(Request.QueryString["leadid"]);
            GetLeadDetails();
        }

        else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]) && Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"]))
        {
            ViewState["custid"] = Request.QueryString["custid"];
            ViewState["eduid"] = Request.QueryString["eduid"];
            GetEndUserDetails();
        }

        else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
        {
            ViewState["custid"] = Request.QueryString["custid"];
            customerCode = Request.QueryString["custid"];
            GetCustomerDetails();
        }

        if (Request.QueryString["oppid"] != null && Request.QueryString["oppid"] != string.Empty)
        {
            Session[OPPORTUNITY_ID] = Request.QueryString["oppid"];
            ViewState["oppid"] = Request.QueryString["oppid"];
            iOpportunityID = int.Parse(Request.QueryString["oppid"]);

            if (iOpportunityID > 0)
            {
                txtOpportunityID.Value = iOpportunityID.ToString();
                OpenOpportunity();
            }
            else
            {
                txtAssignto.Text = UserSession.Instance.UserName;
                cmbStage.SelectedIndex = 0;
            }
        }
        else
        {
            //rgvProducts.DataSource = new List<CatalogDTO>();
            //rgvProducts.DataBind();
            Session[CATALOGLIST] = new List<CatalogDTO>();
        }
    }

    private void SetBackLink()
    {
        if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty && int.Parse(Request.QueryString["leadid"]) != 0)
        {
            if ((Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty) && (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty))
                hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&fm=" + Request.QueryString["fm"] + "&ty=" + Request.QueryString["ty"] + "&ref=opp#tabs";
            //else if ((Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty) && (Request.QueryString["ty"] == "Quote")) {
            //    hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&ty=Quote&ref=opp#tabs";
            //}

            else
                hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&fm=activity&ref=opp#tabs";


        }
        else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
        {
            if (Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"]))
            {
                if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
                    hlBack.NavigateUrl = "~/lead_customer/transaction/enduser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&fm=" + Request.QueryString["fm"] + "&ref=opp#tabs";
                else
                    hlBack.NavigateUrl = "~/lead_customer/transaction/enduser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&fm=endUser&ref=opp#tabs";
            }
            else
            {
                if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
                    hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&fm=" + Request.QueryString["fm"] + "&ref=opp#tabs";
                else
                    hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&fm=lead&ty=customer&ref=opp#tabs";
            }
        }
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        {
            if (Request.QueryString["fm"] == "act")
            {
                if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty && int.Parse(Request.QueryString["leadid"]) != 0)
                {
                    hlBack.NavigateUrl = "~/activity_planner/transaction/activityentry.aspx?leadid=" + Request.QueryString["leadid"];
                }
                else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
                {
                    hlBack.NavigateUrl = "~/activity_planner/transaction/activityentry.aspx?custid=" + Request.QueryString["custid"];
                }
            }
        }

        //if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty && int.Parse(Request.QueryString["leadid"]) != 0)
        //{
        //    if (Request.QueryString["fm"] == "home")
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&fm=home&ref=opp#tabs";
        //    else
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&fm=" + Request.QueryString["fm"] + "&ref=opp#tabs";
        //}
        //else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
        //{
        //    if (Request.QueryString["fm"] == "home")
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&fm=home&ref=opp#tabs";
        //    else
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&ref=opp#tabs";
        //}
        //else if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        //{
        //    if (Request.QueryString["fm"] == "dsbd")
        //    {
        //        hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/CustomerOpportunities.aspx";
        //    }
        //    else if (Request.QueryString["fm"] == "home")
        //    {
        //        hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/CustomerOpportunities.aspx";
        //    }
        //    //hlBack
        //}
    }

    protected void btnAddProduct_Click(object sender, EventArgs e)
    {
        
    }
    
    protected void ButtonSave_Click(object sender)
    {
        try
        {
            SaveOpportunity();
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    protected void ButtonClear_Click(object sender)
    {
       // Clear();
        if (ViewState["leadid"] != null && ViewState["leadid"] != string.Empty && ViewState["leadid"].ToString() != "0")
            Response.Redirect(ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunityentry.aspx?leadid="+ViewState["leadid"].ToString());
        else if (ViewState["custid"] != null && ViewState["custid"] != string.Empty && ViewState["custid"].ToString() != "0")
            Response.Redirect(ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunityentry.aspx?custid=" + Server.UrlEncode(ViewState["custid"].ToString()));
    }

    protected void ButtonOpenContactEntry_Click(object sender)
    {
        StringBuilder additionalParam = new StringBuilder("?fm=oppent");
        
        if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty)
            additionalParam.Append("&oleadid=" + Request.QueryString["leadid"]);

        if (Request.QueryString["oppid"] != null && Request.QueryString["oppid"] != string.Empty)
            additionalParam.Append("&oppid=" + Request.QueryString["oppid"]);

        if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty)
            additionalParam.Append("&custid=" + Request.QueryString["custid"]);

        string referer = Request.ServerVariables["HTTP_REFERER"];
        

        Response.Redirect(ConfigUtil.ApplicationPath + "lead_customer/transaction/leadentry.aspx" + additionalParam.ToString());
    }
    /*
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static List<CatalogDTO> UpdateProduct(string code, string desc, string price)
    {
        List<CatalogDTO> list = (List<CatalogDTO>)HttpContext.Current.Session[CATALOGLIST];

        list = list == null ? new List<CatalogDTO>() : list;

        CatalogDTO catalog = list.Where(p => p.CatlogCode == code
                                            && p.Description == desc).FirstOrDefault();

        if (catalog == null)
        {
            CatalogDTO _catalogDTO = new CatalogDTO();
            _catalogDTO.CatlogCode = code;
            _catalogDTO.Description = desc;
            _catalogDTO.Price = double.Parse(price);
            list.Add(_catalogDTO);
            HttpContext.Current.Session[CATALOGLIST] = list;
        }
        return list;
    }

    protected void rgvProducts_UpdateCommand(object sender, GridCommandEventArgs e)
    {
        List<CatalogDTO> list = (List<CatalogDTO>)Session[CATALOGLIST];
        GridEditableItem item = e.Item as GridEditableItem;
        var catlogCode = item.GetDataKeyValue("CatlogCode").ToString();

        CatalogDTO catalog = list.Where(p => p.CatlogCode == catlogCode).FirstOrDefault();
        item.UpdateValues(catalog);
        Session[CATALOGLIST] = list;
    }

    protected void rgvProducts_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        List<CatalogDTO> list = (List<CatalogDTO>)Session[CATALOGLIST];
        //rgvProducts.DataSource = list;
    }
    */
    #endregion Events
}