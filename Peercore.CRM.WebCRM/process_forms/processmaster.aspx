﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="processmaster.aspx.cs" Inherits="process_forms_processmaster" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldType" runat="server" />
    <asp:HiddenField ID="HiddenFieldFormType" runat="server" />
    <div id="div_content_today_appointments" runat="server" class="divhidden">
        <div class="demo-section">
            <div id="listView_today_appointments">
            </div>
            <div id="pager_today_appointments" class="k-pager-wrap">
            </div>
        </div>
        <script type="text/x-kendo-tmpl" id="appointments_today_template">
               <div class="product">
                    
                    <h3>#:Subject#</h3>
                    <p>#:StartTime#</p>
                    <div>Worth of : #:EndTime#</div>
        </div>
        </script>
    </div>

    <div id="div_content_pending_appointments" runat="server" class="divhidden">
        <div class="demo-section">
            <div id="listView_pending_appointments">
            </div>
            <div id="pager_pending_appointments" class="k-pager-wrap">
            </div>
        </div>
         <script type="text/x-kendo-tmpl" id="Script1">
               <div class="product">
                    
                    <h3>#:Subject#</h3>
                    <p>#:StartTime#</p>
                    <div>Worth of : #:EndTime#</div>
        </div>
        </script>
    </div>
    </form>

    <script type="text/javascript">
        var form_type = document.getElementById("HiddenFieldFormType").value;
        var type = document.getElementById("HiddenFieldType").value;

        if (type == "todayapp") {
            $("#div_content_today_appointments").removeAttr("class");
            $("#div_content_today_appointments").attr("class", "divshow");

            DisplayAppointments('0', '10', type);

            function DisplayAppointments(startindex, rowcount, app_type) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            type: "POST",
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAppointmentsForHome",
                            contentType: 'application/json; charset=utf-8',
                            datatype: "json",
                            data: function () {
                                return {
                                    take: rowcount,
                                    skip: startindex,
                                    appointType: app_type
                                }
                            }
                        },
                        parameterMap: function (options) {
                            return JSON.stringify(options);
                        }
                    },
                    serverFiltering: true,
                    serverSorting: true,
                    pageSize: 3,
                    schema: {
                        data: "d.Data",
                        total: "d.Total"
                    } // schema
                });

                $("#pager_today_appointments").kendoPager({
                    dataSource: dataSource
                });

                $("#listView_today_appointments").kendoListView({
                    dataSource: dataSource,
                    dataBound: function (e) {
                        if (this.dataSource.data().length == 0) {
                            $("#listView_today_appointments").append("<h1>No Today's Appointments Found</h1>");
                        }
                    },
                    pageable: true,
                    template: kendo.template($("#appointments_today_template").html())
                });

                $("div#pager_today_appointments a.k-link").live('click', function () {

                    var id = $(this).data('page');
                    DisplayAppointments(id, '10', type);
                });
            }
        } else {
     //   alert("dd");
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        type: "POST",
                        url: ROOT_PATH + "service/pipelines_stage/pipelines_stage_service.asmx/GetOpportunitiesForHome",
                        contentType: 'application/json; charset=utf-8',
                        datatype: "json",
                        data: function () {
                            return {
                                take: rowcount,
                                skip: startindex
                            }
                        } // data
                    }, //read
                    parameterMap: function (options) {
                        return JSON.stringify(options);
                    } // parameterMap
                }, // transport
                serverFiltering: true,
                serverSorting: true,
                pageSize: 3,
                schema: {
                    data: "d.Data",
                    total: "d.Total"
                } // schema
            });

            $("#pager_pending_appointments").kendoPager({
                dataSource: dataSource
            });

            $("#listView_pending_appointments").kendoListView({
                dataSource: dataSource,
                dataBound: function (e) {
                    if (this.dataSource.data().length == 0) {
                        $("#listView_pending_appointments").append("<h1>No Opportunities Found</h1>");
                    }
                },
                pageable: true,
                template: kendo.template($("#Script1").html())
            });

            $("div#pager_pending_appointments a.k-link").live('click', function () {

                var id = $(this).data('page');
                DisplayOpportunities(id, '10');
            });
        }
    </script>
</body>
</html>

