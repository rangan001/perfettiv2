﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Peercore.CRM.Entities;
//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Shared;
using CRMServiceReference;

public partial class process_forms_processmaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["fm"] == "home_app")
                {
                    HiddenFieldFormType.Value = Request.QueryString["fm"].ToString();
                    HiddenFieldType.Value = Request.QueryString["type"].ToString();
                }
            }

            if (Request.QueryString["type"] == "ln")
            {
                if (Request.QueryString["fm"] == "ln")
                {
                    Response.Expires = -1;
                    Response.Clear();
                    string Logintxt = LoginPage();
                    Response.ContentType = "text/html";
                    Response.Write(Logintxt);
                    Response.End();
                }
            }
        }
    }

    private string LoginPage()
    {
        StringBuilder txt = new StringBuilder();
        OriginatorClient OriginatorClient = new OriginatorClient();
        CommonClient CommonClient = new CommonClient();
        OriginatorDTO OriginatorDTO = new OriginatorDTO();

        try
        {
            OriginatorDTO = OriginatorClient.GetLoginDetails(Request.QueryString["uname"], Request.QueryString["pwd"]);

            if (OriginatorDTO.OriginatorId > 0)
            {
                UserSession.Instance.OriginatorId = OriginatorDTO.OriginatorId;
                UserSession.Instance.Originator = OriginatorDTO.Name;
                UserSession.Instance.OriginatorCookers = OriginatorDTO.Name;
                UserSession.Instance.UserName = Request.QueryString["uname"];
                UserSession.Instance.OriginalUserName = Request.QueryString["uname"];
                UserSession.Instance.DefaultDepartmentId = OriginatorDTO.DefaultDepartmentId;
                UserSession.Instance.OriginatorEmail = OriginatorDTO.Email;

                UserSession.Instance.ManagerMode = true;
                UserSession.Instance.HasChildReps = OriginatorDTO.HasChildReps;
                UserSession.Instance.ChildOriginators = OriginatorClient.GetChildOriginators(Request.QueryString["uname"], true);
                string rep = "";
                string childOriginatorsForRep = UserSession.Instance.ChildOriginators.Replace("originator", "r.originator");
                UserSession.Instance.RepCodeList = OriginatorClient.GetChildReps(Request.QueryString["uname"], UserSession.Instance.ManagerMode, ref rep, childOriginatorsForRep);
                UserSession.Instance.ChildReps = rep;

                UserSession.Instance.BDMList = OriginatorClient.GetBDMString();
                UserSession.Instance.CRMAuthLevel = OriginatorDTO.CRMAuthLevel;

                UserSession.Instance.ClientType = OriginatorDTO.ClientType;
                UserSession.Instance.RepCode = OriginatorDTO.RepCode;
                UserSession.Instance.RepType = OriginatorDTO.RepType;
                UserSession.Instance.OriginatorString = OriginatorDTO.DeptString;
                UserSession.Instance.AccessToken = OriginatorDTO.AccessToken;

                try
                {
                    CommonClient cClient = new CommonClient();
                    cClient.CreateTransactionLog(Request.QueryString["uname"],
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Login,
                        "Backend-Login",
                        OriginatorDTO.Name + " is logged in to the system");
                }
                catch { }

                txt.Append("true");
            }
            else
            {
                txt.Append("<div id=\"message_red\">");
                txt.Append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
                txt.Append("<tr>");
                txt.Append("<td class=\"red-left\"><a href=\"\">Invalid login,Please try again.</a></td>");
                txt.Append("<td class=\"red-right\" align=\"top\"><a class=\"close-red\"><img src=\"assets/images/icon_close_red.gif\" border=\"0\" onclick=\"divHide()\"  alt=\"\" /></a></td>");
                txt.Append("</tr>");
                txt.Append("</table>");
                txt.Append("</div>");
            }

            OriginatorClient.Close();
        }
        catch (Exception)
        {
            throw;
        }
        return txt.ToString();
    }



    //private string LoginPage()
    //{
    //    StringBuilder txt = new StringBuilder();
    //    OriginatorClient OriginatorClient = new OriginatorClient();
    //    OriginatorDTO OriginatorDTO = new OriginatorDTO();

    //    try
    //    {
    //        OriginatorDTO = OriginatorClient.GetLoginDetails(Server.HtmlEncode(Request.QueryString["uname"]),Server.HtmlEncode(Request.QueryString["pwd"]));

    //        if (OriginatorDTO.OriginatorId > 0)
    //        {
    //                UserSession.Instance.OriginatorId = OriginatorDTO.OriginatorId;
    //                UserSession.Instance.Originator = OriginatorDTO.Name;
    //                UserSession.Instance.OriginatorCookers = OriginatorDTO.Name;
    //                UserSession.Instance.UserName = Request.QueryString["uname"];
    //                UserSession.Instance.OriginalUserName = Request.QueryString["uname"];
    //                UserSession.Instance.DefaultDepartmentIdCookers = "CT";
    //                UserSession.Instance.OriginatorEmail = OriginatorDTO.Email;

    //                UserSession.Instance.ManagerMode = true;
    //                UserSession.Instance.HasChildReps = OriginatorDTO.HasChildReps;
    //                UserSession.Instance.ChildOriginatorsCookers = OriginatorClient.GetChildOriginators(Request.QueryString["uname"],false,true);
    //                UserSession.Instance.BDMList = OriginatorClient.GetBDMString();
    //                UserSession.Instance.CRMAuthLevel = OriginatorDTO.CRMAuthLevel;

    //                txt.Append("true");
    //        }
    //        else
    //        {
    //            txt.Append("<div id=\"message_red\">");
    //            txt.Append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
    //            txt.Append("<tr>");
    //            txt.Append("<td class=\"red-left\">Error. <a href=\"\">Invalid login,Please try again.</a></td>");
    //            txt.Append("<td class=\"red-right\" align=\"top\"><a class=\"close-red\"><img src=\"assets/images/icon_close_red.gif\" border=\"0\" onclick=\"divHide()\"  alt=\"\" /></a></td>");
    //            txt.Append("</tr>");
    //            txt.Append("</table>");
    //            txt.Append("</div>");
    //        }

    //        OriginatorClient.Close();
    //    }
    //    catch (Exception)
    //    {
            
    //        throw;
    //    }
    //    return txt.ToString();
    //}

    //private string DeleteMessage()
    //{
    //    StringBuilder txt = new StringBuilder();

    //    bool deleteSuccessful = MessageEntryBR.Instance.DeleteMessage(Guid.Parse(Request.QueryString["id"]));

    //    if (deleteSuccessful)
    //        txt.Append("true");

    //    return txt.ToString();
    //}

    //private string InsertMessage()
    //{
    //    StringBuilder txt = new StringBuilder();
    //    List<OriginatorEntity> lstoriginator = new List<OriginatorEntity>();
    //    OriginatorEntity Originator = null;

    //    MessagesEntity Messages = MessagesEntity.CreateObject();
    //    Messages.Message = Convert.ToString(Request.QueryString["message"]);
    //    Messages.Originator = Convert.ToString(Request.QueryString["originator"]);
    //    if (Convert.ToString(Request.QueryString["id"]) != "")
    //    {
    //        Messages.MessagesId = Guid.Parse(Request.QueryString["id"]);
    //    }
    //    else
    //    {
    //        Messages.MessagesId = System.Guid.NewGuid();
    //    }
    //    Messages.MessageDate = Convert.ToDateTime(Request.QueryString["datetime"]);
    //    Messages.DelFlag = "N";
    //    string[] words = Request.QueryString["selectedOriginator"].Split(',');

    //    for(int i=0; i< words.Length; i++)
    //    {
    //        if (words[i] != "")
    //        {
    //            Originator = new OriginatorEntity();
    //            Originator.OriginatorId = Guid.Parse(words[i]);
    //            lstoriginator.Add(Originator);
    //        }
    //    }

    //    bool saveSuccessful = MessageEntryBR.Instance.InsertMessage(Messages, lstoriginator);

    //    if (saveSuccessful)
    //        txt.Append("true");


    //    return txt.ToString();
    //}

    //private string LoginUser()
    //{
    //    StringBuilder txt = new StringBuilder();
    //    List<OriginatorEntity> lstoriginator = new List<OriginatorEntity>();
    //    OriginatorEntity Originator = null;

    //    MessagesEntity Messages = MessagesEntity.CreateObject();
    //    Messages.Message = Convert.ToString(Request.QueryString["message"]);
    //    Messages.Originator = Convert.ToString(Request.QueryString["originator"]);
    //    if (Convert.ToString(Request.QueryString["id"]) != "")
    //    {
    //        Messages.MessagesId = Guid.Parse(Request.QueryString["id"]);
    //    }
    //    else
    //    {
    //        Messages.MessagesId = System.Guid.NewGuid();
    //    }

    //    Messages.MessageDate = Convert.ToDateTime(Request.QueryString["datetime"]);
    //    Messages.DelFlag = "N";
    //    string[] words = Request.QueryString["selectedOriginator"].Split(',');

    //    for (int i = 0; i < words.Length; i++)
    //    {
    //        if (words[i] != "")
    //        {
    //            Originator = new OriginatorEntity();
    //            Originator.OriginatorId = Guid.Parse(words[i]);
    //            lstoriginator.Add(Originator);
    //        }
    //    }

    //    bool saveSuccessful = MessageEntryBR.Instance.InsertMessage(Messages, lstoriginator);

    //    if (saveSuccessful)
    //        txt.Append("true");

    //    return txt.ToString();
    //}

    //private string LoadOpportunity()
    //{
    //    //opportunityes
    //    StringBuilder sb = new StringBuilder();
    //    OpportunityClient opportunityClient = new OpportunityClient();
    //    List<OpportunityDTO> lstOpportunities = new List<OpportunityDTO>();

    //    #region Argument
    //    ArgsDTO args = GetArgumentSettings();

    //    args.OrderBy = "close_date desc";
    //    args.StartIndex = 0;
    //    args.RowCount = 250;
    //    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentIdCookers;

    //    #endregion

    //    lstOpportunities = opportunityClient.GetAllOpportunities(0, args.Originator, args, DateTime.Now);

    //    if (lstOpportunities != null)
    //    {
    //        if (lstOpportunities.Count != 0)
    //        {
    //            sb.Append("<div style=\"width:100%;\">");
    //            sb.Append("<div style=\"padding-left: 10px;\">Opportunities (" + lstOpportunities [0].RowCount.ToString()+ ")</div>");
    //            sb.Append("</div>");

    //            sb.Append("<div>");
    //            sb.Append("<table width=\"100%\" border=\"0\">");
    //            foreach (OpportunityDTO obj in lstOpportunities)
    //            {
    //                sb.Append("<tr>");
    //                sb.Append("<td class=\"headerbgs\" colspan=\"2\" ><img src=\"assets/images/Opportunityicon.png\">");

    //                if (obj.CustCode != "")
    //                {
    //                    sb.Append("<a href=pipelines_stage/transaction/opportunityentry.aspx?oppid=" + obj.OpportunityID + "&leadid=" + obj.LeadID + "&custid=" + obj.CustCode + "&fm=home class=\"maintopiclinks\">" + obj.LeadName + "</a>");
    //                }
    //                else
    //                {
    //                    sb.Append("<a href=pipelines_stage/transaction/opportunityentry.aspx?oppid=" + obj.OpportunityID + "&leadid=" + obj.LeadID + "&fm=home class=\"maintopiclinks\">" + obj.LeadName + "</a>");
    //                }

    //                sb.Append("</td>");
    //                sb.Append("<tr>");
    //                sb.Append("<td class=\"innerrows\" colspan=\"2\">");
    //                sb.Append(obj.LeadName);
    //                sb.Append("</td>");
    //                sb.Append("</tr>");
    //                sb.Append("<tr class=\"innerrowsfor\">");
    //                sb.Append("<td style=\"width:50%; padding-left:10px\">");
    //                sb.Append("Worth of : ");
    //                sb.Append(obj.Amount);
    //                sb.Append("</td>");
    //                sb.Append("<td style=\"width:50%; text-align:right\">");
    //                sb.Append("Closing Date : ");
    //                sb.Append(obj.CloseDate.ToString("dd/MM/yyyy hh:mm tt"));
    //                sb.Append("</td>");
    //                sb.Append("</tr>");
    //            }
    //            sb.Append("</table>");
    //            sb.Append("</div>");
    //        }
    //    }

    //    return sb.ToString();
    //}

    //private string LoadAppointments()
    //{
    //    StringBuilder txt = new StringBuilder();
    //    List<AppointmentViewDTO> lstAppointmentView = new List<AppointmentViewDTO>();
    //    AppointmentClient appointmentClient = new AppointmentClient();

    //    #region Argument
    //    ArgsDTO args = GetArgumentSettings();
        
    //    string FromValue = DateTime.Parse(DateTime.Today.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");
    //    string ToValue = DateTime.Parse(DateTime.Today.ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");
       
    //    args.StartIndex = 0;
    //    args.RowCount = 10;

    //    args.SStartDate = FromValue;
    //    args.SEndDate = ToValue;
        
    //    #endregion

    //    lstAppointmentView = appointmentClient.GetAppointmentsByCategory(args, "today");
    //    if (lstAppointmentView != null)
    //    {
    //        if (lstAppointmentView.Count != 0)
    //        {
    //            txt.Append("<div id=\"appointment\">");
    //            txt.Append("<div>");
    //            txt.Append("   <h3><a href=\"#\">First</a></h3>");
    //            txt.Append("   <div>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</div>");
    //            txt.Append("</div>");
    //            txt.Append(" <div>");
    //            txt.Append("     <h3><a href=\"#\">Second</a></h3>");
    //            txt.Append("     <div>Phasellus mattis tincidunt nibh.</div>");
    //            txt.Append("</div>");
    //            txt.Append(" <div>");
    //            txt.Append("     <h3><a href=\"#\">Third</a></h3>");
    //            txt.Append("     <div>Nam dui erat, auctor a, dignissim quis.</div>");
    //            txt.Append(" </div>");
    //            txt.Append("</div>");


    //            txt.Append("<div style=\"width:100%;\">");
    //            txt.Append("<div style=\"padding-left: 10px;\">Expired Appointments (" + lstAppointmentView.Count.ToString() + ")</div>");
    //            txt.Append("</div>");

    //            txt.Append("<div style=\"width:100%;\">");
    //            txt.Append("<div style=\"padding-left: 10px;\">Future Appointments (" + lstAppointmentView.Count.ToString() + ")</div>");
    //            txt.Append("</div>");

    //            txt.Append("<div style=\"width:100%;\">");
    //            txt.Append("<div style=\"padding-left: 10px;\">Todays Appointments (" + lstAppointmentView.Count.ToString() + ")</div>");
    //            txt.Append("</div>");

    //            txt.Append("<div>");
    //            txt.Append("<table width=\"100%\" border=\"0\">");

    //            foreach (AppointmentViewDTO obj in lstAppointmentView)
    //            {
    //                txt.Append("<tr>");
    //                txt.Append("<td class=\"headerbgs\"><img src=\"assets/images/remidesicon.png\"> ");

    //                if (obj.LeadId > 0 && obj.CustCode.Trim() == string.Empty)
    //                {
    //                    txt.Append("<a href=activity_planner/transaction/activityentry.aspx?leadId=" + obj.LeadId + "&activityId=" + obj.ActivityId + "&appointmentid=" + obj.AppointmentID + "&custid=" + obj.CustCode + "&fm=home class=\"maintopiclinks\">" + obj.Subject + "</a>");
    //                }
    //                else if (obj.CustCode.Trim() != string.Empty)
    //                {
    //                    txt.Append("<a href=activity_planner/transaction/activityentry.aspx?custid=" + obj.CustCode + "&activityId=" + obj.ActivityId + "&appointmentid=" + obj.AppointmentID + "&custid=" + obj.CustCode + "&fm=home class=\"maintopiclinks\">" + obj.Subject + "</a>");
    //                }
    //                else
    //                {
    //                    txt.Append(obj.Subject);
    //                }
    //                txt.Append("</td>");
    //                txt.Append("</tr>");
    //                txt.Append("<tr>");
    //                txt.Append("<td class=\"innerrowsfor\">");
    //                txt.Append("From : ");
    //                txt.Append(obj.StartTime);
    //                txt.Append(" - ");
    //                txt.Append(obj.EndTime);
    //                txt.Append("</td>");
    //                txt.Append("</tr>");
    //            }
    //            txt.Append("</table>");
    //            txt.Append("</div>");

    //            txt.Append("<script type=\"text/javascript\">");
                
    //            txt.Append("$(\"#appointment\").accordion({ header: 'h3' });");
    //            txt.Append("</script>");
    //        }
    //    }

    //    return txt.ToString();
    //}

    //private string LoadMissedDelivery()
    //{
    //    // Missed Deliveries
    //    List<CustomerActivitiesDTO> lstMissedDeliveries = new List<CustomerActivitiesDTO>();
    //    StringBuilder txt3 = new StringBuilder();
    //    CommonClient commonClient = new CommonClient();

    //    #region Argument
    //    ArgsDTO args = GetArgumentSettings();

    //    string FromValue = DateTime.Parse(DateTime.Today.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");
    //    string ToValue = DateTime.Parse(DateTime.Today.ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");

    //    args.StartIndex = 0;
    //    args.RowCount = 10;

    //    args.SStartDate = FromValue;
    //    args.SEndDate = ToValue;

    //    #endregion

    //    lstMissedDeliveries = commonClient.GetMissedDeliveries(args);
    //    if (lstMissedDeliveries != null)
    //    {
    //        if (lstMissedDeliveries.Count != 0)
    //        {
    //            txt3.Append("<div style=\"width:100%;\">");
    //            txt3.Append("<div style=\"padding-left: 10px;\">Opportunities (" + lstMissedDeliveries[0].RowCount.ToString() + ")</div>");
    //            txt3.Append("</div>");

    //            txt3.Append("<div>");
    //            txt3.Append("<table width=\"100%\" border=\"0\">");
    //            foreach (CustomerActivitiesDTO obj in lstMissedDeliveries)
    //            {
    //                txt3.Append("<tr>");
    //                txt3.Append("<td class=\"headerbgs\"><img src=\"assets/images/missedtruck.png\"> ");
    //                txt3.Append(" <a href=lead_customer/transaction/customer_entry.aspx?custid=" + obj.CustCode + "&fm=home&ref=act class=\"maintopiclinks\">" + obj.LeadName + "</a>");
    //                txt3.Append("</td>");
    //                txt3.Append("</tr>");
    //                txt3.Append("<tr>");
    //                txt3.Append("<td class=\"innerrowsfor\">");
    //                txt3.Append(obj.EndDate.ToString("dd/MMM/yyyy"));
    //                txt3.Append(" - ");
    //                txt3.Append(obj.Subject);
    //                txt3.Append("</td>");
    //                txt3.Append("</tr>");
    //            }
    //            txt3.Append("</table>");
    //            txt3.Append("</div>");
    //        }
    //    }

    //    return txt3.ToString();
    //}

    //private string LoadReminders()
    //{
    //    StringBuilder txt1 = new StringBuilder();
    //    List<CustomerActivitiesDTO> lstReminders = new List<CustomerActivitiesDTO>();
    //    ActivityClient activityClient = new ActivityClient();

    //    #region Argument
    //    ArgsDTO args = GetArgumentSettings();
    //    args.StartIndex = 1;
    //    args.RowCount = 1000;
    //    args.SStartDate = "";
    //    args.SEndDate = "";
    //    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentIdCookers;
    //    args.ManagerMode = UserSession.Instance.ManagerMode;
    //    #endregion
        
    //    lstReminders = activityClient.GetAllHomeActivitiesByCategory(args, "past");
    //    lstReminders = lstReminders.OrderByDescending(c => c.StartDate).ToList();

    //    if (lstReminders != null)
    //    {
    //        if (lstReminders.Count != 0)
    //        {
    //            txt1.Append("<div style=\"width:100%;\">");
    //            txt1.Append("<div style=\"padding-left: 10px;\">Opportunities (" + lstReminders[0].RowCount.ToString() + ")</div>");
    //            txt1.Append("</div>");

    //            txt1.Append("<div>");
    //            txt1.Append("<table width=\"100%\" border=\"0\">");
    //            foreach (CustomerActivitiesDTO obj in lstReminders)
    //            {
    //                txt1.Append("<tr>");
    //                txt1.Append("<td class=\"headerbgs\"><img src=\"assets/images/remidesicon.png\"> ");
    //                //txt.Append("<b>");
    //                if (obj.LeadId > 0 && obj.CustCode.Trim() == string.Empty)
    //                {
    //                    txt1.Append("<a href=activity_planner/transaction/activityentry.aspx?leadId=" + obj.LeadId + "&activityId=" + obj.ActivityID + "&appointmentid=" + obj.AppointmentId + "&fm=home class=\"maintopiclinks\">" + obj.LeadName + "</a>");
    //                }
    //                else if (obj.CustCode.Trim() != string.Empty)
    //                {
    //                    txt1.Append("<a href=activity_planner/transaction/activityentry.aspx?custid=" + obj.CustCode + "&activityId=" + obj.ActivityID + "&appointmentid=" + obj.AppointmentId + "&fm=home class=\"maintopiclinks\">" + obj.LeadName + "</a>");
    //                }
    //                //txt.Append("<b>");
    //                txt1.Append("</td>");
    //                txt1.Append("</tr>");
    //                txt1.Append("<td class=\"innerrows\">");
    //                txt1.Append(obj.Subject);
    //                txt1.Append("</td>");
    //                txt1.Append("<tr>");
    //                txt1.Append("<td class=\"innerrowsfor\">");
    //                txt1.Append("From : ");
    //                txt1.Append(obj.EndDate.ToString("dd/MM/yyyy hh:mm tt"));
    //                txt1.Append(" - ");
    //                txt1.Append(obj.EndDate.ToString("dd/MM/yyyy hh:mm tt"));
    //                txt1.Append("</td>");
    //                txt1.Append("</tr>");
    //                txt1.Append("<tr>");
    //                txt1.Append("<td class=\"innerrowsfor\">");
    //                txt1.Append("Priority : " + obj.Priority + " - Status : " + obj.StatusDesc);
    //                txt1.Append("</td>");
    //                txt1.Append("</tr>");
    //            }
    //            txt1.Append("</table>");
    //            txt1.Append("</div>");
    //        }
    //    }

    //    return txt1.ToString();
    //}

    //private string LoadNewCustomerReminders()
    //{
    //    StringBuilder txt = new StringBuilder();

    //    return txt.ToString();
    //}

    //private ArgsDTO GetArgumentSettings()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.ChildOriginators = UserSession.Instance.ChildOriginators;
    //    args.Originator = ((string.IsNullOrEmpty(UserSession.Instance.FilterByUserName)) ? UserSession.Instance.UserName : UserSession.Instance.FilterByUserName);
    //    if (UserSession.Instance.FilterByRepOriginatorId != 0 && UserSession.Instance.FilterByRepOriginatorId != null)
    //    {
    //        args.OriginatorId = UserSession.Instance.FilterByRepOriginatorId;
    //    }
    //    else
    //    {
    //        args.OriginatorId = UserSession.Instance.OriginatorId;
    //    }
       
    //    return args;
    //}
}