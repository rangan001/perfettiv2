﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Configuration;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;

public partial class programs_process_forms_processmaster : System.Web.UI.Page
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "Update")
                {
                    if (Request.QueryString["fm"] == "leadCustomer")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string saveHeaderText = UpdateHeaderText();
                        //Response.ContentType = "text/html";
                        ////Response.Write(saveHeaderText);
                        //Response.End();
                    }

                }
                else if (Request.QueryString["type"] == "Delete")
                {
                    
                }
                else if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "checkCustomerProgram")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = CheckLeadCustomer();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }

                    if (Request.QueryString["fm"] == "checkvolumeprogram")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = CheckVolumeList();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "get")
                {
                    
                }
                else if (Request.QueryString["type"] == "change")
                {
                    
                }
                else if (Request.QueryString["type"] == "insert")
                {
                    
                }
            }
        }
    }
    #endregion

    #region Methods

    public string CheckLeadCustomer()
    {
      Session[CommonUtility.VOLUME_PROGRAM_CONTACTS] = null;
        string custCode = Server.HtmlDecode(Request.QueryString["CustomerCode"]);
        string isChecked = Request.QueryString["isChecked"];
        List<string> listProgramCustomerContact = new List<string>();
        listProgramCustomerContact = (List<string>)Session[CommonUtility.CUSTOMER_PROGRAM_CONTACTS];
        if (listProgramCustomerContact == null)
            listProgramCustomerContact = new List<string>();

        CallCycleContactDTO callCycleContact = new CallCycleContactDTO();

        if (isChecked == "1")
        {
            if (!listProgramCustomerContact.Contains(custCode))
            {
                listProgramCustomerContact.Add(custCode);
            }
        }
        else
        {
            if (listProgramCustomerContact.Contains(custCode))
            {
                listProgramCustomerContact.Remove(custCode);
            }            
        }
        Session[CommonUtility.CUSTOMER_PROGRAM_CONTACTS] = listProgramCustomerContact;

        return "true";
    }


    public string CheckVolumeList()
    {
        int volume = Convert.ToInt32(Server.HtmlDecode(Request.QueryString["volume"]));
        string isChecked = Request.QueryString["isChecked"];
        List<int> listProgramVolumeContact = new List<int>();
        Session[CommonUtility.CUSTOMER_PROGRAM_CONTACTS] = null;
        listProgramVolumeContact = (List<int>)Session[CommonUtility.VOLUME_PROGRAM_CONTACTS];
        if (listProgramVolumeContact == null)
            listProgramVolumeContact = new List<int>();

        CallCycleContactDTO callCycleContact = new CallCycleContactDTO();

        if (isChecked == "1")
        {
            if (!listProgramVolumeContact.Contains(volume))
            {
                listProgramVolumeContact.Add(volume);
            }
        }
        else
        {
            if (listProgramVolumeContact.Contains(volume))
            {
                listProgramVolumeContact.Remove(volume);
            }
        }
        Session[CommonUtility.VOLUME_PROGRAM_CONTACTS] = listProgramVolumeContact;

        return "true";
    }


    #endregion
}