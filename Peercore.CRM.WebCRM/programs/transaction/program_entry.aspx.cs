﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Text;

public partial class programs_transaction_program_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);

            CommonClient serviceCommon = new CommonClient();
            ProgramDTO oProgram = new ProgramDTO();

            buttonbar.EnableSave(true);
            buttonbar.VisibleSave(true);
            buttonbar.VisibleDeactivate(true);
            buttonbar.EnableDeactivate(true);

            //SetBackLink();
            BindLeadCustomers();
            if (!IsPostBack)
            {
                Session[CommonUtility.CALL_CYCLE_CONTACTS] = null;
                Session[CommonUtility.CUSTOMER_PROGRAM_CONTACTS] = null;
                Session[CommonUtility.VOLUME_PROGRAM_CONTACTS] = null;
                if (Request.QueryString["programid"] != null)
                {                    
                    HiddenFieldProgramID.Value = Request.QueryString["programid"];

                    oProgram = serviceCommon.GetProgramDetailsById(Convert.ToInt32(HiddenFieldProgramID.Value));
                    if (oProgram.SelectionType == "R")
                    {                     
                       Session[CommonUtility.CUSTOMER_PROGRAM_CONTACTS] = oProgram.CustomerList;
                       RadioButtonRandom.Checked = true;
                       div_volume.Visible = false;
                       radioVolume.Visible = false;

                    }
                    else if (oProgram.SelectionType == "V")
                    {
                        Session[CommonUtility.VOLUME_PROGRAM_CONTACTS] = oProgram.VolumeList;
                        RadioButtonVolume.Checked = true;

                            foreach (int volume in oProgram.VolumeList)
                            {
                                ListItem listItem = this.checkBoxListVolume.Items.FindByValue(volume.ToString());
                                if (listItem != null) 
                                    listItem.Selected = true;
                            }

                            div_random.Visible = false;
                            radioRandom.Visible = false;


                    }
                    txtProgramName.Text = oProgram.ProgramName;
                }
                else
                {
                    HiddenFieldProgramID.Value = "0";
                }
            }

            Master.SetBreadCrumb("Program Entry", "#", "");
        }
    }

    //private void SetCallCycleState(int iCallCycleID)
    //{
    //    bool isInActive = false;
    //    if (iCallCycleID == 0)
    //        isInActive = false;
    //    else
    //        isInActive = new CallCycleClient().GetCallCycleState(iCallCycleID);

    //    if (isInActive)
    //    {
    //        buttonbar.SetActiveDeactive(true);
    //    }
    //    else
    //    {
    //        buttonbar.SetActiveDeactive(false);
    //    }
    //}

    //public void LoadCallCycle(int iCallCycleID)
    //{
    //    ArgsDTO argsDTO = new ArgsDTO();

    //    argsDTO.Originator = UserSession.Instance.UserName;
    //    argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
    //    argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //    argsDTO.AdditionalParams = "";
    //    argsDTO.OrderBy = "CallCycleID ASC";

    //    argsDTO.StartIndex = 1;
    //    argsDTO.RowCount = 10;

    //    List<CallCycleDTO> listCallCycleContact = new List<CallCycleDTO>();
    //    CallCycleClient callCycleClient = new CallCycleClient();

    //    listCallCycleContact = callCycleClient.GetCallCycle(argsDTO, false, iCallCycleID, "");

    //    if (listCallCycleContact.Count > 0)
    //    {
    //        //txtDescription.Text = listCallCycleContact[0].Comments;
    //        //txtSchedule.Text = listCallCycleContact[0].Description;
    //        //txtDRName.Text = listCallCycleContact[0].RepName;
    //        HiddenField_DueDate.Value = listCallCycleContact[0].DueOnString;
    //        tbCreated.InnerHtml = "Created By: " + listCallCycleContact[0].CreatedBy + ", " + listCallCycleContact[0].CreatedDateString;
    //    }
    //}

    private void BindLeadCustomers()
    {
        if (CurrentUserSettings == null)
            CurrentUserSettings = new ArgsDTO();
        CurrentUserSettings.ChildOriginators = UserSession.Instance.ChildOriginators;
        CurrentUserSettings.Originator = UserSession.Instance.UserName;
        CurrentUserSettings.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        CurrentUserSettings.ActiveInactiveChecked = true;
        CurrentUserSettings.ReqSentIsChecked = false;
        CurrentUserSettings.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;
        CurrentUserSettings.LeadStage = "";
    }

    protected void ButtonSave_Click(object sender)
    {
        CommonUtility commonUtility = new CommonUtility();

        if (txtProgramName.Text.Trim() == "")
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Program name is required.");
            txtProgramName.Focus();
            return;
        }
        SaveProgram();
    }

    private void SaveProgram()
    {
        try
        {
            bool iNoRecs = false;

            ProgramDTO oProgram = new ProgramDTO();
            CommonClient commonClient = new CommonClient();

            List<string> listProgramCustomerContact = new List<string>();
            listProgramCustomerContact = (List<string>)Session[CommonUtility.CUSTOMER_PROGRAM_CONTACTS];

            List<int> listProgramVolumeContact = new List<int>();

            IEnumerable<int> CheckedItems = checkBoxListVolume.Items.Cast<ListItem>()
                                   .Where(i => i.Selected)
                                   .Select(i => Convert.ToInt32( i.Value ));

            listProgramVolumeContact = CheckedItems.ToList();

            oProgram.ProgramName = txtProgramName.Text;
            if (HiddenFieldProgramID.Value.ToString() != "0")
            {
                oProgram.ProgramId = Convert.ToInt32(HiddenFieldProgramID.Value);
            }
            oProgram.CreatedBy = UserSession.Instance.UserName;

            if (listProgramCustomerContact != null)
            {
                if (listProgramCustomerContact.Count > 0)
                {
                    oProgram.CustomerList = listProgramCustomerContact;
                    oProgram.SelectionType = "R";
                }
            }

            if (listProgramVolumeContact != null)
            {
                if (listProgramVolumeContact.Count > 0)
                {
                    oProgram.VolumeList = listProgramVolumeContact;
                    oProgram.SelectionType = "V";
                }
            }

            if (IsProgramNameExists(oProgram))
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = new CommonUtility().GetErrorMessage("Program Name Already Exists !.");
                return;
            }

            iNoRecs = commonClient.SaveProgram(oProgram);

            if (iNoRecs)
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = new CommonUtility().GetSucessfullMessage("Successfully Saved.");
            }
        }
        catch (Exception oException)
        {
        }
    }

    protected void ButtonClear_Click(object sender)
    {
        Clear();
    }

    private void Clear()
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "OpenWindow", "window.open('" + ConfigUtil.ApplicationPath + "programs/transaction/program_entry.aspx', '_self');", true);       
    }

    private bool IsProgramNameExists(ProgramDTO programDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsProgramNameExists(programDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }      
}