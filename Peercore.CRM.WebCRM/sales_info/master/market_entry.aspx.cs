﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;

public partial class sales_info_master_market_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Remove the session before setting the breadcrumb.
        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("Market Entry ", "#", "");
    }
}