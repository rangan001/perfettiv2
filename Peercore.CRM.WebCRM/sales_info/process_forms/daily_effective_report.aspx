﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="daily_effective_report.aspx.cs" Inherits="sales_info_process_forms_daily_effective_report" %>

<%@ Register Src="~/usercontrols/buttonbar_analysisgraph.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            $("#div_FromDate").css("display", "inline-block");
            $("#div_ToDate").css("display", "inline-block");
            $("#MainContent_toolbar1_a_callcycle").css("background-color", "#ff6600");
        
        });

    </script>
    <div class="divcontectmainforms">
        <div class="title_bar">
            <div style="width: 90%; float: left;">
            </div>
            <div style="width: 10%; float: right;">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/dashboard/transaction/ActivityAnalysis.aspx">
                        <div class="back"></div>
                </asp:HyperLink>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div align="right">
            <div class="bg" align="left">
                <div id="div_message" class="savemsg" runat="server" style="display: none">
                </div>
                <div style="padding-left: 10px;">
                    <ucl:buttonbar ID="buttonbar1" runat="server" />
                </div>
            </div>
        </div>
        <div class="clearall">
            &nbsp;</div>
       <%-- <div style="background-color: #FFFF; padding: 10px; overflow: auto; height: 400px;">
            <table border="0">
                <tr>
                    <td>
                        <input id="fromDate" runat="server" value="" style="width: 100px;" />
                    </td>
                    <td>
                        <input id="toDate" runat="server" value="" style="width: 100px;" />
                    </td>
                    <td>
                        <asp:Button ID="btnGenerateReport" runat="server" Text="GENERATE" OnClick="btnGenerateReport_Click" />
                    </td>
                </tr>
            </table>
        </div>--%>
    </div>
</asp:Content>
