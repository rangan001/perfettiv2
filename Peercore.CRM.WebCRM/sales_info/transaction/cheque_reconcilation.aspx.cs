﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Text;
using CRMServiceReference;
using System.Web.Script.Serialization;

public partial class sales_info_transaction_cheque_reconcilation : PageBase
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //Set the Toolbar buttons
            buttonbar.VisibleSave(true);
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.EnableSave(true);

            if (!IsPostBack)
            {
                dtpDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");
                dtpDateTo.Text = DateTime.Today.ToString("dd-MMM-yyyy");

                SetBackLink();

                //Milinda
                //div_autocomplete.InnerHtml = SetAutocomplete();

                //Remove Other Session.
                ClearSession();
                HiddenFieldIsContinueWithSave.Value = string.Empty;

                

                PageLoad();               

            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Cheque Payment Settlements ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void PageLoad()
    {
        LoadLookupData();
        LoadDistributorBankAccounts();
    }    

    
   
    #endregion

    #region Methods
    private void ClearSession()
    {
        Session[CommonUtility.CUSTOMER_DATA] = null;
    }

    protected void ButtonSave_Click(object sender)
    {
        this.SaveChequePayments();
    }
    
    public void SaveChequePayments()
    {
        CommonUtility commonUtility = new CommonUtility();
        try
        {
            bool isSucc = false;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = "";            

            List<PaymentSettlementDTO> ChequePaymentsList = new List<PaymentSettlementDTO>();
            List<PaymentSettlementDTO> UpdatedChequePaymentsList = new List<PaymentSettlementDTO>();

            if (!string.IsNullOrEmpty(HiddenFieldCheques.Value))
            {
                jsonString = @""+HiddenFieldCheques.Value.Replace("\"ExtensionData\":{},", "");
                ChequePaymentsList = serializer.Deserialize<List<PaymentSettlementDTO>>(jsonString);
            }

            foreach (PaymentSettlementDTO item in ChequePaymentsList)
            {
                //if (item.ChequeNumber.Equals("") && item.OutletBank.Equals("") && item.Amount == 0)
                //{
                //    continue;
                //}

                if (item.PaymentSettlementId == 0)
                {
                    item.CustCode = "CM010000001";//Set Customer Code ??????
                    item.IvceNo = 123;//Set Invoice Number ?????
                    item.DistributorAccountId = Convert.ToInt32(dropdownBankAccount.SelectedValue); // Set Distr Account ID

                    item.PaymentType = "CHQE";//Set type as Cheque
                    item.Status = "RETR";//Set cheque as Returned

                    item.CreatedBy = UserSession.Instance.UserName;//set Created By Name
                }
                else
                {
                    if (item.HasSelect)
                    {
                        if (item.Status.Equals("Returned"))
                            item.HasSelect = false;//to stop Seleceted But Already RETR Cheques from getting updated
                        else
                            item.Status = "RETR";
                    }
                    else
                    {
                        if (item.Status.Equals("Returned"))
                            item.Status = "RETR";
                        else
                            item.Status = "REAL";
                    }
                    item.LastModifiedBy = UserSession.Instance.UserName;//set Last modified By Name
                }

                //to filter out 
                if (item.HasSelect || item.PaymentSettlementId == 0)
                    UpdatedChequePaymentsList.Add(item);

            }

            SalesClient salesClient = new CRMServiceReference.SalesClient();

            if (UpdatedChequePaymentsList.Count != 0)
            {
                isSucc = salesClient.SavePaymentSettlements(UpdatedChequePaymentsList);
            }

            if (isSucc)
            {
                div_message.Attributes.Add("style", "display:block;padding:4px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
            }
            else
            {
                div_message.Attributes.Add("style", "display:block;padding:4px");
                div_message.InnerHtml = commonUtility.GetInfoMessage("Select Cheques to save as Returned");
            }

        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
        //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadTemplate('" + schedul_struct.CallCycleId.ToString() + "');", true);
        //ScriptManager.RegisterStartupScript(this, GetType(), "hideloader2", "hideloader();", true);
    }

    private void LoadLookupData()
    {
        try
        {
            CommonClient common_details = new CommonClient();
            List<LookupTableDTO> lookupList = new List<LookupTableDTO>();

            if (UserSession.Instance.UserName != null)
            {
                ArgsDTO args = new ArgsDTO();
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

                //Load Cheque Payment Status
                lookupList = common_details.GetLookupTables(ConfigUtil.ChequePaymentStatus, args);
                if (lookupList.Count != 0)
                {
                    dropdownChequeStatus.Items.Clear();
                    foreach (LookupTableDTO item in lookupList)
                    {
                        dropdownChequeStatus.Items.Add(new ListItem(item.TableDescription.Trim(), item.TableCode.Trim()));
                        
                        if (item.DefaultValue.Equals("Y"))
                        {
                            dropdownChequeStatus.SelectedValue = item.TableCode.Trim();
                        }
                    }
                    dropdownChequeStatus.Items.Add(new ListItem("All","All"));
                }                
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void LoadDistributorBankAccounts()
    {
        try
        {
            DistributorClient distributorClient = new DistributorClient();
            List<BankAccountDTO> lookupList = new List<BankAccountDTO>();

            if (UserSession.Instance.UserName != null)
            {
                //ArgsDTO args = new ArgsDTO();
                //args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

                string originator = UserSession.Instance.UserName;

                //Load Cheque Payment Status
                lookupList = distributorClient.GetBankAccountsForUser("DIST", originator);

                if (lookupList.Count != 0)
                {
                    dropdownBankAccount.Items.Clear();
                    foreach (BankAccountDTO item in lookupList)
                    {
                        dropdownBankAccount.Items.Add(new ListItem(item.Bank.Trim() + " / " + item.AccountNumber, item.BankAccountId.ToString()));

                        if (item.isDefault)
                        {
                            dropdownBankAccount.SelectedValue = item.BankAccountId.ToString();
                        }
                    }
                    //dropdownBankAccount.Items.Add(new ListItem("All", "All"));
                    dropdownBankAccount.Enabled = true;
                }
                else
                {
                    dropdownBankAccount.Items.Add(new ListItem("You have no saved account details", 0.ToString()));
                    dropdownBankAccount.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void SetBackLink()
    {
        if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
        {
            HiddenFieldLeadStage.Value = Request.QueryString["ty"].ToString();
        }

        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        {
            if (Request.QueryString["fm"] == "home")
            {
                hlBack.NavigateUrl = "~/default.aspx?fm=home";
            }
            else if (Request.QueryString["fm"] == "activity")
            {
                hlBack.NavigateUrl = "~/activity_planner/transaction/activity_scheduler.aspx";
            }
            else if (Request.QueryString["fm"] == "act")
            {
                hlBack.NavigateUrl = "~/calendar/transaction/appointment_entry.aspx";
            }

            else if (Request.QueryString["fm"] == "opp")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunity.aspx?cht=pipe&fm=lead";//?cht=pipe&fm=lead&pipid=1";
            }
            else if (Request.QueryString["fm"] == "oppent")
            {
                StringBuilder additionalParam = new StringBuilder("?fm=Lead");

                if (Request.QueryString["oleadid"] != null && Request.QueryString["oleadid"] != string.Empty)
                    additionalParam.Append("&leadid=" + Request.QueryString["oleadid"]);

                if (Request.QueryString["oppid"] != null && Request.QueryString["oppid"] != string.Empty)
                    additionalParam.Append("&oppid=" + Request.QueryString["oppid"]);

                if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty)
                    additionalParam.Append("&custid=" + Request.QueryString["custid"]);

                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunityentry.aspx" + additionalParam.ToString();
            }
            else if (Request.QueryString["fm"] == "lead")
            {
                if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
                {
                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/lead_contacts.aspx?ty=" + Request.QueryString["ty"] + "&fm=lead";
                }
            }
            else if (Request.QueryString["fm"] == "search")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/search.aspx";
            }

            else if (Request.QueryString["fm"] == "dsbd1")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/ActivityAnalysis.aspx";
            }
            else if (Request.QueryString["fm"] == "dsbd2")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/OpportunityAnalysis.aspx";
            }
        }
    }

    #endregion
    
}