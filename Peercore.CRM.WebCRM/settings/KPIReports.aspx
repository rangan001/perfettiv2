﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="KPIReports.aspx.cs" Inherits="settings_KPIReports" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<asp:HiddenField ID="OriginatorString" runat="server" />
	<asp:HiddenField ID="hfPageIndex" runat="server" />
	<asp:HiddenField ID="hdnSelectedDate" runat="server" ClientIDMode="Static" />
	
	<style>

        .k-grid-excel {
            float: right;
        }

        .k-grid-pdf {
            float: right;
        }

        .k-grid .k-grid-header .k-header .k-link {
            height: auto;
        }

        .k-grid .k-grid-header .k-header {
            white-space: normal;
        }
    </style>
	<style>
		.row {
			font-weight: bold;
			color: #f00;
		}

		.ddlMonth {
			color: #fff;
			font-size: 14px;
			padding: 5px 10px;
			border-radius: 0px;
			border: 0px;
			background-color: #4f51d9;
		}
	</style>
	<div id="div_main" class="divcontectmainforms">
		<div class="toolbar_container" id="id_div_toolbar_container" style="display: none;">

			<div class="toolbar_right" id="div3">
				<div class="leadentry_title_bar">
					<div style="float: right; width: 65%;" id="div_backButton">
						<asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back"></div>
						</asp:HyperLink>
					</div>
				</div>
			</div>
			<div style="margin-top: 30px;">
			</div>
		</div>

		<div id="div_message" runat="server" style="display: none;">
		</div>
		<div class="clearall">
		</div>
		<div class="demo-section k-content">
			<div id="tabstrip">
				<ul>
					<li class="k-state-active">KPI Report
					</li>
					<li>KPI Report Brands
					</li>
					<li>Email Recipients
					</li>
				</ul>
				<%-- KPI REPORT SECTION--%>
				<div>
					<div style="margin-top: 30px;">
					</div>
					<div class="formleft" style="width: 100%">
						<div class="formtextdiv">
							Target Date 
						</div>
						<div class="formdetaildiv_right" style="line-height: 20px;">
							:
						</div>
						<span class="wosub mand">&nbsp;<input type="date" class="textboxwidth" id="txtTargetDate" onchange="SetDate(this)" />
						</span><span style="color: Red">*</span>
						<div class="clearall">

						</div>
						<div class="formtextdiv">
							Target Type 
						</div>
						<div class="formdetaildiv_right" style="line-height: 20px;">
							:
						</div>
						<span class="wosub mand">&nbsp;
                            <asp:DropDownList ID="ddlTargetType" runat="server" class="input-large1" Width="200px" onchange="LoadAllKPIReportsTargetType()">
								<asp:ListItem Value="area" Text="Area Wise" Selected="True"></asp:ListItem>
								<asp:ListItem Value="week" Text="Weekly Wise"></asp:ListItem>
							</asp:DropDownList>
						</span><span style="color: Red">*</span>
						<div class="clearall">
						</div>
					</div>

					<div class="formleft" style="width: 100%" id="divAreaFile">
						<div class="toolbar_right" id="div_content">
							<div class="hoributton">
								<div style="display: none" id="id_div_buttonBar">
									<ucl:buttonbar ID="buttonbar" runat="server" />
								</div>
								<div style="float: right; width: 60%;" id="x">
									<asp:Button ID="excelUpload" runat="server" ToolTip="Upload" Text="Upload" CssClass="savebtn" OnClick="excelUpload_Click" />
								</div>
							</div>
						</div>

						<div class="formtextdiv">
							Select Excel File
						</div>
						<div class="formdetaildiv_right" style="line-height: 26px;">
							:
                    <div id="div_upload" runat="server" style="background-color: #FFFFFF; float: right; overflow: hidden;">
						<asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
						<asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
							ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red" Style="float: left;"></asp:RegularExpressionValidator>
					</div>
						</div>

						<div class="clearall">
						</div>
						<div class="formtextdiv">
						</div>
						<div class="formdetaildiv_right">
							<a href="../docs/templates/SampleKPI.xlsx">Sample Upload File</a>
						</div>
					</div>

					<div class="formleft" style="width: 100%" id="divWeekFile">
						<div class="toolbar_right" id="div_content_week">
							<div class="hoributton">
								<div style="display: none" id="id_div_buttonBar_week">
									<ucl:buttonbar ID="buttonbarWeek" runat="server" />
								</div>
								<div style="float: right; width: 60%;" id="x">
									<asp:Button ID="excelUploadWeek" runat="server" ToolTip="Upload" Text="Upload" CssClass="savebtn" OnClick="excelUploadWeek_Click" />
								</div>
							</div>
						</div>

						<div class="formtextdiv">
							Select Excel File
						</div>
						<div class="formdetaildiv_right" style="line-height: 26px;">
							:
                    <div id="div1" runat="server" style="background-color: #FFFFFF; float: right; overflow: hidden;">
						<asp:FileUpload ID="xmlUploadWeek" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
						<asp:RegularExpressionValidator ID="FileUpLoadValidatorWeek" runat="server" ErrorMessage="Please Select Excel File."
							ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUploadWeek" ForeColor="Red" Style="float: left;"></asp:RegularExpressionValidator>
					</div>
						</div>

						<div class="clearall">
						</div>
						<div class="formtextdiv">
						</div>
						<div class="formdetaildiv_right">
							<a href="../docs/templates/SampleKPIWeek.xlsx">Sample Upload File</a>
						</div>
					</div>

					<div class="toolbar_right" id="div_ddl" style="float: left;">
						<asp:DropDownList ID="ddlYear" CssClass="ddlMonth" runat="server" Width="100px" onchange="LoadAllKPIReportsTargetType()"></asp:DropDownList>
						<asp:DropDownList ID="ddlMonth" CssClass="ddlMonth" runat="server" Width="130px" onchange="LoadAllKPIReportsTargetType()"></asp:DropDownList>
					</div>

					<div class="clearall">
					</div>
					<div style="min-width: 200px; overflow: auto">
						<div style="float: left; width: 100%; overflow: auto">
							<div id="gridKPI"></div>
						</div>
					</div>
				</div>

				<%--KPI REPORT BRAND SECTION--%>
				<div>
					<div style="margin-top: 30px;">
					</div>

					<div class="formleft" style="width: 100%">
						<div class="formtextdiv">
							Selected Brand Coloum
						</div>
						<div class="formdetaildiv_right" style="line-height: 20px;">
							:
						</div>
						<span class="wosub mand">&nbsp;
                            <asp:DropDownList ID="ddlReportBrands" runat="server" class="input-large1" Width="200px">
							</asp:DropDownList>
						</span>
						<div class="clearall">
						</div>
						<div class="formtextdiv">
							Display Name
						</div>
						<div class="formdetaildiv_right" style="line-height: 20px;">
							:
						</div>
						<span class="wosub mand">&nbsp;
                            <asp:TextBox ID="txtKpiBrandDisplayName" runat="server" class="input-large1" Width="200px">
							</asp:TextBox>
						</span>
						<input type="button" ID="btnUpdateDisplayName" value="Update Display Name" style="cursor: pointer; background-color: #4f51d9; color: #fff" />

						<div class="clearall">
						</div>
						<div style="min-width: 200px; overflow: auto">
						<div style="float: left; width: 100%; overflow: auto">
							<div id="gridKPIBrandItems"></div>
						</div>
					</div>
					</div>

				</div>

				<%--Email Recipients SECTION--%>
				<div>
					<div class="formtextdiv">
						Email Recipient
					</div>
					<div class="formdetaildiv_right" style="line-height: 26px;">
						<div class="email-id-row">
							<asp:textbox id="txtEmailRecipients" runat="server" maxlength="50"  />
						</div>

						<asp:Button runat="server" ID="btnAddEmailRecipients" CssClass="k-button" OnClick="btnAddEmailRecipients_Click" Text="Add" />

						<div id="gridEmailRecipients" style="background-color:gray; height: 200px;min-width: 500px; " >
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<%-- Tab_1--%>
	<script type="text/javascript">
		var take_grid = $("#MainContent_hfPageIndex").val();

		function onDataBound() {
			var myElem = document.getElementById('trParentHeader');
			if (myElem === null) {
				$("#gridKPI").find("th.k-header").parent().before("<tr id='trParentHeader'>  <th colspan='2' class='k-header'><strong></strong></th>" +
					"<th colspan='24' class='k-header'><strong>Total Achievements</strong></th> " +
					"<th scope='col' class='k-header' colspan='24'><strong>Average Achievements</strong></th></tr>");
			}
		}

		$(document).ready(function () {
			$("#tabstrip").kendoTabStrip({
				animation: {
					open: {
						effects: "fadeIn"
					}
				}
			});

			LoadAllKPIReports();
			LoadAllEmailRecipients();

			$("#MainContent_ddlTargetType").val("area");
		});

		function LoadAllEmailRecipients() {

			$("#gridEmailRecipients").html("");
			$("#gridEmailRecipients").kendoGrid({
				columns: [
					{ field: "Email", title: "Email", width: "100px" },
					{ template: '<a href="javascript:DeleteEmailRecipient(\'#=Email#\');">Delete</a>', field: "", width: "60px" }
				],
				editable: false, // enable editing
				pageable: true,
				sortable: true,
				filterable: false,
				navigatable: true,
				dataSource: {
					serverSorting: true,
					serverPaging: true,
					serverFiltering: false,
					pageSize: 15,
					schema: {
						data: "d.Data",
						total: "d.Total",
						model: {
							fields: {
								Email: { type: "string" }
							}
						}
					},
					transport: {
						read: {
							url: ROOT_PATH + "service/settings/KPIReportService.asmx/GetAllEmailRecipients",
							contentType: "application/json; charset=utf-8",
							data: { pgindex: 1 },
							type: "POST",
							complete: function (jqXHR, textStatus) {
								if (textStatus == "success") {

									var entityGrid = $("#gridEmailRecipients").data("kendoGrid");
									if ((take_grid != '') && (gridindex == '0')) {
										entityGrid.dataSource.page((take_grid - 1));
										gridindex = '1';
									}
								}
							}
						},
						parameterMap: function (data, operation) {
							data = $.extend({ sort: null, filter: null }, data);
							return JSON.stringify(data);
						}
					}
				}
			});
		}

		function LoadAllKPIReportsTargetType() {
			var targetType = $("#<%= ddlTargetType.ClientID %>").val();

			if (targetType == "area") {
				LoadAllKPIReports();
			}
			else if (targetType == "week") {
				LoadAllKPIReportsWeekly();
			}

		}

		function DeleteEmailRecipient(email) {
			var r = confirm("Do you want to delete this Email Id?");
			if (r == true) {
				var param = { 'email': email };

				$.ajax({
					type: "POST",
					url: ROOT_PATH + "service/settings/KPIReportService.asmx/DeleteEmailRecipients",
					data: JSON.stringify(param),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response) {
						LoadAllEmailRecipients();
						alert("Successfully deleted!");
					},
					error: function (response) {
						alert("Cannot delete this email");
					}
				});
			}
		}

		function LoadAllKPIReports() {
			$('#divAreaFile').css('display', 'block');
			$('#divWeekFile').css('display', 'none');

			var month = $("#<%= ddlMonth.ClientID %>").val();
			var year = $("#<%= ddlYear.ClientID %>").val();

			$("#gridKPI").html("");
			$("#gridKPI").kendoGrid({
				toolbar: ["pdf"],
				//excel: {
				//	allPages: true,
				//	fileName: "KPI_Area_" + month + ".xlsx"
				//},
				pdf: {
					fileName: "KPI_Area_" + month + ".pdf"
				},
				columns: [
					{ field: "BatchId", hidden: true },
					{ field: "AreaId", hidden: true },
					{ field: "AreaName", title: "Area", width: "150px" },
					{ template: "#= kendo.toString(kendo.parseDate(TargetDate, 'dd/MM/yyyy'), 'dd/MM/yyyy') #", field: "TargetDate", title: "Target Date", width: "110px" },
					{ field: "TotReadyStockVal", title: "Ready Stock Value", width: "100px" },
					{ field: "TotPreInvoiceVal", title: "Pre Inv Value", width: "100px" },
					{ field: "TotReadyStockPc", title: "Ready Stock PC", width: "100px" },
					{ field: "TotPreInvoicePc", title: "Pre Inv PC", width: "100px" },
					{ field: "TotBillCount", title: "Bill Count >1500", width: "100px" },
					{ field: "TotBillValue", title: "Bill Value >1500", width: "100px" },

					{ field: "TotWhSalesBillCount", title: "Wholesale Bill Count", width: "100px" },
					{ field: "TotBrand1BillCount", title: "Brand 01 Bill Count", width: "100px" },
					{ field: "TotBrand1BillVal", title: "Brand 01 Bill Value", width: "100px" },
					{ field: "TotBrand2BillCount", title: "Brand 02 Bill Count", width: "100px" },
					{ field: "TotBrand2BillVal", title: "Brand 02 Bill Value", width: "100px" },
					{ field: "TotBrand3BillCount", title: "Brand 03 Bill Count", width: "100px" },
					{ field: "TotBrand3BillVal", title: "Brand 03  Bill Value", width: "100px" },
					{ field: "TotBrand4BillCount", title: "Brand 04 Bill Count", width: "100px" },
					{ field: "TotBrand4BillVal", title: "Brand 04 Bill Value", width: "100px" },
					{ field: "TotBrand5BillCount", title: "Brand 05 Bill Count", width: "100px" },
					{ field: "TotBrand5BillVal", title: "Brand 05 Bill Value", width: "100px" },
					{ field: "TotMultiLineBill", title: "Multi line Bill Count", width: "100px" },
					{ field: "TotReadyStockMandays", title: "Ready Stk Mandays", width: "100px" },
					{ field: "TotPreInvMandays", title: "Pre Inv Mandays", width: "100px" },
					{ field: "TotECO", title: "ECO", width: "100px" },
					{ field: "TotNoSalesAcc", title: "No Sales Account", width: "100px" },
					{ field: "TotNonVisitedOutlets", title: "Non-visit Outlets", width: "100px" },
					{ field: "TotMSSComplience", title: "MSS  Complience", width: "100px" },

					{ field: "AvgReadyStockVal", title: "Ready Stock Value", width: "100px" },
					{ field: "AvgPreInvoiceVal", title: "Pre Invoice Value", width: "100px" },
					{ field: "AvgReadyStockPc", title: "Ready Stock PC", width: "100px" },
					{ field: "AvgPreInvoicePc", title: "Pre Invoice PC", width: "100px" },
					{ field: "AvgBillCount", title: "Bill Count >1500", width: "100px" },
					{ field: "AvgBillValue", title: "Bill Value >1500", width: "100px" },
					{ field: "AvgWhSalesBillCount", title: "Wholesale Bill Count", width: "100px" },
					{ field: "AvgBrand1BillCount", title: "Brand 01 Bill Count", width: "100px" },
					{ field: "AvgBrand1BillVal", title: "Brand 01 Bill Value", width: "100px" },
					{ field: "AvgBrand2BillCount", title: "Brand 02 Bill Count", width: "100px" },
					{ field: "AvgBrand2BillVal", title: "Brand 02 Bill Value", width: "100px" },
					{ field: "AvgBrand3BillCount", title: "Brand 03 Bill Count", width: "100px" },
					{ field: "AvgBrand3BillVal", title: "Brand 03 Bill Value", width: "100px" },
					{ field: "AvgBrand4BillCount", title: "Brand 04 Bill Count", width: "100px" },
					{ field: "AvgBrand4BillVal", title: "Brand 04 Bill Value", width: "100px" },
					{ field: "AvgBrand5BillCount", title: "Brand 05 Bill Count", width: "100px" },
					{ field: "AvgBrand5BillVal", title: "Brand 05 Bill Value", width: "100px" },
					{ field: "AvgMultiLineBill", title: "Multiline Bills ", width: "100px" },
					{ field: "AvgReadyStockMandays", title: "Ready Stock Mandays", width: "100px" },
					{ field: "AvgPreInvMandays", title: "Pre Invoice Mandays", width: "100px" },
					{ field: "AvgECO", title: "ECO", width: "50px" },
					{ field: "AvgNoSalesAcc", title: "No Sales Account", width: "100px" },
					{ field: "AvgNonVisitedOutlets", title: "Non-visit Outlets", width: "100px" },
					{ field: "AvgMSSComplience", title: "MSS Complience", width: "100px" }
				],
				editable: false, // enable editing
				pageable: true,
				sortable: true,
				filterable: false,
				navigatable: true,
				editable: "popup",
				//dataBound: onDataBound,
				dataSource: {
					serverSorting: true,
					serverPaging: true,
					serverFiltering: true,
					pageSize: 15,
					batch: true,
					schema: {
						data: "d.Data",
						total: "d.Total",
						model: {
							fields: {
								BatchId: { type: "number" },
								AreaId: { type: "number" },
								AreaName: { type: "string" },
								TargetDate: { type: "date", format: "{0:dd/MM/yyyy}" },
								TotReadyStockVal: { type: "number" },
								TotPreInvoiceVal: { type: "number" },
								TotReadyStockPc: { type: "number" },
								TotPreInvoicePc: { type: "number" },
								TotBillCount: { type: "number" },
								TotBillValue: { type: "number" },
								AvgReadyStockVal: { type: "number" },
								AvgPreInvoiceVal: { type: "number" }
							}
						}
					},
					transport: {
						read: {
							url: ROOT_PATH + "service/settings/KPIReportService.asmx/GetAllKPIReportsByMonth",
							contentType: "application/json; charset=utf-8",
							data: { pgindex: take_grid, month: month, year: year },
							type: "POST",
							complete: function (jqXHR, textStatus) {
								if (textStatus == "success") {

									var entityGrid = $("#gridKPI").data("kendoGrid");
									if ((take_grid != '') && (gridindex == '0')) {
										entityGrid.dataSource.page((take_grid - 1));
										gridindex = '1';
									}
								}
							}
						},
						parameterMap: function (data, operation) {
							data = $.extend({ sort: null, filter: null }, data);
							return JSON.stringify(data);
						}
					}
				},
				dataBound: function (e) {
					// get the index of the cell
					var columns = e.sender.columns;
					var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "AreaId" + "]").index();

					// iterate the data items and apply row styles where necessary
					var dataItems = e.sender.dataSource.view();
					for (var j = 0; j < dataItems.length; j++) {
						var discontinued = dataItems[j].get("AreaId");
						console.log("discontinued " + discontinued);
						var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
						if (discontinued == 0 || discontinued == '0') {
							row.addClass("row");
						}
					}
				},

			});


			onDataBound();
		}

		function LoadAllKPIReportsWeekly() {
			$('#divAreaFile').css('display', 'none');
			$('#divWeekFile').css('display', 'block');

			var month = $("#<%= ddlMonth.ClientID %>").val();
			var year = $("#<%= ddlYear.ClientID %>").val();

			$("#gridKPI").html("");
			$("#gridKPI").kendoGrid({
				toolbar: ["pdf"],
				//excel: {
				//	allPages: true,
				//	fileName: "KPI_Weekly_" + month + ".xlsx"
				//},
				pdf: {
					fileName: "KPI_Weekly_" + month + ".pdf"
				},
				columns: [
					{ field: "BatchId", hidden: true },
                    { field: "WeekId", hidden: true },
                    { field: "WeekName", title: "Week", width: "100px" },
                    { template: "#= kendo.toString(kendo.parseDate(TargetDate, 'dd/MM/yyyy'), 'dd/MM/yyyy') #", field: "TargetDate", title: "Target Date", width: "110px" },
                    { field: "TotReadyStockVal", title: "Ready Stock Value", width: "100px" },
                    { field: "TotPreInvoiceVal", title: "Pre Inv Value", width: "100px" },
                    { field: "TotReadyStockPc", title: "Ready Stock PC", width: "100px" },
                    { field: "TotPreInvoicePc", title: "Pre Inv PC", width: "100px" },
                    { field: "TotBillCount", title: "Bill Count >1500", width: "100px" },
                    { field: "TotBillValue", title: "Bill Value >1500", width: "100px" },

                    { field: "TotWhSalesBillCount", title: "Wholesale Bill Count", width: "100px" },
                    { field: "TotBrand1BillCount", title: "Brand 01 Bill Count", width: "100px" },
                    { field: "TotBrand1BillVal", title: "Brand 01 Bill Value", width: "100px" },
                    { field: "TotBrand2BillCount", title: "Brand 02 Bill Count", width: "100px" },
                    { field: "TotBrand2BillVal", title: "Brand 02 Bill Value", width: "100px" },
                    { field: "TotBrand3BillCount", title: "Brand 03 Bill Count", width: "100px" },
                    { field: "TotBrand3BillVal", title: "Brand 03  Bill Value", width: "100px" },
                    { field: "TotBrand4BillCount", title: "Brand 04 Bill Count", width: "100px" },
                    { field: "TotBrand4BillVal", title: "Brand 04 Bill Value", width: "100px" },
                    { field: "TotBrand5BillCount", title: "Brand 05 Bill Count", width: "100px" },
                    { field: "TotBrand5BillVal", title: "Brand 05 Bill Value", width: "100px" },
                    { field: "TotMultiLineBill", title: "Multi line Bill Count", width: "100px" },
                    { field: "TotReadyStockMandays", title: "Ready Stk Mandays", width: "100px" },
                    { field: "TotPreInvMandays", title: "Pre Inv Mandays", width: "100px" },
                    { field: "TotECO", title: "ECO", width: "100px" },
                    { field: "TotNoSalesAcc", title: "No Sales Account", width: "100px" },
                    { field: "TotNonVisitedOutlets", title: "Non-visit Outlets", width: "100px" },
                    { field: "TotMSSComplience", title: "MSS  Complience", width: "100px" },

                    { field: "AvgReadyStockVal", title: "Ready Stock Value", width: "100px" },
                    { field: "AvgPreInvoiceVal", title: "Pre Invoice Value", width: "100px" },
                    { field: "AvgReadyStockPc", title: "Ready Stock PC", width: "100px" },
                    { field: "AvgPreInvoicePc", title: "Pre Invoice PC", width: "100px" },
                    { field: "AvgBillCount", title: "Bill Count >1500", width: "100px" },
                    { field: "AvgBillValue", title: "Bill Value >1500", width: "100px" },
                    { field: "AvgWhSalesBillCount", title: "Wholesale Bill Count", width: "100px" },
                    { field: "AvgBrand1BillCount", title: "Brand 01 Bill Count", width: "100px" },
                    { field: "AvgBrand1BillVal", title: "Brand 01 Bill Value", width: "100px" },
                    { field: "AvgBrand2BillCount", title: "Brand 02 Bill Count", width: "100px" },
                    { field: "AvgBrand2BillVal", title: "Brand 02 Bill Value", width: "100px" },
                    { field: "AvgBrand3BillCount", title: "Brand 03 Bill Count", width: "100px" },
                    { field: "AvgBrand3BillVal", title: "Brand 03 Bill Value", width: "100px" },
                    { field: "AvgBrand4BillCount", title: "Brand 04 Bill Count", width: "100px" },
                    { field: "AvgBrand4BillVal", title: "Brand 04 Bill Value", width: "100px" },
                    { field: "AvgBrand5BillCount", title: "Brand 05 Bill Count", width: "100px" },
                    { field: "AvgBrand5BillVal", title: "Brand 05 Bill Value", width: "100px" },
                    { field: "AvgMultiLineBill", title: "Multiline Bills ", width: "100px" },
                    { field: "AvgReadyStockMandays", title: "Ready Stock Mandays", width: "100px" },
                    { field: "AvgPreInvMandays", title: "Pre Invoice Mandays", width: "100px" },
                    { field: "AvgECO", title: "ECO", width: "50px" },
                    { field: "AvgNoSalesAcc", title: "No Sales Account", width: "100px" },
                    { field: "AvgNonVisitedOutlets", title: "Non-visit Outlets", width: "100px" },
                    { field: "AvgMSSComplience", title: "MSS Complience", width: "100px" }
				],
				editable: false, // enable editing
				pageable: true,
				sortable: true,
				filterable: false,
				navigatable: true,
				editable: "popup",
				//dataBound: onDataBound,
				dataSource: {
					serverSorting: true,
					serverPaging: true,
					serverFiltering: true,
					pageSize: 15,
					batch: true,
					schema: {
						data: "d.Data",
						total: "d.Total",
						model: {
							fields: {
								BatchId: { type: "number" },
								WeekId: { type: "number" },
								WeekName: { type: "string" },
								TargetDate: { type: "date", format: "{0:dd/MM/yyyy}" },
								TotReadyStockVal: { type: "number" },
								TotPreInvoiceVal: { type: "number" },
								TotReadyStockPc: { type: "number" },
								TotPreInvoicePc: { type: "number" },
								TotBillCount: { type: "number" },
								TotBillValue: { type: "number" },
								AvgReadyStockVal: { type: "number" },
								AvgPreInvoiceVal: { type: "number" }
							}
						}
					},
					transport: {
						read: {
							url: ROOT_PATH + "service/settings/KPIReportService.asmx/GetAllKPIReportsWeeklyTargetByMonth",
							contentType: "application/json; charset=utf-8",
							data: { pgindex: take_grid, month: month, year: year },
							type: "POST",
							complete: function (jqXHR, textStatus) {
								if (textStatus == "success") {

									var entityGrid = $("#gridKPI").data("kendoGrid");
									if ((take_grid != '') && (gridindex == '0')) {
										entityGrid.dataSource.page((take_grid - 1));
										gridindex = '1';
									}
								}
							}
						},
						parameterMap: function (data, operation) {
							data = $.extend({ sort: null, filter: null }, data);
							return JSON.stringify(data);
						}
					}
				},
				dataBound: function (e) {
					// get the index of the cell
					var columns = e.sender.columns;
					var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "WeekId" + "]").index();

					// iterate the data items and apply row styles where necessary
					var dataItems = e.sender.dataSource.view();
					for (var j = 0; j < dataItems.length; j++) {
						var discontinued = dataItems[j].get("WeekId");
						console.log("discontinued " + discontinued);
						var row = e.sender.tbody.find("[data-uid='" + dataItems[j].uid + "']");
						if (discontinued == 0 || discontinued == '0') {
							row.addClass("row");
						}
					}
				},
			});

			onDataBound();
		}


		function SetDate(txtTargetDate) {
			var targetDt = document.getElementById('txtTargetDate').value;
			$("#<%= hdnSelectedDate.ClientID %>").val(targetDt);

			console.log(targetDt);
		}

	</script>

	<%--Tab_2--%>
	<script type="text/javascript">

		var KpiBrandId = 0;

		$(document).ready(function () {
			$("#MainContent_ddlReportBrands").val(0);
			LoadKPIBrandItems(0);
			GetKPIReportBrandByBrandId(0);
		});

		$("#MainContent_ddlReportBrands").change(function () {
			KpiBrandId = this.value;

			LoadKPIBrandItems(this.value);
			GetKPIReportBrandByBrandId(this.value);
		});

		function LoadKPIBrandItems(kpiBrandId) {
			$("#gridKPIBrandItems").html("");
			$("#gridKPIBrandItems").kendoGrid({
				columns: [
					{ field: "KpiBrandId", hidden: true },
					{ field: "ProductId", hidden: true },
					{ field: "ProductFgsCode", title: "FGS Code", width: "80px" },
					{ field: "ProductName", title: "Product Name", width: "220px" },
					{ field: "BrandName", title: "Brand", width: "110px" },
					{ field: "CategoryName", title: "Category", width: "110px" },
					{ field: "FlavourName", title: "Flavor", width: "110px" },
					{ field: "PackingName", title: "Packaging", width: "110px" },
					{ field: "IsHighValue", title: "High Value Product", width: "80px" },
					{
						field: "IsAssigned",
						template: "<input type=\"checkbox\" #= IsAssigned ? checked='checked' : '' # class=\"option-input checkbox check_rowPT\" onclick='KpiBrandItemClick(#= KpiBrandId #, #= ProductId #, this);' />",
						//headerTemplate: '<input type=\"checkbox\" class=\"option-input checkbox checkAllPT\" />',
						width: "100px",
						sortable: false,
						filterable: false
					}
				],
				editable: false,
				pageable: true,
				sortable: true,
				filterable: true,
				persistSelection: true,
				selectable: "row",
				dataSource: {
					serverSorting: true,
					serverPaging: true,
					serverFiltering: true,
					pageSize: 15,
					batch: true,
					schema: {
						data: "d.Data",
						total: "d.Total",
						model: {
							fields: {
								KpiBrandId: { type: "number" },
								ProductId: { type: "number" },
								ProductFgsCode: { type: "string" },
								ProductCode: { type: "string" },
								ProductName: { type: "string" },
								BrandId: { type: "number" },
								BrandName: { type: "string" },
								CategoryId: { type: "number" },
								CategoryName: { type: "string" },
								FlavourId: { type: "number" },
								FlavourName: { type: "string" },
								PackingId: { type: "number" },
								PackingName: { type: "string" },
								IsHighValue: { type: "boolean" },
								IsAssigned: { type: "boolean" },
								PackingId: { type: "number" },
							}
						}
					},
					transport: {
						read: {
							url: ROOT_PATH + "service/settings/KPIReportService.asmx/GetKPIBrandItemsByBrandId",
							contentType: "application/json; charset=utf-8",
							data: { pgindex: take_grid, kpiBrandId: kpiBrandId },
							type: "POST",
							complete: function (jqXHR, textStatus) {
								if (textStatus == "success") {
									var entityGrid = $("#gridKPIBrandItems").data("kendoGrid");
									if ((take_grid != '') && (gridindex == '0')) {
										entityGrid.dataSource.page((take_grid - 1));
										gridindex = '1';
									}
								}
							}
						},
						parameterMap: function (data, operation) {
							if (operation != "read") {
								return JSON.stringify({ products: data.models })
							}
							else {
								data = $.extend({ sort: null, filter: null }, data);
								return JSON.stringify(data);
							}
						}
					}
				}
			});
		}

		function KpiBrandItemClick(kpibrandId, prodId, cb) {
			var originator = $('#MainContent_OriginatorString').val();

			AssignKPIBrandItems(originator, kpibrandId, prodId, cb.checked);
		}

		function AssignKPIBrandItems(Originator, KpiBrandId, ProductId, IsCheced) {

			var url = ROOT_PATH + "service/settings/kpireportservice.asmx/AssignKPIBrandItems"

			if (KpiBrandId == "0") {
				return;
			}

			$.ajax({
				type: "POST",
				url: url,
				data: '{ originator: "' + Originator + '", ' +
					' kpi_brand_id: "' + KpiBrandId + '", ' +
					' product_id: "' + ProductId + '", ' +
					' is_checked: "' + IsCheced + '" }',
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (msg) {
					if (msg.d.Status == false) {
						var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

						$('#MainContent_div_message').css('display', 'block');
						$("#MainContent_div_message").html(errorMsg);
						//closepopup();
						hideStatusDiv("MainContent_div_message");
					}
				},
				error: function (msg, textStatus) {
					if (textStatus == "error") {
						var msg = msg;
					}
				}
			});
		}

		function GetKPIReportBrandByBrandId(KpiBrandId) {

			var url = ROOT_PATH + "service/settings/kpireportservice.asmx/GetKPIReportBrandByBrandId"

			if (KpiBrandId == "0") {
				$("#MainContent_txtKpiBrandDisplayName").val("");
				return;
			}

			$.ajax({
				type: "POST",
				url: url,
				data: '{ kpi_brand_id: "' + KpiBrandId + '" }',
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (msg) {
					$("#MainContent_txtKpiBrandDisplayName").val(msg.d.BrandDisplayName);
					//if (msg.d.Status == false) {
					//	var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

					//	$('#MainContent_div_message').css('display', 'block');
					//	$("#MainContent_div_message").html(errorMsg);
					//	//closepopup();
					//	hideStatusDiv("MainContent_div_message");
					//}
				},
				error: function (msg, textStatus) {
					if (textStatus == "error") {
						var msg = msg;
					}
				}
			});
		}

		$("#btnUpdateDisplayName").click(function () {
			var displayName = $("#MainContent_txtKpiBrandDisplayName").val();

			var url = ROOT_PATH + "service/settings/kpireportservice.asmx/UpdateKPIBrandDisplayName"

			if (KpiBrandId == "0") {
				return;
			}

			$.ajax({
				type: "POST",
				url: url,
				data: '{ kpi_brand_id: "' + KpiBrandId + '", ' +
					' displayName: "' + displayName + '" }',
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (msg) {
					if (msg.d.Status == false) {
						var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

						$('#MainContent_div_message').css('display', 'block');
						$("#MainContent_div_message").html(errorMsg);
						//closepopup();
						hideStatusDiv("MainContent_div_message");
					}
					else {
						alert("Successfully updated!");
					}
				},
				error: function (msg, textStatus) {
					if (textStatus == "error") {
						var msg = msg;
					}
				}
			});
		});

	</script>
</asp:Content>


