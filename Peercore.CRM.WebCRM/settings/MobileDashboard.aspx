﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MobileDashboard.aspx.cs" Inherits="settings_MobileDashboard" %>

<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }

        #tabmenu {
            margin: 20px 0 0 20px;
        }

        li, p {
            font: 12px/16px Arial, Helvetica, sans-serif;
        }

        #nav {
            overflow: hidden;
            padding-left: 0;
        }

            #nav li {
                float: left;
                list-style: none;
                background: #EEE;
            }

                #nav li a {
                    padding: 10px;
                    border-top: 1px solid #CCC;
                    border-right: 1px solid #CCC;
                    display: block;
                    background: #CCC;
                }

                #nav li:first-child a {
                    border-left: 1px solid #CCC;
                }

        #tab-content {
            border: 0px solid #CCC;
            padding: 10px;
            width: 800px;
            margin-top: -1px;
            -moz-border-radius: 0 0 5px 5px;
        }

        #nav li a.active {
            background: #FFF;
            margin-bottom: -3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="tabmenu">

        <ul id="nav">
            <li><a href="#" class="active">SR Dashboard</a></li>
           <li><a href="#">Tab 2</a></li>
         <%--<li><a href="#">Tab 3</a></li>
        <li><a href="#">Tab 4</a></li>--%>
        </ul>

        <div id="tab-content">

            <div id="tab1">
                <table border="0">
                    <tbody>
                        <tr>
                            <td class="textalignbottom">Invoice Value > X(<span style="color: #f80000;">X</span>) 
                            </td>
                            <td colspan="3">
                                <span class="wosub mand">&nbsp;<input type="text" runat="server" class="txtBillValue" id="txtBillVal1300"
                                    style="" />
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4" class="textalignbottom">
                                <button class="k-button" id="buttonSave1300" type="button" onclick="return buttonSave1300_onclick()">
                                    Save</button>
                                <asp:HiddenField ID="hdnSelectedFlavorId" runat="server" ClientIDMode="Static" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="tab2">
            <p>This can contain anything.</p>
        </div>
        
       <%-- <div id="tab3">
            <p>Like photos:</p><br />
            <img src="http://www.catname.org/images/cat04.jpg" alt=""/>
        </div>
        
        <div id="tab4">
            <p>Or videos:</p><br />
            <iframe width="250" height="180" src="http://www.youtube.com/embed/TZ860P4iTaM" frameborder="0" allowfullscreen></iframe>
        </div>--%>
        </div>

    </div>
    <script type="text/javascript">
        $('#tab-content div').hide();
        $('#tab-content div:first').show();

        $('#nav li').click(function () {
            $('#nav li a').removeClass("active");
            $(this).find('a').addClass("active");
            $('#tab-content div').hide();

            var indexer = $(this).index(); //gets the current index of (this) which is #nav li
            $('#tab-content div:eq(' + indexer + ')').fadeIn(); //uses whatever index the link has to open the corresponding box 
        });
    </script>

    <script type="text/javascript">

        $('.txtBillValue').keypress(function (event) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && (event.which != 8)) {
                event.preventDefault();
            }
        });

        function buttonSave1300_onclick() {
            var valBillVal1300 = $('#MainContent_txtBillVal1300').val();

            var param = { "BillVal1300": valBillVal1300 };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/SaveMobileRepDashboard",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    alert('Successfully Updated.');
                },
                error: function (response) {
                    alert('Error.');
                }
            });
        }
    </script>
</asp:Content>

