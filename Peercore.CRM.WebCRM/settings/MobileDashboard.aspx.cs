﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class settings_MobileDashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
            }

            try
            {
                CommonClient client = new CommonClient();
                MobileRepsDashboardModel mobileDashboard = new MobileRepsDashboardModel();

                mobileDashboard = client.LoadMobileRepsDashboard();

                txtBillVal1300.Value = mobileDashboard.BillVal1300.ToString();
            }
            catch { }

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Mobile Dashboard ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }
}