﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerSalesinfo.ascx.cs" Inherits="usercontrols_CustomerSalesinfo" %>

<div>
    <ul id="panelbarcontent">
        <li class="k-state-active" id="1">Grids
            <div id="gridCustomerSales">
            <%--<div style="float: left; width: 100%; overflow: auto">
                    <div id="gridEndUserSales">
                    </div>
                </div>--%>                
            </div>
<%--                      <div id="message_grid">
                                </div>--%>
            <div style="margin-top:20px;height:5px; display:none;">&nbsp;</div>
        </li>

        <li id="2" >Today’s sales
            <div>
                <asp:RadioButton GroupName="group" ID="RadioButtonCMP" runat="server" Text="&nbsp;CMP"/>                
                <asp:RadioButton  ID="RadioButtonAll" GroupName="group" runat="server" Text="&nbsp;All" Checked="true"  />                 
                <div class="clearall"></div>  
            </div>
            <div id="gridDailySales">               
            </div>
            <div style="margin-top:20px;height:5px; display:none;">&nbsp;</div>
        </li>

        <%--<li id="2">Graph
            <div id="div_trend">
                
            </div>
        </li>--%>
        <li id="3" hidden>Trend Graph
            <div>
                <asp:DropDownList ID="DropDownGraphType" runat="server" Width="200px" CssClass="dropdownwidth select-different">
                    <asp:ListItem Value="Actual" Text="Actual" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Target" Text="Target"></asp:ListItem>
                    <asp:ListItem Value="Target/Actual" Text="Target/Actual" ></asp:ListItem>
                </asp:DropDownList>   
                
                <asp:RadioButton runat="server" ID="rbTrendGraphAccumulate" ValidationGroup="A" GroupName="A" Checked="true" /><span>Accumulated</span>
                <asp:RadioButton runat="server" ID="rbTrendGraphMonthly" ValidationGroup="A" GroupName="A" /><span>Monthly</span>
                             
            </div>
            <div id="div_chart">
                
            </div>
        </li>
    </ul>
</div>
