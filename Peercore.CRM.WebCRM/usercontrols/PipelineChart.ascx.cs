﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using CRMServiceReference;
using Peercore.CRM.Shared;
//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Common;

public partial class usercontrols_PipelineChart : System.Web.UI.UserControl
{
    
    #region Constant
   
    #endregion Constant

    #region Properties

    private KeyValuePair<string, string> PipelineChart
    {
        get
        {
            if (Session[CommonUtility.PIPELINE_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.PIPELINE_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.PIPELINE_DATA] = value;
        }
    }

    private string _showLable = string.Empty;
    public string ShowLable
    {
        get
        {
            return _showLable;
        }
        set
        {
            _showLable = value;
        }
    }
    #endregion Properties

    #region - Events -
    protected void Page_Load(object sender, System.EventArgs e)
    {
        
        //if (!IsPostBack)
        //{
            if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
            {
                if (Session[CommonUtility.PIPELINE_DATA] == null)
                    PipelineChart = new KeyValuePair<string, string>("0", CommonUtility.PIPELINE_DATA);
                //if (Session[CommonUtility.SELECTED_CUSTOMER] != null)
                //    rblCustomer.SelectedIndex = int.Parse(Session[CommonUtility.SELECTED_CUSTOMER].ToString());
                //else
                //    rblCustomer.SelectedIndex = 0;
                
                //rblCustomer_SelectedIndexChanged(this, e);
                LoadFunnelChart(false);
            }
        //}
        //else
        //{
            
        //    //rblCustomer_SelectedIndexChanged(this, e);
        //}
    }

    protected void ChartPipeline_Click(object sender, ImageMapEventArgs e)
    {
        //Session[CommonUtility.PIPELINE_HEADER] = null;
        //SetShowLable(e.PostBackValue);
        //Session[SELECTED_CUSTOMER] = rblCustomer.SelectedIndex.ToString();
        string[] split = e.PostBackValue.Split(new Char[] { '/' });
        PipelineChart = new KeyValuePair<string, string>(split[0], CommonUtility.PIPELINE_DATA);
        //Session[CommonUtility.PIPELINE_HEADER] = split[1].ToString() + "/" + split[2].ToString();
        Response.Redirect("~/pipelines_stage/transaction/opportunity.aspx?cht=pipe&pipid=" + split[0] + "&pipheader=" + split[1]);
    }

    protected void rblCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblCustomer.SelectedIndex == 0)
        {
            LoadFunnelChart(false);
            Session[CommonUtility.SELECTED_CUSTOMER] = "0";
        }
        else
        {
            LoadFunnelChart(true);
            Session[CommonUtility.SELECTED_CUSTOMER] = "1";
        }

        if (sender != this)
        {
            Session[CommonUtility.PIPELINE_HEADER] = null;
            Session[CommonUtility.SELECTED_CUSTOMER] = rblCustomer.SelectedIndex.ToString();
            PipelineChart = new KeyValuePair<string, string>("0", CommonUtility.PIPELINE_DATA);
            Response.Redirect("~/pipelines_stage/transaction/opportunity.aspx?cht=pipe");
        }
    }

    private void LoadFunnelChart(bool isCustomer)
    {
        ArgsDTO args = new ArgsDTO();

        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.Originator = string.IsNullOrEmpty(UserSession.Instance.FilterByUserName) ?
            UserSession.Instance.UserName : UserSession.Instance.FilterByUserName;

        
        PipelineStageClient pipelineStageClient = new PipelineStageClient();

        List<PipelineStageDTO> PipelineStageGraphList = pipelineStageClient.GetPipelineChart(args);

        // Set funnel style
        ChartPipeline.Series["Default"]["FunnelStyle"] = "YIsHeight";

        // Set funnel data point labels style
        ChartPipeline.Series["Default"]["FunnelLabelStyle"] = "OutsideInColumn";

        // Set labels placement
        ChartPipeline.Series["Default"]["FunnelOutsideLabelPlacement"] = "OutsideInColumn";

        // Set gap between points
        ChartPipeline.Series["Default"]["FunnelPointGap"] = "2";

        // Set minimum point height
        ChartPipeline.Series["Default"]["FunnelMinPointHeight"] = "0";

        // Set 3D mode
        ChartPipeline.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

        // Set 3D angle
        ChartPipeline.Series["Default"]["Funnel3DRotationAngle"] = "5";

        // Set 3D drawing style
        ChartPipeline.Series["Default"]["Funnel3DDrawingStyle"] = "CircularBase";

        ChartPipeline.Series["Default"]["FunnelOutsideLabelPlacement"] = "Right";

        ChartPipeline.Series["Default"]["FunnelNeckHeight"] = "0";

        ChartPipeline.Series[0].Points.Clear();
        foreach (PipelineStageDTO item in PipelineStageGraphList)
        {
            DataPoint dataPoint = new DataPoint(0, item.TotalAmount);
            //dataPoint.ToolTip = item.PipelineStage;
            dataPoint.LegendText = item.PipelineStageName;
            dataPoint.LegendPostBackValue = item.PipelineStageID.ToString() + "/" + item.PipelineStageName;// +"/" + rblCustomer.SelectedIndex; 
            
            //dataPoint.LegendToolTip = item.PipelineStage;
            dataPoint.Label = item.PipelineStageName;
            dataPoint.PostBackValue = item.PipelineStageID.ToString() + "/" + item.PipelineStageName;// +"/" + rblCustomer.SelectedIndex;
            dataPoint.Color = GetColor(item.PipelineStageID.Value);
            ChartPipeline.Series[0].Points.Add(dataPoint);

            //if (isCustomer)
            dataPoint.ToolTip = item.PipelineStageName + ", " + item.TotalAmount;// +"\n No. of Customer: " + item.Code;
            //else
            //    dataPoint.ToolTip = item.PipelineStageName + ", Litres : " + item.TotalUnits + "\n No. of Prospect: " + item.Code;

        }
    }
    #endregion - Events -

    #region Methods
    public void SetShowLable(string selectedLable)
    {
        ShowLable = selectedLable;
    }

    public System.Drawing.Color GetColor(int id)
    {
        System.Drawing.Color colorName = System.Drawing.Color.FromArgb(52, 104, 167);
        switch (id)
        {
            case 1:
                colorName = System.Drawing.Color.FromArgb(52, 104, 167);
                //color = "#3468a7";
                break;
            case 2:
                colorName = System.Drawing.Color.FromArgb(219, 132, 39);
                //color = "#db8427";
                break;
            case 7:
                colorName = System.Drawing.Color.FromArgb(184, 57, 53);
                //color = "#b83935";
                break;
            case 8:
                colorName = System.Drawing.Color.FromArgb(135, 170, 64);
                //color = "#87aa40";
                break;
            case 9:
                colorName = System.Drawing.Color.FromArgb(109, 79, 146);
                //color = "#6d4f92";
                break;
        }
        return colorName;
    }

    #endregion
     

}