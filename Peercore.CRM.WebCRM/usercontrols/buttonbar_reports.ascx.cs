﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;

public partial class usercontrols_buttonbar_reports : System.Web.UI.UserControl
{
    #region Delegate
    public delegate void ButtonFilter(object sender);
    public delegate void ButtonExport(object sender);
    public delegate void DropDownListReps(object sender);
    public delegate void DropDownListDistributors(object sender);
    public delegate void DropDownListAse(object sender);
    public delegate void DropDownListShowReportFor(object sender);
    public delegate void DropDownListShowReportForASMDB(object sender);
    public delegate void DropDownListOtherReports(object sender);
    public delegate void DropDownListSFAOptionsProduct1(object sender);
    public delegate void DropDownListAssetReports(object sender);

    public delegate void DropDownListShowReport(object sender);
    public delegate void DropDownListDivisions(object sender);

    public delegate void DropDownListRepSalesItems(object sender);
    public delegate void DropDownListRepSalesItemsBrand(object sender);

    public delegate void DropDownListTerritory(object sender);

    public delegate void DropDownListOrderReportType(object sender);
    
    public delegate void DropDownListQuantityType(object sender);

    public delegate void DropDownListDBActiveInactive(object sender);

    #endregion

    #region Attribute
    public ButtonFilter onButtonFilter;
    public ButtonExport onButtonExport;
    public DropDownListReps onDropDownListReps;
    public DropDownListDistributors onDropDownListDistributors;
    public DropDownListAse onDropDownListAse;
    public DropDownListShowReportFor onDropDownListShowReportFor;
    public DropDownListShowReportForASMDB onDropDownListShowReportForASMDB;
    public DropDownListOtherReports onDropDownListOtherReports;
    public DropDownListSFAOptionsProduct1 onDropDownListSFAOptionsProduct1;
    public DropDownListAssetReports onDropDownListAssetReports;

    public DropDownListShowReportFor onDropDownListShowCustIdleDays;
    public DropDownListShowReport onDropDownListShowReport;

    public DropDownListDivisions onDropDownListDivisions;

    public DropDownListRepSalesItems onDropDownListRepSalesItems;
    public DropDownListRepSalesItemsBrand onDropDownListRepSalesItemsBrand;

    public DropDownListTerritory onDropDownListTerritory;

    public DropDownListOrderReportType onDropDownListOrderReportType;
    
    public DropDownListQuantityType onDropDownListQuantityType;

    public DropDownListDBActiveInactive onDropDownListDBActiveInactive;

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DateTime today = DateTime.Today;
            dtpFromDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
            dtpToDate.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        onButtonFilter(this);

    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        onButtonExport(this);
    }

    protected void cmbReps_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListReps("dropdown");
        }
        catch (Exception ex)
        {
        }
    }

    protected void cmbDistributors_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListDistributors("dropdown");
        }
        catch (Exception ex)
        {
        }
    }

    protected void cmbAse_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListAse("dropdown");
        }
        catch (Exception ex)
        {
        }
    }

    protected void cmbShowReportFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListShowReportFor("dropdown");
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlDBActiveInactive_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListDBActiveInactive("dropdown");
        }
        catch (Exception ex)
        {
        }
    }

    protected void cmbShowReportForASMDB_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListShowReportForASMDB("dropdown");
        }
        catch (Exception ex)
        {

        }
    }

    #endregion

    #region Method

    public void SetReps(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        cmbReps.DataSource = listOriginator;
        cmbReps.DataTextField = "Name";
        cmbReps.DataValueField = valueField;
        cmbReps.DataBind();
    }

    public void SetDistributors(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        cmbDistributors.DataSource = listOriginator;
        cmbDistributors.DataTextField = "Name";
        cmbDistributors.DataValueField = valueField;
        cmbDistributors.DataBind();
    }

    public void SetProducts(List<ProductDTO> listProduct, string valueField = "ProductId")
    {
        cmpProduct.DataSource = listProduct;
        cmpProduct.DataTextField = "Name";
        cmpProduct.DataValueField = valueField;
        cmpProduct.DataBind();
    }

    public void SetAses(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        cmbAse.DataSource = listOriginator;
        cmbAse.DataTextField = "Name";
        cmbAse.DataValueField = valueField;
        cmbAse.DataBind();
    }

    public void SetDivisions()
    {
        ArgsDTO args = new ArgsDTO();
        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        args.OrderBy = " division_name asc ";
        args.AdditionalParams = "";
        args.StartIndex = 1;
        args.RowCount = 100;

        List<DivisionDTO> divisionList = discountSchemeClient.GetAllDivisions(args);

        if (divisionList.Count > 0)
            divisionList.Insert(0, new DivisionDTO() { DivisionName = "ALL", DivisionId = -1 });

        cmbDivisions.DataSource = divisionList;
        cmbDivisions.DataTextField = "DivisionName";
        cmbDivisions.DataValueField = "DivisionId";
        cmbDivisions.DataBind();

    }

    public void SetTerritories(List<TerritoryModel> listTerritory, string valueField = "TerritoryId", string textField = "TerritoryName")
    {
        cmbTerritory.DataSource = listTerritory;
        cmbTerritory.DataTextField = textField;
        cmbTerritory.DataValueField = valueField;
        cmbTerritory.DataBind();
    }

    //public void SetItemBrand(List<BrandDTO> listBrands, string valueField = "BrandId")
    //{
    //    cmbAllFilterBy.DataSource = listBrands;
    //    cmbAllFilterBy.DataTextField = "BrandName";
    //    cmbAllFilterBy.DataValueField = valueField;
    //    cmbAllFilterBy.DataBind();
    //}

    public void SetSFAInvoiceSummaryReportSalesRep(List<OriginatorDTO> listOriginator)
    {
        ddlSFAInvoiceSalesRep.DataSource = listOriginator;
        ddlSFAInvoiceSalesRep.DataTextField = "Name";
        ddlSFAInvoiceSalesRep.DataValueField = "UserName";
        ddlSFAInvoiceSalesRep.DataBind();
    }

    public void SetSFAInvoiceSummaryReportProducts(List<ProductDTO> listProducts)
    {
        ddlSFAInvoiceProduct1.DataSource = listProducts;
        ddlSFAInvoiceProduct1.DataTextField = "Code";
        ddlSFAInvoiceProduct1.DataValueField = "Code";
        ddlSFAInvoiceProduct1.DataBind();

        ddlSFAInvoiceProduct2.DataSource = listProducts;
        ddlSFAInvoiceProduct2.DataTextField = "Code";
        ddlSFAInvoiceProduct2.DataValueField = "Code";
        ddlSFAInvoiceProduct2.DataBind();
    }

    public void SetRepsSelect(string userName)
    {
        cmbReps.SelectedValue = userName;
    }

    public void SetDistributorsSelect(string userName)
    {
        cmbDistributors.SelectedValue = userName;
    }

    public void SetAsesSelect(string userName)
    {
        cmbAse.SelectedValue = userName;
    }

    public void SetTradeloadRepTypeSelect(string userName)
    {
        ddlTradeLoadRepType.SelectedValue = userName;
    }

    public void SetReportForSelect(string userName)
    {
        cmbShowReportFor.SelectedValue = userName;
    }
    
    public void SetReportForSelectASMDB(string userName)
    {
        cmbShowReportForASMDB.SelectedValue = userName;
    }

    public string GetRepsValue()
    {
        if (cmbReps.SelectedItem != null)
            return cmbReps.SelectedItem.Value;
        else
            return "";
    }

    public string GetRepsText()
    {
        if (cmbReps.SelectedItem != null)
            return cmbReps.SelectedItem.Text;
        else
            return "";
    }

    public string GetOrderReportType()
    {
        if (cmbOrderReportType.SelectedItem != null)
            return cmbOrderReportType.SelectedItem.Value;
        else
            return "";
    } 
    
    public string GetQuantityType()
    {
        if (cmbQuantityType.SelectedItem != null)
            return cmbQuantityType.SelectedItem.Value;
        else
            return "";
    }

    public string GetDistributorsValue()
    {
        if (cmbDistributors.SelectedItem != null)
            return cmbDistributors.SelectedItem.Value;
        else
            return "";
    }

    public string GetDivisionValue()
    {
        if (cmbDivisions.SelectedItem != null)
            return cmbDivisions.SelectedItem.Value;
        else
            return "";
    }

    public string GetTerritoryValue()
    {
        if (cmbTerritory.SelectedItem != null)
            return cmbTerritory.SelectedItem.Value;
        else
            return "";
    }

    public string GetProductValue()
    {
        if (cmpProduct.SelectedItem != null)
            return cmpProduct.SelectedItem.Value;
        else
            return "";
    }

    public string GetDistributorsText()
    {
        if (cmbDistributors.SelectedItem != null)
            return cmbDistributors.SelectedItem.Text;
        else
            return "";
    }

    public string GetAsesValue()
    {
        if (cmbAse.SelectedItem != null)
            return cmbAse.SelectedItem.Value;
        else
            return "";
    }

    public string GetDBActiveInactiveValue()
    {
        if (ddlDBActiveInactive.SelectedItem != null)
            return ddlDBActiveInactive.SelectedItem.Value;
        else
            return "";
    }

    public string GetRepValue()
    {
        if (cmbReps.SelectedItem != null)
            return cmbReps.SelectedItem.Value;
        else
            return "";
    }

    public string GetAsesText()
    {
        if (cmbAse.SelectedItem != null)
            return cmbAse.SelectedItem.Text;
        else
            return "";
    }

    public string GetShowReprtForValue()
    {
        if (cmbShowReportFor.SelectedItem != null)
            return cmbShowReportFor.SelectedItem.Value;
        else
            return "";
    }

    public string GetShowReprtForASMDBValue()
    {
        if (cmbShowReportForASMDB.SelectedItem != null)
            return cmbShowReportForASMDB.SelectedItem.Value;
        else
            return "";
    }

    public string GetVisitNoteValue()
    {
        if (cmbVisitNote.SelectedItem != null)
            return cmbVisitNote.SelectedItem.Value;
        else
            return "";
    }

    public string GetReportTypeQtyValue()
    {
        if (cmbReportTypeQtyValue.SelectedItem != null)
            return cmbReportTypeQtyValue.SelectedItem.Value;
        else
            return "q";
    }

    public string GetReportTypeValue()
    {
        if (cmbReportType.SelectedItem != null)
            return cmbReportType.SelectedItem.Value;
        else
            return "q";
    }

    public string GetReportTypeRepsItemSalesValue()
    {
        if (cmb_RepsItemSalesReportType.SelectedItem != null)
            return cmb_RepsItemSalesReportType.SelectedItem.Value;
        else
            return "q";
    }

    public string GetReportTypeRepsItemSalesBrandValue()
    {
        if (cmb_RepsItemSalesBrandReportType.SelectedItem != null)
            return cmb_RepsItemSalesBrandReportType.SelectedItem.Value;
        else
            return "q";
    }

    public string GetReportTypeTradeLoadSummary()
    {
        if (ddlTradeLoadRepType.SelectedItem != null)
            return ddlTradeLoadRepType.SelectedItem.Value;
        else
            return "p";
    }

    public string GetReportTypeUploadSummary()
    {
        if (ddlUploadSummaryRepType.SelectedItem != null)
            return ddlUploadSummaryRepType.SelectedItem.Value;
        else
            return "su";
    }

    public string GetReportTypeQtyValueText()
    {
        if (cmbReportTypeQtyValue.SelectedItem != null)
            return cmbReportTypeQtyValue.SelectedItem.Text;
        else
            return "QTY";
    }

    public string GetReportTypeText()
    {
        if (cmbReportType.SelectedItem != null)
            return cmbReportType.SelectedItem.Text;
        else
            return "QTY";
    }

    public ListItemCollection GetAllRepsInDropDown()
    {
        ListItemCollection repsList = new ListItemCollection();
        repsList = cmbReps.Items;
        return repsList;
    }

    public ListItemCollection GetAllDistributorsInDropDown()
    {
        ListItemCollection distList = new ListItemCollection();
        distList = cmbDistributors.Items;
        return distList;
    }

    public ListItemCollection GetAllASEsInDropDown()
    {
        ListItemCollection aseList = new ListItemCollection();
        aseList = cmbAse.Items;
        return aseList;
    }

    public void SetFromDateText(string fromDate)
    {
        dtpFromDate.Text = fromDate;
    }

    public void SetToDateText(string toDate)
    {
        dtpToDate.Text = toDate;
    }

    public string GetFromDateText()
    {
        return dtpFromDate.Text;
    }

    public string GetMonth()
    {
        return dtpMonth.Text;
    }

    public string GetToDateText()
    {
        return dtpToDate.Text;
    }

    public string GetOtherReportName()
    {
        return ddlOtherReport.SelectedValue;
    }

    public string GetAssetsReportName()
    {
        return ddlAssetRegistery.SelectedValue;
    }


    public void SetVisible(int rptId)
    {
        switch (rptId)
        {
            case 1:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = true;
                div_distributor.Visible = true;
                div_VisitNote.Visible = false;
                div_ase.Visible = true;
                btnexport1.Visible = true;
                cmbVisitNote.Enabled = false;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;//Future Implementation
                div_groupBy.Visible = false;//Future Implementation
                div_filterBy_brand.Visible = true;
                div_filterBy_flavor.Visible = true;
                div_filterBy_hvp.Visible = true;
                div_filterBy_itemGroup.Visible = true;
                div_filterBy_packingMethod.Visible = true;
                #endregion
                break;
            case 2:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = true;//Rep DropDown is Hidden, But data is getting bounded always, can retieve rep collection if wanted
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = true;
                div_report_type.Visible = true;
                cmbVisitNote.Enabled = false;
                div_quantitytype.Visible = true;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = true;
                div_filterBy_flavor.Visible = true;
                div_filterBy_hvp.Visible = true;
                div_filterBy_itemGroup.Visible = true;
                div_filterBy_packingMethod.Visible = true;
                #endregion
                break;
            case 3:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = true;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = true;
                cmbVisitNote.Enabled = false;
                div_quantitytype.Visible = true;

                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = false;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                ChangeShowReportForDropDownInvoiceSummary();
                #endregion
                break;
            case 4: //For Distributor Summary Report --- Add By Irosh Fernando 2015/03/04
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = true;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_rep.Visible = true;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = true;
                cmbVisitNote.Enabled = false;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = false;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                ChangeShowReportForDropDownDistributorSummary();
                #endregion
                break;
            case 5:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_rep.Visible = false;
                div_distributor.Visible = false;
                div_ase.Visible = false;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = true;
                cmbVisitNote.Enabled = false;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = true;
                div_filterBy_hvp.Visible = true;
                div_filterBy_itemGroup.Visible = true;
                div_filterBy_packingMethod.Visible = true;
                div_filterBy_active_inactive.Visible = true;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", false);
                ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;
            case 6: //For Master Report --- Add By Irosh Fernando 2015/03/04
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = true;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_rep.Visible = false;
                div_distributor.Visible = true;
                div_mssr_report_type.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                cmbVisitNote.Enabled = false;
                div_quantitytype.Visible = true;

                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                //div_filterBy_brand.Visible = false;
                //div_filterBy_flavor.Visible = false;
                //div_filterBy_hvp.Visible = false;
                //div_filterBy_itemGroup.Visible = false;
                //div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;
            case 7:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = true;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = true;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = true;
                cmbVisitNote.Enabled = true;
                cmbQuantityType.Enabled = true;
                div_quantitytype.Visible = true;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = true;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                ChangeShowReportForDropDownInvoiceSummary();
                #endregion
                break;
            // For Tradeload Summary Only
            case 8:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = true;
                div_distributor.Visible = true;
                div_VisitNote.Visible = false;
                div_ase.Visible = true;
                btnexport1.Visible = true;
                cmbVisitNote.Enabled = false;
                cmbAse.Enabled = true;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                div_trade_load_report_type.Visible = true;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;//Future Implementation
                div_groupBy.Visible = false;//Future Implementation
                div_filterBy_brand.Visible = true;
                div_filterBy_flavor.Visible = true;
                div_filterBy_hvp.Visible = true;
                div_filterBy_itemGroup.Visible = true;
                div_filterBy_packingMethod.Visible = true;
                #endregion
                break;
            case 9:
                #region basic options
                div_other_reports.Visible = true;
                div_sfa_invoice_summary.Visible = true;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = false;
                div_distributor.Visible = false;
                div_VisitNote.Visible = false;
                div_ase.Visible = false;
                btnexport1.Visible = true;
                cmbVisitNote.Enabled = false;
                cmbAse.Enabled = false;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;//Future Implementation
                div_groupBy.Visible = false;//Future Implementation
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = false;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                #endregion
                break;
            case 10:// For Report data 
                #region basic options
                div_showReport.Visible = true;
                //div_ToDate.Visible = true;
                div_FromDate.Visible = true;
                btnexport1.Visible = true;
                //div_Divisions.Visible = false;
                div_territory.Visible = true;
                lbStartDate.Text = "Last Invoiced Date";
                //div_Idle_cust_Days.Visible = true;
                ChangeShowReportDropDown();
                ChangeShowCustIdleDaysDropDown();
                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                //div_filterBy_brand.Visible = false;
                //div_filterBy_flavor.Visible = false;
                //div_filterBy_hvp.Visible = false;
                //div_filterBy_itemGroup.Visible = false;
                //div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;
            case 11: //For Master Report --- Add By Irosh Fernando 2015/03/04
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = true;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_rep.Visible = true;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = true;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                cmbVisitNote.Enabled = false;
                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                //div_filterBy_brand.Visible = false;
                //div_filterBy_flavor.Visible = false;
                //div_filterBy_hvp.Visible = false;
                //div_filterBy_itemGroup.Visible = false;
                //div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;
            case 12: //Reps Item Sales
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = false;
                div_distributor.Visible = false;
                div_ase.Visible = false;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = false;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                cmbVisitNote.Enabled = false;
                div_RepsItemSales_report_type.Visible = true;
                div_territory.Visible = true;

                //div_RepItemSalesProducts.Visible = true;
                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                div_filterBy_brand.Visible = false;
                //div_filterBy_flavor.Visible = true;
                //div_filterBy_hvp.Visible = true;
                //div_filterBy_itemGroup.Visible = true;
                //div_filterBy_packingMethod.Visible = true;
                div_filterBy_product.Visible = true;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;
            case 13: //For DB Stock Report --- Add By Irosh Fernando 
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = false;
                div_rep.Visible = false;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                div_ReportTypeQtyValue.Visible = true;
                cmbReportTypeQtyValue.Enabled = true;
                cmbVisitNote.Enabled = false;
                div_mssr_report_type.Visible = false;
                cmbMSSRReportType.Visible = false;
                div_territory.Visible = true;
                div_quantitytype.Visible = true;
                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                //div_filterBy_brand.Visible = false;
                //div_filterBy_flavor.Visible = false;
                //div_filterBy_hvp.Visible = false;
                //div_filterBy_itemGroup.Visible = false;
                //div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;
            case 14:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = false;
                div_distributor.Visible = true;
                div_VisitNote.Visible = false;
                div_ase.Visible = true;
                btnexport1.Visible = true;
                cmbVisitNote.Enabled = false;
                cmbAse.Enabled = true;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = false;
                divUploadSummaryReportType.Visible = true;
                div_quantitytype.Visible = true;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;//Future Implementation
                div_groupBy.Visible = false;//Future Implementation
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = false;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                #endregion
                break;
            case 15: //For Master Report --- Add By Irosh Fernando 2015/03/04
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = false;
                div_distributor.Visible = false;
                div_ase.Visible = false;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = false;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                cmbVisitNote.Enabled = false;
                div_RepsItemSales_report_type.Visible = true;
                div_Divisions.Visible = true;
                //div_RepItemSalesProducts.Visible = true;
                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                div_filterBy_brand.Visible = true;
                //div_filterBy_flavor.Visible = true;
                //div_filterBy_hvp.Visible = true;
                //div_filterBy_itemGroup.Visible = true;
                //div_filterBy_packingMethod.Visible = true;
                div_filterBy_product.Visible = true;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;
            case 16: //For Master Report --- Add By Irosh Fernando 2015/03/04
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = false;
                div_distributor.Visible = false;
                div_ase.Visible = false;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = false;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                cmbVisitNote.Enabled = false;
                div_RepsItemSalesBrand_report_type.Visible = true;
                div_Divisions.Visible = false;
                //div_RepItemSalesProducts.Visible = true;
                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                div_filterBy_brand.Visible = true;
                //div_filterBy_flavor.Visible = true;
                //div_filterBy_hvp.Visible = true;
                //div_filterBy_itemGroup.Visible = true;
                //div_filterBy_packingMethod.Visible = true;
                div_filterBy_product.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;
            case 17:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = true;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_rep.Visible = false;
                div_distributor.Visible = false;
                div_VisitNote.Visible = false;
                div_ase.Visible = true;
                btnexport1.Visible = true;
                cmbVisitNote.Enabled = false;
                cmbAse.Enabled = true;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = false;
                divUploadSummaryReportType.Visible = false;

                #endregion
                #region advanced options
                div_orderBy.Visible = false;//Future Implementation
                div_groupBy.Visible = false;//Future Implementation
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = false;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                #endregion
                break;
            case 18:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = true;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_OrderSummary.Visible = true;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = true;
                cmbOrderReportType.Enabled = true;
                div_quantitytype.Visible = true;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = true;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                ChangeShowReportForDropDownInvoiceSummary();
                #endregion
                break;
            case 19:
                #region basic options
                btnexport1.Visible = true;
                div_asset_registry_reports.Visible = true;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                #endregion
                #region advanced options
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", false);
                //div_filterBy_code.Visible = true;
                //div_filterBy_serial.Visible = true;
                HandleFilterVisibilityForAssetRegistry();
                #endregion
                break;
            case 20:
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = false;
                div_distributor.Visible = false;
                div_ase.Visible = false;
                div_OrderSummary.Visible = false;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = false;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                cmbOrderReportType.Enabled = false;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = false;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", false);
                ChangeShowReportForDropDownInvoiceSummary();
                #endregion
                break;
            case 21: // For Trade Discount / 
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = false;
                divDBActiveInactive.Visible = true;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                div_showReportForASMDB.Visible = true;
                div_ReportTypeQtyValue.Visible = false;
                cmbReportTypeQtyValue.Enabled = false;
                cmbVisitNote.Enabled = false;
                div_mssr_report_type.Visible = false;
                cmbMSSRReportType.Visible = false;
                div_territory.Visible = false;
                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                //div_filterBy_brand.Visible = false;
                //div_filterBy_flavor.Visible = false;
                //div_filterBy_hvp.Visible = false;
                //div_filterBy_itemGroup.Visible = false;
                //div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;

            case 22: // For Trade Discount Summary/ 
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = true;
                div_FromDate.Visible = false;
                div_ToDate.Visible = false;
                div_rep.Visible = false;
                divDBActiveInactive.Visible = true;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                div_showReportForASMDB.Visible = true;
                div_ReportTypeQtyValue.Visible = false;
                cmbReportTypeQtyValue.Enabled = false;
                cmbVisitNote.Enabled = false;
                div_mssr_report_type.Visible = false;
                cmbMSSRReportType.Visible = false;
                div_territory.Visible = false;
                #endregion
                #region advanced options
                //div_orderBy.Visible = false;
                //div_groupBy.Visible = false;
                //div_filterBy_brand.Visible = false;
                //div_filterBy_flavor.Visible = false;
                //div_filterBy_hvp.Visible = false;
                //div_filterBy_itemGroup.Visible = false;
                //div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                //ChangeShowReportForDropDownMasterDetails();
                #endregion
                break;

            case 23: // For Return Summary & Details/ 
                #region basic options
                div_other_reports.Visible = false;
                div_sfa_invoice_summary.Visible = false;
                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = true;
                divDBActiveInactive.Visible = false;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = false;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = true;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                div_showReportFor.Visible = false;
                div_showReportForASMDB.Visible = false; 
                div_ReportTypeQtyValue.Visible = false;
                cmbReportTypeQtyValue.Enabled = false;
                cmbVisitNote.Enabled = false;
                div_mssr_report_type.Visible = false;
                cmbMSSRReportType.Visible = false;
                div_territory.Visible = false;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = false;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                ChangeShowReportForDropDownInvoiceSummary();
                #endregion
                break;

            case 24: //BrandwiseSales report/
                #region basic options

                div_Month.Visible = false;
                div_FromDate.Visible = true;
                div_ToDate.Visible = true;
                div_rep.Visible = true;
                div_distributor.Visible = true;
                div_ase.Visible = true;
                div_VisitNote.Visible = true;
                cmbDistributors.Enabled = false;
                cmbReps.Enabled = false;
                cmbAse.Enabled = true;
                btnexport1.Visible = true;
                cmbVisitNote.Enabled = true;
                cmbQuantityType.Enabled = true;
                div_quantitytype.Visible = true;
                #endregion
                #region advanced options
                div_orderBy.Visible = false;
                div_groupBy.Visible = false;
                div_filterBy_brand.Visible = false;
                div_filterBy_flavor.Visible = false;
                div_filterBy_hvp.Visible = true;
                div_filterBy_itemGroup.Visible = false;
                div_filterBy_packingMethod.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
                ChangeShowReportForDropDownInvoiceSummary();
                #endregion
                break;
        }
    }

    public void HandleShowReportForDropDownChanged()
    {
        cmbAse.Enabled = true;
        cmbDistributors.Enabled = true;
        cmbReps.Enabled = true;
        if (cmbShowReportFor.SelectedItem.Value.Equals("ase"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = false;
            cmbReps.Enabled = false;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("distributor"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            cmbReps.Enabled = false;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("dis"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            cmbReps.Enabled = false;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("rep"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            cmbReps.Enabled = true;
        }
    }

    public void HandleShowReportForASMDBDropDownChanged()
    {
        cmbAse.Enabled = true;
        cmbDistributors.Enabled = true;
        ddlDBActiveInactive.Enabled = false; 
        ddlDBActiveInactive.SelectedValue = "A";

        if (cmbShowReportForASMDB.SelectedItem.Value.Equals("ase"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = false;
            ddlDBActiveInactive.Enabled = false;
        }
        else if (cmbShowReportForASMDB.SelectedItem.Value.Equals("dis"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            ddlDBActiveInactive.Enabled = true;
        }
    }
    public void HandleShowReportForDropDownChangedNew() // Added By Rangan
    {
        cmbAse.Enabled = true;
        //cmbDistributors.Enabled = false;
        //cmbReps.Enabled = false;
        if (cmbAse.SelectedItem.Value.Equals("ALL"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = false;
            cmbReps.Enabled = false;
        }
        else
        {
            if (cmbDistributors.SelectedItem.Value.Equals("ALL"))
            {
                cmbAse.Enabled = true;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = false;
            }

            else
            {

                cmbAse.Enabled = true;
                cmbDistributors.Enabled = true;
                cmbReps.Enabled = true;
            }
        }
           
    }


    public void HandleASEDropdownEnable(bool val)
    {
        cmbAse.Enabled = val;
    }

    public void HandleASEDropdownVisible(bool val)
    {
        div_ase.Visible = val;
    } 
    
    public void HandleDBActiveInactiveDropdownVisible(bool val)
    {
        divDBActiveInactive.Visible = val;
    }

    public void HandleDistributerDropdownEnable(bool val)
    {
        cmbDistributors.Enabled = val;
    }

    public void HandleddlDBActiveInactiveDropdownEnable(bool val)
    {
        ddlDBActiveInactive.Enabled = val;
    }

    public void HandleDistributerDropdownVisible(bool val)
    {
        div_distributor.Visible = val;
    }

    public void HandleSalesRepDropdownEnable(bool val)
    {
        cmbReps.Enabled = val;
    }

    public void HandleSalesReportForDropdownEnable(bool val)
    {
        cmbShowReportFor.Enabled = val;
    }

    public void HandleSalesReportForASMDBDropdownEnable(bool val)
    {
        cmbShowReportForASMDB.Enabled = val;
    }

    public void HandleSalesReportForDropdownVisible(bool val)
    {
        div_showReportFor.Visible = val;
    }
    
    public void HandleSalesReportForASMDBDropdownVisible(bool val)
    {
        div_showReportForASMDB.Visible = val;
    }

    public void HandleTradeloadRepTypeDropdownEnable(bool val)
    {
        ddlTradeLoadRepType.Enabled = val;
    }

    public void HandleTradeloadRepTypeDropdownVisible(bool val)
    {
        div_trade_load_report_type.Visible = val;
    }

    public void HandleExportButtonEnable(bool val)
    {
        btnFilter.Enabled = val;
    }

    public void HandleFilterButtonEnable(bool val)
    {
        btnexport1.Enabled = val;
    }

    public void HandleASEChanged_for_TradeLoadSummary()
    {
        cmbAse.Enabled = true;
        cmbDistributors.Enabled = true;
        cmbReps.Enabled = true;
        if (cmbAse.SelectedItem.Value.Equals("ALL"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = false;
            cmbReps.Enabled = false;
        }
        else
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            if (cmbDistributors.Items.Count > 1)
                cmbReps.Enabled = false;

        }
    }

    public void HandleDistributorChanged_for_TradeLoadSummary()
    {
        cmbAse.Enabled = true;
        cmbDistributors.Enabled = true;
        cmbReps.Enabled = true;
        if (cmbDistributors.SelectedItem.Value.Equals("ALL"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            cmbReps.Enabled = false;
        }
        else
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            cmbReps.Enabled = true;
        }
    }

    public void HandleShowReportForDropDownChangedInvoiceSummary()
    {
        cmbAse.Enabled = true;
        cmbDistributors.Enabled = true;
        cmbReps.Enabled = true;
        if (cmbShowReportFor.SelectedItem.Value.Equals("ase"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = false;
            cmbReps.Enabled = false;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("distributor"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            cmbReps.Enabled = false;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("rep"))
        {
            cmbAse.Enabled = true;
            cmbDistributors.Enabled = true;
            cmbReps.Enabled = true;
        }
    }

    //public void HandleShowReportForDropDownChanged_ProductCategory()
    //{
    //    if (cmbProductFilterBy.SelectedItem.Value.Equals("brand"))
    //    {
    //        lvlAllFilterBy.Text = "Brand";
    //        cmbDistributors.Enabled = false;
    //        cmbReps.Enabled = false;
    //    }
    //    else if (cmbProductFilterBy.SelectedItem.Value.Equals("pck_methd"))
    //    {
    //        lvlAllFilterBy.Text = "Packing Method";
    //        cmbDistributors.Enabled = true;
    //        cmbReps.Enabled = false;
    //    }
    //    else if (cmbProductFilterBy.SelectedItem.Value.Equals("item_group"))
    //    {
    //        lvlAllFilterBy.Text = "Item Group";
    //        cmbDistributors.Enabled = true;
    //        cmbReps.Enabled = true;
    //    }
    //    else if (cmbProductFilterBy.SelectedItem.Value.Equals("hvp"))
    //    {
    //        lvlAllFilterBy.Text = "High Value Product";
    //        cmbDistributors.Enabled = true;
    //        cmbReps.Enabled = true;
    //    }
    //}

    public void HandleShowReportForDropDownChangedMasterDetails()
    {
        //cmbAse.Enabled = false;
        //cmbDistributors.Enabled = false;
        //if (cmbShowReportFor.SelectedItem.Value.Equals("distributor"))
        //{
        //    cmbAse.Enabled = true;
        //    cmbDistributors.Enabled = false;
        //}
        //else if (cmbShowReportFor.SelectedItem.Value.Equals("reps"))
        //{
        //    cmbAse.Enabled = true;
        //    cmbDistributors.Enabled = true;
        //}

        div_ase.Visible = false;
        div_distributor.Visible = false;

        div_filterBy_brand.Visible = false;
        div_filterBy_hvp.Visible = false;
        div_filterBy_itemGroup.Visible = false;
        div_filterBy_packingMethod.Visible = false;
        div_filterBy_active_inactive.Visible = false;

        if (cmbShowReportFor.SelectedItem.Value.Equals("distributor"))
        {
            div_ase.Visible = true;
            div_distributor.Visible = false;
            div_filterBy_active_inactive.Visible = true;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("reps"))
        {
            div_ase.Visible = true;
            div_distributor.Visible = true;
            div_filterBy_active_inactive.Visible = true;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("outlet"))
        {
            div_ase.Visible = true;
            div_distributor.Visible = true;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("products"))
        {
            div_filterBy_brand.Visible = true;
            div_filterBy_hvp.Visible = true;
            div_filterBy_itemGroup.Visible = true;
            div_filterBy_packingMethod.Visible = true;
            div_filterBy_active_inactive.Visible = true;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("territory"))
        {
            div_ase.Visible = true;
            div_distributor.Visible = false;
            div_filterBy_active_inactive.Visible = true;
        }
        else if (cmbShowReportFor.SelectedItem.Value.Equals("routes"))
        {
            div_ase.Visible = true;
            div_filterBy_active_inactive.Visible = true;
        }
    }

    public void ChangeShowReportForDropDownInvoiceSummary()
    {
        cmbShowReportFor.Items.Clear();
        cmbShowReportFor.Items.Add(new ListItem { Text = "ASM", Value = "ase", Selected = true });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Distributor", Value = "distributor" });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Rep", Value = "rep" });
    }

    public void ChangeShowReportForDropDownDistributorSummary()
    {
        cmbShowReportFor.Items.Clear();
        //cmbShowReportFor.Items.Add(new ListItem { Text = "ALL", Value = "all", Selected = true });
        cmbShowReportFor.Items.Add(new ListItem { Text = "ASM", Value = "ase", Selected = true });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Distributor", Value = "dis" });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Rep", Value = "rep" });
    }

    public void ChangeShowReportForDropDownMasterDetails()
    {
        cmbShowReportFor.Items.Clear();
        cmbShowReportFor.Items.Add(new ListItem { Text = "Products", Value = "products", Selected = true });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Outlets", Value = "outlet" });
        cmbShowReportFor.Items.Add(new ListItem { Text = "ASM List", Value = "ase" });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Distributors List", Value = "distributor" });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Reps List", Value = "reps" });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Route List", Value = "routes" });
        cmbShowReportFor.Items.Add(new ListItem { Text = "Territory List", Value = "territory" });
    }

    public void EnableDistributorDropDown(bool isEnable)
    {
        if (isEnable)
            cmbDistributors.Enabled = true;
        else
            cmbDistributors.Enabled = false;
    }

    public void EnableRepDropDown(bool isEnable)
    {
        if (isEnable)
            cmbReps.Enabled = true;
        else
            cmbReps.Enabled = false;
    }

    #region Advanced Options

    public void SetBrands(List<BrandDTO> listBrands, string valueField = "BrandId")
    {
        cmbBrand.DataSource = listBrands;
        cmbBrand.DataTextField = "BrandName";
        cmbBrand.DataValueField = valueField;
        cmbBrand.DataBind();
    }

    public void SetPackingMethod(List<ProductPackingDTO> listPacking, string valueField = "PackingId")
    {
        cmbPackingMethod.DataSource = listPacking;
        cmbPackingMethod.DataTextField = "PackingName";
        cmbPackingMethod.DataValueField = valueField;
        cmbPackingMethod.DataBind();
    }

    public void SetItemGroup(List<ProductCategoryDTO> listItemGroup, string valueField = "Id")
    {
        cmbItemGroup.DataSource = listItemGroup;
        cmbItemGroup.DataTextField = "Description";
        cmbItemGroup.DataValueField = valueField;
        cmbItemGroup.DataBind();
    }

    public void SetFlavor(List<FlavorDTO> listFlavors, string valueField = "FlavorId")
    {
        cmbFlavor.DataSource = listFlavors;
        cmbFlavor.DataTextField = "FlavorName";
        cmbFlavor.DataValueField = valueField;
        cmbFlavor.DataBind();
    }

    public string GetFilterByBrandValue()
    {
        return cmbBrand.SelectedItem.Value;
    }

    public string GetFilterByPackingMethodValue()
    {
        return cmbPackingMethod.SelectedItem.Value;
    }

    public string GetFilterByItemGroupValue()
    {
        return cmbItemGroup.SelectedItem.Value;
    }

    public string GetFilterByFlavorValue()
    {
        return cmbFlavor.SelectedItem.Value;
    }

    public string GetFilterByHVPValue()
    {
        return cmbHvp.SelectedItem.Value;
    }
    
    public string GetFilterByActiveInactiveValue()
    {
        return cmbActiveInactive.SelectedItem.Value;
    }

    public string GetOrderByValue()
    {
        return cmbOrderBy.SelectedItem.Value;
    }

    public string GetGroupByValue()
    {
        if (cmbGroupBy.SelectedItem.Value != "all")
            return cmbGroupBy.SelectedItem.Value;
        else
            return "";
    }

    public string GetGroupByText()
    {
        if (cmbGroupBy.SelectedItem.Value != "all")
            return cmbGroupBy.SelectedItem.Text;
        else
            return "";
    }

    public string GetSFASalesRep()
    {
        return ddlSFAInvoiceSalesRep.SelectedItem.Value;
    }

    public string GetSFAProduct1()
    {
        return ddlSFAInvoiceProduct1.SelectedItem.Value;
    }

    public string GetSFAProduct2()
    {
        return ddlSFAInvoiceProduct2.SelectedItem.Value;
    }

    public string GetSFAInvoiceAmount()
    {
        return txtSFAInvoiceAmount.Text;
    }

    public string GetShowIdleValue()
    {
        if (cmbShowIdleDays.SelectedItem != null)
            return cmbShowIdleDays.SelectedItem.Value;
        else
            return "";
    }


    public string GetShowReportValue()
    {
        if (cmbreport.SelectedItem != null)
            return cmbreport.SelectedItem.Value;
        else
            return "";
    }

    public string GetMSSRReportType()
    {
        if (cmbMSSRReportType.SelectedItem != null)
            return cmbMSSRReportType.SelectedItem.Value;
        else
            return "";
    }

    protected void cmbShowReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListShowReport("dropdown");
        }
        catch (Exception ex)
        {
        }
    }

    // report drop down button item disply event handle edited by amila 
    public void HandleShowReportDropDownChangedReportDetails()
    {
        if (cmbreport.SelectedItem.Value.Equals("hours"))
        {
            div_FromDate.Visible = true;
            lbStartDate.Text = "Start Date";
            div_ToDate.Visible = true;
            div_Idle_cust_Days.Visible = false;
        }
        else if (cmbreport.SelectedItem.Value.Equals("Idle"))
        {
            div_FromDate.Visible = true;
            lbStartDate.Text = "Last Invoiced Date";
            div_ToDate.Visible = false;
            div_Divisions.Visible = true;
        }
        else if (cmbreport.SelectedItem.Value.Equals("cancle"))
        {
            div_FromDate.Visible = true;
            lbStartDate.Text = "Start Date";
            div_ToDate.Visible = true;
            div_Idle_cust_Days.Visible = false;
        }
        else if (cmbreport.SelectedItem.Value.Equals("idl_pro"))
        {
            div_FromDate.Visible = true;
            lbStartDate.Text = "Last Invoiced Date";
            div_ToDate.Visible = false;
            div_Idle_cust_Days.Visible = false;
            div_Divisions.Visible = false;
        }
    }

    public void ChangeShowReportDropDown()
    {
        cmbreport.Items.Clear();
        //cmbreport.Items.Add(new ListItem { Text = "Rep working hours", Value = "hours", Selected = true });
        cmbreport.Items.Add(new ListItem { Text = "Idle customers", Value = "Idle", Selected = true });
        cmbreport.Items.Add(new ListItem { Text = "Idle product list", Value = "idl_pro" });
        //cmbreport.Items.Add(new ListItem { Text = "Cancel Invoices", Value = "cancle" });

    }

    public void ChangeShowCustIdleDaysDropDown()
    {
        cmbShowIdleDays.Items.Clear();
        cmbShowIdleDays.Items.Add(new ListItem { Text = "One month", Value = "1", Selected = true });
        cmbShowIdleDays.Items.Add(new ListItem { Text = "Two Months", Value = "2" });
        cmbShowIdleDays.Items.Add(new ListItem { Text = "Three Months", Value = "3" });
        cmbShowIdleDays.Items.Add(new ListItem { Text = "Six Months", Value = "6" });
        cmbShowIdleDays.Items.Add(new ListItem { Text = "One year", Value = "12" });
        cmbShowIdleDays.Items.Add(new ListItem { Text = "Two years", Value = "24" });
    }

    public void HandleFilterVisibilityForAssetRegistry()
    {
        if (ddlAssetRegistery.SelectedItem.Value.Equals("asset_detail"))
        {
            div_FromDate.Visible = false;
            div_ToDate.Visible = false;
            div_filterBy_code.Visible = true;
            div_filterBy_serial.Visible = true;
            div_filterBy_status.Visible = false;
            div_filterBy_type.Visible = true;
            div_filterBy_asset_detail_status.Visible = true;

            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", false);
        }
        else if (ddlAssetRegistery.SelectedItem.Value.Equals("asset_registry"))
        {
            dtpFromDate.Text = new DateTime(2000, 1, 1).ToString("yyyy-MM-dd");
            dtpToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

            div_FromDate.Visible = false;
            div_ToDate.Visible = false;
            div_filterBy_code.Visible = true;
            div_filterBy_serial.Visible = true;
            div_filterBy_status.Visible = true;
            div_filterBy_type.Visible = true;
            div_filterBy_asset_detail_status.Visible = false;

            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", false);
        }
        else
        {
            div_FromDate.Visible = true;
            div_ToDate.Visible = true;
            div_filterBy_code.Visible = false;
            div_filterBy_serial.Visible = false;
            div_filterBy_status.Visible = false;
            div_filterBy_type.Visible = false;
            div_filterBy_asset_detail_status.Visible = false;

            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "Hide_showAdvancedOptions();", true);
        }
    }


    #endregion

    #endregion

    protected void ddlAssetRegistery_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListAssetReports("dropdown");
        }
        catch (Exception)
        {
        }
    }

    protected void ddlOtherReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListOtherReports("dropdown");
            //div_sfa_invoice_summary.Visible = false;
            //if (ddlOtherReport.SelectedValue == "sfa_invoice_summary")
            //{
            //    div_sfa_invoice_summary.Visible = true;
            //}
        }
        catch (Exception ex)
        {
        }
    }

    //protected void DropDownListSFAOptionsProduct1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        onDropDownListSFAOptionsProduct1("dropdown");
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    protected void cmbShowCustIdleDays_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListShowCustIdleDays("dropdown");
        }
        catch (Exception ex)
        {

        }
    }
    protected void cmbDivisions_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListDivisions("dropdown");
        }
        catch (Exception ex)
        {

        }
    }
    protected void cmb_RepsItemSalesReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListRepSalesItems("dropdown");
        }
        catch (Exception ex)
        {

        }
    }

    protected void cmb_RepsItemSalesBrandReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListRepSalesItemsBrand("dropdown");
        }
        catch (Exception ex)
        {

        }
    }

    protected void cmbTerritory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListTerritory("dropdown");
        }
        catch (Exception ex)
        {

        }
    }

    protected void cmbOrderReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListOrderReportType("dropdown");
        }
        catch (Exception ex)
        {

        }
    }
    
    protected void cmbQuantityType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListQuantityType("dropdown");
        }
        catch (Exception ex)
        {

        }
    }

    public void SetAssetTypes(List<AssetTypeModel> list)
    {
        ddlAssetType.DataSource = list;
        ddlAssetType.DataTextField = "AssetTypeName";
        ddlAssetType.DataValueField = "AssetTypeId";
        ddlAssetType.DataBind();
    }


    public string GetAssetCode()
    {
        return txtAssetCode.Text.ToString();
    }

    public string GetAssetSerial()
    {
        return txtAssetSerial.Text.ToString();
    }

    public string GetAssetStatus()
    {
        return ddlAssetStatus.SelectedValue;
    }
    
    public string GetAssetDetailStatus()
    {
        return ddlAssetDetailStatus.SelectedValue;
    }

    public string GetAssetType()
    {
        return ddlAssetType.SelectedValue;
    }


}
