﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contactperson.aspx.cs" Inherits="usercontrols_contactperson" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="tab_button">
<div class="addlinkdiv" style="width:115px">
   <a id="id_AddContactPerson" runat="server" href="#">Add Contact Person</a>
</div>
</div>

<div style="min-width:200px; overflow:auto">
    <div id="LeadContactPersonGrid"></div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        loadLeadContactPersonGrid();
    });
    
            
            </script>
    </form>
</body>
</html>
