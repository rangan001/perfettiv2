﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_address.aspx.cs" Inherits="usercontrols_customer_address" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldAddress_CustCode" runat="server" />
    <div id="div_customer_address">
        <div id="window">
        </div>
        <div class="tab_button">
            <div class="addlinkdiv" style="float: left" id="div_address">
                <a id="id_address" runat="server" href="#">Add Address</a>
            </div>
            <div style="float: left; width: 3px">
                &nbsp;</div>
            <div class="maplink" style="float: left" id="div_map">
                <a id="btnMap" >Map</a>
            </div>
            <div class="clearall">
            </div>
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="LeadCustAddressGrid">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var cust_code = $("#HiddenFieldAddress_CustCode").val();

            loadCustomerAddressGrid(cust_code, 'LeadCustAddressGrid');
            
            var window = $("#window"),
                        undo = $("#undo")
                                .bind("click", function () {
                                    window.data("kendoWindow").open();
                                    undo.hide();
                                });

            var onClose = function () {
                undo.show();
            }

            if (!window.data("kendoWindow")) {
                window.kendoWindow({
                    width: "600px",
                    title: "Customer Address Entry",
                    close: onClose
                });
            }
        });

        $("#btnMap").click(function () {
            loadLeadAddressMap('#LeadCustAddressGrid','', $("#MainContent_HiddenCustomerCode").val());
        });
</script>

    </form>
</body>
</html>