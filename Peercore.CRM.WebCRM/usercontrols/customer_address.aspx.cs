﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;

public partial class usercontrols_customer_address : PageBase
{
    #region Properties
    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion Properties

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(CustomerEntry.Key))
        {
            HiddenFieldAddress_CustCode.Value =  CustomerEntry.Key;
            id_address.HRef = "javascript:addAddress('lead_customer/process_forms/processmaster.aspx?fm=contactdetails&type=insert&custid=" + Server.UrlEncode(CustomerEntry.Key) + "','')";
        }
        else
        {
            id_address.HRef = "javascript:void('0')";
        }
    }
    #endregion
}