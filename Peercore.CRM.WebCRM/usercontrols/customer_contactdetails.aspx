﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_contactdetails.aspx.cs" Inherits="usercontrols_customer_contactdetails" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <script type="text/javascript" src="../../assets/scripts/jquery.maskedinput.js"></script>
    
    <form id="form1" runat="server">
    <div class="form_container">
    <div id="div_contactdetails">

        <div class="formleft">
            <div class="formtextdiv">
                Address 1</div>
            <div class="formdetaildiv_right"> :
                <asp:TextBox ID="txtcustAddress1" runat="server" Height="25px" CssClass="input-large" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
<%--            <div class="formtextdiv">
                Address 2</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtcustAddress2" runat="server" Height="25px" CssClass="input-large" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>--%>
            <div class="formtextdiv">
                City</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtCustCity" runat="server" Height="25px" CssClass="input-large" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
<%--            <div class="formtextdiv">
                State</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtCustState" runat="server" Height="25px" CssClass="input-large" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>            
            <div class="formtextdiv">
                Postcode</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtCustPostcode" runat="server" Height="25px" CssClass="input-large" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>--%>
        </div>
        <div class="formright">
            <div class="formtextdiv">
                Phone</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="mtxtPhone" runat="server" Height="25px" CssClass="input-large-phone" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
           <%-- <div class="formtextdiv">
                Mobile</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="mtxtMobile" runat="server" Height="25px" CssClass="input-large-mobile" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Fax</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="mtxtFax" runat="server" Height="25px" CssClass="input-large-phone" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Website</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtWebsite" runat="server" Height="25px" CssClass="input-large" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Email</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtEmail" runat="server" Height="25px" CssClass="input-large" onblur="settingCustomerUsercontrolValues()"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Pref. Method</div>
            <div class="formdetaildivlead"> :
                <asp:DropDownList ID="ddlPreferredMethod" runat="server" CssClass="input-large" onblur="settingCustomerUsercontrolValues()" />
            </div>
            <div class="clearall">
            </div>--%>
        </div></div>
    </div>
    </form>

    <script type="text/javascript">

        $(document).ready(function () {
            loadCustomerContactDetail();
        });

        jQuery(function ($) {
            $.mask.definitions['~'] = '[+0123456789]';
            $('input.input-large-mobile').mask("~9?999999999999");
        });

        jQuery(function ($) {
            $('input.input-large-phone').mask("9?999999999");
        });

        function loadCustomerContactDetail() {
            document.getElementById("mtxtPhone").value = document.getElementById("MainContent_HiddenFieldContactPhone").value;
            document.getElementById("mtxtMobile").value = document.getElementById("MainContent_HiddenFieldContactMobile").value;
            document.getElementById("mtxtFax").value = document.getElementById("MainContent_HiddenFieldContactFax").value;
            document.getElementById("txtWebsite").value = document.getElementById("MainContent_HiddenFieldContactWebSite").value;
            document.getElementById("txtEmail").value = document.getElementById("MainContent_HiddenFieldContactEmail").value;
            document.getElementById("ddlPreferredMethod").value = document.getElementById("MainContent_HiddenFieldContactPrefMethod").value;

            document.getElementById("txtcustAddress1").value = document.getElementById("MainContent_HiddenFieldCustAddress1").value;
            document.getElementById("txtcustAddress2").value = document.getElementById("MainContent_HiddenFieldCustAddress2").value;
            document.getElementById("txtCustCity").value = document.getElementById("MainContent_HiddenFieldCustCity").value;
            document.getElementById("txtCustState").value = document.getElementById("MainContent_HiddenFieldCustState").value;
            document.getElementById("txtCustPostcode").value = document.getElementById("MainContent_HiddenFieldCustPostcode").value;
        }

        function settingCustomerUsercontrolValues() {
            document.getElementById("MainContent_HiddenFieldCustAddress1").value = document.getElementById("txtcustAddress1").value;
            document.getElementById("MainContent_HiddenFieldCustAddress2").value = document.getElementById("txtcustAddress2").value;
            document.getElementById("MainContent_HiddenFieldCustCity").value = document.getElementById("txtCustCity").value;
            document.getElementById("MainContent_HiddenFieldCustState").value = document.getElementById("txtCustState").value;
            document.getElementById("MainContent_HiddenFieldCustPostcode").value = document.getElementById("txtCustPostcode").value;

            document.getElementById("MainContent_HiddenFieldContactPhone").value = document.getElementById("mtxtPhone").value.replace(/[\!\#\$\%\^\*\(\)\"'"\''\~\`]*/g, "");
            document.getElementById("MainContent_HiddenFieldContactMobile").value = document.getElementById("mtxtMobile").value.replace(/[\!\#\$\%\^\*\(\)\"'"\''\~\`]*/g, "");
            document.getElementById("MainContent_HiddenFieldContactFax").value = document.getElementById("mtxtFax").value.replace(/[\!\#\$\%\^\*\(\)\"'"\''\~\`]*/g, "");
            document.getElementById("MainContent_HiddenFieldContactWebSite").value = document.getElementById("txtWebsite").value;
            document.getElementById("MainContent_HiddenFieldContactEmail").value = document.getElementById("txtEmail").value;
            document.getElementById("MainContent_HiddenFieldContactPrefMethod").value = document.getElementById("ddlPreferredMethod").value;
        }
</script>

</body>
</html>
