﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_contactperson.aspx.cs" Inherits="usercontrols_customer_contactperson" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldContactPerson_CustCode" runat="server" />
    <div id="div_customer_contactperson">
        <div class="tab_button">
            <div class="addlinkdiv" style="float: left" id="div_contactperson">
                <a id="id_contactperson" runat="server" href="#">Add Contact Person</a>
            </div>
            <div style="float: left; width: 3px">
                &nbsp;</div>
            <div class="maplink" style="float: left;display:none" id="div_map">
                <a id="btnMap" runat="server" href="javascript:loadLeadAddressMap();">Map</a>
            </div>
            <div class="clearall">
            </div>
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="LeadContactPersonGrid">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var cust_code = $("#HiddenFieldContactPerson_CustCode").val();
            loadCustomerContactPersonGrid( cust_code, 'LeadContactPersonGrid');
        });
    </script>

    </form>
</body>
</html>