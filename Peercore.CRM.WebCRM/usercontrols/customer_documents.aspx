﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_documents.aspx.cs" Inherits="usercontrols_customer_documents" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldDocument_CustCode" runat="server" />
    <div id="div_customer_documents">
            <div id="divFrame" align="left">
            </div>
            <div style="width: 45%">
                <input class="upload" name="files" id="files" type="file" upload-id="02ebeebf-98aa-459b-b41f-49028fa37e9c" />
            </div>
            <div class="clearall">
            </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="LeadDocumentGrid">
                </div>
            </div>
        </div>
    </div>
       <script type="text/javascript">
           $(document).ready(function () {
               var cust_code = $("#HiddenFieldDocument_CustCode").val();
               //alert(cust_code);
               Customerdocumentupload(cust_code);
               loadCustomerDocumentGrid(cust_code, 'LeadDocumentGrid');
           });
    </script>
    </form>
</body>
</html>
