﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_email.aspx.cs" Inherits="usercontrols_customer_email" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldEmail_CustCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldEmail_EnduserCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldEmail_LeadId" runat="server" />
    <div style="min-width: 200px; overflow: auto">
        <table width="100%">
            <tr>
                <td style="width: 550px; vertical-align: top">
                    <div id="LeademailGrid">
                    </div>
                </td>
                <td valign="top">
                    <div id="div_mailAttachments" class="mailattachment">
                    </div>
                </td>
            </tr>
        </table>
        <div class="clearall">
        </div>
        <table width="100%">
            <tr>
                <td style="width: 550px; vertical-align: top">
                    <div id="div_mailcontent">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var cust_code = $("#HiddenFieldEmail_CustCode").val();
            var enduser_code = $("#HiddenFieldEmail_EnduserCode").val();
            var lead_id = $("#HiddenFieldEmail_LeadId").val();

            loadLeademailGrid(cust_code, enduser_code);
        });
    </script>
    </form>
</body>
</html>
