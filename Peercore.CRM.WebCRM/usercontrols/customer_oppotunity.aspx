﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_oppotunity.aspx.cs" Inherits="usercontrols_customer_oppotunity" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldOpp_CustCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldOpp_EnduserCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldOpp_LeadId" runat="server" />

    <div style="min-width:200px; overflow:auto">
<div class="tab_button">
    <div class="addlinkdiv">
        <a id="link_opportunity" runat="server">ADD OPPORTUNITY</a>
    </div>
</div>
<div id="LeadOpportunityGrid"></div>
</div>

    <script type="text/javascript">
        $(document).ready(function () {
            var cust_code = $("#HiddenFieldOpp_CustCode").val();
            var enduser_code = $("#HiddenFieldOpp_EnduserCode").val();
            var leadid = $("#HiddenFieldOpp_LeadId").val();

            if (enduser_code == undefined || enduser_code == null)
                enduser_code = '';

            loadLeadOpportunityGrid(cust_code,leadid, enduser_code);
        });
    </script>
    </form>
</body>
</html>
