﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class usercontrols_customer_oppotunity : PageBase
{
    #region Properties
    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion Properties

    protected void Page_Load(object sender, EventArgs e)
    {
        ArgsDTO argsDTO = (ArgsDTO)(Session[CommonUtility.GLOBAL_SETTING]);
        string LStageName = (!string.IsNullOrEmpty(argsDTO.LeadStage)) ? argsDTO.LeadStage.ToString() : string.Empty;
        //string LStageName = argsDTO.LeadStage.ToString();

        if ((Request.QueryString["eduid"] != null && Request.QueryString["eduid"] != string.Empty) &&
            (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty))
        {
            HiddenFieldOpp_CustCode.Value = Server.UrlEncode(Request.QueryString["custid"].ToString());
            HiddenFieldOpp_EnduserCode.Value = Server.UrlEncode(Request.QueryString["eduid"].ToString());

            link_opportunity.HRef = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunityentry.aspx?custid=" + Server.UrlEncode(Request.QueryString["custid"]) + "&eduid=" + Server.UrlEncode(Request.QueryString["eduid"].ToString());
        }
        else if (!string.IsNullOrWhiteSpace(CustomerEntry.Key))
        {
            link_opportunity.HRef = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunityentry.aspx?custid=" + Server.UrlEncode(CustomerEntry.Key);
            HiddenFieldOpp_CustCode.Value = Server.UrlEncode(CustomerEntry.Key.ToString());
        }
        else if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty)
        {
            link_opportunity.HRef = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunityentry.aspx?leadid=" + Request.QueryString["leadid"].ToString() + "&fm=lead"+ ((!string.IsNullOrEmpty(LStageName)) ? "&ty=" + LStageName : "");
            HiddenFieldOpp_LeadId.Value = Server.UrlEncode(Request.QueryString["leadid"].ToString());
        }
    }
}