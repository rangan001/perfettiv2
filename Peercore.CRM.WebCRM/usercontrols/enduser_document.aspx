﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="enduser_document.aspx.cs"
    Inherits="usercontrols_enduser_document" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldDocument_CustCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldDocument_EnduserCode" runat="server" />

    <div id="divFrame" align="left">
    </div>
    <div style="width: 45%">
        <input class="upload" name="files" id="files" type="file" upload-id="02ebeebf-98aa-459b-b41f-49028fa37e9c" />
    </div>
    <div id="LeadDocumentGrid">
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var cust_code = $("#HiddenFieldDocument_CustCode").val();
            var enduser_code = $("#HiddenFieldDocument_EnduserCode").val();
            if ((cust_code != null && cust_code != '') && (enduser_code != null && enduser_code != '')) {
                EndUserdocumentupload(cust_code, enduser_code, 'LeadDocumentGrid');
                loadEndUserdocumentGrid(cust_code, enduser_code, 'LeadDocumentGrid');
            }
        });
    </script>
    </form>
</body>
</html>
