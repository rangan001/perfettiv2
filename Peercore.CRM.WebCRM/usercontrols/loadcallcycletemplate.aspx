﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="loadcallcycletemplate.aspx.cs"
    Inherits="usercontrols_loadcallcycletemplate" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title></title>
</head>
<body>
    <div id="div_template" runat="server">

        <div class="popup_div">
            <div class="col1">&nbsp;</div>
            <div class="col2">Day 1</div>
            <div class="col3">Day 2</div>
            <div class="col4">Day 3</div>
            <div class="col5">Day 4</div>
            <div class="col6">Day 5</div>
            <div class="clear"></div>
            <div class="col1">Due On</div>
            <div class="col2"><input type="text" id="dueon1" runat="server" style="width:120px;"></div>
            <div class="col3"><input type="text" id="dueon2" runat="server" style="width:120px;" /></div>
            <div class="col4"><input type="text" id="dueon3" runat="server" style="width:120px;" /></div>
            <div class="col5"><input type="text" id="dueon4" runat="server" style="width:120px;" /></div>
            <div class="col6"><input type="text" id="dueon5" runat="server" style="width:120px;" /></div>
            <div class="clear"></div>
            <div class="col1">Start Time</div>
            <div class="col2"><input type="text" id="time1" runat="server" style="width:120px;" /></div>
            <div class="col3"><input type="text" id="time2" runat="server" style="width:120px;"/></div>
            <div class="col4"><input type="text" id="time3" runat="server" style="width:120px;" /></div>
            <div class="col5"><input type="text" id="time4" runat="server" style="width:120px;" /></div>
            <div class="col6"><input type="text" id="time5" runat="server" style="width:120px;" /></div>
            <div class="clear"></div>
            <div class="col1">Select</div>
            <div class="col2"><input type="checkbox" id="Checkbox1" runat="server" /></div>
            <div class="col3"><input type="checkbox" id="Checkbox2" runat="server" /></div>
            <div class="col4"><input type="checkbox" id="Checkbox3" runat="server" /></div>
            <div class="col5"><input type="checkbox" id="Checkbox4" runat="server" /></div>
            <div class="col6"><input type="checkbox" id="Checkbox5" runat="server" /></div>
            <div class="clear"></div>
            <input type="button" id="buttoncancel" runat="server" OnServerClick="buttonok_click" class="btn"/>
            <input type="button" id="buttonok" runat="server" OnServerClick="buttonok_click" class="btn"/>

        </div>
    </div>



     <script type="text/javascript">
        $(document).ready(function () {
            $("#dueon1").kendoDatePicker({
                value: new Date(),
                format: "dd-MMM-yyyy"
            });
            $("#dueon2").kendoDatePicker({
                value: new Date(),
                format: "dd-MMM-yyyy"
            });
            $("#dueon3").kendoDatePicker({
                value: new Date(),
                format: "dd-MMM-yyyy"
            });
            $("#dueon4").kendoDatePicker({
                value: new Date(),
                format: "dd-MMM-yyyy"
            });
            $("#dueon5").kendoDatePicker({
                value: new Date(),
                format: "dd-MMM-yyyy"
            });

            $("#time1").kendoTimePicker({
                format: "hh:mm:ss tt"
            });
            $("#time2").kendoTimePicker({
                format: "hh:mm:ss tt"
            });
            $("#time3").kendoTimePicker({
                format: "hh:mm:ss tt"
            });
            $("#time4").kendoTimePicker({
                format: "hh:mm:ss tt"
            });
            $("#time5").kendoTimePicker({
                format: "hh:mm:ss tt"
            });


        });


     </script>

</body>
</html>
