﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="option_level.ascx.cs" Inherits="usercontrols_option_level" %>


<div class="divcontectmainforms">
    <div class="formleft">
        <div class="formtextdiv">
            Scheme Name
        </div>
        <div class="formdetaildiv_right">
            :
                    <span class="wosub mand">
                        <asp:TextBox ID="txtSchemeDetails" runat="server" ClientIDMode="Static" Style="width: 400px;"></asp:TextBox>
                        <span style="color: Red">*</span>
                    </span>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
            Scheme Type
        </div>
        <div class="formdetaildiv_right">
            :
                    <asp:DropDownList ID="DropDownListSchemeType" runat="server" CssClass="input-large1" ClientIDMode="Static">
                    </asp:DropDownList>
        </div>
        <div class="clearall">
        </div>
        <div id="div_Percentage" style="display: none">
            <div class="formtextdiv">
                Discount Percentage(%)
            </div>
            <div class="formdetaildiv_right">
                :
                        <span class="wosub mand">
                            <asp:TextBox ID="txtPercentage" runat="server" ClientIDMode="Static" CssClass="validate-numbers-percentage"></asp:TextBox>
                            <span style="color: Red">*</span>
                        </span>
            </div>
        </div>
        <div class="clearall">
        </div>

        <div id="div_value">
            <div class="formtextdiv">
                Discount Value(Rs.)
            </div>
            <div class="formdetaildiv_right">
                :
                        <span class="wosub mand">
                            <asp:TextBox ID="txtDiscountValue" runat="server" ClientIDMode="Static" CssClass="validate-numbers"></asp:TextBox>
                            <span style="color: Red">*</span>
                        </span>
            </div>
        </div>
        <div class="clearall">
        </div>

    </div>
    <div class="formright">

        <div class="formtextdiv">
            Customer Type
        </div>
        <div class="formdetaildiv_right">
            :
                    <asp:DropDownList ID="cmbShopType" runat="server" CssClass="input-large1" ClientIDMode="Static">
                        <asp:ListItem Value="Any" Text="Any"></asp:ListItem>
                        <asp:ListItem Value="Retailer" Text="Retailer"></asp:ListItem>
                        <asp:ListItem Value="Wholesale" Text="Wholesale"></asp:ListItem>
                    </asp:DropDownList>
            <%--<asp:CheckBox ID="chkIsRetailer" runat="server"  ClientIDMode="Static"/>--%>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
            Detail Type
        </div>
        <div class="formdetaildiv_right">
            :
                    <asp:DropDownList ID="DropDownListDetailType" runat="server" CssClass="input-large1" ClientIDMode="Static">
                    </asp:DropDownList>
        </div>
        <div class="clearall">
        </div>

    </div>

    <div class="clearall">
    </div>
    <div id="div_free_product" style="display: none; background-color: #C5E2DA; padding-bottom: 5px; padding-top: 15px;">
        <a id="a_product" class="addlinkdiv" style="margin-left: 10px; cursor: pointer;">Add Free Product</a>

        Condition : 
                <asp:DropDownList ID="ddlFreeProductCondition" runat="server" CssClass="input-large1">
                    <asp:ListItem Text="And" Value="and"></asp:ListItem>
                    <asp:ListItem Text="Or" Value="or"></asp:ListItem>
                </asp:DropDownList>

        <div class="clearall">
        </div>

        <br />
        <div id="dis_product" class="grid_margin"></div>
    </div>
    <div style="background-color: #D4D4D6">
        <br />
        <div id="div_option_level">
            <div id="div_typebill">
                <div class="formtextdiv_small">
                    Is Bill
                </div>
                <div class="formdetaildiv_right">
                    :
                    <asp:CheckBox ID="chkIsBill" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div id="div_bill" style="display: none">

                <div class="formtextdiv_small">
                    Operator
                </div>
                <div class="formdetaildiv_right">
                    :
                        <asp:DropDownList ID="DropDownListOperator" runat="server" ClientIDMode="Static">
                            <asp:ListItem Value="1" Text="greater than"></asp:ListItem>
                            <asp:ListItem Value="2" Text="greater than or equal"></asp:ListItem>
                            <asp:ListItem Value="3" Text="less than"></asp:ListItem>
                            <asp:ListItem Value="4" Text="less than or equal"></asp:ListItem>
                            <asp:ListItem Value="5" Text="equal"></asp:ListItem>
                            <asp:ListItem Value="6" Text="between"></asp:ListItem>
                        </asp:DropDownList>
                </div>
                <div class="formtextdiv_small">
                    Min Value
                </div>
                <div class="formdetaildiv_right">
                    :
                        <asp:TextBox ID="txtMinBValue" runat="server" ClientIDMode="Static" CssClass="validate-numbers" Style="width: 60px;"></asp:TextBox>
                </div>

                <div id="div_between" style="display: none">

                    <div class="formtextdiv_small">
                        Max Value
                    </div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtMaxBValue" runat="server" ClientIDMode="Static" CssClass="validate-numbers" Style="width: 60px;"></asp:TextBox>
                    </div>
                </div>

            </div>
            <div id="div_qty_val">
                <div class="formtextdiv_small" style="width: 50px;">
                    Line Count
                </div>
                <div class="formdetaildiv_right">
                    :
                        <asp:TextBox ID="txtLineCount" runat="server" ClientIDMode="Static" CssClass="validate-numbers" Style="width: 60px;"></asp:TextBox>
                </div>

                <div class="formtextdiv_small">
                    Min Quantity
                </div>
                <div class="formdetaildiv_right">
                    :
                        <asp:TextBox ID="txtMinQuantity" runat="server" ClientIDMode="Static" CssClass="validate-numbers" Style="width: 60px;"></asp:TextBox>
                </div>

                <div class="formtextdiv_small">
                    Max Quantity
                </div>
                <div class="formdetaildiv_right">
                    :
                        <asp:TextBox ID="txtMaxQuantity" runat="server" ClientIDMode="Static" CssClass="validate-numbers" Style="width: 60px;"></asp:TextBox>
                </div>
                <div class="formtextdiv_small">
                    Min Value
                </div>
                <div class="formdetaildiv_right">
                    :
                        <asp:TextBox ID="txtMinValue" runat="server" ClientIDMode="Static" CssClass="validate-numbers" Style="width: 60px;"></asp:TextBox>
                </div>


                <div class="formtextdiv_small">
                    Max Value
                </div>
                <div class="formdetaildiv_right">
                    :
                        <asp:TextBox ID="txtMaxValue" runat="server" ClientIDMode="Static" CssClass="validate-numbers" Style="width: 60px;"></asp:TextBox>
                </div>
            </div>
            <div id="div_isForEvery" style="display: none">
                <div class="formtextdiv_small">
                    Is Every
                </div>
                <div class="formdetaildiv_right">
                    :
                    <asp:CheckBox ID="chkIsForEvery" runat="server" ClientIDMode="Static" />
                </div>
            </div>

            <div>
                <a id="a_option_level" class="addlinkdiv1" style="margin-left: 10px">Add Product Option</a>
            </div>

            <div class="clearall"></div>
            <div id="grid_option_level" class="grid_margin"></div>
        </div>
    </div>

    <div id="div_free_product_form" style="display: none">
        <asp:HiddenField ID="HiddenField_ProductId" runat="server" ClientIDMode="Static" />
        <div id="div_message_popup"></div>
        <div>
            <div class="">
                <div class="formtextdiv">
                    Product
                </div>
                <div class="formdetaildiv">
                    :
                    <asp:TextBox ID="txtProduct" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                    <a href="#" id="id_schedule">
                        <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Packing Method
                </div>
                <div class="formdetaildiv_right">
                    :
                        <asp:DropDownList ID="cmdPackingType1" runat="server" CssClass="input-large select-different" ClientIDMode="Static" />
                    <%--<asp:TextBox ID="txtPackingType1" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_PackingType1">
                            <img id="img2" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>--%>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Quantity
                </div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtQty" runat="server" CssClass="validate-numbers" ClientIDMode="Static"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
            </div>
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="btnYes" class="k-button">
            Add</button>
        <button id="btnNo" class="k-button">
            No</button>


    </div>

    <div id="div_option_level_form" style="display: none">
        <%--<asp:HiddenField ID="HiddenField_BrandId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HiddenField_PackingTypeId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HiddenField_CatlogId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HiddenField_ItemGroupId" runat="server" ClientIDMode="Static" />--%>
        <asp:HiddenField ID="HiddenField_IndexId" runat="server" ClientIDMode="Static" Value="0" />


        <div class="clearall"></div>
        <div id="div_qty_limit">
            <div>
                <div class="formleft">
                    <div class="formtextdiv">
                        Brand
                    </div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:DropDownList ID="cmbBrand" runat="server" CssClass="input-large select-different" ClientIDMode="Static" />
                        <%--<asp:TextBox ID="txtBrand" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_brand">
                            <img id="img3" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>--%>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Packing Method
                    </div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:DropDownList ID="cmdPackingType" runat="server" CssClass="input-large select-different" ClientIDMode="Static" />
                        <%--<asp:TextBox ID="txtPackingType" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_PackingType">
                            <img id="img5" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>--%>
                    </div>
                    <div class="clearall">
                    </div>

                    <div class="formtextdiv">
                        Product
                    </div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:TextBox ID="txtSku" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <a href="#" id="a_Sku">
                            <img id="img2" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                    </div>
                    <div class="clearall">
                    </div>

                    <div id="div_isexcept">
                        <div class="formtextdiv">
                            Level Type
                        </div>
                        <div class="formdetaildiv_right">
                            :
                            <%--<asp:CheckBox ID="chkIsExcept" runat="server" ClientIDMode="Static" Text="" />--%>
                            <asp:DropDownList ID="ddlLevelType" runat="server" CssClass="input-large select-different"
                                ClientIDMode="Static">
                                <asp:ListItem Value="D" Text="Default"></asp:ListItem>
                                <asp:ListItem Value="E" Text="Exception"></asp:ListItem>
                                <asp:ListItem Value="C" Text="Compulsory"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="clearall">
                        </div>
                    </div>
                </div>
                <div class="formright">
                    <div class="formtextdiv">
                        Item Group
                    </div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:DropDownList ID="cmbItemGroup" runat="server" CssClass="input-large select-different" ClientIDMode="Static" />
                        <%--<asp:TextBox ID="txtItemGroup" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_ItemGroup">
                            <img id="img4" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>--%>
                    </div>
                    <div class="clearall">
                    </div>

                    <div class="formtextdiv">
                        Flavor
                    </div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:DropDownList ID="cmbFlavor" runat="server" CssClass="input-large select-different" ClientIDMode="Static" />
                        <%--<asp:TextBox ID="txtCatlog" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
                        <div style="line-height:30px; margin-top:3px;">
                            <a href="#" id="a_Catlog">
                            <img id="img6" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>--%>
                    </div>
                    <div class="clearall">
                    </div>

                    <div class="formtextdiv">
                        HVP
                    </div>
                    <div class="formdetaildiv_right">
                        :
                        <asp:DropDownList ID="cmbIsHVP" runat="server" CssClass="input-large select-different" ClientIDMode="Static">
                            <asp:ListItem Selected="True" Value="Any" Text="Any"></asp:ListItem>
                            <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="No" Text="No"></asp:ListItem>
                        </asp:DropDownList>
                        <%--<asp:CheckBox ID="chbIsHVP" runat="server"  ClientIDMode="Static"/>--%>
                    </div>
                    <div class="clearall">
                    </div>

                </div>
            </div>
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="btnOptionAdd" class="k-button">
            Add</button>
        <button id="btnCombinationAdd" class="k-button">
            Add</button>
        <button id="btnOptionNo" class="k-button">
            No</button>




    </div>

    <div id="program_window">
        <div id="Product_subwindow"></div>
    </div>
    <div id="schememodalWindow" style="display: none">
        <div id="div_schemeconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="btnRemoveYes" class="k-button">
            Yes</button>
        <button id="btnRemoveNo" class="k-button">
            No</button>
    </div>

    <%--<div>
            <div id="div_combination"  style="display: none">
                <a id="a_combination" class="addlinkdiv">Add Combination</a> 
                <div class="clearall">
                </div>
                <div id="grid_combination"></div>
           </div>
           
        </div>--%>
    <asp:HiddenField ID="HiddenField_combination_indexId" runat="server" ClientIDMode="Static" Value="0" />
</div>

<script type="text/javascript">

    var wnd1 = null;
    var wnd = null;
    //var wndRemove = null;

    $("#a_combination").click(function () {
        AddCombination();
    });

    function ShowFreeProductfirmation(ProductId) {
        var message = "Are you sure, You want to delete selected free product?";
        ShowRemoveFromSessiononfirmation(message, 'Delete free product', ProductId, 3, 0);
    }

    function ShowOptionLevelfirmation(indexId) {
        var message = "Are you sure, You want to delete selected option level?";
        ShowRemoveFromSessiononfirmation(message, 'Delete option level', indexId, 1, 0);
    }

    function ShowCombinationfirmation(indexId, optionId) {
        var message = "Are you sure, You want to delete selected combination?";
        ShowRemoveFromSessiononfirmation(message, 'Delete combination', indexId, 2, optionId);
    }

    function ShowSchemeDetailfirmation(indexId) {
        var message = "Are you sure, You want to delete selected scheme detail?";
        ShowRemoveFromSessiononfirmation(message, 'Delete scheme detail', indexId, 4, 0);
    }



    function GetCombinationList1(indexId) {

        // var take_grid = $("#MainContent_hfPageIndex").val();
        $("#grid_combination").html("");
        $("#grid_combination").kendoGrid({
            // height: 400,
            columns: [
                {
                    template: '<a href="javascript:ShowCombinationfirmation(#=IndexId#)">Delete</a>',
                    width: "50px"
                },
                {
                    template: '<a href="javascript:EditCombination(#=IndexId#,\'#=BrandName#\',\'#=ProductPackingName#\',\'#=CatlogName#\',\'#=ProductCatagoryName#\',#=BrandId#,#=ProductPackingId#,#=CatlogId#,#=ProductCategoryId#,#=IsHvp#,#=IsExceptProduct#)">Edit</a>',
                    width: "50px"
                },
                { field: "ProductCatagoryName", title: "Item Group", width: "112px", hidden: false },
                { field: "BrandName", title: "Brand", width: "120px", hidden: false },
                { field: "CatlogName", title: "Flavor", width: "130px", hidden: false },
                { field: "ProductPackingName", title: "Packing Method", width: "100px", hidden: false },
                { field: "IsHvp", title: "HVP", width: "100px", hidden: false }
            ],
            editable: false, // enable editing
            pageable: false,
            sortable: false,
            filterable: false,
            selectable: "single",
            columnMenu: false,
            //dataBound: LeadCustomerGrid_onDataBound,
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetCombinationList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        data: { indexId: indexId },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }//,
                //aggregate: [{ field: "rowCount", aggregate: "max"}]
            }
        });
    }



</script>

<script type="text/javascript">

    $("#a_add_scheme").click(function () {
        AddSchemeToSession();
    });

    $("#a_cancel").click(function () {

        $('#div_scheme_detail').css('display', 'block');
        $('#itemcontent').css('display', 'none');
        $("#div_combination").css("display", "none");

        $('#a_add_scheme').css('display', 'none');
        $('#a_cancel').css('display', 'none');

        $('#MainContent_buttonbar_buttinSave').css('display', 'inline-block');
        $('#a_add_option').css('display', 'inline-block');
    });

    $("#DropDownListOperator").change(function () {
        if ($(this).val() == 6) {
            $("#div_between").css("display", "block");
        } else {
            $("#div_between").css("display", "none");
        }
    });

    $("#DropDownListSchemeType").change(function () {
        SetSchemeTypeUi($(this).val());
    });

    function SetSchemeTypeUi(id) {
        if (id == 1) {
            $("#div_typebill").css("display", "block");
            $("#div_qty_val").css("display", "none");
            $("#div_bill").css("display", "block");
            $('#chkIsBill').prop('checked', true);

        } else {
            $("#div_typebill").css("display", "none");
            $("#div_qty_val").css("display", "block");
            $("#div_bill").css("display", "none");
            $('#chkIsBill').prop('checked', false);
        }
    }

    $("#chkIsBill").change(function () {
        if (this.checked) {
            $("#div_qty_val").css("display", "none");
            $("#div_bill").css("display", "block");
        } else {
            $("#div_qty_val").css("display", "block");
            $("#div_bill").css("display", "none");
        }
    });

    $("#a_option_level").click(function () {
        //AddOptionLevel();
        AddOptionLevelToSession();
    });


    function OptionLevel_onChange(arg) {
        var selectedItem = this.dataItem(this.select());
        $("#HiddenField_IndexId").val(selectedItem.IndexId);
        $("#div_combination").css("display", "block");
        GetCombinationList(selectedItem.IndexId);
    }

    function GetOptionLevelList1() {

        // var take_grid = $("#MainContent_hfPageIndex").val();
        $("#grid_option_level").html("");
        $("#grid_option_level").kendoGrid({
            // height: 400,
            columns: [
                {
                    template: '<a href="javascript:ShowOptionLevelfirmation(#=IndexId#)">Delete</a>',
                    width: "50px"
                },
                {
                    template: '<a href="javascript:EditOptionLevel(#=IndexId#,\'#=BrandName#\',\'#=ProductPackingName#\',\'#=CatlogName#\',\'#=ProductCatagoryName#\',#=BrandId#,#=ProductPackingId#,#=CatlogId#,#=ProductCategoryId#,#=MaxQuantity#,#=MinQuantity#,#=MaxValue#,#=MinValue#,#=OperatorId#,#=IsBill#,#=IsHvp#)">Edit</a>',
                    width: "50px"
                },
                //            { template: '<a href="javascript:EditFreeProduct(#=ProductId#,#=Qty#,\'#=ProductName#\')">#=ProductName#</a>',
                //                field: "ProductName", width: "200px", title: "SKU"
                //            },
                //{field: "ProductName", title: "SKU", width: "85px", hidden: false },
                { field: "ProductCatagoryName", title: "Item Group", width: "112px", hidden: false },
                { field: "BrandName", title: "Brand", width: "120px", hidden: false },
                { field: "CatlogName", title: "Flavor", width: "130px", hidden: false },
                { field: "ProductPackingName", title: "Packing Method", width: "100px", hidden: false },
                { field: "IsHvp", title: "HVP", width: "100px", hidden: false },
                { field: "MinQuantity", title: "Min Quantity", width: "100px", hidden: false },
                { field: "MaxQuantity", title: "Max Quantity", width: "100px", hidden: false },
                { field: "MinValue", title: "Min Value", width: "100px", hidden: false },
                { field: "MaxValue", title: "Max Value", width: "100px", hidden: false }
            ],
            editable: false, // enable editing
            pageable: false,
            sortable: false,
            filterable: false,
            selectable: "single",
            columnMenu: false,
            change: OptionLevel_onChange,
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetOptionLevelList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer', pgindex: take_grid },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }//,
                //aggregate: [{ field: "rowCount", aggregate: "max"}]
            }
        });
    }


    $(document).ready(function () {


        LoadOptionLevelPopUp();
        GetOptionLevelList();
        LoadPopUp();


    });

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>
<script type="text/javascript">

    $("#DropDownListDetailType").change(function () {
        if ($(this).val() == 1 || $(this).val() == 4) {
            $('#div_Percentage').css('display', 'none');
            $('#div_value').css('display', 'block');
            $('#div_free_product').css('display', 'none');
            $('#div_isForEvery').css('display', 'block');
        }
        else if ($(this).val() == 2 || $(this).val() == 5) {
            $('#div_Percentage').css('display', 'block');
            $('#div_value').css('display', 'none');
            $('#div_free_product').css('display', 'none');
            $('#div_isForEvery').css('display', 'block');
        }
        else if ($(this).val() == 3) {
            GetFreeProductList();
            $('#div_Percentage').css('display', 'none');
            $('#div_value').css('display', 'none');
            $('#div_free_product').css('display', 'block');
            $('#div_isForEvery').css('display', 'block');
        }
    });




    $("#a_product").click(function () {
        AddFreeProduct();
    });

    $("#id_schedule").click(function () {
        loadFreeProductList();
    });

    $("#a_Sku").click(function () {
        loadFreeProductList();
    });





    //    function OpenSelectFreeProduct(productId, name) {
    //        $("#HiddenField_ProductId").val(productId);
    //        $("#txtProduct").val(name);
    //        $("#txtSku").val(name);
    //        $("#program_window").data("kendoWindow").close();
    //    }


    function OpenSelectFreeProduct(productId, name, categoryId, flavourId, packingId, brandId, isHighValue) {
        $("#HiddenField_ProductId").val(productId);
        $("#txtProduct").val(name);
        $("#txtSku").val(name);

        $("#cmbBrand").val(brandId);
        $("#cmdPackingType").val(packingId);
        $("#cmbFlavor").val(flavourId);
        $("#cmbItemGroup").val(categoryId);
        $('#chbIsHVP').prop('checked', isHighValue);


        $("#program_window").data("kendoWindow").close();
    }

    $("#chbIsHVP").click(function () {
        $("#HiddenField_ProductId").val(0);
        $("#txtProduct").val("Any");
        $("#txtSku").val("Any");
    });

    $("#cmbBrand").change(function () {
        $("#HiddenField_ProductId").val(0);
        $("#txtProduct").val("Any");
        $("#txtSku").val("Any");
    });

    $("#cmdPackingType").change(function () {
        $("#HiddenField_ProductId").val(0);
        $("#txtProduct").val("Any");
        $("#txtSku").val("Any");
    });

    $("#cmbFlavor").change(function () {
        $("#HiddenField_ProductId").val(0);
        $("#txtProduct").val("Any");
        $("#txtSku").val("Any");
    });

    $("#cmbItemGroup").change(function () {
        $("#HiddenField_ProductId").val(0);
        $("#txtProduct").val("Any");
        $("#txtSku").val("Any");
    });




    function OpenSelectBrand(brandId, name) {
        $("#HiddenField_BrandId").val(brandId);
        $("#txtBrand").val(name);
        $("#program_window").data("kendoWindow").close();
    }
    $("#a_brand").click(function () {
        loadBrandList();
    });
    function loadBrandList() {
        $("#Product_subwindow").html("");
        $("#Product_subwindow").kendoGrid({
            height: 300,
            columns: [
                { field: "BrandId", title: "BrandId", width: "130px", hidden: false },
                {
                    template: '<a href="javascript:OpenSelectBrand(#=BrandId#,\'#=BrandName#\')">#=BrandName#</a>',
                    field: "Brand Name"
                }

            ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            //filterable: true,
            selectable: "single",
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 10,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            ProgramId: { validation: { required: true } },
                            ProgramName: { validation: { required: true } },
                            Status: { validation: { required: true } }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllBrandsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {

                            if (textStatus == "success") {
                                //$("#program_window").data("kendoWindow").open();

                                var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                        .bind("click", function () {
                                            newcust_window.data("kendoWindow").open();
                                            newcust_undo.hide();
                                        });

                                var onClose = function () {
                                    newcust_undo.show();
                                }

                                if (!newcust_window.data("kendoWindow")) {
                                    newcust_window.kendoWindow({
                                        width: "500px",
                                        height: "400px",
                                        title: "Lookup",
                                        close: onClose
                                    });
                                }

                                newcust_window.data("kendoWindow").center().open();

                                $("div#div_loader").hide();
                            }
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }



    function OpenSelectPackingType(PackingTypeId, name) {
        $("#HiddenField_PackingTypeId").val(PackingTypeId);
        $("#txtPackingType1").val(name);
        $("#txtPackingType").val(name);
        $("#program_window").data("kendoWindow").close();
    }
    $("#a_PackingType").click(function () {
        loadPackingTypeList();
    });
    $("#a_PackingType1").click(function () {
        loadPackingTypeList();
    });
    function loadPackingTypeList() {
        $("#Product_subwindow").html("");
        $("#Product_subwindow").kendoGrid({
            height: 300,
            columns: [
                { field: "Code", title: "Packing Type Id", width: "130px", hidden: false },
                {
                    template: '<a href="javascript:OpenSelectPackingType(#=Code#,\'#=Description#\')">#=Description#</a>',
                    field: "Packing Type"
                }

            ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            //filterable: true,
            selectable: "single",
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            ProgramId: { validation: { required: true } },
                            ProgramName: { validation: { required: true } },
                            Status: { validation: { required: true } }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetAllPackingTypeDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        //data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {

                            if (textStatus == "success") {
                                //$("#program_window").data("kendoWindow").open();

                                var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                        .bind("click", function () {
                                            newcust_window.data("kendoWindow").open();
                                            newcust_undo.hide();
                                        });

                                var onClose = function () {
                                    newcust_undo.show();
                                }

                                if (!newcust_window.data("kendoWindow")) {
                                    newcust_window.kendoWindow({
                                        width: "500px",
                                        height: "400px",
                                        title: "Lookup",
                                        close: onClose
                                    });
                                }

                                newcust_window.data("kendoWindow").center().open();

                                $("div#div_loader").hide();
                            }
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }



    function OpenSelectCatlog(CatlogId, name) {
        $("#HiddenField_CatlogId").val(CatlogId);
        $("#txtCatlog").val(name);
        $("#program_window").data("kendoWindow").close();
    }
    $("#a_Catlog").click(function () {
        loadCatlogList();
    });
    function loadCatlogList() {
        $("#Product_subwindow").html("");
        $("#Product_subwindow").kendoGrid({
            height: 300,
            columns: [
                // { field: "CatlogId", title: "Catlog Id", width: "130px", hidden: false },
                {
                    template: '<a href="javascript:OpenSelectCatlog(#=CatlogId#,\'#=Description#\')">#=Description#</a>',
                    field: "Flavor"
                }

            ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            //filterable: true,
            selectable: "single",
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            ProgramId: { validation: { required: true } },
                            ProgramName: { validation: { required: true } },
                            Status: { validation: { required: true } }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetCatalogDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        //data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {

                            if (textStatus == "success") {
                                //$("#program_window").data("kendoWindow").open();

                                var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                        .bind("click", function () {
                                            newcust_window.data("kendoWindow").open();
                                            newcust_undo.hide();
                                        });

                                var onClose = function () {
                                    newcust_undo.show();
                                }

                                if (!newcust_window.data("kendoWindow")) {
                                    newcust_window.kendoWindow({
                                        width: "500px",
                                        height: "400px",
                                        title: "Lookup",
                                        close: onClose
                                    });
                                }

                                newcust_window.data("kendoWindow").center().open();

                                $("div#div_loader").hide();
                            }
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }




    function OpenSelectProductCategory(PackingTypeId, name) {
        $("#HiddenField_ItemGroupId").val(PackingTypeId);
        $("#txtItemGroup").val(name);
        $("#program_window").data("kendoWindow").close();
    }
    $("#a_ItemGroup").click(function () {
        loadProductCategoryList();
    });
    function loadProductCategoryList() {
        $("#Product_subwindow").html("");
        $("#Product_subwindow").kendoGrid({
            height: 300,
            columns: [
                { field: "Code", title: "Item Group id", width: "130px", hidden: false },
                {
                    template: '<a href="javascript:OpenSelectProductCategory(#=Code#,\'#=Description#\')">#=Description#</a>',
                    field: "Item Group"
                }

            ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            //filterable: true,
            selectable: "single",
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            ProgramId: { validation: { required: true } },
                            ProgramName: { validation: { required: true } },
                            Status: { validation: { required: true } }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetAllProductCategoryDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        //data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {

                            if (textStatus == "success") {
                                //$("#program_window").data("kendoWindow").open();

                                var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                        .bind("click", function () {
                                            newcust_window.data("kendoWindow").open();
                                            newcust_undo.hide();
                                        });

                                var onClose = function () {
                                    newcust_undo.show();
                                }

                                if (!newcust_window.data("kendoWindow")) {
                                    newcust_window.kendoWindow({
                                        width: "500px",
                                        height: "400px",
                                        title: "Lookup",
                                        close: onClose
                                    });
                                }

                                newcust_window.data("kendoWindow").center().open();

                                $("div#div_loader").hide();
                            }
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }





</script>
