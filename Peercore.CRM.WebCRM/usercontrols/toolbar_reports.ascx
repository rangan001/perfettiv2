﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="toolbar_reports.ascx.cs" Inherits="usercontrols_toolbar_reports" %>
<nav class="menu1">
    <ul>
        <li> <a id="a_trade_load_summary" href="~/dashboard/transaction/TradeLoadSummary.aspx" runat="server"><img src="../../assets/images/activity_analysis_icon.png"/>Trade Load Summary</a> </li>
        <li> <a id="a_item_wise_sales" href="~/dashboard/transaction/ItemWiseSales.aspx" runat="server"><img src="../../assets/images/sales_reports_icon.png"/>Item Wise Sales</a> </li>
        <li> <a id="a_invoice_summary" href="~/dashboard/transaction/InvoiceSummary.aspx" runat="server"><img src="../../assets/images/opportunity_conversion_icon.png"/>Invoice Summary</a> </li>
        <li> <a id="a_invoice_detail" href="~/dashboard/transaction/invoice_fulldetails.aspx" runat="server"><img src="../../assets/images/opportunity_conversion_icon.png"/>Invoice Details</a> </li>
        <li> <a id="a_distrib_summary" href="~/dashboard/transaction/DistributorSummary.aspx" runat="server"><img src="../../assets/images/activity_analysis_icon.png"/>Distributor Summary</a> </li>
        <li> <a id="a_mssr_stock_sales" href="~/dashboard/transaction/MSSRStockSales.aspx" runat="server"><img src="../../assets/images/activity_analysis_icon.png"/>MSSR Stock & Sales</a></li>
        <li> <a id="a_master_details" href="~/dashboard/transaction/Master_Details.aspx" runat="server"><img src="../../assets/images/opportunity_conversion_icon.png"/>Master</a> </li>
        <li> <a id="a_other_reports" href="~/dashboard/transaction/OtherReports.aspx" runat="server"><img src="../../assets/images/activity_analysis_icon.png"/>Other Reports</a> </li>
        <%--<li> <a id="a_callcycle" href="~/dashboard/transaction/CallCycleContact.aspx" runat="server"><img src="../../assets/images/call_cycle_icon.png">Call Cycle Analysis</a> </li>
        <li> <a id="a_opportunity" href="~/dashboard/transaction/OpportunityAnalysis.aspx" runat="server"><img src="../../assets/images/opportunity_conversion_icon.png">Opportunity Conversion</a> </li>
        <li> <a id="a_enduser" href="~/dashboard/transaction/EndUserEnquiry.aspx" runat="server"><img src="../../assets/images/end_user_sales_icon.png">End User Sales Enquiry</a> </li>
        <li> <a id="a_sales" href="~/dashboard/transaction/EOISales.aspx" runat="server"><img src="../../assets/images/sales_reports_icon.png">Sales Report</a> </li>
        <li> <a id="a_audit" href="~/dashboard/transaction/AuditTrail.aspx" runat="server"><img src="../../assets/images/sales_reports_icon.png">Audit Trail</a> </li>--%>

    </ul>
</nav>
<%--<ul id="nav">
    <li><a href="#"><span>
        <img src="../../assets/images/icon-menu.png" />Menu</span></a>
        <div class="subs">
            <div class="col">
                <ul>
                    <li><a id="a1" href="~/dashboard/transaction/TradeLoadSummary.aspx" runat="server">
                        <img src="../../assets/images/activity_analysis_icon.png">Trade Load Summary</a>
                    </li>--%>
                    <%--<li><a id="a2" href="~/dashboard/transaction/CallCycleContact.aspx" runat="server">
                        <img src="../../assets/images/call_cycle_icon.png">Call Cycle Analysis</a> </li>
                    <li><a id="a3" href="~/dashboard/transaction/OpportunityAnalysis.aspx" runat="server">
                        <img src="../../assets/images/opportunity_conversion_icon.png">Opportunity Conversion</a>
                    </li>
                    <li><a id="a4" href="~/dashboard/transaction/EndUserEnquiry.aspx" runat="server">
                        <img src="../../assets/images/end_user_sales_icon.png">End User Sales Enquiry</a>
                    </li>
                    <li><a id="a5" href="~/dashboard/transaction/EOISales.aspx" runat="server">
                        <img src="../../assets/images/sales_reports_icon.png">Sales Report</a> </li>
                    <li><a id="a6" href="~/dashboard/transaction/AuditTrail.aspx" runat="server">
                        <img src="../../assets/images/sales_reports_icon.png">Audit Trail</a> </li>--%>
<%--                </ul>
            </div>
        </div>
    </li>
</ul>--%>
<script type="text/javascript">
    $("ul#nav").click(function () {
        $("ul#nav .subs").toggle();
    });
</script>