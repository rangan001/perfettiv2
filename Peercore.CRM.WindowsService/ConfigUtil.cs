﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Peercore.CRM.WindowsService
{
    public static class ConfigUtil
    {
        public static string ApplicationPath
        {
            get { return ConfigurationManager.AppSettings["AppPath"]; }
        }

        /// <summary>
        /// Returns application path
        /// </summary>
        public static int RowCount
        {
            get { return int.Parse(ConfigurationManager.AppSettings["RowCount"].ToString()); }
        }

        /// <summary>
        /// Returns Max Row Count.
        /// </summary>
        public static int MaxRowCount
        {
            get { return int.Parse(ConfigurationManager.AppSettings["MaxRowCount"].ToString()); }
        }

        /// <summary>
        /// Returns Start Index
        /// </summary>
        public static int StartIndex
        {
            get { return int.Parse(ConfigurationManager.AppSettings["StartIndex"].ToString()); }
        }

        /// <summary>
        /// Returns Look Market Codee
        /// </summary>
        public static string MarketCode
        {
            get { return ConfigurationManager.AppSettings["Market"].ToString(); }
        }

        /// <summary>
        /// Returns Look Cheque Payment Status Code
        /// </summary>
        public static string ChequePaymentStatus
        {
            get { return ConfigurationManager.AppSettings["ChequeStatus"].ToString(); }
        }

        /// <summary>
        /// Returns Look Payment Settlement Type Code
        /// </summary>
        public static string PaymentSettlementType
        {
            get { return ConfigurationManager.AppSettings["PaymentSettlementType"].ToString(); }
        }

        /// <summary>
        /// Returns Look Channel Codee
        /// </summary>
        public static string ChannelCode
        {
            get { return ConfigurationManager.AppSettings["Channel"].ToString(); }
        }

        /// <summary>
        /// Returns Look Source Codee
        /// </summary>
        public static string SourceCode
        {
            get { return ConfigurationManager.AppSettings["Source"].ToString(); }
        }

        /// <summary>
        /// Returns DateTimePattern
        /// </summary>
        public static string DateTimePattern
        {
            get { return ConfigurationManager.AppSettings["DateTimePattern"].ToString(); }
        }

        /// <summary>
        /// Returns SendAppointmentsToOutlook
        /// </summary>
        public static bool SendAppointmentsToOutlook
        {
            get
            {
                if (ConfigurationManager.AppSettings["SendAppointmentsToOutlook"] == "1")
                    return true;
                else if (ConfigurationManager.AppSettings["SendAppointmentsToOutlook"] == "0")
                    return false;

                return false;
            }
        }

        /// <summary>
        /// Returns StreetAddressType
        /// </summary>
        public static string StreetAddressType
        {
            get { return ConfigurationManager.AppSettings["StreetAddressType"].ToString(); }
        }

        /// <summary>
        /// Returns PostalAddressType
        /// </summary>
        public static string PostalAddressType
        {
            get { return ConfigurationManager.AppSettings["PostalAddressType"].ToString(); }
        }

        public static string ClearState
        {
            get { return ConfigurationManager.AppSettings["ClearState"].ToString(); }
        }

        /// <summary>
        /// Returns Look Channel CodeeSmtpServer
        /// </summary>
        public static string SmtpServer
        {
            get { return ConfigurationManager.AppSettings["SmtpServer"].ToString(); }
        }

        /// <summary>
        /// Returns SmtpPort
        /// </summary>
        public static int SmtpPort
        {
            get { return int.Parse(ConfigurationManager.AppSettings["SmtpPort"].ToString()); }
        }

        /// <summary>
        /// Returns FromMail
        /// </summary>
        public static string FromMail
        {
            get { return ConfigurationManager.AppSettings["FromMail"].ToString(); }
        }

        /// <summary>
        /// Returns Error To Mail
        /// </summary>
        public static string ErrorToMail
        {
            get { return ConfigurationManager.AppSettings["ErrorToMail"].ToString(); }
        }

        /// <summary>
        /// Returns Error Cc Mail
        /// </summary>
        public static string ErrorCcMail
        {
            get { return ConfigurationManager.AppSettings["ErrorCcMail"].ToString(); }
        }

        /// <summary>
        /// Returns ErrorRedirectionPages
        /// </summary>
        public static string ErrorRedirectionPages
        {
            get { return ConfigurationManager.AppSettings["ErrorRedirectionPages"].ToString(); }
        }

        /// <summary>
        /// Returns DateTimePatternForSql
        /// </summary>
        public static string DatePatternForSql
        {
            get { return ConfigurationManager.AppSettings["DatePatternForSql"].ToString(); }
        }

        /// <summary>
        /// Returns DateTimePatternForSql
        /// </summary>
        public static string DateTimePatternForSql
        {
            get { return ConfigurationManager.AppSettings["DateTimePatternForSql"].ToString(); }
        }

        /// <summary>
        /// Returns DefaultDepartmentID
        /// </summary>
        public static string DefaultDepartmentID
        {
            get { return ConfigurationManager.AppSettings["DefaultDepartmentID"].ToString(); }
        }

        /// <summary>
        /// Returns NumberFormat
        /// </summary>
        public static string NumberFormat
        {
            get { return ConfigurationManager.AppSettings["NumberFormat"].ToString(); }
        }

        /// <summary>
        /// Returns CRMModules
        /// </summary>
        public static string CRMModules
        {
            get { return ConfigurationManager.AppSettings["CRMModules"].ToString(); }
        }

        public static string SaveDocPath
        {
            get { return ConfigurationManager.AppSettings["SaveDocPath"].ToString(); }
        }

        public static string ImagePath
        {
            get { return ConfigurationManager.AppSettings["ImagePath"].ToString(); }
        }

        public static string UtilityPath
        {
            get { return ConfigurationManager.AppSettings["UtilityPath"].ToString(); }
        }


        public static string PublicIp
        {
            get { return ConfigurationManager.AppSettings["PublicIp"]; }
        }

        public static string MobitelSMSUserName
        {
            get { return ConfigurationManager.AppSettings["MobitelSMSUserName"]; }
        }

        public static string MobitelSMSPassword
        {
            get { return ConfigurationManager.AppSettings["MobitelSMSPassword"]; }
        }

        public static string MobitelSMSAlias
        {
            get { return ConfigurationManager.AppSettings["MobitelSMSAlias"]; }
        }

        public static string CCNumberList
        {
            get { return ConfigurationManager.AppSettings["CCNumberList"]; }
        }

        public static int SMSGroupSize
        {
            get { return int.Parse(ConfigurationManager.AppSettings["SMSGroupSize"].ToString()); }
        }

        public static int Minutes
        {
            get { return int.Parse(ConfigurationManager.AppSettings["Minutes"].ToString()); }
        }
    }
}
