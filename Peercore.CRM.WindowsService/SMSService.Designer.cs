﻿namespace Peercore.CRM.WindowsService
{
    partial class SMSService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.getAllRepsTableAdapter = new Peercore.CRM.WindowsService.CRMDataSetTableAdapters.GETAllRepsTableAdapter();
            this.getAllRepsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cRMDataSet = new Peercore.CRM.WindowsService.CRMDataSet();
            ((System.ComponentModel.ISupportInitialize)(this.getAllRepsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cRMDataSet)).BeginInit();
            // 
            // getAllRepsTableAdapter
            // 
            this.getAllRepsTableAdapter.ClearBeforeFill = true;
            // 
            // getAllRepsBindingSource
            // 
            this.getAllRepsBindingSource.DataMember = "GetAllReps";
            this.getAllRepsBindingSource.DataSource = this.cRMDataSet;
            // 
            // cRMDataSet
            // 
            this.cRMDataSet.DataSetName = "CRMDataSet";
            this.cRMDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Service1
            // 
            this.ServiceName = "Service1";
            ((System.ComponentModel.ISupportInitialize)(this.getAllRepsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cRMDataSet)).EndInit();

        }

        #endregion

        private CRMDataSetTableAdapters.GETAllRepsTableAdapter getAllRepsTableAdapter;
        private System.Windows.Forms.BindingSource getAllRepsBindingSource;
        private CRMDataSet cRMDataSet;

    }
}
