﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using Peercore.CRM.WindowsService.SMSServiceReference;
using System.IO;
using Peercore.CRM.WindowsService.CRMServiceReference;

namespace Peercore.CRM.WindowsService
{
    public partial class SMSService : ServiceBase
    {
        EnterpriseSMSWSClient client = new EnterpriseSMSWSClient();
        public System.Timers.Timer timer;
        public static double TimeFrequency;

        public SMSService()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            ////// Add By Irosh for Test-------------------------------------------------------------------
            //SMSServiceReference.user user = new SMSServiceReference.user();
            //user.username = ConfigUtil.MobitelSMSUserName;
            //user.password = ConfigUtil.MobitelSMSPassword;

            //SMSServiceReference.alias alias = new SMSServiceReference.alias();
            //alias.alias1 = ConfigUtil.MobitelSMSAlias;
            //EnterpriseSMSWSClient client = new EnterpriseSMSWSClient();
            //longNumber ln = new longNumber();
            //ln.longNumber1 = "0714457246";

            //session s = client.createSession(user);
            //smsMessage[] statusList = client.getMessagesFromLongNUmber(s, ln);
            //////------------------------------------------------------------------------------------------

            timer = new System.Timers.Timer();
            TimeFrequency = 30000;//ConfigUtil.Minutes * 60 * 1000;
            timer.Interval = TimeFrequency; // half an hour
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            LoadReps();
        }

        private void LoadReps()
        {
            string date = String.Format("{0:yyyy-MM-dd}", DateTime.Now);
            DateTime time = DateTime.Parse(date + " 10:05");

            if (DateTime.Now >= time)
            {
                ArgsDTO args = new ArgsDTO();
                args.OrderBy = " name ASC ";
                args.AdditionalParams = " ";
                args.StartIndex = 1;
                args.RowCount = 500;
                OriginatorClient originatorClient = new OriginatorClient();

                this.getAllRepsTableAdapter.Fill(this.cRMDataSet.GETAllReps);

                foreach (DataRow dr in this.cRMDataSet.GETAllReps.Rows)
                {
                    //// Add By Irosh for Test-------------------------------------------------------------------
                    SMSServiceReference.user user = new SMSServiceReference.user();
                    user.username = ConfigUtil.MobitelSMSUserName;
                    user.password = ConfigUtil.MobitelSMSPassword;

                    SMSServiceReference.alias alias = new SMSServiceReference.alias();
                    alias.alias1 = ConfigUtil.MobitelSMSAlias;
                    EnterpriseSMSWSClient client = new EnterpriseSMSWSClient();
                    longNumber ln = new longNumber();
                    ln.longNumber1 = "0714457246";

                    session s = client.createSession(user);
                    smsMessage[] statusList = client.getMessagesFromLongNUmber(s, ln);
                    // //-----------------------------------------------------------------------------------------
                }
            }
        }
    }
}
