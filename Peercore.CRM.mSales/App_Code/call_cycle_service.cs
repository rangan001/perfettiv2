﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.ServiceModel.Web;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class call_cycle_service : System.Web.Services.WebService
{

    public call_cycle_service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCallCycle(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        if (!String.IsNullOrEmpty(UserSession.Instance.RepCode))
            argsDTO.ChildOriginators = "(" + argsDTO.ChildOriginators + " OR c.rep_code =\'" + UserSession.Instance.RepCode + "\')";
        argsDTO.AdditionalParams = "";

        if (sortExpression != null && sortExpression.Count > 0)
        {
            argsDTO.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetSortTypeCallCycles(item.Field);
                if (string.IsNullOrEmpty(argsDTO.OrderBy))
                    argsDTO.OrderBy = item.ToExpression();
                else
                    argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            argsDTO.OrderBy = "CallCycleID ASC";
        }
        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = take;

        List<CallCycleDTO> listCallCycleContact = new List<CallCycleDTO>();
        CallCycleClient callCycleClient = new CallCycleClient();

        listCallCycleContact = callCycleClient.GetCallCycle(argsDTO, false, 0, "");

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (listCallCycleContact != null && listCallCycleContact.Count > 0)
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = listCallCycleContact[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    private string GetSortTypeCallCycles(string sortName)
    {
        string sortingType = "";

        try
        {
            if (sortName == "CallCycleID")
            {
                sortingType = "callcycle_id";
            }
            else if (sortName == "SourceId")
            {
                sortingType = "lead_id";
            }
            else if (sortName == "Schedule")
            {
                sortingType = "description";
            }
            else if (sortName == "DueOn")
            {
                sortingType = "dueon";
            }
            else if (sortName == "Name")
            {
                sortingType = "lead_name";
            }
            else if (sortName == "Originator")
            {
                sortingType = "created_by";
            }
            else if (sortName == "LeadStage")
            {
                sortingType = "lead_stage";
            }
            else if (sortName == "Address")
            {
                sortingType = "address";
            }
            else if (sortName == "City")
            {
                sortingType = "city";
            }
            else if (sortName == "State")
            {
                sortingType = "state";
            }
            else if (sortName == "PostalCode")
            {
                sortingType = "postcode";
            }
            else if (sortName == "LeadStage.StageName")
            {
                sortingType = "lead_Stage";
            }
            else if (sortName == "Contact.Address")
            {
                sortingType = "address";
            }
            else if (sortName == "Contact.Name")
            {
                sortingType = "lead_name";
            }
            else if (sortName == "Contact.City")
            {
                sortingType = "city";
            }
            else if (sortName == "Contact.State")
            {
                sortingType = "state";
            }
            else if (sortName == "Contact.PostalCode")
            {
                sortingType = "postcode";
            }


            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCallCycleContacts(int CallCycleID, int type, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
        if (!String.IsNullOrEmpty(UserSession.Instance.RepCode))
            argsDTO.ChildOriginators = "(" + argsDTO.ChildOriginators + " OR c.rep_code =\'" + UserSession.Instance.RepCode + "\')";
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;


        if (sortExpression != null && sortExpression.Count > 0)
        {
            argsDTO.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetSortTypeCallCycles(item.Field);
                if (string.IsNullOrEmpty(argsDTO.OrderBy))
                    argsDTO.OrderBy = item.ToExpression();
                else
                    argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            argsDTO.OrderBy = "callcycle_id ASC";
        }
        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = 1000;// take;

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "outletsforschedule");
            argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
        CallCycleClient callCycleClient = new CallCycleClient();

        if (type == 0)
        {
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
        }
        else
        {
            if (CallCycleID != 0)
                listCallCycleContact = callCycleClient.GetCallCycleContacts(argsDTO, CallCycleID, false, "");
            else
                listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            //Session[CommonUtility.CALL_CYCLE_ID] = CallCycleID;
            Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (listCallCycleContact != null && listCallCycleContact.Count > 0)
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = listCallCycleContact.Count;
        }
        else
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllCallCyclesList(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        try
        {
            argsDTO.Originator = UserSession.Instance.UserName;
            argsDTO.OriginatorType = UserSession.Instance.OriginatorString;
            argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators.Replace("originator", "created_by");
            if (!String.IsNullOrEmpty(UserSession.Instance.RepCode))
                argsDTO.ChildOriginators = "(" + argsDTO.ChildOriginators + " OR rep_code =\'" + UserSession.Instance.RepCode + "\')";
            argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            argsDTO.AdditionalParams = " ";

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "callcycles", "");
                argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortTypeLookup(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = "description ASC";
            }
            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;

            //List<CallCycleDTO> listCallCycleContact = new List<CallCycleDTO>();
            CallCycleClient callCycleClient = new CallCycleClient();

            List<LookupDTO> lookupList = callCycleClient.GetAllCallCyclesList(argsDTO);
            //listCallCycleContact = callCycleClient.GetCallCycle(argsDTO, false, 0, "");

            DataSourceResult dataSourceResult = new DataSourceResult();

            if (lookupList != null && lookupList.Count > 0)
            {
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = lookupList[0].rowCount;
            }
            else
            {
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = 0;

            }
            //OpportunityService.Close();

            return dataSourceResult;
        }
        catch { return null; }
    }

    private string GetSortTypeLookup(string sortName)
    {
        string sortingType = "";

        try
        {
            if (sortName == "Code")
            {
                sortingType = "callcycle_id";
            }
            else if (sortName == "Description")
            {
                sortingType = "description";
            }

            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public List<OriginatorDTO> GetRepsByOriginator(string name)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();
        List<OriginatorDTO> returnList = new List<OriginatorDTO>();
        DataSourceResult dataSourceResult = new DataSourceResult();

        ////string username = UserSession.Instance.UserName;
        //string username = UserSession.Instance.ChildOriginators.Replace("originator", "o.originator"); ;


        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.AdditionalParams = " dept_string = 'DR'";
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 1000;// ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";
        try
        {
            //lstOriginatorDTO = originatorClient.GetRepsByOriginator(name, username);
            lstOriginatorDTO = originatorClient.GetOriginatorsByCatergory(args);
            foreach (OriginatorDTO item in lstOriginatorDTO)
            {
                //result.Add(item.CustomerCode+ " "+item.Name);
                returnList.Add(item);
            }
            return returnList;
        }
        catch (Exception)
        {

            return null;
        }
    }


    [WebMethod(EnableSession = true)]
    public List<OriginatorDTO> GetRepsByOriginator_For_Route_Assign(string name)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();
        List<OriginatorDTO> returnList = new List<OriginatorDTO>();
        DataSourceResult dataSourceResult = new DataSourceResult();

        ////string username = UserSession.Instance.UserName;
        //string username = UserSession.Instance.ChildOriginators.Replace("originator", "o.originator"); ;

        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.Originator = UserSession.Instance.UserName;
        args.OriginatorType = UserSession.Instance.OriginatorString;
        if (name.Length > 0)
            args.AdditionalParams = " dept_string = 'DR' AND ori.name like '%" + name + "%'";
        else
            args.AdditionalParams = " dept_string = 'DR'";
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 1000; // ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        try
        {
            //lstOriginatorDTO = originatorClient.GetRepsByOriginator(name, username);
            lstOriginatorDTO = originatorClient.GetRepsByOriginator_For_Route_Assign(args);
            foreach (OriginatorDTO item in lstOriginatorDTO)
            {
                //result.Add(item.CustomerCode + " " + item.Name);
                returnList.Add(item);
            }
            return returnList;
        }
        catch (Exception)
        {
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAssignedRoutes(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CallCycleClient callCycleClient = new CallCycleClient();

        List<RouteDTO> data = new List<RouteDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            //args.RowCount = 10;
            args.RowCount = take;

            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "o.originator"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAssignedRoutesSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " id desc";
                }
            }
            else
            {
                args.OrderBy = " id desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "assignedroutes");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = callCycleClient.GetAssignedRoutes(parentOriginator, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAssignedRoutesSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " a.route_name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RouteName":
                sortStr = " a.route_name " + sortoption;
                break;
            case "RepCode":
                sortStr = " a.rep_name " + sortoption;
                break;
            case "RepName":
                sortStr = " a.rep_name " + sortoption;
                break;
            default:
                sortStr = " a.route_name desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public List<RouteMasterDTO> GetRouteNames(string name)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CallCycleClient callCycleClient = new CallCycleClient();
        List<RouteMasterDTO> lstRouteDTO = new List<RouteMasterDTO>();
        List<RouteMasterDTO> returnList = new List<RouteMasterDTO>();
        try
        {
            lstRouteDTO = callCycleClient.GetRouteNames(name);
            foreach (RouteMasterDTO item in lstRouteDTO)
            {
                returnList.Add(item);
            }
            return returnList;
        }
        catch (Exception)
        {

            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllRouteMaster(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CallCycleClient callCycleClient = new CallCycleClient();

        List<RouteMasterDTO> data = new List<RouteMasterDTO>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));
            args.ChildReps = ((UserSession.Instance.ChildReps).Replace("rep_code", "rts.rep_code"));

            if (args.ROriginator == "ADMIN")
            {
                parentOriginator = null;
                args.ChildReps = null;
            }

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllRouteMasterSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allroutemaster");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = callCycleClient.GetAllRouteMaster(parentOriginator, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllRouteMasterSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RouteName":
                sortStr = " name " + sortoption;
                break;
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "RepName":
                sortStr = " rep_name " + sortoption;
                break;
            default:
                sortStr = " name desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetRouteTargetFromRouteId(string routeid)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CallCycleClient callCycleClient = new CallCycleClient();
        RouteMasterDTO routeDTO = new RouteMasterDTO();

        string returnvalue = string.Empty;

        try
        {
            routeDTO = callCycleClient.GetRouteTargetFromRouteId(Convert.ToInt32(routeid));

            //if(routeDTO!=null){
            //    returnvalue = routeDTO.Target.ToString();
            //}

        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return serializer.Serialize(routeDTO);
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCallCycleContactsForRep(string repCode, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;


        if (sortExpression != null && sortExpression.Count > 0)
        {
            argsDTO.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetSortTypeCallCycles(item.Field);
                if (string.IsNullOrEmpty(argsDTO.OrderBy))
                    argsDTO.OrderBy = item.ToExpression();
                else
                    argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            argsDTO.OrderBy = "callcycle_id ASC";
        }
        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = 1000;// take;

        List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
        CallCycleClient callCycleClient = new CallCycleClient();

        argsDTO.AdditionalParams = "rep_code = \'" + repCode + "\'";
        listCallCycleContact = callCycleClient.GetCallCycleContacts(argsDTO, 0, false, "");
        Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (listCallCycleContact != null && listCallCycleContact.Count > 0)
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = listCallCycleContact.Count;
        }
        else
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = 0;

        }

        return dataSourceResult;

    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCallCycleContactsForNewEntry(string repCode, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
        if (!String.IsNullOrEmpty(UserSession.Instance.RepCode))
            argsDTO.ChildOriginators = "(" + argsDTO.ChildOriginators + " OR c.rep_code =\'" + UserSession.Instance.RepCode + "\')";
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;


        if (sortExpression != null && sortExpression.Count > 0)
        {
            argsDTO.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetSortTypeCallCycles(item.Field);
                if (string.IsNullOrEmpty(argsDTO.OrderBy))
                    argsDTO.OrderBy = item.ToExpression();
                else
                    argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            argsDTO.OrderBy = "callcycle_id ASC";
        }
        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = 10000;// take;

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "outletsforschedule");
            argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
        CallCycleClient callCycleClient = new CallCycleClient();

        //if (CallCycleID != 0)
        //    listCallCycleContact = callCycleClient.GetCallCycleContacts(argsDTO, CallCycleID, false, "");
        //else
        //    listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
        //Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;
        listCallCycleContact = callCycleClient.GetCallCycleContactsForNewEntry(argsDTO, repCode);
        Session[CommonUtility.CALL_CYCLE_CONTACTS] = listCallCycleContact;

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (listCallCycleContact != null && listCallCycleContact.Count > 0)
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = listCallCycleContact.Count;
        }
        else
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = 0;

        }

        return dataSourceResult;

    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCallCycleContactsForSelectedRoute(int callCycleID, int routeMasterId, int take, int skip, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CallCycleClient callCycleClient = new CallCycleClient();
        ArgsDTO args = new ArgsDTO();
        //string callcycycleid = string.Empty;

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.Originator = UserSession.Instance.UserName;

        args.ChildOriginators = args.ChildOriginators.Replace("originator", "C.created_by");
        if (!String.IsNullOrEmpty(UserSession.Instance.RepCode))
            args.ChildOriginators = "(" + args.ChildOriginators + " OR c.rep_code =\'" + UserSession.Instance.RepCode + "\')";
        args.StartIndex = 0;
        args.RowCount = take;
        args.AdditionalParams = "route_master_id = " + routeMasterId.ToString();

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        args.RepType = UserSession.Instance.RepType;
        args.ShowCount = 0;

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null && sortExpression.Count > 0)
        {
            args.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetSortTypeCallCycles(item.Field);
                if (string.IsNullOrEmpty(args.OrderBy))
                    args.OrderBy = item.ToExpression();
                else
                    args.OrderBy = args.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            args.OrderBy = "callcycle_id ASC";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "leadcontact", args.LeadStage);
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        List<CallCycleContactDTO> callcycleContactDtoList = callCycleClient.GetCallCycleContacts(args, callCycleID, false, args.RepType);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (callcycleContactDtoList != null && callcycleContactDtoList.Count > 0)
        {
            dataSourceResult.Data = callcycleContactDtoList;
            dataSourceResult.Total = callcycleContactDtoList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = callcycleContactDtoList;
            dataSourceResult.Total = 0;
        }

        //OpportunityService.Close();

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveRouteAssignAndOutlets(OriginatorRouteOutletsDTO originatorRouteOutletsDTO)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CallCycleClient callCycleClient = new CallCycleClient();

        originatorRouteOutletsDTO.Originator.RepCode = originatorRouteOutletsDTO.Route.RepCode;
        originatorRouteOutletsDTO.Originator.UserName = UserSession.Instance.UserName;
        originatorRouteOutletsDTO.Originator.DeptString = UserSession.Instance.OriginatorString;

        originatorRouteOutletsDTO.Route.CreatedBy = UserSession.Instance.UserName;
        originatorRouteOutletsDTO.Route.LastModifiedBy = UserSession.Instance.UserName;

        originatorRouteOutletsDTO.CreatedBy = UserSession.Instance.UserName;
        originatorRouteOutletsDTO.LastModifiedBy = UserSession.Instance.UserName;
        originatorRouteOutletsDTO.DelFlag = "N";
        originatorRouteOutletsDTO.CcType = UserSession.Instance.RepType;

        if (UserSession.Instance.OriginatorString.Equals("ASE"))
            originatorRouteOutletsDTO.Originator.Name = UserSession.Instance.Originator;

        string returnvalue = string.Empty;
        try
        {
            bool status = false;
            status = callCycleClient.SaveOriginatorRouteOutlets(originatorRouteOutletsDTO);

            if (status)
            {
                returnvalue = "true";
            }
            else
            {
                returnvalue = "false";
            }
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return returnvalue;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetTMEItinerary()
    {
        string parentOriginator = "";
        DateTime currentDate = DateTime.Now;
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CallCycleClient callCycleClient = new CallCycleClient();
        ItineraryHeaderDTO data = new ItineraryHeaderDTO();



        DataSourceResult obj_dataSourceResult = null;
        try
        {
            //  data = callCycleClient.GetItineraryDetailsForUser(parentOriginator, currentDate);
            data = Session[CommonUtility.ITINERARY_HEADER_DATA] as ItineraryHeaderDTO;
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data.ItineraryDetailList;
            obj_dataSourceResult.Total = data.ItineraryDetailList[0].rowCount;

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllRouteMasterForRep(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string repCode)
    {
        string parentOriginator = "";
        parentOriginator = repCode;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CallCycleClient callCycleClient = new CallCycleClient();

        List<RouteMasterDTO> data = new List<RouteMasterDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));
            args.ChildReps = ((UserSession.Instance.ChildReps).Replace("rep_code", "rts.rep_code"));

            if (args.ROriginator == "ADMIN")
            {
                parentOriginator = null;
                args.ChildReps = null;
            }

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllRouteMasterSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name asc";
                }
            }
            else
            {
                args.OrderBy = " name asc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allroutemaster");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = callCycleClient.GetAllRouteMaster(parentOriginator, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllRouteMasterForCheckList(int CheckListId, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string parentOriginator = "";
        parentOriginator = UserSession.Instance.OriginalUserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CallCycleClient callCycleClient = new CallCycleClient();

        List<RouteMasterDTO> data = new List<RouteMasterDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));
            args.ChildReps = ((UserSession.Instance.ChildReps).Replace("rep_code", "rts.rep_code"));

            if (args.ROriginator == "ADMIN")
            {
                parentOriginator = null;
                args.ChildReps = null;
            }

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllRouteMasterSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name asc";
                }
            }
            else
            {
                args.OrderBy = " name asc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allroutemaster");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = callCycleClient.GetAllRouteMasterForCheckList(CheckListId, parentOriginator, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllSRTerritoryByOriginator(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string OriginatorType, string Originator)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        MasterClient masterClient = new MasterClient();

        List<SRModel> srList = new List<SRModel>();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllSRTerritoryAssignSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " rep_name asc";
            }
        }
        else
        {
            args.OrderBy = " rep_name asc";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "SRTerritoryAssignFilter");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        srList = masterClient.GetAllSRTerritoryByOriginator(args, OriginatorType, Originator).FindAll(x => x.Status == "A");

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (srList != null && srList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = srList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult IsActiveRepIdAndTerritoryId(string RepId, string TerritoryId)
    {
        CallCycleClient callcyclelient = new CallCycleClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = callcyclelient.UpdateSRTerritory(RepId, TerritoryId);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult UpdateSRTerritory(string RepId, string TerritoryId)
    {
        CallCycleClient callcyclelient = new CallCycleClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = callcyclelient.UpdateSRTerritory(RepId, TerritoryId);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult UpdateDBTerritory(string DistributerId, string TerritoryId, string Originator)
    {
        CallCycleClient callcyclelient = new CallCycleClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = callcyclelient.UpdateDBTerritory(DistributerId, TerritoryId, Originator);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllTerritoriesForDistributer(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string OriginatorType, string Originator)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        MasterClient masterClient = new MasterClient();

        List<DistributerTerritoryModel> srList = new List<DistributerTerritoryModel>();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;

        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = SortString.GetAllDistributerTerritoryAssignSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " territory_code asc";
            }
        }
        else
        {
            args.OrderBy = " territory_code asc";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "DistributerTerritoryAssignFilter");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        srList = masterClient.GetAllTerritoriesForDistributer(args, OriginatorType, Originator);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (srList != null && srList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = srList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = srList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }
}
