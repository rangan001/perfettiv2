﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using Peercore.CRM.Shared;
using System.ComponentModel;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Web.UI;

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Serialization;
using System.IO;

/// <summary>
/// Summary description for customer_entry_Service
/// </summary>

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class customer_entry_Service : System.Web.Services.WebService
{

    #region Properties
    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion Properties

    public customer_entry_Service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Sales History - CustomerEntry
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetSalesHistoryDataAndCount(string customercode, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<SalesOrderDTO> data = new List<SalesOrderDTO>();
        DataSourceResult obj_dataSourceResult = null;
        CustomerClient customerClient = new CustomerClient();
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;

            data = customerClient.GetOutstandingDeliveries(customercode);

            if (data != null)
            {
                if (data.Count != 0)
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = 10;
                }
                else
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = 0;
                }
            }
            else
            {
                obj_dataSourceResult = new DataSourceResult();
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }
    #endregion

    #region Customer Address - CustomerEntry
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerAddressDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CustomerClient customerClient = new CustomerClient();
        List<LeadAddressDTO> leadAddressList = null;

        if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            #endregion

            KeyValuePair<string, string> lead_value = CustomerEntry;

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCustomerAddressSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "assignee_no asc";
                }

                if (filter != null)
                {
                    new CommonUtility().SetFilterField(ref filter, "customeraddress", "");
                    args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            leadAddressList = customerClient.GetCustomerAddresses(Server.UrlDecode(CustomerEntry.Key), args);
        }
        else
        {
            leadAddressList = new List<LeadAddressDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (leadAddressList != null)
        {
            if (leadAddressList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = leadAddressList;
                dataSourceResult.Total = leadAddressList.Count;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = leadAddressList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = leadAddressList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetCustomerAddressSortString(string sortby, string sorttype)
    {
        string sortStr = "assignee_no asc";
        string sortoption = "asc";

        switch (sortby)
        {
            case "Address1":
                sortStr = "address_1 " + sortoption;
                break;
            case "Address2":
                sortStr = "address_2 " + sortoption;
                break;
            case "City":
                sortStr = "city " + sortoption;
                break;
            case "State":
                sortStr = "state " + sortoption;
                break;
            case "PostCode":
                sortStr = "postcode " + sortoption;
                break;
            case "Country":
                sortStr = "country " + sortoption;
                break;
            case "Name":
                sortStr = "name " + sortoption;
                break;
            case "Contact":
                sortStr = "contact " + sortoption;
                break;
            case "Telephone":
                sortStr = "telephone " + sortoption;
                break;
            case "Fax":
                sortStr = "fax " + sortoption;
                break;
            default:
                sortStr = "assignee_no asc ";
                break;
        }

        return sortStr;
    }
    #endregion

    #region ContactPerson - CustomerEntry
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerContactPersonDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CustomerClient customerClient = new CustomerClient();
        List<ContactPersonDTO> contactPersonList = null;

        if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.ClientType = UserSession.Instance.ClientType;
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            KeyValuePair<string, string> lead_value = CustomerEntry;

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCustomerContactPersonSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "contact_no asc";
                }

                if (filter != null)
                {
                    new CommonUtility().SetFilterField(ref filter, "contactperson", "");
                    args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            contactPersonList = customerClient.GetCustomerContacts(Server.UrlDecode(CustomerEntry.Key), args, UserSession.Instance.ClientType, true);
        }
        else
        {
            contactPersonList = new List<ContactPersonDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (contactPersonList != null)
        {
            if (contactPersonList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = contactPersonList;
                dataSourceResult.Total = contactPersonList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = contactPersonList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = contactPersonList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetCustomerContactPersonSortString(string sortby, string sorttype)
    {
        string sortStr = "assignee_no asc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "Address1":
                sortStr = "address_1 " + sortoption;
                break;
            case "Address2":
                sortStr = "address_2 " + sortoption;
                break;
            case "City":
                sortStr = "city " + sortoption;
                break;
            case "State":
                sortStr = "state " + sortoption;
                break;
            case "PostCode":
                sortStr = "postcode " + sortoption;
                break;
            case "Country":
                sortStr = "country " + sortoption;
                break;
            case "Name":
                sortStr = "name " + sortoption;
                break;
            case "Contact":
                sortStr = "contact " + sortoption;
                break;
            case "Telephone":
                sortStr = "telephone " + sortoption;
                break;
            case "Fax":
                sortStr = "fax " + sortoption;
                break;
            default:
                sortStr = "assignee_no asc ";
                break;
        }

        return sortStr;
    }
    #endregion

    #region EndUser - CustomerEntry
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerEndUserDataAndCount(int take, int skip, string statuscode, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        EndUserClient enduserClient = new EndUserClient();
        List<EndUserDTO> enduserList = null;

        if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = CustomerEntry.Key;
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            KeyValuePair<string, string> lead_value = CustomerEntry;

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCustomerEndUserSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "cust_code asc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            enduserList = enduserClient.GetEndUsers(args, statuscode);
        }
        else
        {
            enduserList = new List<EndUserDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (enduserList != null)
        {
            if (enduserList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = enduserList;
                dataSourceResult.Total = enduserList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = enduserList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = enduserList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetCustomerEndUserSortString(string sortby, string sorttype)
    {
        string sortStr = "cust_code asc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "EndUserCode":
                sortStr = "enduser_code " + sortoption;
                break;
            case "Name":
                sortStr = "name " + sortoption;
                break;
            case "Contact":
                sortStr = "city " + sortoption;
                break;
            case "Address1":
                sortStr = "address_1 " + sortoption;
                break;
            case "Address2":
                sortStr = "address_2 " + sortoption;
                break;
            case "City":
                sortStr = "city " + sortoption;
                break;
            case "State":
                sortStr = "state " + sortoption;
                break;
            case "PostCode":
                sortStr = "postcode " + sortoption;
                break;
            default:
                sortStr = "cust_code asc ";
                break;
        }

        return sortStr;
    }
    #endregion

    #region Activity - CustomerEntry
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerActivityDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ActivityClient activityClient = new ActivityClient();
        List<ActivityDTO> activityList = null;

        if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(CustomerEntry.Key);
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            KeyValuePair<string, string> lead_value = CustomerEntry;

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCustomerActivitySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "start_date desc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            activityList = activityClient.GetActivitiesForCustomer(args);
        }
        else
        {
            activityList = new List<ActivityDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (activityList != null)
        {
            if (activityList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = activityList;
                dataSourceResult.Total = activityList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = activityList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = activityList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetCustomerActivitySortString(string sortby, string sorttype)
    {
        string sortStr = "start_date desc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "Subject":
                sortStr = "subject " + sortoption;
                break;
            case "StartDate":
                sortStr = "start_date " + sortoption;
                break;
            case "StatusDescription":
                sortStr = "status " + sortoption;
                break;
            case "AssignedTo":
                sortStr = "assigned_to " + sortoption;
                break;
            case "SentMail":
                sortStr = "sent_mail " + sortoption;
                break;
            case "PriorityDescription":
                sortStr = "priority " + sortoption;
                break;
            case "ActivityTypeDescription":
                sortStr = "activity_type " + sortoption;
                break;
            default:
                sortStr = "start_date desc ";
                break;
        }

        return sortStr;
    }
    #endregion

    #region Document - CustomerEntry
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerDocumentDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CustomerClient documentClient = new CustomerClient();
        List<DocumentDTO> documentList = null;

        if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(CustomerEntry.Key);
            args.ManagerMode = true;
            args.CustomerType = "Customer";
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            KeyValuePair<string, string> lead_value = CustomerEntry;

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCustomerDocumentSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "attached_date desc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs\\";
            documentList = documentClient.GetDocumentsForEntryPages(args, path, Session.SessionID.Trim(), true);
        }
        else
        {
            documentList = new List<DocumentDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (documentList != null)
        {
            if (documentList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = documentList;
                dataSourceResult.Total = documentList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = documentList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = documentList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetCustomerDocumentSortString(string sortby, string sorttype)
    {
        string sortStr = "start_date desc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "DocumentName":
                sortStr = "document_name " + sortoption;
                break;
            case "AttachedBy":
                sortStr = "attached_by " + sortoption;
                break;
            case "AttachedDate":
                sortStr = "attached_date " + sortoption;
                break;
            default:
                sortStr = "start_date desc ";
                break;
        }

        return sortStr;
    }
    #endregion

    #region New Customer - CustomerEntry
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetNewCustomerDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CommonClient commonClient = new CommonClient();
        OriginatorClient originatorClient = new OriginatorClient();
        List<LookupDTO> lookupList = null;

        if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;
            string sWhereCls = "";
            string sRepWhereCls = "";

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(CustomerEntry.Key);
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            KeyValuePair<string, string> lead_value = CustomerEntry;

            sWhereCls = UserSession.Instance.ChildOriginators;

            List<OriginatorDTO> listReps = originatorClient.GetRepGroupOriginators(UserSession.Instance.UserName);

            foreach (OriginatorDTO originator in listReps)
            {
                if (sWhereCls.IndexOf(originator.UserName) == -1)
                    sRepWhereCls += "'" + originator.UserName + "',";
            }

            if (sRepWhereCls != "")
            {
                if (sWhereCls.IndexOf("('") >= 0)
                {
                    sWhereCls = sWhereCls.Insert(sWhereCls.IndexOf("('") + 1, sRepWhereCls);
                    sWhereCls = " AND " + sWhereCls;
                }
                else
                {
                    sRepWhereCls = sRepWhereCls.Substring(0, sRepWhereCls.Length - 1);
                    sWhereCls = " AND (" + sWhereCls + " OR originator IN (" + sRepWhereCls + "))";
                }
            }
            else
                sWhereCls = " AND " + sWhereCls;

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetNewCustomerSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "attached_date desc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            lookupList = commonClient.GetLookups("CustomerLookup", sWhereCls, null, args, true);
        }
        else
        {
            lookupList = new List<LookupDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (lookupList != null)
        {
            if (lookupList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = lookupList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = lookupList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetNewCustomerSortString(string sortby, string sorttype)
    {
        string sortStr = "start_date desc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "DocumentName":
                sortStr = "document_name " + sortoption;
                break;
            case "AttachedBy":
                sortStr = "attached_by " + sortoption;
                break;
            case "AttachedDate":
                sortStr = "attached_date " + sortoption;
                break;
            default:
                sortStr = "start_date desc ";
                break;
        }

        return sortStr;
    }
    #endregion

    #region Promotions - CustomerEntry

    // TODO coplete this function to get customer promotion list

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerPromotionsDataAndCount(int take, int skip, string statuscode, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ActivityClient activityClient = new ActivityClient();
        CommonClient commonClient = new CommonClient();
        CustomerClient customerClient = new CustomerClient();
        List<CatalogDTO> catalogList = null;

        if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            //    args.AdditionalParams = "(product_type like lower('%" + repType + "%') )";
            #endregion

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "catalog", "");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCustomerPromotionSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name asc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            catalogList = commonClient.GetAllCatalog(args);
        }


        else
        {
            catalogList = new List<CatalogDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (catalogList != null)
        {
            if (catalogList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = catalogList;
                dataSourceResult.Total = catalogList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = catalogList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = catalogList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }


    private static string GetCustomerPromotionSortString(string sortby, string sorttype)
    {
        string sortStr = "name desc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "PromotionId":
                sortStr = "promo_id " + sortoption;
                break;
            case "PromotionName":
                sortStr = "name " + sortoption;
                break;
            default:
                sortStr = "name desc ";
                break;
        }

        return sortStr;
    }
    #endregion

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveCustomerImageAndGetImageName(string CustomerCode)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CustomerClient customerClient = new CustomerClient();
        CustomerDTO custDTO = new CustomerDTO();

        string returnvalue = string.Empty;

        try
        {
            custDTO = customerClient.GetCustomer(CustomerCode);
            if (custDTO.ImageContent != null)
            {
                string new_tempFileName = custDTO.CustomerCode;
                string new_path = ConfigUtil.ApplicationPath + "docs/customer_images/" + new_tempFileName + ".jpg";

                //Create Document
                if (!isFileOpen(Server.MapPath(new_path)))
                {
                    FileStream fs = new FileStream(Server.MapPath(new_path), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(custDTO.ImageContent);
                    bw.Close();
                    fs.Close();
                }

                //To avoid Unnessesary Serialization of a Lengthy String
                custDTO.ImageContent = null;
                custDTO.ImageName = new_tempFileName + ".jpg";
                custDTO.ImagePath = new_path;
            }
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return serializer.Serialize(custDTO);
    }

    private bool isFileOpen(string file)
    {
        try
        {
            //first we open the file with a FileStream
            using (FileStream stream = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
            {
                try
                {
                    stream.ReadByte();
                    return false;
                }
                catch (IOException)
                {
                    return true;
                }
                finally
                {
                    stream.Close();
                    stream.Dispose();
                }
            }
        }
        catch (IOException)
        {
            return true;
        }
    }

    #region Customer

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllAllCustomerByRouteMasterId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string RouteMasterId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CustomerClient customerClient = new CustomerClient();
        List<CustomerModel> customerList = null;

        string sortby = string.Empty;
        List<Sort> sortExpression = (List<Sort>)sort;

        #region Args Setting
        ArgsModel args = new ArgsModel();
        //args.ChildOriginators = UserSession.Instance.ChildOriginators;
        //args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        //args.Originator = UserSession.Instance.UserName;
        //args.ManagerMode = true;
        args.StartIndex = (skip / take) + 1;
        args.RowCount = take;
        //    args.AdditionalParams = "(product_type like lower('%" + repType + "%') )";
        #endregion

        if (UserSession.Instance != null)
        {
            if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = GetCustomerSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " cust_code asc";
            }
        }
        else
        {
            args = new ArgsModel();
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "customer", "");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        customerList = customerClient.GetAllAllCustomerByRouteMasterId(args, RouteMasterId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (customerList != null)
        {
            if (customerList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = customerList;
                dataSourceResult.Total = customerList[0].TotalCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = customerList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = customerList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllPendingCustomers(string IsReject, string Originator, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CustomerClient customerClient = new CustomerClient();
        List<CustomerModel> customerList = null;

        string sortby = string.Empty;
        List<Sort> sortExpression = (List<Sort>)sort;

        #region Args Setting

        ArgsModel args = new ArgsModel();
        args.StartIndex = (skip / take) + 1;
        args.RowCount = take;

        #endregion

        if (UserSession.Instance != null)
        {
            if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = GetCustomerSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " cust_code asc";
            }
        }
        else
        {
            args = new ArgsModel();
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "customer", "");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        customerList = customerClient.GetAllPendingCustomers(args, IsReject, Originator);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (customerList != null)
        {
            if (customerList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = customerList;
                dataSourceResult.Total = customerList[0].TotalCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = customerList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = customerList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetCustomerSortString(string sortby, string sorttype)
    {
        string sortStr = "name desc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "OutletCode":
                sortStr = "cust_code " + sortoption;
                break;
            case "OutletName":
                sortStr = "name " + sortoption;
                break;
            case "PromotionId":
                sortStr = "promo_id " + sortoption;
                break;
            case "PromotionName":
                sortStr = "name " + sortoption;
                break;
            default:
                sortStr = "name desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel AcceptCustomer(string Originator, string CustCode)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CustomerClient customerClient = new CustomerClient();
        CustomerDTO custDTO = new CustomerDTO();

        string returnvalue = string.Empty;

        var delstatus = customerClient.AcceptCustomer(Originator, CustCode);
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();
        if (delstatus)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }
    
    [WebMethod]
    public Peercore.CRM.Model.ResponseModel RejectCustomer(string Originator, string CustCode)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CustomerClient customerClient = new CustomerClient();
        CustomerDTO custDTO = new CustomerDTO();

        string returnvalue = string.Empty;

        var delstatus = customerClient.RejectCustomer(Originator, CustCode);
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();
        if (delstatus)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }

    #endregion
    
}
