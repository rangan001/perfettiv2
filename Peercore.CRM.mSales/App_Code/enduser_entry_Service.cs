﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using Peercore.CRM.Shared;
using System.ComponentModel;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Web.UI;

using System.Collections.Generic;
using System.Data;
using System.Linq;

/// <summary>
/// Summary description for enduser_entry_Service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class enduser_entry_Service : System.Web.Services.WebService {

    #region Properties
    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }

    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion Properties

    public enduser_entry_Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 

    }

    #region EndUserCustomer - EndUserEntry
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetEndUserCustomerDataAndCount(int take, int skip,string endusercode ,string customercode, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CommonClient commonClient = new CommonClient();
        EndUserClient endUserClient = new EndUserClient();
        List<CustomerDTO> customerList = null;

        if (EndUserEntry.Key != null)
        {
            string sortby = string.Empty;
            string sWhereCls = "";
            string sRepWhereCls = "";

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(customercode);
            args.EnduserCode = HttpContext.Current.Server.UrlDecode(endusercode);
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            KeyValuePair<string, string> lead_value = EndUserEntry;

            sWhereCls = UserSession.Instance.ChildOriginators;
            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetNewCustomerSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "attached_date desc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }
            customerList = endUserClient.GetEndUserCustomers(args); 
        }
        else
        {
            customerList = new List<CustomerDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (customerList != null)
        {
            if (customerList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = customerList;
                dataSourceResult.Total = customerList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = customerList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = customerList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetNewCustomerSortString(string sortby, string sorttype)
    {
        string sortStr = "start_date desc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "DocumentName":
                sortStr = "document_name " + sortoption;
                break;
            case "AttachedBy":
                sortStr = "attached_by " + sortoption;
                break;
            case "AttachedDate":
                sortStr = "attached_date " + sortoption;
                break;
            default:
                sortStr = "start_date desc ";
                break;
        }

        return sortStr;
    }
    #endregion

    #region ContactPerson - EndUserCustomer
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetEndUserContactPersonDataAndCount(int take, int skip,string custid, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        EndUserClient endUserClient = new EndUserClient();
        List<ContactPersonDTO> contactPersonList = null;

        if (EndUserEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;
            KeyValuePair<string, string> lead_value = EndUserEntry;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = custid;
            args.EnduserCode = lead_value.Key;
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetEndUserContactPersonSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "created_date desc";
                }

                if (filter != null)
                {
                    new CommonUtility().SetFilterField(ref filter, "contactperson", "");
                    args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            contactPersonList = endUserClient.GetEndUserContactPersons(args);
        }
        else
        {
            contactPersonList = new List<ContactPersonDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (contactPersonList != null)
        {
            if (contactPersonList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = contactPersonList;
                dataSourceResult.Total = contactPersonList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = contactPersonList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = contactPersonList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetEndUserContactPersonSortString(string sortby, string sorttype)
    {
        string sortStr = "created_date desc";
        string sortoption = "desc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "Title":
                sortStr = "title " + sortoption;
                break;
            case "FirstName":
                sortStr = "first_name " + sortoption;
                break;
            case "LastName":
                sortStr = "last_name " + sortoption;
                break;
            case "Position":
                sortStr = "position " + sortoption;
                break;
            case "Telephone":
                sortStr = "telephone " + sortoption;
                break;
            case "Mobile":
                sortStr = "mobile " + sortoption;
                break;
            case "EmailAddress":
                sortStr = "email_address " + sortoption;
                break;
            default:
                sortStr = "created_date desc ";
                break;
        }

        return sortStr;
    }
    #endregion

    #region Activity - EndUserCustomer
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetEndUserActivityDataAndCount(int take, int skip, string custid, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ActivityClient activityClient = new ActivityClient();
        List<ActivityDTO> activityList = null;

        KeyValuePair<string, string> lead_value = EndUserEntry;

        if (EndUserEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(custid);
            args.EnduserCode = lead_value.Key;
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            
            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetEndUserActivitySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "start_date desc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            activityList = activityClient.GetActivitiesForEndUser(args);
        }
        else
        {
            activityList = new List<ActivityDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (activityList != null)
        {
            if (activityList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = activityList;
                dataSourceResult.Total = activityList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = activityList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = activityList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetEndUserActivitySortString(string sortby, string sorttype)
    {
        string sortStr = "start_date desc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "Subject":
                sortStr = "subject " + sortoption;
                break;
            case "StartDate":
                sortStr = "start_date " + sortoption;
                break;
            case "StatusDescription":
                sortStr = "status " + sortoption;
                break;
            case "AssignedTo":
                sortStr = "assigned_to " + sortoption;
                break;
            case "SentMail":
                sortStr = "sent_mail " + sortoption;
                break;
            case "PriorityDescription":
                sortStr = "priority " + sortoption;
                break;
            case "ActivityTypeDescription":
                sortStr = "activity_type " + sortoption;
                break;
            default:
                sortStr = "start_date desc ";
                break;
        }

        return sortStr;
    }
    #endregion 

    #region Document - EndUserCustomer
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetDocumenDataAndCount(int take, int skip, string custid, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        CustomerClient documentClient = new CustomerClient();
        List<DocumentDTO> documentList = null;

        KeyValuePair<string, string> lead_value = EndUserEntry;

        if (EndUserEntry.Key != null)
        {
            string sortby = string.Empty;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(custid);
            args.EnduserCode = Server.UrlDecode(lead_value.Key);
            args.ManagerMode = true;
            args.CustomerType = "EndUser";
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetDocumentSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "attached_date desc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs\\";
            documentList = documentClient.GetDocumentsForEntryPages(args, path, Session.SessionID.Trim(), true);

            documentClient.Close();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (documentList != null && documentList.Count > 0)
        {
            dataSourceResult.Data = documentList;
            dataSourceResult.Total = documentList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = documentList;
            dataSourceResult.Total = 0;
        }
        return dataSourceResult;
    }

    private static string GetDocumentSortString(string sortby, string sorttype)
    {
        string sortStr = "start_date desc";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "DocumentName":
                sortStr = "document_name "+ sortoption;
                break;
            case "AttachedBy":
                sortStr = "attached_by " + sortoption;
                break;
            case "AttachedDate":
                sortStr = "attached_date " + sortoption;
                break;
            default:
                sortStr = "document_name " + sortoption;
                break;
        }

        return sortStr;
    }
    #endregion 

    #region Price List - EndUserCustomer
    [WebMethod(EnableSession = true)]
    //public DataSourceResult GetEnduserPriceAndCount(int take, int skip, string custid, string enduserid, string effectivedate, IEnumerable<Sort> sort, Filter filter)
    public DataSourceResult GetEnduserPriceAndCount(int take, int skip, string custid, string effectivedate, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        EndUserClient endUserClient = new EndUserClient();
        List<EndUserPriceDTO> enduserpriceList = null;
        KeyValuePair<string, string> lead_value = EndUserEntry;

        if (EndUserEntry.Key != null)
        {
            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(custid);
            args.EnduserCode = lead_value.Key;
            args.ManagerMode = true;
            args.CustomerType = "EndUser";
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetEnduserPriceSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "catlog_code ASC";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

           // string 
            if(string.IsNullOrEmpty(effectivedate))
                effectivedate = DateTime.Now.ToString("dd-MMM-yyyy");
            enduserpriceList = endUserClient.GetEndUserPricing(args, effectivedate);
            

        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (enduserpriceList != null && enduserpriceList.Count > 0)
        {
            dataSourceResult.Data = enduserpriceList;
            dataSourceResult.Total = enduserpriceList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = enduserpriceList;
            dataSourceResult.Total = 0;
        }
        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetProductLookupAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        CommonClient commonClient = new CommonClient();
        List<CatalogDTO> CatalogList = null;

        KeyValuePair<string, string> lead_value = EndUserEntry;

        if (EndUserEntry.Key != null)
        {
            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.AdditionalParams = "";
            //args.CustomerCode = HttpContext.Current.Server.UrlDecode(custid);
            //args.EnduserCode = lead_value.Key;
            //args.ManagerMode = true;
            //args.CustomerType = "EndUser";
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetEnduserPriceSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "catlog_code ASC";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            // string 
            //if (string.IsNullOrEmpty(effectivedate))
            //    effectivedate = DateTime.Now.ToString("dd-MMM-yyyy");
            CatalogList = commonClient.GetProductsLookup(args);
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (CatalogList != null && CatalogList.Count > 0)
        {
            dataSourceResult.Data = CatalogList;
            dataSourceResult.Total = CatalogList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = CatalogList;
            dataSourceResult.Total = 0;
        }
        return dataSourceResult;
    }


    //[WebMethod(EnableSession = true)]
    //public List<EndUserPriceDTO> GetEnduserPriceAndCount1(string custCode)
    //{
    //    EndUserClient endUserClient = new EndUserClient();
    //    List<EndUserPriceDTO> enduserpriceList = null;

    //    KeyValuePair<string, string> lead_value = EndUserEntry;

    //    if (EndUserEntry.Key != null)
    //    {
    //        #region Args Setting
    //        ArgsDTO args = new ArgsDTO();
    //        args.ChildOriginators = UserSession.Instance.ChildOriginators;
    //        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //        args.Originator = UserSession.Instance.UserName;
    //        args.CustomerCode = HttpContext.Current.Server.UrlDecode("STEWENN0");
    //        args.EnduserCode = lead_value.Key;
    //        args.ManagerMode = true;
    //        args.CustomerType = "EndUser";
    //        args.StartIndex =  1;
    //        args.RowCount = 20;
    //        #endregion

    //        if (UserSession.Instance != null)
    //        {
                
    //                args.OrderBy = "catlog_code ASC";

    //        }
    //        else
    //        {
    //            args = new ArgsDTO();
    //        }

    //         string effectivedate = DateTime.Now.ToString("dd-MMM-yyyy");
    //        enduserpriceList = endUserClient.GetEndUserPricing(args, effectivedate);

    //        //foreach (EndUserPriceDTO item in enduserpriceList)
    //        //{
    //        //    item.Category = new EndUserPriceDTO() { CatlogCode = item.CatlogCode, Description = item.Description };
    //        //}
    //    }
    //    return enduserpriceList;
    //    //DataSourceResult dataSourceResult = new DataSourceResult();

    //    //if (enduserpriceList != null && enduserpriceList.Count > 0)
    //    //{
    //    //    dataSourceResult.Data = enduserpriceList;
    //    //    dataSourceResult.Total = enduserpriceList[0].rowCount;
    //    //}
    //    //else
    //    //{
    //    //    dataSourceResult.Data = enduserpriceList;
    //    //    dataSourceResult.Total = 0;
    //    //}
    //    //return dataSourceResult;
    //}

    private static string GetEnduserPriceSortString(string sortby, string sorttype)
    {
        string sortStr = "catlog_code";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "CatlogCode":
                sortStr = "catlog_code " + sortoption;
                break;
            case "Description":
                sortStr = "description " + sortoption;
                break;
            case "Price":
                sortStr = "price " + sortoption;
                break;
            default:
                sortStr = "catlog_code " + sortoption;
                break;
        }

        return sortStr;
    }

    //[WebMethod(EnableSession = true)]
    //public DataSourceResult GetEnduserPriceMasterAndCount(int take, int skip, string custid, string effectivedate, IEnumerable<Sort> sort, Filter filter)
    //{
    //    List<Sort> sortExpression = (List<Sort>)sort;
    //    EndUserClient endUserClient = new EndUserClient();
    //    List<EndUserPriceDTO> enduserpriceList = null;
    //    List<EndUserPriceTestDTO> fdg = new List<EndUserPriceTestDTO>();
    //    KeyValuePair<string, string> lead_value = EndUserEntry;

    //    if (EndUserEntry.Key != null)
    //    {
    //        #region Args Setting
    //        ArgsDTO args = new ArgsDTO();
    //        args.ChildOriginators = UserSession.Instance.ChildOriginators;
    //        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //        args.Originator = UserSession.Instance.UserName;
    //        args.CustomerCode = HttpContext.Current.Server.UrlDecode(custid);
    //        args.EnduserCode = lead_value.Key;
    //        args.ManagerMode = true;
    //        args.CustomerType = "EndUser";
    //        args.StartIndex = (skip / take) + 1;
    //        args.RowCount = take;
    //        #endregion

    //        if (UserSession.Instance != null)
    //        {
    //            if (sortExpression != null && sortExpression.Count > 0)
    //            {
    //                args.OrderBy = GetEnduserPriceSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
    //            }
    //            else
    //            {
    //                args.OrderBy = "catlog_code ASC";
    //            }
    //        }
    //        else
    //        {
    //            args = new ArgsDTO();
    //        }

    //        // string 
    //        if (string.IsNullOrEmpty(effectivedate))
    //            effectivedate = DateTime.Now.ToString("dd-MMM-yyyy");
    //        enduserpriceList = endUserClient.GetEndUserPricing(args, effectivedate);

    //        foreach (EndUserPriceDTO item in enduserpriceList)
    //        {
    //            EndUserPriceTestDTO ff = new EndUserPriceTestDTO()
    //            {
    //                CatlogCode = item.CatlogCode,
    //                CustCode = item.CustCode,
    //                CustomerName = item.CustomerName,
    //                Description = item.Description,
    //                EffectiveDateString = item.EffectiveDateString,
    //                EndUserCode = item.EndUserCode,
    //                EndUserName = item.EndUserName,
    //                LastUpdatedString = item.LastUpdatedString,
    //                Originator = item.Originator,
    //                Price = item.Price,
    //                rowCount = item.rowCount,
    //                Version = item.Version
    //            };
    //            fdg.Add(ff);
    //        }
    //    }

    //    DataSourceResult dataSourceResult = new DataSourceResult();

    //    if (enduserpriceList != null && enduserpriceList.Count > 0)
    //    {
    //        dataSourceResult.Data = enduserpriceList;
    //        dataSourceResult.Total = enduserpriceList[0].rowCount;
    //    }
    //    else
    //    {
    //        dataSourceResult.Data = enduserpriceList;
    //        dataSourceResult.Total = 0;
    //    }
    //    return dataSourceResult;
    //}
    #endregion

    #region Sales - EndUserCustomer
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetEnduserSalesAndCount(int take, int skip, string custid,string year,string period,string incproduct, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        EndUserClient endUserClient = new EndUserClient();
        List<EnduserEnquiryDTO> enduserEnquiryList = null;

        KeyValuePair<string, string> lead_value = EndUserEntry;

        if (EndUserEntry.Key != null)
        {
            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(custid);
            args.EnduserCode = lead_value.Key;
            args.RepCode = "";
            args.ManagerMode = true;
            args.CustomerType = "EndUser";
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            args.SYear =int.Parse(year);
            args.Period = int.Parse(period);
            #endregion

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetEnduserSalesSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "catlog_code ASC";
                }
            }
            else
            {
                args = new ArgsDTO();
            }


            enduserEnquiryList = endUserClient.GetEnduserSalesWithAllProducts(args, ((incproduct=="1") ? true : false), false);
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (enduserEnquiryList != null && enduserEnquiryList.Count > 0)
        {
            dataSourceResult.Data = enduserEnquiryList;
            dataSourceResult.Total = enduserEnquiryList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = enduserEnquiryList;
            dataSourceResult.Total = 0;
        }
        return dataSourceResult;
    }

    private static string GetEnduserSalesSortString(string sortby, string sorttype)
    {
        string sortStr = "catlog_code";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "CatlogCode":
                sortStr = "catlog_code " + sortoption;
                break;
            case "Description":
                sortStr = "description " + sortoption;
                break;
            case "Price":
                sortStr = "price " + sortoption;
                break;
            default:
                sortStr = "catlog_code " + sortoption;
                break;
        }

        return sortStr;
    }
    #endregion
    
}
