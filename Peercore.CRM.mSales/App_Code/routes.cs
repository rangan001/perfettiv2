﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for routes
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

[ScriptService]
public class routes : System.Web.Services.WebService
{
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllRouteMaster(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        MasterClient masterClient = new MasterClient();

        List<RouteMasterModel> data = new List<RouteMasterModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            ArgsModel args = new ArgsModel();
            args.StartIndex = 0;
            args.RowCount = take;
            //args.ROriginator = UserSession.Instance.OriginatorString;
            //args.Originator = UserSession.Instance.OriginalUserName;
            //args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));
            //args.ChildReps = ((UserSession.Instance.ChildReps).Replace("rep_code", "rts.rep_code"));

            //if (args.ROriginator == "ADMIN")
            //{
            //    parentOriginator = null;
            //    args.ChildReps = null;
            //}

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllRouteMaster2SortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " route_master_name desc";
                }
            }
            else
            {
                args.OrderBy = " route_master_name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allroutemaster");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = masterClient.GetAllRoutesMaster(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].TotalCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllRouteMasterSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RouteName":
                sortStr = " name " + sortoption;
                break;
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "RepName":
                sortStr = " rep_name " + sortoption;
                break;
            default:
                sortStr = " name desc ";
                break;
        }

        return sortStr;
    } 
    
    private static string GetAllRouteMaster2SortString(string sortby, string gridSortOrder)
    {
        string sortStr = " route_master_name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RouteMasterName":
                sortStr = " route_master_name " + sortoption;
                break;
            case "RouteMasterCode":
                sortStr = " route_master_code " + sortoption;
                break;
            case "TerritoryName":
                sortStr = " territory_name " + sortoption;
                break;
            default:
                sortStr = " route_master_name desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod]
    public DataSourceResult GetAllRouteMasterByTerritoryId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string TerritoryId)
    {
        MasterClient masterlient = new MasterClient();
        List<RouteMasterModel> objList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllRoutesSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " route_master_id asc";
            }
        }
        else
        {
            args.OrderBy = " route_master_id asc";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "allroutemaster");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        objList = masterlient.GetAllRouteMasterByTerritoryId(args, TerritoryId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (objList != null && objList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = objList;
            dataSourceResult.Total = objList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = objList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetAllRoutesSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " route_master_id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RouteId":
                sortStr = " route_master_id " + sortoption;
                break;
            case "RouteMasterCode":
                sortStr = " route_master_code " + sortoption;
                break;
            case "RouteMasterName":
                sortStr = " route_master_name " + sortoption;
                break;
            default:
                sortStr = " route_master_id asc ";
                break;
        }

        return sortStr;
    }

    //[WebMethod]
    //public List<RouteMasterModel> GetAllRouteMasterByTerritoryId(string TerritoryId)
    //{
    //    MasterClient masterlient = new MasterClient();

    //    ArgsModel args = new ArgsModel();
    //    args.StartIndex = 1;
    //    args.RowCount = 10000;
    //    args.OrderBy = " route_master_id asc";

    //    return masterlient.GetAllRouteMasterByTerritoryId(args, TerritoryId);
    //}

    [WebMethod]
    public DataSourceResult DeleteRouteFromTerritoryByRouteId(string routeId)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteRouteFromTerritoryByRouteId(routeId);

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }   
    
    [WebMethod]
    public DataSourceResult DeleteRouteMasterByRouteId(string routeId)
    {
        MasterClient masterlient = new MasterClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        var delstatus = masterlient.DeleteRouteMasterByRouteId(routeId, "");

        DataSourceResult dataSourceResult = new DataSourceResult();
        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveRoute(string route_id,
                                                    string territory_id,
                                                    string route_code_old,
                                                    string route_code,
                                                    string route_name,
                                                    string created_by)
    {
        MasterClient masterlient = new MasterClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (route_code_old != route_code && masterlient.GetRouteByRouteCode(route_code).RouteMasterId > 0)
        {
            response.Status = false;
            response.StatusCode = "Exsist";
        }
        else
        {
            RouteMasterModel routeMaster = new RouteMasterModel();
            routeMaster.RouteMasterId = Convert.ToInt32((route_id == "") ? "0" : route_id);
            routeMaster.TerritoryId = Convert.ToInt32(territory_id);
            routeMaster.RouteMasterCode = route_code;
            routeMaster.RouteMasterName = route_name;
            routeMaster.CreatedBy = created_by;

            bool delstatus = masterlient.InsertUpdateRouteMaster(routeMaster);

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }

        return response;
    }
}
