﻿/// <summary>
///
/// Class Name  : DbDataAccessHandle.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:32:24 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.DataAccess.Common.Parameters;
using System.Data.Common;
using Peercore.DataAccess.Common.Helpers;
using System.Data; 
#endregion

namespace Peercore.DataAccess.Common
{
    /// <summary>
    /// DbDataAccessHandle
    /// </summary>
    public abstract class DbDataAccessHandle
    {
        /// <summary>
        /// Gets or sets the connection.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        protected DbConnection Connection { get; set; }

        /// <summary>
        /// Gets or sets the factory.
        /// </summary>
        /// <value>
        /// The factory.
        /// </value>
        protected DbProviderFactory Factory { get; set; }

        /// <summary>
        /// Gets or sets the client helper.
        /// </summary>
        /// <value>
        /// The client helper.
        /// </value>
        protected DbClientHelper ClientHelper { get; set; }

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        public abstract int ExecuteNonQuery(DbSqlStructure structure);

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public abstract int ExecuteNonQuery(DbSqlStructure structure, List<DbInputParameter> parameters);

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="hasOutputValue">if set to <c>true</c> [has output value].</param>
        /// <param name="outparametername">The outparametername.</param>
        /// <returns></returns>
        public abstract int ExecuteNonQuery(DbSqlStructure structure, List<DbInputParameter> parameters, bool hasOutputValue, string outparametername);

        /// <summary>
        /// Gets the one value.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public abstract object GetOneValue(DbSqlStructure structure, List<DbInputParameter> parameters);

        /// <summary>
        /// Gets the one value.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        public abstract object GetOneValue(DbSqlStructure structure);

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public abstract DbDataReader ExecuteQuery(DbSqlStructure structure, List<DbInputParameter> parameters);

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        public abstract DbDataReader ExecuteQuery(DbSqlStructure structure);

        /// <summary>
        /// Executes the report query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual DbDataAdapter ExecuteReportQuery(DbSqlStructure structure) 
        { 
            throw new NotImplementedException(); 
        }

        /// <summary>
        /// Executes the report query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual DbDataAdapter ExecuteReportQuery(DbSqlStructure structure, List<DbInputParameter> parameters) 
        { 
            throw new NotImplementedException(); 
        }

        /// <summary>
        /// Executes the disconnected.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual DataTable ExecuteDisconnected(DbSqlStructure structure, List<DbInputParameter> parameters) 
        { 
            throw new NotImplementedException(); 
        }

        /// <summary>
        /// Executes the disconnected.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual DataTable ExecuteDisconnected(DbSqlStructure structure) 
        { 
            throw new NotImplementedException(); 
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual void Commit()
        {
            throw new NotImplementedException(); 
        }

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual void Rollback()
        {
            throw new NotImplementedException(); 
        }

        /// <summary>
        /// Ends this instance.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual void End()
        {
            throw new NotImplementedException(); 
        }
    }
}
