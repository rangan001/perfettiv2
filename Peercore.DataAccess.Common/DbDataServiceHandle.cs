﻿/// <summary>
///
/// Class Name  : DbDataServiceHandle.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:33:12 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Helpers;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.DataAccess.Common.Exceptions;
#endregion

namespace Peercore.DataAccess.Common
{
    /// <summary>
    /// DbDataServiceHandle
    /// </summary>
    public class DbDataServiceHandle : DbDataAccessHandle
    {
        #region - Class Properties -

        #endregion

        #region - Constructors -

        public DbDataServiceHandle(string connectionString, string provider)
        {
            SerProviderFactory(provider);

            SetConnection(connectionString);

            SetDbClient(provider);
        }

        #endregion

        #region - Public Methods -

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Select query execution failed</exception>
        public override DbDataReader ExecuteQuery(DbSqlStructure structure)
        {
            DbCommand dbCommand = null;

            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                //AttachParameters(structure, null, dbCommand);
                //DbDataReader dbDataReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);

                DbDataReader dbDataReader = ClientHelper.ExecuteQueryCommand(dbCommand, null, CommandBehavior.CloseConnection);
                return dbDataReader;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Select query execution failed", ex); ;
            }
            finally
            {
                if (dbCommand != null)
                    dbCommand.Dispose();
            }
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Select query execution failed</exception>
        public override DbDataReader ExecuteQuery(DbSqlStructure structure, List<DbInputParameter> parameters)
        {
            DbCommand dbCommand = null;

            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                //AttachParameters(structure, parameters, dbCommand);

                //DbDataReader dbDataReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                DbDataReader dbDataReader = ClientHelper.ExecuteQueryCommand(dbCommand, parameters, CommandBehavior.CloseConnection);

                return dbDataReader;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Select query execution failed", ex);
            }
            finally
            {
                if (dbCommand != null)
                    dbCommand.Dispose();
            }
        }

        /// <summary>
        /// Executes the report query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Select query execution failed</exception>
        public override DbDataAdapter ExecuteReportQuery(DbSqlStructure structure)
        {
            DbDataAdapter dbDataAdapter = null;
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                dbDataAdapter = Factory.CreateDataAdapter();
                dbDataAdapter.SelectCommand = dbCommand;

                return dbDataAdapter;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Select query execution failed", ex);
            }
            finally
            {
                if (this.Connection != null)
                    Connection.Close();
            }
        }

        /// <summary>
        /// Executes the report query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Select query execution failed</exception>
        public override DbDataAdapter ExecuteReportQuery(DbSqlStructure structure, List<DbInputParameter> parameters)
        {
            DbDataAdapter dbDataAdapter = null;
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                //foreach (DbInputParameter parameter in parameters)
                //{
                //    DbParameter dbParameter = dbCommand.CreateParameter();
                //    dbParameter.ParameterName = parameter.Name;
                //    dbParameter.DbType = parameter.DataType;
                //    dbParameter.Value = parameter.Value;
                //    dbCommand.Parameters.Add(dbParameter);
                //}

                ClientHelper.AttachParameters(dbCommand, parameters);

                dbDataAdapter = Factory.CreateDataAdapter();
                dbDataAdapter.SelectCommand = dbCommand;

                return dbDataAdapter;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Select query execution failed", ex);
            }
            finally
            {
                if (this.Connection != null)
                    Connection.Close();
            }
        }


        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Non query execution failed</exception>
        public override int ExecuteNonQuery(DbSqlStructure structure)
        {
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                //int numberOfRecordsAffected = dbCommand.ExecuteNonQuery();
                int numberOfRecordsAffected = ClientHelper.ExecuteNonQueryCommand(dbCommand, null);

                return numberOfRecordsAffected;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Non query execution failed", ex);
            }
            finally
            {
                if (this.Connection != null)
                    Connection.Close();
            }
        }

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Non query execution failed</exception>
        public override int ExecuteNonQuery(DbSqlStructure structure, List<DbInputParameter> parameters)
        {
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                //foreach (DbInputParameter parameter in parameters)
                //{
                //    DbParameter dbParameter = dbCommand.CreateParameter();
                //    dbParameter.ParameterName = parameter.Name;
                //    dbParameter.DbType = parameter.DataType;
                //    dbParameter.Value = parameter.Value;
                //    dbCommand.Parameters.Add(dbParameter);
                //}

                //int numberOfRecordsAffected = dbCommand.ExecuteNonQuery();
                int numberOfRecordsAffected = ClientHelper.ExecuteNonQueryCommand(dbCommand, parameters);

                return numberOfRecordsAffected;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Non query execution failed", ex);
            }
            finally
            {
                if (this.Connection != null)
                    Connection.Close();
            }
        }

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="hasOutputValue">if set to <c>true</c> [has output value].</param>
        /// <param name="outparametername">The outparametername.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Non query execution failed</exception>
        public override int ExecuteNonQuery(DbSqlStructure structure, List<DbInputParameter> parameters, bool hasOutputValue, string outparametername)
        {
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                int numberOfRecordsAffected = ClientHelper.ExecuteNonQueryCommand(dbCommand, parameters);

                if (hasOutputValue)
                {
                    if (!dbCommand.Parameters.Contains(outparametername))
                        return numberOfRecordsAffected;

                    numberOfRecordsAffected = int.Parse(dbCommand.Parameters[outparametername].Value.ToString());
                }

                return numberOfRecordsAffected;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Non query execution failed", ex);
            }
            finally
            {
                if (this.Connection != null)
                    Connection.Close();
            }
        }

        /// <summary>
        /// Gets the one value.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Scalar execution failed</exception>
        public override object GetOneValue(DbSqlStructure structure)
        {
            DbCommand dbCommand = null;
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;

                var oObject = ClientHelper.ExecuteScalarCommand(dbCommand);

                if (oObject != null && oObject != DBNull.Value)
                    return oObject;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Scalar execution failed", ex);
            }
            finally
            {
                if (dbCommand != null)
                    dbCommand.Dispose();

                if (this.Connection != null)
                    Connection.Close();
            }
        }

        /// <summary>
        /// Gets the one value.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Scalar execution failed</exception>
        public override object GetOneValue(DbSqlStructure structure, List<DbInputParameter> parameters)
        {
            DbCommand dbCommand = null;
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;

                Object oObject = ClientHelper.ExecuteScalarCommand(dbCommand, parameters);

                if (oObject != null && (oObject != DBNull.Value))
                    return oObject;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Scalar execution failed", ex);
            }
            finally
            {
                if (dbCommand != null)
                    dbCommand.Dispose();

                if (this.Connection != null)
                    Connection.Close();
            }
        }

        /// <summary>
        /// Executes the disconnected.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Select query execution failed</exception>
        public override DataTable ExecuteDisconnected(DbSqlStructure structure, List<DbInputParameter> parameters)
        {
            DbDataAdapter dbDataAdapter = null;
            DataTable dataTable = new DataTable();
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                //foreach (DbInputParameter parameter in parameters)
                //{
                //    DbParameter dbParameter = dbCommand.CreateParameter();
                //    dbParameter.ParameterName = parameter.Name;
                //    dbParameter.DbType = parameter.DataType;
                //    dbParameter.Value = parameter.Value;
                //    dbCommand.Parameters.Add(dbParameter);
                //}

                ClientHelper.AttachParameters(dbCommand, parameters);

                dbDataAdapter = Factory.CreateDataAdapter();
                dbDataAdapter.SelectCommand = dbCommand;

                dbDataAdapter.Fill(dataTable);

                return dataTable;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Select query execution failed", ex);
            }
            finally
            {
                if (dbDataAdapter != null)
                    dbDataAdapter.Dispose();

                if (this.Connection != null)
                    Connection.Close();
            }
        }

        /// <summary>
        /// Executes the disconnected.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Select query execution failed</exception>
        public override DataTable ExecuteDisconnected(DbSqlStructure structure)
        {
            DbDataAdapter dbDataAdapter = null;
            DataTable dataTable = new DataTable();
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                dbDataAdapter = Factory.CreateDataAdapter();
                dbDataAdapter.SelectCommand = dbCommand;

                dbDataAdapter.Fill(dataTable);

                return dataTable;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Select query execution failed", ex);
            }
            finally
            {
                if (dbDataAdapter != null)
                    dbDataAdapter.Dispose();

                if (this.Connection != null)
                    Connection.Close();
            }
        }

        #endregion

        #region - Private Methods -

        private void SetDbClient(string provider)
        {
            switch (provider)
            {
                //case Global.IngresClient:
                //    this.ClientHelper = new IngresClientHelper();
                //    break;
                case Global.SqlClient:
                    this.ClientHelper = new SqlClientHelper();
                    break;
                //case Global.OracleClient:
                //    this.ClientHelper = new OracleClientHelper();
                //    break;
                case Global.SqlLightClient:
                    this.ClientHelper = new SqlLightClientHelper();
                    break;
                case Global.OdbcClient:
                    this.ClientHelper = new OdbcClientHelper();
                    break;
                default:
                    this.ClientHelper = new GenricClientHelper();
                    break;
            }
        }

        private void SetConnection(string connectionString)
        {
            try
            {
                Connection = Factory.CreateConnection();
                Connection.ConnectionString = connectionString;
            }
            catch (Exception ex)
            {
                throw new ConnectionFiledException("Conection to the database failed.", ex);
            }
        }

        private void SerProviderFactory(string provider)
        {
            try
            {
                Factory = DbProviderFactories.GetFactory(provider);
            }
            catch (Exception ex)
            {
                throw new DataProviderNotFoundException("Selected data provider does not Exsist.", ex);
            }
        }

        #endregion
    }
}
