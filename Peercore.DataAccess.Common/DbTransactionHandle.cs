﻿/// <summary>
///
/// Class Name  : DbTransactionHandle.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:34:17 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Helpers;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.DataAccess.Common.Exceptions;
#endregion

namespace Peercore.DataAccess.Common
{
    /// <summary>
    /// DbTransactionHandle
    /// </summary>
    public class DbTransactionHandle : DbDataAccessHandle
    {
        #region - Private Properties -

        /// <summary>
        /// Gets or sets the transaction.
        /// </summary>
        /// <value>
        /// The transaction.
        /// </value>
        private DbTransaction Transaction { get; set; }

        #endregion

        #region - Constructors -

        public DbTransactionHandle(string connectionString, string provider)
        {
            SerProviderFactory(provider);

            SetConnection(connectionString);

            SetDbClient(provider);
        }

        public DbTransactionHandle(DbConnection connection, DbTransaction transaction, string provider)
        {
            this.Connection = connection;
            this.Transaction = transaction;
            SetDbClient(provider);
        }

        #endregion

        #region - Public Methods -

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Non query execution failed</exception>
        public override int ExecuteNonQuery(DbSqlStructure structure)
        {
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.Transaction = this.Transaction;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                //int numberOfRecordsAffected = dbCommand.ExecuteNonQuery();
                int numberOfRecordsAffected = ClientHelper.ExecuteNonQueryCommand(dbCommand, null);

                return numberOfRecordsAffected;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Non query execution failed", ex);
            }
            finally
            {
                if (this.Connection != null)
                    Connection.Close();
            }
        }

        /*
        The application life cycle refers to the time at which the application process actually begins 
         * running IIS until the time it stops. This is marked by the application start and end events in the
         * startup file of your application.

        */

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Non query execution failed</exception>
        public override int ExecuteNonQuery(DbSqlStructure structure, List<DbInputParameter> parameters)
        {
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.Transaction = this.Transaction;
                dbCommand.CommandTimeout = 0;

                //foreach (DbInputParameter parameter in parameters)
                //{
                //    DbParameter dbParameter = dbCommand.CreateParameter();
                //    dbParameter.ParameterName = parameter.Name;
                //    dbParameter.DbType = parameter.DataType;
                //    dbParameter.Value = parameter.Value;
                //    dbCommand.Parameters.Add(dbParameter);
                //}

                //int numberOfRecordsAffected = dbCommand.ExecuteNonQuery();

                int numberOfRecordsAffected = this.ClientHelper.ExecuteNonQueryCommand(dbCommand, parameters);

                return numberOfRecordsAffected;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Non query execution failed", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Executes the non query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="hasOutputValue">if set to <c>true</c> [has output value].</param>
        /// <param name="outparametername">The outparametername.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Non query execution failed</exception>
        public override int ExecuteNonQuery(DbSqlStructure structure, List<DbInputParameter> parameters, bool hasOutputValue, string outparametername)
        {
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                DbCommand dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.Transaction = this.Transaction;
                dbCommand.CommandTimeout = 0;

                int numberOfRecordsAffected = this.ClientHelper.ExecuteNonQueryCommand(dbCommand, parameters);

                if (hasOutputValue)
                {
                    if (!dbCommand.Parameters.Contains(outparametername)) return numberOfRecordsAffected;

                    numberOfRecordsAffected = int.Parse(dbCommand.Parameters[outparametername].Value.ToString());
                }

                return numberOfRecordsAffected;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Non query execution failed", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the one value.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Scalar execution failed</exception>
        public override object GetOneValue(DbSqlStructure structure, List<DbInputParameter> parameters)
        {
            DbCommand dbCommand = null;
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.Transaction = this.Transaction;

                //if (parameters != null)
                //{
                //    foreach (DbInputParameter parameter in parameters)
                //    {
                //        DbParameter dbParameter = dbCommand.CreateParameter();
                //        dbParameter.ParameterName = parameter.Name;
                //        dbParameter.DbType = parameter.DataType;
                //        dbParameter.Value = parameter.Value;
                //        dbCommand.Parameters.Add(dbParameter);
                //    }
                //}

                //Object oObject = dbCommand.ExecuteScalar();


                Object oObject = this.ClientHelper.ExecuteScalarCommand(dbCommand, parameters);

                if (oObject != null && (oObject != DBNull.Value))
                    return oObject;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Scalar execution failed", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the one value.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Scalar execution failed</exception>
        public override object GetOneValue(DbSqlStructure structure)
        {
            DbCommand dbCommand = null;
            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.Transaction = this.Transaction;

                //if (parameters != null)
                //{
                //    foreach (DbInputParameter parameter in parameters)
                //    {
                //        DbParameter dbParameter = dbCommand.CreateParameter();
                //        dbParameter.ParameterName = parameter.Name;
                //        dbParameter.DbType = parameter.DataType;
                //        dbParameter.Value = parameter.Value;
                //        dbCommand.Parameters.Add(dbParameter);
                //    }
                //}

                //Object oObject = dbCommand.ExecuteScalar();


                Object oObject = this.ClientHelper.ExecuteScalarCommand(dbCommand);

                if (oObject != null && (oObject != DBNull.Value))
                    return oObject;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Scalar execution failed", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Select query execution failed</exception>
        public override DbDataReader ExecuteQuery(DbSqlStructure structure)
        {
            DbCommand dbCommand = null;

            try
            {

                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.Transaction = this.Transaction;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                DbDataReader dbDataReader = ClientHelper.ExecuteQueryCommand(dbCommand, null, CommandBehavior.CloseConnection);
                return dbDataReader;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Select query execution failed", ex); ;
            }
            finally
            {
                if (dbCommand != null)
                    dbCommand.Dispose();
            }
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="QueryExecutionException">Select query execution failed</exception>
        public override DbDataReader ExecuteQuery(DbSqlStructure structure, List<DbInputParameter> parameters)
        {
            DbCommand dbCommand = null;

            try
            {
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();

                dbCommand = this.Connection.CreateCommand();
                dbCommand.CommandText = structure.Sql;
                dbCommand.Transaction = this.Transaction;
                dbCommand.CommandType = structure.SqlType;
                dbCommand.CommandTimeout = 0;

                //if (parameters != null)
                //{
                //    foreach (DbInputParameter parameter in parameters)
                //    {
                //        DbParameter dbParameter = dbCommand.CreateParameter();
                //        dbParameter.ParameterName = parameter.Name;
                //        dbParameter.DbType = parameter.DataType;
                //        dbParameter.Value = parameter.Value;
                //        dbCommand.Parameters.Add(dbParameter);
                //    }
                //}



                //DbDataReader dbDataReader = dbCommand.ExecuteReader();

                DbDataReader dbDataReader = this.ClientHelper.ExecuteQueryCommand(dbCommand, parameters);

                return dbDataReader;
            }
            catch (Exception ex)
            {
                throw new QueryExecutionException("Select query execution failed", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        public override void Commit()
        {
            if (this.Transaction != null)
                this.Transaction.Commit();
        }

        /// <summary>
        /// Rollbacks this instance.
        /// </summary>
        public override void Rollback()
        {
            if (this.Transaction != null)
                this.Transaction.Rollback();
        }

        /// <summary>
        /// Ends this instance.
        /// </summary>
        public override void End()
        {
            if (this.Connection.State == ConnectionState.Open)
                this.Connection.Close();
        }

        #endregion

        #region - Private Methods -

        /// <summary>
        /// Sets the database client.
        /// </summary>
        /// <param name="provider">The provider.</param>
        private void SetDbClient(string provider)
        {
            switch (provider)
            {
                //case Global.IngresClient:
                //    this.ClientHelper = new IngresClientHelper();
                //    break;
                case Global.SqlClient:
                    this.ClientHelper = new SqlClientHelper();
                    break;
                //case Global.OracleClient:
                //    this.ClientHelper = new OracleClientHelper();
                //    break;
                case Global.OdbcClient:
                    this.ClientHelper = new OdbcClientHelper();
                    break;
                case Global.SqlLightClient:
                    this.ClientHelper = new SqlLightClientHelper();
                    break;
                default:
                    this.ClientHelper = new GenricClientHelper();
                    break;
            }
        }

        /// <summary>
        /// Sets the connection.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <exception cref="ConnectionFiledException">Conection to the database failed.</exception>
        private void SetConnection(string connectionString)
        {
            try
            {
                Connection = Factory.CreateConnection();
                Connection.ConnectionString = connectionString;
            }
            catch (Exception ex)
            {
                throw new ConnectionFiledException("Conection to the database failed.", ex);
            }
        }

        /// <summary>
        /// Sers the provider factory.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <exception cref="DataProviderNotFoundException">Selected data provider does not Exsist.</exception>
        private void SerProviderFactory(string provider)
        {
            try
            {
                Factory = DbProviderFactories.GetFactory(provider);
            }
            catch (Exception ex)
            {
                throw new DataProviderNotFoundException("Selected data provider does not Exsist.", ex);
            }
        }

        #endregion
    }
}