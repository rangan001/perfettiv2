﻿/// <summary>
///
/// Class Name  : ExtensionMethods.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Kushan Fernando
/// Created On  : 5/22/2012 9:47:11 AM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System;
using System.Data.Common;
using System.Linq;
#endregion

namespace Peercore.DataAccess.Common
{
    /// <summary>
    /// ExtensionMethods
    /// </summary>
    public static class ExtensionMethods
    {
        #region - GetValue -

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static object GetValue(this DbDataReader dbDataReader, string name)
        {
            int ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
                return null;
            else
                return dbDataReader.GetValue(ordinal);
        }

        #endregion

        #region - GetDateTime -

        /// <summary>
        /// Gets the date time.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static DateTime GetDateTime(this DbDataReader dbDataReader, string name)
        {
            return GetDateTime(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the date time.
        /// </summary>
        /// <param name="dReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="nullable">if set to <c>true</c> [nullable].</param>
        /// <returns></returns>
        public static DateTime? GetDateTime(this DbDataReader dReader, string name, bool IsNullable)
        {
            int ordinal = dReader.GetOrdinal(name);
            if (dReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.DateTime'.");
            }
            else
                return dReader.GetDateTime(ordinal);
        }

        /// <summary>
        /// Gets the date time.
        /// </summary>
        /// <param name="dbDataReader">The database data reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="dateTimeKind">Kind of the date time.</param>
        /// <returns></returns>
        public static DateTime GetDateTime(this DbDataReader dbDataReader, string name, DateTimeKind dateTimeKind)
        {
            return dbDataReader.GetDateTime(name, false, dateTimeKind).Value;
        }

        /// <summary>
        /// Gets the date time.
        /// </summary>
        /// <param name="dReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <param name="dateTimeKind">Kind of the date time.</param>
        /// <returns></returns>
        public static DateTime? GetDateTime(this DbDataReader dReader, string name, bool IsNullable, DateTimeKind dateTimeKind)
        {
            var value = GetDateTime(dReader, name, IsNullable);
            if (value.HasValue)
            {
                if (dateTimeKind == DateTimeKind.Utc)
                    return value.Value.ToUniversalTime();
                else if (dateTimeKind == DateTimeKind.Local)
                    return value.Value.ToLocalTime();
                else
                    return value.Value;
            }
            else
                return null;
        }

        #endregion

        #region - GetDouble -

        /// <summary>
        /// Gets the double.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static double GetDouble(this DbDataReader dbDataReader, string name)
        {
            return GetDouble(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the double.
        /// </summary>
        /// <param name="dbDataReader">The db data reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static double? GetDouble(this DbDataReader dbDataReader, string name, bool IsNullable)
        {
            int ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Double'.");
            }
            else
                return dbDataReader.GetDouble(ordinal);
        }

        #endregion

        #region - GetDecimal -

        /// <summary>
        /// Gets the decimal.
        /// </summary>
        /// <param name="dbDataReader">The i reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static decimal GetDecimal(this DbDataReader dbDataReader, string name)
        {
            return GetDecimal(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the decimal.
        /// </summary>
        /// <param name="dbDataReader">The db data reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static decimal? GetDecimal(this DbDataReader dbDataReader, string name, bool IsNullable)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Decimal'.");
            }
            else
                return dbDataReader.GetDecimal(ordinal);
        }

        #endregion

        #region - GetString -

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string GetString(this DbDataReader dbDataReader, string name)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
                return null;
            else
                return dbDataReader.GetString(ordinal);
        }

        #endregion

        #region - GetBoolean -

        /// <summary>
        /// Gets the boolean.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static bool GetBoolean(this DbDataReader dbDataReader, string name)
        {
            return GetBoolean(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the boolean.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static bool? GetBoolean(this DbDataReader dbDataReader, string name, bool IsNullable)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Boolean'.");
            }
            else
                return dbDataReader.GetBoolean(ordinal);
        }

        #endregion

        #region - GetByte -

        /// <summary>
        /// Gets the byte.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static byte GetByte(this DbDataReader dbDataReader, string name)
        {
            return GetByte(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the byte.
        /// </summary>
        /// <param name="dbDataReader">The db data reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static byte? GetByte(this DbDataReader dbDataReader, string name, bool IsNullable)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Byte'.");
            }
            else
                return dbDataReader.GetByte(ordinal);
        }

        #endregion

        #region - GetBytes -

        /// <summary>
        /// Gets the bytes.
        /// </summary>
        /// <param name="dbDataReader">The db data reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static byte[] GetBytes(this DbDataReader dbDataReader, string name, int length)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
            {
                return null;
            }
            else
            {
                var buffer = new byte[length];
                var actualLength = dbDataReader.GetBytes(ordinal, 0, buffer, 0, length);

                return buffer.Take((int)actualLength).ToArray();
            }
        }
        
        #endregion

        #region - GetDataTypeName -

        /// <summary>
        /// Gets the name of the data type.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string GetDataTypeName(this DbDataReader dbDataReader, string name)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            return dbDataReader.GetDataTypeName(ordinal);
        }

        #endregion

        #region - GetInt16 -

        /// <summary>
        /// Gets the int16.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Int16 GetInt16(this DbDataReader dbDataReader, string name)
        {
            return GetInt16(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the int16.
        /// </summary>
        /// <param name="dReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static Int16? GetInt16(this DbDataReader dReader, string name, bool IsNullable)
        {
            var ordinal = dReader.GetOrdinal(name);
            if (dReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Int16'.");
            }
            else
                return dReader.GetInt16(ordinal);
        }

        #endregion

        #region - GetInt32 -

        /// <summary>
        /// Gets the int32.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Int32 GetInt32(this DbDataReader dbDataReader, string name)
        {
            return GetInt32(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the int32.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static Int32? GetInt32(this DbDataReader dbDataReader, string name, bool IsNullable)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Int32'.");
            }
            else
                return dbDataReader.GetInt32(ordinal);
        }

        #endregion

        #region - GetInt64 -

        /// <summary>
        /// Gets the int64.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Int64 GetInt64(this DbDataReader dbDataReader, string name)
        {
            return GetInt64(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the int64.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Int64? GetInt64(this DbDataReader dbDataReader, string name, bool IsNullable)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Int64'.");
            }
            else
                return dbDataReader.GetInt64(ordinal);
        }

        #endregion

        #region - GetChar -

        /// <summary>
        /// Gets the char.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static char GetChar(this DbDataReader dbDataReader, string name)
        {
            return GetChar(dbDataReader, name, false).Value;
        }

        /// <summary>
        /// Gets the char.
        /// </summary>
        /// <param name="dbDataReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static char? GetChar(this DbDataReader dbDataReader, string name, bool IsNullable)
        {
            var ordinal = dbDataReader.GetOrdinal(name);
            if (dbDataReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Char'.");
            }
            else
                return dbDataReader.GetChar(ordinal);
        }

        #endregion

        #region - GetFloat -

        /// <summary>
        /// Gets the float.
        /// </summary>
        /// <param name="dReader">The data reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static float GetFloat(this DbDataReader dReader, string name)
        {
            return GetFloat(dReader, name, false).Value;
        }

        /// <summary>
        /// Gets the float.
        /// </summary>
        /// <param name="dReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static float? GetFloat(this DbDataReader dReader, string name, bool IsNullable)
        {
            var ordinal = dReader.GetOrdinal(name);
            if (dReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new InvalidCastException("Cannot cast DBNull.Value to type 'System.Single'.");
            }
            else
                return dReader.GetFloat(ordinal);
        }

        #endregion

        #region - GetGuid - 

        /// <summary>
        /// Gets the GUID.
        /// </summary>
        /// <param name="dReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Guid GetGuid(this DbDataReader dReader, string name)
        {
            return GetGuid(dReader, name, false).Value;
        }

        /// <summary>
        /// Gets the GUID.
        /// </summary>
        /// <param name="dReader">The d reader.</param>
        /// <param name="name">The name.</param>
        /// <param name="IsNullable">if set to <c>true</c> [is nullable].</param>
        /// <returns></returns>
        public static Guid? GetGuid(this DbDataReader dReader, string name, bool IsNullable)
        {
            var ordinal = dReader.GetOrdinal(name);
            if (dReader.IsDBNull(ordinal))
            {
                if (IsNullable)
                    return null;
                else
                    throw new Exception("Cannot cast DBNull.Value to type 'System.Guid'.");
            }
            else
                return dReader.GetGuid(ordinal);
        }

        #endregion
    }
}
