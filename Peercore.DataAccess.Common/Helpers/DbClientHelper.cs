﻿/// <summary>
///
/// Class Name  : DbClientHelper.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:26:50 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters; 
#endregion

namespace Peercore.DataAccess.Common.Helpers
{
    /// <summary>
    /// DbClientHelper
    /// </summary>
    public abstract class DbClientHelper
    {
        /// <summary>
        /// Executes the query command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandBehavior">The command behavior.</param>
        /// <returns></returns>
        public virtual DbDataReader ExecuteQueryCommand(DbCommand command, List<DbInputParameter> parameters,CommandBehavior commandBehavior=CommandBehavior.Default)
        {
            AttachParameters(command, parameters);
            FormatDbCommand(command);

            DbDataReader dbDataReader = command.ExecuteReader(commandBehavior);

            return dbDataReader;
        }

        /// <summary>
        /// Executes the non query command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public virtual int ExecuteNonQueryCommand(DbCommand command, List<DbInputParameter> parameters)
        {
            AttachParameters(command, parameters);
            FormatDbCommand(command);

            int rowsEffected = command.ExecuteNonQuery();

            return rowsEffected;
        }

        /// <summary>
        /// Executes the scalar command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public virtual object ExecuteScalarCommand(DbCommand command, List<DbInputParameter> parameters)
        {
            AttachParameters(command, parameters);
            FormatDbCommand(command);

            Object oObject = command.ExecuteScalar();

            return oObject;
        }

        /// <summary>
        /// Executes the scalar command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public virtual object ExecuteScalarCommand(DbCommand command)
        {
            FormatDbCommand(command);

            return command.ExecuteScalar();
        }

        /// <summary>
        /// Attaches the parameters.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        public virtual void AttachParameters(DbCommand command, List<DbInputParameter> parameters)
        {
            if (parameters != null)
            {
                foreach (DbInputParameter parameter in parameters)
                {
                    DbParameter dbParameter = command.CreateParameter();
                    dbParameter.ParameterName = parameter.Name;
                    dbParameter.DbType = parameter.DataType;
                    dbParameter.Value = parameter.Value;
                    dbParameter.Direction = parameter.ParameterDirection;
                    command.Parameters.Add(dbParameter);
                }
            }
        }

        /// <summary>
        /// Formats the database command.
        /// </summary>
        /// <param name="command">The command.</param>
        protected virtual void FormatDbCommand(DbCommand command)
        {
            
        }
    }
}
