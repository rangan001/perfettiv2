﻿/// <summary>
///
/// Class Name  : IngresClientHelper.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:27:34 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System; 
#endregion

namespace Peercore.DataAccess.Common.Helpers
{
    /// <summary>
    /// IngresClientHelper
    /// </summary>
    internal class IngresClientHelper : DbClientHelper
    {
        /// <summary>
        /// Executes the query command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandBehavior">The command behavior.</param>
        /// <returns></returns>
        public override DbDataReader ExecuteQueryCommand(DbCommand command, List<DbInputParameter> parameters, System.Data.CommandBehavior commandBehavior = CommandBehavior.Default)
        {
            return base.ExecuteQueryCommand(command, parameters, commandBehavior);
        }

        /// <summary>
        /// Executes the non query command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public override int ExecuteNonQueryCommand(DbCommand command, List<DbInputParameter> parameters)
        {
            return base.ExecuteNonQueryCommand(command, parameters);
        }

        /// <summary>
        /// Executes the scalar command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public override object ExecuteScalarCommand(DbCommand command, List<DbInputParameter> parameters)
        {
            return base.ExecuteScalarCommand(command, parameters);
        }

        /// <summary>
        /// Attaches the parameters.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        public override void AttachParameters(DbCommand command, List<DbInputParameter> parameters)
        {
            if (parameters != null)
            {
                foreach (DbInputParameter parameter in parameters)
                {
                    DbParameter dbParameter = command.CreateParameter();
                    dbParameter.ParameterName = parameter.Name.Replace('@', '?');
                    dbParameter.DbType = parameter.DataType;

                    if (parameter.Value == DBNull.Value)
                        dbParameter.Value = GetDefaultDbValue(parameter.DataType);
                    else
                        dbParameter.Value = parameter.Value;

                    command.Parameters.Add(dbParameter);
                }
            }
        }

        /// <summary>
        /// Formats the database command.
        /// </summary>
        /// <param name="command">The command.</param>
        protected override void FormatDbCommand(DbCommand command)
        {
            command.CommandText = command.CommandText.Replace('@', '?');
        }

        /// <summary>
        /// Gets the default database value.
        /// </summary>
        /// <param name="dbType">Type of the database.</param>
        /// <returns></returns>
        private object GetDefaultDbValue(DbType dbType)
        {
            switch (dbType)
            {
                case DbType.AnsiString: return DBNull.Value;
                case DbType.AnsiStringFixedLength: return DBNull.Value;
                case DbType.Binary: return DBNull.Value;
                case DbType.Boolean: return DBNull.Value;
                case DbType.Byte: return DBNull.Value;
                case DbType.Currency: return DBNull.Value;
                case DbType.Date: return DBNull.Value;
                case DbType.DateTime: return string.Empty;
                case DbType.DateTime2: return DBNull.Value;
                case DbType.DateTimeOffset: return DBNull.Value;
                case DbType.Decimal: return DBNull.Value;
                case DbType.Double: return DBNull.Value;
                case DbType.Guid: return DBNull.Value;
                case DbType.Int16: return DBNull.Value;
                case DbType.Int32: return DBNull.Value;
                case DbType.Int64: return DBNull.Value;
                case DbType.Object: return DBNull.Value;
                case DbType.SByte: return DBNull.Value;
                case DbType.Single: return DBNull.Value;
                case DbType.String: return DBNull.Value;
                case DbType.StringFixedLength: return DBNull.Value;
                case DbType.Time: return DBNull.Value;
                case DbType.UInt16: return DBNull.Value;
                case DbType.UInt32: return DBNull.Value;
                case DbType.UInt64: return DBNull.Value;
                case DbType.VarNumeric: return DBNull.Value;
                case DbType.Xml: return DBNull.Value;
                default: return DBNull.Value;
            }
        }
    }
}
