﻿/// <summary>
///
/// Class Name  : SqlClientHelper.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:29:08 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters; 
#endregion

namespace Peercore.DataAccess.Common.Helpers
{
    /// <summary>
    /// SqlClientHelper
    /// </summary>
    internal class SqlClientHelper : DbClientHelper
    {
        /// <summary>
        /// Executes the query command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandBehavior">The command behavior.</param>
        /// <returns></returns>
        public override DbDataReader ExecuteQueryCommand(DbCommand command, List<DbInputParameter> parameters, System.Data.CommandBehavior commandBehavior = CommandBehavior.Default)
        {
            return base.ExecuteQueryCommand(command, parameters, commandBehavior);
        }

        /// <summary>
        /// Executes the non query command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public override int ExecuteNonQueryCommand(DbCommand command, List<DbInputParameter> parameters)
        {
            return base.ExecuteNonQueryCommand(command, parameters);
        }

        /// <summary>
        /// Executes the scalar command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public override object ExecuteScalarCommand(DbCommand command, List<DbInputParameter> parameters)
        {
            return base.ExecuteScalarCommand(command, parameters);
        }

        /// <summary>
        /// Attaches the parameters.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        public override void AttachParameters(DbCommand command, List<DbInputParameter> parameters)
        {
            base.AttachParameters(command, parameters);
        }
    }
}
