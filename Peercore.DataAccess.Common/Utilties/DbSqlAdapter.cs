﻿/// <summary>
///
/// Class Name  : DbSqlAdapter.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:25:00 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Peercore.DataAccess.Common.Exceptions; 
#endregion

namespace Peercore.DataAccess.Common.Utilties
{
    /// <summary>
    /// DbProviderType
    /// </summary>
    public enum DbProviderType
    {
        /// <summary>
        /// The generic
        /// </summary>
        Generic,

        /// <summary>
        /// The ingres
        /// </summary>
        Ingres,

        /// <summary>
        /// The SQL server
        /// </summary>
        SqlServer,

        /// <summary>
        /// The oracle
        /// </summary>
        Oracle,

        /// <summary>
        /// The SQL light
        /// </summary>
        SqlLight,

        /// <summary>
        /// The ODBC
        /// </summary>
        Odbc
    }

    /// <summary>
    /// DbSqlAdapter
    /// </summary>
    public class DbSqlAdapter
    {
        /// <summary>
        /// Gets or sets the name of the table.
        /// </summary>
        /// <value>
        /// The name of the table.
        /// </value>
        public string TableName { get; set; }

        /// <summary>
        /// Gets or sets the key field.
        /// </summary>
        /// <value>
        /// The key field.
        /// </value>
        public string KeyField { get; set; }

        /// <summary>
        /// Gets or sets the database provider.
        /// </summary>
        /// <value>
        /// The database provider.
        /// </value>
        public string DbProvider { get; set; }

        /// <summary>
        /// The query content
        /// </summary>
        private XDocument QueryContent;

        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>
        /// The provider.
        /// </value>
        public DbProviderType Provider
        {
            get
            {
                switch (this.DbProvider)
                {
                    //case Global.IngresClient:
                    //    return DbProviderType.Ingres;
                    case Global.SqlClient:
                        return DbProviderType.SqlServer;
                    //case Global.OracleClient:
                    //    return DbProviderType.Oracle;
                    case Global.SqlLightClient:
                        return DbProviderType.SqlLight;
                    case Global.OdbcClient:
                        return DbProviderType.Odbc;
                    default: return DbProviderType.Generic;
                }

            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbSqlAdapter"/> class.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="dbProvider">The database provider.</param>
        public DbSqlAdapter(string fileName,string dbProvider)
        {
            this.DbProvider = dbProvider;
            Stream s = Assembly.GetCallingAssembly().GetManifestResourceStream(fileName);
            LoadSqlResource(fileName,s);
            SetTableDetails();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbSqlAdapter"/> class.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        public DbSqlAdapter(string fileName)
        {
            Stream s = Assembly.GetCallingAssembly().GetManifestResourceStream(fileName);
            LoadSqlResource(fileName, s);
            SetTableDetails();
        }

        /// <summary>
        /// Gets the <see cref="DbSqlStructure"/> with the specified key.
        /// </summary>
        /// <value>
        /// The <see cref="DbSqlStructure"/>.
        /// </value>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public DbSqlStructure this[string key]
        {
            get
            {
                return GetSqlStructure(key);
            }

        }

        /// <summary>
        /// Gets the SQL structure.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// <exception cref="XmlSqlStructureException">Cannot find the specified SQL id</exception>
        private DbSqlStructure GetSqlStructure(string key)
        {
            XElement element = QueryContent.Descendants("Query").FirstOrDefault(s => s.Attribute("id").Value.Equals(key));

            if(element==null)
                throw new XmlSqlStructureException("Cannot find the specified SQL id");

            DbSqlStructure structure = new DbSqlStructure();

            if (!string.IsNullOrWhiteSpace(element.Element(this.Provider.ToString()).Value))
            {
                structure.Sql = element.Element(this.Provider.ToString()).Value.Trim();
                structure.SqlType = (CommandType)Enum.Parse(typeof(CommandType), element.Element(this.Provider.ToString()).Attribute("commandType").Value);
            }
            else
            {
                structure.Sql = element.Element(DbProviderType.Generic.ToString()).Value.Trim();
                structure.SqlType = (CommandType)Enum.Parse(typeof(CommandType), element.Element("Generic").Attribute("commandType").Value);
            }

            structure.ProviderType = this.Provider;

            return structure;
        }

        /// <summary>
        /// Loads the SQL resource.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="stream">The stream.</param>
        /// <exception cref="XmlSqlStructureException">Cannot find the specified SQL structure file</exception>
        private void LoadSqlResource(string fileName, Stream stream)
        {
            if (stream == null)
                throw new XmlSqlStructureException("Cannot find the specified SQL structure file");

            this.QueryContent = XDocument.Load(stream);
        }

        /// <summary>
        /// Sets the table details.
        /// </summary>
        private void SetTableDetails()
        {
            XElement element = QueryContent.Descendants("Table").FirstOrDefault(s => s.Attribute("type").Value.Equals("Primary"));

            if (element == null)
                return;

            this.TableName = element.Attribute("name").Value;

            this.KeyField = element.Attribute("key").Value;
        }
    }
}
