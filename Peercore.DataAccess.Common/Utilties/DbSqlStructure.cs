﻿/// <summary>
///
/// Class Name  : DbSqlStructure.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:25:37 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System.Data; 
#endregion

namespace Peercore.DataAccess.Common.Utilties
{
    /// <summary>
    /// DbSqlStructure
    /// </summary>
    public class DbSqlStructure
    {
        /// <summary>
        /// Gets or sets the SQL.
        /// </summary>
        /// <value>
        /// The SQL.
        /// </value>
        public string Sql { get; set; }

        /// <summary>
        /// The SQL type
        /// </summary>
        private CommandType sqlType = CommandType.Text;

        /// <summary>
        /// Gets or sets the type of the SQL.
        /// </summary>
        /// <value>
        /// The type of the SQL.
        /// </value>
        public CommandType SqlType
        {
            get { return sqlType; }
            set { sqlType = value; }
        }

        /// <summary>
        /// Formats the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public DbSqlStructure Format(params object[] args)
        {
            this.Sql = string.Format(this.Sql, args);
            return this;
        }

        /// <summary>
        /// The provider type
        /// </summary>
        private DbProviderType providerType = DbProviderType.Generic;

        /// <summary>
        /// Gets or sets the type of the provider.
        /// </summary>
        /// <value>
        /// The type of the provider.
        /// </value>
        public DbProviderType ProviderType
        {
            get { return providerType; }
            set { providerType = value; }
        }

        /// <summary>
        /// The table name
        /// </summary>
        private string tableName;

        /// <summary>
        /// Gets or sets the name of the table.
        /// </summary>
        /// <value>
        /// The name of the table.
        /// </value>
        public string TableName
        {
            get { return tableName; }
            set { tableName = value; }
        }

        /// <summary>
        /// The key field
        /// </summary>
        private string keyField;

        /// <summary>
        /// Gets or sets the key field.
        /// </summary>
        /// <value>
        /// The key field.
        /// </value>
        public string KeyField
        {
            get { return keyField; }
            set { keyField = value; }
        }
    }
}
