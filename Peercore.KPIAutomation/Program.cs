﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Peercore.KPIAutomation
{
    class Program
    {
        private static Logger logger;

        private static string pathApplication = ConfigurationManager.AppSettings.Get("AppPath");
        private static string saveLocation = ConfigurationManager.AppSettings.Get("SavePath");

        static void Main(string[] args)
        {
            string processStart_KPI = ConfigurationManager.AppSettings.Get("KPIProcessStart");
            string processEnd_KPI = ConfigurationManager.AppSettings.Get("KPIProcessEnd");

            string saveDocStart_KPI = ConfigurationManager.AppSettings.Get("SaveKPIDocStart");
            string saveDocEnd_KPI = ConfigurationManager.AppSettings.Get("SaveKPIDocEnd");

            string emailSendStart_KPI = ConfigurationManager.AppSettings.Get("EmailSendKPIStart");
            string emailSendEnd_KPI = ConfigurationManager.AppSettings.Get("EmailSendKPIDocEnd");

            string IsProcess = ConfigurationManager.AppSettings.Get("IsProcess");
            string IsReport = ConfigurationManager.AppSettings.Get("IsReport");
            string IsEmail = ConfigurationManager.AppSettings.Get("IsEmail");

            string logFile = ConfigurationManager.AppSettings["KPIAuto:log"];

            logger = new LoggerConfiguration()
                //.WriteTo.MSSqlServer(
                //    connectionString: operationConnectionStr,
                //    period: TimeSpan.Zero,
                //    batchPostingLimit: 5,
                //    autoCreateSqlTable: false,
                //    tableName: tableName,
                //    restrictedToMinimumLevel: LogEventLevel.Error,
                //    columnOptions: columnOption)
                .WriteTo.RollingFile(pathFormat: logFile, restrictedToMinimumLevel: LogEventLevel.Information)
                .CreateLogger();

            bool isSuccess = false;

            logger.Information($"Start Process - KPI Automation.");

            if (IsProcess == "1")
            {
                if (IsBetweenTime(processStart_KPI, processEnd_KPI))
                {
                    logger.Information("Start Area KPI Process.");

                    isSuccess = ExecuteKPIActualProcess();

                    logger.Information("End Area KPI Process.");

                    if (isSuccess)
                    {
                        logger.Information("Start Week KPI Process.");

                        isSuccess = ExecuteKPIActualWeeklyProcess();

                        logger.Information("End Week KPI Process.");
                    }
                }
            }

            if (IsReport == "1")
            {
                if (IsBetweenTime(saveDocStart_KPI, saveDocEnd_KPI))
                {
                    logger.Information("Start document generating.");

                    if (!IsFileExists_KPIActual())
                    {
                        SaveKPIActualReport();

                        logger.Information("KPI Actual Files Successfully Created on " + pathApplication);
                    }

                    //if (!IsFileExists_KPIWeekly())
                    //{
                    //    SaveKPIWeeklyReport();
                    //    Console.WriteLine("KPI Weekly Files Successfully Created on " + pathApplication);
                    //}
                }
            }

            if (IsEmail == "1")
            {
                if (IsBetweenTime(emailSendStart_KPI, emailSendEnd_KPI))
                {
                    logger.Information("Start document emailing.");

                    bool isEmailSent = SendEmailKPIReports();
                    if (isEmailSent)
                    {
                        logger.Information("Email sent successfull.");
                    }
                    else
                    {
                        logger.Information("Email sent un-successfull.");
                    }
                }
            }

            logger.Information($"End Process - KPI Automation.");

            Thread.Sleep(2000);
            System.Environment.Exit(0);
        }

        #region "KPI Report Process"

        public static bool ExecuteKPIActualProcess()
        {
            bool isSuccess = false;

            try
            {
                String connString = ConfigurationManager.ConnectionStrings["PeercoreCRM"].ConnectionString;

                // Connect to SQL
                logger.Information("KPI Area process : Connecting to SQL Server ... ");
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();
                    logger.Information("KPI Area process : Connecting to SQL Server - Successful ");

                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = connection;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateKPIReportActualProcess";

                    objCommand.Parameters.AddWithValue("@selected_date", DateTime.Now);
                    //objCommand.Parameters.AddWithValue("@selected_date", "2020-10-20");
                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        logger.Information("KPI Area process : successfully. ");

                        isSuccess = true;
                    }
                    else
                    {
                        logger.Error("KPI Area process : Error. ");
                    }
                }
            }
            catch (SqlException e)
            {
                logger.Error("KPI Area process : " + e.Message);
            }

            return isSuccess;
        }

        public static bool ExecuteKPIActualWeeklyProcess()
        {
            bool isSuccess = false;
            try
            {
                String connString = ConfigurationManager.ConnectionStrings["PeercoreCRM"].ConnectionString;

                logger.Information("KPI Week process : Connecting to SQL Server ... ");

                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();
                    logger.Information("KPI Week process : Connecting to SQL Server - Successful ");

                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = connection;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateKPIReportActualWeeklyProcess";

                    objCommand.Parameters.AddWithValue("@selected_date", DateTime.Now.ToString("yyyy-MM-dd"));
                    //objCommand.Parameters.AddWithValue("@selected_date", "2020-10-20");
                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        isSuccess = true;

                        logger.Information("KPI Week process : successfully. ");
                    }
                    else
                    {
                        logger.Error("KPI Week process : Error. ");
                    }
                }
            }
            catch (SqlException e)
            {
                logger.Error("KPI Week process : " + e.Message);
            }
            return isSuccess;
        }

        private static ReportDocument GetReportDocument_KPIActual()
        {
            try
            {
                ReportDocument rptDoc = new ReportDocument();

                rptDoc.Load(pathApplication + "\\Reports\\kpi_report.rpt");

                //rptDoc.Load(@"D:\Projects\Negete\Perfetti\perfetti\Peercore.KPIAutomation\Reports\kpi_report.rpt");

                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables;

                string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

                crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
                crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
                crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
                //crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());
                crConnectionInfo.Password = strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString();

                Console.WriteLine(crConnectionInfo.ServerName);
                Console.WriteLine(crConnectionInfo.DatabaseName);

                CrTables = rptDoc.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
                {
                    crtableLogoninfo = CrTable.LogOnInfo;
                    crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crtableLogoninfo);
                }

                return rptDoc;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private static ReportDocument GetReportDocument_KPIWeekly()
        {
            try
            {
                ReportDocument rptDoc = new ReportDocument();

                rptDoc.Load(pathApplication + "\\Reports\\kpi_weekly_report.rpt");

                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables;

                string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

                crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
                crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
                crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
                //crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());
                crConnectionInfo.Password = strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString();

                CrTables = rptDoc.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
                {
                    crtableLogoninfo = CrTable.LogOnInfo;
                    crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crtableLogoninfo);
                }

                return rptDoc;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private static void SaveKPIActualReport()
        {
            try
            {
                Console.WriteLine("Start Save Reports - KPI Actual");
                ReportDocument repDoc = new ReportDocument();
                repDoc = GetReportDocument_KPIActual();

                string fileName = ConfigurationManager.AppSettings.Get("FileName_KPIReport");

                if (!Directory.Exists(saveLocation))
                {
                    Directory.CreateDirectory(saveLocation);
                }

                //Save PDF Report
                string downloadAsFilename = saveLocation + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf";
                repDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
                Console.WriteLine(downloadAsFilename);

                //Save Excel file
                Console.WriteLine(saveLocation);
                downloadAsFilename = saveLocation + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
                repDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);

                Console.WriteLine(downloadAsFilename);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void SaveKPIWeeklyReport()
        {
            try
            {
                Console.WriteLine("Start Save Reports - KPI Weekly");
                ReportDocument repDoc = new ReportDocument();
                repDoc = GetReportDocument_KPIWeekly();

                string fileName = ConfigurationManager.AppSettings.Get("FileName_WeeklyReportName");

                if (!Directory.Exists(saveLocation))
                {
                    Directory.CreateDirectory(saveLocation);
                }

                //Save PDF Report
                string downloadAsFilename = saveLocation + fileName + DateTime.Now.ToString("yyyMMdd") + ".pdf";
                repDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
                Console.WriteLine(downloadAsFilename);

                //Save Excel file
                Console.WriteLine(saveLocation);
                downloadAsFilename = saveLocation + fileName + DateTime.Now.ToString("yyyMMdd") + ".xlsx";
                repDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);

                Console.WriteLine(downloadAsFilename);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static bool IsFileExists_KPIActual()
        {
            bool isExist = false;
            string fileName = ConfigurationManager.AppSettings.Get("FileName_KPIReport");
            string downloadAsFilename = saveLocation + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf";
            Console.WriteLine(downloadAsFilename);

            if (File.Exists(downloadAsFilename))
            {
                isExist = true;
            }

            return isExist;
        }

        public static bool IsFileExists_KPIWeekly()
        {
            bool isExist = false;
            string fileName = ConfigurationManager.AppSettings.Get("FileName_WeeklyReportName");
            string downloadAsFilename = saveLocation + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf";
            Console.WriteLine(downloadAsFilename);

            if (File.Exists(downloadAsFilename))
            {
                isExist = true;
            }

            return isExist;
        }

        public static bool SendEmailKPIReports()
        {
            bool isSent = false;

            try
            {
                string fileNameKPIActual = ConfigurationManager.AppSettings.Get("FileName_KPIReport");
                string fileNameKPIWeekly = ConfigurationManager.AppSettings.Get("FileName_WeeklyReportName");

                string downloadAsFilenamepdf = saveLocation + fileNameKPIActual + DateTime.Now.ToString("yyyyMMdd") + ".pdf";
                string downloadAsFilenameex = saveLocation + fileNameKPIActual + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";

                //string downloadWeeklypdf = saveLocation + fileNameKPIWeekly + DateTime.Now.ToString("yyyMMdd") + ".pdf";
                //string downloadWeeklyexcel = saveLocation + fileNameKPIWeekly + DateTime.Now.ToString("yyyMMdd") + ".xlsx";

                string mailBody = ConfigurationManager.AppSettings.Get("mailBody");
                string emailFrom = ConfigurationManager.AppSettings.Get("emailFrom");
                string password = ConfigurationManager.AppSettings.Get("password");

                string mailingList = "iroshf@peercore.com.au";//getEmailList();

                Console.WriteLine(DateTime.Now.ToShortTimeString() + " " + emailFrom);

                if (!string.IsNullOrEmpty(mailingList))
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(emailFrom);
                        //mail.To.Add("iroshf@peercore.com.au");
                        //mail.To.Add("Data.Entry@sl.pvmgrp.com");
                        //mail.To.Add("praga@peercore.com.au");
                        //mail.To.Add("Don.Shehan@PerfettiVanMelle.com");
                        //mail.To.Add(mailingList);

                        string[] emaillist = getEmailListFromConfig();
                        foreach (string email in emaillist)
                        {
                            if (email.Trim().Length > 0)
                            {
                                Console.WriteLine(email);
                                mail.Bcc.Add(email);
                            }
                        }

                        mail.Subject = "KPI - Report for " + DateTime.Now.ToString("dd/MM/yyyy");

                        mail.Body = mailBody;
                        mail.IsBodyHtml = true;
                        mail.Attachments.Add(new Attachment(downloadAsFilenamepdf));
                        mail.Attachments.Add(new Attachment(downloadAsFilenameex));
                        //mail.Attachments.Add(new Attachment(downloadWeeklypdf));
                        //mail.Attachments.Add(new Attachment(downloadWeeklyexcel));

                        var smtp = new SmtpClient
						{
							Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = true,
                            Credentials = new NetworkCredential(emailFrom, password)
						};

						//smtp.UseDefaultCredentials = true;
						//smtp.Credentials = new NetworkCredential(emailFrom, password, "smtp.gmail.com");
						//smtp.Host = "smtp.gmail.com";
						//smtp.Port = 587;
						//smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
						//smtp.EnableSsl = true;

						{
                            smtp.Send(mail);
                            isSent = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now.ToShortTimeString() + " " + ex.Message);
            }

            return isSent;
        }


        #endregion
 
        public static string getEmailList()
        {
            string mailingList = "";

            try
            {
                String connString = ConfigurationManager.ConnectionStrings["PeercoreCRM"].ConnectionString;
                DataTable objDS = new DataTable();
                // Connect to SQL
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();

                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = connection;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetEmailList";

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        if (item["email_recipients"] != DBNull.Value) mailingList = item["email_recipients"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return mailingList;
        }

        public static string[] getEmailListFromConfig()
        {
            List<string> listEmails = new List<string>();
            string[] emaillist;

            try
            {
                String connString = ConfigurationManager.ConnectionStrings["PeercoreCRM"].ConnectionString;
                DataTable objDS = new DataTable();

                // Connect to SQL
                using (SqlConnection connection = new SqlConnection(connString))
                {
                    connection.Open();
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = connection;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetEmailList";
                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                //Add Emails from Database
                if (objDS.Rows.Count > 0)
                {
                    DataRow dr = objDS.Rows[0];
                    emaillist = dr["email_recipients"].ToString().Split(';');

                    foreach (string email in emaillist)
                    {
                        listEmails.Add(email);
                    }
                }

				//Add Emails from .config file
				string emailListKPI = ConfigurationManager.AppSettings.Get("KPIEmailList");
				emaillist = emailListKPI.Split(';');

                foreach (string email in emaillist)
                {
                    listEmails.Add(email);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listEmails.ToArray();
        }

        public static bool IsBetweenTime(string start, string end)
        {
            try
            {
                TimeSpan isStart = TimeSpan.Parse(start);
                TimeSpan isEnd = TimeSpan.Parse(end);

                TimeSpan now = DateTime.Now.TimeOfDay;
                bool isExecute = false;

                if ((now > isStart) && (now < isEnd))
                {
                    //match found
                    isExecute = true;
                }

                return isExecute;
            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}
