﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataHandlers.MediaDataHandler
{
    public class MediaFileDataHandler: TempleteDataHandler
    {

        public bool SaveMediaFile(string repCode,
                                    string fileName, 
                                    string extntion, 
                                    string filePath, 
                                    string thumbnailPath, 
                                    string description,
                                    DateTime createdDate, 
                                    string createdBy)
        {
            bool status = false;

            try
            {
                string query = @"INSERT INTO media(RepCode,MediaFileName,FileExtention,MediaPath,ThumbnailPath,Description,CreatedDate,CreatedBy) 
                                    values(@RepCode,@MediaFileName,@FileExtention,@MediaPath,@ThumbnailPath,@Description,@CreatedDate,@CreatedBy);";

                SqlCmd.Parameters.AddWithValue("@RepCode", repCode);
                SqlCmd.Parameters.AddWithValue("@MediaFileName", fileName);
                SqlCmd.Parameters.AddWithValue("@FileExtention", extntion);
                SqlCmd.Parameters.AddWithValue("@MediaPath", filePath);
                SqlCmd.Parameters.AddWithValue("@ThumbnailPath", thumbnailPath);
                SqlCmd.Parameters.AddWithValue("@Description", description);
                SqlCmd.Parameters.AddWithValue("@CreatedDate", createdDate);
                SqlCmd.Parameters.AddWithValue("@CreatedBy", createdBy);

                SqlCon.Open();
                SqlCmd.CommandText = query;
                SqlCmd.ExecuteNonQuery();
                status = true;

            }
            catch (Exception)
            {
                if (SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }

                throw;
            }
            finally
            {
                SqlCon.Close();
            }

            return status;
        }

        public DataTable GetMediaData(string accessToken,string prefix)
        {
            try
            {
                //accessToken = "CAEE4E26-CF4A-4DBD-B850-0D9F27693F09";
                //accessToken = "DEC8AD60-D1B2-419F-9756-E0E7DE5CAB16";
                string where = "";
                if (!string.IsNullOrEmpty(accessToken) )
                {
                    where = "OR o.access_token = '" + accessToken + "'";
                }
                   
                if (SqlCon.State == ConnectionState.Closed)
                {
                    SqlCon.Open();
                }
                string Qry = @"SELECT m.MediaPath,
                                        m.Description,
                                        m.CustCode,
                                        c.name,
		                                m.CreatedDate
                                FROM media m LEFT JOIN originator o ON
	                                    m.RepCode = o.originator
                                    LEFT JOIN customer c ON
                                        m.CustCode = c.cust_code
                                WHERE m.Status = 1 AND m.CategoryPrefix = '" + prefix +"' AND (m.RepCode IN ('All') " + where + ")";

                SqlCmd.CommandText = Qry;
                SqlDataAdapter dataAdapter = new SqlDataAdapter(SqlCmd);
                dataAdapter.Fill(dataTable);
                return dataTable;
            }
            finally
            {
                SqlCmd.Parameters.Clear();
                if (SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }
            }
        }

        public DataTable GetMediaCategories()
        {
            try
            {
                if (SqlCon.State == ConnectionState.Closed)
                {
                    SqlCon.Open();
                }
                string Qry = @" SELECT category_name,category_prefix
                                FROM media_category
                                WHERE status = 'A' OR status = 'X'";

                SqlCmd.CommandText = Qry;
                SqlDataAdapter dataAdapter = new SqlDataAdapter(SqlCmd);
                dataAdapter.Fill(dataTable);
                return dataTable;
            }
            finally
            {
                SqlCmd.Parameters.Clear();
                if (SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }
            }
        }
        /*
        public string GetRepCodeFromAccessToken(string accessToken)
        {
            try
            {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = SqlCon;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetRepCodeFromAccessToken";

                    objCommand.Parameters.AddWithValue("@accessToken", accessToken);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.VarChar, 20);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    string rtn = objCommand.Parameters["@ReturnVal"].Value.ToString();

                    return rtn;
                
            }
            catch (Exception)
            {
                if (SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }

                throw;
            }
            finally
            {
                SqlCon.Close();
            }
        }
        */
    }


}
