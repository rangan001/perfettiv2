﻿using DataHandlers.MediaDataHandler;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediaApp.MediaForms
{
    public partial class DisplayMedia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string accessToken = "";
                if (Request.QueryString["token"] != null && Request.QueryString["token"] != "")
                {
                    accessToken = Request.QueryString["token"].ToString();
                }

                BindCategories();
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadTableData();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void DynamicTableCreation(DataTable dataTable)
        {
            DataTable dt = dataTable;
            var validextImg = new[] { ".Jpg", ".png", ".jpg", "jpeg", ".gif", ".jfif" };
            var validextVideo = new[] { ".mp4", ".avi", ".flv" };

            string preImagePath = ConfigurationManager.AppSettings["ImagePrePath"];
            string preVideoPath = ConfigurationManager.AppSettings["VideoPrePath"];

            StringBuilder sb = new StringBuilder();
            //Table start.

            sb.Append("<table id='mediaGrid' class='table table-striped  table-sm' cellspacing'0'>");
            sb.Append("<thead height'100px'>");
            //Adding HeaderRow.
            sb.Append("<tr>");

            sb.Append("<th></th>");

            sb.Append("</tr>");
            sb.Append("</thead>");

            //Adding DataRow.
            foreach (DataRow row in dt.Rows)
            {
                sb.Append("<tr>");

                sb.Append("<td style='width:50px;border: 0px solid #ccc'>");

                //foreach (DataColumn column in dt.Columns)
                //{
                if (dt.Columns[0].ColumnName == "MediaPath")
                {
                    string fileExtention = Path.GetExtension(row[dt.Columns[0].ColumnName].ToString());
                    fileExtention = fileExtention.ToLower();
                    if (validextImg.Contains(fileExtention))
                    {
                        sb.Append("<img style='width:100%;'  src =\"" + preImagePath + row[dt.Columns[0].ColumnName].ToString().Replace(@"..\", "") + "\">");
                    }
                    if (validextVideo.Contains(fileExtention))
                    {
                        sb.Append("<video style='width:100%;' controls><source type = \"video/mp4\" src = \"" + preVideoPath + row[dt.Columns[0].ColumnName].ToString().Replace(@"..\", "") + "\">");
                    }
                }

                sb.Append(row[dt.Columns[1].ColumnName].ToString());
                sb.Append(row[dt.Columns[2].ColumnName].ToString());
                sb.Append(" - ");
                sb.Append(row[dt.Columns[3].ColumnName].ToString());
                sb.Append(" - ");
                sb.Append(row[dt.Columns[4].ColumnName].ToString());
                sb.Append(" - ");
                //sb.Append("<input type=\"src\" id=\"delPOIbutton\" value=\"Delete\" onclick=\"deleteRow(this)\"/>");
                sb.Append("</td>");
                
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            //Table end.
            sb.Append("</table>");
            ltTable.Text = sb.ToString();
        }

        public void BindCategories()
        {
            try
            {
                MediaFileDataHandler dataHandler = new MediaFileDataHandler();
                DataTable dt = new DataTable();

                dt = dataHandler.GetMediaCategories();
                ddlCategory.DataSource = dt;

                ddlCategory.DataTextField = "category_name";
                ddlCategory.DataValueField = "category_prefix";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem(" - Select -", "NA"));

            }
            catch (Exception)
            {

                throw;
            }
        }

        public void LoadTableData()
        {
            try
            {
                MediaFileDataHandler dataHandler = new MediaFileDataHandler();
                string accessToken = "";
                if (Request.QueryString["token"] != null && Request.QueryString["token"] != "")
                {
                    accessToken = Request.QueryString["token"].ToString();
                }

                string prefix = ddlCategory.SelectedValue.ToString();
                DynamicTableCreation(dataHandler.GetMediaData(accessToken, prefix));
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}