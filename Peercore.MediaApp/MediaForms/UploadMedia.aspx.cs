﻿using DataHandlers.MediaDataHandler;
using Microsoft.WindowsAPICodePack.Shell;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MediaApp.MediaForms
{
    public partial class UploadMedia : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string accessToken = null;
                if (Request.QueryString["token"] != null && Request.QueryString["token"] != "")
                {
                    accessToken = Request.QueryString["token"].ToString();
                }
                MediaFileDataHandler dt = new MediaFileDataHandler();
                grdImg.DataSource = dt.GetMediaData(accessToken,"");
                grdImg.DataBind();

                LoadRepList();
            }
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string mediaName = Path.GetFileName(FileUploader.FileName);
            int fileSize = FileUploader.PostedFile.ContentLength; //1073741824
            string fileExtention = Path.GetExtension(mediaName).ToLower();
            string folder = "";
            string thumbPath = "";

            string userType = ddlRepList.SelectedValue.ToString();
            var validextImg = new[] { ".Jpg", ".png", ".jpg", "jpeg",".gif" };
            var validextVedio = new[] { ".mp4", ".avi", ".flv" };

            string targetPath = Server.MapPath("~/ThumbnailPath");
            string Location = Server.MapPath("~/MediaFiles");

            //string Location = ConfigurationManager.AppSettings["MediaFilePath"];

            if (validextImg.Contains(fileExtention))
            {
                folder = Location + "\\IMG\\";
            }
            else if (validextVedio.Contains(fileExtention))
            {
                folder = Location + "\\VIDEO\\";
            }
            else
            {
                //Not valied File type
            }

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            if (!(string.IsNullOrEmpty(userType)))
            {
                folder = folder + "\\" + userType + "\\";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
            }

            string SaveLocation = folder + mediaName;

            try
            {
                FileUploader.PostedFile.SaveAs(SaveLocation);
                Response.Write("The file has been uploaded.");
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.Message);
            }

            /** Thubnail Genarate **/
            if (validextImg.Contains(fileExtention))
            {
                thumbPath = copyImage(SaveLocation);
            }
            else if (validextVedio.Contains(fileExtention))
            {
                ShellFile shellFile = ShellFile.FromFilePath(SaveLocation);
                Bitmap bm = shellFile.Thumbnail.Bitmap;
                bm.Save(targetPath + "\\" + mediaName + ".jpg", ImageFormat.Jpeg);
            }
            else
            {
                //Cannot create thumbnail
            }


            string imagePath = @"..\MediaFiles\";
            imagePath = imagePath + (RelativePath(Location,SaveLocation).ToString()) ;
            string thumbImgPath = @"..\MediaFiles\" + mediaName;
            //thumbImg = thumbImg + (RelativePath(Location, SaveLocation).ToString());

            bool isSaved = saveMedia(mediaName, fileExtention,imagePath, thumbImgPath);
        }

        public string RelativePath(string absPath, string relTo)
        {
            string[] absDirs = absPath.Split('\\');
            string[] relDirs = relTo.Split('\\');

            // Get the shortest of the two paths
            int len = absDirs.Length < relDirs.Length ? absDirs.Length :
            relDirs.Length;

            // Use to determine where in the loop we exited
            int lastCommonRoot = -1;
            int index;

            // Find common root
            for (index = 0; index < len; index++)
            {
                if (absDirs[index] == relDirs[index]) lastCommonRoot = index;
                else break;
            }

            // If we didn't find a common prefix then throw
            if (lastCommonRoot == -1)
            {
                throw new ArgumentException("Paths do not have a common base");
            }

            // Build up the relative path
            StringBuilder relativePath = new StringBuilder();

            // Add on the ..
            for (index = lastCommonRoot + 1; index < absDirs.Length; index++)
            {
                if (absDirs[index].Length > 0) relativePath.Append("..\\");
            }

            // Add on the folders
            for (index = lastCommonRoot + 1; index < relDirs.Length - 1; index++)
            {
                relativePath.Append(relDirs[index] + "\\");
            }
            relativePath.Append(relDirs[relDirs.Length - 1]);

            return relativePath.ToString();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

        }

        public string copyImage(string SaveLocation)
        {
            string targetPath = Server.MapPath("~/ThumbnailPath/");
            string imgName = Path.GetFileName(FileUploader.FileName);
            string imgPath = SaveLocation; //Server.MapPath("~/FILE/") + imgName;
            byte[] bytes = Convert.FromBase64String(GetThumbNail(imgPath).Split(',')[1]);
            System.Drawing.Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = System.Drawing.Image.FromStream(ms);
            }

            //targetPath = ConfigurationManager.AppSettings["ThumbnailPath"]; //Server.MapPath("~/Thumbnail/");

            image.Save(targetPath + "\\" + imgName + ".jpg");

            return targetPath = String.Format("{0} {1} .jpg", targetPath, imgName); ;
        }

        public string GetThumbNail(string url)
        {
            string path = url;
            System.Drawing.Image image = System.Drawing.Image.FromFile(path);
            using (System.Drawing.Image thumbnail = image.GetThumbnailImage(50, 70, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero))
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    thumbnail.Save(memoryStream, ImageFormat.Png);
                    Byte[] bytes = new Byte[memoryStream.Length];
                    memoryStream.Position = 0;
                    memoryStream.Read(bytes, 0, (int)bytes.Length);
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    return "data:image/png;base64," + base64String;
                }
            }
        }

        private bool ThumbnailCallback()
        {
            return false;
        }

        public bool saveMedia(string mediaName,string fileExtention,string imagePath, string thumbImgPath)
        {
            bool status = true;
            MediaFileDataHandler mediaDataHandler = new MediaFileDataHandler();
            try
            {
                string repCode = ddlRepList.SelectedValue.ToString();
                if(String.IsNullOrEmpty(repCode))
                {
                    repCode = "All";
                }
                string descrption = txtDescription.Text.ToString();
                DateTime today = DateTime.Now;
                string user = "yasintha";

                status = mediaDataHandler.SaveMediaFile(repCode, mediaName, fileExtention, imagePath, thumbImgPath, descrption,today,user);
            }
            catch (Exception)
            {

                throw;
            }

            return status;
        }

        public void LoadRepList()
        {
            try
            {
                RepDataHandler repDT = new RepDataHandler();
                DataTable dt = repDT.GetRepList();

                ddlRepList.DataTextField = "rep_code";
                ddlRepList.DataValueField = "name";

                ddlRepList.DataSource = dt;
                ddlRepList.DataBind();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}