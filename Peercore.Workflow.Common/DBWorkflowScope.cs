﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using Peercore.Workflow.Common.Exceptions;

namespace Peercore.Workflow.Common
{
    public class DbWorkflowScope:IDisposable
    {
        public DbConnection Connection { get; private set; }

        public string ConnectionString { get; private set; }

        private DbProviderFactory Factory { get; set; }

        public DbTransaction Transaction { get; private set; }

        public bool IsComplete { get; private set; }

        public bool IsCommitSuccessful { get; private set; }

        public string Provider { get; private set; }

        public DbWorkflowScope(string connectionString, string provider)
        {
            this.Provider = provider;
            this.ConnectionString = connectionString;

            SerProviderFactory(provider);

            SetConnection(connectionString);

            if (this.Connection.State != ConnectionState.Open)
                this.Connection.Open();
            Transaction = this.Connection.BeginTransaction();
        }

        public void Commit()
        {
            if (this.Transaction != null)
                this.Transaction.Commit();

            this.IsCommitSuccessful = true;
            this.IsComplete = true;
        }

        public void Rollback()
        {
            if (this.Transaction != null)
                this.Transaction.Rollback();

            this.IsComplete = true;
        }

        public void End()
        {
            if (!this.IsComplete)
            {
                if (this.Transaction != null)
                    this.Transaction.Rollback();
            }
            if (this.Connection.State == ConnectionState.Open)
                this.Connection.Close();

            this.IsComplete=false;
        }

        public void Dispose()
        {
            End();
        }

        private void SetConnection(string connectionString)
        {
            try
            {
                Connection = Factory.CreateConnection();
                Connection.ConnectionString = connectionString;
            }
            catch (Exception ex)
            {
                throw new ConnectionFiledException("Conection to the database failed.", ex);
            }
        }

        private void SerProviderFactory(string provider)
        {
            try
            {
                Factory = DbProviderFactories.GetFactory(provider);
            }
            catch (Exception ex)
            {
                throw new DataProviderNotFoundException("Selected data provider does not Exsist.", ex);
            }
        }
    }
}
