﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ReportServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class ReportService : IReportService
    {
        private string ConnectionString = ConfigurationManager.ConnectionStrings["PeercoreCRM"].ConnectionString;

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }

            return composite;
        }

        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetReportDataForStockAsAt?date={date}&asm={asm}&distributor={distributor}")]
        public DataSet GetReportDataForStockAsAt(string date, string asm, string distributor)
        {
            DataSet ds = new DataSet();
            DataTable objDS = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.Connection = conn;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "sp_Get_DBStock_Report";

                objCommand.Parameters.AddWithValue("@date", date);
                objCommand.Parameters.AddWithValue("@asm", asm);
                objCommand.Parameters.AddWithValue("@distributor", distributor);

                SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                objAdap.Fill(objDS);

                if (objDS.Rows.Count > 0)
                {
                    ds.Tables.Add(objDS);
                }
            }

            return ds;
        }
    }
}
