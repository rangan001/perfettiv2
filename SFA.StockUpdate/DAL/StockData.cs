﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace SFA.StockUpdate.DAL
{
	public class StockData
	{
        private string _connectionString;

        public StockData(IConfiguration IConfiguration)
        {
            _connectionString = IConfiguration.GetConnectionString("DefaultConnectionString");
        }

        public bool UpdateStockOPBBF()
        {
            int rowEffect = 0;
            var status = false;

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("UpdateStockOPB", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    rowEffect = cmd.ExecuteNonQuery();
                }

                if (rowEffect > 0)
                    status = true;
            }
            catch (Exception ex)
            {
                status = false;
            }

            return status;
        }
    }
}

